## Quel rôle pour les citoyens dans l’élaboration et l’application de la loi ?

### Consultation pour une nouvelle Assemblée nationale

# Participation à l'évaluation de la mise en œuvre des lois

<div style="text-align: justify;"><img src="https://consultation.democratie-numerique.assemblee-nationale.fr/an/participation_lois.png"></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">Le contrôle et l'évaluation des politiques publiques est la seconde mission des parlementaires. Pour l'heure, les citoyens ne peuvent pas faire valoir leur point de vue sur l'évaluation de la mise en œuvre des lois, alors qu’ils sont les mieux placés pour en juger.</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">&nbsp;</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">Le numérique peut faciliter la coopération entre citoyens pour le suivi de la mise en œuvre des lois. D’autres formes de contribution à l’évaluation des lois peuvent également être envisagées.</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">&nbsp;</span></div>
<div><i style="font-size: 11pt;"><span style="color: rgb(107, 36, 178);">Comment organiser la remontée d'informations au Parlement et la coordination entre citoyens en matière d'évaluation de la mise en œuvre des lois ?</span></i></div>
<div><br></div>
<div><span style="font-size: 10pt; color: rgb(230, 0, 0);">Nota bene</span><span style="font-size: 10pt;">&nbsp;: par défaut, les contributions les plus populaires apparaissent en tête de la page par ordre décroissant. Vous pouvez si vous le souhaitez afficher les contributions les plus récentes ou les plus anciennes. Pour cela, modifiez le mode de </span><b style="font-size: 10pt;"><u>classement</u></b><span style="font-size: 10pt;"> à droite de «&nbsp;Commentaires&nbsp;» avant la 1ère contribution.</span></div>

#### Commentaires

Un outil numérique devrait permettre, via un tableau de bord, de suivre le cycle de vie d'une loi. Chaque étude d'impact, d'effet d'une loi devrait être systématiquement mise à disposition des citoyens (avec des données en OpenData)

Score : 140 / Réponses : 7

----

Les loi sont souvent issues de programmes de partis, chiffrés lors de la campagne électorale mais jamais évalué. Dans mon métier d'ingénieur, on m'a appris à faire des "retours d'expérience" pour évaluer les points positifs, négatifs, les manques afin de corriger les erreurs et surtout ne pas les reproduire. Si on ne fait pas cela, nous sommes des apprentis-sorciers qui essaient des recettes reprises d'autres pays dans des contextes différents ou des propositions dogmatiques inadaptées.
La mise en œuvre des lois par l'exécutif doit être analysées, mesurées, disséquées par des organismes publics (INSEE, OFCE, Pole Emploi, INRS, ...) dont les rapport devront être rendus publics et facilement accessibles. 
Pour  les projets de loi "significatifs" (gros impact économique, sociétal, budgétaire, éthique,...), une clause de revoyure doit être votée avec la loi pour permettre cette évaluation au bout d'un délai de 12 mois ou plus et éventuellement suspendre la loi ou la modifier. 
Les objectifs de la la loi doivent être affichés clairement et les indicateurs de succès ou d'échec votés avec la loi (nombre de chômeurs, croissance des investissements, nombre de "mariages pour tous", taux de réussite des élèves aux examen,...) afin d'éviter des guerres de chiffres en final.
Il faut aussi que cette évaluation "institutionnelle" puisse être confrontée à l'avis des citoyens via une plateforme numérique. La vérité du terrain doit être prise en compte et réellement écoutée. Les citoyens se sentiraient ainsi plus proches de nos institutions.
Les grands média pourraient aussi être mobilisés pour qu'en phase d'évaluation de la loi, des débats contradictoires soient organisés et qu'ainsi les citoyens soient informés et plus concernés par les lois et leurs impacts. L'école via des cours d'instruction civique pourrait aussi jouer son rôle de formation des futurs citoyens.
Un tel dispositif dispositif aurait certainement l'avantage vertueux de motiver les citoyens à mieux lire et décortiquer les programmes des candidats aux futures élections.

Score : 120 / Réponses : 14

----

**Supprimons l'interdiction du mandat impératif** (article 27 de la Constitution)
Cette disposition était peut-être valable au XIXe siècle, mais aujourd'hui l'activité des parlementaires est scrutée en temps réel via [NosDéputés.fr](https://www.nosdeputes.fr/) par exemple.
Alors comment justifier qu'un élu n'ait aucun compte à rendre ? Ce serait bien la moindre des choses que celui qui trahit ouvertement la confiance de ses électeurs puisse être destitué sans attendre la fin de son mandat.
Mettre un terme à l'article 27 ouvrira ainsi la voie à des députés modernes dont l'action politique sera conçue en concertation permanente avec ses concitoyens.

Score : 106 / Réponses : 7

----

Supprimer la Cour de justice de la République et soumettre les membres du gouvernement à la justice ordinaire

Score : 86 / Réponses : 2

----

Personne n est sensee ignorer la loi mais il faut un bac +7 pour la decrypter. Il serait interessant de  faire valide la comprehension d un texte  de loi si il est incomprehensible ou soumis a interpretation on le modifie

Score : 65 / Réponses : 1

----

Pour Transparency International France, la transparence, l’intégrité et la capacité à rendre compte de l’action publique (« redevabilité ») sont des conditions essentielles du renouveau démocratique. Au-delà de la participation individuelle des citoyens à la procédure parlementaire, cela soulève la question plus large  de la régulation du lobbying. Lorsque son usage est rendu clair et transparent, il peut contribuer à apporter aux décideurs publics des éléments d’information et de compréhension sur des questions toujours plus complexes.

Pour Transparency France, le lobbying doit être clairement situé dans une relation associant décideurs publics, représentants d’intérêts et citoyens. En effet, un cadre adapté doit être dessiné pour clarifier les relations entre les décideurs publics et les représentants d’intérêts, au regard de la société, et créer ainsi les conditions de la confiance des citoyens dans la décision publique. Ce cadre doit être notamment inspiré par trois principes : l’équité d’accès aux décideurs publics, l’intégrité des échanges et la traçabilité de la décision publique adossée à une plus grande transparence. Des règles doivent être adoptées tant pour les représentants d’intérêts que pour l’ensemble des acteurs participant à la décision publique.
1/ Traçabilité Les décisions publiques étant prises en leur nom, les citoyens doivent pouvoir savoir comment leurs représentants fondent leurs décisions : quelles sont les personnes et organisations qu’ils ont rencontrées, quelles sont les positions ou arguments qui leur ont été présentés, quels arbitrages ont été opérés ? Organiser la traçabilité renforce la légitimité et la crédibilité de l’action publique, dans une société où la confiance, tant dans les acteurs publics que les acteurs économiques, est fortement dégradée.
2/ Intégrité Pour Transparency France, le lobbying existe de fait et n’est pas condamnable en soi. Cependant, des scandales, par exemple récemment au sein du Parlement européen, montrent à tous que des dérives existent ou sont possibles. Organiser l’intégrité du lobbying permet d’en prévenir les dérives (opacité, conflits d’intérêts, pantouflage, trafic d’influence, corruption) qui sont alors d’une autre nature.
3/ Équité Si les lobbyistes ont pour activité de défendre des intérêts ou de promouvoir des causes, les décideurs publics doivent, pour leur part, consulter de manière équilibrée les différents groupes d’intérêts concernés par un sujet en débat afin de bénéficier d’informations, de points de vues pluriels, voire contradictoires. Dans un second temps, les décideurs publics doivent arbitrer et fonder leurs décisions en fonction de l’intérêt général. Permettre, par un cadre adapté, l’équité d’accès aux décideurs, c’est s’organiser pour que la décision publique ne soit pas l’expression d’intérêts particuliers prédominants.
Principales recommandations pour le Parlement : transparence des agendas des parlementaires, accroître la transparence des clubs parlementaires et des colloques dits « parlementaires » (obligation d’information sur les activités conduites et les sources de financement), renforcer la transparence entourant les voyages des parlementaires et mieux encadrer la participation aux groupes d’étude et groupes d’amitié, ouvrir la possibilité à la société civile de saisir l’instance chargée de l’application des règles adoptées, créer un statut pour les collaborateurs parlementaires et les soumettre aux mêmes règles de déontologie que les parlementaires, assurer une transparence sur les dons et cadeaux reçus par les parlementaires, inciter les parlementaires à consulter le registre des représentants d’intérêts de la HATVP préalablement à toute rencontre avec un représentants d’intérêts.
Pour plus de détails sur nos recommandations :
-	rapport « Transparence et intégrité du lobbying, un enjeu de démocratie » (octobre 2014) https://www.transparency-france.org/wp-content/uploads/2016/04/Rapport-Lobbying-en-France_Octobre-2014.pdf  
-	Rapport “Pour un Parlement Exemplaire” (mai 2017) https://transparency-france.org/wp-content/uploads/

Score : 58 / Réponses : 1

----

Application des lois: 
Inéligibilité à vie pour toute personne condamnée pour corruption.
Afin d'éviter les "tentations": Interdiction de l'entrée des lobbyistes dans l'enceinte du parlement.
Suppression du pantouflage, par l’obligation de démissionner de la fonction publique pour tout fonctionnaire partant travailler dans le privé, et inversement.
Application des recommandations d'Anticor et Transparency international: obligation pour un-e élu-e de rendre publique sa déclaration de patrimoine, suppression de la réserve parlementaire…

Score : 56 / Réponses : 3

----

Il convient de tenir compte des pétitions existantes ou des collectifs associatifs se regroupant pour dénoncer une loi défaillante et parfois nocive, ayant donné lieu à des enquêtes et investigations en démontrant les "effets pervers" ou les abus.  S'il y a autant de protestations, c'est qu'il y a matière, et que la THEORIE élaborée par les parlementaires (et la navette parlementaire avec le Sénat) est démentie par le principe de réalité et l'application sur le terrain. C'est une remontée d'information appréciable, car les parlementaires et les sénateurs ont du mal à accepter qu'une loi élaborée pendant plusieurs années s'avère inapplicable ou ne donne pas les résultats escomptés. 
Je cite un exemple concret : la loi sur la protection des majeurs donne lieu à une critique  générale, dénonçant un abus de pouvoir de la part des "mandataires judiciaires" qui mène parfois à la ruine des majeurs protégés et à la colère des familles.  Hors malgré les vives critiques et les reportages télévisés ou écrits, il semble impossible de tenir compte des remarques FACTUELLES et VECUES par les familles ou les majeurs protégés eux-mêmes. Pourquoi? Pourquoi ne pas auditionner certains collectifs qui ont réuni des témoignages accablants sur l'application - ou la non application - d'une loi qui présente au final plus d'inconvénients que d'avantages pour les citoyens et ne se justifie pas par l'intérêt général - mais plutôt celui de certains "lobbys"?

Score : 55 / Réponses : 2

----

Supprimer le monopole du déclenchement de poursuites judiciaires par l'administration fiscale en cas de fraude : la justice doit pouvoir enquêter librement et de sa propre initiative, même contre l'avis du ministre

Score : 52 / Réponses : 5

----

Les citoyens devraient pouvoir interpeller l’exécutif  sur des questions concernant par exemple l'application des lois. Pourquoi telle ou telle loi n'est pas appliquée etc.

Score : 45 / Réponses : 9

----

* Préalable pour l'évaluation des lois pour le citoyen, que les lois soient :
	-	faciles d'accès : via le numérique et format papier ? (pour ceux qui n'ont pas accès au numérique ou pour qui ce n'est intuitif d'utiliser l'outil numérique) 
	-	compréhensibles : via des mini-videos créées par l'AN sur le modele de celles du mouvement. Utilisation d'un langage en test : le FALC, Facile à Lire et à Comprendre, Sophie Cluzel connaît le sujet.

Lors du processus de création de la loi, un tableau bord qui reprend toutes les étapes de la loi avec le temps nécessaire à chaque étape, peut permettre d'évaluer aussi le temps total de création avant vote et application.

* Applications des lois :
	- 	Il apparaît évident que toutes les nouvelles lois doivent être évaluées à partir de maintenant, tout comme, il apparaît nécessaire que la loi et ses décrets, soient votés en même temps.
	- 	Un contrôle et une évaluation complète de la loi avant vote (constitutionnalité, lois existantes, etc..) paraît primordial (ex Loi Leonetti). De ce fait, il faut pouvoir inclure les clauses qui permettent d'ajuster la loi si nécessaire.
	- 	Appliquer un temps limite de l'application de la loi et de ses décrets dans le temps (a définir suivant les types de lois), ce qui permettrait d'alerter le gouvernement et les parlementaires quand le temps donné s'apprête à être atteint voire dépassé > continuité du tableau de bord.
	- 	Un contrôle de l'impact des lois et des effets « pervers » dans le temps, rencontre un fort écho.

* Créer et renouveler le dialogue :
Des propositions de bon sens sont à rappeler :
- 	Supprimer la cour de justice de la république et soumettre les membres du gouvernement à la justice ordinaire (promesse de campagne)
- 	Inéligibilité à vie pour toute personne condamnée pour corruption
-	Interdiction de l'entrée des lobbyistes dans l'enceinte du parlement

* Consultations :
Impliquer les acteurs citoyens qui vont devoir faire appliquer la loi et évaluer celle-ci grâce à une assemblée paritaire par régions, réunissant (nombre à définir) :
	-	parlementaires
	-	associatifs
	-	élus locaux
	-	citoyens

Assemblée paritaire à réunir 2 fois par an pour évaluer les lois, grâce à un tirage au sort, similaire à ceux des tribunaux.

Score : 34 / Réponses : 0

----

Rendre TOUS les votes en scrutin public, afin d’avoir une analyse du vote scrutin par scrutin, savoir quel député a voté quelle loi.

Score : 31 / Réponses : 6

----

ÉLEVER LE RETOUR D’EXPÉRIENCE AU RANG D’ÉVALUATION LÉGISLATIVE

Le site de suivi du projet de loi [lien : http://consultation.democratie-numerique.assemblee-nationale.fr/topic/59d733cb12406bdd5d6277ea#comment-59f730a0e8a9656c1a8e73c9] deviendra, enfin, celui de l’évaluation de la loi, une fois promulguée. Cela permettra aux citoyens de contribuer à son évaluation par des retours d’expérience.

Les destinataires de la loi sont incontestablement parmi les mieux placés pour en apprécier les bienfaits. Ils doivent ainsi pouvoir témoigner de ce qu’une nouvelle loi a changé, en positif ou en négatif, dans le quotidien de ceux auxquels elle se destinait. Si cela ne saurait constituer, à soi seul, l’évaluation d’une loi – car il est probable que les témoignages les plus nombreux seront les plus critiques –, cela permettra d’en avoir un aperçu, si chaque contributeur expose en quoi il est concerné par la loi et les raisons de son évaluation positive, négative ou neutre.

Autres propositions dans le cadre de cette consultation : http://constitutiondecodee.blog.lemonde.fr/2017/10/30/participation-numerique/

Score : 31 / Réponses : 2

----

En amont:
- les études d'impact accompagnant les projets de loi sont peu connues du public http://etudesimpact.assemblee-nationale.fr/.
A mon avis, cet outil déjà disponible mérite d'être développé par les services de l'Assemblée nationale (à voir bien entendu en fonction des effectifs disponibles)
- on pourrait également, via par exemple un CODEV, faire remonter des avis citoyens

En aval:
- pourquoi ne pas réaliser une évaluation de la loi à l'aide de citoyens et la compléter au besoin plutôt que d'en prendre une nouvelle?
- la démarche de simplification engagée via le gouvernement sous la précédente législature ne pourrait elle pas se faire au niveau de l'assemblée nationale en prenant bien soin de représenter tous les partis politiques? Ces simplifications envisagées pourrait faire l'objet par la suite d'une consultation citoyennes

Score : 29 / Réponses : 4

----

**decret d'applications**

Actuellement de nombreuses loi sont votées mais pas mises en oeuvre car les decrets ne sont pas pris... De plus suivant le contenu des décrets, le sens et le but de la loi peuvent être altérés.

Il faudrait donc déjà que ceux-ci  soient rédigés au moment du vote de la loi afin que les parlementaires se prononcent sur un lot cohérent

Score : 27 / Réponses : 1

----

**Enregistrer et publier les avis des rapporteurs et du gouvernement sur les amendements**

Lors des débats sur un projet ou une proposition de loi dans l'hémicycle, avant de procéder au vote de chaque amendement, les parlementaires exposent leurs arguments et obtiennent un avis (favorable, défavorable ou sagesse) du rapporteur de la commission ainsi que du gouvernement.

Au Sénat, les services de la séance enregistrent cette information et la reportent dans le système d'informations des amendements rendant ainsi ces informations publiques pour chacun une fois que les débats ont eu lieu. À l'Assemblée nationale, seul le sort de l'amendement est ainsi retranscrit. Il serait particulièrement intéressant pour l'évaluation de la mise en œuvre de la procédure législative de pouvoir disposer de cette information pour les deux chambres.

Score : 24 / Réponses : 1

----

Lors de l’atelier que nous avons organisé à Clichy sur le thème de la participation à l’évaluation de la mise en oeuvre des lois, une idée principale a émergé de la part des citoyens de la Ve circonscription des Hauts-de-Seine :
Créer une plateforme multimédia, multicanal qui informe et qui permet la participation des citoyens. Proposer des “résumés simplifiés” des lois avec des indications sur les “attendus” de la loi dans les années à venir et les objectifs qu’elle est censée atteindre. Les citoyens deviennent ainsi acteurs d’un processus de vérification de l’application de la loi grâce à des indicateurs quantitatifs et qualitatifs définis par l’Assemblée Nationale ou le Gouvernement.

Score : 23 / Réponses : 0

----

**Assurer la publicité des délégations de vote**

La Constitution française donne deux missions au Parlement : contrôler l'action du gouvernement et voter la loi. Si les missions de contrôle font l'objet d'une transparence satisfaisante, le Parlement reste encore à ce jour assez opaque en ce qui concerne le vote de la loi.

Pour les scrutins publics et solennels faisant l'objet d'un relevé public, le Parlement ne publie pas l'ensemble des informations relatives à leur déroulement et notamment l'usage qui est fait par les parlementaires des « délégations de vote ». En effet, les élus absents peuvent charger l'un de leurs collègues de porter leur voix lorsque le motif de leur absence est légitime, mais il n'est pas fait mention de cet usage sur le relevé du scrutin. Ainsi, aucune liste des parlementaires qui étaient physiquement présents lors de chacun de ces votes n'est à ce jour rendue publique. Les citoyens ne peuvent donc savoir si tel ou tel député a été présent lors d'un scrutin à l'Assemblée ou au Sénat.

Des progrès quant à la publication des votants ont été réalisés lors de la précédente législature à l'Assemblée nationale, mais le vote de parlementaires absents à travers le mécanisme de délégation de vote reste totalement opaque, alors que cette information est enregistrée électroniquement et pourrait donc tout simplement être reportée sur le résultat du scrutin.

Pire, à la « Haute Assemblée », le mécanisme des délégations de vote pratiqué est tout simplement contraire aux règles établies par la Constitution. En effet, dans son article 27, la Constitution prévoit que « Le droit de vote des membres du Parlement est personnel » et que lorsque les délégations sont mises en œuvre, « nul ne peut recevoir délégation de plus d'un mandat ». Un parlementaire peut donc théoriquement porter au plus une seule délégation d'un collègue absent. Or, au Sénat, par le mécanisme dit des "votes de groupe", les sénateurs votent pour plusieurs dizaines de leurs collègues absents en déposant des liasses de vote pour tout leur groupe dans les urnes.

Il convient donc :
 - de rendre publique lors de la publication du résultat des scrutins le nom du délégué lorsqu'un député a fait porter sa voix à un autre par le mécanisme des délégations ;
 - d'enrichir les informations publiées relatives aux scrutins (heure et numéro de texte ou d'amendement) afin de permettre de facilement retrouver les articles ou les amendements votés et le compte-rendu de la séance associée ;
 - de s'assurer que les deux chambres du Parlement font un usage conforme à leurs règlements et à la Constitution des délégations de vote.

Score : 23 / Réponses : 0

----

**Rendre publics les accès physiques des lobbyistes au Parlement**

De très nombreux lobbyistes de toutes natures sont accueillis au sein du Parlement pour des colloques ou des rendez vous avec les parlementaires. Si cette activité de partage d'expertise est inhérente à une bonne prise de décision démocratique, il faut que les activités des lobbyistes fassent l'objet d'une plus grande transparence afin d'identifier les cas d'inégalités de moyens entre les différents représentants d'intérêts et le bon respect des règles déontologiques de ces professions.

Un moyen simple d'assurer une plus grande transparence démocratique serait de rendre publiques les entrées des représentants d'intérêts au Parlement. Ceux-ci devant d'ores et déjà s'enregistrer sur les systèmes informatiques de l'Assemblée à chaque visite, il serait très simple techniquement de rendre ce listing public.

Score : 23 / Réponses : 0

----

Chiffrage des lois comme le CBO aux USA

Faire que les calculs d'impact des lois soient:
 - systématique
 - que SI une loi doit etre lancée sans étude d'impact (car urgence) il soit possible au gouvernement, ou AN, ou Sénat de forcer une réévaluation de la loi.
 - le faire comme un organe rattaché au congré (voir le modèle du CBO aux USA: https://fr.wikipedia.org/wiki/Congressional_Budget_Office)
 - que ces évaluations soient TOUTES publiques
 - Il faut chiffrer AVANT le vote, et aussi réévaluer l'ensemble en prennant en compte tous les amendements.

=> RENDRE CES RAPPORTS LISIBLES

Par exemple, meme le plan de financement de la securité sociale de 2018 qui est un document court est difficilement lisible sauf si l'on est du métier.

Donc il faudrait que ce bureau produise un rapport + un résumé simple

Score : 22 / Réponses : 3

----

**Systématiser les scrutins publics sur les votes des lois et de leurs articles**

La Constitution française donne deux missions au Parlement : contrôler l'action du gouvernement et voter la loi. Si les missions de contrôle font l'objet d'une transparence satisfaisante, le Parlement reste encore à ce jour assez opaque en ce qui concerne le vote de la loi.

En effet, l'immense majorité des scrutins réalisés en commission comme en plénière ne font pas l'objet d'un relevé des votes mais simplement d'un décompte à main levée dont seul le résultat est notifié. Seuls quelques rares votes sont effectivement enregistrés par la pratique des scrutins dits publics et solennels, réalisés au Sénat dans des urnes à la tribune et électroniquement à l'Assemblée nationale.

Si le nombre d'amendements mis au vote est tel que systématiser les scrutins publics pourrait paraître complexe, il semble pour autant anormal que les citoyens ne soient pas en mesure de connaître a minima la position de leurs représentants sur les grandes lignes des lois votées au Parlement. À défaut de réaliser des scrutins publics pour l'ensemble des amendements, un socle minimal de transparence de la procédure pourrait être assuré en systématisant les scrutins publics sur chacun des articles des lois amendées.

Un raccourcissement du délai prévu au règlement entre l'annonce d'un scrutin public et sa tenue pourrait également être envisagé afin de fluidifier le déroulement des débats.

Il est en revanche crucial de ne pas tendre à une bureaucratisation des votes : les parlementaires doivent voter dans la foulée du débat sur chacun des amendements et des articles et surtout pas en enchaînant d'un bloc un ensemble de votes à la chaîne lors d'une série de scrutins décorrélés du débat. La méthode du Parlement Européen consistant à organiser des séances de débats, puis séparément des séances de votes en série sont absolument incompréhensibles par les citoyens. Il en est de même pour le mécanisme des secondes délibérations que peuvent aujourd'hui demander le Gouvernement ou le président de la commission saisie au fond ; aujourd'hui permises par la Constitution, celles-ci sont à proscrire : le résultat d'un vote devrait être le résultat du consensus obtenu sur les bancs entre les députés ayant participé au débat et certainement pas un simple entérinement politique d'une décision prise en dehors de la procédure parlementaire.

Score : 22 / Réponses : 0

----

**Rendre publics les débats en Commissions Mixtes Paritaires**

L'accélération des discussions relatives aux projets et propositions de loi par l'emploi de plus en plus systématique de la procédure accélérée (dite d'urgence) pour nombre de textes, confère un rôle crucial à l'étape de conciliation entre le Sénat et l'Assemblée. Ces Commissions Mixtes Paritaires (CMP) qui réunissent 14 parlementaires, 7 de chaque chambre, se tiennent pourtant toujours à huis-clos et leurs délibérations ne font que l'objet d'un compte rendu le plus souvent extrêmement succinct. Au vu de l'impact qu'ont ces décisions sur le texte finalement adopté, ce manque de transparence pose problème et entrave conséquemment toute évaluation de l'empreinte législative.

Il convient donc d'offrir une plus grande transparence aux délibérations qui ont lieu dans ces CMP en :
- systématisant la captation vidéo et retransmission en direct de ces réunions ;
- produisant un compte-rendu détaillé des débats qui s'y déroulent comme pour l'hémicycle et les commissions, et ce y compris lors de l'échec de ces CMP.

Score : 21 / Réponses : 0

----

Organisation de “conférences de citoyens” pour contrôler l'application des lois

Les instituts de sondage développent depuis longtemps des dispositifs d’enquête qualitatifs allant plus loin que les classiques “études d’opinion” que l’on diffuse dans les médias. Ces dispositifs permettent de réduire les coûts de consultation sans perdre l’intérêt de la représentativité des opinions recueillies. 

Après avoir participé à son élaboration (par la remise d'un texte commun), la conférence de citoyens pourrait être réunie pour suivre l'application d'une loi. Cette fois, la conférence recevrait l'avis d'experts et de personnes concernées par l'application de la loi. 
Ainsi, suite à ces échanges, la conférence de citoyens remettraient aux députés une série de recommandation et points de tensions vis-à-vis de l'application de la loi ou son essence même.

Score : 19 / Réponses : 0

----

**Publier au JO la participation aux auditions organisées dans le cadre du travail des rapporteurs**

Une étape importante du travail législatif est réalisée en amont des délibérations en commissions et en hémicycle. Il s'agit des auditions permettant au rapporteur du texte mais également aux parlementaires souhaitant s'investir sur le travail législatif ou de contrôle concerné, d'écouter la diversité des points de vues relatifs aux dispositions étudiées.

Si ces auditions enrichissent souvent le travail du rapporteur lors de l'écriture de son rapport et si la liste des personnes auditionnées fait le plus souvent l'objet d'une publication en annexe, les citoyens ne peuvent pas savoir quand ces auditions ont eu lieu ni quels parlementaires y ont participé.

Or, le Journal Officiel rend public tous les matins la liste des parlementaires ayant été présents lors des réunions de commissions sous l'action des administrateurs du Sénat et de l'Assemblée qui notent les présents et les reportent pour publication au Journal Officiel.

Il serait donc intéressant pour mieux informer les citoyens sur l'investissement des parlementaires de rendre public, via ce même mécanisme de publication au Journal Officiel, la liste des personnes présentes lors de ces auditions. Une publication structurée de ces informations de manière systématique, en Open Data, serait également souhaitable.

Score : 19 / Réponses : 0

----

"les études d’impact accompagnant les projets de loi" J'ai expérimenté la contribution AN, et ne l'ai pas trouvé efficace, c'est une consultation de façade, mais tient-on compte des contributions? Non, m'a répondu très aimablement un député à ma contribution qui pensait comme moi sur la loi et a été obligé de se plier à "l'exécutif" c'est à dire le gouvernement (c'était dans le précédent quinquennat). 
Par ailleurs, je répète qu'on consulte trop souvent les experts d'un domaine et pas les citoyens qui eux, ont une expérience concrète de l'application ou la non application de la loi. Je pense en particulier à la Justice, j'ai entendu (chaîne LCP) la Garde des Sceaux faire des propositions très intéressantes sur la réforme de la Justice civile et pénale dans les six prochains mois  et mettre le "justiciable" au coeur de son projet. Mais justement peut-on s'assurer que le justiciable sera consulté suffisamment en amont?   Il est certain que les professionnels de la justice n'ont pas la même vision,  que celle du citoyen qui voit parfois sa vie ravagée par les lenteurs et l'archaïsme des procédures et du droit. Le mot "archaïsme" a été judicieusement prononcé par la Garde des Sceaux : il faut absolument associer les citoyens à l'élaboration de la réforme de la Justice.  Vu par les seuls magistrats, l'habitude de la lenteur et de la complexité de la Justice est devenue "normale", même s'ils en souffrent, et l'ont fait savoir les professionnels de la Justice semblent incapables de s'auto-réformer. Pourquoi le droit serait-il intouchable et inadaptable? Revue en amont avec les citoyens, la justice civile et pénale a de grandes chances d'en sortir plus claire et plus efficace...et surtout plus rapide !

Score : 17 / Réponses : 2

----

Le Comité En Marche 9e de Moselle a réfléchi sur ce sujet et souhaite mettre en avant deux propositions : 

1. L'assemblée national devrait mettre en ligne une version vulgarisé de la loi. Cette version pourra être évalué par des citoyens via des questions fermés 

2. Si il y a plus de 500000 personnes qui mettent en avant un effet pervers de la loi. Cela générerait une question dans la commission parlementaire concerné. Question qui devra être traité. 
 
Cordialement,

Score : 17 / Réponses : 6

----

* Contribution collective Café Citoyen : Présidée par Mme Nadia HAI Députée de la   11ème circonscription des Yvelines * 

**78-11-Q4 Rendre public les évaluations et mettre en place des indicateurs dès l'origine** 

Le CESE doit aussi participer à l'évaluation des Lois votées. Chacune des assemblées doit rendre ces évaluations.
La mise en œuvre des **lois doit être analysée, mesurée, disséquée par les organismes publics** (INSEE, OFCE, Pôle Emploi, INRS, Cours des Comptes... ) et être revue à date anniversaire (12 ou 24 mois). Si les **objectifs recherchés par la loi ne sont pas atteints, la loi** doit pouvoir être revue et améliorée, c'est à dire **modifiée, suspendue ou abrogée** le cas échéant.
Ces avis doivent être **disponibles au public** car réalisés par des organismes publics.(sous forme de versions pédagogiques et in extenso).
L'évaluation institutionnelle doit être **vulgarisée** afin d'être soumise à l'avis ou au vote  des citoyens via la plateforme numérique.
Définir entre député et citoyen un ou des objectifs pour chaque loi.
Lors de la promulgation d'une loi, une commission d'experts (y.c. des citoyens sachant ou impliqués) doit :

Sous un mois proposer un **cadre d'évaluation** : objectif (les 3 majeurs), cibles visées, périmètre d'évaluation, indicateurs chiffrés et mode de calcul, niveau à atteindre au bout du temps imparti.

Dès que possible, les **indicateurs seront mesurés** afin **d'enrayer** des possibles **dérives** au plus tôt.

A l'échéance fixée, les **objectifs seront atteints, en progrès mais pas atteints, ou en dégradation**. Ceci nécessitera des mesures correctives, des plans d'actions ou un abandon du projet.

Score : 17 / Réponses : 0

----

La mise en œuvre de lois incluant  des economies et/ ou des depenses devrait etre auditees regulierement. Ceci permettrait de verifier qu on arrive bien aux résultats attendus. Ces résultats pourront être consultable en ligne...

Score : 13 / Réponses : 0

----

On pourrait permettre aux citoyens de déposer sur une plateforme un avis, une question ou une demande à un ou plusieurs parlementaires. Ceux-ci seraient alors tenus de donner une réponse construite et publique (pourquoi pas sur la chaîne parlementaire par exemple). Les obligations de réponse ne concerneraient évidemment que ceux visés par les demandes les plus soutenues.

Score : 11 / Réponses : 1

----

L'évaluation des lois concerne plusieurs catégories de personnes:
- les députés qui l'ont votée avec un objectif en tête
- la population dans son ensemble
- les personnes qui travaillent dans le domaine impacté (santé, social, justice, sécurité, prisons, ...)
- autre ? ...
Chaque évaluation de la mise en oeuvre d'une loi, au-delà de juste savoir si les décrets sont bien sortis (voir fiche synthèse 52 de l'assemblée nationale), doit passer par une consultation impérative de ces différentes catégories de personnes et du traçage de ces contributions.

Score : 10 / Réponses : 0

----

Les dispositions finales des lois doivent fixer la procédure d'évaluation : à partir de quand et par qui

Score : 9 / Réponses : 0

----

La 1re question c’est de savoir de quels citoyens on parle. Une 1re étape c’est a minima de rentrer dans une logique d’évaluation « pluraliste  et participative » : pluraliste - elle prend en compte tous les points de vue; et participative - elle intègre ces points de vue pas seulement dans la remontée d’informations, mais également dans le pilotage de l’évaluation. C’est une approche qui existe depuis une trentaine d’année, y compris en France, et dont l’Assemblée peut s’inspirer pour au moins prendre en compte les vues des citoyens à travers des organisations qui veulent les représenter (associations, syndicats, collectifs, etc.).

Si maintenant l’ambition est d’intégrer directement les citoyens dans les évaluations, la question de fond est le degré d’ambition, du plus bas (information) au plus élevé (coconstruction) en passant par les stades intermédiaires de consultation et concertation.

Information : le 1er enjeu c’est que les citoyens ne sont pas au courant des évaluations qui sont menées et ne peuvent donc pas s’y intéresser ou y participer. Les Parlementaires ont un rôle fondamental à jouer pour non seulement faire connaître ces évaluations, mais aussi expliquer qu’il s’agit de juger du résultat de l’action publique en s’appuyant sur des données probantes. Pour que les citoyens s’investissent dans les évaluations menées par l’Assemblée, il faut donc que l’Assemblée accepte d’en être un étendard.

Consultation : Une façon simple d’intégrer les citoyens, c’est d’intégrer une démarche de consultation systématique ouverte à tous. C’est par exemple ce qu’a décidé la Commission européenne en 2014. Cependant, la consultation « sans filtre » est rarement très intéressante (beaucoup de « bruit »). En revanche, on peut organiser des consultations sur le territoire sur des formats variés, d’un simple panel formé sur le sujet traité (format « conférence citoyenne ») à des consultations très larges (cf. le million de participants pour la Commission Thélot sur l’école). Ce ne sont jamais de simples consultations ouvertes, mais des processus de formation, discussion, réflexion plus ou moins profonds, numériques ou face-à-face, garantissant une contribution pertinente. Les Parlementaires peuvent en être parties prenantes, sur le modèle des consultations « Debate Europe » de la Commission européenne (2005-2010). Tous les outils de collecte habituels en direction des parties prenantes (entretiens, ateliers, enquête par questionnaire, etc.) doivent également être mobilisés et sont un préalable souvent aux consultations de citoyens, ne serait-ce que pour poser les bonnes questions.

Concertation : Pour que les citoyens s’intéressent aux évaluations, peut-on faire en sorte qu’ils soient directement impliqués dans le choix des lois à évaluer, mais aussi dans la définition des questions et des critères de jugement ? Plus largement, il est possible d’organiser la transparence du processus évaluatif, depuis la constitution du référentiel jusqu’à la collecte des données et à l’élaboration du jugement et des recommandations. Etalab a montré la voie sur ce sujet depuis quelques années maintenant. Une transparence forte et assumée donne de la crédibilité à l’évaluation « qui n’a rien à cacher ». Former et accompagner un groupe de citoyens pour lui permettre d’être « armé » sur le processus évaluatif, en revanche, c’est lui permettre d’être vraiment proactif sur le sujet et d’imprimer sa marque dans l’évaluation, par exemple en étant capable de critiquer positivement ou négativement les preuves et les argumentaires avancés.

Coconstruction : la démarche la plus approfondie serait celle où des citoyens seraient directement impliqués dans l’évaluation, depuis le début et jusqu’à la fin. Sur le modèle des jurys citoyens, un groupe de citoyens pourrait être investi d’un rôle dans le pilotage de l’évaluation à chacune de ses étapes (en parallèle ou en remplacement. Cela nécessiterait que les Parlementaires jouent un rôle de « parrain » ou de « commanditaire » d’une évaluation confiée à des citoyens, dont l’Assemblée serait le garant.

Score : 9 / Réponses : 0

----

Il convient de modifier la Constitution afin de permettre aux citoyens de proposer l'abrogation d'une loi dont l'application ne répond pas à l'impact qui en était attendu ou dont les effets sont néfastes. De même, les lois contredisant d'autres lois en vigueur ou en décalage avec l'évolution des moeurs, devraient être abrogées selon le même principe.
Cette évaluation ex post de la loi par les citoyens devrait être systématisée.

Score : 8 / Réponses : 0

----

Suppression automatique de l'immunité parlementaire  pour tout acte, délit ou crime de droit commun.

Score : 8 / Réponses : 3

----

Il faudrait que le Parlement même incite les citoyens à donner leur avis. 
Aujourd'hui les Français se taisent de lassitude envers les politiciens qu'ils considèrent bien souvent comme trop éloignés de leur réalité. 
Le Parlement est souvent aperçu comme "éloigné" par les Français c'est pourquoi je pense que le Parlement (ses deux chambres) devrait être plus au contact des citoyens.  Ces citoyens sont nombreux à s'être écartés, il faut donc avoir des démarches de rencontre avec eux. Car un parlementaire est un représentant de la Nation mais aussi de leur circonscription. 
La politique doit donc être "réapprovisionnée" en citoyens qui ne font plus de démarches par eux - mêmes. Si les citoyens sont aujourd'hui de plus en plus inertes (malgré de fortes contestations des lois du gouvernement ) Il faut donc se rapprocher d'eux.

Score : 6 / Réponses : 8

----

Afin d'éviter tout biais dans l'évaluation (par exemple si elle était réalisée par un député qui a voté pour ou un député qui a voté contre), elle doit être menée par une autorité indépendante, avec des moyens suffisants (ce qui est un autre sujet).
Les rapports émis par cette autorité doivent être obligatoirement disponibles, diffusés et pris en compte lors d'une loi future.

Score : 6 / Réponses : 0

----

Les visas des lois doivent être écrits sous forme d'"attendus" ou "considérant..." comme les règlements communautaires afin de bien connaître et de comprendre les objectifs poursuivis ce qui facilitent le travail des contrôles a postériori

Score : 5 / Réponses : 0

----

Au vu des différents commentaires je pense qu'il faut créer une assemblée citoyenne qui a son mot à dire sans étiquette de parti.  Ces personnes doivent représenter ce qu'est la France d'aujourd'hui, retraité, actifs expert dans leur domaine et notre jeunesse.  Cette assemblée nouvelle et active permettra la consultation citoyenne tout en sachant que ces personnes doivent avoir l'objectif de représenter objectivement les citoyens qu'ils rencontrent au quotidien et dans la vraie vie.

Score : 5 / Réponses : 2

----

les exécutifs doivent s'intéresser au problèmes des gens qui son en souffrance et les résoudre en votant des lois dans l’intérêt général revoir par exemple le minimum vital en tenant compte le loyer et les dépenses nécessaire pour vivre sanctuarisé et insaisissable l'ordinateur  qui devient un bien nécessaire pour toute démarches administratif et bientôt les fiches de payes seront numérises le droit a l'information et a internet pour tous une mesure d’utilité publique se n'est pas une question d'argent c'est du bon sens arrêtez de faire souffrir les gens innover créer être audacieux pour endiguer les injustices

Score : 4 / Réponses : 5

----

Les lois doivent être écrites à l'indicatif et ne comporter que des dispositions prescriptives et opératoires.

Score : 3 / Réponses : 3

----

Il devrait être interdit de mettre en application une loi sans suivre son application sur le terrain  et pouvoir faire des modifications si nécessaire ( ne pas avoir peur de revenir rapidement en arrière )  . Je m'explique on décide qu'il faut appliquer une loi . Cela génère un formulaire administratif de plus . Il faut vérifier qu'il n'existe pas d'autres formulaires à éliminer , modifier pour toujours plus simplifier le quotidien. Si on s'aperçoit qu'il complexifie le quotidien , immédiatement on réalise  les changements nécessaires ( modification d'une autre loi , aide à la simplification des formulaires ..)
On fait la loi et on est responsable jusque dans son application

Score : 3 / Réponses : 0

----

PREVOIR DES CONTRÔLES EFFICACES DE L'APPLICATION DES LOIS

Un nombre croissant de lois voient le jour chaque année et de plus en plus celles-ci ne sont pas appliquées. 
Une des raisons essentielles est qu'il n'existe pas de contrôle correspondants ou de manière insuffisante, ce qui n'incite pas les citoyens, les professionnels à les respecter. 
Dès lors, la création de la loi ne sert à rien si elle n'est pas réellement appliquée. 
Des dispositifs de contrôles dissuasifs et permanents doivent être envisagés. Il convient de les accompagner de sanctions réelles en cas de non application de la loi.

Score : 3 / Réponses : 0

----

Contribution comité local LREM Les Essarts Le Roi, Le Perray en Yvelines, Levis Saint Nom : 
Nous avons évidemment pensé aux mêmes solutions que pour les consultations en amont des textes (questionnaires, réunion publiques) avec une exigence plus forte sur le document support de cette consultation qui devra émaner plutôt de la cour des comptes ou d’un organisme d’analyse et non pas du Ministère.
Une autre idée : la mise en place d’une assemblée de citoyen se réunissant 2 fois par an dans chacune des circonscriptions sur une ou 2 journées pour évaluer les lois à la demande de l’assemblée nationale. 
Ces assemblées seraient constituées de citoyens tirés au sort (en respectant un panel représentatif) sur les listes électorales dans la même logique que les jurés d’assise pour chaque assemblée (peu de possibilité de se faire exempter et indemnités versées par l’état) .
 La session couvrira l’évaluation de plusieurs lois  avec des commissions pour chacune d’elle. Préalablement chaque citoyen tiré au sort recevra le dossier réalisé par la cour des comptes ou autres organismes et pourra bénéficier d’une formation en ligne sur les grands principes de la loi (idem les microlearning ou MOOC en cours sur le site En-Marche) pour se familiariser avec le vocabulaire et les grandes notions. 
Une session pourrait être divisée en 
une partie de prise de connaissance du dossier (1/2 journée) 
une deuxième partie (séparée de quelques jours de la première) pour l'évaluation.

Toujours les mêmes exigences quel que soit le mode de consultation, la transparence et la diffusion des résultats. 

Généralement, Il faudrait veiller également à privilégier la qualité des lois sur la quantité, Et ne pas tomber dans l'excès normatif qui rendrait le fonctionnement des institutions lourd et incompréhensible. Un texte qui dure est un texte concis et clair. Ainsi la marge d'interprétation est laissée aux magistrats. Les consultations doivent permettre d'éclairer les électeurs et de susciter une meilleure compréhension donc une plus grande adhésion, Et également de recueillir les initiatives citoyennes pour faire du citoyen un véritable acteur. Cependant, elles n' enlèvent pas le travail du législateur qui doit prendre en compte l'objectif de mutation de la société afin de prendre des textes qui perdureront dans le temps et s’adapteront aux changements de la société.

Score : 3 / Réponses : 0

----

Un Président devrait être vu comme l'incarnation d'une vision de la France, que la fonction exécutive suprême permet de tester.
Les deux chambres parlementaires doivent pouvoir voter la destitution du Président en exercice. Comme aimait à le rappeler Charles de Gaulle, un Président, c'est avant tout une certaine idée de la France. Mais on ne peut "pivoter", pour filer la métaphore entrepreneuriale, que si on a une nouvelle idée. Le numérique permet à moindres coûts de renouveller cette consultation. Il faut faire confiance à la cryptographie (les banques, les terroristes le font bien) pour connaître le vote de tous les citoyens susceptibles d'être un jour député. A l'élection d'un nouveau Président, tous les députés en place n'ayant pas voté pour lui précédemment peuvent "dégager" aussi.

Il faut dédramatiser la destitution dans notre pays, et la voir comme de la flexi-sécurité, ou un pacs.

Les réformes de 2007 et de 2014 sur l'article de 2008 doivent permettre de destituer le Président comme on débauche. C'est je crois leur esprit profond.

Score : 2 / Réponses : 6

----

Pour participer à l'évaluation , il faut savoir quoi mesurer. Donc chaque loi devrait comporter les critères qui permettent de juger de son efficacité future, avec des périodes d'évaluation  et des moments de bilan. Les résultats diffusés et connus devrait permettre à chacun de se faire une opinion, ainsi dans ce système vertueux et transparent tous auraient intérêt s'impliquer dans l'action publique.

Score : 2 / Réponses : 0

----

Étant étudiante, je me rends sur le site de l’Assemblée Nationale pour des recherches dans le cadre d’un devoir d’Histoire. Je tombe alors, sur le fil d’actualité du site, sur cette consultation. Par curiosité, je clique sur le lien. Je parcours rapidement et je me souviens d’une idée que j’avais eu il y a quelques semaines mais qui avait été bloqué à un moment donné …et qui vient d’être complétée grâce aux questions que je viens de lire. Mon idée consistait simplement à créer une organisation qui permettrait à des étudiants de produire des évaluations et des rapports validés par leurs professeurs. Cela aurait pu entrer dans le cadre de la loi "Egalité et citoyenneté" qui implique que les universités valorisent l’engagement. Et puis je me suis dit que de toute façon personne ne lirait les rapports et donc que finalement ça ne servirait à rien …
Ma contribution à la question posée est donc la suivante.
Je pars de constats. En ce début d’année universitaire, dans le cadre de la loi « Egalité et citoyenneté » il est demandé aux universités de trouver un moyen de faire valoir l’engagement des étudiants dans leur parcours dans la mesure où il permet un apprentissage. L’apprentissage qu’il est possible d’en retirer est indiscutable mais je trouve qu’il y a un problème avec l’engagement étudiant. En effet, je suis engagée dans des associations étudiantes et le constat que j’en tire est le suivant : l’engagement étudiant est désengagé, c’est à dire qu’au tant qu’étudiant on s’engage mais au tant qu’être rationnel nous effectuons un calcul et souvent il s’avère que l’engagement associatif n’est pas « rentable » puisque nous sommes très pris par les études en terme de temps. Autrement dit, il est plus rentable en terme de résultat de réviser un cours plutôt que de s’occuper à organiser une maraude puisque le premier est reconnue et l’autre pas (pour le moment du moins). L’initiative du gouvernement est d’intégrer l’associatif à l’universitaire. Je pense qu’il serait intéressant (je pars du postulat que ça n’a pas été fait) d’envisager d’intégrer l’universitaire à l’associatif. 
Autre constat : les acteurs du champ de la recherche se plaignent fréquemment du manque de crédibilité publique accordé aux résultats de leur recherche. 
Encore un autre constat : il y a une nécessité dans notre démocratie d’améliorer la participation citoyenne et le Président de Parlement lance une consultation à ce sujet.
Dès lors, je propose la création d’un dispositif qui se déploirait dans chaque universités, écoles, etc qui consiste à mobiliser les étudiants dans l’évaluation d’une politique publique, quantitativement ET (j’insiste) qualitativement. Les modalités précises doivent être, bien entendu, affinées et surtout bien réfléchies puisque seront déterminantes sur le résultat. Cette évaluation sera validée par les enseignants-chercheurs. L’engagement des étudiants sera donc rentable car valorisée par la validation de crédits ou autre (soyons imaginatif !). Les données collectées ainsi constitueront les variables d’un indicateur composite disponible sur une plateforme nationale accessible aux parlementaires. Puisqu’effectué localement il sera même possible de comparer les évaluations entre les régions. Ce serait donc un outil à la disposition des parlementaires alimentés par les citoyens et vérifié scientifiquement. (On pourrait même imaginer une incitation économique dès lors qu’une variable serait entrée…)
Si vous avez fait attention, j’ai écrit « création d’un dispositif ». J’aurais pu dire « association ». Ce « dispositif » n’en est pas un puisqu’il a vocation à contribuer à la gouvernance. Je propose donc, au passage, la création d’une nouvelle forme juridique : les OSG : Organisations Sociales Gouvernementales car si nous voulons promouvoir la démocratie participatif il faut, en partie, se doter de l’arsenal juridique adapté. 
Ainsi, par la création d'un indice mon idée de départ prendrait tout son sens, puisque les rapports produits prendraient sens, et ce grâce au numérique !

Score : 2 / Réponses : 0

----

UNE SEANCE MENSUELLE DE QUESTIONS CITOYENNES AU GVT
Permettre aux citoyens de poser des questions au gouvernement sur une plateforme web. Chaque mois, les 3 questions qui recueillent le plus de votes sur la plateforme (au-dessus d’un certain seuil) sont posées en séance par leurs auteurs au Gouvernement dans l’hémicycle de l’Assemblée Nationale. Comme les actuelles ‘Questions au Gouvernement’, elles sont retransmises en direct sur la chaine parlementaire à la télévision. Cela donnerait une occasion à tous (et aux jeunes notamment) de venir au Parlement soit pour intervenir soit pour soutenir leurs amis. Et donnerait du Parlement une image plus ouverte et moins opaque.

Score : 2 / Réponses : 0

----

les Nations Unies travaillent depuis quelque temps à la mise à disposition d'outils permettant l'évaluation de l'efficacité des politiques en vue d'atteindre les objectifs (environnementaux) d'Aichi.. et à ma connaissance, l'évaluation de l'efficacité des politiques est vraiment à l'ordre du jour de bien des pays, en particulier dans un contexte de limitations des moyens financiers (à mon souvenir, rapport Magenta au Royaume Uni par exemple).. évaluer l'efficacité est un enjeu majeur qui n'est pas simple, il est important de travailler de concert vers la transparence, l'objectivité et la rigueur de telles évaluations.

Score : 2 / Réponses : 0

----

Demander au citoyen d'évaluer la politique du gouvernement qu'est-ce que cela signifie ? Peu importe l'outil au final qui sera mis en place, même si je pense que le numérique reste une solution plus qu'envisageable, il faut aussi questionner la légitimité de tout un chacun à répondre à ce questionnaire qui semble, tel que présenté, s'intéresser à ce que pense le citoyen parce que c'est lui le plus à même d'en juger ? Mais peut-on considérer qu'un citoyen qui n'est pas touché par une action politique peut juger de son efficacité ou de son bien fondé ? Je pense que l'évaluation de la politique du gouvernement doit passer pas la création de panels de citoyens touchés par la réforme dont il est question. Par exemple sur les APL, interroger ceux qui en bénéficient, ou pour les réformes de l'université, interroger les étudiants.

Score : 1 / Réponses : 5

----

Il est important de remettre les choses en place entre l'exécutif et le législatif. Il est donc urgent d'interdire à l'exécutif (le gouvernement) de créer des lois. Son travail est à l'application des lois, pas à la création. Aujourd'hui combien de lois ne sont pas appliqué car le gouvernement ne fais pas son travail et s'occupe de ce qu'il ne devrait pas faire. De même pour l'assemblée, nous avons élu nos parlementaires pour proposer des lois correspondant à nos attentes. Et actuellement il ne faut pas leurs travail de législateur car ils ne proposent aucune loi.

Score : 1 / Réponses : 3

----

1) Maîtriser la "stabilité législative" avec constance et encadrement pour évacuer les lois obsolètes ou non efficaces.
2) Communiquer autour : des objectifs / des résultats, en suivant un agenda avec des dates butoir. 
3) Revenir sur les objectifs afin d'évaluer, de prendre en compte les effets indésirables de la dite-loi.
4) La plateforme numérique sur laquelle les citoyens s'expriment doit être transparente : les citoyens doivent accepter d'être identifié afin d'éviter toute dérive. Elle ne doit pas non plus avoir valeur de référendum.

Score : 1 / Réponses : 0

----

A l'instar des jurés d'assises, avant l'adoption d'une loi permettre à un groupe de citoyens sur la base d'un tirage au sort issu des listes électorales ou sur la base du volontariat de "challenger" le parlementaire à l'initiative de la dite loi. Le but étant de valider sa bonne compréhension et son équité par un échantillon de personnes représentatif de la Société. L'atelier de travail pourrait se faire par visio conference via un lien sécurisé mis en place par l'Assemblée Nationale. En amont, mise en ligne du texte de loi sur la plateforme et permettre au citoyen tiré au sort ou volontaire de poser ses questions.

Score : 1 / Réponses : 0

----

Une commission d’évaluation composée de citoyens tirés au sort.

Cette proposition s’inspire de la désignation des jurés de cours d’assises (lieu le plus démocratique que soit dans une vie de citoyens).

Ces citoyens désignés pourraient siéger dans cette commission aux cotés de parlementaires et de fonctionnaires expert sur les thématiques évaluées.
Il est envisageable de transposer le mode de désignation des jurés d’assises (à l’exception du caractère contraignant et obligatoire) en remplissant certaines conditions telles que la nationalité française ou de ne pas se trouver dans un cas d’incapacité ou d’incompatibilité avec la fonction de membre de la commission d’évaluation (des personnes condamnées pour crime…).
En outre, un régime d’indemnisation compensatrice des membres de la commission peut garantir la présence de salariés à ces commissions complété par un régime de protection (aucun salarié ne peut être sanctionné ou faire l’objet d’une mesure discriminatoire en raison de l’exercice de la fonction de membre de la commission d’évaluation).

Une expérimentation sur un département permettrait de garantir la faisabilité de cette proposition. Cette commission serait ainsi présidée par le préfet de département et composée d’1 ou 2 députés désignés par le président de l’assemblée nationale, des citoyens sur liste électorale tirés aux sorts, d’associations… (liste non exhaustive)

Score : 1 / Réponses : 0

----

Ouvrir les processus d’élaboration des études d’impact et rapports d’évaluation des politiques publiques grâce au numérique

Les outils numériques peuvent être utilisés en amont des politiques publiques, pour mieux intégrer les acteurs de la société civile dans la réalisation des études d’impact et en aval, lors de l’élaboration des rapports d’évaluation. Cette participation permettrait notamment de renforcer l'efficience de l’action publique en intégrant des contreexpertises externes pour l’évaluation des politiques publiques. Pour mieux prendre en compte ces expertises extérieures, il serait conseillé de :
> Mettre en ligne le calendrier des études d’impact et des rapports d’évaluation de manière anticipée ;
> Ouvrir l’écriture des études d’impact et rapports d’évaluation à des contributions extérieures. Sur une période donnée, des contributeurs extérieurs pourraient commenter, amender, compléter les études sur le principe du contradictoire ;

Score : 1 / Réponses : 0

----

Les 3 propositions qui ont émergé suite à l'atelier de contribution du 3 novembre 2017 à Paris : 

1 - Permettre une participation citoyenne en appui aux rapports d’évaluation de loi au bout de 3 ans, rendus obligatoires, en vertu de l'article 145-7 du règlement de l'AN
2 - Définir de manière claire des objectifs mesurables et des critères d’évaluation de l’efficacité et d’impact. Pourquoi pas en faire un standard à appliquer et intégrer aux études d'impacts qui sont réalisées en amont.
3 - Impliquer les citoyens ou entreprises innovantes dans l'expérimentation de l'analyse de données permettant d'objectiver l'évaluation.

Score : 1 / Réponses : 0

----

A la question « Comment intégrer les citoyens »  en ce qui concerne l’évaluation et le contrôle de la mise en œuvre des lois,  voici le résultat de nos réflexions :
En amont l’Assemblée Nationale devra communiquer sur cette ouverture aux citoyens (par voies de presse, son site, LCP…)
Modalités
1 - le citoyen volontaire s’inscrit sur le site de l’assemblée dédié (en préambule il signe une charte de probité)
2 – il communique avec l’AN par voie informatique (plateforme d’échange similaire à celle mise en place pour la consultation actuelle)
3 – l’AN envoie systématiquement les thèmes sur lesquels le citoyen peut apporter sa contribution
4 – le citoyen y répond ou pas selon l’intérêt qu’il porte à la thématique
5 – dans tous les cas l’AN continue à le tenir informé de l’avancement des travaux par voie informatique
6 – si le citoyen choisit de travailler sur  un sujet il peut :
a)	Simplement transmettre ses retours à l’AN sans avoir à se déplacer
b)	S’il souhaite participer à des réunions « en personne » il s’inscrit sur une 2nd liste où il sera tiré au sort
c)	Ce tirage au sort pourrait être graduel :
-	1er temps : 1 citoyen par région
-	2ème temps : (afin d’éviter 1 surnombre entrainant des frais conséquents) tri parmi les citoyens présélectionnés pour arriver à 5/6 personnes maximum.
-	Ce tirage au sort serait valable pour la durée de la thématique. Il serait assorti d’un planning
7 – Comment traiter les retours des citoyens ?
a)	Par le biais d’une Intelligence Artificielle capable de regrouper les réflexions identiques et de faire le tri
b)	Grâce à des personnels dédiés aptes à faire la même chose que l’IA mais qui « ressentent mieux » les choses
c)	Dans tous les cas il faudra « in fine » des personnes pour affiner ces retours, en faire une synthèse et la rapporter à l’Assemblée et aux citoyens inscrits ou pas par tous les moyens possibles
8 – Quels thèmes ?
	Un  citoyen inscrit pourrait proposer un thème et s’il est également évoqué par d’autres citoyens (nécessité de définir un nombre) l’Assemblée pourrait le mettre à l’ordre du jour.

Score : 1 / Réponses : 1

----

Faire des panels départementaux ou régionaux de citoyens qui évalueront l'application et les implications de certaines lois au quotidien pour les citoyens dont ils sont le plus proches. "La loi doit être pour tous" est un principe qui créé parfois plus d'inégalité que d'égalité si on ne fait pas attention à certaines particularités territoriales.

Score : 1 / Réponses : 0

----

Bonjour
Etablir un plan stratégique est somme toute très simple. Quelques chiffres, quelques beaux graphiques ou belles images pour illustrer; le tout envelopper dans de belles paroles et le tour est jouer. 
Pour ce qui est du suivi, nous parlons de gestion , d'évaluation. 
Cela requiert en plus des points évoqués ci dessus, une série d'indicateurs (qualitatifs et quantitatifs) reconnus par les acteurs et observateurs du plan, un calendrier, un cadre et des responsabilités clairement attribuées et acceptées .
L'accès à un tableau de bord doit être intégré dans la définition même de ce projet comme l'évoque Garlan Nizon dans les commentaires.
LE HATVP doit garantir une vraie transparence sur les composantes du projet  / loi et sur son suivi .
Dans le cadre du suivi et de l'évaluation des lois, exclure les députés n'ayant pas remplis 
* les obligations de déclarations (13 à ce jour dont les noms ne sont publiés)  
* n'ayant pas siégé un nombre de fois minimum dans l'hémicycle  

Enfin , en lisant les commentaires déjà postés et faisant référence aux autres pays, pourquoi la France ne pourrait elle pas pour une fois, reprendre et adapter ce qui fonctionne dans d'autres communautés ? Je ne vois pas le bénéfice de réinventer la roue. Ce serait peut être la première évolution à apporter. Moins d'égo, plus de pragmatisme.

Score : 1 / Réponses : 0

----

Afin de faciliter le travail d'évaluation  d'une  loi par les citoyens, s'il devait n'y avoir qu'une seule mesure à prendre immédiatement, ce serait de mettre en tête de chaque loi un texte de vulgarisation de cette loi, en présentant les raisons de cette disposition, et ce qu'on en attend sur le terrain.

Score : 1 / Réponses : 0

----

Observatoire citoyen

Evaluation de la loi

Devant la prolifération des lois l’accès à celles-ci devient difficile lors de la consultation des textes. La plupart renvoient à des décrets qui pour certains ont été publiés depuis de très nombreuses années. La bonne compréhension de ces textes devient inaccessible pour le commun des mortels. C’est donc au détriment de l’acceptation de l’application de la loi.

Nous proposons que toutes les nouvelles lois :
-	remplacent donc fusionnent tous les textes précédents traitant du même sujet.
-	soient présentées sur internet sur un site de l’assemblée nationale dédié en accès libre avant sa mise en application.
-	mises en application sur une durée à définir.
-	Evaluées par des acteurs indépendants de l’exécutif.
-	Promulguées après modification prenant en compte les résultats de l’évaluation si nécessaire.

Les acteurs indépendants de l’exécutif pourraient être des associations composées d’un panel de personnes représentatives de la société.

L’évaluation de la loi pourrait se faire en toute objectivité après écoute de tous les acteurs :
-	Magistrats
-	Services chargés de l’application
-	Personnes soumises à l’application

Enfin il serait souhaitable que toute nouvelle loi soit étudiée et élaborée dans un contexte national serein en dehors de toutes pressions individuelles ou publiques.

Score : 1 / Réponses : 0

----

Il faudrait surtout qu'avant de légiférer, on évalue l'impact des lois précédentes votées sur le même sujet. Cette évaluation pourrait être menée par des commissions parlementaires, auditionnant tous les acteurs concernés (en particulier les associations, et les administrations qui les ont appliquées). Les rapports d'évaluation parlementaire devraient être rendus publics et facilement consultables. Ils devraient servir de base aux consultations en amont des nouveaux projets de loi.

Score : 1 / Réponses : 0

----

Je suis d'accord pour la suppression de l'article 27 de la constitution!

Score : 1 / Réponses : 0

----

Les recours au Conseil Constitutionnel doivent être totalement transparents et les décisions du Conseil Constitutionnel argumentées, motivées et citant les personnes ou organismes ou société qui ont déposé un mémoire pour contester la constitutionnalité d'une loi. Les décisions de fin 2016 sur l'imposition des multinationales votée par le parlement mais annulés par le Conseil Constitutionnel est incompréhensible pour les citoyens contribuables qui ne savent qui a fait ce recours ni ses motivations détaillées. Dans les autres décisions de justice, cours de cassation, cours d'appel, les parties demandeur et demanderesse sont citées ainsi que les motivations des uns et des autres et celles retenues pour la décision.

Score : 1 / Réponses : 0

----

Vous voulez plus de participation citoyenne commencer par faire de la pédagogie en expliquant le chemin d'une loi.
La majorité des citoyens ne savent pas quel est le tenant et quel est l'aboutissant du trajet d'une proposition du loi.

Score : 1 / Réponses : 0

----

Constat : 
La présence des citoyens dans le débat démocratique ne peut qu'être vivifiante dans un environnement politique qui se laisse facilement enfermer dans la gestion de problémes techniques ou des échéances électorales. Néanmoins, l'intervention individuelle du citoyen ou l'organisation spontanée des citoyens peuvent présenter des écueils : la méconnaissance du fonctionnement des institutions, des difficultés à comprendre le langage juridique, la méconnaissance des données macro, l'absence d'une vision globale de la société, un activisme de certains acteurs de la société civile qui emploient les mêmes méthodes de lobbying que des grands groupes, résultant en la confiscation de la parole des citoyens moins habitués à la prise de parole en public, une sur-représentation de certaines thématiques dans le débat publique, ...
Proposition : 
Distinguer deux types de débats démocratiques avec les citoyens : (1) le débat démocratique participatif et (2) le débat démocratique de négociation. Ce dernier se justifie (1) par le fait que les débats participatifs n'engagent pas les citoyens dans la faisabilité de leurs propositions; aussi, les propositions sont souvent théoriques et les discussions, chronophages et frustrantes, et (2) parce que dans les faits, le rôle uniquement participatif des citoyens présente peu d'enjeu dans les débats; aussi les acteurs de la mise en oeuvre des solutions (élus, sachants, opérateurs économiques,...) entrent peu en interaction avec eux pour résoudre les problèmes qu'ils soulèvent.
L'espace démocratique participatif est réservé à l'expression citoyenne spontanée sur une plateforme numérique nationale (i) qu'elle se manifeste individuellement ou (ii) qu'elle soit collective par le biais des pétitions et par le biais de conclusions issues des "café-débats citoyens". La plate-forme nationale peut être du type "Telabotanica" ou "Doctissimo". Elle doit cependant être animée  localement par les services déconcentrés de l'état et des réseaux de la société civile.
L'espace démocratique de négociation, nécessite l'intermédiation des collectivités et des institutions pour son organisation et son animation. 
Pour que la  présence des citoyens dans le débat soit opérante, il faut que leur nombre soit égal au  nombre d'élus dans les collectivités. Le corps des citoyens doit être composé par exemple de 2/3 de citoyens tirés au sort et de 1/3 de citoyens/société civile cooptés pour leurs expertises techniques.
Les citoyens participent aux travaux des collectivités (Communes, EPCI, Régions) en portant leur attention sur la pertinence et l'applicabilité des lois qui s'imposent aux collectivités et leurs administrés. Leur rôle est de soulever les problèmes, de comprendre les conditions nécessaires à leur résolution (cadre juridique, financier, ordre publique, ...) et de participer à la négociation qui permettra de résoudre les conflits d'intérêts. Si la partie négociation, en l'état du droit, se fait dans les collectivités, les propositions d'évolution des lois doivent se faire collectivement, c'est à dire avec l'ensemble du corps des citoyens, à l'échelle de la région.
Formation et information des citoyens : Il faudra préalablement des formations et des lieux dédiés à cette fin : les locaux vacants suite à la fusion des régions peuvent être mis à disposition pour des sessions de formations MOOC (hebdomadaires ou bi-mensuelles), (2) des ateliers d'élaboration de la loi en lien avec l'Assemblée Nationale et (3) des plennières avec les élus des différentes collectivités  et les députés (mensuelles ou trimestrielles).
L'animation de ces sessions de travail doit être dévolue au CESER rénové (déclinaisons régionale du CNESE) dès lors que le CESER joue son rôle de force de proposition et non de force politique.
De manière général tous les citoyens doivent avoir accès au big data de la collectivité qui les intéresse. Ce ne sera pas une duplication de l'Insee, mais davatage une ressource qualitative ou les indicateurs sont co-construits avec les citoyens. L'Open data est accessible par une simple application smartphone

Score : 1 / Réponses : 0

----

une constatation  : trop souvent les lois sont diluées par leurs décrets d'application et par les différents règlements administratifs et normes en vigueur, Il s'agirai de faire "le ménage" pour enlever contradiction, redondances et doublons.. 
tout ceci est un obstacle à la mise en applications des lois

Score : 1 / Réponses : 0

----

Consultation citoyenne (20 personnes) au Lion d'Angers

- Créer une base de données avec les informations de base à connaître pour comprendre la loi.
- Lors de l'écriture de la loi, définir des objectifs, critères d'évaluation, temps de mise en oeuvre et mesures des impacts.
- Avoir une plateforme dédiée à l'évaluation en lien avec LégiFrance
- Evaluer l'efficacité d'une loi en veillant à consulter des acteurs variés (nationaux, locaux, élus, syndicats, citoyens...) en adaptant les supports (réunions publiques, réunions privées, consultation numérique...).
- Définir un délai, un calendrier pour l'évaluation de l'efficacité d'une la loi.
- En lien avec la plate-forme, proposer des modèles de diagnostics et d'évaluation prêts à l'emploi et adapter aux différents publics susceptibles de participer.
- Prévoir un espace ou une procédure permettant de faire remonter plus rapidement les initiatives, expériences locales efficaces pour les reproduire.

Score : 1 / Réponses : 0

----

Prendre en compte l'évaluation faite par les services de l'Etat ou des Collectivités Locale, évaluation chiffrée ou non, qui est obligatoirement faite par les fonctionnaires depuis très longtemps et dont personne ne se soucie. Ce sont les fonctionnaires qui sont le mieux à même de vérifier sur le terrain 1° que la loi est respectée et 2° qu'elle est efficace ou non. Leur avis peut même être pris en compte avant l'élaboration des lois. Le parlement s'il n'a pas déjà accès à ces informations devrait être informé et ces informations mises sur une plateforme seraient accessibles aux citoyens, pouvant à leur tour mettre leur avis sur la loi. Des délais importants seraient mis à disposition de chacun car l'évaluation d'une loi ne peut se faire en quelques mois.

Score : 1 / Réponses : 0

----

Bonjour,
Trop de textes sont incompréhensibles, s’entremêlent, donc difficilement applicables, à se demander si une lecture finale est réalisée ou si cela permet une interprétation dans un sens ou un autre, donc texte augmentant la fréquentation des tribunaux déjà fort chargé etc .

Score : 1 / Réponses : 0

----

PREVOIR DES SANCTIONS EN CAS DE NON-RESPECT DES LOIS
- Un nombre croissant de lois voient le jour chaque année et de plus en plus celles-ci ne sont pas appliquées.
- Une des raisons essentielles est qu’il n’existe pas de sanctions correspondantes en cas d'inapplication. 
- Des contrôles renforcés effectifs devraient intervenir en parallèle.

En cas de Non Respect de la loi, il conviendrait de prévoir le versement d'amendes ou des travaux d'intérêt généraux pour TOUS LES CITOYENS quel que soit leur situation économique, fiscale, etc. 

- Ces sommes permettraient:  
1) de convaincre toutes les personnes sur le territoire français de respecter la loi
2) de permettre l'égalité de tous devant l'application de la loi
3) de faire cesser le sentiment d'impunité qui se développe et qui accroît considérablement le non respect de la loi
4) de financer les contrôles avec les sommes recueillies par les amendes
5) de financer de nouveaux projets de l'Etat au profit des citoyens avec l'argent recueilli par les amendes ou du moins de limiter la dette.

Score : 1 / Réponses : 0

----

Ameliorer la lisibilite des lois,decrets,y compris pour les maires et autres representants du peuple

Score : 1 / Réponses : 0

----

Dans le cadre de la modernisation de l’Assemblée nationale, il convient d’ouvrir à chaque citoyen(ne) le droit de s’exprimer ou de donner son avis sur des propositions et des projets de loi relatifs au budget de la nation, au budget de la sécurité sociale mais aussi aux questions sociales, à la formation professionnelle, à la politique de l’environnement, du logement, aux grands projets d’urbanisme, d’infrastructures  (projets de barrages, d’aéroports, d’autoroutes …). 
Dans une société en crise où les citoyens(es) rejettent de plus en plus les politiques et les décisions gouvernementales, il faut réinventer la politique et proposer aux citoyens(es) des droits nouveaux afin qu’ils puissent être  des acteurs à part entière des décisions politiques et qu’ils puissent disposer d’un pouvoir de contrôle quant à la mise en œuvre des décisions. 
La démocratie active doit se substituer à la démocratie formelle actuelle. C’est la raison pour laquelle il faut permettre aux citoyens(es) d’être acteurs dans le processus de l’élaboration de la loi. La mise en ligne des avant-projets de loi constitue un lien démocratique indispensable à la construction de la démocratie active. 
Cette nouvelle démocratie ou démocratie athénienne du numérique peut redonner aux citoyens le goût de la chose publique à condition que les décisions, lois et règlementations soient élaborées  avec eux d’amont en aval au sein de l’Assemblée nationale. 
Une plateforme numérique devrait être créée au sein de chaque commission parlementaire permanente permettant aux citoyens (es) de déposer des amendements examinés dans le cadre de la préparation du débat  législatif en séance publique. Le règlement intérieur des commissions permanentes devra prévoir l’organisation du dépôt des amendements des citoyens selon un calendrier affiché sur cette plateforme en ligne. Les citoyens seront sélectionnés par le bureau des commissions. Selon un système de quotas, les auteurs pourront défendre leurs amendements en direct en ligne.

Score : 1 / Réponses : 0

----

-	Réceptacle obligatoire : le numérique
-	Évaluation : éléments de mesures à inclure dans les décrets et/ou circulaires de mise en application (Ne peut être indiqué dans la loi, qui elle, donne le cadre).
-	Création de questionnaires formalisés (évolutifs modifiables et perfectibles dans le temps)
-	Présentation linéaire de la loi
-	Lecture facilitée de la loi

Contribution collective du comité LREM Rennes Centre Sud

Score : 1 / Réponses : 0

----

Chaque texte voté devrait inclure son processus d’évaluation avec critères et indicateurs, délais définis et validés
Confier l’évaluation exclusivement à ou aux oppositions, au motif de ne pas être juge et partie, me semble être un risque de non objectivité , constituer un groupe «  mixte » majorité / oppositions d’évaluateurs me semble préférable pour garantir la qualité de l’évaluation.

Score : 1 / Réponses : 0

----

Contribution effectuée par un groupe de citoyens de la 13e circonscription des Hauts-de-Seine ( Antony, Bourg-la-Reine-Châtenay-Malabry, Sceaux)

1)	 Qu’est-ce que l’évaluation de la mise en œuvre des lois ? 
Selon nous, c’est la possibilité d’apprécier les résultats d’une loi plusieurs années après son adoption. Cette évaluation est réalisée par les députés trois ans après le décret d'application. A quel moment les citoyens peuvent-ils intervenir en appui et/ou en validation de l'évaluation effectuée par les élus ? 

2)	 juridiquement est-ce qu’il y a des outils existants permettant d’apprécier les résultats d’une loi ?
Est-ce que le contrôle et l’évaluation d’une loi sont deux actions distinctes ou pas ? Suivant l’opinion du groupe de travail, il est nécessaire, sous réserve, de définir comment une loi est évaluée. Cette disposition doit être le pendant de la concertation préalable. Exemple avec la loi d’orientation sur les transports dont la consultation se déroule jusqu’au 15 janvier.

3)	 Quelles sont les caractéristiques de l’évaluation de la loi ?  

Idéalement l’évaluation doit être à la fois qualifiée, d’où la (nécessité de faire participer des experts). L’évaluation doit mais également quantifiée. D’où l’importance d’avoir recours à des outils statistiques.
Par ailleurs l’évaluation doit-être indépendante. La question sous-jacente est donc de savoir qui la gère ? Selon nous, la gestion de l’évaluation doit se faire par des organes indépendants de pourvoyeurs des lois et/ou travaillant en parallèle avec les députés chargés de l'évaluer

4)	Faut-il avoir une évaluation systématisée ?
 Selon nous, une évaluation citoyenne pour les questions de la vie quotidienne est nécessaire, plutôt sous une forme d'accès au travail des députés, idée du "regard citoyen" de l'évaluation des députés. Une évaluation de l’évaluation ?

5)	Quel sont les enjeux de l’évaluation de la loi ?  
L’enjeu principal est de donner un plus grand pouvoir aux utilisateurs de la loi : les citoyens. Par exemple, les professionnels qui sont souvent aux premières loges, mais également les différents acteurs publics, les associations territoriales, etc. 
Un autre enjeu est donc de donner à l’évaluation « une vraie vie ».

6)	La place des outils dans l’évaluation ?

Les outils sont essentiels pour que le processus d’évaluation de loi soit optimal. Cela peut se faire à travers des réunions, des baromètres, des outils numériques comme les sondages, etc.
Et aussi en posant des questions à un groupe de citoyens observateurs qui seraient régulièrement interrogés dans un domaine.
A noter que la mise en place et l’utilisation de ces outils à un coût. Mais il est possible d’avoir un « retour sur investissement » par la pertinence des jugements émis et des recommandations nécessaires pour valider une loi ou la faire évoluer. 

7)	Comment contrôler une évaluation ? 

Voilà une question épineuse. Car elle sous-entend que le contrôle d’une évaluation doit être rendue public et que les citoyens puissent participer au contrôle au même titre que les acteurs publics. Et pourtant on ne demanderait pas à une personne de contrôler des éléments techniques d’un métier qu’elle ne connaît pas. La concertation citoyenne est donc une solution permettant aux citoyens d’avoir une part à jouer dans l’évaluation de la loi, tout en laissant les experts et les députés faire correctement leur travail.   

8)	L’évaluation à l’échelle locale ?

Une plus grande participation des citoyens peut cependant se faire à l’échelle locale.  Si on prend par exemple la réforme des rythmes scolaires en se donnant un peu plus de temps, peut-être qu’une plus grande concertation des principales personnes concernées, c’est à dire les professeurs et les parents pourraient apporter des résultats intéressants. 

Reste que dans la majorité des domaines, le contrôle de l’évaluation de la loi par les citoyens ne s’y prête pas aisément et que cette mission est dévolue institutionnellement aux députés qui sont des élus de la nation, ce que ne sont pas les citoyens dont la légitimité est réelle mais d'une n

Score : 1 / Réponses : 0

----

Le premier travail et peut-être le plus difficile me semble la définition des indicateurs d’évaluation pour chaque loi. 

En fonction des sujets, les citoyens à même de pouvoir remonter des informations ne seront pas du tout les mêmes. Prenons un exemple de sujet lié à une politique de santé, il faudrait avoir des remontées via :
- Des professionnels de santé (pas nécessairement rattachés à une organisation professionnelle et/ou syndicale),
- Des patients,
- Des proches de patients,
- Des données structurées, par exemple des statistiques.

La remontée d’information peut donc passer par : 
- Les élus impliqués dans la mise en oeuvre des politiques,
- Les professionnels impliqués dans la mise en oeuvre des politiques,
- Les citoyens concernés de près ou de loin par les politiques mises en oeuvre.

Elle peut s’organiser via :
- Les élus,
- Les organisations professionnelles et/ou syndicales,
- Des sondages, des enquêtes, une application mobile spécifique auprès des citoyens concernés,
- La captation de données.

Score : 1 / Réponses : 0

----

Pour faciliter le contrôle et l'évaluation des politiques publiques, pourquoi ne pas se baser sur ce qui marche déjà? Le suivi de la procédure législative existe déjà via les sites du Sénat et de l'AN, il suffirait peut-¨être de centraliser ces suivis et le fléchage des textes sur un espace commun aux deux chambres.

Idées:
- Permettre aux citoyens, via une pétition en ligne sur le site de l'AN, de demander l'évaluation d'une politique publique (si le seuil fixé de signataires est atteint, l'Assemblée chargera une "commission parlementaire spéciale" pour faire le bilan et l'évaluation de la politique publique visée).

- Permettre aux citoyens, toujours sur la base d'une pétition en ligne sur le site de l'AN, de proposer une enquête parlementaire lorsqu'un événement alerte ou interpelle l'opinion publique. Au delà du seuil fixé ( exemple: 1 million de signataires), une commission parlementaire sera chargé de l'enquête.

- L'Assemblée Nationale pourrait lancer régulièrement des "enquêtes de satisfaction"  ou des "questionnaires citoyens" en relation avec certaines lois votées ou des projets/propositions de loi discutées. Des avis qui pourraient servir l'institution pour évaluer la perception des citoyens vis-à-vis des politiques publiques menées ou effectuer une "enquête d'opinion" sur certains sujets (connaissances de la loi votées qui permettrait de cibler les aspects de la loi qui nécessitent une meilleure communication/explication; attentes/satisfactions des citoyens etc...). En d'autres termes, des outils qui serviront à la fois aux citoyens pour se sentir impliqué dans le processus législatif mais aussi aux parlementaires et au gouvernement qui pourront ainsi utiliser ces outils pour améliorer leur communication/sensibilisation autour des textes discutés ou des politiques publiques initiées.
Des outils pouvant être déployés au sein des chambres parlementaires ou décentralisés en circonscription, au bénéfice de chaque député.

Score : 1 / Réponses : 0

----

L’approche mixte de l’évaluation
Je rapprocherais les problématique d’évaluation des approches sur la qualité de service : en amont, en cours, a posteriori, centralisée, décentralisée, évaluation, notation, régulation.
De nombreux services privés basés sur la multitude (covoiturage, hôtellerie) ont une approche de la qualité basée sur une notation a posteriori et décentralisée. La qualité n’est pas définie en amont ou garantie de manière uniforme par un ensemble de procédures et/ou de critères et de normes. Elle est définie en cours et/ou en fin de chaîne et par l’utilisateur du service qui va définir une note (en chiffres, en étoiles…).
La fiabilité de l’évaluation découle dans ses systèmes, de la multitude qui en est à l’origine. C’est non seulement la note mais aussi sa pondération (nombre de micro-notes à l’origine de cette moyenne) qui régulent théoriquement la qualité et/ou plutôt qui mettent en évidence la non-qualité.

Certains services publics sont conçus avec une approche qualité mais ne sont pas (assez) évalués a posteriori : médecine scolaire par exemple. Il leur manque donc quelque chose d’a posteriori et de décentralisé et de plus systématique.

Le modèle essentiellement quantitatif de notation par l’usager a toutefois ses limites. Elles sont apparues assez rapidement avec trois phénomènes principaux :
- L’achat de notes positives, de followers, de fans de pages Facebook… 
- L’impossibilité de mettre une seule note pertinente pour un service évalué selon plusieurs facettes. Pour les hôtels par exemple, on nous demande maintenant souvent de noter l’enregistrement, puis la chambre. Le restaurant a aussi son propre système de notation).
- L’absence de professionnels non seulement de l’évaluation mais aussi du domaine évalué.

Le domaine de la restauration offre un exemple original et intéressant de coexistence de deux systèmes d’évaluation :
- Amont, très organisé, basé sur des critères précis et une communication centralisée : le Guide Michelin, les guides gourmands, les guides spécifiques : bio, végétarien, sans gluten…
- Aval et décentralisé avec un système de notes via des sites à forte audience comme Tripadvisor, la Fourchette…

J’aurais donc tendance à recommande des approches mixtes de ce genre pour les politiques publiques :
- Un ensemble d’objectifs qualité définis en amont,
- De nombreux recueil de micro-évaluations en cours de réalisation et a posteriori.

Score : 1 / Réponses : 0

----

Dans les messages abordant l'évaluation, une tendance à la généralisation est fréquente. Une partie du domaine à évaluer ne nécessite pas la même attention puisque celui-ci est très vaste : la loi de finances annuelle (et ses révisions), la loi de santé publique annuelle sans oublier tous les projets de loi inscrits dans la session parlementaire.
Parmi les projets de loi, certains modifient le code du travail, le code de la santé, le code civil, le code pénal... Pour évaluer, il est préférable de connaître le domaine. La formation de quelques heures mentionnée dans un message me parait insuffisante sur de tels sujets. On pourrait penser à choisir des membres d'associations de malades ou de proches de malades pour la santé, de syndicalistes pour le travail, mais il sera difficile de trouver suffisamment de bénévoles pour les 99 codes du droit national. 

Un autre aspect consiste à évaluer les services publics. L’Education Nationale publie régulièrement des résultats. Il existe également les rapports PISA qui permettent de comprendre qu’il existe une grande disparité de résultats à l’Education Nationale. En complément à la proposition d’assemblées, je propose que des publications complémentaires de résultats d’accompagnement, de formation, de soins…soient préparées par des personnes choisies parmi les membres d’associations sensibles à la fragilité (élèves handicapés, personnes âgées en EHPAD, personnes atteintes de troubles psychiques, adultes handicapés….). L'objectif serait d'éviter la trop grande disparité que l'on peut découvrir entre certains services d'un domaine donné (Education Nationale, EHPAD, adultes handicapés, troubles psychiques ...).

Score : 1 / Réponses : 0

----

La remontée d'informations devrait en effet pouvoir passer par le numérique. Néanmoins, pour éviter d'avoir à connaître des difficultés très spécifiques à chacun, rencontrées dans l'application des lois, il pourrait être fait appel, selon le domaine, aux associations représentatives (d'agriculteurs, d'éleveurs, humanitaires, sportives, etc.), lesquelles pourraient s'exprimer par le biais d'une plate-forme numérique mise en place à cet effet.
Des accès seraient fournis au représentant de l'association concernée, lui permettant, après identification, de consulter les éléments mis à la disposition des parlementaires dans le cadre de leur mission d'évaluation des politiques publiques et de formuler des observations ou des doléances qui seraient examinées, après étude par le service compétent de l'Assemblée, lors des séances des commissions.
Vive la France, vive la liberté d'expression!

Score : 0 / Réponses : 1

----

comme tous citoyens  . nous devrions avoir la possibil5 de vérifier la bonne application des lois votees

Score : 0 / Réponses : 0

----

A l'heure où la confiance que l'on plaçait auparavant en nos élus diminue de plus en plus, il est important de tenter de redonner une importance à ces derniers (en l'occurrence, les parlementaires), tout en y associant les citoyens.

A ce titre, il serait opportun, par voie numérique, que les citoyens puissent décider sur quelle loi portera la prochaine mission d'information, qui se déroulerait ensuite comme maintenant. 

L'objet de la mission devra ainsi recueillir un nombre de votes déterminé des citoyens sur la plateforme numérique pour lier effectivement le Parlement (je serais d'avis que toutes les propositions développées ici soient applicables, et à l'Assemblée Nationale, et au Sénat !). 

Peut-être prévoir également la possibilité désigner au sein des citoyens-votants, un représentant qui accompagnera les deux députés en mission d'information et qui participera à la rédaction du rapport.

Score : 0 / Réponses : 0

----

Evaluation des politiques: A partir de programmes et d'objectifs clairement définis au préalable, nous devrions avoir le droit en tant que citoyennes et citoyens de révoquer les élu.e.s. en cours de mandat (modalités du recours citoyen à définir par les élu.e.s).

Score : 0 / Réponses : 0

----

Il serait utile que chaque politique publique soit explicitée dans un style "accessible".
Du style (par exemple) "Politique de la culture>Politique muséale>Politique concernant tel ou tel thème" et "Politique de la culture>Politique concernant les festivals>Politique concernant le financement des festivals"...
Que l'on puisse faire une proposition au "niveau 3", qui en fonction du nombre d'approbations remonte au "niveau 2", et entre les différentes propositions un choix soit proposé pour devenir la priorité dans la politique concernant les festivals.

Score : 0 / Réponses : 0

----

l'évaluation des lois doit s'inscrire dans une pratique démocratique de l'évaluation es politiques publiques associant les citoyens, les personnes qualifiées et les parties prenantes à la loi

Score : 0 / Réponses : 0

----

la prise des éventuels textes d'application doit être enfermée dans des délais, lesquels font partie de l'évaluation, délai de "fabrication" et contenu

Score : 0 / Réponses : 0

----

toute loi doit énumérée les textes antérieurs qu'elle abroge

Score : 0 / Réponses : 0

----

Toutes les lois modifiées doivent être accessibles en version consolidée

Score : 0 / Réponses : 0

----

Toute loi devrait faire l'objet d'une évaluation dans une période courte après sa mise en oeuvre (un an semble un ordre de grandeur raisonnable mais cela dépend des lois)< Cette évaluation devrait être rendu publique et accessible facilement (sur le fond et sur la forme) pour les électeurs. Cette évaluation doit être faite par des élus (de la majorité et de l'opposition), des représentants des citoyens qui décideraient de la forme de cette évaluation ( techno, par une consultation citoyenne, par des réunions avec des électeurs impliqués,. Attention à ne pas en faire des évaluation comme celle faites par l'UE qui sont a peu près illisible et qui coûtent très cher

Score : 0 / Réponses : 0

----

Je ne suis pas certain que ce soit au citoyen d'évaluer les lois. Ce travail nécessite une expertise importante que nous n'avons pas tous. Mais l'évaluation des politiques publiques manque cruellement en France. Il faut une institution pour ça.

Score : 0 / Réponses : 0

----

Je trouve toutes ces thématiques intéressantes mais répétitives dans la réponse à apporter. Elles sont intrinsequement liées les unes aux autres et comportent finalement la même réponse : refonder la démocratie en réaffirmant le pouvoir d agir, pour et sur  le socius et l individuel, de chacun des citoyens qui le souhaitent. C est donc remettre l action politique au cœur des  préoccupations citoyennes et leur donner des leviers pour agir. Donc comme je l ai dit dans les autres thématiques qui se lient à celle-ci, constitution de conseil citoyen à tous les niveaux avec un pouvoir consultatif et de vote.

Score : 0 / Réponses : 0

----

Dans le journal officiel  227 de septembre 2017 un décret est publié sur les exceptions dans le code forestier au principe d'acceptation en cas de non réponse de l'Administration. Ce décret est en complète contradiction avec la loi. Comment faisons nous, pauvres quidam que nous sommes, pour comprendre et respecter le Processus?

Score : 0 / Réponses : 0

----

À la manière des questions au gouvernement, les citoyens internautes pourraient poser des questions sur un projet ou une proposition de loi dont les plus soutenus seraient posées en sessions par un rapporteur à l'Assemblée nationale. 
La participation directe des citoyens au processus démocratique est complémentaire au travail des élus. Ces derniers ont des questions que les premiers n'ont pas et réciproquement.

Score : 0 / Réponses : 0

----

Créer un logiciel, qui a chaque fois  qu'un citoyen fait une contribution la fait remonter directement au parlement, ce même logiciel pourrait regrouper les mêmes idées et évité les doublons de proposition similaires
La coordination entre les citoyens pourraient se faire via une plateforme pouvant être consulté par chacun et donnant un moyen d'agir de façon groupé pour intervenir sur les lois discutées

Score : 0 / Réponses : 0

----

Commencez aussi par relier les lois à des besoins réels et à des cas documentés réclamant la résolution de problème dans la société française.  On pourraient déjà voir combien sont utiles.

Score : 0 / Réponses : 0

----

Le problème est je crois dans la différence qu'il peut y avoir entre l'efficacité d'une loi particulière et l'efficacité globale d'un corpus législatif. Mille bonnes lois peuvent faire un ensemble indigeste, cause de nombreux blocages... Si l'on doit chercher la participation du citoyen c'est, non pas dans l'évaluation d'une loi, mais dans l'évaluation globale d'une complexité liée à de multiples lois.

 En effet dans notre quotidien (familial, bénévole, professionnel, sportif,....) nous sommes rarement face à une loi, mais plus souvent devant naviguer entre une énorme quantité de lois et autres textes. 

Toute loi nouvelle vise à apporter une solution meilleure à un problème particulier, et parfois, même souvent, elle y parvient, pour peu qu'elle soit appliquée. Mais cette loi nouvelle inscrit aussi assez souvent "en creux" et de façon assez peu visible (et donc difficilement évaluable) une pierre  à la montagne de la complexité législative, et ébranle (généralement  légèrement)  la stabilité législative source de sécurité.

C'est le même problème que de faire du shooping . Vous avez toujours utilisé une fourchette pour presser votre ail. Un jour, vous tombez sur un magnifique presse-ail. Vous l'achetez en voyant tout de suite les avantages de l'objet: presser de l'ail sera plus rapide et vous éviterez les doigts qui sentent mauvais. Une évaluation même, à posteriori, ne pourra que confirmer son utilité. 
Par contre, si vous évaluez la qualité de la fermeture du tiroir de la cuisine, vous vous rendrez compte qu'il coince souvent, vous obligeant à de savantes manœuvres toujours plus complexe et coûteuse en temps pour le fermer. Bien sûr, le presse-ail n'est pas seul en cause, la responsabilité est partagée par les tout aussi utiles étrognoneur de pommes, coupe-oeufs, etc

Score : 0 / Réponses : 0

----

[...] Je rajoute à mon message précédent et qu'il est vain à mon avis  de chercher à améliorer le taux d'application effective des textes législatifs sans travailler à la réduction de la complexité législative. 

Pour continuer sur ma métaphore, il ne faut pas s'étonner que le presse-ail ne soit pas utilisé si on ne peut plus ouvrir le tiroir.

Simplification et application: ces 2 problèmes me semblent si intimement liés, que je crois nécessaire de les associer dans la mise en place de groupe de travail citoyen.  

Le rôle du citoyen dans son interaction avec les parlementaires peut être de leur rappeler que, s'ils décident d'acheter un presse-ail, il faut qu'ils pensent aussi à jeter quelque chose pour que le tiroir ferme toujours.

Et comme vous le savez, il est plus simple d'acheter une chose que d'en jeter une autre.

Score : 0 / Réponses : 0

----

Si la plateforme numérique peut être une source de coopération du suivi de la mise en œuvre des lois ou des politiques publiques, il faudrait la mettre à disposition non seulement des citoyens mais aussi des parlementaires dont -comme c'est indiqué dans le résumé de présentation- la deuxième mission. On l'a vu ces dernières années : une multitude de lois sont prises sans que soit fait auparavant un bilan des lois précédentes. Le seul bilan présenté avec un projet de loi est un bilan qui supporte inévitablement la loi en préparation, il est donc assez souvent biaisé. De même, pour ce qui est des politiques publiques mises en oeuvre, le législature s'appuie trop souvent sur la Cour des comptes qui si elle doit veiller au bon usage des deniers publics s'invite souvent à la définition de ces politiques par des visions purement comptables et budgétaires.
Du côté citoyen, si cette plateforme voyait le jour, serait-il possible, aussi d'envisager la mise à disposition d'une formation (elearning, emooc etc) pour aider à la compréhension des lois, des implications budgétaires, des interactions entre les différents niveaux de décision (national:/local, périmètre de l'application de la loi etc) afin que la contribution du citoyen et de la citoyenne puisse être suffisamment éclairée ? La compréhension de certaines lois sans connaitre le contexte, l'historique ou l'interaction avec d'autres lois ou codes me paraît être un un préalable.

Score : 0 / Réponses : 0

----

Dans chaque département, les parlementaires devraient organiser des conférences citoyennes impliquant des élus locaux et des citoyens tirés au sort au cours des quels le préfet viendrait rendre compte de l'action du gouvernement, d'abord en application de la loi de finances mais aussi sur des lois choisies par les parlementaires et les membres ce la conférence.

Score : 0 / Réponses : 0

----

Je pense que le contrôle parlementaire et l'évaluation des politiques publiques dont notre Parlement n'use pas assez doit être présentés aux administrés sur un site internet dédié aux fins de contrôles populaires.

Cependant, il appartient au législateur qui a reçu mandat directement ( Député ) ou indirectement ( Sénateur ) d'obliger le Gouvernement à prendre des décrets dans un délai défini dans la loi ou le règlement des assemblées. 

Le contrôle parlementaire doit être plus rigoureux et contraindre à l'annulation d'une loi qui n'a pas reçu ses décrets d'applications. 

Quand bien même, le Premier Ministre est responsable de l'exécution des lois et il appartient aux parlementaires de prendre les mesures nécessaires si celui-ci n'effectue pas ses tâches constitutionnelles. 

Par ailleurs, il serait souhaitable que les Députés particulièrement car élus par le peuple et à qui ils doivent rendre compte en circonscription se saisissent de ce pouvoir de l'évaluation des politiques publiques pour orienter et contrôler davantage l'action du Gouvernement.

Score : 0 / Réponses : 0

----

Exemple d'amélioration en fonction de la difficulté d'application : pensions de réversion à partir de la date de décès dans tous les cas;  supprimer le délai.
Je vous propose une mesure d'équité: maintenir les droits à pension de réversion à partir de la date de décès, pour toute pension de réversion non liquidée, quelle que soit la date de la demande de réversion (par la veuve) et pas seulement si elle est faite dans l'année du décès. Les caisses de retraite ne peuvent contacter les anciens conjoints. Les veuves divorcées ou séparées ignorent souvent leurs droits et ne savent pas si leur ancien mari est décédé. Résultat : des pensions de réversion jamais  réclamées ou trop tard (exemple: 15 ans après) et des veuves aux faibles ressources privées de leurs droits.: 
[En jargon juridico-administratif la pension de réversion sont "quérables" et non "portables". La solution pratique n'a pas été anticipée; chaque divorcé ou séparé n'a comme solution que d'être curieux et vigilant, de demander tous les 11 mois si son ex est toujours en vie !  concrètement, de demander à la mairie de naissance de son ex une copie intégrale de l'acte de naissance.  Idem pour les veufs et orphelins]. Le conciliateur, le médiateur et la commission de recours refuseraient  actuellement toute demande hors délai.
Cette mesure est simple, acceptable par tous, immédiate à adapter dans la loi,  décréter et appliquer . Elle rejoint la volonté d'équité annoncée par Emmanuel MACRON.. D'intérêt général, il serait juste qu'elle soit rétroactive comme le permis à point. Qu'en pensez-vous?
NB: idem pour le capital décès à demander à la CPAM,...

Score : 0 / Réponses : 0

----

Le diable est souvent dans les détails: décrets d'application, instructions administratives-opposables?-, modalités d'application qui sembleraient pouvoir de fait être contraires à l'esprit de la loi voire au texte de la loi, sans prendre en considération de fait le mode de calcul le plus avantageux pour le citoyen/l'usager.
Exemple : refus CARSAT de prendre en compte dans les 25 meilleures années la dernière année travaillée "si elle n'est pas complète"(exemple: travail en 2017 jusqu'au 31 aôut et demande de retraite à effet du 1er septembre).... sauf "si la retraite est demandée à  effet du 1er janvier de l'année suivante"(ce que le salarié futur retraité devrait savoir comment?). Le texte cité par la CARSAT pour ce refus prévoit pourtant expressément  :
<< I.-Pour l'application de l'article L. 351-1, et sous réserve des dispositions des articles R. 173-4-3 et R. 351-29-1 le salaire servant de base au calcul de la pension est le salaire annuel moyen correspondant aux cotisations permettant la validation d'au moins un trimestre d'assurance selon les règles définies par l'article R. 351-9 et versées au cours des vingt-cinq années civiles d'assurance accomplies postérieurement au 31 décembre 1947 dont la prise en considération est la plus avantageuse pour l'assuré>>. 
Curieusement, la CARSAT refuse mais demande les bulletins de salaire mais ne s'en sert que pour vérifier le nombre de trimestres cotisés.
En pratique, il  serait tout à fait possible à la CARSAT  de faire AGIRC ARCCO un calcul provisoire en 2017 et de régulariser en 2018 lorsqu'elle a connaissance des salaires cotisés 2017.... comme elle le ferait dans le cas où pour une fin de travail au 31 août 2017 la retraite serait demandée à  effet du 1er janvier 2018.
Il serait équitable que les modalités d'application soient clarifiées/révisées dans l'intérêt de l'assuré avec effet rétroactif, s'agissant d'une mesure d'intérêt général (comme l'effet rétroactif du permis à point). Qu'en pensez-vous?

Score : 0 / Réponses : 0

----

L'évaluation pourrait-elle aménager systématiquement une dérogation favorable au citoyen /à l'usager / l'assuré lésé ?
exemple : suppression du délai de régularisation en cas de fraude d'un ou plusieurs employeurs ou d'oubli des caisses de retraite ; recalcul et régularisation des retraites de base et complémentaires
 En cas de fraude d''un ou plusieurs employeurs (cotisations prélevées mais non reversées à la CARSAT, à la MSA ou au RSI...) ou d'oubli des caisses de retraite(problème de coordination?), pouvoir demander une régularisation quelle que soit la date à laquelle le retraité, le conjoint ou les enfants s'en aperçoivent. (parfois dans les papiers du retraité décédé). Ce cas a pu échapper à l'intéressé et aux caisses a fortiori en cas de cumul d'activités dans différents régimes pour les mêmes trimestres (pas de « trous>> dans le relevé de carrière)
Qu'en pensez-vous?

Score : 0 / Réponses : 0

----

A l'ère numérique, il existe des solutions simples, rapides et efficaces à mettre en œuvrent dans le cadre de l’évaluation des politiques publiques. L’assemblée nationale doit prendre le virage de la communication ciblée et plus largement du marketing "actif". Elle doit mettre en place un certains nombres d'outils simples permettant aux citoyens de donner leurs avis sur les politiques publiques :
1- mettre en place des enquêtes de "satisfaction" dès la 1ere année de mise en place d'une politique;
2- permettre, aux travers d'un vote numérique sur le site assemblee-nationale.fr, d'évaluer une politique;
3- mettre en place des campagnes de newsletter en demandant aux citoyens volontaires de faire un retour sur l'application de la politique dans la "vie réelle";
4- tout simplement organiser, à l'assemblée nationale, des réunions publiques permettant de débattre sur la politique en question (cela permettra également aux citoyens de se réapproprier l'assemblée nationale).

etc. etc.

Score : 0 / Réponses : 0

----

très bonne initiative pour que les citoyens que nous sommes puissions intégrer les lois qui nous concernent de près ou de loin.
l'adage dit que personne n'est sensée ignoré la loi mais faut-il encore être au courant des lois qui sont votés au sein de l'assemblée nationale et de leur portée au quotidien.
que les citoyens soient impliqués est une bonne chose.

Score : 0 / Réponses : 0

----

Pour refonder les institutions républicaines et le débat public, et que les citoyens se réinvestissent dans la prise de décisions :

- Supprimer le Sénat et le Conseil économique, social et environnemental.
- Créer une Assemblée de l’intervention populaire qui sera en charge d’ emmètre un avis sur l’impact écologique et social des lois. Elle pourrait notamment défendre les avis des citoyens devant les députés. Elle permettrait de faire participer de « simples » citoyen aux décisions sur des sujets qui font controverse. Elle pourrait être tirée au sort (afin de diversifier l’origine sociale notamment) en totalité ou pour moitié par exemple.

Score : 0 / Réponses : 0

----

Proposer en parallèle aux plateformes d'échanges, des observatoires parlementaires transpartisans dédiés :
- au suivi de l'application des lois
- au suivi de l'expérimentation et à l'évaluation
- à la comparaison aux dispositifs étrangers (européens et extracommunautaires)
- à la simplification législative

Score : 0 / Réponses : 0

----

Le rôle de contrôle du parlement devrait l'amener à produire ou faire produire des évaluations beaucoup plus systématiques, et à débattre de leur contenu et des suites à leur donner. Les documents d'évaluation pourraient être mis à disposition des citoyens qui souhaitent contribuer à la réflexion en amont des débats parlementaires.

Score : 0 / Réponses : 0

----

La fonction d'évaluation des politiques publiques du Parlement devrait être renforcée. Cela peut passer notamment par un temps d'examen de la loi de règlement plus long puisqu'il s'agit de mesurer les effets des politiques mises en place. L'association des citoyens et des bénéficiaires concernés des politiques publiques  paraît nécessaire à cette évaluation lors de l'examen de la loi de règlement. Une conférence de citoyens/ bénéficiaires de la politique étudiée pourrait ainsi être mise en place pour enrichir l'adoption de la loi de règlement  et recueillir l'évaluation citoyenne sur quelques politiques publiques chaque année. L'année suivante d'autres politiques feraient l'objet d'analyses citoyennes.

Score : 0 / Réponses : 0

----

Bonjour, afin de rétablir la confiance dans les institutions, un engagement à évaluer et à faire participer des citoyens à l évaluation des impacts des lois me semble fondamental. Comme décrit dans certains ouvrages (ex Marc Ferracci) il faudrait mettre en oeuvre systématiquement les indicateurs d évaluation d impact et disposer de moyens de les faire connaitre. Les députés et sénateurs sont légitimes pour créer les lois, les citoyens sont les "bénéficiaires" des lois et donc légitimes pour les évaluer.

Score : 0 / Réponses : 0

----

Un outil numérique pourrait être un prolongement des dispositions détaillées sur vie-publique.fr avec une infographie claire, notamment avec les décrets d'application parus ou non

Score : 0 / Réponses : 0

----

Un service public dédié devrait forcer les élus à rendre compte des choix et décisions prises au nom du peuple, qui délibérerait alors sur la valeur des politiques au regard de leur expérience de vie. Les élus parlementaires ou territoriaux seraient tous concernés et confrontés non pas seulement à leurs pairs et à leur parti politique, mais à tous les citoyen-ne-s.
Les citoyen-ne-s seraient tous invités à participer à des assemblées locales, en proximité de leur lieux de vie, pour être informés des décisions publiques et questionner les fonctionnaires indépendamment des élus de l’exécutif.

Score : 0 / Réponses : 0

----

Débrief d'une réunion d'un comité EM de 5 personnes 

On peut utiliser un outil informatique pour receuillir les avis et permettre aux citoyens de participer (type www.demodyne.org)
Peut-on intégrer tout le monde dans la participation ? 
Quid des syndicats ? 

Associer représentant institutionnels et citoyens concernés par le projet. 
- tirage au sort dans une sous population identifiée (volontariat sur inscription)
On créé une plateforme collaborative avec les volontaires sélectionnés, où ils peuvent échanger entre eux et faire remonter les idées et où les élus peuvent tester des idées sur cette population 
- nouveau partenaire social qui est "la rue"
- Demander aux professionnels de formuler des solutions 
- QCM (type loi logement En marche) 
- Inscription volontaire sur une plateforme sans sélection OU Inscription volontaire sur plateforme avec sélection de 5 personnes par groupes 

Lobbying inversé en prennant 5 à 8 personnes qui sont spécialisés et professionnels d'un domaine qui vont faire émerger des solutions. 

Exemple : entreprise libérée 

Questions en suspens : 
Comment cibler les personnes concernées pour les volontaires : publicité internet, affiche en mairie, campagne nationale service information de l'assemblée, 

Conclusion : 
Possibilité par le numérique plateforme collaborative ou pour le présentiel : avoir les personnes dans une salle et banaliser une journée de travail et payer les déplacement pour être pleinement disponible

Score : 0 / Réponses : 0

----

Il doit y avoir des personnes désignées pour les suivies pour les récoltent des informations collectés sur le terrain pour mieux comprendre les desarois des habitants ainsi permettre des remontées directs aux parlementaires qui permet d'avancer plus rapide sur le sujets concernés et d'évaluer les couts et les moyens humaines et matériels.

Score : 0 / Réponses : 0

----

En ce qui concerne l’évaluation de la mise en oeuvre des lois ! Je propose un débat entre les différents acteurs (représentants du gouvernement ou du parlement et partenaires sociaux) avec un moment de réponse aux questions populaires qui ont eu le plus de succès (sur une plateforme en ligne de questions avec notation de ces questions par le peuple) et après le débat un vote sous forme de référendum en ligne avec la question simple : approuvez- vous la mise en oeuvre ou non ! Si le résultat est non le représentant légal du gouvernement ou parlement devra représenter dans un délai décidé lors du débat un nouveau plan prenant en compte les remarques du débat ! Les personnes qui participent a ce genre d’initiative populaire pourraient le faire avec un identifiant lié à leur papier d’identité !

Score : 0 / Réponses : 0

----

Cette synthèse est le résultat d'une contribution collective organisée par le député Bruno Studer.

Nous recommandons de soumettre aux citoyens les thèmes suivants :
- dangers ou difficultés dans la mise en œuvre des lois (en amont)
- non application ou mauvaise application des lois (en aval)
- décrets d'application : non publiés ou en retard, non conformes
- méthode de la consultation
- lobbying. La consultation citoyenne est un contrepoids
- inflation législative, nettoyage, ré écriture des lois modifiées

Quelques propositions :
- décrets : consultation des citoyens en amont; transparence dans leur élaboration
- imposer un délai minimum entre le vote des lois et la parution des décrets
- méthode de consultation : plate forme numérique permanente
- nettoyer tous les codes de lois
- inclure dans les projets de lois des critères dévaluation et des indicateurs de suivi

Score : 0 / Réponses : 0

----

Synthèse de l'atelier citoyen organisé par la Députée de la 1ère circonscription de la Drôme:
- Lors de la phase d’élaboration de la loi on devrait inscrire des indicateurs d’évaluation de la loi afin de pouvoir l'apprécier concrètement.
- Il n’y a pas que la mise en œuvre des lois qui doit être évaluée, mais également son efficacité. C'est particulièrement sur l'efficacité que les citoyens devraient pouvoir donner leur avis et être consultés.
- Il faudrait pouvoir consulter tous les citoyens qui ont participé en amont et leur demander s’ils sont satisfaits de l’évaluation faite par les parlementaires.
- Consultations citoyennes organisées par les députés, où ils rendraient des comptes devant leurs administrés sur la mise en oeuvre d'une loi et recueilleraient en parallèle leurs contributions.

Score : 0 / Réponses : 0

----

Comment mesurer l'impact d'une loi sans avoir la certitude qu'elle sera appliquée.  L'application de la loi se fait in finé au TRIBUNAL, et par des JUGES, qui eux, sous prétexte que la justice doit être indépendante, n'ont pas de compte à rendre à qui que ce soit et ne peuvent pas être sanctionner pour ne pas avoir fait leur travail. Tout le monde doit rendre compte, un salarié à son patron, un enfant à ses parents, un soldat à son chef de section, un président à son peuple, TOUS.... SAUF LES JUGES. Une commission composée à part égales de juges, d'avocats, et de citoyens doit être créée,  avec possibilité de la saisir par voie numérique,  dans le but de déterminer si le droit à bien été appliqué à tous les niveaux (1° instance, appel, cassation) chose que ne fait pas le CSM aujourd'hui. A partir de là seulement on pourra mesurer l'impact d'une loi...

Score : 0 / Réponses : 1

----

L'écriture de la loi vise à ordonner la vie démocratique selon les règles républicaines; cependant, il s'agit toujours d'une "construction intellectuelle", comme le droit dans son ensemble. Les effets de bord, les effets pervers ne peuvent pas tous apparaître immédiatement au législateur. Face à ces effets indésirables le grand public tend trop facilement à voir une incapacité à gérer démocratiquement le vie publique. Repli dans l'abstention, populisme, voire pire. Il faut donc imaginer les moyens d'expliquer la complexité.
Des outils existent; on pense spontanément au numérique. Pourtant un des outils les plus adaptés ne serait-il pas la télévision (accessible aussi bien entendu sur le web) ? Pourquoi ne pas imaginer une émission (ou des émissions) qui expliqueraient cette complexité avec des moyens simples, orientés vers le plus grand nombre. Cela pourrait s'appeler, par exemple, "Pourquoi ça coince ?" ou "La loi et les grains de sable", etc. Chaque émission aborderait une thématique, réaliserait une enquête de terrain, solliciterait des citoyens. La parole serait bien sûr donnée au législateur, à des grands témoins, des solutions seraient esquissées et proposées. 
Tiens, au fait, il existe une chaîne parlementaire. Cela ne pourrait-il pas être une de ses missions ?

Score : 0 / Réponses : 0

----

Rendre obligatoire la publication des données d’évaluation en open data

Les données d’évaluation représentent l’ensemble des données utilisées pour la préparation des rapports d’évaluation des politiques publiques, notamment dans le cadre des missions d’évaluations parlementaires et des évaluations de la Cour des comptes. L’ouverture des données publiques d’évaluation est un prérequis pour renforcer le débat sur les critères d’évaluation et la construction des indicateurs. Elle permettrait en effet d’améliorer la précision et la pertinence de ces indicateurs.

La mise en place d’un LégaLab, un service parlementaire de la donnée ouvert aux élu-e-s et citoyen-ne-s permettrait de multiplier les usages autour de ces données publiques  et de soutenir les parlementaires dans leur travail d’évaluation de la mise en oeuvre de la loi.

Score : 0 / Réponses : 0

----

Si l'utilisation du numérique peut effectivement être un moyen de permettre la coopération entre le citoyen et les instances gouvernementales et parlementaire, il a encore une limite de taille qui réside dans la couverture de notre Pays. De fait, actuellement une partie de nos concitoyens en sera exclue.
Toutefois l'apport du numérique me semble intéressant car il permet, par le biais de plateformes gouvernementale et parlementaires, de participer à l'évaluation et au contrôle des lois votées. Le ressenti, le vécu dans le cadre de la mise en oeuvre d'une loi peut aides le Parlement dans sa mission de contrôle et d'évaluation des politiques publiques.
Cette contribution citoyenne doit être encadrée pour que les contributeurs soient clairement identifiés pour éviter toute dérive. Elle peut également être l'oeuvre d'un collectif citoyen, clairement identifié, qui ne soit pas l'émanation d'un parti politique, représenté ou pas au sein du Parlement.

Score : 0 / Réponses : 0

----

La production des lois et la mise en œuvre des politiques publiques doivent être assorties d’une évaluation robuste et fiable des effets produits. Les différentes instances parlementaires permettent de mesurer l’impact de la loi, mais à mon sens ce travail pourrait être amélioré dans les aspects suivants =
Les résultats attendus ne sont pas toujours orientés « clients » c’est-à-dire vers les citoyens
L’approche est peu micro-évaluative avec une vraie granulométrie locale
L’approche me semble d’avantage de l’évaluation brute qu’une boucle d’amélioration entre l’application du dispositif législatif, l’objectif,  le résultat  initial observable et des actions correctives qui pourraient modifier le dispositif.
Aussi  je pense que quelques  groupes d’évaluation pourraient constituer un support intéressant pour les parlementaires.  La composition serait pluridisciplinaire et avec différentes parties prenantes : acteurs de terrains chargés de l’appliquer,  destinataires ou bénéficiaires et citoyens « financeurs du dispositif ».  Le groupe produirait des synthèses d’éclairages à destination du parlement au semestre.

Score : 0 / Réponses : 0

----

SYNTHESE CONSULTATION CITOYENNE 5911

Intervenir sans freiner le processus législatif.
Admettre un minimum de temps pour les citoyens : jours légaux de congés pour répondre à une convocation du député sur une thématique particulière.
Possibilité d’intervention dès le début du travail en commission.
Point de passage obligé e la commission auprès des citoyens (exemple par dizaine d’articles). A ce point de passage, le député va chercher les citoyens qui ont été retenus.
L’avis contraire des citoyens doit être inscrit.
Mise à disposition des textes, auditions publiques, amendements citoyens non évoqués par manque de temps.
Droit de veto rejeté.

Score : 0 / Réponses : 0

----

SYNTHESE CONSULTATION CITOYENNE 5911

Avant même d’évaluer une loi, s’assurer que l’ensemble des décrets est publié.
Fixer des rendez-vous de principe pour l’évaluation puisque les effets / résultats arrivent sur une durée variable.
Fournir un cadre pour l’évaluation incluant les données.
L’évaluation diffère par exemple pour le domaine de l’éducation (professeurs, parents et élèves n’ont pas les mêmes attentes) : veiller sur « qui » évalue.
Concilier évaluation quantitative et qualitative.

Score : 0 / Réponses : 0

----

SYNTHESE CONSULTATION CITOYENNE 5911

Avant même d’évaluer une loi, s’assurer que l’ensemble des décrets est publié.
Fixer des rendez-vous de principe pour l’évaluation puisque les effets / résultats arrivent sur une durée variable.
Fournir un cadre pour l’évaluation incluant les données.
L’évaluation diffère par exemple pour le domaine de l’éducation (professeurs, parents et élèves n’ont pas les mêmes attentes) : veiller sur « qui » évalue.
Concilier évaluation quantitative et qualitative.

Score : 0 / Réponses : 0

----

Commentaire rédigé collectivement par le comité local dont je suis l'animatrice

Afin de pouvoir évaluer l'efficacité d'une loi il faut qu’au moment de sa création les objectifs soient clairement explicités.
Nous pourrons à partir de cela faire des sondages simples (type satisfait ? bouton OUI et NON sur une I PAD par exemple). Ces sondages seraient menés à bien par les membres des  comités locaux sur un secteur géographique prédéfini qui compileraient par la suite  les réponses et les feraient remonter au niveau national
Afin d’intégrer le problème de la fracture numérique nous proposons de faire le rélai aussi par le biais de CL, nous pourrions faire des convocations papier via les boites aux lettres (étant donné que nous visons à intégrer les personnes »exclues « du numérique) pour assister à des permanences définies pour écouter et nous faire l’echo des personnes non connectés

Score : 0 / Réponses : 0

----

L'outil numérique permettra très certainement de toucher les citoyens mais avant toute chose il faudra rendre les textes de loi lisibles. 
On produit tous les jours beaucoup de textes mais qui sont difficiles à saisir pour un citoyen ordinaire. 
Enfin, grand nombre de nos concitoyens ne sont pas à l'aise avec les outils numériques d'où le problème peut-être de formation citoyenne aux outils numériques.

Score : 0 / Réponses : 0

----

Pour une démocratie renforcée, je suis en faveur d'un contrôle direct de l’action des députés par une chambre de citoyens tirés au sort avec l’objectif d’être la plus représentative de la population française en termes d’âge, de CSP et zone géographique, qui, à la majorité qualifiée, pourra adresser des avertissements, des sanctions voire des “licenciements” envers des députés qui ne se conforment pas aux règles de déontologie imposées par leur fonction. Partisan de l’équilibre, s’il y a des mauvais points, il doit aussi y en avoir de bons ! 
En soulignant voire en récompensant l’intégrité, les qualités (rhétoriques, intellectuelles, humaines, engagements, contacts avec les citoyens) et le travail accompli par certains députés, cela restaurera la confiance des Français dans la chambre et incitera les autres députés à tendre vers la vertu en cherchant à imiter leurs confrères les plus méritants qui seront dans ce système, les plus valorisés, médiatisés et récompensés.

Score : 0 / Réponses : 0

----

Tirer au sort des citoyens pour l'élaboration d'une loi proposée soit par la société civile au terme d'une pétition citoyenne (qui aurait plus de 100 000 signatures comme au Royaume-Uni), soit par des députés. On les met ensuite en relation avec un groupe d'expert issu d'institutions publiques ou non afin des les épauler dans la rédaction d'une loi citoyenne. Surtout, laisser le temps aux citoyens de mûrir leur réflexion: proposer des séances écartées dans le temps, des conférences... Le but est d'avoir une décision issue des citoyens, pour les citoyens mais éclairée, c'est-à-dire informée sur les conséquences de cette décision, sur la conjoncture, les enjeux. On aurait une décision qui sera humaine, libéré des enjeux politiques et des lobbys. il faut s'affranchir de tout ce qui bloque la mise en place de décisions de bons sens mais qui sont freinés par les schémas politiques actuels et les grandes entreprises. Trop de mesures ne sont pas appliqués par mauvaise volonté des dirigeants.

Score : 0 / Réponses : 0

----

La diffusion de l’exclusion territorialisée doit elle nous faire conclure à l’échec relatif de la politique de la Ville et donc à son impossible évaluation ?

Les travaux de la Délégation Interministerielle à la Ville (DIV) ont permis de formaliser des processus d’évaluation et des méthodes. En se référant à des grands objectifs nationaux, à des cibles qui conduisent à des programmes d’action évalués selon des référentiels sur des sites donnés. « L’augmentation du niveau de qualification des jeunes sur le territoire » peut ainsi donner lieu à un programme de formation sur x individus (réalisation), évalué par un taux de réussite à un test au sortir de leur cursus (résultat) et la mesure de l’évolution de l’employabilité de cette « cible » (impact).

Plusieurs difficultés liées à la nature de cette politique

Le rapport de la Cour des Comptes (novembre 2007) pointe des évaluations incomplètes sur des objectifs « rabattus » au détriment de l’emploi et au bénéfice de l’insertion par exemple ou le manque d’articulation avec d’autres contrats existants (contrats locaux de sécurité). La DIV surligne elle-même ([Annexe 3, note de cadrage relative à l’évaluation des CUCS, 5 Juillet 2007)] la difficulté de trouver de « bons indicateurs » : disponibles, précis sans être pléthoriques. Comment discerner ce qui résulte des actions menées dans le cadre des contrats, des politiques ordinaires ou du contexte socio-économique ? Quelle légitimité des référentiels quand ils sont construits par rapport à des « villes moyennes » du territoire concerné ? En juillet 2012, le rapport de la Cour des Comptes ([La politique de la Ville : une décennie de réformes, Juillet 2012)] soulignait ainsi que certains outils mis en place tels que la Dotation de Solidarité Urbaine (DSU) n’intégraient pas en amont une évaluation de leur résultat. Les rapports que les collectivités doivent renseigner pour justifier de son utilisation ne comportent pas d’évaluation de réduction d’inégalités prévues par exemple.

Ces difficultés d’évaluation peuvent être dépassées

Des objectifs et des indicateurs ont été mis en oeuvre par la Loi du 1er août 2003 renforcés par une meilleur connaissance de ces politiques par la création de l’Observatoire national des zones urbaines sensibles (ONZUS) et une culture de l’évaluation financée notamment par le ‘programme 147’ de performance. Mais l’évaluation de la Politique de la Ville ne doit elle pas être davantage appréhendée en terme de plus value ?directe ou indirecte- apportée à l’action publique ? Dans ce contexte, l’évaluation d’une « dynamique de rattrapage » tend à cristalliser dans les bassins populaires ou en reconversion des oppositions binaires là où se développe le plus souvent un éventail de fragilités. Elle peut aussi s’avérer vaine dans la mesure où les lieux de décision sont extérieurs aux territoires considérés.

Dés lors que l’évaluation porte sur les actions développées par la Politique de la Ville et ses modes d’action, elle atteint ses objectifs. En termes de mise à niveau des politiques publiques que de dynamiques articulées de développement et de solidarités, son ambition est d’apprécier la qualité conjointe Etat/collectivités signataires vis-à-vis des collectivités « externes ». C’est bien ce déficit d’articulation notamment entre l’effort historique de rénovation urbaine et les volets sociaux que les acteurs de ces politiques ont pointés. Sa résorption doit être un critère de refonte des réformes à venir.

Score : 0 / Réponses : 0

----

Contribution collective des comités strasbourgeois En Marche ! :

Constat : il est parfois difficile d'identifier qui est précisément concerné par la loi d’autant plus que les lois sont de plus en plus nombreuses et volumineuses.
Propositions : L'évaluation des lois et de leur application serait à l’initiative :
1. De l’Etat et autres organismes publics (collectivités territoriales…).
2. Des citoyens : le retour sur cette évaluation se ferait soit directement via le député soit par voie numérique via la mise en place d’une plateforme nationale par thématique.
3. Une évaluation tous les 2 ans organisée par le député sur les lois les plus importantes ou celles qui ont le plus d’impact sur la circonscription.

Score : 0 / Réponses : 0

----

Les outils d’intelligence artificielle permettraient de compiler l’amas de lois, les trier, détecter les contradictions, les oublis; ils pourraient l’être en parallèle les decisions des juridictions pour compléter, orienter, humaniser les textes. Que nos élus traitent des manques et clarifient l’existant; qu’ils se concentrent sur la simplification, l’accessibilite des textes !  Qu’ils créent des commissions dédiées à l’élimination des textes, et non pas seulement à leur élaboration.

Score : 0 / Réponses : 0

----

Les lois devraient être construites comme des plans d’action. 
Elles devraient être spécifiques, mesurables, atteignables, réalistes et timées.
Elles devraient avoir un cycle de type roue de Deming. Finalement, elles devraient entre pensée en processus qualité et être piloter comme un indicateur de performance.

Score : 0 / Réponses : 0

----

D'après la fiche 52 sur le contrôle de l'application des lois et l'évaluation de la législation et des politiques, il existe déjà plusieurs mécanismes intéressants. Des citoyens volontaires pourraient peut-être se manifester auprès des commissions temporaires ou permanentes, ou auprès de leur député, lorsqu'ils veulent faire remonter des informations sur l'application ou les conséquences des lois et politiques, mais la question semble plutôt être : que fait-on de ces évaluations ? 
Elles pourraient être rendues publiques et aisément compréhensible pour le grand public via un site dédié et dans les médias, cela déclencherait de nouvelles participations et de nouveaux retours de la part de personnes concernées et intéressées, qui pourraient être triés et ajoutés au travail de la commission. 
Ensuite, des conclusions doivent être tirées : repasser la loi devant les deux assemblées afin d'apporter les modifications nécessaires, convoquer des citoyens s'étant exprimés sur le sujet, souhaitant participer à l'amélioration de la loi, ou s'étant exprimés plus en amont dans l'écriture ou l'élaboration de la loi (en présentiel dans des comités citoyens ou en ligne via de nouvelles techniques de participation, cf. propositions dans la catégorie correspondante) et incorporer le résultat de leurs discussions aux discussions parlementaires. 
Nouveau vote sur la loi et relance du processus d'évaluation jusqu'à la fois suivante.

Score : 0 / Réponses : 0

----

Les associations à but non lucratif sont un vivier de ressources de compétences, d'expérimentations et d'actions.
Souvent apolitiques, elles suppléent bénévolement, à partir de décisions prises démocratiquement, à un manque d'action publique. Leurs avis et propositions, dans leurs domaines de compétence (ce qui se vérifie par leurs statuts) devraient être recherchés et valorisés car émanant déjà démocratiquement de collectifs de citoyens engagés.

Score : 0 / Réponses : 0

----

Tirer au sort un panel de citoyens (comme pour les jurés d'assise) en leur soumettant un questionnaire destiné à permettre d'évaluer l'atteinte des objectifs visés par la loi (en laissant un espace libre à la fin pour bénéficier de remontées sur des points non envisagés ou occultés même involontairement). 
Le panel de citoyens tiré au sort seraient constitués à la fois de bénéficiaires de la loi ET de personnes impliquées dans la mise en application de la loi. Cela permettrait d'avoir une vision globale de l'impact des textes de lois destinés à être évalués.
Pour les personnes tirées au sort qui ne bénéficierait pas d'un accès internet et/ou auraient des difficultés à compléter le questionnaire en ligne, il faudrait  prévoir la possibilité de se rendre en mairie pour compléter le questionnaire (ou d'envoyer le questionnaire à un service de l'état qui se chargerait de compléter le questionnaire en ligne).
La réponse au questionnaire serait rendue obligatoire (une pénalité financière même modique pourrait être envisagée afin de financer une partie du coût de la consultation)

Score : 0 / Réponses : 0

----

Bonjour
oui a un outils informatique nouveau ou reforme, et ou un outil télévisuelle, pour connaitre de façon dite simple, pour le tout a chacun, connaitre les détails du début a la fin de la loi (qui lance, qui propose des améliorations, les vote etc etc) si la loi est applicable, comment ,quand.et quelle recours et possible si non pertinente au yeux des citoyens, et si déjà des mouvements de procédure sont en cours. par contre si recours de façon x ou y qu'il soit accompagnée de solutions.oui, si une loi a été avancé ce qu'il y a un besoin ,ou un problème.
 donc l'outils pour savoir ce qui a été fait, pour mieux proposé et débattre , avant même la fin d'une dite loi, pour que cela soit le plus complet pour tous.

Score : 0 / Réponses : 0

----

Afin de contrôler la législation, il pourrait être intéressant de constituer des commissions qui seraient chargées d’évaluer des politiques publiques complètes (politique énergétique, de transport, de logement, de recherche, de sécurité, etc) plutôt qu’une seule loi, et ne comprenant pas que des parlementaires.
Ces commissions seraient composées de quelques parlementaires (députés et sénateurs représentant la diversité politique des deux assemblées), de spécialistes du domaine étudié (scientifiques, juristes, chercheurs dans le domaine étudié par exemple) et d’une majorité de citoyens tirés au sort parmi les volontaires pour auditer une politique publique précise. Ces évaluations pourraient être lancées à l’initiative du gouvernement, du Parlement ou d’un certain nombre de citoyens. Les citoyens tirés au sort auraient un statut particulier pour leur permettre de mener à bien leurs missions.
Ce groupe de travail aurait plusieurs buts à propos de la politique publique passée au crible :
- définir les objectifs poursuivis de cette politique, en identifier d’autres possibles, et déterminer s’ils sont accessibles et cohérents
- faire l’état du droit actuel, et notamment vérifier que les lois votées par le Parlement ont bien été suivies des décrets d’application pour les rendre effectives et que ces lois poursuivent bien les objectifs définis pour cette politique
- faire des propositions pour changer les priorités ou les moyens alloués, pour mettre en adéquation les objectifs et les outils
Un rapport serait rendu par cette commission, avec les idées majoritaires et des opinions minoritaires défendues par ses membres. Le gouvernement serait tenu de prendre les décrets d’application manquants pour l’application de la loi et tout projet ou proposition de loi déposé sur les bases de ce rapport serait automatiquement mis à l’ordre du jour de l’Assemblée Nationale ou du Sénat dans les six mois à compter de sa publication.
Pour permettre à cette commission de travailler dans de bonnes conditions, ses membres pourraient auditionner qui bon leur semble et se faire épauler par l’administration concernée. Un site Internet et des registres en mairie pourraient également permettre à n’importe quel citoyen de formuler ses observations.

Score : 0 / Réponses : 0

----

Professionnel de Santé , je m'interroge sur les débats qui perdurent sur "les déserts" médicaux et la panoplie de mesurettes  aussi incohérentes les unes que les autres dès lors qu'on ne s'attaque pas  la racine du problème :un numerus clausus étendu à presque toutes les professions de santé et son instrument "Le Paces" , gavage sélectif sans fondement  d'efficience. Dès l'instauration de ce système , la sonnette d'alarme a été agitée prévoyant 40 ans plus tard non les déserts médicaux mais la désertion médicale qui n'est pas que dans la Creuse ou la Lozère mais Paris intra muros quand un patient doit attendre 6 mois  pour un rendez vous de spécialiste. Un simple calcul   prévisionnel"  met à plat l'évidence :  si on part de 1000 étudiants inscrits en "Paces" , 200 sont "admis" après le gavage digéré de cet examen (qui n'a que provoqué une fracture sociale gigantesque grâce aux prépas à 8000 euros par an )..
Sur les 200 "admis" , après plus de 10 ans de cursus , internat , projets maritaux , démission , désertion psychiatriques et dépressions , il ne reste que 70 prétendants à une installation avec encore une fuite vers la santé publique..et pendant le même temps , pour le même quotas 177 médecins partent en retraite en fermant le rideau faute de pouvoir céder leur clientèle......Comment  voulez vous que les mesurettes  "sparadrap"  *     tiennent la route ! Dans moins de 8 ans , il n'y aura plus d'anesthésistes , ni de gynécologues ni de cardiologues ..prévisions des Ordres ! Qu'est ce qu'on attend pour reprendre ses esprits , remettre en place un contrôle continu sur deux ans avec à la 3 ème année une ouverture possible vers les carrières et disciplines  choisies et non imposées qui pourraient tripler le nombre de praticiens  disponibles dans une dizaine d'années certes , tout en sachant qu'il faudra compter sur une génération et demi pour  arriver seulement à un équilibre approximatif ! Bien entendu , le temps que les infrastructures supplémentaires d'accueil rattrapent leur retard, les cours sur internet et les stages via les pays étrangers seront nécessaires.. J'ai pu donner avec cet exemple précis qui impacte la priorité pour tous les citoyens ,leur SANTE , l'image du fossé qui s'établit entre les décideurs  , les consultants ( souvent "administratifs" ) et les professionnels de terrain ou les collectifs et  associations de malades , bien plus au fait des réalités .
L'initiative de reconsidérer les avis des citoyens de "terrain" est une image de la vraie démocratie qui si elle a le pouvoir de s'exprimer donnera un véritable tournant dans la compréhension des lois et leurs applications

*NB : dans les mesures "sparadraps " , il faut citer en outre , l'embauche devenue urgente de diplômés "étrangers" dans les hôpitaux .;à moindre coût qui ne parlent pas la langue française ( pensez à l'anesthésie ! ) , de transferts de compétence pour masquer les  carences de spécialistes ( à moindre coût) toutes ces mesures qui ont fait dégringoler la place de la Santé française sur l'échelle mondiale depuis 40 ans..

Score : 0 / Réponses : 0

----

Enfin ! Nous allons pouvoir être entendus.  Merci Adrien et son équipe.

Score : 0 / Réponses : 0

----

Excellente initiative. En souhaitant être entendus

Score : 0 / Réponses : 0

----

Cette initiative nous permet d'avoir une participation plus active dans le cadre légal et nos droits et devoirs en face de nos responsabilités citoyennes

Score : 0 / Réponses : 0

----

Cela ne peut aller que dans le bon sens, et pourquoi pas demander l'avis des citoyens sur les normes

Score : 0 / Réponses : 0

----

Tout à fait d’accord avec l’evaluation Des impacts des lois, ce qui est à la portée des citoyens quels qu’ils soient. Ce sont eux qui peuvent en mesurer les effets sur leur vie. Il revient ensuite aux élus de mesurer le bien fondé de tel effet sur la majorité de la population.

Score : 0 / Réponses : 0

----

La publication des contrôles déjà existants serait une première mesure pour informer les citoyens des évaluations mises en oeuvre par les parlementaires. La deuxième mesure importante est de mettre en place un outil numérique pour communiquer avec nos parlementaires afin d'alimenter leur connaissance de la mise en oeuvre des lois sur leur territoire.

Score : 0 / Réponses : 0

----

Je me demande si nos députés, nos représentants politiques, ont du oubliés que leurs devoirs être écoute leurs citoyens avoir un devoir de citoyenneté envers eux (vers tous les citoyens)

Score : 0 / Réponses : 0

----

Essayons d'instaurer un rapport de force  non violent et éclairé pour une démocratie plus humaine et moins technocratique , plus compassionnelle comme alternative à  la raison d'état,essayons  de faire comprendre que la maltraitance animale est un signe  précurseur de la maltraitance sur les  humains, oui essayons...!

Score : 0 / Réponses : 0

----

Du fait du désintérêt des élections et du désabusement général des français vis à vis des politiques, je suis de plus en plus convaincue qu'il soit nécessaire d'associer les citoyens au suivi des lois afin de réamorcer la vie politique

Score : 0 / Réponses : 0

----

Une loi n'est valable que si elle peut-être mise en application, non redondante ou pire contradictoire avec une précédente, que l'Etat veuille bien l'utilise et l'appliquer à tous sans discrimination, sans vouloir mettre en exergue tel franche de la population pour faire du populisme (on en a vu), que si elle est efficace et pour cela il est impératif de mesurer son efficacité afin de la maintenir ou de l'abroger.
Pour cela, je suis d'accord avec les idées et qui ont été émises par vous autres et qui nourrice extrêmement bien le sujet pour donner de véritables outils.
Je veux juste terminer que "cette participation à l'évaluation de la mise en œuvre des lois" et de leur vie est extrêmement importante et à la notre portée de nous tous citoyens. Je retiens également l'idée de Gérard Bodin sur l'exemple du Canada
Nous râlons de ces lois "dites stupides, inutiles,....... et bien dans ce sens, avec cette participation, mettons nous acteurs et non pas que râleurs et plus que simple sujet de la Nation.

Score : 0 / Réponses : 0

----

Une démocratie, c'est par le peuple...
Alors il faut utiliser la proximité et l'outil informatique.
Chaque citoyen devrait pouvoir poser LA question qui lui tient le plus à cœur sur un site de sa circonscription et visible par tous.
Si cette question obtient un certain % ( par exemple 5% des inscrits), alors le député prends le relais en invitant un groupe constitué de 10 citoyens afin de lancer une consultation dans sa circonscription.
Si cette dernière obtient un seuil à définir, alors elle est lancée dans la région puis si succès au niveau national.
Nous avons les idées, les outils  ALORS passons à l'action.

Score : 0 / Réponses : 0

----

Comme dans la Grèce antique et à l'image de ce qui a été fait en Irlande pour l'IVG, tirer au sort une centaine de citoyens, leur donner tous les renseignements, et leur demander de proposer des lois utiles à la nation, puis les laisser repartir à la maison au bout d'un temps défini (2 mois ?)

Score : 0 / Réponses : 0

----

Arrêtons les promesses de campagne démagogiques ou irréalistes, rendons les promesses de campagne pénalisantes en cas de non respect de la parole donnée, ses promesses ayant permis à l'élu d'accéder à son mandat. Un comité proche du conseil constitutionnel devrait statuer sur la faisabilité des promesses des élus afin d'aider les citoyens à croire en leurs élus et à évincer ceux qui promettent l'irréalisable.. Les promesses réalisables qui ne seront jamais soumises aux votes ou encore moins débattues devraient conduire à des sanctions, que ce soient financières, disciplinaire aboutissant à l'éventuel renvoi de l'élu, ou civiques avec l'interdiction de nouveaux mandats politiques. Les élus parviennent à leurs fonctions en promettant un tas de choses aux citoyens, ils représentent ses promesses, que les promesses soient réalisables au moins théoriquement et que l'élu soit contraint se se battre pour ses promesses. Ce devrait être généralisé aux élections présidentielles, législatives ou locales.

Score : 0 / Réponses : 2

----

Il existe aujourd'hui des méthodes très rodées d'évaluation des politiques publiques qui s'appuient sur la mesure des résultats obtenus en fonction des objectifs et des indicateurs fixés en amont. 
Le plus important est de bien se mettre d'accord sur la méthode au départ, pour qu'ils n'y ait pas de polémique au moment de l'évaluation, quelques années plus tard. 
Pour ce qui est de la participation citoyenne à cette évaluation, il serait très intéressant de prévoir la mise en place de groupes miroir, composés de citoyens ciblés par la loi. 
Par exemple, si c'est le projet de loi "orientation et réussite étudiante" : http://www.education.gouv.fr/cid122039/plan-etudiants-accompagner-chacun-vers-la-reussite.html
Plusieurs groupes miroirs pourront être constitués sur base du volontariat,  par les députés, dans chaque circonscription, pour une évaluation à mi mandat :
groupe miroir étudiants bac général
 groupe miroir étudiants bac pro, 
 groupe miroir prof d'université, 
 groupe miroir profs du secondaire,  
groupe miroir conseillers d'orientation, 
 groupe miroir employeurs...
groupe miroir parents d'élèves...

Sur la base des remontés par groupe miroir,  structurées autour d'un guide qui serait le même pour tous les députés, chacun pourrait ensuite disposer d'une mesure de l'efficacité de la loi avec des propositions de mesures correctives, à synthétiser au niveau national pour ensuite faire l'objet d'une nouvelle concertation entre députés. 
Les résultats pourront ensuite être diffusés sur la plateforme de l'assemblée nationale,   avec éventuellement un projet de  correction de la loi si tel est le diagnostic.

Score : 0 / Réponses : 1

----

D'accord

Score : 0 / Réponses : 0

----

D'accord

Score : 0 / Réponses : 0

----

Je soutiens !

Score : 0 / Réponses : 0

----

faudrait faire des consultations numériques au niveau locale (plateforme sur les sites des mairies, départements, régions), puis ensuite les conseils municipales, départementales et/ou régionales les regrouperont à un niveau par circonscription de faire parvenir les résultats, les avis via les députés/sénateurs au niveau nationales (parlement)

Score : 0 / Réponses : 0

----

En accord avec ce qu'avait proposé Ségolène Royal, je pense qu'à chaque fois qu'il y a un projet de loi, il devrait y avoir une révision des lois précédentes, afin que les lois ne se contredisent pas et alourdissent les volumes des lois, ce qui fait le "bonheur" des avocats...

Score : 0 / Réponses : 0

----

Sarah Dano : merci je ne connaissais pas cette pratique (j'étais enfant du temps du franc) et je trouve que cela a le mérite de donner du sens et de rappeler les fondamentaux. Pour ce qui est des incertitudes liées aux lois de finance, un budget pluriannuel va dans de donner plus de visibilité , comme cela se passe d'ailleurs dans les entreprises qui font de la programmation pluriannuelle.

Score : 0 / Réponses : 0

----

Et une EMISSION TV interactive ?

Bonjour ayant travaillé de nombreuses années dans le milieu de la TV, il me semble que l'outil internet devrait être associé à une émission de télévision intéractive sur la chaîne parlementaire et une web TV propre afin que les projets soient travaillé et soumis en amont de manière globale : Internet + TV afin que tout le monde puisse interagir même celles et ceux qui ne sont pas  l'aise avec le numérique ou n'y ont pas accès.

La construction s'articulerait sur la base d'internet et accompagnée en support explicatif et constructif via l'émission.

je pourrai développer le concept sans souci ICI ou directement.

Score : 0 / Réponses : 0

----

Dans l'entreprise où je travaille nous avons mis en place une score card mensuelle comprenant un certains nombre d'indicateurs à suivre selon les services. Cela permet d'identifier les écarts aux objectifs fixés. Nous pourrions envisager un outils similaire reprenant les objectifs attendu du gouvernement sur la loi concernée et des indicateurs à suivre afin de mesurer la bonne application/performance de la loi. Cette publication se ferait par semestre et serait mise en ligne afin de permettre aux citoyens d'avoir un avis éclairé sur les lois et d'orienter ainsi leurs questions au gouvernement. Un panel de citoyen pourrait également être sélectionné afin de participer a la construction de ces indicateurs.

Score : 0 / Réponses : 0

----

Utiliser un Sénat profondément réformé comme moyen d'évaluer la mise en oeuvre des lois.

Il faut transformer le Sénat pour en faire une assemblée véritablement représentative de toutes les composantes de la société française. Une assemblée qui serait complémentaire de l’Assemblée Nationale (dont les députés semblent parfois déconnectés de la vie réelle). Les sénateurs seraient ,pour moitié, de “simples” citoyens tirés au sort et représentant l’ensemble des catégories socio-professionnelles. Un quart serait constitué d’élus locaux, tirés également au sort. Le dernier quart serait formé de représentants syndicaux (salariés et patronat).

Les nouveaux sénateurs, profondément ancrés dans la réalité du terrain, pourraient facilement faire remonter les effets positifs ou négatifs d'une loi récemment appliquée.

Score : 0 / Réponses : 0

----

Il faudrait d'un coté permettre les sudgestions pour des projets de lois de la part des citoyens.
Puis moyen d'apporter des éléments supplémentaires afin de correspondre au terrain.
Et enfin une vérification avec des cas pratiques pour la faisabilité en plus des discutions dans l'hémicycle 
Pour finir par une adoption par les représentants du peuple.
Si nous voulions être complets il faudrait une commission de surveillance après la mise en application de la loi afin d'être sur qu'elle soit cohérente au besoin de la société.
Autre point majeur mais inapplicable en raison de la volonté dictatoriale du système, ce serais d'avoir un vrai conseil  constitutionnel qui vérifie que la loi soit bien en accord avec notre constitution.
Bien rêveur ce système avec une assemblé d'élus représentant à peine 20% de la population. 
Je reconnais une bonne initiative et l'approuve pleinement en espérant que ce ne soit pas encore de la poudre de perlimpinpin.
Cdt.

Score : 0 / Réponses : 0

----

Dès lors qu’une loi engendre de nombreux échanges au parlement, dans les médias…, nous proposons de mesurer la mise en œuvre de celle-ci par un questionnaire vers un panel de citoyens.

Le panel répondra à une cible en fonction des mesures engendrées par la loi.
Exemple : 
•	mesure sur le travail → cible : des actifs (salariés, artisans, profession libérales, patrons de TPE/PME…) 
•	mesures sur les retraites → cible : des séniors (plus de 50 ans)
•	mesures sur les associations → cible : des bureaux d’associations pour diffusion à leurs membres.
L’adressage du questionnaire devra être à la fois papier et numérique afin d’adresser TOUS les citoyens.

Les question seront très précises, sur un modèle QCM (Questionnaire à Choix Multiples) afin de permettre un traitement automatisé et rapide, mais également pour inciter les citoyens  le temps à y participer.

Score : 0 / Réponses : 0

----

Quand on regarde ce qu`il se passe  lors des reunions parlementaires , des propositions  intelligentes de l opposition sont rejetees ,ignorees , laminees par le groupe presidentiel .Or ce Groupe Presidentiel ne represente que au mieux 25% de la population francaise . Un pareil desequilibre n`est pas acceptable en democratie .Je suggere donc que les lois en cours de discussion passent devant une commission paritaire Opposition / Majorite et que la desision d`accepter ou refuser la loi en discussion soit confiee a un Jury d`honneur de 3 juges. independants du Pouvoir en Place  Dans cet ordre d`idee je propose de supprimer le Senat .

Score : 0 / Réponses : 0

----

Les entreprises passent au mode "agile", pourquoi pas la fabrique des lois? Au lieu d'avoir des processus longs et des délais de mise en application et d'évaluation  longs, il vaudrait mieux définir les grandes thématiques, faire évoluer par petits pas et affiner par expérimentations ou aller retour avec des panels (en "bordant" le lobbying ...). Il faut ensuite exiger que les "abus" decoulant de la loi soient systématiquement remontés. 
Cela permet :
1/ de viser plus juste dans les détails des lois / amendements
2/ d'éviter des blocages et psychodrames sur telle ou telle loi (on avance par petits pas et on évalue les résultats ! )
3/ d'avancer concrètement, pas à pas, mais dans la durée pour chaque thématique.
Sinon on reste dans un mode "tunnel", beaucoup de temps pour modifier une loi, la faire appliquer, et avoir une idée de ses effets.

Score : 0 / Réponses : 0

----

Le but de ma proposition est d`obliger a une veritable discussion , echange d`arguments echappant a une ideologie de groupe , que le Bien Public soit seul pris en compte

Score : 0 / Réponses : 0

----

Je suggere une limite dans le temps tres stricte pour l etude de cette proposition de Loi .Et que la LOi votee ainsi puisse etre retoquee ou annulee si aparaissent des effets indesirables  non aparents lors de la decision de son application .

Score : 0 / Réponses : 0

----

L'évaluation des lois? Mais jamais les politiques ne s'en occupent! Il faut à chaque fois réformer (en rappelant à quel point c'est difficile de le faire en france) sans faire le bilan des précédentes lois ou des besoins sur le terrain (il y a pourtant toujours du positif à garder voir à développer et des points à améliorer). L'évaluation devrait pourtant être à la base de projet
Les citoyens pourraient facilement être associé à cette étape avec des indicateurs précis (qui doivent être déterminés en même temps que le projet de loi)

Score : 0 / Réponses : 0

----

Que dire des lois ? Au quotidien, des lois simples comme le code de la route, celles qui régissent le bruit, le respect des autres, les animaux de compagnie, etc. Ne sont pas appliquées. Commençons par la peut être ??????

Score : 0 / Réponses : 0

----

L'évaluation de la loi pourrait se baser sur des indicateurs eux-mêmes liés aux aspects formels du texte législatif (compréhension du texte, connaissance du vote et de sa genèse, etc) et aux aspects pratiques (application impersonnelle et égalitaire du texte/ cas isolés auxquels la loi ne répond pas, accès aux statistiques officielles concernant son application, commission d'évaluation citoyenne volontaire, etc).

Score : 0 / Réponses : 0

----

- Proposition de Renaissance Numérique et la Fondation Jean Jaurès

Évaluation et « clause de revoyure » des lois 
- Toutes les lois doivent comporter, dans leur rédaction, des critères d’évaluation qui permettront de déterminer de leur bonne mise en œuvre, avec des objectifs à atteindre dans un calendrier donné. 
- Pour évaluer l’application de chaque loi et de ses effets, une commission mixte élus/citoyens (panel de citoyens tirés au sort selon des critères de représentativité) est mise en place, qui établit un bilan des lois appliquées à un an, cinq ans et dix ans après la promulgation des décrets d’application. Leur rapport d’évaluation est public, accessible en ligne en format ouvert et débattu au Parlement. 
- Une « clause de revoyure » pourrait être recommandée par la commission mixte.

[Proposition issue du Livre blanc “Démocratie : Le réenchantement numérique” : http://bit.ly/2zAPucV]

Score : 0 / Réponses : 0

----

Gérard GREBERT, citoyen « Lambda »
 Une « idée » concernant : La Participation des citoyens aux travaux de l’Assemblée Nationale
-	Au-delà  des outils de communication numériques et des circuits locaux (comités, groupes de travail….) ouvrant la possibilité à tout citoyen  d’émettre des avis et propositions à l’Assemblée, une participation « active et physique » de citoyens « Lambda »  est souhaitée dans tous les travaux de l’Assemblée (commissions, groupes de travail, voir … ?  études d’impact précédant la mise en place des lois… ?).
-	Dispositif :
Pour ce faire, l’ensemble des élus de l’Assemblée pourrait disposer d’un « VIVIER de Citoyens » motivés et volontaires dans lequel, lors du déclenchement de chaque étude ou concertation, quelques citoyens  pourraient être invités à participer.
-	Création du vivier :
Chaque citoyen pourrait s’inscrire en se positionnant sur 2 ou 3 thèmes pour lesquels il se
sent concerné ou particulièrement positionné pour apporter son expérience ou son expertise. Ces thèmes seraient à choisir parmi une liste préétablie de 25 ou 30 thèmes pouvant faire l’objet de travaux de groupes de l’Assemblée Nationale, plus, éventuellement un pré-positionnement sur un thème libre non listé.
               Lancement du projet :    Afin que l’ouverture soit maximale :
-	Présentation du projet à l’ensemble des Députés afin que toutes les tendances politiques soient impliquées. 
-	Information, par leurs circuits propres, par les partis, les mouvements, les associations …. auprès de leurs adhérents et sympathisants.
-	Messages d’information via les médias. Messages très courts (mais diffusés en boucle) à
des heures de grande écoute ( 19H55 ?) sur les chaines d’information télévisées (TF1, France2, France3, BFMTV, LCI ….) invitant les citoyens à s’inscrire dans le fichier de façon très simple : Coordonnées Thèmes choisis .(1 mini à 3 thèmes de la liste + 1 libre facultatif).
          Activation du Vivier : »
           Dés lors, ce Vivier constitué (les inscriptions restant valables 2 ans) et restant ouvert (pas de date limite, afin que le vivier reste « vivant » et que tout citoyen puisse s’inscrire quand il le souhaite), les comités, commissions, groupes de travail de l’Assemblée pourront puiser le nombre souhaité de participants « citoyens Lambda » à leurs travaux. Pour ce faire, dans un premier temps, le comité procédera au tirage au sort de 6 régions. Puis à l’intérieur de ces régions tirera au sort 6 citoyens parmi ceux qui se seront positionnés sur le thème étudié. (Prévoir des suppléants en cas de désistement des citoyens retenus ou de leur « éviction » ?? suite à une vérification éventuelle des capacités citoyennes et de la bonne moralité des personnes retenues  ?  Casier judiciaire ?..). Par ailleurs, rechercher la parité Hommes/Femmes (par exemple en retenant les 3 premières régions pour le tirage au sort des citoyennes et les 3 autres pour les citoyens….). 
           Les citoyens retenus seront contactés et confirmeront ou infirmeront leur volonté de participer aux travaux concernés.
           Rôle des citoyens cooptés : 
-	Participation aux travaux de la commission les ayant retenus.
-	Intervention sur le terrain afin de remonter un maximum de renseignement sur l’avis des citoyens quand une question se pose à la commission et que l’avis des citoyens « de base » est souhaitable (intervention directe auprès des citoyens, des associations, des élus, des acteurs de la vie économique… ; pourquoi pas la réalisation de mini sondages ou mini referendums auprès des citoyens de la région ?....selon les besoins de la commission).
-	Compte rendu à la commission des interventions sur le terrain.
-	Retour à la »base » de l’avancée des travaux de la commission.
-	Participation au Rapport final des travaux de la commission et diffusion en région ??.....
 NOTA : Voir l’intérêt …et la Constitutionnalité  d’un tel dispositif ?

Score : 0 / Réponses : 0

----

DGF : Ne serait il pas envisageable que le nombre d habitants ne soit pas le seul critère de calcul de la DGF ? Mais également la situation géographique ? Les problématique particulières ? (Dépollution ?) Le revenu moyen par habitants ? La pression démographique ? ...

Score : 0 / Réponses : 0

----

Un citoyen doit pouvoir questionner le gouvernement:  
La répartition des agendas est figée dans le marbre semble-t-il: Une semaine sur quatre est dédiée au contrôle des actions du gouvernement. De la même manière que chaque groupe parlementaire peut poser ses questions aux gouvernements, je propose que les citoyens en fassent autant via un groupe parlementaire virtuel :  

Cinq questions tirées au sort ou votées par les citoyens (mode formule 1 ou Eurovision) sont posées par le président de l’Assemblée Nationale. Les réponses sont bien sûr notées au compte rendu.  Un citoyen ne peut évidemment pas proposer plusieurs questions pour la même séance plénière.

Score : 0 / Réponses : 0

----

Contribution Collective des comités :Zola, Richelieu , Centre 

Comment Participer ? 

  Utiliser le numérique .
  Faire des enquêtes, sondages sur l'efficacité de la loi .
  Comparer avec d'autres pays  sur la loi existante (ou non ).

 A quel stade intervenir ? 

  Pendant : possibilité amélioration .
 Après la mise en oeuvre  : possibilité de révision de la loi .

Pouvoir intervenir dans l'évaluation ?

 Nous sommes concernés à titre personnel, collectif , corporatif (risque conflit d'intérêt )
  Légitimité du citoyen à intervenir sans être un expert .
 Préconiser un dispositif, un protocole d'évaluation , l'évaluation doit être quantifiée .

Autres Idées :
Promouvoir une éducation citoyenne :formations en lignes , réunions, 
groupes de discussions .

Score : 0 / Réponses : 0

----

La mise en place d’un logiciel qui permettrait avant le vote d’une loi, que les citoyens puissent donner leur avis, opinion et surtout leurs revendications; qui après les députés et toutes la sphère politique et gouvernementale puissent en prendre compte. 
Cette structure numérique pourra aux citoyens de se faire entendre.

Score : 0 / Réponses : 0

----

Proposer, débattre, amender, constitue le cœur de notre démocratie parlementaire. Cependant, malgré un effort certain ces dernières années, [l'application des lois](https://www.senat.fr/notice-rapport/2016/r16-677-notice.html) reste en deçà de l'énergie que nos parlementaires y mettent et frustre l'attente des citoyens quant l'efficacité de ses représentants. 

Est-il raisonnable qu'il faille plus de 6 mois en moyenne entre le vote d'une loi et sa mise en application ? La confiance votée à l'exécutif qui, d'après le [rapport du Sénat](https://www.senat.fr/rap/r16-677/r16-6771.pdf), omet de fournir 40% des rapports demandés par le Parlement ne doit elle pas être remise en cause sur ce critère ? 

Pour renouer avec la confiance entre les citoyens et les gouvernants au sens large, il semble impératif d'être exemplaire sur l'applicabilité de la loi votée par les représentants du peuple.

Score : 0 / Réponses : 0

----

Je suis tout à fait de votre avis, souvent nos représentants,ne nous représentent pas (!!!!!) comme nous le voudrions.

Score : 0 / Réponses : 0

----

Systématiquement un travail de rencontre et de dialogue sur le terrain doit être mis en place pour évaluer l'impact d'une loi sur les personnes qui en dépendent. Ce type de retour pourrait permettre de réaliser que quelques lignes votées en assemblée peuvent faire basculer la vie de certains, dans un sens comme dans l'autre.

Score : 0 / Réponses : 0

----

Prévoir systématiquement des groupes de citoyens directement concernés par la loi a évaluer et travaillant à son évaluation avec des pairs. Ils croiseraient leur travail d’analyse avec celui d’experts / chercheurs pour produire une évaluation plus complète de la loi.

Score : 0 / Réponses : 0

----

il serait intéressant de proposer 'un rythme' d'évaluation de la loi, annuel par exemple.
Pendant une année les citoyens pourraient faire remonter leurs observations sur un site dédié

Score : 0 / Réponses : 0

----

Pour faire remonter les lois , il faut une plateforme avec l'avancement de chaque loi . Pour le retour de cette loi , il faut prendre une catégorie de citoyens , et leur prépare des questions ou même les faire participer à l'Assemblée nationale .

Score : 0 / Réponses : 0

----

Souvent, les évaluations se font sous la forme de questionnaires. Il est plus informant de la faire sous la forme de débats numériques portant sur des récits de cas d'usage. Ainsi les défauts associés à leur mise en oeuvre n'apparaîtraient pas simplement  comme des faits divers embarrassant.

Score : 0 / Réponses : 0

----

Il s'agirait de mettre en place un guichet unique en matière de suivi des objectifs, des moyens d'évaluation (indicateurs qualitatifs et quantitatifs) et du niveau d'application de chaque loi (via le taux de publication des décrets d'application), pour permettre à l'ensemble des citoyens de participer en connaissance de cause à l'évaluation des lois. Ce "tableau de bord" serait aussi le moyen de mettre en lumière les rapports et études écrits en lien avec la loi en question. 

Au sujet des indicateurs qualitatifs d'une loi, il serait intéressant à terme de mêler les citoyens à l'évaluation des lois par ce biais, en recueillant leur ressenti, leur expérience face à ces nouvelles dispositions législatives. 

Il faudrait aussi, selon l'ensemble des participants à notre atelier, éviter au mieux le mille-feuille législatif, en augmentant le nombre de dispositions législatives au sein de nos nouvelles lois qui abrogent d'autres dispositions, et en mettant fin à la sur-transposition des directives européennes. . 

Enfin, il ressort très nettement qu'il y a une nécessité de simplifier voire de vulgariser le contenu des rapports d'évaluation, quels qu'ils soient, pour les rendre plus intelligibles et accessibles. 

> Contribution à l'atelier citoyen organisé par Olivier Véran, Député de la 1ère circonscription de l'Isère, le vendredi 3 novembre 2017

Score : 0 / Réponses : 0

----

Restitution Atelier Citoyen au LLL du 2/11

Proposition #3: Les lieux pour favoriser la participation 

Divers lieux institutionnels de proximité existent pour organiser la participation des citoyens au débat  démocratique : les conseils de quartiers  pour les habitants, les conseils citoyens pour les quartiers prioritaires ou le Comité d’information et de concertation des associations (CICA). Mais le constat est toujours le même : ce sont toujours les mêmes personnes motivées qui viennent et le débat ne s'élargit pas.

D'un  autre côté de nombreux lieux du quotidien marquent le rythme de nos  journées. Ces cafés, ces gares ou ces places, sont des lieux déjà  investis par les citoyens. Afin de désacraliser le rapport des citoyens  vis à vis de l'Assemblée Nationale il faut créer des passerelles entre  ces espaces, faciliter la circulation des idées et de leurs porteurs. En  revanche sur la place publique il manque souvent les informations et  les documents nécessaires au bon déroulement des débats entre citoyens.

Nous  proposons que le travail parlementaire qui va suivre la consultation  s'intéresse à la création d'un dispositif (cartographie et label) qui  vise au moins deux objectifs :
1 - Renforcer les lieux de participation en présentiel.
2 - Faciliter la prise en compte par l’Assemblée nationale de ce qui se déroule sur les territoires.

Cette  cartographie peut s’appuie sur un référencement collaboratif des lieux  qui accueillent la participation citoyenne dans leur diversité pour  qu’ils soient inclusifs et accessibles à tous (cafés, institutions,  tiers lieux, espaces de médiation numérique, locaux associatifs, lieux  de travail, …).  Les évènements qui s'y déroulent sont également  référencés (débats, ateliers, formations, …).  Le label vient soutenir  des acteurs locaux qui souhaitent s'engager dans une telle démarche.

Score : 0 / Réponses : 0

----

Restitution Atelier Citoyen au LLL du 2/11

Proposition #4: Favoriser l'intelligence collective entre les acteurs territoriaux

Il  existe une multitudes d'acteurs institutionnels, territoriaux ou  associatifs qui font vivre la démocratie locale à l'échelle parfois d'un  Département ou d'une Région. Mais les démarches de maillage territorial  comme le développement de partenariats constructifs et efficients  prennent du temps. 

Afin  de réduire le temps de développement de nouveaux réseaux d'acteurs, il  faut organiser des ateliers à destination des associations et des agents  territoriaux afin de développer les usages collaboratifs en ligne. Ces  usages doivent servir à organiser les échanges autour de sujets couvrant  différentes échelles territoriales, consolider les expériences menées et partager les bilans. 

Les travaux parlementaires qui vont suivre peuvent s'intéresser à la façon dont les Députés souhaitent proposer ou recevoir des propositions de débats et trouver les financements nécessaires à la mise en place d'un tel projet.

Score : 0 / Réponses : 0

----

Une mise en place de referendum facultatifs où suite à la publication d'un texte de loi ou d'un traité international, un comité référendaire pourrait récolter en x jours xx xxx signatures pour obtenir facilement l'avis du peuple. 
Seule la majorité du peuple est requise, texte accepté = texte en vigueur.

Score : -1 / Réponses : 0

----

cela permettrait d'avoir des lois plus en accord avec la réalité des Français

Score : -1 / Réponses : 0

----

Moins de lois bavardes et incompréhensibles et plus de contrôle de l’action gouvernementale, y compris par voie de consultation et d débat public avec les citoyens

Score : -1 / Réponses : 0

----

Pondre des lois c'est bien mais les faire respecter par nos élu(e)s c'est une autre paire de manche. Ils ou elles ont tellement de choses à cacher. PAS NOUS.

Score : -1 / Réponses : 0

----

les gens doivent interpeller l’exécutif par le biais d'une antenne rattaché a la présidence ou au 1er ministre  qui doit être dédier a la vie quotidienne  des gens et des injustices et violence faite aux gens pour permettre de corriger les dérives des lois  et les souffrances fait au gens exemple les frais exceptionnels a des prix exorbitant et non encadré en plus de la pension alimentaire  décidé par un ou une juge des affaires familiale sans l'accord de l'autre parent débiteur laisse le parent débiteur en surendettement et voir en exclusion

Score : -1 / Réponses : 0

----

La lecture et la compréhension des lois faisant appel à des connaissances juridiques et à un registre de vocabulaire technique , il serait intéressant que des relais citoyens puissent en vulgariser le contenu et grâce à un maillage territorial d'individus, les expliquer en réunion publique, aux élus locaux et aux citoyens du territoire de proximité.

Score : -1 / Réponses : 0

----

C'est le moment de rappeler un fait connu : c'est très simple de compliquer les choses, par exemple en rajoutant une loi sans prendre la peine de vérifier sa compatibilité avec ce qui existe, ou la possibilité de son application,ou la possibilité d'effets pervers . C'est au contraire très compliqué de simplifier .
Nous sommes dans une période d'inflation législative car le gouvernement répond à chaque problème par une loi sans trop se préoccuper de son application.

Score : -1 / Réponses : 0

----

Favoriser la facilité à comprendre un texte de loi qui est très souvent compliqué pour la plupart des citoyens.

Score : -1 / Réponses : 0

----

Bonjour,

Certaines villes permettent, via l'application mobile municipale, de prendre une photo d'un endroit dégradé, d'une situation, etc... puis de l'envoyer immédiatement à un service de la Mairie (la voirie par exemple).

Ce type de remontée spontanée permettrait de mesurer les infractions à une loi (en nombre de photos envoyées), et encouragerait une réactivité des services publics concernés pour résoudre les incidents.

Score : -1 / Réponses : 0

----

Bonjour,

Une idée serait de rendre consultable en ligne l'intégralité des lois (du code civil ou autre), et de permettre sur le même site web d'alerter sur une infraction par rapport à telle loi, avec commentaire et photo à l'appui.

Les citoyens internautes disposeraient alors des règles & d'un point de contact avec les services publics, sur un même espace web.

Score : -1 / Réponses : 0

----

il va falloir  abroger toutes les lois qui ne servent plus a rien et qui sont dépassé. (trop de lois tue les lois )

Score : -1 / Réponses : 0

----

sur toutes les lois mise en place, il y a aucune étude qui valorise leur mise application !!!  crée des lois d'accord mais il vaudrait mieux en crée moins mais qu'elle soit appliquer !!!

Score : -1 / Réponses : 0

----

certaine loi sont  influencé par le lobbying de certain, demandons que le lobbying ne soit  plus déterminants et que prime l'intérêt général que personnel (comme on a vue par le passé )

Score : -1 / Réponses : 0

----

que les experts soit dans la loi interdit de siéger au sein de grosse multi nationale !!! ou salarier !!

Score : -1 / Réponses : 0

----

sa serait bien que l on puisse pouvoir faire un suivi de l application des lois sur nos territoires et pouvoir expliqué  avec des thermes pas trop technique ces lois

Score : -1 / Réponses : 0

----

Chaque citoyen devrait pouvoir interagir sur une plateforme dédiée répertoriée par LOI pour faire remonter les dysfonctionnements éventuels dans la mise en oeuvre de celles-ci - Cela permettrait au législateur d'avoir, sur une durée définie à l'avance, un retour sur sa mise en oeuvre et par la même une évaluation "à chaud" de chaque LOI. Comme un "projet" une phase test devrait être déterminée (3 à 6 mois par ex) pour évaluer la mise en oeuvre. la LOI doit elle aussi s'adapter à notre temps et trouver de nouvelles façons de mise en oeuvre...

Score : -1 / Réponses : 0

----

En matière d'évaluation : les études d'impacts devraient être connues et diffusées auprès des citoyens, via une plateforme numérique, (avec possibilité d'accès au sein des services des municipalités pour les citoyens n'ayant pas accès à internet pour des raisons diverses).  Une date d'échéance pour l'évaluation, y compris des décrets associés aux textes, et les modalités correspondantes à cette évaluation seraient un plus.

Score : -1 / Réponses : 2

----

Impôt et consultation citoyenne annuelle.
Les impôts financent les dépenses de l’état.
On pourrait imaginer que les citoyens évaluent les dépenses publiques et s’intéressent à l’impôt dans une démarche pro-active.
Ainsi toute déclaration annuelle d’impôt (ou de non imposition) pourrait, avant être validée, intégrer une étape dite « d’évaluation et de prospective ».
Un questionnaire simple (3 questions à choix multiples pourrait être proposé aux contribuables, par exemple :
1-	Classez les 3 (3, 4 ou 5, à définir) lois adoptées  au cours de l’année précédent selon la pertinence que vous leur reconnaissez. (Suivent 3, 4 ou 5 mesures adoptées par le Parlement)
2-	Pour la mesure que vous avez placée en N°, quel est, selon vous, l’axe qui pourrait être encore amélioré ?
3-	Classez les projets (ou propositions) de loi suivants selon le degré de priorité que vous leur accordez. (Suivent 3, 4 ou 5 mesures envisagées par le gouvernement ou le parlement, une case « autre proposition » permettrait de faire éventuellement remonter des priorités largement partagées)

Score : -1 / Réponses : 0

----

Il ne devrait pas être possible de proposer et à fortiori de voter une nouvelle loi sans que celles déjà adoptées sur le même objet n’ai fait l’objet d’une véritable évaluation, de mise en œuvre de moyens adéquats (je prends en exemple les lois portant sur la sécurité). Trop de lois sont votées pour des raisons dogmatiques.

Score : -1 / Réponses : 0

----

Qu il y est un écran à l assemblée ou l on puissent voir  les suggestions en directe des citoyens qui sont tirés au sort pour donner leur avis sur cette loi et sur ses avancées.

Score : -1 / Réponses : 0

----

Lorsqu'on prépare une loi (projet ou proposition) la/les commission(s) auditionné(nt) des experts ou des représentants de la société civile
1 an après le vote d'une loi on devrait entendre à nouveau des personnes qualifiées pour voir l impact et les ajustements nécessaires (voir les décrets manquants)
On devra it également auditionné ceux chargé du contrôle du respect de la loi (quand il y a des sanctions pénales ou administratives)  tels que , policier, autorité administrative pour qu'ils apportent également leurs éclairages

Score : -1 / Réponses : 0

----

Mise en place d'outils simples et accessibles à tous (méthode de management par objectifs) :
a) Suivi du calendrier de mise en place de la loi : publication des décrets, déploiement avec mise à jour tous les mois
b) Mise en place d'une gouvernance de déploiement en mode projet (avec 1 chef de projet identifié)
c) Contrôle par rapport impacts économiques simulés en amont 
d) Etablissement d'un bilan annuel associé d'un plan d'actions

Score : -1 / Réponses : 0

----

L’évaluation d’une loi devrait se réaliser par la mise à disposition des citoyens d’un questionnaire ouvert sur l’expérience qu’ils en ont eu. Une brève présentation du contexte et la validation des critères d’application de la loi (pour permettre une analyse des réponses au questionnaire) constituerait la première partie.
Vient ensuite la question des avantages qu’ils en ont retiré, les difficultés éventuelles à en bénéficier ou à les mettre en œuvre, les suggestions d’amélioration qu’ils souhaitent.

Score : -1 / Réponses : 0

----

Et si on interdisait simplement de présenter une  proposition de loi qui n'a pas été évaluée du point de vue financier, social et écologique?
Et si on interdisait de promulguer des lois sans date limite de revision (par exemple 2 ans ou 3 ans), au terme desquels la loi serait représentée devant l'assemblée avec un bilan de résultats ?

Score : -1 / Réponses : 0

----

1 Expliquer pour lutter contre l'indifférence . Si on passe devant 'La tireuse de cartes ' de Lucas de Leyde au Louvre , sans médiatisation, sans accroche , sans débat, sans éclairage sur la vie artistique , rien ne se passe .
2 Nous avons de bons 'produits' mais ils ne peuvent pas vivre si l'on ne tient pas compte des réactions des citoyens ...comme dans le marketing , il faut intégrer au processus délibératif les réactions citoyennes 
3 Comme la ville et la campagne ne font qu'un, élus et citoyens constituent un ensemble non dissociable . C'est le peuple de France . 
4 Parvenir à ce que chacun se reconnaisse dans l'Assemblée , ses usages, ses décisions , ses hésitations ;;sa vie !

Score : -1 / Réponses : 0

----

Comment participer

Score : -1 / Réponses : 0

----

autre proposition : pourquoi ne pas imaginer qu'on fasse des lois à l'essai , en les testant pendant un laps de temps suffisant
Une loi testée a plus de chanceê ensuite vraiment appliquée.

Score : -1 / Réponses : 0

----

En début d'année 2018, la loi de finances récemment votée entrera en vigueur, exonérant une partie des citoyens du paiement de la TH. Je suis retraité et je ne bénéficie pas à ce titre de la réduction des 10% sur mes revenus imposables. Cette réduction est limitée à une certaine somme. Cette limitation pour quelques centaines d'euros me contraint à ne pas être éligible à l'exonération de la TH. Un véritable scandale car un autre Français ayant les mêmes revenus que moi mais bénéficiant de la réduction des 10% sera exonéré de la TH. Une loi ne peut traiter les citoyens que sur un plan d'égalité, sinon c'est inconstitutionnel, comme l'est par ailleurs le paiement de la TH qui va concerner en vérité  2 Français sur 10.

Score : -1 / Réponses : 0

----

Restitution Atelier Citoyen au LLL du 2/11 

Proposition #1: Financement de la participation présentielle 

Pour un vrai mécanisme de financement public des initiatives démocratiques.

L’écosystème   des initiatives démocratiques rassemblant les acteurs associatifs tout   comme des entreprises privées, souffre d’un manque de financement   chronique. Il n’y a pas en France de culture philanthropique privée  dans  ce domaine et les financements publics sont rares et assez opaques  dans  leur gestion. Dès lors, les acteurs des initiatives démocratiques ne peuvent aujourd’hui assumer  pleinement leur rôle  de reconnecter le citoyen avec le champ  politique. Cela pénalise  évidemment la bonne coordination et  collaboration des acteurs en créant  un climat de défiance et/ou  suspicion lié à l’origine de leurs  financements respectifs.

La   suppression de la réserve parlementaire libère des moyens, près de 150   millions d’euros par an, pouvant être affectés en partie à un  mécanisme  de financement des initiatives démocratiques. Tout comme il  existe un  mécanisme clair et transparent de financement des partis  politiques  s’appuyant sur le nombre de voix obtenus aux élections, il  est  nécessaire de mettre en place un un mécanisme clair et transparent de financement pour les acteurs du champs des initiatives démocratiques, acteurs incontournables aujourd’hui.

Les   ratios et critères pourront être identifiés et discutés lors de cette   consultation et du travail parlementaire qui suivra. Quelques pistes de   réflexions: ingénierie participative mélant numérique et physique,  nombre de votes sur les plateformes, nombre de participants aux réunions  et ateliers, qualité des contributions, nombres de signataires de pétitions etc...

Score : -1 / Réponses : 0

----

Restitution Atelier Citoyen au LLL du 2/11 

Proposition #2: Méthode participative physique et méthode participative numérique 

l    est acquis aujourd'hui que la participation physique comme la    participation numérique peuvent exister sous de multiples formes ( de   la  réunion classique à l'hologramme en passant par des plate-formes  en   ligne associées à des ateliers physiques). Elles sont le reflet  des   diversités des territoires sur lesquels ces méthodes sont  appliquées,   des publics concernés, des objectifs politiques visés et  par effet de   bord, des financements alloués.

Il    est également  acquis qu'une multitude de démarches participatives se  réalisent sur les territoires mais que ces dernières n'ont que peu de  connexions entre elles. De plus les porteurs de projets ne souhaitent    logiquement pas verser en bien commun les méthodes et les savoir-faire    qui les font vivre, sans garantie qu'ils pourront toujours en vivre    après la mise en commun.

En  débloquant les fonds nécessaires à  l'amélioration des méthodes  participatives physiques et / ou numériques  en France, il est important de développer une branche de l'ogptoolbox en partenariat  avec les acteurs  sociaux locaux , les associations et les citoyens ciblés afin de déterminer les objectifs et les fonctions que doit avoir cet outil numérique. Ex : faciliter l'expérimentation  de nouvelles  méthodes participatives hybrides par les acteurs d'un même réseau ou individuellement, pour alimenter ensuite le réseau par leurs restitutions.

Les  objectifs et les fonctionnalités pourront être identifiés et discutés  lors de cette  consultation et du travail parlementaire qui suivra.

Score : -1 / Réponses : 0

----

Utiliser les relais existants: 
Comités En Marche le plus proche de son domicile, 
Représentant En Marche départemental, 
Attaché parlementaire de la circonscription, 
Député de la circonscription qui transmet aux intéressés au ministère et à l'assemblée 
ET retour d'un ''accusé réception'' avec avis de prise en compte ou non.

Score : -2 / Réponses : 0

----

Il faut une plate-forme numérique où les différentes commissions de l’Assemblée nationale puissent se retrouver avec des commissions citoyennes sœurs afin d’établir ensemble le suivi des lois et leurs applications et si nécessaire leur abrogation

Score : -3 / Réponses : 0
