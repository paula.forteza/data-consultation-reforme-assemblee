## Quel rôle pour les citoyens dans l’élaboration et l’application de la loi ?

### Consultation pour une nouvelle Assemblée nationale

# Autres idées ?

<div><img src="https://consultation.democratie-numerique.assemblee-nationale.fr/an/idees.png"></div>
<div><span style="font-size: 16px;">Des idées ont pu nous échapper et ne pas rentrer dans les catégories ci-dessus. N'hésitez pas à les inscrire dans cette boîte à idées. </span></div>
<div><br></div>
<div><span style="font-size: 10pt; color: rgb(230, 0, 0);">Nota bene</span><span style="font-size: 10pt;">&nbsp;: par défaut, les contributions les plus populaires apparaissent en tête de la page par ordre décroissant. Vous pouvez si vous le souhaitez afficher les contributions les plus récentes ou les plus anciennes. Pour cela, modifiez le mode de </span><b style="font-size: 10pt;"><u>classement</u></b><span style="font-size: 10pt;"> à droite de «&nbsp;Commentaires&nbsp;» avant la 1ère contribution.</span></div>

#### Commentaires

Convocation d’une Assemblée Constituante Tirée Au Sort :
Parce que ce n’est pas aux hommes au pouvoir d’écrire les règles du pouvoir, nous voulons une Assemblée Constituante démocratique, donc tirée au sort.
  
Nous vous proposons une méthode pour réécrire la Constitution en évitant les conflits d’intérêts :
“Faire écrire une nouvelle Constitution par un échantillon mathématiquement représentatif de la population globale (env. 1500 citoyens tirés au sort)” 
Pour tirer au sort cette Assemblée Constituante, on peut imaginer plusieurs processus permettant l’écriture d’une Constitution soucieuse de l’intérêt général. 
  
(En savoir plus : https://le-democrate.fr/actas)

Score : 287 / Réponses : 34

----

J'aimerai que nos élus Nationaux et régionaux, comme le font déjà de nombreuses mairie, publient tous les mois un compte rendu de leurs activités, de leurs dépenses avec l'argent public et bien sur de leur vote à l’assemblé auprès de leurs électeurs. Cela me semble être bon pour la transparence.

Score : 139 / Réponses : 14

----

# Inscrire dans la loi l'obligation pour les institutions publiques de prioriser l'utilisation de logiciels libres et l'obligation pour les institutions publiques de produire des logiciels libres

Définition de logiciel libre : logiciel dont le code source est publié permettant à l'utilisateur de comprendre le code mais aussi de l'utiliser, le réutiliser et le modifier.

La liberté dans les logiciels est une question démocratique primordiale. Il s'agit pour une institution publique de ne pas obliger les citoyen.ne.s à utiliser un logiciel privatif, dont on ne connait pas les fonctionnalités, si par exemple il récupère des données personnelles. Les logiciels libres sont la seule solution pour garantir aux citoyen.ne.s une transparence nécessaire pour pouvoir utiliser le logiciel en toute connaissance de cause.

L'obligation de produire des logiciels libres pour les institutions publiques est une proposition de bon sens dans la lignée du "Argent public, code des logiciels public". Les logiciels produits par les institutions publiques, avec l'argent public, doivent être publiés pour que les citoyen.ne.s puissent les utiliser, les réutiliser et les modifier.

Score : 134 / Réponses : 50

----

# Adopter le jugement majoritaire comme mode de scrutin.

Depuis de nombreuses années l'abstention progresse à chaque élection. Un sentiment d'éloignement de la vie publique ou une remise en question de nos institutions démocratiques sont des raisons qui traduisent souvent un sentiment de **l'inutilité du vote**.

**Le mode de scrutin doit être remis en question**. De nombreuses études sont ou ont été menées pour essayer de créer un scrutin qui traduise le choix complexe de nos opinions tout en ayant un résultat qui soit représentatif de l'avis de la majorité de la population. **Le jugement majoritaire** est très souvent cité comme le système parfait pour cela (car sans paradoxe d'Arrow).
Depuis des décennies, nous sommes contraints par le choix d'un seul et unique candidat (ou groupe de candidat) alors que bien souvent, notre avis n'est pas aussi tranché ou autant catégorique que cela. Nos opinons ne sont pas binaires et il est contre-productif de les réduire à cela via nos modes de scrutins actuels. L'abstention est la résultante que choisissent beaucoup d'entre nous, mais cela n'exprime rien de concret et précis dans notre système démocratique.

Avec le jugement majoritaire, la démocratie sera renouvelée. Si vous pensez que tous les candidats proposés sont inadaptés à recevoir un mandat, vous pourrez le dire. Si vous pensez que un, deux, trois ou quatre candidats sont aptes à recevoir un mandat, vous pourrez le dire, et vous pouvez précisément indiquer vos opinons sur chaque candidats entre *Très bien, Bien, Assez bien, Passable, Insuffisant, A rejeter*.

Un bulletin d'exemple est disponible à cette adresse : https://parlement-et-citoyens.fr/projects/petition-legislative/collect/deposez-votre-petition/proposals/adopter-le-jugement-majoritaire-comme-mode-de-scrutin

Je vous laisse faire votre opinion sur cette proposition avec les différentes sources suivantes (liste non exhaustive) :
- (Vidéo) "Réformons l'élection présidentielle ! — Science étonnante #35", 21 octobre 2016,https://www.youtube.com/watch?v=ZoGH7d51bvc
- (Bande dessinée) "Vous reprendrez bien un peu de démocratie ?" Marjolaine Leray, consulté le 22 septembre 2017, https://lechoixcommun.fr/articles/Vous_reprendrez_bien_un_peu_de_democratie-1.html
- (Article) "Le Jugement Majoritaire", consulté le 22 septembre 2017, https://www.lechoixcommun.fr/articles/Le_Jugement_Majoritaire.html
- (Etude scientifique) "Jugement Majoritaire vs. Vote Majoritaire", Michel Balinski et Rida Laraki (CNRS, Ecole Polytechnique), 3 décembre 2012, https://halshs.archives-ouvertes.fr/hal-00760250/document
- (Site web) jugementmajoritaire2017.com, consulté le 22 septembre 2017, https://www.jugementmajoritaire2017.com/ 
- (Page Wikipédia) "Jugement majoritaire", consulté le 22 septembre 2017, https://fr.wikipedia.org/wiki/Jugement_majoritaire

Score : 103 / Réponses : 53

----

Avant de chercher à renforcer la participation des citoyens, on devrait commencer par se demander comment améliorer la participation des élus eux-mêmes. Je suis en effet particulièrement déçu de constater que, sur les huit députés qui se sont portés volontaires pour être membre de ce groupe de travail « Démocratie numérique », seuls trois ou quatre prennent la peine d'être présents à chaque réunion. C'est insultant vis-à-vis des experts qui eux font cet effort de venir contribuer. Apparemment, un député a trop d'obligations pour honorer tous ses devoirs. Je fais donc cette proposition de bon sens : **qu'un député ait la possibilité de se faire représenter par le collaborateur de son choix**, dans l'hémicycle, en commission ou ailleurs. Que les députés montrent l'exemple en utilisant les moyens qu'on leur alloue pour assurer une continuité de leur fonction, c'est un préalable pour rétablir la confiance des citoyens.

Score : 73 / Réponses : 11

----

Une réforme me paraît essentielle pour prendre en considération le silence de certains électeurs et électrices.
A chaque élection, il est constaté un nombre important d'abstentions et de votes blanc. Cela ne gêne personne, l'essentiel étant d'être élu.e.
Je pense, qu'aujourd'hui, le vote blanc doit être effectivement reconnu et apprécié comme un suffrage exprimé. Une personne qui se déplace pour exprimer son désaccord me paraît devoir être considérée.
Cette reconnaissance effective du vote blanc doit, de ce fait, conduire à reconsidérer une élection si son pourcentage atteint un seuil à définir, et d'autant plus s'il est majoritaire.
Je ne crois pas que la démocratie souffrirait d'une telle mesure.

Score : 64 / Réponses : 4

----

Moralisation de la vie politique est une URGENCE !

Pourquoi ne pas s'inspirer du modèle Suédois où, la corruption des hommes et femmes politiques, est égale à ZERO !

Score : 54 / Réponses : 4

----

Il faudrait prévoir systématiquement, lors de l'adoption d'un nouveau texte législatif,  un article listant une serie de dispositions obsolètes à supprimer en même temps, afin de lutter contre l'inflation législative.

Score : 35 / Réponses : 2

----

pour des referendum d'initiative citoyennes

Score : 34 / Réponses : 1

----

Faire connaitre la consultation !!! J'en ai entendu parler lorsque F De Rugy en a parlé dans l'emission "on n'est pas couchés"

Score : 34 / Réponses : 0

----

**L'Open Source, seule garantie des valeurs du Gouvernement ouvert pour la Civic Tech**      

---

Le numérique apporte des solutions concrètes aux enjeux de la démocratie : outils de débat, de délibération, de remontées d'idées, d’ouverture des données, d’organisation, de lobbying citoyen… Ces outils numériques participent d’une démarche de "Gouvernement ouvert" fondée sur des principes éthiques, d'ouverture et de transparence. Ils sont appelés à devenir des parties intégrantes du processus de décision, associant les citoyens à la conception et à l’arbitrage des projets qui les concernent. Une ligne de code peut ainsi commander la publication d'un argument, un vote en faveur d'une proposition ou participer d’un algorithme traitant les propositions citoyennes.   

--- 

À terme, ces technologies sont amenées à devenir de véritables intermédiaires démocratiques entre les citoyens et les institutions. D'où l'enjeu qu'elles soient **ouvertes, collaboratives et, dans l'idéal, décentralisées** pour éviter qu'elles ne deviennent pas plus opaques et centralisées que nos institutions actuelles.   
 
---

Pour garantir aux institutions et aux citoyens qu'un vote en ligne n'est pas truqué, que les données personnelles ne sont pas exposées, qu’une ouverture des données issues d'une concertation est exhaustive, **un outil de participation numérique doit nécessairement être open source**, c’est-à-dire que son code doit être accessible et auditable par n’importe qui. Ainsi, l’écriture du code informatique doit s’approcher de l’idéal de l’écriture de la loi : **ouverte, transparente, sous le regard de tous.**   
 
---

Aussi, l'Open Source est une condition nécessaire pour une **véritable mise en concurrence et égalité devant la commande publique.** En pouvant analyser les formats d'échange et de stockage de l'information, de nouveaux éditeurs et prestataires peuvent proposer des solutions innovantes. Dans le cas contraire, les acteurs publics se retrouvent souvent coincés par un éditeur.    

---

Enfin, le logiciel libre est l'opportunité d’une **mutualisation des investissements pour la création de biens communs numériques partagés entre plusieurs collectivités et institutions, bénéficiant plus largement aux citoyens.** Elles pourront par exemple adapter ces solutions à leurs besoins avec un partenaire local et reverser leurs contributions à l'ensemble des utilisateurs.

Score : 32 / Réponses : 3

----

**Assurer un droit d'accès aux documents administratifs du Parlement**

Depuis la loi CADA de 1978, les citoyens disposent d'un droit d'accès aux documents administratifs produits et reçus par les administrations centrales et territoriales. Ce droit a été reconnu comme une liberté publique, ayant donc une valeur constitutionnelle par la jurisprudence (décision du [Conseil d'État n° 228830 du 29 avril 2002](https://www.legifrance.gouv.fr/affichJuriAdmin.do?idTexte=CETATEXT000008112306&) et décision du [Conseil constitutionnel n°2014-5 LOM du 23 octobre 2014](http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2014/2014-5-lom/decision-n-2014-5-lom-du-23-octobre-2014.142541.html?)). Le droit européen à travers les directives sur l'information publique de 2003 et 2013 a également créé un socle européen garantissant aux citoyens la réutilisation des documents publics ainsi que des voies de recours.

Si les parlements ont fortement investi dans des processus d'Open Data, la France n'a que partiellement transcrit les directives européennes. En effet s'agissant des documents produits ou reçus par les administrations parlementaires, aucun droit d'accès ni de réutilisation de ces informations n'est garanti et aucune voie de recours n'a été prévue par le législateur. La loi renvoie à l'[ordonnance de 1958](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=A48AF53361594ADB7BD5DB1634ACB969.tplgfr31s_1?idArticle=LEGIARTI000006530059&cidTexte=LEGITEXT000006069203&dateTexte=20171023&) l'organisation de ces dispositions sans que cette dernière aie été modifiée afin de garantir de tels droits.

Il convient donc de :
- compléter le droit français afin de garantir un droit d'accès et de réutilisation des documents produits par le Parlement et son administration ;
- faire entrer la jurisprudence constitutionnelle du droit d'accès et de réutilisation des documents publics dans la Constitution.

Score : 29 / Réponses : 1

----

**Assurer la transparence de la Questure**

L'administration financière des deux chambres parlementaires est réalisée par la Questure de chacune. Il se trouve que ces organes essentiels des deux assemblées ne font pas l'objet du minimum de transparence démocratique que l'on pourrait légitimement attendre :
- leurs réunions ne donnent lieu à aucun compte-rendu public ;
- les règlements budgétaires et financiers qui édictent les règles en vigueur à l'Assemblée ou au Sénat ne sont pas publics ;
- les comptes détaillés des deux chambres ne sont pas non plus accessibles aux citoyens ;
- enfin l'organisme de contrôle de la Questure, la commission spéciale chargée de vérifier et d'apurer les comptes, se réunit également sans que ses travaux ne soient rendus publics.

Il convient donc que la gestion financière des deux chambres fasse l'objet du minimum de transparence qu'une démocratie est en devoir de produire.

Score : 29 / Réponses : 1

----

Proposition: Faciliter la presence physique du public au débat à l'Assemblée nationale. Car aujourd'hui il faut passer par son député pour avoir une invitation. Personnellement, j'ai fait une demande sans jamais avoir de réponse. Donc je propose d'avoir une inscription par internet avec scan des papiers nécessaires, ouverte à tous les citoyens. Avec des quotas par régions d'origine pourquoi pas et également garder 10-20 places pour les personnes non inscrites qui se présentent le jour J. Bref, ne plus passer par une invitation de son député, pour plus de transparence. Et plus de mails sur ce sujet pour les députés, qui ont bien autre chose à faire.

Score : 28 / Réponses : 0

----

Une des motivations principales de certains élus de faire carrière en politique est la rémunération assez élevée qu'ils perçoivent. En effet, elle est environ de 7000€ par mois pour un député par exemple, ce qui représente 6,5 SMIC. Je propose alors que tous les élus nationaux et dirigeants des exécutifs locaux reçoivent pour indemnité le salaire moyen brut d'un français, qui était de 2958 € en 2016. Je pense que placer les indemnités de mandat au niveau du salaire moyen de la population est extrêmement symbolique, cela constitue un juste équilibre.

Score : 27 / Réponses : 1

----

OBJET: Pour une Constituante tirée au sort.
Mise en place d'une assemblée constituante de 1500 (?) inscrits bénévoles tirés au sort sur les listes électorales avec mission de réviser de A à Z notre Constitution en 6 (?) mois. Puis ratification par référendum.
La procédure complète est exposée sur le forum du CLIC;
http://clic-ric.forumactif.org/t30-procedure-de-tirage-au-sort-d-une-constituante 
Aller faire des remarques, critiques mêmes acerbes! et si possible suggestions!

Score : 26 / Réponses : 3

----

Rendre obligatoire le recours au référendum pour réviser la Constitution ou ratifier tout nouveau traité européen et garantir le respect de la décision populaire.

Score : 24 / Réponses : 6

----

**Publier au JO la participation aux réunions du Bureau et de la Questure**

Si toutes les réunions des commissions font l'objet d'une publication de la liste des présents au Journal Officiel, ce n'est pas le cas pour les travaux des organes « administratifs » qui régissent le bon fonctionnement de l'Assemblée et du Sénat : la Questure, le Bureau et la Conférence des présidents.

Pour mieux valoriser ces investissements, il serait souhaitable que la liste des parlementaires qui sont présents lors de ces réunions soit rendue publique dans les même dispositions que pour ceux qui s'investissent dans les travaux de commissions, via le mécanisme de publication des présences au Journal Officiel par les administrateurs.

Score : 24 / Réponses : 0

----

**Permettre une saisine citoyenne du/de la déontologue de l'Assemblée**

Le déontologue est la personne en charge du bon fonctionnement déontologique au sein de l'Assemblée. Il conseille les parlementaires et est régulièrement chargé par le Bureau de thèmes déontologiques. Les précédents déontologues se sont notamment penchés sur la problématiques des colloques ou la déontologie des collaborateurs parlementaires.

En revanche, le déontologue n'est pas tenu de répondre aux sollicitations des journalistes ou aux interpellations par voie de presse sur des cas particuliers. Il serait de bonne gouvernance que le droit parlementaire prévoie des modalités de saisine par des citoyens ou des journalistes afin qu'il se penche sur une situation ou un thème particulier.

En effet, jusqu'ici les déontologues ont très peu participé aux grands thèmes qui ont fait débat en matière déontologique ces dernières années. Offrir un mécanisme de saisine citoyenne permettra de renforcer le statut du déontologue et la place de la déontologie au sein de l'Assemblée nationale.

Score : 23 / Réponses : 0

----

Créer un Conseil national des médias à la place du Conseil supérieur de l'audiovisuel pour en faire un véritable contre-pouvoir citoyen garantissant le pluralisme des opinions et des supports, ainsi que la qualité de tous les médias

Score : 22 / Réponses : 3

----

Pour chaque loi il faudrait garder une trace du contexte dans lequel elle a été créée et des problèmes qu'elle est censée régler.

En effet, une loi qui ne produirait pas l'effet escompté ne devrait pas rester.

Score : 22 / Réponses : 1

----

J'aimerai que l'on puisse évoqué le statut du suppléant de nos députés qui aujourd'hui manque de reconnaissance. En effet, lorsque le député est sur Paris ou en déplacement, il ne peut pas être sur sa circonscription et encore moins proche de ses concitoyens.  C'est alors que le suppléant intervient en tant que représentant de celui-ci. Selon le protocole et les institutions, il n'est pas reconnu alors qu'il s'est présenté au côté du candidat titulaire. Si l'on veut plus de présence de terrain et bien évidemment d'efficacité, il faut une reconnaissance en tant que telle.  Bien évidemment, Les députés disposent de collaborateurs pour les seconder. Le suppléant lui est toujours présent et travaille sur le terrain. Est-ce une évolution envisageable ?

Score : 20 / Réponses : 1

----

Le comportement de certains députés en séance publique est inadmissible. Si  un élève le reproduisait en classe, une sanction lourde tomberait immédiatement.

Il est inconcevable que dans une instance des plus importantes de notre pays on puisse avoir un tel comportement. L'image de l'Assemblée nationale en souffre beaucoup. Puisque qu'en son sein même des députés ne la respectent pas, pourquoi nous, si éloignés de vous, devrions respecter cette institution ? Ceux qui la déshonorent doivent être remis sur le droit chemin.

Score : 20 / Réponses : 3

----

Si l'Assemblée Nationale avait réellement à cœur de représenter le peuple, alors elle ne se contenterait pas de simplement améliorer la fabrique de la loi pour le compte du gouvernement et de la commission européenne.

D'abord elle s'écouterait elle même et défendrait les conclusions du travail transpartisan de la commission Bartolone Winock de 2015 : http://www2.assemblee-nationale.fr/static/14/institutions/Rapport_groupe_travail_avenir_institutions_T1.pdf

Ensuite elle écouterait et prendrait en compte les propositions citoyennes qui sont sur la place publique comme par exemple : 
- le projet de loi de l'association Article 3 : http://www.article3.fr/informations/proposition-loi-constitutionnelle
- les propositions des Jours heureux : http://les-jours-heureux.fr/category/25-mesures/ 
- les propositions du 32 mars de Nuit Debout : https://www.rennesdebout.bzh/wp-content/uploads/2017/03/Propositions-du-32-Mars.pdf 
- les 6 propositions de A nous la démocratie : https://anouslademocratie.fr/les-6-propositions/ 
- le projet de Sénat Citoyen : http://senatcitoyen.org/PDF/Sénat%20Citoyen%20-%20Le%20projet%20-%2014%20Dec%202016%20-%20Web.pdf

Est-ce simplement envisageable sous notre Monarchie Présidentielle ??

Score : 19 / Réponses : 11

----

Créer un statut de l’élu
pour démocratiser l’accès aux responsabilités politiques en permettant à chacun de prendre un congé républicain, sans risque pour son emploi ou ses droits quels qu’ils soient, en vue de se présenter à des élections.

Score : 19 / Réponses : 1

----

De manière générale 
- décomplexifier la procédure législative et le vocabulaire utilisé dans la communication par les instance officielles
- lutter de manière urgente comme l'image de l'"hémicycle vide" -> permettre au suppléant ou aux collaborateurs de siéger quand le député en est empêché et renforcer le rôle du député-suppléant en circonscription ou à l'Assemblée
- lutter contre les biais d'enquête car ceux qui répondent sur cette plateforme ne sont pas représentatifs -> envoyer par courrier un questionnaire avec enveloppe de retour timbrée 
- Informer le citoyen sur LES rôles des députés : déplacements, commissions, Assemblée, circonscriptions...
- généraliser les visites de l'Assemblée et les proposer au plus grand nombre notamment via les réseaux sociaux

Score : 16 / Réponses : 1

----

Débattons sérieusement du temps de travail et du revenu universel . Le préambule de la Constitution du 27 octobre 1946 indique :

10. La Nation assure à l'individu et à la famille les conditions nécessaires à leur développement.

11. Elle garantit à tous, notamment à l'enfant, à la mère et aux vieux travailleurs, la protection de la santé, la sécurité matérielle, le repos et les loisirs. Tout être humain qui, en raison de son âge, de son état physique ou mental, de la situation économique, se trouve dans l'incapacité de travailler a le droit d'obtenir de la collectivité des moyens convenables d'existence.

Ce qui n'est absolument pas le cas en France : pourquoi ne pas se partager le travail et peut on réellement vivre décemment avec 470 euros par mois ?

Score : 13 / Réponses : 2

----

**Formation des jeunes pour une démocratie numérique à venir**

La marche vers une démocratie numérique passe par la formation des jeunes générations. Une formation sérieuse et indépendante ne peut se faire qu'avec l'utilisation de logiciels libres. Les liaisons entretenues entre le ministère de l'Éducation et Microsoft (et autres)  doivent cesser.
Propos de Richard Stallman sur le sujet lors d'une conférence intitulée « Vers une société numérique libre »

« Enfin, je dois expliquer pourquoi l'éducation doit enseigner uniquement le logiciel libre. Toutes les écoles, de tous les niveaux, depuis le jardin d'enfants à l'université et les activités d'éducation des adultes doivent enseigner uniquement le logiciel libre. Jamais un programme privateur. Mais pourquoi ? Pas surtout pour faire des économies. C'est un bénéfice secondaire possible. Mais cette question est importante. Il ne s'agit pas de comment faire un peu mieux l'éducation. C'est de faire la bonne éducation et pas la mauvaise éducation. Pourquoi est-ce que des développeurs du privateur offrent des copies gratuites aux écoles et aux élèves ? Ils veulent utiliser les écoles comme des instruments pour imposer de la dépendance à la société entière. Leur plan fonctionne ainsi :Ils livrent des copies gratuites à l'école. L'école enseigne leur utilisation aux élèves. Les élèves deviennent dépendants. Puis ils se forment dépendants et après leur diplôme, le même développeur ne leur offre plus des copies gratuites. Jamais ! Et quelques-uns trouvent de l'emploi dans des entreprises. Le développeur n'offre jamais aux entreprises des copies gratuites. C'est-à-dire que le plan c'est que l'école dirige ses élèves dans le chemin de la dépendance permanente et que les élèves tirent le reste de la société dans le même chemin. C'est comme donner des drogues addictives à l'école en disant que la première dose est gratuite ! C'est pour rendre dépendants les élèves. Non ! L'école rejetterait les drogues addictives, que ce soit gratuit ou pas, et doit rejeter les programmes privateurs, gratuits ou pas, parce que l'école a une mission sociale d'éduquer des bons citoyens d'une société forte, capable, indépendante, solidaire et libre. Et en informatique, ça veut dire former des utilisateurs habitués au logiciel libre et prêts à participer dans une société numérique libre. L'école ne doit jamais enseigner l'utilisation d'un programme privateur parce que ce serait inventer de la dépendance. Il ne faut pas ! »

https://www.april.org/vers-une-societe-numerique-libre-richard-stallman-2013

Score : 13 / Réponses : 0

----

Pour favoriser la participation citoyenne il est nécessaire de rétablir la confiance des citoyens dans le système politique. De nombreuses suggestions ont déjà été exprimées lors de consultations sur la plateforme Parlement et Citoyens. Vous les retrouverez ici :
- Rétablir la confiance dans l’action publique : https://parlement-et-citoyens.fr/project/retablir-la-confiance-dans-l-action-publique/consultation/consultation-30
- Généraliser les consultations en ligne : https://parlement-et-citoyens.fr/project/generaliser-les-consultations-en-ligne/consultation/consultation-29
- Comment refonder la démocratie locale ? : https://parlement-et-citoyens.fr/project/comment-refonder-la-democratie-locale/consultation/consultation-28
- Restaurer la confiance entre citoyens et parlementaires : https://parlement-et-citoyens.fr/project/restaurer-la-confiance-entre-citoyens-et-parlementaires/consultation/consultation

Score : 11 / Réponses : 1

----

ne pourrait-on pas prévoir une procédure de révocation individuelle des ministres par l'Assemblée nationale ?

Lorsqu'un ministre, et non pas tout le gouvernement, ne respecte pas le travail du parlement ou a une attitude déplacée envers lui, les députés pourraient par un vote à la majorité qualifiée exiger la démission du ministre et son remplacement.

Cette procédure permettrait en tout cas de maintenir une confiance suffisante entre les membres du Gouvernement et la majorité à l'Assemblée. Elle serait plus efficace que la motion de censure pour renverser tout le Gouvernement qui n'est jamais utilisée et qui a peu de chance de l'être plus souvent à l'avenir.

Cette procédure exige une révision de la Constitution.

Score : 11 / Réponses : 0

----

Je pense qu'il est temps de lancer un grand chantier pour regrouper toutes les petites municipalités de moins de 1500 habitants. Plus ces communes sont petites, moins elles ont de moyens pour pouvoir exercer effectivement leurs prérogatives. L'union fait la force.

Score : 10 / Réponses : 9

----

**Bac à sable institutionnel**

Afin de stimuler l'expérimentation de nouvelles formes de démocratie, autorisons des changements institutionnels d'un an. Un changement temporaire de la Constitution (réformant par exemple le mode de scrutin des parlementaires) serait adopté par référendum, et conservé si un second référendum l'approuve à l'issue de la période d'essai d'un an.

Score : 10 / Réponses : 2

----

création d'assemblées populaires locales qui puissent débattre de sujets locaux et nationaux, proposer des lois, des amendements ou des référendum. Elles doivent pouvoir proposer des actions locales, s'opposer à des décisions d'élus locaux ou nationaux, débattre des propositions des élus et citoyens et les voter. Elles doivent pouvoir discuter les une avec les autres... Il faut que les gens puissent se rencontrer et débattre. Le numérique peut servir de moyen de communication entre ces assemblées. Le numérique ne doit servir que de moyen pour faciliter les échanges. La démocratie vivante a besoin d'échanges vivants concret.

Score : 10 / Réponses : 1

----

[HackerSpace] [CivicTech] [Engagement citoyen]

Construire un hackerspace de l'Assemblée Nationale et plus largement, pour l'ensemble des institutions

Un lieu d'échange et de co-construction de projets favorisant l'engagement citoyen dans la vie de la cité. Le lieu serait ouvert en continu pour favoriser le travail collaboratif entre "simple" citoyen, citoyen geek, élus, représentant de l'administration... afin que chacun puisse faire émerger des solutions innovantes  et ainsi simplifier, améliorer et innover dans le fonctionnement de notre démocratie.

Score : 9 / Réponses : 3

----

1.  Un problème de ressources (humaines, financières) va se poser inévitablement.  Combien de personnes vont devoir dépouiller les requêtes, suggestions, propositions diverses?  Les députés vont être débordés et vont devoir recruter un nombre important d'assistants à moins d'avoir recours à de la sous-traitance sous forme d'élaboration de systèmes de classements, d'algorithmes, bref, de tri des données brutes.  Cela reste un défi important.

2.  Il existe (sauf erreur) 63 codes de lois et donc des milliers de lois et décrets.  Comment comparer ce que demandent les citoyens avec les textes existants?  Qui fera ce travail?  Par exemple, le citoyen Untel demandera la création d'une loi alors qu'un règlement existe déjà dans un code qu'il ignore.  Il y aura beaucoup de demandes et sans doute pas mal de déceptions, de frustrations ou d'abandon.  Un gros travail pédagogique reste à faire.  De l'individu à l'intérêt général (la loi s'applique à tous) il existe souvent une très grande distance.

3.  Sur quels critères sera établie la pertinence des questions, des demandes, des exigences?  Quels personnels seront attelés à trier le bon grain de l'ivraie?  Quelle sera la réponse faite aux demandeurs?

4.  Pendant la Révolution, le public était admis à faire valoir son avis en continu, depuis les bancs des spectateurs dans je ne sais quelle assemblée (Nationale, constituante, convention ou autre?).  Il s'ensuivit un chaos et une paralysie des débats, à tel point qu'il a été mis fin à cette forme de démocratie directe.  Grâce à internet les choses ont changé, et les problèmes aussi.

5.  pour conclure, je suis tout à fait favorable à l'établissement d'une plateforme numérique telle qu'elle est proposée.  Mais, il faudra y mettre de l'ordre, des ressources et faire des choix; et les Français n'aiment pas qu'on leur dise ce qu'il faut faire.  Des décisions devront être prises, qui ne plairont pas à tous.  Espérons que cela satisfera la majorité du public.

Score : 9 / Réponses : 4

----

**Technologie-collège au service de la formation de cohortes d'élèves en marche vers une démocratie numérique**

Ayant écouté quelques-uns des intervenants – tables rondes des groupes de travail – les observations récurrentes concernant le manque de compétences de nombreux citoyens ne manque de laisser perplexe.
En 1985 puis 1998, étaient inclus dans les programmes de Technologie-collège les connaissances de base en informatique. Cette discipline, la seule crée au 20e siècle, a été malmenée, a souffert, et le résultat est sous nos yeux.

Le chemin vers une démocratie numérique ne peut se faire que par une formation sérieuse des individus qui la composent. Il était indiqué en préambule des objectifs des programmes de Technologie-collège : «apprendre à regarder le monde qui nous entoure sans complaisance et sans fascination ». complaisance.

Le ministère de l’Éducation doit mettre au point un programme précis pour chaque cycle, faire confiance aux ressources humaines présentes en son sein. Il faut cesser de croire que des intervenants extérieurs seront plus aptes à délivrer une formation adaptée et raisonnée que des pédagogues formés et responsables. Les enseignants de cette discipline ont toutes les compétences requises pour mener à bien cette tâche.

Score : 8 / Réponses : 1

----

**Données des mineurs à protéger**
Nous confions nos enfants à l’Éducation nationale. Ils sont mineurs. Il est inadmissible que cette institution livre les données de cohortes d'élèves à des entités situées à l'étranger (Google - Alphabet, Microsoft, Apple). L'État offre la vie scolaire, la vie privée de nos enfants, croyant (feignant de croire) aux garanties qui lui sont données >> Gmail, Google drive.... et nombre d'autres qui s'approprient un maximum d'informations.

L'Éducation nationale se doit de fournir à tous ses établissements des ENT fonctionnels, dignes de ce nom afin d’empêcher que pour des raisons de commodité ou de facilité leurs responsables se tournent ailleurs, victimes de l'effet « bling-bling».

L'Éducation nationale doit reconnaître en son sein (ils y ont existé, ils y sont peut-être encore), ou embaucher les informaticiens, les développeurs qui auront les capacités à développer les produits indispensables.
 
Notre démocratie numérique doit être souveraine, indépendante et protéger les plus faibles, les plus démunis : la vie privée des adultes en devenir est une valeur à protéger avec détermination.

Score : 7 / Réponses : 1

----

RENFORCER encore et toujours LA CNIL
Nombre d'outils et d'initiatives proviennent d'éditeurs spécialisés qui  effectuent un formidable travail d'innovation démocratique.
Néanmoins, passée l'exaltation et en dépit de la confiance envers l'excellence éthique actuelle de ces startups de la CIVIC TECH, ne faudrait-il pas donner à un organisme comme la CNIL la prérogative d'une surveillance spécifique des risques potentiels de dérives de ces éditeurs, dans le même esprit que la vigilance amorcée à l'encontre des GAFAM ?

Score : 6 / Réponses : 0

----

AJOUTER UN CHAMP « COURRIEL » DANS LA FICHE DES CANDIDATS AUX ÉLECTIONS

Lors des élections, les professions de foi sont disponibles et le ministère de l'Intérieur publie le fichier des candidats. C'est bien. Mais comment entrer en contact avec les candidats par courriel est souvent extrêmement difficile. C'est paradoxale mais énormément de candidat font tout pour ne pas être contactables.

L'association April (http://www.april.org/) organise à chaque élection, via l'initiative Pacte du logiciel libre (http://candidats.fr/), un appel à contacter les candidats. Et à chaque fois, c'est un défi très difficile que de pouvoir trouver le courriel des candidats.

PROPOSITION : ajouter un champ « courriel » dans le formulaire de candidature aux élections et rendre ce champ disponible dans le fichier fourni par le ministère de l'intérieur.

Remarque : attendu qu'il est très facile de créer une adresse de courriel dédiée à une élection, on peut considérer qu'il ne s'agirait pas alors d'une information personnelle mais bien d'une information diffusable sans restriction.

Score : 6 / Réponses : 1

----

CRÉER UNE IDENTITÉ CITOYENNE NUMÉRIQUE

Une identité citoyenne numérique doit être créée, de façon sécurisée. Elle fonctionnerait à l’instar du numéro fiscal, qui sécurise toutes les démarches en la matière.

Elle permettra, d’abord, d’avoir une identification unique pour toutes les démarches citoyennes, dans le cadre d’une démocratie numérique, auprès des sites institutionnels et évitera de devoir systématiquement créer un identifiant.

Elle sécurisera, ensuite, l’identité de la personne qui agit et évitera ainsi l’un des risques principaux de la mise en œuvre d’une « démocratie numérique » (ou avec participation citoyenne grâce à l’interface numérique) qu’est l’usurpation possible d’identité. On peut aisément créer de faux profils, soit pour se faire passer pour quelqu’un d’autre, soit pour démultiplier une présence sur Internet, une seule personne pouvant « voter » plusieurs fois grâce à de nombreuses identifications.

Autres propositions dans le cadre de cette consultation : http://constitutiondecodee.blog.lemonde.fr/2017/10/30/participation-numerique/

Score : 5 / Réponses : 1

----

Le vote blanc n’est ni une abstention ni un vote nul. 

Il permet aux électeurs d’adresser un message clair à ses élus et offre une meilleure lisibilité des résultats d’un scrutin.

Il faudrait donc revenir sur la Loi du 21 février 2014 concernant le vote blanc. 

Car celle-ci, comme l’écrit admirablement bien Charles-Édouard SÉNAC, a l’art de prendre les électeurs pour des pions ! 

En effet, les bulletins blancs n’entrent toujours pas en compte pour la détermination des suffrages exprimés. 

Aussi, il serait souhaitable de modifier le troisième alinéa de l’article L. 65 du Code électoral en le complétant par la phrase suivante : 
« Les bulletins blancs sont décomptés séparément et entrent en compte pour la détermination des suffrages exprimés ».

Score : 4 / Réponses : 0

----

Renouveler la démocratie avec Internet et l’Open Data
C'était déjà un thème à l'ordre du jour en 2013. Peut-être que cette conférence est bonne à suivre et à relire :
https://www.april.org/renouveler-la-democratie-avec-internet-et-lopen-data-alexandre-quintard-kaigre

Score : 4 / Réponses : 0

----

1 : Casier vierge, ça parait le minimum, il en faut pour être instit, or les petits arrangements semble bien trop tentant avec de tel pouvoir.
2 : PAS DE PRO DE LA POLITIQUE : Mandat unique non renouvelable à vie. éviter les pratiques électoraliste.
3 : interdiction totale de cumul (communauté d'aglo, institution diverse, activité privé etc...Un seul salaire, un seul temps plein, limiter les conflit d'intéret (cf ferrand), une seule sphère d'"influence et aucun lien d'intéret privé/famille"...
4 : VALABLE POUR TOUT TYPE DE MANDATS mairie, CG CR COmunauté de commune etc...

Score : 3 / Réponses : 0

----

Créer un droit de révoquer un élu en cours de mandat, par référendum, sur demande d'une partie du corps électoral s'il ne respecte pas ses engagements. Il est anormal que sous prétexte de son élection, un élu ne rende pas compte de son action en cours de mandat.

Score : 3 / Réponses : 1

----

Supprimer le Sénat et réduire de moitié le nombre de députés .
Election des députés pour un seul mandat. Suppression des indemnités et des avantages comme leur régime spécial de retraite.Limiter à un, le poste d'attaché parlementaire par député.Salaire limité à 3 fois le SMIC.Même droits à l'assurance chômage que ceux des citoyens sans plus.

Score : 2 / Réponses : 1

----

Convoquer une assemblée constituante afin de permettre au peuple de reprendre les rênes du pouvoir volé par les lobbies et les multinationales et asservir aux banques. Il est nécessaire de permettre la mise,en place d'un vrai système démocratique équitable solidaire et humaniste.

Score : 2 / Réponses : 0

----

supprimer les lobbies (finance, énergie, pharmaceutique ,armement,  tabac, alcool, agriculture...)

Score : 2 / Réponses : 0

----

Bonjour , vous nous donnez à lire une charte de consultation, parfait merci. 
Mais les députés eux-mêmes ne la respectent pas dans leurs interventions ou ne l'ont pas lue. Chaque fois que l'un d'entre eux prend la parole ce ne sont que  des insultes, des remarques dégradantes et inutiles. Outre que cela donne un spectacle indigne de notre démocratie cela fait perdre du temps de travail parlementaire que chacun dit trop court. 
Imaginez des collégiens venant assister à une séance de l'Assemblée puis à qui on demande  d'avoir un comportement respectueux mutuel  en classe.  Bel exemple. 
Ce n'est pas parce que cela fait partie d'une tradition que cela doit être poursuivi. Profitons du renouvellement pour corriger cela aussi. 
Bon courage

Score : 2 / Réponses : 0

----

Je souhaiterais que le vote blanc soit reconnu comme suffrage exprimé.

Score : 2 / Réponses : 0

----

institution d'une possibilité de révocation des élus par référendum

Score : 2 / Réponses : 0

----

Je ne comprends pas que la présente consultation, très intéressante, ait été conçue avec un délai aussi court, et avec si peu de publicité. 
A retenir pour les prochaines consultations de ce type.

Score : 2 / Réponses : 1

----

Moi se que je voudrai c'est que chaque individu décide que toute loi voter soit fait par chaque citoyen cela serait la démocratie pas se que nous vivons actuellement ils votent des loi pour se qui les arrange et nous spolié  de notre liberté de vivre et donc c'est a nous de changer cette démocratie moribonde 
Première chose a faire supprimer de façon définitive tout ses avantages que l'on donne a ses anciens président et a ses ancien ministres c'est la qu'il  faut commencer car la c'est vraiment voler l'argent du contribuable avec leur fameuse loi qui votent quel que soit les parti il faut en finir et dire non et non et surtout arrêter de nous spolier de notre liberté  de parole 
Si nous ne réagissons pas nous perdrons nos acquis il est vrai que cela a déjà commencer et nous avons perdu énormément
Avec la bénédiction des média

Score : 2 / Réponses : 0

----

Bonne initiative et plein de bonnes idées sur ce forum. Mais il faut savoir que cette consultation a lieu ! Je viens de l'apprendre aujourd'hui. 
Et faut-il prendre le temps de lire les contributions et commentaires ! Combien de citoyen-e-s trouveront le temps à y consacrer après une journée de travail plus ou moins pénible et les occupations familiales et ménagères à la maison ensuite. 
Pour que les citoyen-e-s s'intéressent à la vie politique et à) l'élaboration des lois qui vont les concerner, l'idée d'atelier citoyens animés par les élu-e-s est très intéressante mais comment les inciter à prendre le temps pour y participer ?
Les faire sur les lieux de travail ou à proximité avec du temps payé accordé (1 heure par trimestre ? ) pour consulter les textes de lois qui les intéressent et donner leur point de vue ? Ce ne serait que la contribution du Medef à la vie démocratique après les 40 milliards que l’État lui a accordé et qui ont été redistribué en dividendes aux actionnaires.

Score : 2 / Réponses : 0

----

Je propose de changer complètement le mode d'élection des députés.

Suppression des élections législatives. Les député-es seraient élu-es en fonction des résultats du premier tour des élections présidentielles. 
Avant le premier tour, les candidat-es aux élections présidentielles devront rendre publique une liste de 577 personnes. 
Un-e candidat-e qui a fait 10% aux présidentielles gagnera 10% des sièges à l'Assemblée Nationale. 

Une candidature ayant fait moins de 4% n'aurait pas de puissance politique à l'Assemblée nationale, et ne n'aura donc aucun député. 
Prenons l'exemple des élections présidentielles de 2017

Il restera donc des sièges à attribuer. Ils seront attribués par une "mini élection législative".

EM!               24,01%.            24% des sièges.

FN                 21,30%.            21% des sièges.

LR                   20,01%.          20% des sièges.

LFI                  19,58%.           19% des sièges.

PS                   6,36%.             6% des sièges.

DLF                  4,70%.             4% des sièges.

Lassalle           1,21%.                0 siège.

Poutou            1,09%.              0 siège.

UPR                 0,92%.              0 siège.

Arthaud          0,63%.              0 siège.

Cheminade   0,18%.               0 siège.

Il reste 6% des sièges à attribuer. Les mini élections législatives les attribueront .

Ce nouveau système permettra l'épanouissement de toutes les pensées politiques au sein de l'Assemblée nationale.

Score : 2 / Réponses : 0

----

Bonne idée que de faire participer les citoyens, mais il faut faire de l'éducation populaire au sens politique du terme. Que les citoyens connaissent leur droits, leur histoire, comprendre par exemple les enjeux réels des lois (ex: loi travail, ou encore la baisse des cotisations  sociales ) en gros, pouvoir comprendre à qui profite la loi!!! D'autre part, séparer les médias de toute relations de l'état et de surtout des puissances des entreprises multinationales qui véhiculent bien souvent l'idéologie néo-libérale.

Score : 2 / Réponses : 1

----

Chaque député devrait avoir pour tâche de recueillir l'avis des électeurs de sa circonscription pour chaque sujet débattu à L'AN. Son vote devrait être le reflet exact de sa consultation citoyenne.

Score : 2 / Réponses : 1

----

Allez, je me lance in extremis dans le test de la plateforme (..plutôt que de critiquer éternellement la déconnexion de l'hémicycle...autant que je change en moi ce que je veux voir changer dans le monde, comme dirait le petit monsieur pacifiste). 
Bref, chère assemblée nationale, pour te regarder faire depuis quelques temps, je dirais que si tu proposes aux citoyens de "participer" à l'assemblée, c'est hélas le signe que les députés ne remplissent actuellement pas le rôle de "relais" qui leur incombe. En effet, si l'assemblée jouait pleinement son rôle de représentation des citoyens, chaque député se faisant l'intermédiaire des citoyens de sa circonscription, les citoyens n'auraient pas besoin/envie de venir s'exprimer ici. Ne serait-ce pas le signe que le canal qui relie le citoyen à la loi est bouché? Et pour moi, ce bouchon, c'est l'opacité. 
Actuellement, beaucoup de décisions, de votes, d'états de présence/absence de mon député me sont inaccessibles. La plateforme de l'assemblée ou la rédaction des compte-rendus ne donnent pas toujours les informations le plus basiques. 
Aussi, plutôt que de me suggérer de faire des propositions de participations à la loi, pourquoi avant tout, ne pas me permettre de mieux suivre/comprendre le travail de mon député? La transparence du travail parlementaire me paraît devoir précéder tout autre forme d'initiative...
Pour ma part, j'aimerais par exemple : 
1) que les votes en commissions soient publics, au même titre que les séance plénière. 
2) Je souhaiterais également que lorsqu'il y a saisine du conseil constitutionnel par 60 élus, connaître les élus qui ont portés cette saisine. (à titre personnel, je serais par exemple très désireuse de savoir si mon député a eu la "bonne" idée de porter cette saisine concernant la loi vigilance, mais le compte-rendu ne le dit pas : http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/les-decisions/acces-par-date/decisions-depuis-1959/2017/2017-750-dc/saisine-par-60-deputes.148846.html) 
3) qu'éventuellement on puisse connaître dans l'onglet "position de vote", les votes de notre député, amendement par amendement (plutôt que seulement la loi dans son intégralité), et ce pour chaque amendement. 
4) Que soit affiché un motif d'absence simple lorsque le député n'apparaît pas à une séance ("est actuellement en commission finance"/"est en déplacement en circonscription")..etc. afin d'augmenter la compréhension du travail effectué (ou non) par le député concerné. 

Ça permettrait déjà à chaque citoyen de savoir si son représentant..le représente...

Allez, je vais me coucher. Bonne nuit, j'attends ta réponse. Bisous

Score : 2 / Réponses : 0

----

Il faut mettre la thématique des "oubliés" de notre République au premier plan. Les villages ruraux, les jeunes des banlieues, les personnes âgées coupées du numérique...Ces personnes sont au coeur de nombreux débats sans jamais être consultés. Je propose de lancer, en lien avec l'assemblée, des groupes citoyen sur le terrain pour consulter, comprendre l'impact d'une loi, créer, imaginer avec ces personnes le futur de notre pays.

Score : 2 / Réponses : 0

----

Le rôle et les conditions pour assurer la fonction de SUPPLEANT méritent sans doute d'être évoqués dans le cadre de cette consultation.
J'ai personnellement découvert en mai l'absence de cadre définissant la fonction et je suis encore assez stupéfait.
Le magnifique renouvellement de l'Assemblée Nationale donne d'ailleurs à cette "fonction" un caractère encore plus surprenant si rien ne devait changer.
En effet, "autrefois", les suppléants étaient déjà pour la quasi-totalité d'entre eux, des notables de la politique déjà parfaitement installés dans une autre fonction élective. Ce complément à leur CV ne changeait que peu de choses dans le fonctionnement au quotidien sur le terrain au sein de leur circonscription.
Aujourd'hui nombre de suppléants sont naturellement issus, comme leurs députés, de la société civile et se retrouvent après les élections dans une espèce d'espace indéfini assez troublant. 

L'idée générale est non seulement de pouvoir être préparée à prendre une relève très hypothétique mais aussi d'apporter une contribution plus directe aux affaires locales et nationales.

Quelques remarques, commentaires ou proposition ci-dessous :

La délégation ou la représentation doivent être plus clairement définies même si elles doivent rester à discrétion du député titulaire. Le suppléant doit pouvoir assurer légalement l'autorité que veut bien lui attribuer son député. Cette mission est alors reconnue comme un droit.

Droit : 
Accéder librement à l'Assemblée National comme simple visiteur et avec accès à certains locaux généraux.
Accéder à certaines bases de données législatives et historiques ; à certains travaux en cours.
Pouvoir participer comme auditeur et sur demande à des réunions ou séances de commission etc…
Etre informé des programmes de l'Assemblée et des points les plus remarquables de l'agenda.
Etre remboursé des frais de déplacement sur justificatif (une visite à l'AN par mois max ?). Les frais de représentation doivent pouvoir être remboursés (sur budget du député ?).
Etre rémunéré au cas pas cas pour des prestations clairement encadrée. La notion d'emploi "fictif" a souvent été évoquée dans le cadre de l'Assemblée. On pourrait à contrario évoquer le risque de travail dissimulé pour certains suppléants.
Pouvoir démissionner sans justificatif (en cas de désaccord avec le député, par exemple) ou être révoqué (décision du Président de l'AN à la demande du député en place) mais sans que des élections soient reprogrammées obligatoirement.

Devoir :
Pallier très ponctuellement une défaillance temporaire du député.
Assurer un devoir de réserve et de confidentialité

Score : 2 / Réponses : 0

----

pourquoi ne pas associer les écoles en présentant un projet de loi,l'expliquer de manière simple,et faire remonter les réactions de certains élèves 
peut être pas en maternelle..mais dès le primaire ils ont de l'imagination !
et un avis sur tout.

Score : 2 / Réponses : 0

----

En recapitulatif pratique: les Communautes de Commune etant sous la responsabilite des Prefectures, donnons a chaque Prefet la mission de s'assurer que chaque CDC sur le territoire a mis en place un reseau citoyen et une consultation citoyenne a la fois en ligne plateforme numerique et son accompagnement, et en presentiel dans les nombreux espaces existants deja, tiers lieux, salle des Fetes, EPN. Car notre espoir est qu'il sorte de cette consultation AN une obligation de consultation citoyenne pour les elus.  C'est le sens de l'avenir et le bon sens. MERCI a toutes et tous!

Score : 2 / Réponses : 0

----

A  une heure de la fermeture de la consultation, j’ai bien conscience que peu de personnes liront ce poste et apporteront un soutient. Je souhaiterais compléter les commentaires précédents par des points jamais abordés jusqu’à présent. (Sauf erreur de ma part) 

L’enjeu de la présente consultation est de faire participer davantage les citoyens dans le processus législatif grâce aux nouvelles technologies. Même si tout le monde n’a pas respecté le jeu, les propositions dans les cinq premières rubriques se sont concentrées sur le processus démocratique lui-même. Je pense qu’il manque une rubrique sur la plateforme numérique proprement dite, ainsi que ses conditions d’usages. 

Il y a un consensus manifestement pour utiliser des logiciels libres. J’y souscris à 100%. Cependant, ceux-ci ne font pas l’ALPHA et l’OMEGA d’un système qui rendrait/maintiendrait la confiance des citoyens envers les institutions. 

Dans la mesure où les citoyens sont de plus en plus méfiants envers les institutions, sont opposés à la fusion de fichiers des différentes administrations, allons-nous confier l’hébergement de cette plateforme numérique à ces mêmes institutions ?
Je pense que ces questions méritent d’être débattues …  au parlement !   

Dans les précédentes rubriques, il y a un consensus des participants pour plus de transparence et moralisation de la part de nos parlementaires. Mais charité ordonnée commence par soi-même : 

Dans la mesure où le processus législatif est étendu aux citoyens (et qu’importe la technologie), cette règle devrait s’appliquer également à tous les citoyens qui souhaitent prendre part à ce processus : 
Donc : L’identité de la personne qui réalise un acte relevant de la prérogative d’un parlementaire (dépôt d’un texte, vote d’un texte, question au gouvernement, …) doit être clairement établie et pas masquée derrière un pseudonyme. Désolé pour les contributeurs de l’actuelle consultation. 

Dans la mesure, où les citoyens rejettent de plus en plus le mode représentatif, et souhaite s’immiscer davantage dans ce processus, l’usage de collectif, associations diverses ou autres assemblées me semblent contradictoire avec l’esprit d’une participation individuelle. D’autant que la gestion des membres (par exemple les étrangers, les personnes déchues de leur droits civiques, les parties prenantes, …) de toutes ces organisations n’est pas aussi transparente que celle de nos deux actuelles assemblées que nous rejetons avec tant de forces.
Donc : Seules les personnes physiques, jouissant de leur droits civiques, en âge de voter, peuvent prétendre à la réalisation des actes relevant de la prérogative d’un parlementaire. 

Néanmoins, comment garantir sur cette plateforme numérique, la RGPD (Règlement Générale pour la Protection des Données Personnels) avec les deux règles précédentes ? Je n’ai pas la réponse à cette question, mais nous n’y échapperons pas. 

Enfin la dématérialisation (implicite avec une plateforme numérique) implique dès la conception une gestion de l’archivage des documents (comme l’INA par exemple) sur une durée au moins de 150 ans (au moins trois générations de personnes qui ont un espérance de vie de 80 ans) qui dépasse aujourd’hui l’ère informatique (début des années 1960).  
Ceci implique une gestion très couteuse de la plateforme car le cycle de vie des technologies actuelles est très courte (Loi de Moore)  et la conversion des documents dans les nouveaux standards vers les nouveaux est  particulièrement couteux. (Essayer aujourd’hui de  déposer vos souvenirs VHS des années 1990 sur Youtube ! ) Bref, il y a une question d’archivage légale (et oui, c’est le domaine juridique). 

Cela ne doit pas nous empêcher de construire cette plateforme, il faut juste avoir conscience de ce qu’elle implique.

Score : 2 / Réponses : 1

----

les conditions d’inscription sur les listes électorales sont mauvaises en France. Trop de nos concitoyens ne sont pas inscrits ou plus inscrits. On estime que 10% voire 15% d’entre eux ne sont pas inscrits ou mal inscrits c’est à dire encore inscrit sur une commune qu’il n’habitent plus. comment y remédier.Le fichier le plus à jour quant aux résidants d’une copine est le fichier des clients abonnés a rdv par exemple ou autres fournisseurs d’énergie .aujoird’hui on n’avpas Le droit de croise peut les fichiers. Il faut donc changer la loi sur ce point tout autre méthode ne sera pas aussi efficace.

Score : 1 / Réponses : 1

----

Pouvoir, par initiative populaire, saisir le conseil constitutionnel pour qu’il vérifie tout ou partie d’une loi. Par exemple, pour la récente loi sur la sécurité intérieure et la lutte contre le terrorisme, même si des doutes planent sur la constitutionnalité de certaines mesures, le président de la république dit : « Je ne prendrai pas la décision de déférer cette loi devant le Conseil constitutionnel (...) La qualité du travail a permis d’aboutir à un texte pleinement satisfaisant. ». Autant que le sache, le président n’est pas qualifié, légalement parlant, pour juger de la qualité et de la constitutionnalité d’un texte. Par ailleurs, le groupe Socialiste et Républicain au Sénat a refusé d’envoyer le texte au Conseil constitutionnel, pour des raisons de cohérence de vote. S’agissant d’un texte sécuritaire de plus, qui concerne l’intrusion de l’État dans la vie des citoyens, les raisons invoquées pour ne pas le faire vérifier semblent particulièrement faibles, d’autant que, si un texte est bon, pourquoi craindre sa vérification ?

Dans ce genre de cas, une initiative citoyenne devrait permettre la saisine du Conseil constitutionnel sur tout ou partie de la loi, avant son entrée en vigueur, afin d’éviter d’attendre les QPC des mois, voire des années après l’entrée en vigueur, et d’éviter que des lois anticonstitutionnelles n’aient été appliquées tout ce temps alors qu’une saisine en amont aurait pu l’éviter. Par exemple, on pourrait, avant la promulgation, donner un délai d’attente de deux semaines ; si, pendant ce délai, aucune initiative populaire concernant la constitutionnalité de la loi n’est déclarée, ladite loi est promulguée. Dans le cas contraire, le délai d’attente est prolongé de plusieurs semaines, le temps de laisser les citoyens qui le souhaitent apporter leur soutien à l’initiative ; si, dans le délai imparti, l’initiative reçoit un nombre de soutiens suffisant, le Conseil sera alors saisi, pour examen de l’entièreté de la loi si l’initiative ne précisait pas que l’examen devait porter sur des parties précises de la loi.

Score : 1 / Réponses : 0

----

Paul Ricoeur disait : "Est démocratique, une société qui se reconnaît divisée, c'est-à-dire traversée par des contradictions d'intérêt et qui se fixe comme modalité, d'associer à parts égales, chaque citoyen dans l'expression de ces contradictions, l'analyse de ces contradictions et la mise en délibération de ces contradictions, en vue d'arriver à un arbitrage"  et Monsieur Emmanuel Macron disait à l'acropole le 3 septembre " Puisque c’est ici que fut pris le risque de cette démocratie qui confie au peuple le gouvernement du peuple, et considère que le plus grand nombre, c’est mieux que le petit nombre pour édicter une loi respectable, interrogeons-nous : qu’avons-nous fait, nous, de la démocratie ? ».  Ne pourrions nous pas faire un subtil mélange entre  démocratie  représentative et démocratie directe, entre démocratie pragmatique et efficace, et démocratie à l'écoute des citoyens  ?

Score : 1 / Réponses : 2

----

Ne pourrait-on pas vérifier avant de proposer une loi s'il n'y en a pas une existante qui s'en rapproche? Il s'agirait alors de l'actualiser. 
Notre pays croule sous les lois (mises en application ou pas d'ailleurs). Un bon dépoussiérage me semble nécessaire.

Score : 1 / Réponses : 1

----

Les citoyens devraient avoir accès aux compte-rendus des conseils européens et conseils de l'Union  via internet

Score : 1 / Réponses : 0

----

Un député par département et un sénateur par région.
On saurait enfin pour qui on vote et non plus juste pour un parti.

Score : 1 / Réponses : 0

----

Le législateur (le peuple et l'assemblée nationale) devrait être seul à avoir l'initiative de la loi. L'exécutif ne devait pouvoir présenter un projet de loi qu'à la demande du législateur. Le fait pour le président d'outrepasser les fonctions très limitées qui lui sont attribuées par l'article 5 de la constitution devrait automatiquement déclencher un référendum révocatoire.

Score : 1 / Réponses : 0

----

-> Médiatiser les consultations numériques, les futures plateformes citoyennes.
Quand on voit le peu de monde présent sur cette consultation, on se dit qu'on est loin du compte...
Sans l'article du Monde, je n'en n'aurais pas entendu parler.

Score : 1 / Réponses : 0

----

Créer une profession règlementée d’homme ou femme politique. Ils seraient salariés par l’Etat, soumis aux régimes sociaux des fonctionnaires, soumis a des règles de non-cumul, de performance et d’assiduité, de limite d’age, de virginité du casier judiciaire, mis à disposition des administrations nécessiteuses, obligés de faire des publications, obligés de faire des « stages » non rémunérés dans des associations et entreprises.

Score : 1 / Réponses : 0

----

Il est très difficile de se faire une opinion juste (non biaisée) sur un dossier à partir des seules informations diffusées par les médias. Si l’on reprend l’exemple des APL, on a surtout entendu le « scandale» que représentait leur suppression. Beaucoup moins en a été dit à propos du fonctionnement global du secteur du logement social, de sa potentielle inefficience et de la nécessité de sa réforme.

Il serait utile pour que le citoyen puisse appréhender correctement les questions posées que des dossiers non partisants présentant les problématiques, les faits, les différentes solutions proposées avec leur avantages et inconvénients soient mis à disposition.

Score : 1 / Réponses : 0

----

Légalisation du cannabis dont la production et la distribution appartiendrait à l'Etat pour :
- limiter les trafics
- générer des rentrées fiscales, estimées à 2 milliards par an (1.5 milliard de rentrées directes et 500 millions de lutte contre les traffics). Ces rentrées fiscales permettraient de financer des projets d'éducation ou des campagnes de prévention
- contrôler la qualité des produits vendus
- mobiliser les forces de l'ordre sur d'autres sujets, les rendant plus efficace.

Je ne suis pas fumeur (ni cigarettes ni cannabis), mais rendons-nous à l'évidence. Nous sommes parmi les pays d'Europe avec la législation la plus stricte mais d'autre part le plus gros consommateurs en pourcentage de la population.

Score : 1 / Réponses : 0

----

Je suis pour plus de participation des citoyens dans l'élaboration des textes de lois.
Je trouve anormal que nos représentant (es) ne soient pas á l'assemblée quand cela est nécessaire. Ils ont été élus (es) pour cela.
Je pense que des plateformes devraient être accessible pour s'exprimer sur des points sensibles.

Score : 1 / Réponses : 0

----

Redonner confiance dans la politique
Rappeler le rôle essentiel des partis politiques dans la formation des futurs élus: apprentissage de la discussion et de l'importance des compromis dans la construction de projets politiques équilibrés, compréhension de l'intérêt général, introduction au fonctionnement des institutions, etc.   
Réaffirmer clairement l'attachement de tous les élus et candidats aux valeurs morales les plus strictes; respect des lois, paiement des impôts, transparence sur les éventuels conflits d'intérêt, refus des interventions abusives, aucun abus de pouvoir, etc.
Exemples d'engagements
Les élus de la République s'engagent à ne jamais présenter de textes, de propositions de lois, d'amendements préparés par une entreprise, une banque, un groupement professionnel, sans en signaler explicitement l'origine. 
Les élus de la République s'engagent à publier leur emploi du temps, et notamment leurs rendez-vous. 
Les élus de la République s'engagent à ne pas céder à la facilité, aux petites phrases à destination des médias, aux attaques sordides contre leurs adversaires, aux agences de communication et aux fake news.

Score : 1 / Réponses : 0

----

Le temps le plus fort de notre démocratie, c’est la présidentielle. La procédure actuelle de parrainage est pénible pour les maires et inégalitaire pour les candidats, le temps passé à collecter les signatures étant perdu pour la campagne, ce qu’a noté [la commission Jospin](https://fr.m.wikipedia.org/wiki/Commission_sur_la_r%C3%A9novation_et_la_d%C3%A9ontologie_de_la_vie_publique). Je suis donc pour **laisser les citoyens choisir leurs candidats**, en gardant le débat ouvert sur le comment (primaire totalement ouverte, parrainages citoyens sur une plateforme en ligne, jury citoyen...) L’intérêt des citoyens est déjà là si on en juge selon le succès de [LaPrimaire.org](https://laprimaire.org/).

Score : 1 / Réponses : 0

----

- Une plus grande transparence sur les dépenses des députés. Il est important que le système des notes de frais (comme dans le secteur du privé) soit rendus publics. Un travail de député n'est pas un travail "normal", c'est une personne publique et il induit donc une transparence totale (je ne parle pas ici des dépenses liée à la vie privée bien entendu).
- La moralisation de la vie politique, qui est une urgence. Il est nécessaire que les élus soient des exemples pour les citoyens, et il est scandaleux de découvrir par la presse chaque semaine de nouveaux exemples d'élus de tout bord qui profitent du système. Il existe des politiques vertueux et engagés, ils ne devraient rester que ceux là!

Score : 1 / Réponses : 0

----

Convoquer une assemblée constituante, composée à 50% d'élu-e-s à la proportionnelle et à 50% de citoyen-ne-s tiré-e-s au sort.

Score : 1 / Réponses : 0

----

Transparence des députés et présence aux séances
Il faut absolument que les députés non présents aux séances et commissions soient sanctionnés, de la même manière qu'un citoyen le serait s'il ne va pas au travail. Il faut que la sanction soit dissuasive et encourage les députés à faire ce pour quoi ils ont été élus. 
De plus, un site avec la publication officielle des dépenses, contrôlées indépendamment de l'AN, doit être ouvert pour l'information des citoyens.

Score : 1 / Réponses : 0

----

La Démocratie est un bien précieux depuis des années celle-ci a du plomb dans l'aile à cause de l'Abstention A chaque étage les Citoyens doivent participer proposer émettre des idées c'est ainsi que notre maison Démocratie retrouvera sa plénitude dans ce monde incertains qui bouge Merci par avance .. Cordialement ...

Score : 1 / Réponses : 0

----

Limiter la vie d’élu à 2 mandats dans le temps afin d’en finir avec tous(tes) ces carriéristes qui se soucient rapidement davantage de leur carrière que de la France.

Score : 1 / Réponses : 0

----

Quand je vois le nombres de votes pour les propositions je trouve cela lamentable cette consultation est quasiment secrète. Sur un sujet aussi important il n'y a aucune communication officielle ni publicité pour informer le grand public. Une prolongation de la consultation avec une campagne de publicité grand public donnerait une légitimité aux propositions

Score : 1 / Réponses : 0

----

moi j'aimerais interroger la question financière autour des élus: les indemnités et non des salaires, les avantages en nature (chauffeur, coiffeur, etc), et tout ce qui fait que lorsqu'on me dit "c'est la crise il va falloir se serrer la ceinture" je dis "pourquoi pas, mais je serais d'accord le jour où les personnes qui prennent ce genre de décision commence par l'appliquer chez eux!!! C'est une des nombreuses incohérences de notre système.

J'aimerais qu'on réfléchisse sur le fait que l'accès au pouvoir semble faire tourner la tête en règle générale à l'Homme et comment s'en protéger pour arrêter de se faire prendre pour des lapins de 3 semaines!

J'aimerais qu'on réfléchisse sur le fait que nos élus vendent nos richesses, nos acquis sans scrupule et que nous sommes impuissants face à ça!

J'aimerais qu'on réfléchisse sur le fait que des retraités dorment dans la rue alors que des personnes qu'on nous oblige à subir durant 5 ans nous précarise et nous humilie sous prétexte que notre système politique et électoral est dépassé...

Et j'aimerais avoir le temps pour réfléchir à tout ça, mais ce n'est sincèrement pas le cas, parce que moi je ne suis pas "indemnisé" pour ça!! Alors je cours pour survivre...

Score : 1 / Réponses : 0

----

Créer une commission spécialisée chargée de lire les pétitions, de les évaluer et éventuellement de recevoir les auteurs ou leurs représentants, afin de transformer celles-ci en un texte de proposition présentable à l'assemblée, comme cela se fait en Nouvelle-Zélande par exemple.

Score : 1 / Réponses : 0

----

Il faut supprimer les exigences de parité sexuelle et d'alternance sexuelle dans toutes les élections, constitutions d'assemblées, etc. Ces exigences puériles sont contraires au besoin de compétence, disponibilité, probité... de toute fonction élective, et font planer sur chaque élu le doute qu'il ne l'ait été qu'à raison de son sexe.

Score : 1 / Réponses : 0

----

incorporer dans les loi : Le respect de la loi , Le respect des citoyens et de leur famille ; Le respect de la bienveillance ,  Le respect de la fidélité familiale et conjugal , Le respect de la vie  des valeurs de vie et de conception de vie , Le respect envers les personnes et envers la famille des personnes ,La pénalisation du viol des droit fondamentaux de vie et de conception de la vie ,Le respect des droits fondamentaux de la vie et de la conception de la vie sur la terre , La pénalisation des violences conjugal et familiale au lieu de les aider et de les soutenir ces violence pour les envenimer encore plus comme c'est le cas actuellement .

Score : 1 / Réponses : 0

----

j'attends toujours la reconnaissance  du vote bianc comme un vrai vote  !!!!!!!

Score : 1 / Réponses : 0

----

CONTINUER CETTE INITIATIVE PAS ASSEZ CONNUE! 
Le délai est trop court.
Merci d'avance.

Score : 1 / Réponses : 0

----

Sécurité routière : 
Tant de voitures et camions ont les phares mal réglés et éblouissent les conducteurs venant d'en face. Les contrôles techniques s'avèrent donc inefficaces. Il serait vraiment utile de réfléchir à une mesure éducative ou contraignante pour faire cesser ce danger.
Merci.

Score : 1 / Réponses : 0

----

Mettre en place une carte d'identité citoyenne qui permettrait d'offrir des droits aux citoyens de son domicile. 

Elle permettrait le vote, mais aussi la consultation des projets de lois, le contrôle du travail parlementaire. 

Cette carte serait bien entendu sécurisé avec toutes les garanties actuelles.

Score : 1 / Réponses : 0

----

Il faut rendre la présence des parlementaires obligatoires. Il faut aussi rendre la création d'un compte rendu officiel et annuel du travail du député OBLIGATOIRE !!!!

Score : 1 / Réponses : 0

----

- Propositions de Renaissance Numérique et la Fondation Jean Jaurès

Une assemblée mixte pour réformer la Constitution
- Pour intégrer toutes les modifications prônées dans cette réforme en faveur d’une démocratie plus ouverte, une réforme de la Constitution est nécessaire. 
- Une assemblée mixte constituée à moitié d’élus, à moitié de citoyens représentatifs de la population française tirés au sort, a pour mission, pendant neuf mois, de mettre en place un texte intégrant la nouvelle place du citoyen à l’ère de la société collaborative dans les principes fondateurs de notre pays.
- Un certain nombre de questions techniques découlant des nouveaux processus de gouvernance devront être posées et tranchées : quelle place dans la décision donne-t-on au citoyen ? comment organiser la co-construction de la loi sans trop ralentir ou bloquer le travail législatif ? sous quelle(s) modalité(s) voter le budget participatif de l’État ? …

Trois conditions à la réussite de cette réforme 
- Adapter les contenus pédagogiques, notamment concernant l’enseignement civique à l’école, aux nouvelles réalités de la citoyenneté à l’ère du numérique (droits et devoirs, outils d’expression, apprentissage du débat contradictoire, compréhension du monde informationnel, pluralité et véracité des sources...), afin d’éviter les risques d’exclusion démocratique.
- Repenser dans sa globalité, avec une vision stratégique pluriannuelle, la politique nationale de médiation numérique afin qu’elle s’adresse prioritairement aux publics a-numériques pour favoriser l’e-inclusion démocratique.
Un maillage de lieux dédiés à l’innovation et la participation politique type « civic hall », doit être créé, pour installer une véritable proximité dans cet usage démocratique des outils numériques : un lieu citoyen, où des dispositifs sont mis en place pour permettre de proposer des idées, mais aussi accéder à l’information et aux données publiques. Mêler le lieu historique du commun, qui est la mairie, avec l’outil numérique est une façon forte et claire de réussir cette transition. 
- Insuffler la culture du gouvernement ouvert auprès de l’administration et des élus et leurs collaborateurs. Face à l’arrivée des nouveaux outils numériques dans leur quotidien, ils ne perçoivent pas toujours les opportunités réelles du numérique pour faire évoluer leurs méthodes de travail et leurs missions.

La signature numérique pour les citoyens 
- Soumettre un projet, voter, participer en ligne à des assemblées via des supports numériques nécessite que l’identité de chacun soit connue et reconnue. C’est la condition de la transparence du débat et de la validité des décisions. 
- Il faut donc rapidement mettre en place un dispositif national pour lancer le projet. Plusieurs pistes sont possibles : sur la base du volontariat : tout citoyen qui le souhaite pourra mettre en place son identification en ligne, afin d’avoir accès à tous les services en ligne ; de façon systématique : à la naissance de tout nouveau citoyen, à partir de l’acte de naissance. Ainsi, un renouveau de génération permettra à la France de passer au tout numérique. 
- Son attribution dès 16 ans permettrait aux adolescents de s’investir tôt dans les expériences démocratiques et de vivre ainsi un véritable apprentissage, accompagné, à la citoyenneté.

Un regard citoyen sur l’agenda des élus
- Transparence des agendas des élus : toujours en ligne, accessibles à tous et actualisés.

[Propositions issues du Livre blanc “Démocratie : Le réenchantement numérique” : http://bit.ly/2zAPucV]

Score : 1 / Réponses : 0

----

Il faudrait inverser le calendrier des élections présidentielles et législatives. Deux légitimités sortiraient des urnes et le rôle du président serait davantage de fédérer des majorités en fonction des projets qu'il souhaite mettre en oeuvre et moins celui du Messie.
Aujourd'hui, les partis politiques élaborent des programmes. Puis les différents candidats développent leur propre programme en vue de primaire pour au final connaitre une grande confusion entre les attributions du représentant du pouvoir exécutif et ceux qui représentent le pouvoir législatif.
Commencer par les élections législatives permettraient de mettre l'accent sur le débat d'idées et de programmes plutôt que sur la personnalité des candidats. Et la période entre les deux élections seraient propice à l'élaboration de majorité en fonction d'un calendrier de législatif établi à l'avance sur la durée du quinquennat. 
Et en prime, on arrête les dépenses somptuaires pour l'organisation de meeting et autres manoeuvres clientélistes

Score : 1 / Réponses : 0

----

Permettre aux collaborateurs parlementaires d'accéder aux bases de données dans diverses disciplines d'une ou plusieurs universités dans le cadre de l'étude et la préparation des projets de loi.

Score : 1 / Réponses : 0

----

Puisqu'il est question de transparence, il est indispensable de faire apparaître avant la date de clôture, le nombre de votants total pour chaque proposition. 
Dans la façon présente de présenter les votes, si 4 000 personnes votent pour une proposition et que les 3 980 personnes que cette proposition dérange votent contre, il ne restera qu'un modeste score de +20 laissant penser que personne ne s'y intéresse.

Score : 1 / Réponses : 0

----

Limiter le nombre d'amendements à 10 pour chacun des groupes politiques présents au Parlement, et permettre le même nombre à un panel représentatif de citoyens, ainsi que des représentants d'entreprises pour plus de clarté et d'équilibre de forces, en espérant tendre à la concertation

Score : 1 / Réponses : 0

----

Bonjour, Le think tank point d’aencrage a publié cette année son rapport démocratie, technologie & citoyenneté. 

http://pointdaencrage.org/2017/05/09/democratie-technologie-citoyennete-construire-nos-institutions-numeriques/

Parmi les 10 propositions du rapport on trouve en particulier:
TIRER AU SORT DES CONFÉRENCES DE CITOYENS POUR
ORIENTER LES ENJEUX DE LONG TERME

Sur le modèle des G1000 organisés en Belgique, en Islande et récemment en Espagne, un
ambitieux processus démocratique permettrait de confier à des citoyens tirés au sort et
accompagnés par des facilitateurs professionnels la définition de grandes orientations
politiques de long terme ou la résolution de problématiques dont le débat public peine à se
saisir de manière efficace ou rationnelle. Le projet lancé par le collectif Echo Citoyen sur la
politique des drogues pourrait servir d’exemple.

Score : 1 / Réponses : 0

----

Bonjour, Le think tank point d’aencrage a publié cette année son rapport démocratie, technologie & citoyenneté.

http://pointdaencrage.org/2017/05/09/democratie-technologie-citoyennete-construire-nos-institutions-numeriques/

Parmi les 10 propositions du rapport on trouve en particulier: 

SOUMETTRE LES NOUVEAUX MINISTRES À UN GRAND ORAL
DEVANT LE PARLEMENT

Etre ministre c’est bien, connaître son sujet c’est mieux. Le fonctionnement de notre vie politique fait que les responsables publics n’ont parfois qu’une vague idée des enjeux que recouvre leur portefeuille de compétence. Plusieurs raisons à cela : c’est le président seul qui nomme et révoque les ministres ; les nominations sont souvent plus liées à un équilibre entre
tendances politiques qu’aux domaines d’expertise ; enfin, l’influence parfois
disproportionnée qui est accordée aux hauts-fonctionnaires dans la gestion de l’administration et de l’orientation stratégique des politiques publiques.
Nous nous inspirons de la procédure existante au sein de l’Union européenne : le Président de la Commission propose des commissaires, qui passent ensuite une longue audition face aux parlementaires européens qui se conclut par un vote dont le résultat est contraignant.
Ainsi en 2010, la candidate bulgare, Roumiana Jeleva, désignée à l’aide humanitaire, a dû se retirer en raison notamment de son manque de compétence sur le sujet.
Très simple à implémenter et accompagnée d’un audit de la situation fiscale des candidats,
cette mesure permettrait de s’assurer dès son entrée en fonction que chaque ministre dispose des compétences et de la probité nécessaire à la fonction.

Score : 1 / Réponses : 0

----

Implémenter une statistique avec un nuage de mots pour connaître en une seule vue  les mots significatifs  les plus cités par les contributeurs.

Score : 1 / Réponses : 0

----

Pour stimuler fidéliser encourager les participants une procédure saine: leur renvoyer sur leur adresse mail un remerciement automatique;  surtout une copie de leur dépôt, preuve de leur contribution.

Score : 1 / Réponses : 0

----

Lors de l’atelier législatif organisé par, le député du Val-de-Marne, Guillaume Gouffier-Cha le 6 novembre à Vincennes, trente personnes ont échangé dans quatre groupes de travail sur la démocratie numérique et les nouvelles formes de participation citoyenne.  
 
Tous les groupes ont réfléchi sur le thème de la participation à l’évaluation de la mise en œuvre des lois. Les autres thèmes retenus sont la participation numérique et la consultation en amont des textes.  
 
Quatre propositions ont obtenu le plus de suffrages à la suite de la mise en convergence : 
 
1. Rendre l’évaluation des lois plus claire. Définir les modalités d’évaluation de chaque loi en amont de son adoption, avec une échéance fixe : des critères d’évaluation (y compris sur les impacts en matière de simplification législative)) inscrits dans la loi, ainsi que le calendrier d’évaluation précis.  
 
2. Etre plus précis sur les méthodologies de consultation, valoriser et rendre compréhensible la restitution des consultations, consulter selon une méthode numérique avec une large publicité, avec une consultation sur le terrain complémentaire en parallèle. 
 
3. Vulgariser le débat public autour des lois, avec un site internet de l’Assemblée nationale à repenser, des partenariats pour mieux expliciter les lois en utilisant des outils simples et modernes (vidéos, schémas, webinar) 
 
4. Mettre en place une consultation citoyenne par tirage au sort à la fois pour la concertation et l’évaluation de la loi, élaborée par la commission qui porte la loi.

Score : 1 / Réponses : 0

----

une proposition : avant de réflechir à un projer de loi : 
- avoir "l'obligation" de recenser tout ce qui existe sur le sujet, 
- vérifier si c'est encore actuel et pertinent 
- l'abroger avant de voter autre chose si nécessaire.

l'urgence n'est pas aujourd'hui de voter de nouveaux textes mais de rationaliser et faire diminuer le poids de ceux qui existent déjà.

Score : 1 / Réponses : 0

----

Revoir complètement de fonctionnement des QAG, donner moins la parole à la majorité qui pose des questions sois envoyé du ministère sous des questions ou il y a la réponse dans la question, et faire en sort que ça sois en échange, deux Minutes de question, deux Minutes de responsable et un Minutes de commentaires à la responsable

Score : 1 / Réponses : 0

----

Nous personnes handicapées intellectuelles sommes des citoyens comme les autres.
Nous avons le droit de voter et donner notre avis.

L’Association Nous Aussi a eu l’occasion de tester le vote électronique proposé par Vot’Matic.
Cette solution est adapté aux personnes que nous représentons, 
mais elle est aussi accessible aux autres types d’handicaps (présence de pictogrammes et d’audio).

L’exercice d’une réelle citoyenneté passe par l’accessibilité au vote, à tous et pour tous !
Avant d’exercer ce droit, les programmes doivent être en Facile A Lire et à Comprendre (FALC).
Le Facile A Lire et à Comprendre est un ensemble de règles ayant pour finalité de rendre l'information facile à lire et à comprendre, 
notamment pour les personnes ayant d’un handicap intellectuel.
Les personnes handicapées intellectuelles ont le droit d’obtenir de bonnes informations comme tout le monde.
C’est écrit dans la Convention des Nations Unies relative aux droits des personnes handicapées. 
Dans l’article 9 de cette convention, il est écrit que les personnes handicapées doivent recevoir des informations accessibles.
Grâce à de bonnes informations, les gens apprennent ce qu’ils ont besoin de savoir.
Cela les aide à faire des choix et à prendre des décisions tout seul et donc de voter !

Nous vous invitons à prendre connaissance du discours de l’ancien Président de l’Association Nous Aussi, Monsieur Cédric Mametz
lors d’une conférence parlementaire,
le 5 décembre 2013
sur le travail social à l’Assemblée Nationale.
http://www.nousaussi.org/2013/12/intervention-de-c%C3%A9dric-mametz-%C3%A0-l-assembl%C3%A9e-nationale.html   
 
Notre slogan est « Rien pour nous sans nous » !

Score : 1 / Réponses : 0

----

Il faudrait augmenter les occasions de rencontres / discussions avec le député de sa circonscription.

Score : 1 / Réponses : 0

----

Permettre à des citoyens volontaires, tirés au sort dans leur domaine de compétence, de participer aux travaux du CESE.

Score : 1 / Réponses : 0

----

Je pense que la constitution et l'affaire des Français, des électeurs.
Donc toute modification doit obligatoirement et uniquement passer par un référendum. Ainsi, après une campagne courte d'information, chacun prendra ses responsabilités et la majorité l'emportera .
Comme cela, on évitera les votes de posture des partis politiques.

Score : 0 / Réponses : 1

----

**Permettre aux parlementaires de proposer des lois et amendements ayant un coût positif.**
C'est pour l'instant interdit par l'article 40 de la Constitution.
J'ai imaginé [une façon](https://forum.parlement-ouvert.fr/t/proposition-de-procedure-generique-delaboration-de-la-loi-renforcant-le-role-du-parlement/78) d'aboutir à un budget cohérent malgré l'abandon de cette restriction. Je serais ravi de découvrir d'autres procédures qui permettraient de donner aux parlementaires la pleine initiative de la loi.

Score : 0 / Réponses : 0

----

**Écrivons collectivement une nouvelle Constitution**

[Osons le big bang démocratique](http://www.fondation-nature-homme.org/sites/default/files/osons_le-big_bang3.pdf), comme le propose la fondation Nicolas Hulot pour la Nature et l'Homme (sic, j'aurais mis "l'humain" à leur place). Le document détaille la procédure : définition du cahier des charges par des citoyens aléatoires, élection d'une Assemblée constituante, puis adoption par référendum (comme en Islande).

Ceci devrait être effectué à toutes les échelles : nationale, européenne, mondiale.

Score : 0 / Réponses : 0

----

**Donner davantage d'autonomie aux collectivités locales**, pour leur permettre notamment d'expérimenter de nouvelles formes de démocratie (comme des nouveaux modes de scrutin), et de lever leur propre impôt. Le Brésil fonctionne comme ça, et ça stimule les budgets participatifs et la vie démocratique locale. 

[69% des Français sont favorables](http://tnova.fr/sondages/l-observatoire-de-la-democratie-en-france-sondage-viavoice-terra-nova-la-revue-civique-le-monde-france-inter-lcp) à "donner davantage de pouvoir aux collectivités territoriales".

Score : 0 / Réponses : 0

----

J'aimerai que les députés soient plus présents et accessibles dans leurs circonscription autre que pour les inaugurations, fêtes etc....Que nos élus locaux et régionaux publient un compte rendu trimestriel de leurs dépenses afin d'assure une transparence sur leur façon d'utiliser l'argent public
D'autre part, j'aimerai que l'on étudie un nouveau mode de scrutin pour que toutes les opinions soient prises en compte pour avoir une élection plus juste et représentative........les bulletins blancs ne sont pas toujours comptabilisée et l'abstention (parfois très importante) n'est pas prise en compte, ce qui fait que la personne est "mal" élue ou élu par un nombre restreint de personne qui ne représente pas la majorité des citoyens

Score : 0 / Réponses : 0

----

Les citoyens doivent pouvoir accéder aux délibérations des groupes parlementaires, ainsi qu'aux réunions entre présidents de groupes, pour avoir connaissance et pouvoir étudier les arguments qui ont fait changer d'avis leurs représentants. 
D'une part, cela permettra aux citoyens de pouvoir se faire leurs propres avis sur les évolutions de position de leurs parlementaires, et pourquoi pas se faire convaincre eux aussi grâce aux arguments avancés. 
D'autre part, cela protégera les députés eux mêmes contre d'éventuelles pressions de la part de leurs collègues et ainsi, garantira l'indépendance et la liberté de chaque député, tout en permettant de lutter contre les postures, véritable fléau nuisant à l'image de l'Assemblée Nationale.

Score : 0 / Réponses : 1

----

Si l'on numérise , il faut stabiliser certains aspects de la vie politique. Je propose la remise à plat de toutes les circonscriptions électorales de France et leur refonte selon des critères objetifs, géographiques, économiques et sociaux. Le découpage serait fait par une autorité , indépendante de quelque pouvoir que ce soit. Interdiction aux gouvernements de pratiquer du découpage.

Score : 0 / Réponses : 0

----

Mesdames, Messieurs 

Il y a six mois, j'ai écrit une réflexion Agora 2022 que j'ai retravaillé avec une quarantaine de personnes dont plusieurs  membres de l'AFDC qui l'on trouvé intéressante et innovante. Elle dépasse largement  le cadre de cette consultation car elle propose une refondation de nos institutions en introduisant un nouveau paradigme tout en conservant l'esprit original de la cinquième république. Je souhaiterai la présenter à une deux ou trois personne en privé dans un tout premier temps durant une paire d' heures avant d'envisager de la rendre publique. Je fais partie de la France "silencieuse". Cette réflexion est déjà assez avancée mais reste encore au stade de concept. Par conséquent je ne rentre pas dans le cadre de cette bonne démarche d'initiative citoyenne qui est de rendre publique et d'échanger. Et je vous prie de m'en excuser.  Néanmoins  j'aimerais qu'un des organisateurs me contacte pour échanger sur cette réflexion. Je vous en remercie  par avance. Bons échanges à vous tous. 
Vive la démocratie

Score : 0 / Réponses : 0

----

4 mesures pour redonner sa place au citoyen dans le débat législatif et sa légitimité à l'action politique.
1- CHARGER TOUS LES ELUS D’ UNE MISSION PERMANENTE D’ANIMATION DEMOCRATIQUE CITOYENNE : C’est à dire, établir,  par niveau de responsabilité ( de la mairie à la région), « un calendrier » de consultations et/ou débats citoyens à mettre en place selon des dispositifs divers mais garantissant ouverture, transparence et communication sur leur bassin électoral.

2- ATTRIBUER A L’UNE DES 2 CHAMBRES NATIONALES ( sans doute d’avantage l’Assemblée) LA RESPONSABILITE DE COORDONNER ET SYNTHETISER L’EXPRESSION CITOYENNE POUR LA REPRESENTER DANS LEUR TRAVAIL LEGISLATIF : Il existe de plus en plus d’initiatives citoyennes, à toutes les échelles, sur moult médias. Du café citoyen aux sites de pétitions, en passant par les Conseils de Développement, Maisons de Quartiers, etc... autant d’espaces physiques comme virtuels dans lesquels les citoyens s’expriment. Je pense que le travail parlementaire devrait consister à collecter ce qui en émane, à formaliser des passerelles et des débats entre ces espaces pour in fine être en mesure de synthétiser l’expression citoyenne sur leur circonscription et construire leur travail législatif, de proposition, élaboration et vote des lois à partir de cette expression citoyenne. La question se posera alors du mode électoral qui pourraient devenir plutôt indirect à partir des « organisations » citoyennes présentes sur le territoire.

3- ATTRIBUER A LA SECONDE CHAMBRE (peut-être le sénat) LA RESPONSABILITE DE COORDONNER ET SYNTHETISER LES CONNAISSANCES ET EXPERTISES POUR DIRIGER LEUR TRAVAIL LEGISLATIF : La réalité est complexe et ne peut être gérée à coup de « il suffit de », « Il n’y a qu’à » , etc.. Le regard des experts et scientifiques est plus que légitime sur les lois. Pourtant, tout travail scientifique s’appuie plus ou moins explicitement sur des choix épistémologiques en références à des valeurs, dont découlent des postulats sur lesquels ils développent leurs recherches. C’est la raison pour laquelle, les partis politiques bien qu’ils s’appuient de plus en plus sur des études scientifiques aboutissent à des conclusions différentes. C’est pourquoi, je pense que cette chambre devrait être alors élue au suffrage direct, à partir de listes nationales, établies par les partis politiques en référence à des choix de valeur explicites pour que soit représentée (pour faire simple) leur mode de pensée et ce proportionnellement à l’ »opinion citoyenne. »

4- INVERSER LES RôLES ENTRE GOUVERNEMENT ET CHAMBRES . C’est à dire que la fonction du gouvernement soit d’animer et arbitrer le travail législatifs des 2 chambres. Le gouvernement, constitué par le 1er ministre en accord avec le Président de la République, après avoir obtenu la confiance de la « Grande Assemblée » (c-a-d les représentants du peuple et des « experts ») établirait le calendrier des chantiers législatifs. Pour chaque loi, à l’issu du débat parlementaire avec un double aller-retour commençant par l’une ou l’autre chambre selon que la proposition de loi émerge de l’une ou l’autre, le gouvernement aurait la charge de rédiger la loi dans sa forme finale en prenant en compte au mieux le travail des 2 assemblées en arbitrant en transparence.  Puis il serait chargé de l’application des lois. Il serait soumis annuellement à un vote de confiance de la Grande Assemblée qui validerait ou non, son arbitrage et sa mise en application.

Score : 0 / Réponses : 0

----

Faire appliquer la loi pour les mairies qui ne l' applique pas et faire cesser les abus et excès de pouvoir. Certaines mairies sont dirigées parfois par une même famille , et cela a tendance à ressembler à une maffia avec tout ce qui en découle. ..
Il est nécessaire de faire appliquer la loi , et celle ci doit être la même pour tous .

Score : 0 / Réponses : 0

----

L'application de la loi est l'affaire de tous. Des institutions publiques, Etat, collectivités territoriales, établissements publics, groupements d'intérêt public, mais aussi des personnes privées.
A cet égard, le respect de la loi devrait être rappelé à certains organismes privés (banques, assurances, sociétés de téléphonie mobile et fournisseurs d'accès à internet) qui la méconnaissent bien plus que les administrations. 
Des procédures simples d'accès au juge ou à des instances de régulation existantes, mais ouvertes et accessibles à chacun, devraient être conçues de sorte que tout écart de la part de ces personnes privées reçoive une réponse juridique immédiate.

Score : 0 / Réponses : 0

----

Disposer d'Un lieu de centralisation des consultations numériques ouvertes : pour mutualiser les efforts de chacun de visibilité. Ville/ ONG / Etats. 

ex. participationcitoyenne.gouv.fr

Score : 0 / Réponses : 0

----

#Inclusion 
- Avoir un poste dédié "participation citoyenne" dans les espaces numériques publics. Où un médiateur intervient déjà.
- permettre ainsi un début d'évangélisation sur les opportunités d'ouverture citoyenne du digital dans des lieux dédiés à l'acculturation digitale de tous les publics

Score : 0 / Réponses : 0

----

Comme pour les salaries du public ou du privé, en cas  d'absentéisme, ces heures sont déduites des salaires. Il est souvent entendu que certains députés sont absents; Je pense qu'il serait logique de leur déduire ces heures sur leurs indemnités

Score : 0 / Réponses : 1

----

nos député doivent et ce doivent avoir des référents régionaux dans les divers métiers existent. et faire très attentions au représentent qui a ce jour ne sont plus vraiment représentatif (ex 25 % des pêcheurs on voté pour leur représentants ce qui a ce jour fait 75 % de mécontent) prévoir des référents hors des structures pour avoir vraiment une vraie vision des problématiques exposé

Score : 0 / Réponses : 0

----

En France, à combien se montent les frais de bouche, de représentation, de déplacement, de l'ensemble des responsables publics? L'idée de réduire chacune de ces enveloppes ainsi que celle des rémunération, pour les fusionner en une enveloppe unique me plait assez. Chacun serait rémunéré sur le solde de l'enveloppe unique qui sera je n'en doute pas, bien mieux gérée...
Un responsable rémunéré 5000€ par mois, frais de bouches 3000€, de déplacement 3000€, de représentation 3000€, se verrait attribuer une enveloppe globale de 10000€. A charge pour lui de préserver sa rémunération qui pourrait d'ailleurs être d'un montant supérieur à précédemment mais plafonnée...

Score : 0 / Réponses : 0

----

Bonjour,

Pour renforcer la participation des citoyens à l’élaboration et à l’application de la loi, il me semble nécessaire d'ouvrir davantage l'Assemblée nationale sur la Cité. L'ouvrir physiquement, c'est-à-dire faciliter l'entrée du citoyen au sein du Palais Bourbon. J'ai 16 ans et j'ai déjà visité plusieurs Parlements en Europe : à Berlin, à Budapest, etc. Accéder à l'Assemblée nationale française est très compliqué et je ne l'ai jamais fait. Les citoyens patientent pendant les journées du Patrimoine pour pénétrer derrière ses grands murs et ses grandes grilles. 
Pour participer à la loi, le citoyen français doit développer avec le législateur une relation privilégiée :  elle ne peut pas être seulement numérique ou épistolaire.  Ouvrez !

Score : 0 / Réponses : 0

----

Osons enfin le tirage au sort ! Le serpent de mer de la démocratie!
En faisant d’abord litière de toutes les caricatures qui s’attachent à cette revendication. 

Bien évidemment le système de la Grèce antique n’était pas parfait ! Le problème résidait d’ailleurs plus dans la composition et la représentativité des listes électorales que dans le principe du tirage au sort. En France, aujourd’hui, toute citoyenne et tout citoyen à qui les droits civiques n’ont pas été retirés par décision de justice sont éligibles. 
Le scrutin est universel. Certes, mais la désignation est aujourd’hui censitaire car hors les candidatures soutenues par les partis politiques, point de salut ! 
Les 600 000 militants des différents partis politiques répertoriés en France en 2016- et le chiffre est généreux, certaines enquêtes limitent ce chiffre à 350 000 !- ne représentent que 1,3% des 44 800 000 électeurs inscrits sur les listes électorales… 
Ainsi plus de 98% des électeurs n’ont qu’une chance extrêmement limitée de se voir cooptés au titre de la société civile pour compléter des listes établies dans un vivier particulièrement étriqué ! 
Il convient donc que la possibilité de représentation soit étendue à l’ensemble du corps électoral, effectivement et réellement, sans que le filtre des assemblées militantes opère une sélection purgative. L’imagination, les capacités et les talents ne se limitent heureusement pas à une minorité de militants ! 
Le tirage au sort permettra de grandement élargir l’assiette, notamment en direction de ceux qui ne se reconnaissent pas dans l’offre politique exixtante! 

La comparaison souvent faite avec les jurys d’Assise tendent à réduire ces derniers à un simple rôle de supplétifs quand les magistrats professionnels, et notamment le président de la Cour d’Assise, tiendraient la main des jurés… Drôle de conception de la justice et de son indépendance ! Quand en réalité, c’est le code pénal qui encadre les jurés, pas les professionnels de la justice ! 
Le simple citoyen devenu  juré d’une Cour d’Assises, se sentant véritablement habité par la responsabilité, s’investit totalement dans la mission qui lui est confiée, avec à la fois le nécessaire recul de l’humilité, la conscience aigüe de poser un acte individuel au seuil de la conscience collective, avec enfin la gravité exigeante du devoir à remplir. 
Pourquoi n’en serait-il pas de même dans la sphère politique ?

Plusieurs règles ou aménagements devront être pris en compte. 

Devront être exclus du tirage au sort pour un poste donné, ou pour le contrôle d’un poste particulier, ceux qui viennent de l’occuper pendant deux mandats consécutifs, cette disposition répondant à la règle du non cumul et du non renouvellement des mandats. 

Sauf à considérer que l’exercice d’un mandat relève d’une obligation impérative, non négociable et se substituant à toute autre priorité familiale, professionnelle ou de santé, il convient de laisser à chacun le libre choix du moment où il souhaitera éventuellement siéger dans une instance représentative. Ne  pourront donc être tiré au sort que les citoyennes et les citoyens qui le souhaiteront.

Pour respecter une parité que le tirage au sort ne garantit pas dans l’absolu, il sera possible de constituer des listes en fonction du genre des citoyens et prévoir ensuite un tirage au sort respectueux du partage des responsabilités. Le tirage au sort pourra concerner dans un premier temps : Une fraction des membres du Conseil Economique, Social et Environnemental, tout ou  partie des membres d’un jury à créer pour évaluer le travail parlementaire, un contingent particulier  de députés ou sénateurs.

 Des modalités spécifiques restent à définir. Cet espace de consultation permet certes de lancer des idées, pas nécessairement de les approfondir, il conviendra, dans le cadre d’une réflexion plus aboutie de border précisément tous les aspects de cette proposition, et notamment de la rendre  complémentaire au travail et à l’expression des partis politiques.

Score : 0 / Réponses : 2

----

IL faudrait que le gouvernement et les députés/ sénateurs veillent à rédiger correctement les lois pour qu'il n'y ait pas de problèmes d'interpretation ultérieurement ni d'articulation avec les taxtes existants. Trop de lois sont mal rédigées, ne peuvent être appliquées sans douter. Il y a quelques années nous juristes n'avions pas ces problèmes d'applicabilité.

Score : 0 / Réponses : 0

----

Renforcer la participation des citoyens à l’élaboration et à l’application de la loi est une initiative intéressante et ambitieuse. Notamment lorsque viennent en débat les questions sur les grands problèmes de société ou celles concernant les réformes structurelles  nécessaires à notre pays. 
Toutefois, on doit s’interroger sur la population qui se sentira concernée en raison de la technicité des questions abordées. 
Cette initiative de consultation intervient à un moment où la perte de confiance des citoyens dans l’action publique n’est plus à démontrer, la défiance à l’égard des élus semblant  s’installer durablement, entraînant abstention les jours de vote et  refuge dans le populisme et le vote extrême.
Pour toutes ces raisons, la consultation citoyenne proposée ne doit pas se limiter à l’élaboration  de la loi et à son application mais aussi aux échelons de proximité, chers à nos élus territoriaux.
A ce titre, les communes et les intercommunalités, ont un rôle majeur à jouer.
Certaines collectivités ont su de leur propre initiative mettre en place de telles pratiques, définir des règles participatives qui sont loin d’être de simples gadgets pour associer les citoyens à la prise de décision sur tous les sujets de la vie municipale et intercommunale. 
Inspirons nous de leur exemple. Le non agir ne peut perdurer sans courir le risque de fragiliser encore davantage le lien social.
Jusqu’ici, malgré la pléthore de rapports publiés au cours des dernières décennies, les gouvernements successifs se sont limités à poser les grands principes d’une démocratie participative sans en fixer les modalités d’application sauf à minima.
Bien que la participation des citoyens aux décisions locales ait été amorcée au début des années 1990, beaucoup reste à faire.
Renforcer le pouvoir d’agir des citoyens, c’est reconnaître leur compétence pour intervenir dans l’élaboration des décisions publiques et le contrôle de leur application, leur permettre de mieux comprendre les enjeux de l’action publique, installer la transparence dans les décisions prises et restaurer ainsi la confiance dans l’exercice de la démocratie.  
C’est aussi conjuguer démocratie représentative et démocratie participative pour parvenir à une action publique co construite et mieux partagée.

Régine Saint-Criq

Score : 0 / Réponses : 0

----

Première remarque : la formule “renforcer la participation des citoyens à l’élaboration et à l’application de la loi.” ne me parait pas adaptée. 

Car l’ ELABORATION de la loi, c’est le travail du parlementaire, et son APPLICATION c’est la mission des pouvoirs publics. Le citoyen est censé la connaître et est tenu de la respecter. 

Le législateur a donc une mission qui consiste à inscrire les intérêts particuliers des citoyens dans un cadre contraignant qui fait émerger l’intérêt général, moteur d’une société démocratique, qu’on appelle la loi.

Dans ce contexte nous voyons surgir une innovation technologique qui bouleverse la société tout entière : Internet.

Internet permet à chaque citoyen d’exposer publiquement ses intérêts et les outils numériques donnent les moyens aux parlementaires d’exercer leur activité de façon innovante ; collecter les avis, dégager des synthèses et produire du sens. 

Conclusion : commençons par comprendre ce qu’on dit et dire bien ce qu’on comprend. (ne vous fâchez pas … :)  )

Deuxième remarque : se saisir des outils numériques change les mœurs bien au-delà de la seule exécution de tâches pratiques : d’une part le législateur va faire évoluer sa connaissance des citoyens, électeurs, contribuables, et cette connaissance va influencer son action. 

D’autre part le citoyen-électeur-contribuable va voir son rôle et son pouvoir transformés par les outils numériques. C’est le “web tribunicien”.

Pour ces raisons et pour favoriser l’émergence de bonnes pratiques du “web tribunicien”, il faut documenter l’action de cette consultation en ligne. Cette action de documentation est indispensable pour :
-	constituer le corpus théorique de la transition digitale publique
-	faire connaître l’action des acteurs de la transition, 
-	inscrire cette transition dans le récit national

Conclusion : il faut que quelqu’un quelque part s’empare du sujet dans la fonction publique.

Troisième remarque : tout usage ou introduction de l’Internet pose une question préalable non résolue : la question de la donnée personnelle et de l’anonymat. 

La persistance de la non-résolution de cette question et l’absence de stratégie politique maintient les initiatives digitales à un niveau subalterne. 

A cet égard, la création de principes de droit accompagnée des indispensables expérimentations et essais empiriques donnera sans conteste à la France un crédit d’autorité au sein des observateurs et parlementaires européens.

Est-il envisageable d’ouvrir à Etalab.gouv.fr dont c’est la vocation (par exemple, mais il y a peut-être d'autres opérateurs) le champ de recherche-développement de ce “web tribunicien” naissant ?

Conclusion : cette consultation n’est donc pas seulement un événement circonstanciel mais aussi et surtout un enjeu de société.

Score : 0 / Réponses : 0

----

Élire toutes les assemblées (du conseil municipal à l'Assemblée nationale) à la proportionnelle intégrale. 
En ce qui concerne les députés ceux-ci ont un mandat de représentation nationale et non locale : supprimer le découpage par circonscription et élire (en métropole) les députés au niveau régional (n députés pour une région selon son poids démographique au dernier recensement). On conserverait une représentation plus locale pour les territoires ultramarins (département par exemple) compte tenu de leurs spécificités.

Score : 0 / Réponses : 2

----

On sait tous que les élus sont trop payés, alors pourquoi ne pas, pour que ces derniers s'intéressent a leur circonscription et qu'ils ne soient pas élu juste comme ça, les payer peu( le salaire d'un français moyen) mais payer leur déplacements et le reste de leur argent irait a celui ou celle ou ceux qui proposeraient une bonne idée pour la circonscription et de faire cela tous les mois, et payer les élus en fonction de leurs présences  à l'assemblée, comme pour un fonctionnaire .

Score : 0 / Réponses : 0

----

Il faudrait faire en sorte que les députés qui nous représentent(ou sont censés)
se fassent à manger tout seuls, pas comme Mr De Rugy , bref toutes ces choses du quotidien d'un Français.

Score : 0 / Réponses : 1

----

Le mandat impératif, pour que les élus aient l'obligation de tenir leurs promesses, c'est à dire une "contractualisation" du mandat avec des objectifs et des sanctions.

Score : 0 / Réponses : 0

----

Il est impératif de revoir complètement notre constitution actuelle car elle ne permet pas un fonctionnement démocratique de nos institutions ; ce site en est la preuve.
Une nouvelle constitution dont la rédaction sera confiée aux citoyens eux-mêmes (via une ou des assemblées tirés au sort) sera la meilleure manière d'améliorer nos institutions gangrénées par la corruption.
Plusieurs pistes pourraient alors voir le jour : une vraie séparation des pouvoirs, suppression du Sénat et remplacement par une chambre tirée au sort parmi les citoyens, la mise en place du référendum d'initiative citoyenne en toute matière, etc.
Mais le plus important est les règles du pouvoir ne doivent plus être écrites par ceux qui sont au pouvoir.

Score : 0 / Réponses : 0

----

Nous faisons face à une réelle crise de la représentation. Aujourd'hui malgré le vote au suffrage universel direct ou indirect, les citoyens ne se sentent pas représentés. Je ne crois pas que la majorité actuelle, bien qu'elle affiche de nouvelles têtes puissent rompre avec ce pessimisme ambiant. je rejoins les propos de La démocrate. Pourquoi ne pas convoquer une Assemblée constituante tirée au sort. Certains citoyens n'ont pas les moyens matériels ou le temps de se consacrer à la politique. La dotation parlementaire serait ainsi  un vrai outil démocratique pour permettre à n'importe quel citoyen de se consacrer à la chose publique et à l’intérêt général . Nous le faisons déjà pour les jurés de cour d'assise. Bien sur, ce projet peut sembler quelque peu naïf, d'autant qu'il faudra veiller à accompagner ces nouveaux élus tout au long de leur mandat, sans pour autant qu'ils soient manipulés par des conseillers technos et finalement représentants de l'ordre ancien. On peut s'inspirer de la démocratie athénienne mais sans avoir une armée d'esclave pour laisser les citoyens s'occuper des affaires de la Cité.

Score : 0 / Réponses : 0

----

Mettre chaque année à l'ordre du jour du parlement une loi de simplification et clarification
Cette loi permettrait de prendre en compte les remontées des citoyens et institutionnels sur les dispositions obsolètes ou mal ecrites ou absurdes
Dès lors qu'une telle dispositions est identifiée par un citoyen/association,/administration il pourrait la saisir et les citoyens pourrait "voter" pour les dispositions les plus urgentes à prendre en compte
Les dispositions pourrait être classés par code puis par numéro d'article s et pour les dispositions non codifiées par numéro de loi afin de rapprocher facilement les propositions visant les mêmes textes
On pourrait imposée par exemple qu'au minimum les 100 dispositions les plus citées soient reprises dans cette loi de simplification ou que le refus de prendre en compte la demande soit motivé par le parlement.

Score : 0 / Réponses : 0

----

La justice est rendue au nom du peuple par les magistrats + le truchement du jury populaire tiré au sort.
La Loi crée par le peuple doit aussi être co_validee par un groupe de citoyens tirés au sort et auquel le Rapporteur devra rendre compte de l'orientation des travaux de sa commission et prendre note des avis citoyens.

Score : 0 / Réponses : 0

----

Instaurer des citoyens tirés au sort au sein des conseils municipaux sans augmenter le nombre d'élus par collectivité; ces citoyens seraient des vigies à l'échelle des communes et permettraient d'avoir une démocratie participative citoyenne intégrée aux institutions ; ces citoyens pourraient être des relais pour l'évaluation de politiques publiques.

Score : 0 / Réponses : 0

----

Aujourd'hui beaucoup de citoyens sont désintéressés de la politique. J'aime à croire que de redonner une place à nos citoyens dans la vie politique pourrait faire avancer notre pays.
Des ateliers dans chaque ville ou village (avec l'aide des maires ou des députés) serraient une des solutions pour renforcer la proximité et montrer l’intérêt que portent nos politiciens envers nos citoyens. A l’issue de chaque atelier, un tirage au sort permettrait à nos citoyens d'avoir une place à l'assemblée nationale.
Mais pour que cela soit réalisable, les lois doivent être plus accessible dans la rédaction, la compréhension (lors des ateliers), sinon une partie de nos citoyens serait lésée. 
Une formation ponctuelle (en e-learning,...) de nos citoyens (qui le souhaitent) pourrait être aussi une solution pour permettre d'être tous égaux dans la mise en œuvre des lois.

Score : 0 / Réponses : 0

----

les citoyens n'ont plus confiance dans les politiques et quoi que l'on dise ou quoi que l'on fasse il y a encore  de la suspicion. Engagée depuis 3 ans comme élue locale, je ressens régulièrement cette défiance qui me navre . Je propose de  travailler sur les compétences que devrait acquérir chaque élu :
savoir être : exemplarité, écoute active, empathie, dynamisme
savoir :  instruction civique et citoyenne, quelques notions en droit public
savoir faire : animer des réunions, construire un discours, monter un projet, manager une équipe, s'exprimer oralement pour être entendu et écouté
etc...
Par ailleurs, je pense nécessaire d'encourager l'engagement politique des citoyen en  donnant un vrai statut aux élus : des droits à la retraite, la possibilité offerte à  tous de retrouver l'emploi après le mandat, de vivre décemment quand la responsabilité nécessite beaucoup de temps et demande parfois de laisser son emploi...

Score : 0 / Réponses : 0

----

Organiser le tirage au sort des représentants dans certaines instances démocratiques (conseils municipaux, départementaux, régionaux,...) selon un pourcentage à discuter; 25 % pourrait être un bon début. Sur la base du volontariat, non pas dans une perspective  populiste, mais pour former les citoyens à la complexité démocratique. La démocratie ce n'est pas simple; il faut donc former à la complexité citoyenne pour lutter contre le populisme et la radicalité .

Score : 0 / Réponses : 0

----

Une revue des codes législatifs devrait être effectuée régulièrement de manière ouverte avec l'aide de citoyens pour simplifier et s'assurer que les textes de loi sont compréhensibles par tous.

Score : 0 / Réponses : 0

----

Il conviendrait de diminuer le nombre de parlementaires.

Score : 0 / Réponses : 0

----

Chaque territoire a ses propres cartes à jouer : il existe un gisement de projets entrepreneuriaux ingénieux à révéler et déployer qui apportent des solutions aux défis dans tous les secteurs (agriculture, tourisme, énergie, santé, etc.) en misant sur des modèles d’avenir (économie du partage, sociale et solidaire, du numérique, coopérative etc.).
Certaines idées-projets ne voient pas le jour faute d’avoir un système d’écoute et de dépôt des dossiers sur des appels d’offres informels ou sur candidature spontanée. Les acteurs et décideurs manquent d’information sur les idées les plus audacieuses et de liens en réseau avec les projets identiques qui fonctionnent ailleurs !
En reprenant, en exemple, les thématiques du projet d'Emmanuel Macron, un site (en version BETA), voulu très simple d'utilisation, a été mise en place à la demande de Michel DELPON, un député de la Dordogne. L'idée est de donner une note aux idées-projets proposées et de pouvoir soumettre les siennes aux votes de la communauté. Travailler à l'évolution de la législation pour soutenir des projets jugés porteurs c'est aussi de la démocratie numérique, mais en mode prospective. Je vous laisse imaginer les économies que l'on pourrait réaliser parfois sur des choses qui manque d'un bon sens de terrain...
En sachant que toute idée peut être améliorée...

https://e-démocratie.fr

Score : 0 / Réponses : 0

----

Il serait souhaitable de limiter le nombre de mandats dans le temps., la fonction d'élu n'étant pas un métier. Le renouvellement permet aussi un plus grand brassage de populations destinées à représenter le peuple ; ce qui peut aider à reconstruire la confiance des citoyens avec leurs institutions.

Score : 0 / Réponses : 0

----

Serait-il intéressant de réaliser un véritable audit populaire en effectuant un sondage sur la totalité du territoire à une échelle non contestable scientifiquement cent milles voire un million de personnes, pour que enfin nous puissions avoir un moyen de pression et surtout de prise de conscience du plus grand nombre. L'idée serait de faire ce sondage et de pouvoir laisser le résultat à la personne interrogée (le questeur pourrait garder une souche) ?!

PS : Comme je ne sais pas si je peux incérer un doc je vous joins une copie de mon idée de sondage ci-dessous.

Rodolphe B.

"Idée de sondage : réaliser un sondage sur toute la France avec un maximum de personnes pour ….

Titre du sondage :

Sexe : 		Age : 		situation professionnelle :
Code postal : 	......................................................................................	

Question 1 : êtes-vous satisfait de la façon dont la FRANCE est gouvernée ?
Oui   Non

Question 2 : Pensez-vous avec certitude qu’actuellement en l’état vous pourriez donner votre avis, quel que soit le sujet sur une loi, une proposition ou une position de la France sur tous les sujets qui la touche ?
Oui   non

Question 3 : Aimeriez-vous pouvoir dire oui ou non lorsqu’une loi doit être voter, et que votre voie soit comptée dans les réponses pour adopter la décision sur cette loi ?
Oui   Non

Question 4 : Aimeriez-vous lorsque vous avez une idée, pouvoir la proposer pour qu’elle puisse être écouté et discuté avec tout le monde afin de vois si celle-ci serait bonne à mettre en place ou à travailler pour l’améliorer afin de l’essayer ?
Oui   Non

Question 5 : Aimeriez-vous avoir un contrôle sur les missions, les moyens matériels et financiers des personnes élus par les citoyens avec des possibilités d’actions sur leurs mandats ?
Oui   Non

Question 6 : Aimeriez-vous avoir une véritable démocratie au sens littéral du terme en France
Oui   Non

Si vous avez répondu non aux deux premières questions et vous avez répondu oui aux questions suivantes alors vous devez vous poser la question suivante :
Que suis-je en train de faire pour que ce que je souhaite se réalise ?

Si vous avez répondus non aux questions de 3 à 6 alors vous vous trouvez dans un monde parfait, et vous n’avez aucune question à vous poser, juste à continuer comme si de rien n’était, car tout va bien."

Score : 0 / Réponses : 0

----

POUR UNE NOUVELLE ACCESSIBILITÉ CITOYENNE
L’Etat français est engagé depuis quelques années dans un processus de modernisation, de  numérisation et de mise à disposition de services en ligne.                                                                                                                                                                                                                                                                                                                                                                                          

En parallèle, des outils innovants apparaissent, qui participent d'une réinvention des services publics et constituent un formidable gisement d'opportunités, tant pour les pouvoir publics que pour les citoyens, lesquels se tiennent aujourd’hui, à la lisière du processus décisionnel (législatif ou réglementaire).

Or, de nombreuses initiatives, publiques et privées, tendent à faire du citoyen une composante forte et une partie intégrante de ce processus.

Afin d’assurer l’efficacité des services en ligne et des outils participatifs mis à la disposition des citoyens, il convient de placer la question de l’accessibilité numérique au cœur des réflexions et chantiers, qu’ils soient en cours ou à venir.
Administration numérique, services publics en ligne, démocratie participative…ces mots n’ont de sens que si des outils réellement efficaces y sont attachés. 
Afin d’assurer cette efficacité, nous en appelons aux pouvoirs publics afin de voir inscrire dans la loi un principe de mesure de l’accessibilité et de la qualité de l’expérience utilisateur des outils ou service mis à disposition du citoyen.
Cette accessibilité peut et doit faire l’objet d’évaluations et d’audits périodiques, non par l’application de barèmes experts, académiques ou théoriques, mais en allant directement recueillir l’avis des citoyens et des utilisateurs.
Par ce biais, rendons les services publics plus accessibles aux citoyens.
Par ce biais, rendons la contribution citoyenne accessible.
Julie PLEMENITI ARBANAS - Co-fondatrice UserlynX. www.userlynx.com  
06 59 25 83 87

Score : 0 / Réponses : 0

----

Bonjour, 

Je synthétise ici un an de recherches sur la démocratie participative, disponible sur mon blog politty.wordpress.com.

Ce que je propose : la négociation coopérative par thème. Il s'agit de remplacer l'assemblée nationale actuelle par des tables de négociations pour chaque thème de débat, tables dans lesquelles chaque pan de la société est représenté.

Partons de ce que veulent les citoyens : 

1 - pouvoir intervenir sur les sujets qui leur tiennent à coeur, et déléguer leur participation pour les autres sujets 

2- désigner des représentants qui correspondent à leurs valeurs, qui connaissent leurs besoins et leurs préoccupations (et non pas ceux qui ont les meilleurs solutions, ce qui relève plus de la technique).

Concernant la procédure de décision : aujourd’hui c’est la majorité qui décide. Si un mouvement est majoritaire alors il n’a pas besoin de consulter les minorités s’il ne le veut pas. Ce mode de décision est rendu caduque par la fragmentation de la société entre 4 groupes d’environ 25% de la population, ce que l’on a vu aux dernières présidentielles. La légitimité d’un seul de ces groupes à gouverner tout seul est de plus en plus faible.

Voici la procédure de négociation coopérative : 

1- Bien problématiser un débat : quel problème cherche-t-on à résoudre ? 

2- Pour chaque thème, la société se structure en différents courants, en désignant son représentant (démocratie liquide). Les représentants ne sont pas là pour apporter des solutions mais pour exprimer les besoins fondamentaux de ses électeurs (besoin de sécurité, d'appartenance, etc). 

3- Les représentants se réunissent autour d’une table de négociation et expriment les besoins de leurs électeurs.

4 - La société civile, via les associations et les think tank, traitent ces besoins et proposent des solutions. De nombreux aller-retours commencent entre la table de négociation et la société civile.

5 - La négociation est régie par une règle de coopération, de fraternité : on est là pour résoudre un problème en ne laissant personne au bord du chemin. On essaye de trouver des solutions qui répondent aux besoins de tous. On peut demander le remplacement d'un négociateur qui ne se montrerait pas coopératif. 

6 - Le but de la table de négociation est aussi de faire évoluer les opinions dans la société, de faire faire du chemin à tous pour arriver à des solutions acceptables par tous. 

En fin de compte, il s’agit donc bel et bien de supprimer l’assemblée nationale actuelle afin de la remplacer par des tables de négociation coopérative entre les différents pans de la société, pour chaque thème de débat.  

En restant à votre disposition,
Bien cordialement,
Floraine Cordier

Score : 0 / Réponses : 0

----

Remplacer le Sénat par un Conseil des Cinq Cents Citoyens tirés au sort qui examinerait tous les projets de lois d'initiative citoyenne.

Score : 0 / Réponses : 0

----

Pour une réglementation de la dépense des institutions et des élus 

Aujourd'hui nos élus et politiques demandent beaucoup d'efforts au Français économiquement. 
Venant du privé, je dois rendre des comptes à mon entreprise sur les dépenses que j'engage au nom de mon entreprise ou les frais que j'ai engagé personnellement.

Il me semble inconcevable que encore à ce jour l'argent des Français soit distribué gracieusement sans justification particulière. 

À quand une loi ou charte réglementant les dépenses de l'assemblée, du senat, des élus, des corps ministériels... des groupes parlementaires, la rémunération des élus et de leurs employés.
Car à ce jour il existe encore trop d'ombres au tableau. 

Comment l'argent est il dépensé ?
Sur quels justificatifs ?
Quelle est la pertinence de ces dépenses ? 
Nos élus ont ils trop de droits par rapport aux citoyens? (salaires,attachés parlementaires, réserve parlementaire,double cotisation retraite et chômage...) 

Je compte sur votre participation pour changer ce fonctionnement inadmissible et injuste à l'égard des Français.

Score : 0 / Réponses : 0

----

Rétablir le lien entre l'élu et la circonscription qui l'a élu. Trop de députés une fois élus mésestiment ce lien essentiel et reviennent voir leurs électeurs, de manière directe et frontale uniquement lorsque des élections sont en vue. Il ne s'agit pas d'obliger les député(e)s à rester un nombre X de jours dans leurs circonscriptions mais les induire à développer un certain nombre de rencontres et d'échanges, numériques ou physiques, destinés à faire connaître le rôle du député(e) qui peut être autre chose qu'une simple voix destinée à la majorité présidentielle ou à l'opposition. Un(e) député(e), c'est une circonscription qui s'exprime même si certains n'ont pas voté pour lui. Que se soit sous la forme d'un compte-rendu mensuel, de rencontres, d'initiatives populaires ou de stages/séjours pour les plus jeunes, restaurer le lien entre politique et nation passe par donner une culture politique, même petite, aux jeunes de demain, ceux qui demain ne pourraient alors plus dire qu'ils ne savaient pas.

Score : 0 / Réponses : 0

----

J'aimerais que l'on change la façon de rédiger les textes. La loi devient souvent illisible car pour comprendre un point de droit il faut se rapporter à des foules d'articles de lois différentes. Et donc à chaque fois que l'on légifère si nous pouvions réécrire les textes par annule et remplace et non par auditif à un article existant. Cela rendrait la lecture plus simple pour tout un chacun. Chaque nouveau texte devrait être autoportant et les anciennes lois devenir caduc. De même pour les décrets d'applications qui devraient faire l'objet d'un contrôle plus approfondis du parlement pour s'assurer qu'il respecte bien la volonté du législateur. Enfin chaque administration responsable de la future application des textes devrait être associé quand à la faisabilité et le coût de la mise en œuvre. Trop de lois ne sont que lettres au père noël. Et bien souvent nous avons des administration qui faute de moyen ne font que faire semblant. Et faire semblant coûte plus chers que ne rien faire à tout prendre. Le politique doit avoir le courage de prendre son temps au lieu d'encourager de façon démagogique une inflation des textes que les fonctionnaires et plus généralement les français sont bien en peine de suivre.

Score : 0 / Réponses : 0

----

Bonjour mes compatriotes ,
Je pense que si on veut changer la façon de faire de la politique, "en mode XXI ème siècle ", commençons par construire au vrai sens du terme un nouveau site pour  notre assemblée nationale . On peut faire appel aux jeunes talents architectes et quand le bâtiment va, tout va !!
Bises

Score : 0 / Réponses : 0

----

Moitié des députés élus à la proportionnelle intégrale sur liste nationale :.
- pas de proportionnelle sur des zones plus restreintes car cela empêche les petits partis d'être représentés ( c'est une manière de casser en douce le principe de la proportionnelle,
- pas de proportionnelle cache misère genre 50 députés sur 400 où un parti qui représente 10% des Français aurait glorieusement 5 députés c'est à dire 1,25% des députés ( et c'est pourtant bien ce qui se dessine).
Pourquoi la moitié des députés à la proportionnelle et pas la totalité :
pour éviter un parisianisme écrasant et aussi pour qu'il reste dans chaque circonscription géographique ( agrandie du coup) la possibilité d'aller voir un député localement pour aller lui parler et éventuellement lui demander des comptes.

Score : 0 / Réponses : 0

----

Voici quelques idées qui pourraient faire avancer le schimibilick.

1- Diminution drastique du nombre de députés et de sénateur - un nombre de 100 par chambre suffirait.

2- Passage à maximum 10 super region avec beaucoup plus d'autonomie et être plus proche des citoyens.

3- Une decentralisation digne de ce nom avec le transfert d'un grand nombre de ministère dans les regions. 

4- Pris en compte d'un quorum lors de toute élections + prise en compte des votes blancs + obligation du vote => invalidation de l'élection et non representation des candidats.

5- Mise à disposition de la feuille d'imposition de toutes les personnes élues (politiques, syndicales...)

J'espere que cela fera avancer le débat.

Score : 0 / Réponses : 0

----

Nous avons trop vu de lois votées basées sur des mensonges éhontés.
Les responsables politiques et fonctionnaires validant et présentant ces propos devant la représentation nationale devraient voir leur responsabilité pénale engagée.

Score : 0 / Réponses : 0

----

**Création d'un comité citoyen sur le secret **:   
L'acuité politique des citoyens comme des élus s'appuie sur les informations dont ils disposent.    
Bien que légitimes, les différents niveaux de classifications sont autant de barrières mises en place par les gouvernements successifs à une partie de ces informations, permettant à un gouvernement éventuellement malintentionné de fausser le débat démocratique, et même de cacher les preuves de sa manipulation.    
    
De plus, la catégorie "archives incommunicables" crée par le gouvernement Fillon, concernant les documents sans date de déclassification prévue, est un écueil aux travaux légitimes des historiens.     
    
Dans le but de rétablir la confiance entre les citoyens, les élus et le gouvernement, nous proposons, sous la tutelle de la Commission consultative du secret de la défense nationale, la création d'un comité citoyen sur le secret, composé de 10 membres (tirés au sort ou proposés par les groupes politiques au pro-rata de leur représentation à l'assemblée), habilités "Très Secret Défense" pour la durée de leur travaux, qui seraient chargés de confirmer, de reclassifier ou de déclassifier les classifications passées et présentes.

Score : 0 / Réponses : 0

----

Un des problèmes importants que connaît notre démocratie aujourd'hui est que beaucoup d'hommes politiques font carrière depuis la sortie de leurs études (Sciences Po, ENA...) jusqu'à l'entrée dans leur tombe.  Afin que la politique ne soit plus un métier à vie mais une parenthèse dans une vie professionnelle, je propose, au delà du cumul limité  des mandats dans le temps (2 ou 3), de faciliter le retour des élus dans leur vie professionnelle, en mettant en place des aides et aménagements liés à l'abandon d'une activité professionnelle ( par exemple : la suspension du contrat de travail). Cela permettra d'une part de favoriser l'élection de citoyens connaissant la «vraie vie», et d'autre part, cela pourrait motiver les jeunes voulant s'engager en politique mais craignant de mettre en péril leur carrière professionnelle à se lancer.

Score : 0 / Réponses : 0

----

Nos démocraties et la Politique ont besoin de renouer avec le bon sens, la confiance et la transparence, ces valeurs égarées sur la route d'un certain progrès qu'une science sans conscience organise sans garde fou ce qui conduit inévitablement et rapidement à un effondrement systémique ! 
Vous avez encore le pouvoir de reprendre ce bien volé et les citoyens vous y aideront. Il vous faut pour cela, renouer avec une véritable démocratie active et participative dans l'engagement d'une responsabilité sociétale et environnementale avant que l'on ne vous accuse de crime contre l'humanité !

Score : 0 / Réponses : 0

----

Il y a près de 1400 salariés à l'assemblée nationale, c'est l'équivalent d'une bonne entreprise de taille intermédiaire française.

Les modes de management doivent évoluer au sein de l'assemblée afin de pouvoir inclure l'ensemble des salariés afin qu'ils puissent faire évoluer leur mission dans le sens voulu par les questeurs, qui donnent l'orientation à l'évolution de l'assemblée.

Des modes de managements coopératifs existent aujourd'hui, qui s'appuyent sur les méthodes agiles et sur l'entreprise libérée qui donnent de très bons résultats pour faire évoluer les Hommes et les mentalités dans une structure.

Nous avons avec Humanaa mis en place l'entreprise libérée de manière opérationnelle avec management libéré dans les entreprises (Vinci, Siemens, Leyton, Lapayre) et dans les institutions publique (ARCEP et MSA).

Nous travaillons en deux étapes :
- Une étape diagnostic, afin de connaître la culture managériale de l'organisation,
- Un accompagnement opérationnel, qui permet de mettre en place et de faire vivre le changement,

De manière opérationnelle cela signifie :
- Travailler sur le cadre (vision, valeur, etc.) avec la direction d'une insitituion,
- Décliner le cadrage au niveau des différents service,
- Faire en sorte que les managers qui transmette ce cadre passe d'une posture hiérarchique à une posture de facilitation et de bienveillance
     - Afin de libérer la parole et de faire émerger des idées,
- Faire participer les équipes à l'amélioration de leur travail
     - Pour gagner en plaisir, en performance, en motivation, en implication et -autonomie,
- Ancrer les pratiques dans le temps,

Voici 3 exemples concrets :
1/ Vinci: (gestion de crise) Film de 8 min
Vidéo Témoignage : https://www.youtube.com/watch?v=kDXhMHmnHOI&t=1s
Sur un site de Vinci à Roissy (150 personnes), une grève en plein été 2015, des performance économique faibles et une réorganisation qui a suscité l’annonce d’une prochaine grève et de fortes tensions au sein des équipes.

Durant 4 mois, les équipes ont construits eux-même une nouvelle organisation qui correspond aux réels besoins du terrain, ils ont su tirer les enseignements des dysfonctionnements et définir un nouveau type de relation : la coopération.

Depuis notre accompagnement :
Les managers ont su changer de posture et sont à l’écoute de leurs équipes, Les équipes sont plus impliquées et sentent qu'ils contribuent à la performance de leur site, Le DG a retrouvé la confiance en ses équipes, et compris a quel point le terrain pouvait être source de valeur ajoutée.

2/ MSA : (gestion de crise)
Nous avons accompagné la fusion de deux caisses à Périgueux et à Agen, les équipes étaient rétissante à travailler ensemble et les organisations de travail différentes.

Suite à un diagnostic terrain nous avons compris que l’objectif nécessaire était de mobiliser les équipes à retirer le « meilleur des 2 », à travers une vision et des valeurs communes. Aujourd’hui les équipes ont construit une harmonisation des fonctionnements et défini des pratiques de travail commune.

3/ Lapeyre : (amélioration des conditions d’organisations)
A l’échelle d’un magasin 50 personnes qui voulait impliquer ses équipes dans la performance et gagner en efficacité dans ses services.

Nous avons travaillé sur la vision et les valeurs avec le dirigeant, après avoir fait un diagnostic complet de la typologie managériale de l’entreprise.

Nous avons accompagné les managers sur la posture afin qu’ils soient en mesure de prendre en compte les remontées du terrain, d’accepter les remarques. Puis les équipes ont travaillé sur la démultiplication des objectifs en plans d’actions concrets.

Cordialement, Marc Dore, Humanaa

Score : 0 / Réponses : 0

----

Renforcer l'éducation civique que ce soit lors de la scolarité ou lors de la vie professionnelle. A chaque cycle scolaire (primaire, collège, lycée, université ou autres) ainsi que tous les 10 ans en entreprise par exemple, une à deux journées pourraient être consacrées à la présentation de l'organisation de l'Etat (de l'échelon national jusqu'à l'échelon de la commune en passant par les communautés d'agglomérations), le mécanisme de vote d'une loi,etc. Un grand nombre de citoyens ne s'investissent pas par méconnaissance des "rouages". Chaque citoyen devrait connaître les moyens de faire entendre sa parole ou d'agir  et pour cela il faut savoir "Comment ça marche".

Score : 0 / Réponses : 1

----

Suppression des tribunaux d’exception, en particulier des ordres professionnels de santé qui sont un déni de la démocratie

Score : 0 / Réponses : 0

----

La  proposition de Mr de Rugy est intéressante en théorie, mais pratiquement on a vu ce qu’il est advenu de l’avis des citoyens qui ont répondu au référendum sur le traité européen. Il n’en a été tenu aucun compte et il en est de même dans de nombreux domaines. C’est aussi le sort que connaissent certaines pétitions même lorsqu’elles sont signées par des centaines de milliers de personnes, comme dernièrement celle du Pr Joyeux demandant le retour du vaccin DTpolio sans aluminium. Les citoyens sont de plus en plus conscients - et révoltés – du passage en force de certaines lois et de la soumission du monde politique au pouvoir des lobbies, pour ne pas dire de la corruption de hauts responsables par des firmes pharmaceutiques, chimiques ou autres, et qui échappent à toute sanction comme l’a montré encore récemment le classement du scandale de l’amiante. Autre exemple : voter une loi qui exclut sans raison valable de l’école et de la crèche des milliers d’enfants, les stigmatise et les désocialise, est si contraire au respect des droits de l’homme qu’aucune autre démocratie n’a voulu l’adopter. Les citoyens sont de plus en plus nombreux à ne plus voter parce qu’ils n’ont plus aucune confiance dans le fonctionnement démocratique de nos institutions. Ce n’est pas qu’ils doutent de la probité de tous leurs élus mais qu’ils doutent de leur capacité à résister. Faire participer les citoyens n’aura de sens que si on les écoute. Sinon, ce sera encore, comme beaucoup le pensent, une entreprise de com.

Score : 0 / Réponses : 0

----

Seul le peuple (par l'initiative citoyenne) et l'assemblée nationale doivent avoir l'initiative des lois. Le pouvoir exécutif est là pour exécuter. Le président est là, je cite l'article 5 de la constitution, pour veiller au respect de la constitution, arbitrer, et assurer la continuïté de l'Etat, pas pour prendre des initiatives législatives ou constitutionnelles (quand il le fait il se rend coupable de forfaiture).

Score : 0 / Réponses : 0

----

La nomination des ministres et hauts fonctionnaires (chefs de cabinet, délégués généraux, etc.) devrait être validée individuellement par l'assemblée nationale. La révocation des ministres et hauts fonctionnaires devrait être possible individuellement par l'assemblée nationale ou par le peuple (initiative citoyenne).

Score : 0 / Réponses : 0

----

Faire des vidéos et donner la parole aux peuples

Score : 0 / Réponses : 0

----

Pour le vote obligatoire, la prise en compte du vote blanc, le droit des citoyens à révoquer des élus.
Pour l'application du scrutin de Condorcet randomisé. 
Pour la convocation d'une assemblée Constituante tirée au sort (partiellement). 
Bref, il est peut-être grand temps de changer de République plutôt que d'appliquer des rustines à la 5ème ?

Score : 0 / Réponses : 0

----

Prendre en compte que les citoyens, qui ne votent pas où  plus c'est qu'ils ne croyent plus à tout ses politiciens, véreux magouilles parasites. Qui s'en mettent pleins les poches .Que , la justice, soit plus ferme, cette violences grandit de jours, en jours. Pour les femmes, nos jeunes, nos anciens, les citoyens . Cette politique des riches , alors que des citoyens ne s'en sortent plus taxé taxé sur tout. Les , politiciens, vivent dans un autre monde. Champagne, caviar, bling bling ! ! ! Sachez que la vie de milliers de personnes, vivent en dessous de la pauvreté.

Score : 0 / Réponses : 0

----

De nombreuses réformes sont nécessaires en vue d'améliorer notre démocratie, sa représentativité, et son fonctionnement.
Il me semble en premier lieu utile de rappeler qu'une nation est composée d'hommes et de femmes, qui ont tous le droit de s'exprimer.
Si l'utilisation des moyens digitaux peut faciliter les débats, et l'expression de la partie de la population la plus "connectée", le suffrage universel demeure encore la seule modalité satisfaisante de l'expression de la totalité des citoyens. Aussi, des assemblées constituées de représentants tirés au sort ne me semblent pas en phase avec les principes démocratiques.
En même temps, "l'offre politique", au sens noble du mot, doit être diverse, et la constitution des assemblées elles-mêmes représentatives de l'ensemble des opinions exprimées.
Le financement des campagnes doit être revu, afin que les "petites" formations ou les candidats indépendants ne soient pas découragés par le non remboursement éventuel des sommes nécessaires à leur campagne.  
Le scrutin majoritaire donc être remplacé par la proportionnelle pour les élections législatives. Elle sera assortie d'une prime au parti ayant recueilli le plus de voix (de l'ordre de 10 % des sièges à pourvoir), afin qu'une majorité stable puisse gouverner. Un député ne pourra voir son mandat renouvelé qu'une seule fois. 
L'existence du Sénat doit faire l'objet de réflexions approfondies. Aux premiers temps d'une démocratie, il peut être considéré comme un élément modérateur, gage d'une représentativité accrue. 
Aujourd'hui, ces fonctions ne sont plus réellement utiles. Le Sénat ne présente que des élus, qui votent pour d'autres élus, et il n'a pas réellement de pouvoir législatif, puisque en cas de désaccord avec l'Assemblée nationale, c'est celle-ci qui a le dernier mot. L'adoption des textes serait donc facilitée. Les finances publiques se trouveraient aussi opportunément améliorées !
En parallèle, la composition et le rôle du Conseil Economique, Social et Environnemental (CESE) doivent être revus. 
En premier lieu, cette assemblée ne doit évidemment plus être le simple lieu de retraite de personnalités cooptées pour leur seule notoriété, parfois établie dans des domaines bien éloignés des thématiques censées être traitées par le CESE...
La composition du CESE pourrait être revue ainsi :
* un tiers des membres désignés par les syndicats de salariés et les organisations patronales
* un tiers des membres composées de personnalités qualifiées dans les domaines de compétence du CESE. Ces personnalités seraient choisies par un collège composé des présidents de l'Assemblée nationale et de chaque groupe politique représenté dans l'hémicycle, ainsi que par les présidents de la Cour des Comptes et du Conseil d'Etat (les nominations devant être effectuées à l'unanimité)
* un tiers des membres élus sur les mêmes modalités que les actuels sénateurs. 
Les membres seraient élus ou désignés pour un mandat de deux ans, renouvelable une seule fois.
Le CESE pourrait rédiger des propositions de loi, que l'Assemblée nationale aurait l'obligation d'examiner.
Si l'Assemblée nationale vote les lois, il faut aussi que ces dernières soient appliquées. Combien de textes sont restés sans décret d'application ? La Constitution doit rendre obligatoire un observatoire composé de hauts magistrats indépendants, chargés de vérifier la publication des textes d'application des lois. A défaut de publication dans un délai à définir, une information sera faite au Président de la République, qui devra être obligatoirement publiée au Journal Officiel, et diffusée par voie de presse (selon des modalités à préciser).
Enfin, et sans épuiser le débat, l'impact de toute mesure législative ayant une incidence financière doit donner lieu à une évaluation. Celle-ci doit être réalisée conjointement par les services gouvernementaux et par la Cour des Comptes, selon des procédures et des échéances préétablies et adaptées aux mesures qui ont été prises (et dont les effets sont susceptibles de se produire à plus ou moins long terme).

Score : 0 / Réponses : 0

----

Faire passer en France un vote pour le maintien ou non du Sénat en précisant son utilité réelle.

Score : 0 / Réponses : 0

----

VISA veut interdire l'argent liquide et nous disons non.une pértition est en cours

Score : 0 / Réponses : 0

----

LA REPRESENTATIVITE DEMOCRATIQUE HASARDEUSE OU LE TIRAGE AU SORT DES SENATEURS. .
(ATTENTION : NOUVELLE PROPOSITION. VOTEZ ET ENVOYEZ VOS COMMENTAIRES A FSMOREAU@GMAIL.COM) .
Force est de constater, que malgré les avancées récentes pour introduire une meilleure représentativité de la « société civile » dans nos institutions dirigeantes, la majorité des dirigeants élus sont issus de classes privilégiées, ont un niveau de confort financier bien supérieur à la moyenne et ont généralement intégré le réseau des élites par des relations personnelles. Cet élitisme de fait, endémique à toutes les démocraties modernes est due au concept de « méritocratie » défectueux ; puisque de par le concept d’élection, ceux qui partent avec ces avantages ont une plus haute probabilité de gagner le pouvoir et de conserver ce dispositif qui s’oppose au principe démocratique (dêmos : Peuple, et kratos : Pouvoir). Cette imperfection est inhérente au système des ELECTIONS. .
D’autre part, malgré l’accessibilité accrue à l’information et à l’éducation, il est à noter que la complexité grandissante des structures de la société, la diversification et la libéralisation des cultures au sein de la mondialisation, tend à désengager le simple citoyen de la Politique. Les taux de participations chutent et le citoyen lambda ne consacre que peu de son attention aux orientations prisent par ses représentants, excepté si son emploi, pouvoir d’achat, liberté, privilèges ou identité en sont directement affectés. .
Troisièmement, il faut reconnaitre que cette complexification de la société ne permet en réalité aux dirigeants, que de prendre des orientations politiques générales, mais pas d’en définir leurs mises en œuvre, cette tâche, bien que contrôlée par les ministres, incombe en réalité aux centaines de milliers de fonctionnaires experts de leur domaines et sélectionnés pour leurs compétences plus que pour leur charisme ou influence. Aussi, il n’est pas essentiel d’avoir fait des études de chirurgie pour prendre des décisions de santé publique, ou d’avoir fait les beaux-arts pour être ministre de la culture. Les experts mettent en œuvre les décisions et lois des dirigeants. Donc les compétences de nos dirigeant sont somme toutes, essentiellement des dispositions à la communication, pour se faire élire. .
Ma proposition offre une alternative aux imperfections de l’Election, ou plutôt un remède qui rend au peuple sa véritable représentativité, C’EST LE TIRAGE AU SORT.
.
Bien que favorisée dans la démocratie Athénienne ou Vénitienne sous l’auspice de la religion (le hasard vu comme décision de divinités), et désavoué par la classe intellectuelle privilégiée de la révolution, le tirage au sort subsiste dans nos démocraties modernes dans un domaine lourd de conséquence. En effet, dans le domaine judiciaire, la représentativité qui décidera du destin d’un criminel majeur est assurée par le tirage au sort des jurés. D’autre part, même en politique, les sondages sérieux utilisent le hasard du tirage au sort pour légitimiser leur représentativité. .
Maintenant posez-vous la question : Si vous étiez tiré au sort pour intervenir directement avec un pouvoir décisionnel équivalent aux chambres d’élus pour un mandat d’un an (et un mois de formation et de passation) non-renouvelable afin de neutraliser la corruption, le népotisme, les blocages de la partisannerie, et d’assurer une véritable représentation populaire dans un Senat citoyen. (Rôle à distance, rémunéré avec la garantie de retrouver votre emploi à la fin de votre mandat.). Refuseriez-vous ce rôle ? .
Bien que souvent saboté par les partis, nombre d’expériences sont aujourd’hui en cours en Irlande, au Canada, en Islande, en Allemagne, aux USA, etc. afin d’ajouter le tirage au sort au processus démocratique. .
Il est probable que pour une vaste majorité, vous ne considéreriez toutefois jamais de faire campagne pour des élections. Et ce pour toute sorte de raisons. Le hasard serait-il une option de l’institution démocratique afin d’en éliminer les travers ? .

Score : 0 / Réponses : 0

----

Transparence des Élus que Chaque Député De Circonscription exemple 6 e Du Val De Marne Justifie Auprès Des Citoyens De Vincennes ST Mande Fontenay Les Frais Réels Engages D Autre Part  Exiger Un Casier Judicaire Vierge Coome Obligatoire Pour Profession assermenté

Score : 0 / Réponses : 0

----

Transparence des frais et casier judiciaire vierge

Score : 0 / Réponses : 0

----

Rendre obligatoire le recours au référendum pour garantir le respect de la décision populaire.

Score : 0 / Réponses : 0

----

Il n'est pas si facile que cela pour des fonctionnaires d'état ou territoriaux de parler des sujets qu'ils connaissent  le mieux dans une instance nationale sans mandat de leur hiérarchie. Même en respectant le devoir de loyauté, réserve , etc...ce n'est pas très évident .Il me semble que les citoyens contributeurs devraient avoir une protection juridique spéciale  pour pouvoir s'exprimer sur des sujets en lien avec leur métier comme pour le délégué syndical .Personnellement , je sais que ma hiérarchie me tomberait dessus dans l'instant. même si je contribue en toute loyauté. etc....

Score : 0 / Réponses : 0

----

Contrôle des élus  et du travail fourni reellement

Score : 0 / Réponses : 0

----

Je propose de généraliser les concepts développés dans http://www.UniText.fr  qui viennent des projets industriels quand il faut gérer de grosses quantités de documents avec des liaisons, des recopies, des points de vue différents et des niveaux de synthèse différents. "Nul n'est sensé ignorer la loi" est malheureusement difficile à appliquer, et une facilité de lecture en mode web serait tellement bienvenue !

L'Industrie nous montre le "Lean Management", ainsi une idée a des causes, des effets, et des prétextes à discussion.  Voir : http://www.lesamisdeunitext.fr/sites/default/files/attached_files/20170510homeunitext.JPG . Il faudrait bien avoir les clefs de lectures, selon différents points de vue, selon que je suis employeur, employé, citoyen, propriétaire, locataire, chef de PME, agriculteur, consommateur, pollueur, justiciable,  dans les actes de la vie courante, avec mon voisinage, etc. et en effet comme cela est souvent cité dans cette consultation numérique éviter les textes à tiroirs qui se citent les uns les autres et finissent pas se contredire !

Aussi, selon le niveau d'information, il faudrait plusieurs niveaux de lecture, ainsi je saute la lecture d'une clef parce que je connais le sujet, ou parce que je ne fais partie de la définition du critère d'application, donc je survole jusqu'à trouver une clef qui me concerne ... Donc ne pas être obligé de balayer tout le texte détaillé si je ne suis pas concerné ou si connais déjà.

Score : 0 / Réponses : 0

----

Tirer au sort, par des méthodes statistiques, des citoyen.nes pour préparer les lois de deux types :
1) Celles pour lesquelles les parlementaires sont en conflit d'intérêt ( nombre de parlementaires, mille-feuille administratif, statut...)
2) pour les questions de société ( fin de vie, pma, dépénalisation du cannabis...), les parlementaires ne disposant d'aucune autorité morale sur leurs concitoyens.

Score : 0 / Réponses : 0

----

Humaniser les relations
Permettre une relation directe entre le citoyen et son député.
Depuis plusieurs années alors que les moyens de communications se sont démocratisés, les relations entre un député de circonscription et ses mandants se sont distendues. S'il est toujours présents sur les cérémonies, réunions officielles, événements locaux, force est de constater que les convives, donc les relations du député restent les même et qu'il ne s'agit pas de la population mais d'une élite locale. Le citoyen a certes la capacité d'adresser des courriers, des courriels ou de prendre rdv avec le député mais il faut être lucide, ces tentatives de contact passent sous les fourches caudines du filtre des fonctionnaires de l'assemblée ou des attachés parlementaires. Au mieux, certains arrivent à obtenir une entrevue directe après plusieurs mois mais pour quelques secondes, le député ayant une cérémonie ou une visite locale au cours de laquelle la population est tenue à distance. Au mieux, il serait possible d'obtenir un selfie à l'arrivée ou au départ de l'elu.
Ah, il y a bien quelques moments de proximité quand un élu se découvre des talents d'écrivain et qu'il procède à une séance de dédicace dans une librairie locale....mais cette proximité relève comme beaucoup de fois dans notre société d'un moment marketing.
La solution réside dans un système de contact ou l'élu serait en lien direct avec les électeurs. Il sait le faire lorsqu'il est en campagne et il serait bon que l'elu se considère en campagne permanente.
Cette distance n'est pas l'apanage du député mais de l'ensemble de l'administration française ou les interlocuteurs de l'administre même s'ils sont de qualité n'ont aucun pouvoir de réponse immédiate et directe. Au mieux, ils prennent en compte votre demande. Qui n'a pas vécu, ces plateformes telephoniques ou l'interlocuteur cite une fiche réflexe ou vous répète des informations que vous avez déjà mais ne peut solutionner votre problème. Qui n'a pas vécu, ses même choses face à un guichet, ou face à un gentil fonctionnaire à qui l'Etat a oublié de lui donner la compétence de solutionner le problème de son administré.
Rendre de l'humanité dans les relations permettrait de restaurer la confiance des citoyens et leur implication

Score : 0 / Réponses : 0

----

Ne pas supprimer de suggestion des Français qui en inscrivent sur ce site, c’est ça la démocratie. Qui a l’autorité pour supprimer celle-ci ou celle-là ?

Score : 0 / Réponses : 0

----

Limiter le nombre de députés, calculer leur salaire proportionnellement à leur présence et à leur activité en commissions. Supprimer le Sénat qui est inutile si l’on regarde à quoi consiste sa « mission » et son budget.

Score : 0 / Réponses : 0

----

Qu'il s'agisse de loi, de référendum ou d'élection, devrait être exigé un écart significatif entre les diverses propositions : par exemple 1% du corps électoral; à défaut, renouvellement de la consultation, après révision du projet ou des candidatures. Ainsi éliminera-t-on des décisions "de justesse" liées  à des aléas personnels, climatiques, etc.

Score : 0 / Réponses : 0

----

Plus de responsabilités partagées discutées  hors des assemblées d'élus peuvent faire rentrer les abstentionnistes dans la vie politique du pays.

Score : 0 / Réponses : 0

----

Entre le formel et le réel : choisir une ou plusieurs régions pour évaluer, sur une année, l'impact des lois entrées en vigueur ainsi que la pertinence des mesures d'application..

Score : 0 / Réponses : 0

----

Bonjour, ce qui me plaît dans autres idées, c'est justement imaginer pour l'avenir des projets utopiques mais qui peuvent un jour correspondre à notre société moderne, utiliser l'informatique le plus possible et laisser l'imagination de tous vagabonder, un peu comme un artiste devant une toile blanche, sans avoir fait d'études, uniquement avec son cerveau qui fonctionne .

Score : 0 / Réponses : 0

----

les violences familiale et conjugal c'est inadmissible  et qu'elles soit aider soutenue et envenimer par la gendarmerie française c'est encore plus inadmissible et inacceptable .il faudrait incorporer dans les loi : Le respect de la loi , Le respect des citoyens et de leur famille ; Le respect de la bienveillance , Le respect de la fidélité familiale et conjugal , Le respect de la vie des valeurs de vie et de conception de vie , Le respect envers les personnes et envers la famille des personnes ,La pénalisation du viol des droit fondamentaux de vie et de conception de la vie ,Le respect des droits fondamentaux de la vie et de la conception de la vie sur la terre , La pénalisation des violences conjugal et familiale au lieu de les aider et de les soutenir ces violence pour les envenimer encore plus comme c'est le cas actuellement .avec la gendarmerie française qui aide soutien et envenime ces violences que l'ont subit nous les victimes de ces violences inadmissible et inacceptable . ce qui est encore moins acceptable et admissible ce sont les méfaits et les délits qu'ils commettes envers la famille envers les personnes qui compose la famille et envers la vie des personnes qui compose la famille . ci joint ci dessous un exemple formel et concret des méfaits et des délits qu'ils commettes envers la famille et envers ceux qui la compose dont envers autrui et envers les premiers piliers fondamentaux de la vie et de la conception de la vie sur la terre : monsieur Philipe LAGARDE 
madame Marie Hélenne PICHOT
monsieur Philipe SALLES DE ST PAUL
qui ont commis des acte malveillants et délinquant de délits commis envers la famille et ceux qui la compose le :06.12.2011 au sein du tribunal de Bressuire dans les deux sèvres en France
monsieur Frédéric Mabileau gendarme a Nueil les aubiers 
qui a commis des acte malveillants et délits passible de poursuite pénal d'un ans d'emprisonnement et de 15000 euros d'amende . délits commis envers la famille et ceux qui la compose .il a commis par des délits de faux et usage de faux le :11.10.2014 au sains de la gendarmerie de Nueil les aubiers dans les 
deux sèvres en france dans le 79250.
pour que je soit ensuite convoquer devant madame Drochon délégué du procureur du tribunal de Niort dans les deux sèvres le 25 .11.2014 a Bressuire qui a commis le délits de violation de la bienveillance et du respect de la famille et de ceux qui la compose .
monsieur Nicolas Plaire gendarme a Nueil les aubiers 
qui a commis des acte malveillants et délits passible de poursuite pénal d'un ans d'emprisonnement et de 15000 euros d'amende . délit commis envers la famille et ceux qui la compose .il a commis par des délits de faux et usage de faux le : 30.01.2016 au sains de la gendarmerie de Nueil les aubiers dans les deux sèvres en France dans le 79250 pour que je soit convoquer devant le tribunal correctionnel de Niort le 02.06.2016 a 16 heure .20 minutes alors que c'est moi qui suis la victimes des violences conjugal et des manipulations mensongère .
toutes ces personnes malveillantes , délinquantes et mal intentionnées ont commis des délits de blasphème a l'encontre de la famille et du premier pilier principal de la vie sur la terre ce qui représente des propos haineux envers la famille , la bienveillance l'amour familial et conjugal et envers la conception de la vie sur la terre . et cela est du terrorisme envers la famille , la vie des gens et envers la conception de la vie sur la terre

Score : 0 / Réponses : 0

----

Le Sénat, constitué degrands électeurs (maires, etc) valide les textes votés en 1ète lecture à l'Assemblée. Participation dans cette validation.

Score : 0 / Réponses : 0

----

A propos de l'examen des pétitions à l'Assemblée Nationale.
Il m'arrive de signer des pétions proposée par Change.org, ... ou pas !
Je propose qu'à chaque proposition de pétition, la possibilité soit offerte de dire : Je ne suis pas d'accord avec cette pétition; 
Ne serait-ce pas ça, la vrai démocratie ?

Score : 0 / Réponses : 0

----

il faut obliger à plus d'équité ,plus  de représentativité entre  les différents courants d'opinions mème si ça n'arrange pas nos opinions personnels----c'est ca la DEMOCRATIE---il est scandaleux que des millions d'individus ne soient pas représentés

Score : 0 / Réponses : 0

----

APPLIQUER LES PRINCIPES FONDAMENTAUX ET PREVOIR DES MESURES DE CONTROLES ET DE RESPONSABILITES DISSUASIFS

Ex:  AGIR POUR L'INTERDICTION DE LA MENDICITE DES ENFANTS ET LA SCOLARISATION OBLIGATOIRE 
Alors que le froid revient, il est insupportable de voir des enfants roms particulièrement mendier et dormir dans la rue. Cette situation ne peut plus durer en France, pays des droits de l'homme et des citoyens, protecteur des droits de la personne humaine. 
Des contrôles réels doivent être mis en place. Il convient de donner des moyens efficaces aux maires pour agir. Le cas échéant, il convient aussi d'envisager des responsabilités des pouvoirs publics si rien n'est fait.

Score : 0 / Réponses : 0

----

Mettre en place un quorum nécessaire aux votes des lois, on ne peut sérieusement continuer à voir des lois d'importance majeure être votées par 17 députés comme pour la loi santé de Madame Touraine ou 77 députés pour la réforme de l'isf, sur 544 élus. Les élus doivent représenter le peuple votant et cesser de se cacher chez eux à chaque vote crucial mais tendancieux pour sa base électorale. Un quorum obligatoire de 50% voire des deux tiers des élus avec une majorité à 50% des présents renforcerait le poids de l'assemblée dans la prise des décisions et justifierait la réelle implication des élus lors des moments fondamentaux.

Score : 0 / Réponses : 0

----

Premièrement enlever toutes notions de pseudo assumez votre identité.
Participation aux votes, référendum etc.. par le système informatique nous payons bien nos impôts de cette manière.
Représentation parmi les élus de l'assemblée Nationale, des citoyens votants Blanc,Nul,et abstention inexistantes aujourd'hui.
Réduction du nombres des parlementaires et personnels immédiatement     - 30% ainsi que les salaires et avantages pourquoi attendre 2022.
Engagement des élus par contrat sur leurs propositions.
Demande de Remboursement des dépenses injustifiées des collectivités territoriales et administrations ou réduction de leurs budgets à venir.

Score : 0 / Réponses : 0

----

Une démocratie, c’est par le peuple… Alors il faut utiliser la proximité et l’outil informatique. Chaque citoyen devrait pouvoir poser LA question qui lui tient le plus à cœur sur un site de sa circonscription et visible par tous. Si cette question obtient un certain % ( par exemple 5% des inscrits), alors le député prends le relais en invitant un groupe constitué de 10 citoyens afin de lancer une consultation dans sa circonscription. Si cette dernière obtient un seuil à définir, alors elle est lancée dans la région puis si succès au niveau national. Nous avons les idées, les outils ALORS passons à l’action.

Score : 0 / Réponses : 0

----

Création d'une plate-forme des représentants d'un service chargé de faire respecter la loi et création des bureaux de déclaration de domicile qui existent dans les pays européens mais pas encore en France.

Score : 0 / Réponses : 0

----

Un lien entre parlementaires français et députés européens français devrait être établi afin de garantir la cohérence des orientations françaises lors d'un même quinquennat.

Score : 0 / Réponses : 0

----

Pour redonner confiance aux électeurs - environ 1 sur 2 ne vote pas - il faudrait que les candidats aux élections, une fois élus, s'engagent à respecter une charte démocratique écrite par des citoyens. ETHIQUE et DEMOCRATIE
Exemple, celle  qui a été rédigée par le Groupe des Citoyens Libres (Saint-Etienne, 42) --> http://42.collectif-roosevelt.net/charte_democratique/charte_democratique.html
Le texte actualisé aux élections législatives de 2017 peut être envoyé par Email.

Score : 0 / Réponses : 0

----

Simplifier l'attribution des pensions de reversions... le délai est purement scandaleux!!

Score : 0 / Réponses : 0

----

je souhaite que l état impose l'égalité de tous les citoyens(manuels et intellectuels) . Il est inadmissible que les citoyens manuels reçoivent toute leurs vie des miettes pour vivre à l'encontre de ceux qui font des études. Vous voulez mettre l apprentissage en avant alors préoccupez vous des  salaires attribués et leur évolution. il faut rétablir de la justice entre les hommes  bac+ 5  OK se retrouvent avec un bon salaires alors que l' APPRENTISSAGE+ 15 ANS se retrouvent automatiquement au SMIC chaque fois qu'ils changent d'entreprise. 
IL FAUT PARTAGER  ET RECONNAITRE LES MERITE DE CHACUN POUR DONNER ENVIE AUX JEUNES D'ALLER DANS CES FILIERES SINISTREES .
Notre dame n'aurait jamais été construite si cela reposait uniquement sur les architectes.
Je vous laisse à votre réflexion

Score : 0 / Réponses : 0

----

et pourquoi pas plusieurs présidents

Score : 0 / Réponses : 0

----

Retour Citoyen

Score : 0 / Réponses : 0

----

RENOVER ET DEMOCRATISER LA 3e ASSEMBLEE 
Rénover et démocratiser le CESE (Conseil Economique, Social et Environnemental). Avant son élection, E Macron déclarait vouloir modifier sa composition pour y insuffler les ‘forces civiles, associatives et citoyennes’ (Discours du 1/5/17 à La Villette). Prenons-le au mot ! Réservons au moins la moitié des sièges du CESE à des citoyens tirés au sort (en assurant une certaine représentativité en terme d’âges, de CSPs et de géographies). Renforçons ses liens avec le Parlement, notamment par l’obligation de sa saisine par le Parlement avant certaines lois (Est-il normal que la Loi Travail de Myriam El Khomri ne soit jamais parvenue au CESE en amont de son examen au Parlement et… des défilés dans la rue ?). Faisons-en un acteur du dialogue social (syndicats et patronat y garderaient leur représentation) et citoyen.

Score : 0 / Réponses : 0

----

Les votes blancs doivent être pris en compte, aujourd'hui les politiques nous obligent à voter oui non ou l'abstention, du coup ou les gens ne vont pas voter où ils votent à contrecœur.

Score : 0 / Réponses : 0

----

A propos de la politique de santé
Dans la loi de mars 2002, dite loi Kouchner, il était écrit (Art. L. 1411-1). - La nation définit sa politique de santé selon des priorités pluriannuelles.....
« Au vu de ces travaux, le Gouvernement remet un rapport au Parlement, avant le 15 juin, sur les orientations de la politique de santé qu'il retient en vue notamment de l'examen du projet de loi de financement de la sécurité sociale pour l'année suivante. Est joint à ce rapport l'avis de la Conférence nationale de santé. Ce rapport fait l'objet d'un débat au Parlement. 
Cette disposition n'a jamais été appliquée et l'article L1411.1 du code de la santé publique a été modifié. Dans sa dernière mouture (2016), il n'est plus fait mention du Parlement. La Stratégie nationale de santé qui détermine les orientations de la politique de santé pour cinq ans, va être publiée fin 2017, par décret, sans avis du Parlement.
Comment une démocratie peut-elle définir ses priorités de santé sans que le Parlement se prononce ?

Score : 0 / Réponses : 0

----

PROLONGER LA PERIODE DE CETTE CONSULTATION
La communication de cette excellente initiative n'a pas été suffisante par rapport à l'importance des enjeux. Du coup les contributions risquent de ne pas être à la hauteur en terme de nombre et de représentativité. Pourriez-vous faire une nouvelle communication (officielle, médias, réseaux sociaux, vidéo youtube, etc...) dressant un premier bilan des premières contributions, annonçant un prolongement de la consultation, et incitant davantage de personnes à contribuer ?

Score : 0 / Réponses : 0

----

Aujourd'hui nous vivons un anniversaire avorté, celui de la révolution d'octobre 1917 en URSS. Cet avortement est lourd de sens: il nous montre s'il en était encore besoin, qu'en 100 ans d'une histoire cruelle la démocratie mise en place en Russie n'a pas été suivie d'effet. Au contraire, elle a porté au pouvoir des fascistes pires que l'aristocratie régnante et abandonné tous les idéalistes à leur sort. Le moyen informatique de consultation ferait, en effet, moins de morts et plus de justice. Déjà, il permettrait de valider les élections sur le temps et la distance. Puis il autoriserait les initiatives sur présentation d'une réflexion collective. Enfin il faut saluer ici l'opportunité d'une consultation spontanée, gratuite et éloignée des pressions lobbyistes et politiques, sans qu'il soit vraiment besoin d'attendre leurs erreurs pour les sanctionner par les urnes tous les 5 ans.. Bravo et merci pour votre mail.

Score : 0 / Réponses : 0

----

Je connaissais l'initiative du Président de l'AN, que j'approuve, mais je n'ai découvert le site internet qu'hier! Je ne peux sérieusement lire l'intégralité des messages le site fermant demain. Je le regrette.
Je propose une structure qui pourrait s'intituler "Organe numérique permanent de propositions citoyennes" fonctionnant avec la Charte de consultation ci-dessus, accessible à chaque citoyen et lui permettant d'émettre des idées concernant la législation à élaborer, celle en cours de construction et celle en vigueur. Cette structure devrait, en préalable, donner en temps réel les lois en projet de discussions à l'AN.
Je ne suis pas favorable "au forum de discussions" l'informatique se prêtant pour moi,très mal à cet exercice.( Cela est dû certainement à mon âge! )
Je vous remercie d'avoir pu m'exprimer. Je compte bien participer à l'avenir, si ce site devient une réalité, aux débats citoyens et notamment sur ceux concernant les réformes constitutionnelles et les strates territoriales trop nombreuses et incompréhensibles.

Score : 0 / Réponses : 0

----

**"Les Lycéens au cœur de la Démocratie et du devenir de celle-ci"**

La mise en place d’un portail spécifiquement pensé pour les Jeunes serait très importante.
Notre association Démocratie Locale Participative du XVème a constaté lors des stages qu’elle organisait avec des lycéens en seconde que la **défiance qu’ils portent vis-à-vis de la politique tient sa source principalement d'un manque d’informations** quant aux possibilités offertes aux citoyens.

Ce portail pourrait s’organiser en plusieurs sections avec des **contenus explicatifs** d’une part, et des **espaces de discussion, modérés par des équipes pédagogique**s de concert avec des membres de l’Assemblée Nationale, d’autre part.

Voici un exemple de schéma d’organisation de la plate-forme suivant la "vie" d’une loi :

1. **Présentation de l’état du droit existant** sur l’objet de la loi mise en débat public : les avancées qu’elle a déjà portées en cas de révision et les écueils qu’elle doit annihiler. Des exemples concrets illustreront cette présentation introductive.

2. **Présentation du projet en cours de discussion** : ses objectifs et les avancées dans son élaboration, appuyés encore une fois de projections et d’exemples concrets.

3. **Ouverture d’un espace numérique dédié** à recueillir leurs réflexions et leurs propositions. Cet espace pourrait être segmenté selon les points de cette loi ou segmenté suivant les institutions partenaires de cette expérimentation. En effet, il pourrait être proposé des espaces de discussion, modérés par un/des enseignants d’un établissement volontaire, qui seraient ouverts à tous les lycéens de cet établissement. Afin de comprendre les attentes, notamment suivant les quartiers d’implantation ou les statuts socio-économiques, la lecture des échanges de tous ces espaces dédiés à des établissements volontaires serait accessible à tous les citoyens, sans négliger la sécurité des contributeurs.

4. **Analyse de la loi définitivement votée** avec les amendements proposés par les lycéens au cours de cette consultation et pris en compte dans la nouvelle loi.

5. Enfin, au terme de trois ans d’existence, ou bien lors d’un nouveau débat public concernant cette loi, les espaces d’échanges coopératifs par établissements, pourraient être ré-ouverts pour **recueillir les évaluations de cette loi et les améliorations à apporter** dans un article ou dans les décrets d’application.

L’intérêt de cette coopération est qu’elle démontre **l’importance des travaux des députés** de l’Assemblée Nationale auprès de jeunes citoyens qui auront bientôt le droit de les désigner et qu’elle n’empiète pas sur la légitimité et les responsabilités des membres de cette Assemblée.

Cette démarche de concertation proposée par l’Assemblée Nationale, n’excluant aucune autre forme de concertation, intégrerait **"Les Lycéens au cœur de la Démocratie et du devenir de celle-ci"**.

**L'association DLP15** (http://www.dlp15.org) est prête à coopérer pour mettre en œuvre une expérimentation.

**Pour connaître nos activités** : https://twitter.com/DLP_15

Score : 0 / Réponses : 1

----

Il serait souhaitable d'évaluer régulièrement la pertinence ou pas d'une lois votée de même que : eventuellement "l'effet cobra" qu'elle a pu générer

Score : 0 / Réponses : 0

----

La question des algorithmes de la démocratie numérique - quelle que soit sa forme - me semble essentielle. Un thème capital et concret, qui doit être étudié.

Score : 0 / Réponses : 0

----

Ne devrait-on pas commencer par se demander comment renforcer la participation des élus eux-mêmes ? je n'arrive pas à comprendre qu'il soit possible que 63 députés sur 75 présents sur 577 députés puissent voter une loi aussi importante et lourde de conséquences que celle qui OBLIGE que nos tous petits subissent une vaccination de 11 vaccins, comme je ne comprends pas que des êtres responsables passent de 3 à 11 vaccins, ce qui semble énorme ! et ceci sans entendre les avis opposés. Ne devrait-il pas y avoir l'obligation d'une représentation de votant à par exemple 80%, ce qui amènerait peut-être plus de sagesse.

Score : 0 / Réponses : 0

----

Simplification législative

Comment les citoyens peuvent aider à une simplification législative nécessaire à notre pays ?

La France est un pays complexe qui a de part son histoire a une législation et des règles complexes et ce dans tous les domaines.

Les règles étant complexes, elles ne sont pas toujours comprises ou trop souvent détournées.

Les exemples au quotidien sont légions, et l expérience des citoyens forte utile pour savoir quoi simplifier.

Une fois ce travail fait il faudra muscler la réponse de la justice aux contournements d un droit français ainsi renouvelé.

La France ne peut plus ce permettre d avoir des lois et règles parmi les plus sévères des pays occidentaux avec l application la plus mole.

L idée est ainsi de lancer un grand nettoyage républicain de nos règles et Lois.

Score : 0 / Réponses : 0

----

Thématique à mi-chemin entre la thématique : "consultation en amont des textes" et "participation numérique, participation présentielle".

Proposition issue d'un groupe de travail local.

Mettre en place un site national pour reccueillir en continu, c'est à dire de façon non bornée dans le temps, les opinions des citoyens sur des thématiques proposées par la plateforme et des thématiques qui peuvent être proposées par les citoyens.
Ce site devra proposer des contributions rendues anonymes pour le grand public et faire l'objet d'une modération.
L'objectif est de pouvoir consulter un temps réel, un état de l'opinion sur des thèmes définis et de façon géolocalisée, sous forme de baromètre indicatif.
Le site pourra proposer à cet effet un tableau de bord des thèmes et des contributions sur des critères définis, avec le nombre de contributeurs. Ces critères pourront être étendus en fonction des demandes des contributeurs. 
La difficulté avec une plateforme telle que celle-ci (réponses sous forme de blog ou de commentaires) est que les contributions doivent être analysées de façon subjective, puisque que les critères objectifs n'ont pas été définis.
Cette plateforme pourrait également être un lieu de restitution d'animations citoyennes en présentiel (avec des critères de restitution à définir).
Finalement, il pourrait être judicieux qu'une commission spécifique soit créée au parlement pour gérer sous forme d'amendements ou de propositions de lois, des projets ayans fait l'objet d'une remontée citoyenne suffisamment représentative. Néanmoins, il est important que les parlementaires, demeurent des "médiateurs" de l'opinion car la prise en compte directe de l'opinion peut avoir des effets non conformes aux fondements de notre constitution.
Nous sommes à votre disposition pour contribuer à vos travaux

Score : 0 / Réponses : 0

----

Bonjour. Bien que notre pays ait encore quelques progrès à faire pour assurer une couverture totale d'"Internet pour tous", je pense que l'avenir est au vote électronique pour tous. Cela permettrait des économies considérables en termes d'organisation, et donc de multiplier les recours à l'expression démocratique populaire (référendums, etc.). Bien entendu, il faut que la sécurité (au sens large du terme) de ce vote électronique soit garantie. La blockchain semble être une technologie prometteuse pour ce faire (cf. les expérimentations en Ukraine). Je demande donc que l'administration lance une étude sérieuse sur ce projet.

Score : 0 / Réponses : 2

----

Et une EMISSION TV interactive ?

Bonjour ayant travaillé de nombreuses années dans le milieu de la TV, il me semble que l'outil internet devrait être associé à une émission de télévision intéractive sur la chaîne parlementaire et une web TV propre afin que les projets soient travaillé et soumis en amont de manière globale : Internet + TV afin que tout le monde puisse interagir même celles et ceux qui ne sont pas  l'aise avec le numérique ou n'y ont pas accès.

La construction s'articulerait sur la base d'internet et accompagnée en support explicatif et constructif via l'émission.

je pourrai développer le concept sans souci ICI ou directement.

Score : 0 / Réponses : 0

----

Transformer le Sénat pour rendre l'Assemblé Nationale plus efficace.

Il faut transformer le Sénat pour en faire une assemblée véritablement représentative de toutes les composantes de la société française. Une assemblée qui serait complémentaire de l'Assemblée Nationale (dont les députés semblent parfois déconnectés de la vie réelle). Les sénateurs seraient ,pour moitié, de "simples" citoyens tirés au sort et représentant l'ensemble des catégories socio-professionnelles. Un quart serait constitué d'élus locaux, tirés également au sort. Le dernier quart serait formé de représentants syndicaux (salariés et patronat). Cette assemblée aurait deux rôles principaux : 

- Faire remonter jusqu'à l'Assemblée Nationale les problèmes concrets rencontrés sur le terrain par l'ensemble ou une partie des citoyens avec l'obligation pour les députés de débattre régulièrement de sujets soumis par les sénateurs.

- Donner son avis par un vote  sur l'ensemble des lois votées à l'Assemblée Nationale. Une loi n'ayant pas obtenu la majorité au Sénat devrait repasser par l'Assemblée Nationale.

Score : 0 / Réponses : 0

----

-	Réseaux sociaux comme moyen massif d’intéresser les gens (jeune = avenir) à l’action publique, comme moyen massif d’informer et faire réagir en temps réels (sondages, opinions…) /Snapchat, Facebook, Instagram, Twitter…
-	Inculquer dès le plus jeune âge, via l’école notamment, l’habitude de vie publique et numérique.

Contribution collective du comité LREM Rennes Centre Sud

Score : 0 / Réponses : 1

----

Evasion fiscale en France = 80 milliards d’euros par an.
Travail non déclaré en France = 30 milliards d’euros par an.

Et si nous mettions en place un vrai service de lutte actif et efficace afin d’aider la France à combattre ses virus qui tuent notre pays ?

Arrêtons de laisser les élus se cacher derrière l’Europe pour ne rien faire.

Score : 0 / Réponses : 0

----

Une excellente initiative qui mérite d'être prorogée au moins jusqu'à la fin de l'année, avec une plus grande publicité auprès du grand public (pour ma part, découvrant l'initiative hier, impossible en si peu de temps de lire et contribuer aux nombreuses questions et propositions déjà  faites). Dans un soucis de simplification, ne devrait-on pas aussi utiliser les outils numériques pour faire un état des lieux publique des lois existantes et de leur efficacité (évaluer conjointement par des élus/experts/citoyens), afin de supprimer celles désuètes ou inefficaces et valoriser/améliorer les autres ?

Score : 0 / Réponses : 0

----

Il me semble que nous avons les moyens techniques d'une démocratie directe et que le mandat représentatif n'a plus de raison d'être.
Le parlement devrait se transformer en une administration de création et d'abrogation des lois, sous l'autorité des citoyens.
- l'origine ou la fin d'une loi devrait se faire sur demande d'une partie du peuple (50000 personnes)
- le rôle du "parlement" serait de consulter : des experts, des organismes (collectifs, associations...) ainsi que les idées soumises par des individus, afin d'établir une loi juste pour tous, simple et claire. Tous les éléments de réflexion étant consultables par le peuple.
- l'adoption de la loi se ferait par un vote citoyen :
            - En premier passage, la loi serait soumise à l'accord sur les termes employés et son objectif. Si 50% des votants soulèvent un même problème, elle doit être rectifiée
             - En deuxième passage, l'adoption du texte de loi : les citoyens ayant 1 mois pour voter, elle est définitivement adoptée si seulement 70% des votants sont d'accord. Sinon elle devra être rectifiée et représentée au peuple.
              - toute loi concernant une partie de la population doit se faire avec la participation de celle-ci, même si elle n'a pas le droit de vote. 
Le vote restant non obligatoire. 
Les membres du "parlement" étant révocable de façon collective ou individuelle, sur demande citoyenne et avec un vote majoritaire à 60%

Concernant le budget, les citoyens devraient choisir et voter la destination de l'argent de leurs impôts.

Ce sont justes quelques idées que je vous soumets pour alimenter la réflexion

Score : 0 / Réponses : 0

----

Dès lors qu'une question citoyenne récolte 500 000 signataires, les parlementaires devraient être obligés d'examiner le sujet dans l'année.

Score : 0 / Réponses : 1

----

Il faut baisser réellement les charges qui pèsent sur le travail, et ne pas se contenter de quelques pourcentages qui ne donnent pas de création d’emploi. 
Tabler sur une limitation à 100 euros de charges (patron et salarié cumulés) jusqu’à un salaire de 2000 euros, avec passage aux 32 heures, et création d’emplois, pas de recours aux heures supplémentaires car cette mesure viserait à lutter contre le chômage. Un peu de solidarité dans ce pays....
Augmentation conséquente des contrôles et de la sanction du travail non déclaré.
Taxation des mouvements boursiers qui ne créent aucune richesse collective.
Taxation des produits importés de pays pratiquant l’esclavage moderne.

Score : 0 / Réponses : 0

----

Améliorer la représentation citoyenne passe par la réduction des biais de "sélection naturelle" des parlementaires.
Selon une étude du chercheur Luc Rouban, parue dans les Cahiers du Cevipof (CNRS) 2011, “parce que l’absence de risques professionnels encourage davantage les salariés du public à candidater”, la proportion des enseignants et fonctionnaires est très largement supérieure à l'Assemblée que dans la population française. Cela vaut d'ailleurs pour d'autres institutions.
Dans la "vraie vie", l'absence de risque n'existe pas.
Ma proposition : demander à tous agent de l'Etat de démissionner de la fonction publique avant de briguer un mandat de député (et de sénateur).

Score : 0 / Réponses : 0

----

Un des risques de la Démocratie c' est de donner le pouvoir à ceux qui veulent le pouvoir. Vouloir le pouvoir est presque contradictoire avec les qualités morales nécessaires à son bon exercice.
Le tirage au sort, la providence, peut être une solution s' il s' accompagne d' un minimum d' enquête, mais comment et sur quels critères choisir les enquêteurs?
A un moment, il faut que des personnes reconnues comme porteur des valeurs et ayant les qualités nécessaires: engagement, sensibilité, intelligence, altruisme, amour du pays; soient désignées et proposées par ceux qui veulent les recommander. Des listes pourraient être ouvertes en lignes, et un niveau d' adhésion de par exemple 1000 citoyens, amènerait au niveau correspondant à proposer un mandat à la personne proposé. Un quota de mandat de   30%, serait dans un premier temps réservé à ce mode de désignation qui permettrait d' intégrer différents représentant de la société.

Score : 0 / Réponses : 0

----

Concernant la communication relative à la participation citoyenne.

On a ici il me semble deux problématiques à distinguer et à rapprocher de logiques de flux et de logiques de fond.

Tout doit être fait pour amener les citoyens à venir participer à des actions citoyennes. On est ici dans une logique de flux.

Tout doit être fait aussi pour valoriser à la fois :
- La production des citoyens dans le cadre des appels à participation. Il va falloir trouver des moyens de restituer le volume comme la diversité des contributions.

- Les textes de loi dans leur version finale : l’accès au contenu brut comme la mise en oeuvre de divers outils de vulgarisation. Les sites LégiFrance et Service public remplissent une fonction déterminante. On ne peut que louer leur qualité sur le fond. Les points d’amélioration sont peut-être à chercher du côté de la forme et de la communication : les faire connaître davantage et diversifier les approches pour aller vers du multimédia : site web + téléchargement de fiches et/ou d’infographies + vidéos courtes + Quizz et jeux + Parcours interactifs simples.

Score : 0 / Réponses : 0

----

Une idée originale serait une application mobile “Occupation citoyenne dans la file d’attente”. 
Nous sommes tous amenés à “faire la queue” et c’est généralement quelque chose qui nous agace. Pourquoi ne pas transformer ce temps d’attente en action citoyenne. Si je saisie mon smartphone, je peux avoir accès par exemple :
- A une rubrique ephéméride : “C’est un ## septembre #### que cette loi a été adoptée. Voici les # choses importantes qu’elle a changé dans les vie des Français”.
- Un sondage,
- Un résultat de consultation visuel optimisé pour mobile,
- La liste des consultations en cours,
- Des quizz ludiques sur les institutions ou certaines dispositions de la loi...

Score : 0 / Réponses : 0

----

Assemblées-Citoyennes-Locales-Ensembles-Décisionnaires
Instaurer 1 jeudi / mois non travaillé-payé aux citoyens volontaires qui formeront les assemblées-citoyennes-locales-ensembles-décisionnaires
avec un minimum de 7% des électeurs & ouvert à tous dans toutes les communes en vu de refonder le parlement
-
Celui qui décide pour le peuple doit-être & demeurer LA CONCERTATION DU PEUPLE, de chaque citoyen-citoyenne, 
elle doit-être organisée en Assemblées-Citoyennes-Locales-Ensembles-Décisionnaires
-
Il faut pour les décisions importantes, que ce soit les citoyens  qui décident ensemble par concertation de chaque citoyenne, de chaque citoyen.
"Nous portons chacune chacun, une partie de responsabilité de ce monde, mais notre voix ne compte plus, ce qui nous fait dévier gravement du bon sens commun. "
Décider ensemble, est la seule solution pérenne.

#AssembléesCitoyennesLocalesEnsemblesDécisionnaires - #auxACLED

Voici le premier essai que j'ai écrit, document (à remanier) du 8 décembre 2016 - https://imgur.com/KA9kYbV

Score : 0 / Réponses : 0

----

Actuellement, il n'est plus possible de rester dans le petit cadre de l'Assemblée dit Nationale pour répondre aux problèmes de niveau global, de niveau intermédiaire et de niveau local.
Le niveau local correspond à l'intérêt de mettre en commun et de pouvoir sauvegarder et entretenir ce que nous avons de plus précieux, à l'heure de d'évolution climatique engagée, à savoir notre intelligence. Les structures telles que le département, la nation, ne suffisent plus à répondre à la problématique de mutation, de métamorphose, engagées. Le niveau local doit bénéficier d'un centre d'Information pour résoudre tous les problème dits quotidiens et notamment ceux de formation, éducation, et maintien de la santé et de la sécurité locale. Pour cela une mini structure de pouvoir d'information, de législation de préparation budgétaire et de projets, de décision donc, exécutive, et enfin de bilan, donc judiciaire est à mettre en place. En groupant les niveaux locaux, nous obtenons une structure intermédiaire, la région, pour assurer la liberté notamment de circulation (au moins 8 millions d'habitants). Les régions se regroupent, se fédéralisent au niveau européen. La république nationale actuelle est un blocage majeur pour permettre la libération de toutes les intelligences sur l'aire spatiales actuelle de la France. Le premier enjeu, après avoir assuré le pain quotidien, est de développer les capacités de réflexion de toute la population. Actuellement les connexions entre les intelligences sont au niveau zéro des capacités. Les moyens sont aux mains d'entités occupées à s'enrichir quantitativement en privilégiant l'immédiateté au détriment de la globalité, pas de promouvoir l'intelligence comme outil commun pour relever le défi de la métamorphose climatique, entre autres, qui s'accélère. Les forces de régression et de repli sur soi, les carcans des monopoles et les habitudes établies dont la configuration politique à l’œuvre assemblée, dite nationale, actuelle font aussi partie, sont à lœuvre. Nous avons un pouvoir exécutif pour gérer le quotidien mais nous n'avons pas de projet viable à long terme. Une réponse existe, Libre UE 5.2, et ses annexes, Contrat constitutionnel, le plan A, Grand Est, Le niveau local pour compléter le dispositif sont à dispositif de qui voudra bien s'en saisir.

Score : 0 / Réponses : 0

----

..sont à disposi..tion

Score : 0 / Réponses : 0

----

On pourrait imaginer voter pour un projet pour chaque portefeuille (urbanisme, culture, éducation, etc) au lieu de voter pour une personne ou une liste proposant un projet auquel on n'adhère pas forcément dans sa globalité.
Pourquoi ne pas commencer au niveau des communes et faire du mairie un fonctionnaire au vu de la plus en plus haute technicité de sa tâche et des dérives qui peuvent apparaître pour une personne se sentant investie d'une toute puissance ?
Le Conseil municipal jouerait pleinement son rôle citoyen.

Score : 0 / Réponses : 0

----

Le temps peut manquer parfois pour s'investir dans une démarche citoyenne comme celle-ci lorsque - entre autres - l'on travaille 39 heures et plus.
Pourrait-on imaginer un temps citoyen pour une période déterminée (comme un syndicaliste l'a) afin de travailler sur une initiative citoyenne validée au préalable selon des modalités claires et à définir ?

Score : 0 / Réponses : 0

----

Il parait urgent d'organiser une relecture de la constitution. Nous avons besoin d'un texte court, précis et définitif. Les ajouts successifs, les préambules et autres apports ouvrent la porte à des débats sans fin et à des exégèses qui ne devraient pas être possibles sur un texte de cette nature. Ce texte remanié devrait dans tous les cas dominer la hiérarchie des lois et règlements y compris en ce qui concerne les lois, règlements, traités, et  accords supra nationaux.

Score : 0 / Réponses : 0

----

La même plateforme numérique ouverte pourrait concerner les règles de fonctionnement et le contrôle des dépenses parlementaires actuellement en cours de réforme.
De même, le Déontologie de l'Assemblée nationale pourrait voir ses pouvoirs accrus en fonction des thèmes débattus par les citoyens.
Enfin, la question de la "légalisation" ou plutôt de rendre la pratique du lobbying claire et démocratique est un thème central : pratique informelle mais néanmoins courante dans les couloirs de l'Assemblée, le poids des initiatives citoyennes serait un garde-fou ou un contre-pouvoir nécessaire pour "démocratiser la Démocratie". Je n'ai aucune idée de la façon dont elle est actuellement pratiquée, néanmoins, rendre cette pratique visible à travers des cartes professionnelles et un enregistrement préalable du lobbyiste auprès du Déontologue, du Président et/ou du Député-e participerait à davantage de transparence dans l'élaboration des projets de loi et/ou amendements proposés.

Score : 0 / Réponses : 0

----

- Proposition de Renaissance Numérique et la Fondation Jean Jaurès

Libérer les innovations démocratiques
- Mettre en place un conseil national, également ancré localement, dédié à l’innovation démocratique : le Conseil de l’innovation démocratique (CID). 
- Il s’appuie sur plusieurs instances déjà existantes et qui ont acquis une forte expérience sur ces sujets : la Commission nationale du débat public (CNDP), ETALAB et la Direction interministérielle du numérique et du système d’information et de communication (DINSIC). 
- À l’image du Conseil d’analyse économique, le CID serait rattaché au Premier ministre, mais disposerait d’une autonomie des moyens et des décisions.
- Sa composition : un organe mixte, composé de représentants des administrations support, d’acteurs de la société civile et de citoyens tirés au sort. Le mandat des acteurs et citoyens est de deux ans. Ils sont rémunérés et les compétences acquises au cours de cette année sont fortement valorisées dans le parcours professionnel (en figurant par exemple dans le compte professionnel d’activité, au titre d’engagement citoyen). Parmi ses membres, un président et un vice-président seront élus au cours d’un scrutin majoritaire. 
- Le CID a un rôle de proposition, d’évaluation et d’accompagnement des innovations démocratiques. 
- Saisi par des citoyens ou des administrations, ou de sa propre initiative, il organise l’étude et la formalisation de propositions nouvelles dans le domaine démocratique.
- Le CID supervise l’évaluation des lois et politiques locales à un an, cinq ans et dix ans après leur mise en place (voir la proposition sur la clause de revoyure). Son rapport doit faire l’objet d’un débat public au Parlement.
- Il accompagne la mise en place d’un droit d’expérimentation territorial : il soutient les citoyens et les collectivités territoriales dans la mise en place des expérimentations : de la mobilisation des acteurs à la mise en œuvre des expérimentations, leur évaluation et leur élargissement à l’échelle nationale ; 
il organise les concertations citoyennes impulsées par les acteurs politiques ou les citoyens ; il finance les projets démocratiques innovants.
- Le CID dispose de relais locaux, répartis sur l’ensemble du territoire, s’appuyant sur les réseaux de préfectures. Sur chaque territoire, des sous-préfets « démocratie » seront chargés de vérifier les conditions et bonnes mises en place des innovations et de proposer leur généralisation en cas de bonne pratique. 
- Un fond est dédié à l’innovation démocratique pour agir au niveau national et local. Le financement du fond repose sur l’État et les citoyens directement qui ont la possibilité de soutenir des projets sur des sites publics de financement participatif. Un montant fixe est fourni par la Caisse des Dépôts et géré par le CID. Une variable vient se rajouter au budget selon la règle suivante : pour chaque euro citoyen investi, le CID investit le triple. 

[Proposition issue du Livre blanc “Démocratie : Le réenchantement numérique” : http://bit.ly/2zAPucV]

Score : 0 / Réponses : 0

----

pour combattre le surendettement il faudrait interdire l'incitation aux crédits à la consommation sous quel que forme que ce soit

Score : 0 / Réponses : 0

----

que l’impôt sur le revenu soit considéré comme un investissement et non plus comme un taxe. et que les aides sociales soient calculées par rapport au niveau de participation à l’impôt

Score : 0 / Réponses : 0

----

Placer les documents produits, reçus ou détenus par les assemblées parlementaires et les informations qu’ils contiennent dans le régime général des documents administratifs et des informations publiques, en matière de droit à communication, d’obligation de mise en ligne et de droit de réutilisation.

Score : 0 / Réponses : 0

----

Il est surprenant de ne pas pouvoir se connecter sur ce site via l'identité numérique France-Connect fournie par l'Etat, alors que celle de Google le permet.

Score : 0 / Réponses : 0

----

Assemblées-Citoyennes-Locales-Ensembles-Décisionnaires
--->
Instaurer 1 jeudi / mois non travaillé-payé aux citoyens volontaires qui formeront les assemblées-citoyennes-locales-ensembles-décisionnaires
avec un minimum de 7% des électeurs & ouvert à tous dans toutes les communes en vu de refonder le parlement
-
Celui qui décide pour le peuple doit-être & demeurer LA CONCERTATION DU PEUPLE, de chaque citoyen-citoyenne, 
elle doit-être organisée en Assemblées-Citoyennes-Locales-Ensembles-Décisionnaires
-
Il faut pour les décisions importantes, que ce soit les citoyens  qui décident ensemble par concertation.
"Nous portons chacune chacun, une partie de responsabilité de ce monde, mais notre voix ne compte plus, ce qui nous fait dévier gravement du bon sens commun. "
Décider ensemble, est la seule solution pérenne.

#AssembléesCitoyennesLocalesEnsemblesDécisionnaires 
Voici le premier essai que j'ai écrit, document (à simplifier encore, la loi doit être accessible et compréhensible de tous) du 8 décembre 2016 - https://imgur.com/KA9kYbV

Score : 0 / Réponses : 0

----

Je trouve particulièrement frustrant et pour le coup non transparent le fait qu'il ne soit pas possible d'avoir des informations précises sur des choses simples comme quand, ou et comment a été faite l'information concernant cette consultation, par exemple. D'autant plus qu'elle pourrait avoir des effets majeurs dans la vie de tout citoyen. Est-ce de l'amateurisme ou une volonté de limiter les participations?
Est-ce qu'une annulation est prévue si le nombre de participants est trop faible? Quant aux fonctionnalités de la plateforme, elle sont vraiment succintes. OK pour raconter ses vacances mais tout à fait inadaptée à ce genre d'exercice.
Tout ça est bien dommage, j'ai bien failli y croire.

Score : 0 / Réponses : 0

----

Il faut mettre fin au fonctionnement à l’unanimité en Europe qui bloque quasiment toute évolution politique et sociale.

Score : 0 / Réponses : 0

----

Une vraie démocratie enfin. Je fais signer des pétitions sans résultat sur la gestion de ma commune. Si cela pouvait changer ce serait une belle avancée pour les citoyens

Score : 0 / Réponses : 0

----

Rendre obligatoire la consultation des riverains pour toutes installations et modifications de leur environnement. 
Moderniser la communication par voie d'affichage des enquêtes publiques. Tout le monde ne lit pas les panneaux d'affichage publiques

Score : 0 / Réponses : 0

----

Rendre certaines lois ou catégories de lois évaluables tous les trois ans. Un stop ou encore pour trois nouvelles années par un panel de citoyens.

Score : 0 / Réponses : 0

----

Proposer une mandature à points comme pour le permis de conduire, non sur 12 points mais disons, pour exemple, 120 points. Toujours pour exemple, une absence dans l'hémicycle équivaudrait à une perte de 2 points, une absence à une commission 2 autres points, un manquement de rapport de question au Gouvernement voulue par les citoyens de sa circonscription vaudrait 5 points, etc.
Bien entendu, un observatoire citoyen ou associatif aurait droit à siéger au Parlement pour relever les manquements....

Score : 0 / Réponses : 0

----

Pourquoi ne pas revenir à une démocratie directe ou tout le monde peut voter (de manière électronique) ? Des représentants s'imposaient quand le mode de transport et d'échange d'information était le cheval.
*la sélectivité des votants induite par le mode de vote est-elle plus problématique que celle du mode de scrutin actuel auquel bon nombre de citoyens ne participent pas ?
*tout le monde pourrait participer aux prises de décision et ses sentir impliqué dans la vie du pays
*le mode de sélection des lois pourrait être simplement de recueillir le soutien d'une fraction significative (1% ? 5%?) de la population.

Score : 0 / Réponses : 0

----

Favoriser les colocations "intergénérationnel" étudiants / retraités (manque de logements étudiants !) en instaurant une "sorte de Pinel" pour les loueurs !

Score : 0 / Réponses : 0

----

On ne peux pas commencer à légiférer avec un tel outil tant qu'il n'est pas suffisamment convivial (moteur de recherche, accessibilité, possibilité de classer les interventions)  et  connu du grand public par une campagne publicitaire confiée à une agence de communication. (Avec 500 millions d'€ de budget de fonctionnement, l'AN en a les moyens).

Score : 0 / Réponses : 0

----

Pourquoi ne pas avoir proposé https://franceconnect.gouv.fr/ pour permettre une connexion à ce site ?

Score : 0 / Réponses : 0

----

Construire un robot qui proposerait des statistiques journalières d'utilisation de la plate-forme, ou même les dernières contributions  vers une instance Mastodon par exemple.

Score : 0 / Réponses : 0

----

Pendant la campagne présidentielle,

La réforme en matière de transparence et d’éthique de la vie publique. est une  nouvelle avancé aux dispositions prises sous la Présidence de François Hollande, en particulier,  le renforcement de l’encadrement des activités de conseil des parlementaires,  la prévention des conflits d’intérêts au sein du Parlement,  l’interdiction des emplois familiaux dans les cabinets politiques,  Il este des progrès à faire sur plusieurs sujets essentiels comme   la transparence des dépenses des parlementaires,  le financement de la vie politique,   le lobbying, De plus, la levée de l'immunité parlementaire systématique doit rentrer dans les faits, lorsqu’il y a demande de la justice, quelques soit les raisons d'une mise en examen, ou de comparution pour témoignage - Cette demande pourrait relever d'un collège spécial composés de 3 magistrats.
Les activités politiques du parlementaire seraient suspendues pendant la durée des procédures, lesquelles devront être le plus rapide possible.

Score : 0 / Réponses : 0

----

Interdire totalement le cumul d'un mandat de parlementaire avec tout autre mandat ( y compris conseiller départemental, régional ou municipal).
Il n'est pas sérieux de prétendre que l'on peut exercer correctement un mandat de parlementaire avec un autre mandat. C'est juste de la mégalomanie ou du jemenfoutisme par rapport à la démocratie.
Par ailleurs il me semble qu'il serait de bonne pratique démocratique de limiter la durée à seulement deux mandats parlementaires successifs ou non.

Score : 0 / Réponses : 0

----

Voilà un outil de cohésion vital qui est a l'image de ce qui se pratique en Suisse en démocratie directe. La Nation a un besoin important de se décoller des intérêts des grands argentiers pour laisser exister l'implication plus directes des habitants dans la vie locale. Par exemple la surnormalisation, les lois imposées de l'extérieur sont un frein aux solutions simples et locales.

La démocratie de proximité est à inventer pour que chacun puisse s'exprimer en dehors des intenses exécutives locales. En effet les grand principes de séparation des pouvoirs sont attaqués de toutes part, à tout les échelons. Ces règles anticollusions s'énoncent simplement mais sont délicates à rédiger dans leur mise en oeuvre. Une fois inventées, elles demandent aussi une permanente attention et vigilance mais ce sont les principes et les règles de bases édictées qui doivent en elles même intrinsèquement nourrir le désir et l'envie de s'impliquer pour sa communauté.

Score : 0 / Réponses : 0

----

Je débute et je ne suis pas certain d’étre dans la bonne rubrique , mais j’ai un commentaire à proposer suite à l’actualité de ces derniers jours . N’y aurait il pas 30 députés disponibles pour rendre 1 visite dans les maisons de retraite et autres ? Nous ne vous en voudrons pas de passer aprés les gens emprisonnés , Nous , gens honnétes et travailleurs . Eux qui n’ont peut étre pas travaillés , mais qui sont maintenant logés et nourris gratuitement alors que nous travailleurs et gens honnétes payons jusqu’à presque 3000 E par mois pour des soins laissants à désirer ( en étant sympa ) et nourris…..Justice dites vous ?? Amicalement votre

Score : 0 / Réponses : 0

----

nous sommes actuellement interrogés sur la question du logement
il faudrait créer le site pour que nous puissions intervenir dans le cours des discussions.

par exemple un onglet sur le site de l'AN : 'en cours de discussion' où nous pourrions déposer observations, partages d'expériences, faire remonter les ressentis du terrain  ...

Merci pour cette initiative de consultation citoyenne !

Score : 0 / Réponses : 0

----

La **procédure parlementaire elle-même ne permet pas au Parlement d'exercer l'influence qu'il devrait avoir sur la vie de la Nation**.
Avant d'impliquer les citoyens dans la procédure parlementaire, il est nécessaire de rendre celle-ci plus transparente et de donner les moyens aux parlementaires d'instruire eux-mêmes les dossiers.

**Sujet 1 **: le "parlementarisme rationalisé" instauré par la Constitution ne se justifie plus en période d'hyper-présidentialisation du régime. La restriction du droit d'amendement des parlementaires pourrait être abrogé. Dans le même temps, son usage pourrait être recentré (pour éviter que certains ne fasse "tourner le compteur" pour justifier leur mandat auprès des électeurs) : un parlementaire qui dépose 300 amendements dans l'année ne les a probablement pas écrits lui-même... mais s'est contenté de les reprendre des lobbies.

**Sujet  2 **: les moyens humains du Parlement sont gaspillés. Il y a beaucoup de fonctionnaires A+ ; à tous les grades les fonctionnaires sont payés le double de la fonction publique d'Etat.
Il serait facile de doubler les services du Parlement si les services étaient repyramidés (10% d'A+, 50% de A, etc.) et si les salaires étaient normalisés.

**Sujet 3** : les parlementaires pourraient être associés au travail gouvernemental en amont. Plusieurs exemples: créer des commissions mixtes gouvernement/parlement, nommer au "cabinet" du ministre de jeunes parlementaires chargés d'assurer le travail technique selon les directives politiques du ministre senior (modèle anglais), assister aux arbitrages en RIM, etc.

**Sujet 4** : un parlementaire à temps plein est toujours plus crédible. Il l'est également s'il représente beaucoup d'électeurs. La suite logique du non cumul des mandats serait de réduire la taille de l'AN à 200 et celle du Sénat à 100 parlementaires. Une certaine spécialisation des parlementaires entre contrôle du gouvernement et procédure législative, à laquelle ils se consacreraient à 100% tout en représentant 300.000 citoyens, leur donnerait également une stature et une influence qu'aucun d'entre eux ne peut espérer avoir actuellement.

Score : 0 / Réponses : 0

----

Aujourd’hui 7 novembre 2017. C’est bien que vous ayez prolongé de 48 heures  la durée de vie de cette plate-forme ; mais vous auriez pu aussi bien lui donner un mois supplémentaire. Personnellement, c’est seulement il y a quelques jours  que j’en ai découvert l’existence.  Il est certain que les médias n’en ont pratiquement pas parlé. Je me demande si les députés eux-mêmes l’ont fait ? En tous cas, je n’ai rien vu de tel dans ma circonscription. (La IVème du Finistère). Or, précisément – et c’est ma suggestion – je pense que cette révision en profondeur que vous initiez sur le fonctionnement de l’Assemblée nationale, doit s’accompagner d’un rigoureux parallèle avec la réflexion que vous proposez d’autre part sur le statut du député (Ajoutons, très précisément : la »Mission ». Il y a  un énorme travail à faire là-desssus, à faire parallèlement sur les deux thèmes. Les députés et l’Assemblée nationale ne confondent pas, même s’ils n’existent pas l’une sans les autres et réciproquement. Les mécanismes de fonctionnement d’une institution et les mécanismes de fonctionnement d’un groupe humain sont deux choses très différentes. Par exemple, dans son intervention vidéo du 26 octobre 2017, (thème : ouverture de l’Assemblée natonale à la société),  Jean-Louis Debré fait allusion à ce qu’il considère comme le dilemme absolu du député : est-il le représentant de la nation ou bien le représentant de sa circonscription ? Jean-Louis Debré sait très bien qu’il est l’un et l’autre, « en même temps » l’un et l’autre, dirait Macron. On définit souvent le député par ses deux fonctions : faire les lois et contrôler le gouvernement, mais on pourrait le faire aussi, et ce serait mieux, par sa double représentation : représentant de la nation et représentant d’une  circonscription. J’irai plus loin. Le député est une véritable interface : il représente sa circonscription face à la nation ; il représente la nation face à sa circonscription.  Cela dit, il y a un énorme problème quand vous envisagez  la réforme en profondeur de l’institution « Assemblée nationale ». C’est tout le problème de la défiance des élus face à leurs représentants. (Lire, entre autres, LA CONTRE-DEMOCRATIE de Pierre Rosanvallon). On ne peut pas s’attaquer à ce problème sans remettre en question, de fond en comble, la fonction (ou faut-il dire « la mission » ?) du représentant. C’est par le niveau local que nous avons une chance de réformer la démocratie. C’est aussi par la relation concrète, vécue (numériquement et présentiellement) à établir entre l’électeur et l’élu. Pierre Rosanvallon écrit : « Faire vivre le représenté dans le représentant ». ll aurait pu dire aussi : « Faire vivre le représenté PAR le représentant » ; Et enfin il aurait encore pu aller jusqu’à écrire que c’est au représenté d’aider le représentant à vivre et à remplir sa mission. Aujourd’hui 7 novembre 2017. C’est bien que vous ayez prolongé de 48 heures  la durée de vie de cette plate-forme ; mais vous auriez pu aussi bien lui donner un mois supplémentaire. Personnellement, c’est seulement il y a quelques jours  que j’en ai découvert l’existence.  Il est certain que les médias n’en ont pratiquement pas parlé. Je me demande si les députés eux-mêmes l’ont fait ? En tous cas, je n’ai rien vu de tel dans ma circonscription. (La IVème du Finistère). Or, précisément – et c’est ma suggestion – je pense que cette révision en profondeur que vous initiez sur le fonctionnement de l’Assemblée nationale, doit s’accompagner d’un rigoureux parallèle avec la réflexion que vous proposez d’autre part sur le statut du député (Ajoutons, très précisément : la »Mission ». Il y a  un énorme travail à faire là-desssus, à faire parallèlement sur les deux thèmes. Les députés et l’Assemblée nationale ne confondent pas, même s’ils n’existent pas l’une sans les autres et réciproquement. Les mécanismes de fonctionnement d’une institution et les mécanismes de fonctionnement d’un groupe humain sont deux choses très différentes. Par exemple, dans son intervention vidéo du 26 octobre 2017, (thème : ouverture de l’Assemblée natonale

Score : 0 / Réponses : 0

----

Intervention N°2
Dans notre société actuelle, à l'heure de la mise en évidence du rôle du cortex préfrontal, à l'heure de la diffusion des informations sur les variations climatiques et donc les conséquences sur les plantes, et le vivant en général, il apparaît que la vision proposée par l'Assemblée nationale est quelque peu écornée. A partir du moment où nous posons que l'intelligence, donc la formation du cerveau et l'utilisation de cet outil dépend de l'information généralisée, la solution étriquée que constitue cette Assemblée au demeurant très vénérable, est bien insuffisante. 

Pourquoi la France au final a perdu toutes les guerres dans lesquelles elle s'est engagée depuis deux siècles? A cause justement d'une structure inachevée au final réductrice. 
La division du pouvoir entre état (public) et le reste( privé), la confiscation quasi féodale de la capacité du citoyen à être soldat afin de défendre la république, l'armée de la république réduite à une professionnalisation qui tangente un mercenariat rampant, la mise sous tutelle du pouvoir judiciaire réduit à une simple autorité, l'inflation du nombre d'élus nationaux, combinée à la duplication du pouvoir législatif et pire encore, l'initiative de la production législative laissée quasiment au seul pouvoir exécutif, constituent les ingrédients de base d'une société figée. 

La déviance cinquantenaire de la Vème république instaurant un conseil constitutionnel serf comme l'a encore montré la prouvé la dernière décision QPC651, ou encore, une assemblée godilleuse qui a, par son premier vote sérieux, enterré les dernières libertés des citoyens en permettant au juge administratif de prendre le dessus sur la justice au prétexte de terrorisme, montrent l'inadéquation entre la production législative et le monde réel.
L'état diviseur opprime.
L'obligation, pour sauvegarder l'Humain mais pas uniquement, implique de sauvegarder la diversité. Les déviants, pseudo terroristes mais vrais ignorants, servant de prétexte à l'état pour augmenter le niveau d'oppression à légard de toute la population vont à l'encontre de la nécessaire diversité d'apports pour tenter de répondre aux défis notamment ceux posés par le bouleversement climatique communément reconnu, sauf par quelque énergumène sénile. 
Ne pas reconnaître que la division du pouvoir, implique une division- séparation de ceux ci en quatre entités afin justement de permettre la libération des énergies, est la première entrave à notre aptitude à répondre au challenge que constitue le repli sur soi notamment national. Deux entités partisanes au moins en sont les vecteurs au sein de l'aréopage élu en fonction pour ce quinquennat. 
La vraie sortie est «Osons» ouvrir les vannes (les conséquences du stress des derniers conflits mondiaux étant en voie d'être effacés) et créons les moyens de collecter toutes les informations à un niveau quasi individuel, pour les mettre ensuite à la disposition de tout et chacun voulant y accéder. 
C'est la condition minimale initiale. 
suite..

Score : 0 / Réponses : 0

----

Intervention 3 suite..
Concrètement cela se traduit par la création d'un pouvoir informatif. Informatif parce qu'il collecte et met à disposition toute l'information possible . Actuellement c'est le pouvoir essentiellement exécutif qui à travers experts et instruments de collecte dispersés voir divisés et le principal bénéficiaire de l'information. C'est encore plus flagrant quand c'est le seul président de l'instance véritable despote omnipotent, qui représente l'alpha et l'oméga de cette démocratie factice, trompe l’œil en vigueur.
Évidemment que le principe de  division du pouvoir implique une instance législative. Seulement fondée sur une assise essentiellement spatiale , elle fait doublon avec sa contrepartie sénatoriale issue d'une division spatiale, elle aussi. D'où un parlement redondant. Il est impératif de supprimer ce vice constitutif fondamental. 
Une solution est de projeter le bicamérisme au niveau supérieur à savoir l'union européenne, si c'est la solution fédérative qui est retenue.. 
Cela est à compléter par une autonomie affirmée au niveau régional (s'il est viable). Une région viable implique au moins 8 millions de personnes.

Le niveau local est à gouverner selon des principes identiques (séparation des pouvoirs, démarche démocratique, respect des principes de la CEDH). La métropolisation seule réalisation créatrice de richesse est l'outil à utiliser pour cette adaptation.

Le pouvoir judiciaire, par une erreur historique a été mis sous l'éteignoir depuis plus de deux siècles, et réduit au statut de simple autorité. Cela n'est pas tolérable. Le pouvoir législatif doit être indépendant. Cela est valable aussi pour le pouvoir judiciaire.

Il reste que de toute cette construction, le principal intéressé à savoir «Nous, peuple souverain, tous unis», est le principal absent. Il doit retrouver sa pleine et totale souveraineté. L'altérité est une condition nécessaire évidemment.

Tant que cette primauté ne sera pas mise en action, il est vain d'espérer répondre aux défis qui sont à relever aujourd'hui.
Je propose un nouveau contrat constitutionnel, une Constitution, à refonder pour bâtir enfin la société de l'intelligence, la seule susceptible de trouver les solutions aux problèmes multidimensionnels complexes qui apparaissent aujourd'hui.
Il pourrait être utile de se référer au droit des contrats de Marine Goubinat pour la mise en place de ce nouveau contrat social.

Score : 0 / Réponses : 0

----

Je me permets de suggérer l'idée simple que les lois soient appliquées.
Il arrive souvent que des puissants parviennent à contourner les lois avec l'assentiment des forces exécutives mais aussi du système judiciaire ! C'est insupportable, cela créé de la violence, ce qui est inadmissible de la part d'une société qui se prétend démocratique.
Il arrive souvent que l'administration, ne pouvant faire respecter les lois, les modifie afin de mettre en conformité le contrevenant, autrement-dit, la loi n'est rien qu'une barrière face aux faibles puisqu'elle est aménageable suivant les besoins des puissants.
C'est un comportement arbitraire, monarchiste ou fasciste.

Je souhaite (je rêve, je sais) que la loi devienne le principe et non plus la lettre. Je veux dire qu'il existe de fait de nombreuses exceptions, imprécisions dans les textes de lois (la loi n'est plus qu'une vague idée alors que le texte devient un objet corruptible) etc  qui permettent d'enfreindre l'esprit de la loi telle que voulue par le souverain. Cette possibilité de passer outre la volonté souveraine n'est pas admissible, c'est fabriquer de la violence, c'est violer les principes fondamentaux de la société qui se proclame démocratique, c'est détruire la notion même de loi comme applicable, donc fabriquer de l'arbitraire. Certes le passage de l'idée au réel est difficile, voire impossible.

Pour terminer, pour que les lois puissent être justement applicables dans nos vies, je souhaite la publication de toutes les plaintes (anonymisées) auprès des procureurs et des motivations à accepter ou rejeter. L'arbitraire du procureur est insupportable dans un système qui se prétend démocratique.
On ne peut pas d'un coté prétendre à la transparence et de l'autre étouffer ce qui dérange le pouvoir car le procureur EST le pouvoir et ne connaît pas de contre-pouvoir.

Score : 0 / Réponses : 0

----

Je connaissais l'initiative du Président de l'AN, que j'approuve, mais je n'ai découvert ce site que très récemment et n'ai pu lire que peu de messages. Je le regrette.
Je propose la création d'une structure qui pourrait s'intituler "Organe numérique permanent de propositions citoyennes" fonctionnant avec la Charte de consultation ci-dessus permettant à tous d’émettre des idées ou des opinions sur les législations en vigueur et en cours d'élaboration. Pour la législation en élaboration il serait logique et indispensable  que la structure soit informée du calendrier des discussions parlementaires.
Je ne suis pas favorable au forum de discussions car, cela est certainement dû à mon âge, je pense - à tort ou à raison- que l'outil informatique se prête mal à ce genre d'exercice.
Si cette structure, ou similaire, se réalise j'y participerais volontiers et notamment en ce qui concerne la réforme constitutionnelle et l'organisation territoriale ( les strates sont devenues trop nombreuses et incompréhensibles pour le citoyen que je suis ).
Je vous remercie d'avoir pu m'exprimer et souhaite le succès à cette démarche démocratique.

Score : 0 / Réponses : 0

----

Je connaissais l’initiative du Président de l’AN, que j’approuve, mais je n’ai découvert ce site que très récemment et n’ai pu lire que peu de messages. Je le regrette.
 Je propose la création d’une structure qui pourrait s’intituler “Organe numérique permanent de propositions citoyennes” fonctionnant avec la Charte de consultation ci-dessus et permettant à tous d’émettre des idées ou des opinions sur les législations en vigueur et en cours d’élaboration. Pour la législation en élaboration il serait logique et indispensable que la structure soit informée du calendrier des discussions parlementaires.
 Je ne suis pas favorable au forum de discussions car, cela est certainement dû à mon âge, je pense - à tort ou à raison- que l’outil informatique se prête mal à ce genre d’exercice.
 Si cette structure, ou similaire, se réalise j’y participerais volontiers et notamment en ce qui concerne la réforme constitutionnelle et l’organisation territoriale ( les strates sont devenues trop nombreuses et incompréhensibles pour le citoyen que je suis ).
 Je vous remercie d’avoir pu m’exprimer et souhaite le succès à cette démarche démocratique.

Score : 0 / Réponses : 0

----

Première idée : prolonger cette consultation notablement, le faire savoir, en faire la promotion et en rendre public les conclusions. 
Deuxième idée : dans un premier temps, la fracture numérique impose de ne pas se focaliser sur ce medium pour consulter les citoyens. Toutes les idées que j'ai vues ici (les consultations en face à face, les réunions, les déplacements de sénateurs et députés vers leur territoire, pourquoi pas les permanences obligatoires pour nos élus, etc... ) doivent être mises en place en parallèle avec un développement des consultations numériques.
Troisième idée : "en même temps", permettre l'accès du numérique à un plus grand nombre. Il fallait jusqu'alors 3 générations pour changer les mentalités, espérons que ce délai soit raccourci désormais avec les jeunes générations. Mais pour permettre au plus grand nombre l'accès au numérique (l'imaginer pour tous actuellement est illusoire, en particulier pour les non "digital natives"), il faut imaginer et mettre en pratique différentes actions : information, éducation, prise en main, sous forme de stages, d'ateliers... ET mettre à la disposition des plus démunis un service d'aide : un local (dépendant de la mairie ?) avec ordinateur(s) et du personnel, car de plus en plus de services publics ne se feront qu'en ligne désormais. Petit à petit, ces services viendront à disparaitre naturellement puisque dans une ou deux générations, tout le monde sera connecté. La question sera à ce moment là, quels sont les dangers du "tout-connecté". Gageons qu'il y aura des irréductibles qui se battront pour préserver nos libertés individuelles.

Score : 0 / Réponses : 0

----

CHARTES DES DEBATS NUMERIQUES
Forum des débat a fait débattre les citoyens sur une charte des débats numériques lors des assises Sciences sociétés. Il en est ressorti des recommandations pour la rédaction d'une charte que votre charte est lois de remplir.
Une charte encadrant le débat numérique  inspirée des lois d'Habermas est nécessaire :
1. pour éviter que le débat nous fasse crocheter par le grand Numérik
2. Pour éviter que le débat ne soit qu'une opération de communication pour les politiques
3. Pour commencer la démocratie sociale dès le lycée.
4. Pour produire des débats adaptés aux participants.
5.  Pour prendre du recul sur ses croyances et ses instincts.
6. Pour éviter qu'il soit un simple déversoir aux passions.
7. Pour avoir un débat amélioré.
8. Pour réguler les interventions dans le débat et en décrire la production.
9. Pour rassurer les citoyens sur le fait que leur engagement prépare la décision.

Score : 0 / Réponses : 0

----

Revenir à un calendrier électoral qui donnera mécaniquement plus de pouvoir à l'Assemblée nationale en soumettant le vote à mi-mandat du Président de la République.

Score : 0 / Réponses : 0

----

Les lois sont generalement mal ecrites et sont illisibles . Il faut structurer une loi.
Art 1: objet de la loi en une phrase
Art 2. Lois abrogees (au minimum 2 au maximum 10)
Art 3. Duree de Validite de la loi (5 ans)et indicateurs d'efficacite

Comme ce qui s'enonce clairement la loi doit etre breve. Une limite a 4 pages (sauf loi de finances) serait la bienvenue car elle ne permettrait pas a  l'administration de deformer l'esprit de la loi. Plus la loi est synthetique plus rapidement les decrets pourront sortir et mieux elle sera evaluee.

Score : 0 / Réponses : 0

----

Un citoyen pourrait vouloir transmettre toute une réflexion sur un sujet particulier qui lui tient à cœur. Il pourrait pour cela transmettre un document écrit, qui passerait devant une commission de l'Assemblée, chargée de la pertinence d'entendre ce citoyen, et lui donner ainsi la possibilité de présenter son travail lors d'une audition.

Score : 0 / Réponses : 0

----

Participation! 1/3 des places de l'assemblée devraient être aux citoyens -tiré aux sorts dans une liste de volontaire- 1/3 des convocations des professionnels concerné par  sujet /PPL.  Et 1/3 de député pour proposer et vérifier la législation (encore que ; de ce que j'ai vu 3/4 des député-e-s ne connaissent rien à Dalloz) et a assurer le financier (cahier des charges) !  Trouvez vous normal; qu on ait plus de député que les états  unis?! Des députe-e-s qui en plus  n ont plus aucune subvention parlementaire?!! Avec la particularité dans ce gouvernement EM qu'il ne sont jamais à leur permanence tout en réussissant a être aussi absents à l assemblée?!  Au passage

Score : 0 / Réponses : 0

----

Sur des questions très quotidiennes et au coup par coup et qui concernent certaines administrations, entre autres : le travail (inspection du travail), la consommation ( directions de la concurrence et des prix départementales des réclamations leur sont adressées, qui peuvent trouver un prolongement lors des débats parlementaires. Mais le tempo entre les deux n'est pas le ^même. Est il envisageable de pense à un mode de transmission d'un bilan public sur le site de l'assemblée nationale de ces administrations qui gèrent le quotidien ?

Score : -1 / Réponses : 0

----

j'aimerais que nos députés se penchent sur la liste des saisie vente mobilier de façon a revoir cette liste et inclure l'ordinateur qui est nécessaire pour toute démarches administratif  et le droit a l’information ainsi que la literie dit matelas au moins conservé l'entourage du lit qui n'est pas un bien de luxe et acheté a conforma sans valeur et au moins un canapé de salon au minima

Score : -1 / Réponses : 0

----

le minimum vital aujourd’hui c’est 534 euros mais est ce que on tiens compte du loyer et des dépenses nécessaires pour vivre elect gaz internet assurance habitation faudrait revoir la loi de façon a ne pas faire souffrir encore plus les personnes en difficulté c'est du bon sens

Score : -1 / Réponses : 1

----

Instaurer une chaîne ou une diffusion en direct des séances qui explique en simultané ce qu'il se passe. Les séances, notamment dans l'hémicycle sont déjà retransmises sur le site internet de l'Assemblée Nationale ou sur LCP, mais elles restent très souvent assez complexes à déchiffrer pour quelqu'un qui ne regarde pas habituellement les débats. 

Il faudrait donc qu'une diffusion permette d'expliquer en même temps que les débats : les usages de l'Assemblée Nationale (ex : pourquoi le vote à tel moment se fait à main levée et pourquoi il ne l'est pas toujours, pourquoi tel député pose telle question), les amendements proposés (les expliciter un peu) et autres.

Score : -1 / Réponses : 3

----

Créer une page sur les agences d'état sur le site AN.
Evidemment celle ci ont des sites chacune, mais une sorte de page récapitulative de leur:
- rôle, mission
- budget
- QUELs responsables publiques (comission, députés, sénateurs) sont responsable de les suivre.
- quand a été tenue la dernière "revue" ou "suivi" de la mission
- rappports (annuels, trimestriels, mensuels) remis par cette agence à la représentation nationale

Ces agences sont des "rejettons" indépendant mais payés par l'état ils sont suivis par l'état.

Note, peut etre n'est-ce pas a mettre sur le site de l'AN, mais alors où?

Ces points sombre souvents décriés (parfois à raison) sont créées par des votes à l'AN. Il doit etre facile de savoir ce qui s'y passe

Score : -1 / Réponses : 0

----

Pourquoi ne pas faire 1 seul site du parlement (sénat et AN).
Objectif:
- mettre en commun les moyens techniques, faire un eu d'économie
- uniformiser pour les citoyen l'accés à l'information.

Les deux chambre ont des rôles différents mais travaillent sur les meme lois. et ont des missions complémentaire.

Cela permettrait de mieux comprendre.

NOTE au passage on pourrait y ajouter une page sur le conseil constitutionnel et la constitution.

Score : -1 / Réponses : 0

----

Je ne peut que saluer la consultation actuelle. Toutefois, il m'apparaît nécessaire de déterminer l'impact d'une consultation, en effet dans certains domaines, elle est prévue mais souvent sa prise en compte n'est qu'un avis donc sans réelle prise en compte. 

Suite à mon expérience , je me suis rendu compte que les représentants des collectivités, les élus, prennent rarement en compte les  arguments avancés par le public ( ensemble de la société civile) pour ne pas dire qu'il ont simplement bonne conscience d'avoir mis en place une consultation(souvent encadrée par les textes réglementaires. L'avis du public n'a pas le droit d'avoir une incidence sur le regard qu'ils portent et quand je dis regard ....

Ainsi :   
 Est ce que le résultat sera à même d'influer sur la décision finale ? 
Quelles seront les garanties pour assurer que l'avis du public sera pris en compte, sachant que le domaine législatif est complexe et très dense.. Il faut dans ce domaine cerner l'ensemble des données règlementaires, ce qui n'est pas à la portée de chacun.

Score : -1 / Réponses : 1

----

Représentativité et légitimité: Afin d'améliorer la représentativité de l'assemblée nationale, le mode de scrutin proportionnel plurinominal ainsi que le vote obligatoire devraient être instaurés. Nous aurions ainsi une plus grande légitimité de la décision prise.

Score : -1 / Réponses : 0

----

Cumul des mandats: Une limitation stricte du cumul des mandats, y compris dans le temps (2 maxi) devrait être instaurée, ceci afin d'améliorer la présence effective et participative sur les bancs et en commissions.

Score : -1 / Réponses : 1

----

Je ne sais pas trop comment cela pourrait être mis en pratique, mais les lobbies ne devraient pas pouvoir présenter aux députés des projets de loi et des amendements tout faits ; il est inadmissible que des groupes de pression puissent, de cette manière, faire discuter à l’assemblée des textes taillés sur mesure pour leurs intérêts, souvent au détriment du bien public et de l’environnement.

Score : -1 / Réponses : 0

----

**[Démocratiser l'élection des parlementaires](https://forum.parlement-ouvert.fr/t/democratiser-lelection-des-parlementaires/79)**

Chaque citoyen aurait quatre possibilités :
- voter pour une des listes nationales se présentant aux législatives ;
- voter pour qu’un citoyen aléatoire soit député ;
- voter les textes de loi directement ou par délégation, grâce à la [démocratie délégative](https://fr.wikipedia.org/wiki/D%C3%A9mocratie_liquide) ;
- ne pas s’exprimer, c’est-à-dire voter blanc.

Les députés et les droits de votes à l’Assemblée seraient alors répartis au prorata des trois possibilités exprimées.

Score : -1 / Réponses : 0

----

**Donner la mainmise de l'agenda parlementaire aux parlementaires.**
Pour l'instant, c'est le gouvernement qui accapare la plupart des discussions.

Score : -1 / Réponses : 4

----

**Réformer la procédure de nomination**

Les parlementaires ont souvent à se prononcer sur la nomination de plusieurs personnes (gouvernement, hauts fonctionnaires, etc.). L'issue pourrait être plus représentative de la diversité des opinions présentes. Voici comment faire :

Imaginons que n élu.e.s doivent nommer k personnes. Chaque élu.e propose la nomination d'une personne à un des k postes. Dès lors qu'une personne obtient n/k voix, elle est nommée à ce poste, et les n/k élu.e.s ne peuvent plus voter pour la suite des nominations. Un scrutin au jugement majoritaire a lieu pour répartir les éventuels postes non attribués aux candidat.e.s restant.e.s.

On peut imaginer un raffinement de cette méthode pour éviter l'effet de "premier arrivé, premier servi" ou pour permettre un veto. Mais même sans raffinement, ce serait préférable au vote majoritaire actuel. Par exemple, le gouvernement serait alors composé de tous les partis politiques en proportion de leur poids politique, ce qui inciterait au dialogue et au consensus.

Score : -1 / Réponses : 0

----

**Ouvrir la possibilité de révoquer un.e élu.e local.e** et autoriser le mandat impératif, pour peu qu'il soit lié à une réelle démocratie directe (il est interdit à l'article 27 de la Constitution). Toute personne élue non membre d'un Parlement serait révocable dès lors qu'au moins 10% des inscrits aurait signé une pétition proposant de la remplacer par une personne donnée (5% en cas de condamnation judiciaire), et qu'elle aurait perdu au vote déclenché par cette pétition. On pourrait aussi imaginer qu'elle soit révoquée si elle obtient moins de 95% des voix qu'elle avait obtenu lors de son élection, et dans ce cas il y aurait une nouvelle élection. Dans le cas d'un jugement majoritaire, le seuil de 95% des voix serait remplacé par : obtenir une note significativement inférieure (0,3 d'écart par exemple).

[82% des Français](http://tnova.fr/sondages/l-observatoire-de-la-democratie-en-france-sondage-viavoice-terra-nova-la-revue-civique-le-monde-france-inter-lcp) sont favorables pour permettre à une assemblée de citoyens de démettre des élus qui ne respectent pas leurs engagements.

Score : -1 / Réponses : 0

----

Tenir compte des CITOYENS EXPERTS. Trop souvent pour élaborer une loi on consulte les experts du domaine. Ainsi dans l'élaboration de la loi pénale, sur les prisons etc, le député en charge confirme avoir consulté des professionnels de la justice, des prisons etc. Hors les citoyens sont maintenant très informés et sont devenus avec Internet des "experts amateurs".  Il y a de nombreux citoyens que leur parcours de vie a amené à se pencher de façon concrète sur les lois,  droit civil et pénal, administratif. Il convient d'associer des CITOYENS EXPERTS  en amont à l'élaboration de lois, par appel à volontaires, tirage au sort ...Même démarche pour l'évaluation de la loi : les "effets pervers" ou la non application des lois prennent trop de temps à être repérés par les commissions d'enquête. Le Sénat ne donne pas de réponse aux failles signalées etc. Faire confiance aux citoyens pour une évaluation "de terrain" argumentée  et factuelle pour une réelle amélioration des lois défaillantes..

Score : -1 / Réponses : 0

----

Créer des commissions interprétatives des lois :
A/L'intérêt
Certaines dispositions sont sujettes à interprétation.
1/ L'interprétation qui en est faite par le juge peut être contraire à l'esprit du texte.
2/ Parfois, le juge n'a pas l'occasion de se prononcer se qui conduit les justiciables à se trouver en insécurité juridique.
B/Le fonctionnement
Après le vote d'une loi, je propose de nommer des députés et sénateurs ayant participé au vote qui constitueront la commission interprétative. Si le besoin s'en fait sentir ultérieurement, cette commission pourra être saisie et émettre un avis qui s'imposera aux juges et aux justiciables.
Il conviendra de définir selon quelles modalités cette commission interprétative pourra être saisie : je propose de permettre la saisine de cette commission sur une initiative citoyenne. Les requérants pourront proposer l'interprétation qui leur semble être la plus appropriée et désigner une ou plusieurs personnes compétentes pour présenter leur problématique auprès de la commission.
Une seconde modalité de saisine pourrait s'inspirer de celles relatives à la saisine du Conseil constitutionnel (article 61-1 de la Constitution) : lorsque, à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative est sujette à une pluralité d'interprétation, la commission interprétative pourrait être saisie de cette question sur renvoi du Conseil d'État ou de la Cour de cassation qui se prononcerait dans un délai déterminé.
C/Des mécanismes ayant une finalité comparable
Ils existes déjà des mécanismes permettant l'interprétation de normes juridiques édictées à un niveau différent de celui de la loi : 
1/Les commissions paritaires d'interprétation des conventions et accords collectifs de branche
2/La question préjudicielle permettant de saisir la CJUE en vue de l'interprétation des textes de l'Union européenne
Pourquoi ne pas donner les mêmes outils pour ce qui concerne l'interprétation de la loi ?

Score : -1 / Réponses : 3

----

Faire élire les présidents de France Télévisions et Radio France par le Parlement

Score : -1 / Réponses : 0

----

Adopter une loi anti-concentration des médias, protégeant le secteur des intérêts financiers, favorisant la transformation des médias en coopératives de salariés et de lecteurs/auditeurs/téléspectateurs et attribuer des fréquences aux médias locaux et associatifs

Score : -1 / Réponses : 0

----

Combattre la sondocratie : interdire les sondages dans les jours précédant les élections et adopter la proposition de loi sur les sondages votée à l'unanimité par le Sénat en 2011 et enterrée depuis

Score : -1 / Réponses : 0

----

Protéger les sources et l'indépendance des rédactions à l'égard des pouvoirs économiques et politiques par le renforcement du statut juridique des rédactions et une charte déontologique dans la convention collective

Score : -1 / Réponses : 0

----

Constitutionnaliser la règle verte : ne pas prélever sur la nature davantage que ce qu'elle peut reconstituer, ni produire plus que ce qu'elle peut supporter

Score : -1 / Réponses : 0

----

Instaurer une peine de déchéance des droits civiques en cas de fraude fiscale ou d'activités illicites permettant le financement d'activités terroristes, punir les financements des trafics alimentant les réseaux terroristes et réquisitionner les entreprises qui collaborent avec les agresseurs

Score : -1 / Réponses : 0

----

Créer une mission parlementaire spéciale pour faire le bilan de toutes les privatisations et faveurs fiscales décidées au cours des trois décennies passées

Score : -1 / Réponses : 0

----

Mettre en place une commission d'enquête parlementaire sur le pillage économique et industriel des dernières années (abandons de fleurons comme Alstom, Alcatel, EADS…) et permettre la mise en examen et la détention préventive des suspects

Score : -1 / Réponses : 0

----

Rendre effectif le droit de réquisition des entreprises d'intérêt général par l'État

Score : -1 / Réponses : 0

----

Décréter un moratoire sur les partenariats public-privé (PPP), abroger les dispositions législatives les permettant et pratiquer un audit de ceux qui sont en cours

Score : -1 / Réponses : 0

----

Beaucoup de personnes se plaignent de ne pas être écoutés/entendus.
Il existe des Comités locaux ; il manque souvent les coordonnées des personnes à contacter et plus souvent encore, le retour (accusé réception).
Chacun pourrait être mis en contact avec son ou  ses ''messagers '' en commençant par le local..
Voir mon message à un autre endroit du site:
Utiliser les relais existants (messagers): 
Comités En Marche le plus proche de son domicile, 
Représentant En Marche départemental, 
Attaché parlementaire de la circonscription, 
Député de la circonscription qui transmet aux intéressés au ministère et à l'assemblée 
ET retour d'un ''accusé réception'' avec avis de prise en compte ou non.

Score : -1 / Réponses : 0

----

Pour une plus grande transparence quant à l’utilisation des fonds publics

-> Inscrire dans une loi organique le principe et les conditions d'application, pour l'ensemble des administrations françaises, d'une "divulgation proactive" inspirée du modèle canadien (http://www.tbs-sct.gc.ca/pd-dp/index-fra.asp)

Frais de voyage et d’accueil, contrats, reclassifications de postes, subventions publiques… ces informations, rendues publiques de manière rapide, permettraient à chaque citoyen d’avoir une information plus claire sur l’utilisation d’une partie importante des fonds publics, de lever les soupçons éventuels et de renforcer la confiance des citoyens dans la décision publique. 

Ces données pourraient être publiées sur un portail unique comme c’est le cas, là encore, au Canada qui a créé un site dédié au « Gouvernement ouvert » (http://ouvert.canada.ca/fr)

Score : -1 / Réponses : 0

----

Des [résultats d'un sondage](http://adrien-fabre.com/sondage/resultats.php#_ed) sur la démocratie : les Français sont massivement favorables à un renforcement de la démocratie, que ce soit en "votant davantage sur des propositions que sur des personnes", en "permettant à une assemblée de citoyens de démettre les élus qui ne respectent pas leurs engagements", en faisant "voter les réformes importantes par référendum", en créant "un site internet officiel de débats et de consultation politiques" ou en faisant "voter directement les citoyens sur le budget de l'État", entre autres. C'est simple, il n'y a que l'autorisation du droit de vote des étrangers en situation irrégulière qui n'a pas été bien accueilli par un échantillon représentatif de la population.

Un [autre sondage](http://tnova.fr/sondages/l-observatoire-de-la-democratie-en-france-sondage-viavoice-terra-nova-la-revue-civique-le-monde-france-inter-lcp) sur ce thème.

Score : -1 / Réponses : 0

----

**Élire l'Assemblée nationale à la proportionnelle intégrale sur listes nationales.**

Score : -1 / Réponses : 1

----

**Pour l'élection des député.e.s, laisser la possibilité aux citoyen.ne.s de choisir entre quelqu'un issue de la liste d'un parti, quelqu'un tiré au sort, ou la [démocratie délégative](https://fr.wikipedia.org/wiki/D%C3%A9mocratie_liquide).**

Les sièges remplis de l'Assemblée serait répartis au prorata des différents choix entre les listes et des citoyen.ne.s aléatoires. Par exemple, si 30% votent En Marche et 10% votent aléatoire, 30% des sièges iront aux candidats d'En Marche et 10% à des citoyen.ne.s tiré.e.s au sort.

[Voir la proposition complète](https://forum.parlement-ouvert.fr/t/democratiser-lelection-des-parlementaires/79)

Score : -1 / Réponses : 2

----

Pour rebondir sur la question de l'absentéisme des députés et garantir leur participation aux travaux de l'Assemblée qui sont évoqués par plusieurs contributions ci-dessous, je propose d'accorder un véritable statut au suppléant des députés. 
Le suppléant ne serait plus mobilisé seulement lorsque le député est nommé au Gouvernement mais tout au long du mandat. Il représenterait ainsi le député lorsqu'il est absent dans l'hémicycle ou bien en commission, au sein du groupe, dans les réunions locales, etc.
Les électeurs éliraient donc un vrai binôme (femme et homme obligatoirement). On pourrait admettre que l'un des deux exerce à titre principal et que l'autre agisse en vertu d'une délégation de compétences ou de pouvoirs de manière occasionnelle (mais fréquente) tout au long du mandat. 
Lorsque l'un est en circonscription, l'autre est à Paris, lorsque l'un est retenu par une rencontre ou une réunion, l'autre siège (avec droit de vote et d'expression) à l'Assemblée, etc.

Score : -1 / Réponses : 0

----

On pourrait imaginer une procédure de consultation des députés auprès des citoyens de leur circonscription. Ainsi chaque député pourrait, à l'échelon de sa circonscription, interroger les électeurs sur une question d'actualité, ou sur un problème qu'il se pose lui-même ou sur l'intérêt de déposer un texte sur tel ou tel objet ou d'engager une réflexion sur telle ou telle chose. 

Soit cette consultation s'effectue comme un référendum non décisionnel, avec bulletins et vote dans les mairies ou ailleurs, soit elle se déroule par voie numérique, ce qui est sans doute plus facile à mettre en oeuvre à condition que les mairies ou préfecture mettent à la disposition des ordinateurs pour que tout le monde puisse participer. Dans ce  cas, chaque député pourrait disposer sur le site de l'Assemblée nationale d'une page dédiée aux consultations citoyennes des électeurs de sa circonscription. Chaque électeur pourrait alors voter une fois (il faudrait un mode d'inscription préalable par un numéro électeur qui lui soit personnel, avec un contrôle du nombre de votes, etc.).

Les consultations devraient répondre à des principes inscrits au préalable dans une Charte contraignante ou une loi, avec des sanctions (réelles et effectives du Bureau) en cas d'abus. Aucune consultation ne pourrait ainsi être initiée sur un texte déjà déposé ou en discussion à l'Assemblée sous peine d'avoir des interférences dangereuses évidemment. Il faudrait privilégier les questions locales.

Dans le prolongement de ce processus, chaque électeur pourrait déposer des propositions de consultation sur des questions locales que le député pourrait accepter et reprendre ou non, selon leur pertinence...

Score : -1 / Réponses : 2

----

j'estime que les députés doit sur le sujets patrimoine immobilier et le non cumul des mandats que tout les députés devoit envoyer leur compte rendu par Courrier Postal de leur mandat

Score : -1 / Réponses : 0

----

Il me semble qu'effectivement les députés finissent par se déconnecter quelque peu du peuple, même si ces députés sont de bonne volonté, ce que je crois.
Pour éviter cette déconnexion, pourquoi ne pas autoriser les députés à faire des sondages sur le peuple en demandant à de nombre concitoyen:
"Que pourrions nous améliorer dans votre vie"
"Que pourrions nous améliorer dans la vie de vos amis?"
"Que pourrions nous faire pour la société?"
Une appli installée sur le  téléphone des citoyens qui souhaitent éventuellement répondre au sondage permettrait cela.

Score : -1 / Réponses : 1

----

Il faudrait réformer d'urgence le droit à  mourir  dans la dignité : cela nous éviterait d'aller en Suisse ou en Belgique, et cela instaurerait une égalité entre tous les citoyens, aisés ou démunis.

Score : -1 / Réponses : 0

----

Ouvrir sérieusement le débat sur le droit à l'euthanasie.

Score : -1 / Réponses : 0

----

Bonjour, dans l'objectif d'intégrer les citoyens au processus de décision, il serait souhaitable d'envisager une dose de tirage au sort à l'Assemblée nationale et/ou au Sénat. Il faudrait que des citoyens volontaires de tout le territoire national, et sans antécédents judiciaires, puissent être désignés aléatoirement pour occuper temporairement des sièges de la chambre. Le débat devra porter sur la proportion de sièges aloués par le sort (plus de 10% pour être visible et influent, et moins de 50% pour ne pas supplanter les représentants élus), sur la durée de mandat et son caractère renouvelable (1 an non renouvelable assurerait la rotation des positions, moins de 5 ans pour ne pas supplanter les représentants élus), sur leur responsabilité devant la nation (mécanismes de contrôles/sanctions de présence) et sur les indemnités. Ce mécanisme aurait la vertu d'ouvrir la décision politique aux citoyens, de les intéresser en les rendant responsables, sans mettre en danger la stabilité de chambres dont les membres resteraient majoritairement élus pour de plus longs mandats. La décision serait éclairée d'une plus large compréhension sociologique des problèmes politiques.

Score : -1 / Réponses : 0

----

On parle beaucoup à juste titre de non cumul des mandats. Une difficulté est la reprise d'une activité professionnelle après un mandat ayant obligé à arrêter son activité. Il faut que tout citoyen actif (salarié public ou privé, artisan, profession libérale, entrepreneur) puisse avoir une garantie de retour dans son activité précédente sans perte de revenus. Une caisse de solidarité pourrait être crée avec un prélèvement sur les indemnités des parlementaires. Une loi doit protéger la carrière des salariés prenant un mandat, pour faciliter leur retour à l'emploi sans discrimination, en veillant à une réelle mise en pratique contrairement à ce qui se passe avec les syndicalistes.

Score : -1 / Réponses : 0

----

J’aimerai que nos élus Nationaux et régionaux, entre autres les députés, travaillent comme nous tous .
C'est à dire : Lorsque je ne suis pas au travail, mon employeur ne me paie pas. Pourquoi, un élu absent de l'hémicycle est tout d même rémunéré ?
Leur rémunération devrait être proportionnelle à leur présence, et si cette dernière est trop faible, il devrait être sanctionné (sanction pécunière et/ou limogé).

Score : -1 / Réponses : 0

----

Le régime particulier de certaines catégories professionnelles est montré du doigt par les députés. Par exemple les cheminots, mais ne sont pas les seuls.
En revanche, ils (les députés) en oublient un, très particulier, qui ne semble pas les choquer mais qui pour moi devrait être le premier réformé, pour s'aligner sur le régime général : Il s'agit du leur.
Elus, montrerz l'exemple !

Score : -1 / Réponses : 1

----

Sur la vie publique et l'interaction citoyen / élus, il y a deux axes à regarder :
1/ comment faire pour que les citoyens puissent s'exprimer
2/ comment s'assurer qu'ils ont réellement été écoutés

Sur le 1 actuellement, il existe de nombreux mécanismes de consultations ou d'intégration des citoyens dans la vie publique :
* conseil de développement au niveau agglo, interco et département par exemple
* rendez vous avec son maire, son député, etc.
Le problème est que ces mécanismes sont méconnus et que la disponibilité des acteurs fait qu'il n'est pas toujours possible de les rencontrer. De plus ces mécanismes sont aussi synonymes de fracture car en général non représentatif de la diversité de la vie publique (on retrouve surtout des CSP+)
=> il faudrait donc une plus forte communication sur ces mécanismes.
Ensuite coté citoyen, il est malheureusement plus facile de se plaindre que de participer de manière pro-active 

Les plateformes numériques permettent une participation facilitée à la vie publique, mais parfois trop facilitée. En effet cliquer sur un bouton "j'aime" ou "j'aime pas" s'est exprimé un avis succinct mais pas forcément très élaboré....  De plus il est possible de créer une pétition sur tout et à partir d'informations biaisées voire fausses...
Il y a donc peut être des gardes fous à apporter :
* proposer plusieurs choix sur chaque pétition (pour, contre, rien à faire, sans avis, etc.) de manière à ce que différents s'expriment
* permettre des commentaires et des avis étayés comme sur cette plateforme
* pour tout ce qui est "fake news", le problème c'est que la plupart des gardes fous risquent de s'apparenter à de la censure...

Il est également important de faire en sorte que tous les citoyens s'expriment et pas seulement puissent s'exprimer. En effet, dans le cadre de consultation en ligne, on retrouve souvent le même type de personnes (CSP+, personne ayant du temps, etc.) ce qui crée des biais idéologiques. Il faudrait donc aller chercher l'avis des personnes et non pas seulement attendre que les personnes parlent. Ceci demande un gros effort et une vraie politique d'inclusion de TOUTE la population.
Quelques idées : micro trottoirs, tirage au sort (mais peut on imposer la démocratie...)

Sur le deuxième point "prise en compte réelle de l'avis des citoyens" il est important d'apporter une réponse argumentée aux différents points soulevés par les citoyens. Trop souvent, les réponses sont du type merci de votre avis nous en tiendrons compte (ou pas...)
Il faudrait donc que les réponses soient réellement argumentées. Par exemple, sur la loi anti-terroriste en quoi demander les identifiants de personnes suspectes permettra de faciliter l'action de la police. Trop souvent les réponses sont du type "évitement" ou "argument d'autorité".
On veut des citoyens éclairés mais on ne veut pas qu'ils aient tous les éléments pour décider.

Dernier point la majorité n'a pas toujours raison, le fait d'avoir un certain nombre de votes ou de personnes d'accord avec soi, n'implique pas que nous ayons raison et il faudra donc expliquer en amont aux personnes, que le fait d'obtenir le seuil ne veut pas dire que leur idée sera une loi .... mais simplement que ca amènera peut être de la discussion. Pour reprendre quelques exemples qui arriveront : "la peine de mort", "l'immigration", "nombre de vaccins obligatoires ou non" et certains sujets dépendent donc d'une vision politique à long terme et de choix forts même s'ils peuvent aller à l'encontre de la majorité...

Score : -1 / Réponses : 1

----

On ne peut pas tancer les jeunes d'irrespect envers la police ou les professeurs et tolérer le spectacle pitoyable, quasi quotidien, de l'irrespect des orateurs, des lazzis, des noms d'oiseaux et autres gestes qui vaudraient outrages dans d'autres lieux et pour d'autres personnes. De même, une tenue correcte, y compris ne pas dormir, être avachi ou consulter son smartphone pendant les débats semble le minimum minimorum qu'on peut attendre d'un élu de la représentation nationale. Enfin un présentéisme rigoureux sur tous les débats et pas seulement le jour des questions au gouvernement, filmé, est là encore un minimum en terme d'exemplarité. Le niveau de présence donnera lieux à une proratisation du traitement et sera communiqué chaque mois. Par ailleurs, le fait que les députés ne puissent embaucher d'attachés parlementaires de leur famille me semble inepte. S'ils veulent tricher, ils pourront toujours embaucher un membre de la famille d'un collègue qui leur rendra la politesse. Il me semble plus pertinent de rendre accessible le travail des attachés ainsi que leurs qualifications. Je souhaite, in fine, une réduction du nombre de députés qui seront mieux rémunérés mais devront un travail à plein temps.

Score : -1 / Réponses : 0

----

Mettre en place un référendum d'initiative populaire

Score : -1 / Réponses : 2

----

Que ce soit pour les primaires de droite ou de gauche, il fallait signer une charte. Tout le monde parle de Laïcité, pourquoi ne pas signer une charte de laïcité à l'occasion des élections Nationales (ou autres, de façon à cibler un maximum d'électeurs).
ça aurait pour intérêt de rendre délictuel toute déclaration (ou acte), mettant en avant la religion par rapport aux lois de la république.
La signature engagerait et le refus de signature pourrait entrainer par exemple, la suppression (tout ou partie des subsides de l'état) .
Bien sur tout le monde ne se déplace pas pour voter, la réflexion n'est donc pas complètement aboutie, mais la solution se trouve certainement entre le système actuel et le vote obligatoire.

Score : -1 / Réponses : 0

----

Je souhaiterai que les délits de prescription soient totalement supprimés quelque soit le crime, le délai ou le fait.
Il doit toujours être possible pour une personne victime de faits d’aller devant le juge même des dizaines après les faits.
Je ne parle pas seulement des faits de viol et d’agressions mais aussi de touts les magouilles financières révélées des années après les faits,

Score : -1 / Réponses : 2

----

Bonsoir,
Cela fait un moment que je me pose la question ,Pourquoi le ministère du transport n'oblige pas les transporteurs étrangers a payé un tarif kilométriques utilisé sur nos route et autoroute pour rééquilibre la concurrence que je trouve déloyale avec les transporteurs Français.
Pour moi on a la solution et le matériels et déjà en place , se servir des bornes écotaxes  qui ne servent t'a rien a ce jour! Ce système est en place chez nos voisins frontaliers ceci est d'ailleurs la raison pour la quelle nos routes de l'EST sont régulièrement emprunter surtout par les routiers des pays comme la Pologne, Bulgarie, Roumanie etc.
voici ma petite réflexion pour l'économie Française sans forcement taper sur les Français

Score : -1 / Réponses : 0

----

Je trouve tout à fait anormal que les députés et sénateurs également décident eux même de leur avantages , pour moi cela doit revenir a un jury composé essentiellement d'électeurs de toute la France qui décidera de tous leurs avantages , on a vu ce que cela donne avec l'affaire Fillon ! ! !   Maya  .

Score : -1 / Réponses : 0

----

Les députés font les Lois. Très bien. Mais est-il normal qu'ils fassent les Lois qui régissent leurs propres activités. A mon avis, non. Cela éviterait les votes en catimini de Lois à leurs avantages, tels que, par exemple, l'abolition de la CSG sur leurs indemnités parlementaires. Votre avis, Monsieur Le Président.

Score : -1 / Réponses : 0

----

Bonjour, c'est quand que l'on commencera à diminuer d'une façon drastique le nombre d'élus dans ce pays ?  650 000 élus pour 65 millons d'habitants.... C'est énorme, incompréhensible et totalement injustifié !  Donc,  il nous faut supprimer le sénat et vite: économie 900 millions d'euros !  Ensuite, un député élu au suffrage universel pour 400 000 habitants: cela suffit amplement...  Ce qui nous ferait au maximun 165 ou 169 députés...
Diminuer le nombre de  fonctionnaires parlementaires ainsi que leur rémunération qui est deux à trois fois plus élevés que pour l'ensemble des fonctionnaires d'état....Voilà pour nos parlementaires, les élus de la nation...
Suppression du Conseil Economique Social et Environnemental (C.E.S.E.) qui n'a jamais rien produit d'utile: aucun de ces rapports, aussi pertinent soit il au dires de Mr Jean Paul Delevoye n'a  jamais  amélioré les conditions de vie des français....Naguère sous François Mitterand le C.E.S.E. était un refuge pour chanteur désargenté telle Georgette Lemaire...etc...Aujourd'hui c'est un refuge pour sportifs de haut niveau en manque d'argent et de notoriété telle Mme Laura Flessel avant qu'elle ne soit ministre.....etc..etc...
Diminution du nombre de communes par trois au minimum....

Score : -1 / Réponses : 0

----

Bonjour,

Cette démarche basée sur le numérique qui se veut démocratique et participative est de fait exclusive, vs inclusive, et stigmatisante. 
En effet, comme pour la loi "pour une République Numérique" de A.Lemaire, et validée par E.Macron, il semble que vous refusiez que les 20% de la population française ne possédant les compétences numériques de base, participent à la vie démocratique de notre pays. 
Dans cette loi, comme dans le budget en cours ou le travail actuel de Mounir Mahjoubi, il n'est pas prévu 1 € pour former ces personnes qui sont essentiellement  parmi nos concitoyens pauvres donc peu ou pas qualifiés.

Il s'agit donc, sauf erreur, d'acter le fait que près de 13 millions de personnes vivant ici, sont/seront entre autre, dépendantes des travailleurs sociaux pour se dépatouiller, au coup par coup avec les services publics numériques; cette inaction du gouvernement aura donc comme conséquence l’augmentation des couts sociaux en ce qu'elle déplace, et ce sera de plus en plus le cas, 13 millions de personnes vers l'exclusion, vs l'autonomie et le respect

C'est bien sur une attitude stigmatisante donc profondément anti démocratique car il y a près de 4000 Espaces Publics Numériques, ou assimilés, en France qui pourraient lutter contre cette exclusion numérique... mais il faudrait payer ces formations...
Du point de vue des comptes de la nation, l'occultation de cette situation participe à l'augmentation de la dette et de la baisse du niveau de vie des habitants.

En effet, le Royaume Uni (http://urlz.fr/5Ynr)  a fait faire une étude, en 2014; il en ressort que l'e inclusion, le fait que la quasi totalité des personnes , comme en Europe du nord, possède les compétences numériques de base, permettrait 70 milliards de livres de baisse des dépenses publiques d’ici 2020.
De plus, le « rattrapage numérique » de l’ensemble de la société britannique pourrait rapporter quelques 63 milliards de livres à l’économie nationale et à titre individuel des économies de l’ordre de 1064 livres/ an iront dans la poche des citoyens britanniques.

Il est donc clair que le status quo actuel, sous l'angle purement comptable, plombe les finances et l'économie nationales.
Il parait donc urgent de nouer des partenariats publics (un peu), privés (beaucoup),comme en Grande Bretagne, pour financer la lutte contre ce fléau.

Il y a même, potentiellement, une autre importante source de financement encore inusitée, participative  et socialement innovante, avec un cout 0 pour l'Etat, qui pourrait être utilisée.

Après avoir fait un état de lieux objectif, et proposé des solutions pour l'améliorer, je suis prêt à participer à leur concrétisation.

A vous lire.

Score : -1 / Réponses : 2

----

Suite à l'intervention de Monsieur François de Rugy, Président de l' Assemblée Nationale dans l'émission "On est pas couché",  j'ai pris connaissance du lancement de cette consultation démocratique. J’espère par le texte ci-dessous y contribuer efficacement à mon niveau.

Dans la phase de préparation de la Loi Copé-Zimmermann de janvier 201 1 sur la parité dans les Conseils d'Administration et de Surveillance, puis son extension en août 2014 aux entreprises de 200 à 500 salariés,  j'ai pu personnellement observer concrètement l'insuffisance de l'étude sur la manière dont l'application de la Loi serait véritablement évaluée et en conséquence qu' aucune disposition précise n'avait été pour ce faire inscrite dans la Loi. 
En particulier, il n'a pas été mentionné quelles seraient le calendrier des étapes à prévoir pour une telle évaluation de l’application, quels seraient les acteurs chargés de cette évaluation et avec quelles ressources, en particulier financières.
En conséquence, la façon dont cette Loi a été évaluée, et l'est encore aujourd'hui, est imparfaite.
Elle est au mieux menée de manière quelque peu désordonnée grâce divers acteurs opérant pour ce faire de manière quasi bénévole, tels mon cabinet d'expertise, Gouvernance & Structures. Monsieur  Daniel Lebègue, Ex Président de l’Institut Français des Administrateurs et Membre du Comité Scientifique de la présente consultation connait bien mon activité.  

Je note en particulier que dans le cas des entreprises non cotées, dès aujourd'hui concernées par la Loi,  aucune véritable évaluation n'a vu le jour. Des auditions éventuelles ne suffiront pas, car les statistiques indispensables ne sont pas disponibles.  
N'ayant pas la compétence, ni l'expérience nécessaire, je ne peux pas avoir d'avis éclairé sur l'approche générale à retenir pour l'évaluation des Lois,  mais pouvant utiliser comme l'un des points de départ cet exemple précis récent, il me semble qu'il faudrait au cours de ces travaux se pencher  sur l'amélioration de l'évaluation des Lois par l'Assemblée Nationale, et ceci  dès la préparation des Lois.  Pourquoi sur ce point ne pas étendre au Sénat et au CESE question posée ? Un diagnostic pourrait être établi, des conclusions tirées et des actions menées.

En résumé: 
Comme cela a été souvent avancé, sans doute moins de Lois, mais en évaluer l'application, tant pour les Lois existantes que celles  à venir, avec une approche et  des moyens précis définis en même temps que la Loi.

Score : -1 / Réponses : 0

----

J'aimerai que nos élus exercent leur contrôle sur le gouvernement et l'Etat, en lien avec les institutions concernées (ex: cour des compte), en pilotant des équipes transverses multi-ministères pour chasser tous les gaspis des différents ministères et organisations de l’état. La gestion des ces équipes transverses pourraient être coordonnée au niveau de la présidence due l'assemblée et du Sénat. Cela s'effectue avec efficacité dans la Grande Industrie qui économise ainsi des millions.

Score : -1 / Réponses : 0

----

Bonjour M.Le Président,

-cette consultation est une excellente initiative , à reproduire

-l’abstention aux élections est importante : le vote papier n’est plus adapté à notre de vie , aussi à l’instar des impôts ,le vote en ligne devrait être possible 

-des lustres allumés en continue à l’assemblée ça sert à quoi ?à rien et cela pollue...montrer l’exemple...

-les Gardes républicains alignés cela sert à quoi?à rien...j’aimerais bien que cet argent public servent intelligemment ,par exemple à augmenter le personnel auprès des enfants autistes…

Tout ceci est anachronique et donc à moderniser rapidement ...merci

Score : -1 / Réponses : 0

----

Arreter d'essayer de creer des plateformes numeriques dont le but est de demander aux citoyens leur avis sur le monde politique. Surtout quand des entrepreuneurs francais ce sont donné du mal pour le faire. Vous voulez connaitre l'avis des francais ? Fait la promotion de  "Stig" et vous l'aurez. 
Cordialement

Score : -1 / Réponses : 0

----

Contraindre les députés à supprimer au moins deux lois avant d'en voter une nouvelle. Et ce pour freiner l'inflation administrative, au mieux inefficace, au pire nuisible.
Remplacer le Sénat par une assemblée virtuelle des citoyens qui aurait un rôle consultatif.
Supprimer le Conseil économique social et environnemental et tous les conseils régionaux.

Score : -1 / Réponses : 0

----

Remettre d'urgence la Monarchie Constitutionnelle ( celle testée pendant 1 au début de la Révolution, peu de Français le savent) 😂

Score : -1 / Réponses : 0

----

réduisez le volume de lois et les manifestations de rue en changeant le mode de scrutin afin que les gens puissent élire leurs députés, présidents etc..  tout en sortant les lignes de programme qui ne leur plaise pas ... comme aux US.

Score : -1 / Réponses : 0

----

Inviter à l'assemblée nationale tous les participants à cette consultation.democratie-numerique.assemblee-nationale.fr pour un debriefing par grand thème.

Score : -1 / Réponses : 1

----

Développer une plateforme National dans laquelle chaque mairie pour faire appel aux citoyens afin de financer ces projets.

Un kickstarter public tout simplement.

Score : -1 / Réponses : 0

----

Bonjour, je suis une française expatriée. À la suite des difficultés que nous avons eues pour voter en Mai et en Juin, j'aimerais que l'on puisse voter une loi, obligeant le gouvernement, à nous assurer l'utilisation du numérique (nos ordinateurs, pour voter). Cette fois ci on nous a obligé à voter au consulat, sous prétexte qu'il y avait des risques . Ma circonscription électorale a la taille de l'Amérique latine et du sud. Il n'y a pas des consulats partout!

Score : -1 / Réponses : 1

----

Un Parlement efficace n'est pas nécessairement un Parlement pléthore. 

Attaché au bicamérisme, je tiens cependant à la réduction du nombre de parlementaires qui s'attacheront davantage au contrôle et à l'orientation de l'action du Gouvernement. 

La décentralisation a impliqué des compétences importantes aux collectivités territoriales trop nombreuses aussi et qui implique que les compétences transférées sont autant de travail en moins au niveau étatique. 

Les Etats-Unis ont un Sénat de 100 membres et une Chambre de 435 membres pour une population quatre fois plus importante que la nôtre et le Parlement américain est plus populaire et plus prompt à user de ses prérogatives. 

En France, on dispose d'un Sénat de 348 membres et d'une Assemblée de 577 membres. Il y a une surreprésentation si l'on se compare aux américains qui sont loin d'être sous-représentés. 

Dans une France hyper-connectée et ouverte sur le monde, dans une pratique politique et des usages nouveaux par les citoyens, la réduction du nombre de parlementaire est une nécessité.

Score : -1 / Réponses : 0

----

Bonjour, j’espérais qu'il soit possible d'élaborer des lois constitutionnelle afin de mettre en place l'initiative citoyenne et la transparence dans la vie publique dans la Constitution. En effet l'initiative citoyenne complétera l'article 6 de la DDHC, La transparence dans la vie publique précisera l'article 14 et 15 de la DDHC. Je rappelle que la DDHC a une valeur constitutionnelle. J'aimerais que la France soit le pays le moins corrompu du monde, comme les pays nordiques, selon la perception de l'indice de corruption de transparency international.

Score : -1 / Réponses : 0

----

Qui censure les idées? Comment voulez-vous que l'on participe si vous censurez les idées? La mienne est de remettre une Monarchie Constitutionnelle qui respecte les régionalismes... On n'avancera que comme ça en globalisation accentuée.

Score : -1 / Réponses : 1

----

Pour économiser nos ressources j'aimerais  qu'une réduction drastique du nombre de personnels politiques et administratifs de l'Exécutif et du Législatif leur permette de cohabiter dans les mêmes bâtiments (avec une séparation en cas de cohabitation politique).  Leurs palais nationaux d'aujourd'hui  deviendraient des musées pour tourisme ( donc rentables), ou des bibliothèques, ou des salles de  congrès...Les coordinations entre ces personnels ,   la mutualisation des ressources humaines, des outils administratifs, et des dépenses énergétiques, seraient plus efficaces et plus rapides. Rapidité aussi dans la mise en application des textes (décrets d'application, circulaires administratives....),

Score : -1 / Réponses : 1

----

Pour le bon fonctionnement il faudrait que les commentaires soit plus compacte car je pense que vous serez comme moi et en lire que la moitié.Et pour vos prochaines réunions et toutes reunions citoyennes  et d élus interdire le portable ,70 % des personnes sont déçus plus les intervenants quel exemples pour nos enfants et explique moi comment faite vous pour être à 100% dans la réunion.

Score : -1 / Réponses : 0

----

Réflechir à la mise en place de plus de collaborateurs et d'une enveloppe de fonctionnement plus importante pour les députés. Il est certain que c'est à contre-courant des idées actuelles dans l'opinion mais un parlementaire doit avoir accès à des expertises pointues et ne pas dépendre de tierces parties.

Score : -1 / Réponses : 0

----

La retransmission télévisée des questions au Gouvernement montre :
- un protocole désuet et dispendieux : hommes en armes et habit, tambours, livrées et autres artifices rappelant le passé monarchique de la France; 
- des députés qui cultivent l'intervention disruptive, s'invectivent, se conduisent grossièrement, et déshonorent ainsi l'Assemblée Nationale.

J'exprime ici une opinion que je sais partagée par nombre de mes concitoyenne.s et concitoyens qui souhaitent une République plus simple et plus sobre et une Assemblée Nationale plus consciente et plus responsable.

Score : -1 / Réponses : 0

----

On accorde trop d'importance aux métropoles qui flattent l'égo des hommes puissants et des politiques. Les métropoles sont des lieux de concentration. Ces lieux sont relativement petits par rapport au reste du territoire. En simplifiant tout se passe comme si les métropoles étaient peuplées et de vastes territoires autour restaient déserts. Cette situation est néfaste au regard de la préservation de notre environnement et de la survie même du vivant, dont l'espèce humaine. Quelques exemples pour s'en convaincre. 
L'agriculture devenue intensive, mécanisée, utilisant de nombreux intrants et pesticides est énergivore de dépendante. Idem pour l'élevage industriel qui ne respecte pas les bêtes. Les terres appartiennent à de grands propriétaires qui utilisent peu de main d'oeuvre. La production de ce type d'agriculture/élevage est de piètre qualité le plus souvent car la recherche du profit et du rendement passe avant la recherche de nourriture saine. Les produits ne contiennent pas de bactéries ou virus certes, mais ils contiennent des agents chimiques qui nous empoisonnent à petit feu sûrement.
Dans les grandes villes, l'existence de pôles ou zones de travail, d'habitation, de commerces bien distinctes génère de très nombreux transport de personnes. Sans cesse nous entendons parler d'amélioration des mobilités toujours plus chères (énergiquement) à réaliser ou utiliser. De plus l'amélioration de ces mobilités incite ou oblige les gens à toujours aller plus loin pour se loger ou pour travailler. C'est grâce au TGV et aux avions que les sociétés basées dans les métropoles peuvent envoyer leurs cadres très loin. En l'absence de ces liaisons rapides, ces mêmes sociétés seraient contraintes de créer des agences ou filiales mieux réparties sur le territoire. Au lieu d'améliorer la mobilité, il faut accroitre la multi fonctionnalité des quartiers. Ainsi les gens pourront éviter de nombreux déplacements. puisqu'ils trouveront emplois, habitations, écoles, commerces à proximité.
L'idée de concentrer sur le plateau de Saclay, une grande partie de l'enseignement supérieur et de la recherche est une ineptie en termes de multi-fonctionnalité: parce qu'elle exige plus de mobilité et parce qu'elle prive d'autres territoires d'universités ou d'écoles locales avec l'activité annexe associée.
Il serait donc opportun pour toutes ces raisons de lutter contre la création de nouvelles concentrations urbaines et de tenter de faire maigrir les concentrations urbaines existantes. Le moyen ? Décentraliser les emplois, les écoles, les commerces. Rendre les territoires de plus en plus multi-fonctionnels et de plus en plus autonomes.

Score : -1 / Réponses : 0

----

Simplifier ! nous connaissons une inflation du nombre de lois et de décrets sans que cela soit simplifié à un moment donné : plus de 10'000 lois, plus de 100'000 décrets. 
Le journal officiel est passé de 13'000 à 23'000 pages en 40 ans. Comment voulez-vous que nos élus (ainsi que nous citoyens ) nous nous y retrouvions ?
 Parfois, des lois votées ne sont toujours pas appliquées plusieurs mois après leur adoption définitive car les décrets n'ont pas été publiés, souvent par négligence ou parce que l'appareil juridique ne suit pas le rythme des députés.
A plusieurs reprises, des lois ont été annoncées alors que des mesures étaient déjà présentes dans des lois existantes. et parfois non appliquées.

Une proposition pourrait être qu'à chaque nouveaux textes votés, il devrait être convenu de rendre caduques au moins 1 texte afin de commencer un travail de simplification plus que nécessaire. 
Il faudrait s'assurer que les textes votés ne soient pas déjà présents et cela devrait être une vérification dans le cadre de nouvelles lois. 

Via une plateforme collaborative, il pourrait être également possible pour les citoyens de faire remonter des idées de simplifications.

Score : -1 / Réponses : 0

----

Autres idées : synthétisées sous la forme des "3 P" du travail du député

P pour pédagogie : beaucoup trop de préjugés et de croyances populaires sur le travail du député, et méconnaissance du travail législatif. Pédagogie pour répondre à la question : que fait le député ?
P pour partage : communiquer, recréer du lien via des permanences citoyennes, connaître l'organigramme de son équipe, avoir des vidéos explicatives et de vulgarisation sur le travail législatif
P pour proximité : travailler avec les acteurs locaux, comme les associations d'éducation populaire - pour encourager, redonner voix et confiance aux citoyens

Score : -1 / Réponses : 0

----

La mise en place d'un plan national de formation aux usages numériques pour tous les citoyens.
Les chiffres sont mauvais avec un Français sur cinq demeure "en dehors" du numérique, ne maîtrisant ni les outils ni les usages.
Il est donc légitime d'envisager d'en faire une priorité.

Score : -1 / Réponses : 0

----

Les citoyens français(e) doivent être invités à arbitrer les priorités national par une consultation généralisé, pour constater leurs difficultés et leurs suggestions, et s’impliquer concrètement dans l’amélioration de leurs vie.

Score : -1 / Réponses : 0

----

Je propose que les abstentions soient considérées comme un vote pour des personnes tirées au sort sur les listes des jurés d'assises.
Cela aurait beaucoup d'avantages:
* indépendance des élus tirés au sort par rapport aux partis, pour une meilleure indépendance des pouvoirs législatif et exécutif.
* meilleure représentativité des élus, la part tirée au sort serait un reflet de la population réelle.
* incitation des partis à rechercher un vote d'adhésion pour faire baisser l'abstention plutôt que des clivages et l'exploitation de"rentes de situation" qui la nourrissent.
* émergence de compétences, certains tirés au sort sortant du lot.
* redonner goût à la politique, chacun pouvant s'identifier à un élu tiré au sort, Et s'intéresser à son parcours (mieux que kho lanta!)
* règle le problème de sur-représentation des partis lorsque l'abstention augmente et qu'un parti recueille 80% du pouvoir de l'assemblée avec les votes de moins de 20% des inscrits.

Score : -1 / Réponses : 0

----

Quid du nombre de députés ?

Score : -1 / Réponses : 0

----

- Un parcours citoyen renforcé et obligatoire pour tous les mineurs futurs citoyens, qu'ils puissent visiter l'assemblée nationale ou le sénat ou rencontrer leurs élus ou collaborateurs, de façon à les intéresser et les impliquer. 
- Présence de jeunes à chaque séance par exemple. Qu'ils puissent observer comment l'intérêt collectif est défendu ( ou pas ) par l'élaboration de la loi. 

- Une assemblée nationale et un sénat des jeunes parallèlement.

> Le but étant d'en faire des futurs citoyens impliqués, critiques et avertis, à même de servir l'intérêt collectif.

Score : -1 / Réponses : 0

----

Il n’y a plus de politique intérieure nationale au sein de l’UE, pourquoi cette mascarade ? https://youtu.be/EQZY05KA-bE 
Tout ceci n’a de sens que si nous sortons de l’UE. Dites le.

Score : -1 / Réponses : 0

----

Bonjour, il me semble que l'Assemblée ainsi que le Sénat se composent de trop de membres. 
Parallèlement à la réduction de ses membres il y aurait une représentation de la Députation au niveau local qui serait beaucoup plus interactive dans la descente  -et explication- des textes ainsi que dans la remonté des informations des informations locales. 
La réduction du nombre de Députés augmente leur pouvoir de décision, afin de cadrer leur action il y aurait la mise en place d'un pouvoir intermédiaire -aussi valable pour le Sénat- chargé de valider ou non le réalisme (économique et social) des Lois présentées. 
Ce pouvoir "les 4 C" serait constitué de deux entités existantes dont les pouvoirs seraient étendus pour sortir du consultatif. Le Conseil Constitutionnel serait chargé de valider les Lois ainsi que de réformer le Code Civile, réformes présentables devant le Representation Nationale. La Cour des Comptes aurait pour charge l'aspect économique et pratique de l'application des Lois avec entre autre la rationalisation des dépenses de l'Etat, des investissements  etc. 
Les 4C serait un second pouvoir parrallele a la Presidence, au Gouvernement et à la Représentation Nationale sur une temporalité plus longue avec une rationalité accrue. La réforme est structurelle à la conduite d'un État centralisé.
Cordialement.

Score : -1 / Réponses : 0

----

Création d'une chaine TV sur internet pour chaque département. Ces chaines   abriteraient  des interventions vidéos du préfet du département et du préfet de région. L'objectif de ces communications étant d'expliquer le pourquoi des décisions aux citoyens et donc de donner du sens aux décisions/actions/choix pris ou subis. Les préfets seraient ainsi des véritables relais de communication grand public. La perte de sens, souvent en raison d'une méconnaissance du pourquoi, est selon moi un des facteurs clés du désintérêt des citoyens vis à vis de la vie politique

Score : -1 / Réponses : 0

----

Mon expérience des commissions parlementaires me fait souvent regretter la méconnaissance des travaux réalisés dans ce cadre où les postures partisanes sont souvent laissées au vestiaire. Nul besoin ici de réforme de fond; juste un peu d'organisation pertinente...

Score : -1 / Réponses : 0

----

Étant donné la rigueur budgétaire actuelle, il me semble possible de diminuer les salaires reçus par les kinésithérapeutes,très élevés pour un si faible niveau d études, et surtout considérant qu il s agit d argent public...Les personnes exerçant ce metier ont en effet des salaires supérieurs à 3000 ou 4000 e, même débutants, équivalent à des salaires de prof d université avec des carrières très avancées ! !! Cela n a pas de sens !

Score : -1 / Réponses : 0

----

https://twitter.com/i/moments/806931850438422529

Score : -1 / Réponses : 0

----

Instaurer un service avec des citoyens pour enclencher une vraie lutte contre l’évasion fiscale qui coûte 80 milliards d’euros chaque année à la France.

Score : -1 / Réponses : 0

----

Une petite idée : pourquoi pas de vote toujours secret au niveau des Assemblées. Le but ; redonner un peu de liberté aux élus qui aujourd’hui sont dépendant de leurs groupe politique.

Score : -1 / Réponses : 0

----

Le politique c'est la réflexion, le débat de tout un chacun dans tout lieu public sur des thèmes sociaux, philosophiques, économiques, de santé, d'éducation, d'environnement, de justice... de protection pour les populations précaires,  migrantes, pour les futures générations.... La démocratie c'est la reconnaissance de notre histoire, de notre constitution née de la révolution, pour aboutir à la fin de la monarchie, à la 1ère république, au 1er parlement représentant direct du peuple. L'action citoyenne ici et ailleurs, c'est l'ouverture,, la mixité, le dialogue, c'est la fin de toute discrimination, de toute forme de racisme, d'obscurantisme d'appartenance sociale, culturelle, religieuse, et que finisse cette phrase "je suis d'origine immigrée de... 3ème génération, que finisse le contrôle au facies... " pour un meilleur équilibre entre "égalité, liberté, fraternité". Cette devise qui se perd sous notre "République en état d'urgence et avec ses lois d'exception..."  Je souhaiterais que tous les mouvements citoyens soient entendus au Parlement -qui nous représente, qui devrait être à la hauteur de nos idéaux, de nos ambitions pour les générations futures... La parole devrait être donnée aux comités, mouvements, organisations citoyennes... et des propositions de réformes, de lois allant dans le sens d'une démocratie ouverte et franche, une démocratie qui soit digne de ses engagements, de ses responsabilités, de son histoire,  pour une voie vers plus d'égalité, de justice, de solidarité et non de protectionnisme, de sécuritaire, de fermeture et d'oubli de notre histoire multi culturelle, de notre histoire de colonialisme :  immigrations consécutives à notre besoin de développement  des années  50/70, pour notre essor industriel ; nous avons fait appel à l'immigration massive d'ouvriers, de familles venant de pays aujourd'hui stigmatisés ... Nous sommes responsables de cette non intégration, de la non-reconnaissance de leurs sacrifices ...  Nous avons le devoir de mémoire, de reconnaissance pour eux et leurs descendants...

Score : -1 / Réponses : 0

----

Ne pas négliger le suivi et la communication de celle ci de manière rigoureuse. Un bilan/synthèse régulier.

Score : -1 / Réponses : 0

----

Proposer une loi pour que certaines chaînes d'info tapageuses, ou celles proposant des tele-réalités dégradantes deviennent payantes ? Il faut proteger les cerveaux des plus grands et des plus petits. Est ce que la constitution le permet ?

Score : -1 / Réponses : 0

----

Lorsqu'une part (20% à 50% des inscrits d'une circonscription) des citoyens ne se sent plus représentées par son député, une nouvelle élection doit avoir lieu. 

Ainsi le député aura toujours le bien être de ses concitoyens en ligne de mire.

Score : -1 / Réponses : 0

----

Supprimer au nom de l'égalité des citoyens, du dispositif « verrou de Bercy » qui laisse qui laisse au ministère de l’économie le monopole des poursuites pénales en matière de fraude fiscale. Ce verrou », porte une atteinte certaine  à l’égalité des citoyens devant la justice, et permet de  donner du poids aux gros contribuables pour négocier hors transparence.

Score : -1 / Réponses : 0

----

Avoir accès  dans tous Les pays (ca ne marche  pas si on est au usa) aux replay des débats  en commissions en assemblée  et au interviews menées  l assemblee. (Par exemple les entretiens publique lors d enquêtes  ou de consultations  menées par l assemblée comme pour le groupe Bigard)
C est aujourd hui seulement possible partiellement en live avec lcp. 

Voir la liste des gens qui sont censés y sieger

Score : -2 / Réponses : 3

----

Je propose d'ouvrir L'assemblée aux jeunes étudiants et lycéens ! Plusieurs régions possède des PRJ ( Parlement Régional de la Jeunesse ) ou des jeunes de toutes ages ou formation ( Lycées , Université , CFA ,ect..) travaillent pour améliorer les conditions de vie des jeunes dans leurs région . Dans différent thèmes . Les élus régionaux ensuite votent nos projets ( qui peut porter sur n'importe quel sujets) . Cela permet a la jeunesse d’être écouté par les représentants de la nation ! Je trouve dommage que L'AN n'est toujours pas de système similaire . Cela permettrait a la jeunesse de se réconcilier avec la politique et de la sortir de l'isolement que tout les gouvernements droites ou gauches ( y compris celui-ci) affligent aux jeunes Français .

Score : -2 / Réponses : 0

----

Créer une Assemblée de l’intervention populaire et du long terme
 émettant un avis sur l’impact écologique et social des lois. Elle pourrait notamment défendre les avis des conventions de citoyen·ne·s devant les député·e·s.

Score : -2 / Réponses : 0

----

**Assurer une représentativité réelle à l'Assemblée nationale**, par exemple grâce à un scrutin à la proportionnelle sur listes nationales.

Score : -2 / Réponses : 0

----

**Fournir aux député.e.s 300 experts disponibles pour répondre à leurs questions et les aider à comprendre et rédiger des lois.**
Cela peut se financer en augmentant la dotation de l'Assemblée nationale, ou en divisant par deux le salaire des député.e.s

Score : -2 / Réponses : 2

----

Refuser la logique de l'exception pour réaffirmer l'État de droit

    Permettre la sortie de l'état d'urgence à l'initiative du Parlement, état qui ne protège pas mieux
    Faire l'évaluation des lois antiterroristes sécuritaires existantes
    Arrêter progressivement les opérations Sentinelle pour confier la sécurité des lieux publics à la police
    Garantir le contrôle par le juge judiciaire des opérations de lutte contre le terrorisme et augmenter les moyens de la justice antiterroriste pour garantir une lutte efficace, durable et respectueuse des droits et libertés fondamentaux

Score : -2 / Réponses : 1

----

Démocratiser l'accès aux responsabilités politiques en permettant à chacun de prendre un congé républicain, sans risque pour son emploi ou ses droits quels qu'ils soient, en vue de se présenter à des élections

Score : -2 / Réponses : 1

----

Je pense qu'il serait intéressant, à l'instar du code de déontologie des députés, d'établir une charte de la participation citoyenne au sein de l'Assemblée nationale. Outre quelques principes élémentaires, ce texte (charte ou code...) contiendrait en annexe une liste des "bonnes pratiques" effectuées par les députés en circonscription ou sur Internet. Ainsi, par exemple, la pratiques des ateliers législatifs citoyens pratiqués en Saône-et-Loire serait mentionnée et expliquée en annexe. Il aurait été intéressant d'y mentionner également les procédures mises en oeuvre par certains députés dans l'usage de la réserve parlementaire, notamment la création de jury citoyen (même si cette réserve, on le sait, est supprimée par la loi du 15 septembre 2017). Cela inciterait les députés à inventer de nouvelles procédures participatives ou à s'approprier celles déjà existantes.
Parmi les principes à énoncer pourraient être mentionnés dans la Charte la possibilité d'organiser une consultation citoyenne numérique sur les propositions de loi ou bien encore la nécessité de se rapprocher par tous moyens des citoyens pour les consulter lorsque l'on est rapporteur d'un projet ou d'une proposition de loi, etc. Il faut creuser encore la mention des principes, mais c'est une voie à explorer...

Score : -2 / Réponses : 0

----

**Élire l'Assemblée nationale avec le système [mixed-member proportional](https://en.wikipedia.org/wiki/Mixed-member_proportional_representation)** : utilisé en Allemagne, en Bolivie, en Écosse et en Nouvelle-Zélande, ce mode de scrutin permet d'assurer une représentation proportionnelle au niveau nationale en même temps que des député.e.s élu.e.s au niveau local, par circonscription (ça repose sur le fait de réserver une part -- disons la moitié -- des sièges de l'Assemblée à des élu.e.s sans attache territoriale).

[71% des Français y sont favorables](http://adrien-fabre.com/sondage/resultats.php#_ed).

Score : -2 / Réponses : 2

----

Dominique Valck, Président du Conseil de développement durable de la Métropole du Grand Nancy et Co-Président de la Coordination Nationale des Conseil de développement

**Consultation des citoyens sur les grands projets** :

http://conseildedeveloppementdurable.grand-nancy.org/fileadmin/documents/agora/Fonctionnement/contributions/2015/2015-01-27contribution_C3D_consultation_Vandieres_V_lecture_ecran.pdf

http://www.conseils-de-developpement.fr/

Je souhaitais vous faire par des réflexions du Conseil de développement durable d e la Métropole du Grand Nancy sur les consultations citoyennes.
En effet, notre assemblée a souhaité en 2015, dans le cadre de la consultation des lorrains sur l’implantation de la gare à Vandières et la reconversion de Louvigny en gare fret TGV, produire un éclairage sur la qualité démocratique d’un processus démocratique et construire une grille de questionnement nécessaires pour se forger une opinion sur ce vaste dossier. Bases qui nous auraient semblées nécessaires à minima pour que les Lorrains puissent prendre position en connaissance de cause.
Ce processus de consultation, s’il a eu le mérite d’être utilisé pose question quant au délai d’une démarche qui aurait assurément mérité de prendre son temps. Prendre le temps pour réconcilier élus et citoyens sur des échanges constructifs et la co-construction de politiques publiques au meilleur de l’intérêt commun et sur le processus de prise de décision qui dans ce cas n’a pas abouti et qui n’a pas été explicité clairement aux lorrains.
Nous avons souhaité proposer un schéma pour construire de prochaines consultations citoyennes.
La seconde partie de cette contribution porte sur les questions plus techniques et en chaîne que soulève ce projet et dont les réponses ne nous ont semblaient peu transparentes, peu lisibles et difficiles à récolter, pour le citoyen non expert, afin qu’il réponde en conscience à la vaste question posée.
Compte tenu des réflexions actuelles de la société civile et de l’assemblée nationale sur l’idée générale de rénovation démocratique, j’ai le plaisir de vous transmettre notre contribution validée par le Bureau du Conseil de développement durable du 26 janvier 2015.
Par ailleurs, vous trouverez également un lien vers le site de la Coordination Nationale des Conseils de développement (CNCD), les Conseils de développement se sont emparés de ces dossiers de participation citoyenne depuis longtemps. 
En vous remerciant de toute l’attention que vous voudrez bien porter au travail de notre assemblée, je reste à votre disposition pour échanger sur ce dossier.

Score : -2 / Réponses : 1

----

Avoir une page dédiée aux différentes formes d'outils démocratiques pour que chacun puisse voir comment il peut participer à la vie politique du pays (en s'inspirant de ce document?: http://www.fondation-nature-homme.org/magazine/democratie-participative-guide-des-outils-pour-agir).
Puis un outil de suivi de la proposition dès qu'elle rentre dans l'agenda officiel législatif: quand est-ce que le texte est étudié, débattu, et quand il est accepté ou non. Avec la possibilité à tout moment de contacter les personnes qui travaillent sur ce texte (téléphone, mail, rendez-vous, visio-conférence, etc.). En s'inspirant de la plateforme: la fabrique de la loi.

Score : -2 / Réponses : 0

----

Il y a trop de lois ! Il faudrait justement créer un service dont l'objectif serait de supprimer les lois inutiles ou surnuméraires. Ex la loi sur l'eau ; un tiers de la loi est constitué de principes généraux...
Un autre problème : le domaine de la loi trop important, beaucoup de texte ne devraient être que des décrets... 
le Conseil Constitutionnel devrait être supprimé : Il est insupportable que des juges soient au-dessus du législateur. S'il devait être maintenu qu'au moins ses juges soient élus afin qu'ils aient la légitimité nécessaire pour intervenir dans le processus législatif. La même remarque, l'élection, pourrait être faite au sujet du Conseil supérieur de la Magistrature. Les juges interviennent sur la loi par la jurisprudence alors qu'ils ne sont pas élus. Dès lors, pourquoi les Présidents du Tribunal de Grande Instance et Procureurs de la République et même Président des Tribunaux Administratifs ne seraient ils pas élus ?

Score : -2 / Réponses : 0

----

Ne faudrait il pas créer un " statut du citoyen" dans lequel, pour voter, la Nationalité ne serait plus suffisante sauf à signer en plus " une charte de la démocratie et de la République" dans lequel le citoyen reconnaitrait notamment le principe de la laïcité, entendu comme le renvoie de la religion "à la maison", et les principes données par le préambule de la constitution et même la constitution. Une sorte de "permis de voter" du citoyen ?

Score : -2 / Réponses : 0

----

Préparons l'avenir...
La période de fonctionnement de cet espace de participation citoyenne se termine le 09 novembre 2017.
Pour que des discussions, des travaux , des sondages, etc puissent être lancés par des citoyens motivés,  je vous propose de constituer une liste avec les mails des intervenants intéressés.
 Le principe de base est que la liste ne sera communiquée qu'à ceux qui y seront inscrits.. Et l'on pourra demander à en sortir à tout moment.
Je vous donne mon mail bachaud.yvan@free.fr . Cordialement

Score : -2 / Réponses : 0

----

Il faudrait déjà avant de faire une loi voir se qu ils se passentautour de nous et que toute loi soit compatible avec l Europe pour que l on ne soit pas pénalisé par rapport à d autre pays comme souvent aujourd'hui  . Donc consulte les citoyens d autre pays de l Europe pour avoir aussi leur avis.

Score : -2 / Réponses : 0

----

Il me semble nécessaire de changer le mode de scrutin pour l'élection des députés, afin de rendre l'Assemblée nationale plus représentative des citoyens. Mais il n'est pas question de revenir à la proportionnelle intégrale, qui a provoqué l'instabilité gouvernementale sous la Quatrième République ! 

Pour allier représentativité et stabilité, je vous propose d'instaurer un mode de scrutin mixte : la majorité des députés serait élue au scrutin proportionnel au niveau régional, tandis qu'une minorité de députés (dans les régions ou territoires qui comptent moins de quatre députés) serait élue au scrutin majoritaire à deux tours. Il y aurait alors deux principaux cas de figure :
 
* Si un parti obtient la majorité absolue des sièges dès le premier tour, aucun second tour n'aurait lieu, sauf bien sûr dans les rares circonscriptions concernées par le scrutin majoritaire à deux tours.
* Si aucun parti n'obtient la majorité absolue des sièges au premier tour, un second tour serait organisé dans toutes les circonscriptions, y compris dans celles concernées par le scrutin proportionnel. Dans ces dernières circonscriptions, seules les deux formations arrivées en tête à l'échelle nationale lors du premier tour pourraient participer au second tour. Trois quarts des sièges de chacune de ces circonscriptions seraient répartis à la proportionnelle, en fonction des résultats du premier tour ; le quart de sièges restant constituerait une prime majoritaire, revenant à la formation ayant obtenu le plus de voix à l'échelle nationale au second tour.
 
J'ai fait différentes simulations pour voir ce qu'aurait donné un tel mode de scrutin dans la réalité, en fonction des résultats des législatives de 2002, 2007, 2012 et 2017. Ainsi, lors des dernières législatives, La République en marche n'aurait obtenu que 210 sièges sur 577 au premier tour, mais à l'issue du second tour, cette formation aurait eu un total de 303 sièges après attribution d'une prime majoritaire. De même, en 2012, le PS n'aurait eu que 208 sièges au premier tour, mais aurait eu un total de 298 sièges au second tour après attribution d'une prime majoritaire. Et en 2007, l'UMP aurait eu 296 sièges dès le premier tour, du fait de son avance en pourcentage de voix sur les autres partis.

En outre, si à l'issue du second tour, la formation arrivée en tête n'obtenait, par exemple, que 51 % des voix à l'échelle nationale, alors que l'attribution de la totalité de la prime majoritaire aurait pour effet de lui donner plus de 60 % des sièges de l'Assemblée nationale, les sièges « en trop » seraient attribués à la formation arrivée seconde. Ainsi, nous aurions évité la situation des législatives de 2002, où la droite parlementaire avait obtenu 398 sièges (soit près de 69 % des sièges de l'Assemblée) avec seulement 52,76 % des voix au second tour (pourcentage qui, appliqué au nombre de sièges de l'Assemblée, correspond à seulement 304 sièges...).

Autre avantage majeur de ce mode de scrutin : il ne nécessiterait pas de révision constitutionnelle. Il s'agirait de modifier le Code électoral, ce qui est déjà moins difficile que de réformer la Constitution ! 

Comme je l'ai écrit plus haut, j'ai fait différentes simulations montrant quels auraient été les résultats des législatives des 15 dernières années avec un tel mode de scrutin. Je les ai envoyées à plusieurs députés, notamment à madame Paula FORTEZA, députée de la deuxième circonscription des Français de l'étranger, qui s'est montrée intéressée et qui m'a incité à venir sur ce site. Un grand merci à elle.

Score : -2 / Réponses : 5

----

Interdire de faire une carrière politique. En vraie démocratie, la politique est l'affaire de tous. Idéalement chaque citoyen majeur devrait au moins une fois dans sa vie être membre d'une institution démocratique (niveau communal, départemental, régional ou national). Au plus chaque citoyen est membre d'une institution une seule fois. Pour réaliser cette participation de tous à la vie politique, on ne peut se satisfaire du système des élections trop lourd, trop cher, élitiste. Il semble donc que le tirage au sort soit la seule solution simple et viable pour renouveler les membres des institutions. Ce tirage au sort doit de plus respecter au mieux la représentation des diverses classes sociales du territoire communal, départemental, régional, national. Par exemple si 30% d'ouvriers, 30% de membres dans l'institution. Idem pour la représentation des classes d'âge. Point besoin d'être expert, intellectuel ou d'avoir suivi de hautes études pour être membre. N'oublions pas que les membres de toute institution ne sont qu'un système de gouvernance de l'administration sous-jacente. Le renouvellement des membres doit être assez rapide et sans doute progressif (par tiers par exemple). En effet le renouvellement rapide permet au membre de retrouver son emploi initial après son mandat. De plus le renouvellement rapide limite fortement la pression des lobbies. La rémunération du membre de toute institution est égale à ses revenus (salaires, rentes, ...) dans sa situation avant mandat. Il n'y a donc aucun avantage financier lié à la mandature. Les frais et dépenses liés à la bonne réalisation du mandat sont plafonnés et justifiés. Etre membre d'une institution, c'est d'abord un devoir civique de citoyen. Ces membres sont essentiellement au service des autres citoyens durant leur mandat. Cette situation est assez comparable aux membres d'un conseil syndical par rapport aux copropriétaires.

Score : -2 / Réponses : 0

----

Beaucoup de contributions appellent à un renforcement de l'usage du referendum, notamment d'initiative citoyenne
A l'heure où beaucoup de décisions ou de positions se prennent sur la base des sondages et où les représentants élus et les corps intermédiaires accusent une crise de légitimité cela est incontestablement une idée à creuser.
Ma proposition en revanche serait certes de faire des referendum mais pas à réponse binaire OUI ou NON par rapport à une question donnée mais pour choisir entre deux (ou plusieurs) propositions également documentées et budgétées.
Le parlement et les services de l'état pourraient largement proposer deux ou trois grandes options structurantes (celle soutenue par le gouvernement, celle soutenue par l'opposition de droite, celle par l'opposition de gauche...) sur chaque thématique portée au vote des citoyens et ceux-ci trancheraient en connaissance de cause ... mais jamais au profit de l'immobilisme ou de la seule critique du gouvernement en place.

Score : -2 / Réponses : 1

----

Transformer via un référendum le Sénat en en faisant un outil de renouveau du lien démocratique et de reconnexion du monde politique avec les réalités de la vie de la majorité des citoyens. Cette transformation commencerait par redonner à la chambre haute du Parlement la dénomination qu’elle avait sous la IVème République : « Le Conseil de la République ». Cette nouvelle institution pourrait questionner, faire des propositions et contrôler le travail de l’Assemblée Nationale. Elle serait constituée à 50% de représentants élus des collectivités (comme les sénateurs actuels) et pour l’autre moitié de citoyens nommés par tirage au sort (avec une méthode permettant d’assurer la parité, la représentativité des classes d’âge, des catégories professionnelles et des zones géographiques ) sur le modèle de ce qui a déjà été expérimenté avec succès dans des pays comme l’Islande, l’Estonie, l’Irlande et le Canada. Les «conseillers» élus représentants des collectivités locales auraient un mandat de 5 ans alors que les «conseillers» citoyens tirés au sort le seraient pour un an et bénéficieraient d’un accompagnement pour les assister dans leurs travaux et d’un système de « congé démocratique » (sur le modèle du congé maternité) leur permettant de se consacrer pleinement à leur tâche puis de reprendre le cours de leur activité professionnelle.

Score : -2 / Réponses : 0

----

visiblement, la ruralité est de plus en plus oubliée par l'exécutif malgré ses promesses. la cause principale est la disproportion entre les élus des villes et les élus des champs. on parle de réduire le nombre de députés ? coupons déjà les députés qui, soit disant, représentent les français de l'étranger. pour les défendre, il y a des centaines de consuls et d'ambassadeurs avec leurs services.
ensuite on va répartir les circonscriptions, non en fonction du nombre d'électeurs mais de la surface du territoire. exemple : trop d'élus à paris, lyon, lille, marseille, ... mais quelle différence entre la défense des intérêts des électeurs de paris 16ème nord et de ceux de paris 16ème sud ? d'un autre côté, a-t-on besoin d'un député et d'un sénateur pour le micro territoire de st-barthélémy qui pourrait très bien être couvert, comme avant, par les élus de la guadeloupe. sans douleur, on peut supprimer près de 100 sièges avec leurs avantages coûteux, retraite comprise. avec un peu de recherche, on peut ramener le nombre total de députés à 360 issus des circonscriptions + 40 élus à la proportionnelle, sur liste nationale. total 400 au lieu de 577.

Score : -2 / Réponses : 0

----

C'est bien de vouloir intervenir encore faut-il savoir de quoi l'on parle et avoir conscience des incidences et effets que cela peut provoquer. La intervient les capacités à analyser le problème dans son ensemble dans lequel s'inscrit  la plupart du temps la composante humaine avec tout ce que cela implique s'il y a un problème sous jacent d'ordre psychique. Mais il est nécessaire de faire évoluer un système au risque de le voir devenir obsolète. Il suffit d'ouvrir les yeux et d'entendre.

Score : -2 / Réponses : 0
