## Quel rôle pour les citoyens dans l’élaboration et l’application de la loi ?

### Consultation pour une nouvelle Assemblée nationale

# Clôture de la consultation

<div><span style="color: rgb(31, 73, 125); font-size: 12pt;">La consultation est désormais close.</span></div>
<div><br></div>
<div><span style="color: rgb(31, 73, 125); font-size: 12pt;">Vous avez déposé 1334 contributions, 1700 commentaires&nbsp; et avez voté 17 321 fois. Un grand merci à tous pour votre participation.</span></div>
<div><span style="color: rgb(31, 73, 125); font-size: 12pt;">&nbsp;</span></div>
<div><span style="color: rgb(31, 73, 125); font-size: 12pt;">L’ensemble des contributions fera l’objet d’une synthèse qui sera publiée mi-décembre 2017 avec le premier rapport du groupe de travail sur la démocratie numérique et les nouvelles formes de participation citoyenne.</span></div>
<div><span style="color: rgb(31, 73, 125); font-size: 12pt;">&nbsp;</span></div>
<div><span style="color: rgb(31, 73, 125); font-size: 12pt;">Auparavant les meilleurs contributeurs, sélectionnés par le comité scientifique de la consultation sur la base des critères préétablis, seront conviés le 25 novembre 2017 pour travailler avec les députés sur des propositions qui pourront être reprises par les parlementaires.&nbsp; Ces ateliers de travail feront l’objet d’une restitution publique.</span></div>
<div><br></div>
<div><span style="color: rgb(31, 73, 125); font-size: 12pt;">Retrouvez toute l’actualité et l’activité du groupe de travail&nbsp;sur la démocratie numérique et les nouvelles formes de participation citoyenne (voir Plus d'information).</span></div>
