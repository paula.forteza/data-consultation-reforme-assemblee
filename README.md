# Quel rôle pour les citoyens dans l’élaboration et l’application de la loi ?

## Consultation pour une nouvelle Assemblée nationale

* [Clôture de la consultation](/5a04869f065b06ef10281d05/)
* [Bienvenue sur cette plateforme](/59d737c712406bdd5d627805/)
* [Initiatives citoyennes](/59d734d112406bdd5d6277ff/)
* [Consultations en amont des textes](/59d7347212406bdd5d6277f7/)
* [Interactions avec la procédure législative](/59d733cb12406bdd5d6277ea/)
* [Participation à l'évaluation de la mise en œuvre des lois](/59d7335412406bdd5d6277e4/)
* [Participation numérique et participation présentielle](/59d7325812406bdd5d6277d4/)
* [Autres idées ?](/59d731a712406bdd5d6277d1/)
