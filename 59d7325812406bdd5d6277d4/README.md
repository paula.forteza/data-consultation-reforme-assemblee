## Quel rôle pour les citoyens dans l’élaboration et l’application de la loi ?

### Consultation pour une nouvelle Assemblée nationale

# Participation numérique et participation présentielle

<div style="text-align: justify;"><img src="https://consultation.democratie-numerique.assemblee-nationale.fr/an/participation_numerique.png"></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">Le numérique permet de généraliser la participation aux travaux du Parlement. Toutefois, une des grandes limites de cet outil est l’existence d’une fracture numérique. Elle se manifeste notamment par le fait que certains n’aient pas accès à internet ou ne soient pas à l'aise avec l'informatique. Par ailleurs, les consultations numériques sont fondées sur le volontariat. Il existe des biais importants en termes de représentativité des participants.</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">&nbsp;</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">Les formes de participation numérique doivent donc être complétées par d'autres formes de participation, par exemple par l'intermédiaire de visioconférences ou en présentiel. Il est notamment possible de réunir des panels citoyens tirés au sort.</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">&nbsp;</span></div>
<div><i style="font-size: 11pt;"><span style="color: rgb(107, 36, 178);">Comment articuler participation numérique et autres formes de participation? Dans quels cas privilégier l'une ou l'autre de ces formes ? Comment garantir l'impartialité et la rigueur des consultations ?</span></i></div>
<div><br></div>
<div><span style="font-size: 10pt; color: rgb(230, 0, 0);">Nota bene</span><span style="font-size: 10pt;">&nbsp;: par défaut, les contributions les plus populaires apparaissent en tête de la page par ordre décroissant. Vous pouvez si vous le souhaitez afficher les contributions les plus récentes ou les plus anciennes. Pour cela, modifiez le mode de </span><b style="font-size: 10pt;"><u>classement</u></b><span style="font-size: 10pt;"> à droite de «&nbsp;Commentaires&nbsp;» avant la 1ère contribution.</span></div>

#### Commentaires

Comme évoqué lors de notre intervention dans l'évènement du 20 septembre "Réinventez l'Assemblée nationale" organisé par Make.org en présence de plusieurs députés et du Président de l'Assemblée nationale, Kawaa réitère par cette plateforme notre proposition de développer des débats "hors les murs" pour que le débat public se développe de manière décentralisée dans chaque circonscription, en complément des débats "dans l'hémicycle". Une rubrique "Hors les murs" pourrait ainsi voir le jour sur le site de l'Assemblée nationale permettant aux citoyens, députés et acteurs de la société civile de créer des débats ou d'y participer. Les lieux qui souhaitent accueillir ces débats (cafés, ...) pourront également s'y référencer. Voici une ébauche de ce que cette page pourrait donner : https://drive.google.com/a/essec.edu/file/d/0B2urUjlWrNODb3BpSGU5UjZZSEk/view?usp=sharing

Score : 125 / Réponses : 14

----

**L’article 3 : pour une reconnaissance du tirage au sort**
  
L’article 3 de notre Constitution réserve tous les pouvoirs à nos représentants élus. Si le développement du système de l’élection de représentants a constitué une avancée historique lors de la Révolution française, par rapport au fonctionnement d’une monarchie absolue et arbitraire, il apparaît néanmoins que notre système de démocratie « représentative » est à bout de souffle en ce début de 21ème siècle. 

Aujourd’hui dans une société qui n’a jamais été aussi éduquée, les citoyens ne supportent plus de subir des décisions prises sans eux. La défiance vis-à-vis d’une classe politique qui semble autocentrée et consanguine avec des intérêts privés puissants est à son comble. Néanmoins l’individualisme et le fonctionnement des réseaux sociaux font qu’il n’existe plus de lieux de délibérations collectives permettant de définir un bien commun inclusif des intérêts du plus grand nombre. 

De nombreuses expérimentations de conventions de citoyens ont montré la qualité des débats de simples citoyens même sur des sujets complexes, mais ces expérimentations sont toujours consultatives et n’engagent jamais les politiques.

C’est pour cela qu’à Sénat Citoyen ( http://www.senatcitoyen.fr/ ) nous pensons qu’une façon de recréer ces espaces de délibérations citoyennes au service du bien commun est de mettre en face des 600 000 élus de la république 600 000 citoyens tirés au sort. Cette obligation civique d’une durée de deux ans, généralement à temps partiel, sauf au niveau national, doit permettre d’injecter massivement les préoccupations des citoyens dans l’agenda politique. La mission de ces assemblées citoyennes serait de questionner et contrôler les élus avec un pouvoir d’enquête qui pourrait déboucher dans les cas ultimes sur un processus de révocation.  

En effet nous prônons la mise en place d’un nouveau principe démocratique opérationnel : **« Pour tout pouvoir constitué, exécutif ou assemblée élue, une assemblée citoyenne tirée au sort est chargée de questionner et contrôler ce pouvoir »**. Vous pouvez voir sa déclinaison au niveau national [ici](http://senatcitoyen.org/PDF/S%c3%a9nat%20Citoyen%20-%20Le%20projet%20-%2014%20Dec%202016%20-%20Web.pdf)

La mise en place de ce principe nécessiterait une refonte profonde de la Constitution. Néanmoins pour permettre des expérimentations au niveau local, et notamment pour les structures "non démocratiques" que sont les intercommunalités, il faut déverrouiller le monopole des élus inscrit dans l’article 3 pour reconnaitre la représentation par tirage au sort.

**Proposition d’article 3 :**

La souveraineté nationale appartient au peuple qui l'exerce, à tous les niveaux de la république, par ses représentants **élus ou tirés au sort** et par la voie du référendum …

…

Sont électeurs, **éligibles ou tirés au sort,** dans les conditions déterminées par la loi, tous les nationaux français majeurs des deux sexes, jouissant de leurs droits civils et politiques.

Score : 99 / Réponses : 8

----

Apparemment, une tare courante de la démocratie participative est que ce sont toujours les mêmes personnes qui participent (c'est, pour ma part, ma première participation). Pour éviter cet écueil, il faut en informer le grand public (la plupart des gens à qui j'en ai parlé n'étaient pas au courant). Il faudrait une campagne télé, radio et journaux. Je n'ai pas la moindre idée du cout, mais cela me semble fondamental pour attirer un nombre important de personnes.
À fortiori, lorsque de telles consultations seront normalisées, on pourra mettre en place un devoir d'information des médias. Pas sous forme d'opinion politique, mais simplement un message tel : "Cette consultation s'ouvre, vous pouvez donner votre avis sur cette plateforme ...", cela diffusé largement. On peut même imaginer ces informations passées sur les réseaux sociaux, sous forme de posts publicitaires temporairement ancrés en haut des fils d'actualités des pages facebook et twitter des personnes se connectant depuis la France.

Score : 87 / Réponses : 6

----

La démocratie se vit aussi sur le terrain, et l'élu doit en être un acteur actif.
C'est pourquoi je propose d'instaurer pour les députés le **devoir d'aller à la rencontre des citoyens**, au moins une fois par mois et pas seulement en période électorale.
Concrètement, on peut s'inspirer de [l'exemple d'un député au Québec](https://aller-retour.org/je-vous-ecoute) : si une rencontre politique rassemble au moins vingt citoyens, alors le député est tenu de répondre à l'invitation.
Comme la rencontre est à l'initiative des citoyens, elle peut servir aussi bien à initier une loi, à faciliter la concertation, à interagir avec la procédure législative ou à contribuer à son évaluation.
Même si certains députés le font déjà spontanément, institutionnaliser cette bonne pratique renforcera le lien entre les citoyens et leurs élus.

Score : 37 / Réponses : 2

----

**Pas de « civic tech » sans inclusion numérique ! **

La mission Société Numérique, pôle de l’Agence du Numérique,  déploie un programme d’action pour favoriser l’autonomie et la capacité de tous à saisir les opportunités du numérique et pour accompagner la transition des territoires.

Société Numérique met en œuvre **l’article 69 de la loi du 7 octobre 2016 pour une République Numérique**. La mission est en ainsi charge de l’élaboration d’une plateforme « Orientations nationales pour le développement des usages et des services numériques dans les territoires ».

L’objectif est de proposer une plateforme en ligne, opérationnelle et agrégeant l’expertise et ressources de l’Etat tout en facilitant et accélérant la mise en place d’actions concrètes à partir d’expériences réussies en matière de médiation comme de développement de services numériques de proximité.

Pour cela, une concertation nationale a été menée auprès des acteurs de terrain, afin notamment de **recenser les bonnes pratiques en matière de participation citoyenne numérique.** De nombreuses associations, collectivités territoriales, administrations ont contribué, aux côtés de dizaines de citoyens. 

Vous pouvez trouver l’intégralité des contributions sur ce sujet en suivant ce lien :
https://article69.agencedunumerique.gouv.fr/topic/588b532bc51fa31d5291fd4a

Retrouvez la synthèse de la concertation :
http://agencedunumerique.gouv.fr/wp-content/uploads/2017/06/Synthese_Article69.pdf

Des ateliers de travail ont suivi cette concertation afin de co-construire les contenus et ressources de la plateforme notamment en matière de participation citoyenne ; dont la synthèse est disponible ici : https://agencedunumerique.gouv.fr/wp-content/uploads/2016/10/CR-Ateliers-Paris-6-07-2017.pdf 

Le travail de co-élaboration de la plateforme se poursuit actuellement.

Société Numérique soutient également le [« chèque APTIC »](http://aptic.fr/) **comme outil efficace pour favoriser la montée en compétence numérique de la population.** Ce projet - conçu par un acteur aquitain partenaire de l’Agence, [Médias-Cité-](https://medias-cite.coop/) est accéléré par l’Agence du Numérique. L’objectif est de répondre aux besoins de formation des citoyens (particuliers, salariés, associations) en créant un lien entre les acteurs de la médiation numérique offrant des actions d’accompagnement (Espaces Publics Numériques, associations, bibliothèques, fablab,tiers lieux, etc.) et les financeurs de ces actions.  En savoir plus sur https://www.aptic.fr 

Enfin, Société Numérique participe à la structuration du secteur de la médiation numérique (regroupant l’ensemble des acteurs qui interviennent et participent à la montée en compétence numérique de la population française). La mission représente l’Etat au sein de la coopérative d’intérêt collectif La MedNum, qui regroupe aujourd’hui (elle a vocation à s’agrandir) une cinquantaine d’associations, start-ups et acteurs de l’innovation sociale engagés en faveur de la meilleure appropriation du numérique par tous : https://www.lamednum.coop

Score : 35 / Réponses : 2

----

Pour la création d’un Portail Citoyen - contribution (député des Yvelines)

L’une des grandes difficultés pratiques que connait tout député, et plus largement toute instance politique (conseil municipal, régional, gouvernement…), est de pouvoir joindre ses concitoyens, les informer de son action. Paradoxalement, les outils numériques comme le courriel, le site Internet ou les réseaux sociaux touchent un nombre finalement assez retreint de citoyens, le plus souvent les militants et supporters de l’élu ou du parti qu’il représente. La seule solution reste bien souvent l’envoi par la poste de lettres d’information ou encore le tractage, mais dont le coût est élevé et l’impact incertain.
A l’exception des députés des Français de l’étranger qui ont accès aux adresses courriel, seule une démarche proactive de la part du citoyen (inscription à la liste de diffusion ou abonnement aux réseaux sociaux du député) permet au député de communiquer utilement avec lui. Un très grand nombre de citoyens demeure ainsi à l’écart du flux d’information de leur député et plus largement des instances politiques qui le représente.
D’où l’idée de créer un « Portail Citoyen » qui pourrait naître d’une transformation ou évolution du portail Service-public.fr par l’élargissement de ses missions, avec un volet dédié aux services administratifs comme actuellement et un volet dédié aux élus. A l’instar d’un nombre croissant de domaines, lorsqu’un citoyen s’inscrit par exemple sur le site impots.gouv.fr pour payer ses impôts en ligne, la règle serait qu’il doive créer son compte sur le « Portail citoyen » et qu’il soit automatiquement, sauf avis express contraire (en décochant la case au moment de l’inscription ou plus tard), abonné aux fils d’information des instances politiques (député, conseil municipal et régional, gouvernement, etc.). Le député aurait ainsi l’essentiel des coordonnées courriel de ses concitoyens et pourrait véritablement dialoguer avec eux, co-construire les projets de la circonscription et pour un coût qui serait lui-même réduit.
Le « Portail Citoyen » pourrait aller plus loin et permettre, dans son volet politique, aux citoyens de s’abonner à certains documents (par exemple, recevoir automatiquement et avec notification la dernière délibération du conseil municipal), thématiques (tous les documents ou actualités officielles sur la santé ou les transports) ou encore certaines réunions (avec une fonction agenda de toutes les réunions politiques). Chaque citoyen pourrait de son compte personnel gérer ses différents abonnements et documents officiels.

De manière plus ascendante, le « Portail Citoyen » pourrait également servir de plate-forme de dialogue entre le citoyen et les instances politiques, de consultation et votation citoyenne, etc.

Score : 35 / Réponses : 6

----

**Les plateformes de consultation publique en ligne doivent être basées sur du logiciel libre**

« La qualité des outils logiciels – plateformes dédiées, messagerie, outils collaboratifs… – est déterminante pour l’exercice. […] Le logiciel doit, de préférence, être un logiciel libre dont le code soit accessible et puisse ainsi être audité par le public. Les choix méthodologiques de la consultation doivent être publics et doivent pouvoir être discutés. » Consultations ouvertes sur internet organisées par les administrations; un instrument précieux au service de la participation du public qui requiert une forte implication des organisateurs, [Novembre 2016, COEPIA](https://cdn2.nextinpact.com/medias/coepia_consultations_ouvertes_sur_internet_2016.pdf).

Seule l’utilisation de logiciel libre, intrinsèquement transparent et donc auditable par tous, peut offrir un niveau de confiance et d'impartialité suffisant en préservant contre toute possibilité de manipulation. L’outil conditionne l’usage ; « Le code est loi » disait Lawrence Lessig en 2001.

Lire en ce sens ce billet de Regards Citoyens : [Le numérique ne pourra aider la démocratie sans en adopter les fondements](https://www.regardscitoyens.org/civic-tech-ou-civic-business-le-numerique-ne-pourra-pas-aider-la-democratie-sans-en-adopter-les-fondements/).

Par ailleurs, le modèle de gestion horizontale des logiciels libres s’inscrit parfaitement dans l’esprit d’une démocratie plus participative et plus proche des citoyens.

Dans cette objectif , garantir la possibilité de partage, de réutilisation et de reproductibilité des logiciels de consultation publique en ligne semble particulièrement opportune. On pourra ainsi imaginer des initiatives citoyennes locales, ainsi que des collectivités, qui pourraient avoir accès à des outils puissants, de confiance, et bénéficiant d’un investissement mutualiser.

Score : 32 / Réponses : 2

----

Suite à une réunion d'environ 40 citoyens de Paris 13, voici notre contribution. La participation numérique et la participation présentielle sont vues comme complémentaires et non antinomiques.

Actions : 
-	Mise en place d’une plateforme numérique pour les remontées des consultations mieux diffusée et plus facilement accessible.
-	Communication des thématiques de travail sur un planning défini plusieurs mois à l’avance pour que les citoyens puissent avoir le temps d’apporter des participations. Ce planning doit comprendre : la thématique, les problématiques, les budgets affectés ou potentiels, le délai.
-	Définir des personnes qui seront porte-paroles au niveau de l’Assemblée Nationale des groupes de travail de citoyens ayant une expertise sur les thématiques évoquées. Ces personnes auront aussi un rôle de communication descendante : informer les citoyens des propositions conservées au niveau législatif.
-	Mettre en place des groupes de travail thématique avec un tirage au sort des citoyens pour permettre à des personnes non proches de la chose politique et publique de s’y intéresser
-	Revoir les canaux de communication et de formation à la citoyenneté

Score : 25 / Réponses : 2

----

•  Essayez sur les trois supports ( Médias, numériques et présentiels ), de créer une pédagogie du circuit parlementaire en appuyant sur l’élaboration de la loi et les amendements.

•  Créer une plateforme de dépôt d’amendements au sein des différents groupes parlementaires. Chaque amendement présenté ayant été suffisamment sollicité par des citoyens se devra d’être discuté en séance.

•  Consultation sur les grandes lois, notamment la loi de finance; avec une partie participative restreinte. Cette consultation peut être étendue mais ne doit pas déborder sur les lois sociétales (hystérisation du débat, trolling …)

    Néanmoins cette consultation pose le problème de l’échantillon représentatif, si des classes sociales participent plus que d’autres.

•  La possibilité pour des citoyens de poser des questions (triées par qui ? ) au gouvernement en séance (En direct, format vidéo ou rapporteur ? ) de façon à se sentir impliqué dans le débat.

Les points de la plateforme validés par l’atelier :

•  « Faire simple et compréhensible » - récolter la parole du terrain sur des sujets important la vie des citoyens
•  Assemblées locales, en proximité de leur lieux de vie, pour être informés des décisions publiques et questionner les fonctionnaires indépendamment des élus de l’exécutif
•  Aller vers les citoyens (être proactif) qui n’ont pas le temps ou pensent que leur avis ne compte pas
•  Devoir d’information des médias pour la promotion des consultations
•  Par voie de presse (notamment),  des annonces sur les prochains textes en discussion

Score : 24 / Réponses : 0

----

**Garantir la neutralité des outils numériques mis en œuvre par le Parlement**

À l'heure où le Parlement déploie des innovations démocratiques numériques en s'appuyant sur des acteurs de la société civile, il est important de rappeller que le numérique n'est pas nécessairement démocratique. Certains garde-fous démocratiques sont essentiels pour cela :
- l'usage et le développement de Logiciel Libre : en plus d'offrir une grande ouverture en autorisant à tout citoyen de contribuer en corrigeant ou complétant le logiciel, le logiciel libre expose les recettes indispensables de son bon fonctionnement pour permettre de comprendre les processus techniques appliqués ; en l'absence de cette exposition, vérifier la neutralité de ces processus est impossible ;
- la publication des données produites en Open Data, afin de ne pas déposséder les participants de leurs contributions et de les placer sur un pied d'égalité, tout en permettant l'émergence par la réutilisation d'analyses et interprétations complémentaires.

Sans ces critères, il est impossible de s'assurer de la neutralité des opérateurs. Cette absence de neutralité peut poser de réels problèmes de souveraineté lorsqu'ils ont un rôle de tiers de confiance, comme par exemple pour les consultations.

Enfin, le recours au Logiciel Libre et à l'Open Data pourraient permettre à certaines communautés du numériques de s'impliquer concrètement dans la vie parlementaire en proposant des améliorations des outils existants. Une nouvelle forme de participation entre la société civile et l'administration parlementaire pourrait ainsi voir le jour.

Score : 22 / Réponses : 0

----

**Ouvrir un tiers lieu pour permettre aux citoyens de passage à Paris d'avoir une autre relation au Parlement qu'une relation commerciale à travers la boutique**

À l'Assemblée, le seul lieu accessible à tout citoyen sans invitation est la boutique de l'Assemblée. S'il est important que les productions écrites et les objets littéraires ou marketing de valorisation du Parlement soient accessibles à tous, ce choix donne une vision très mercantile de l'institution aux citoyens qui n'ont pas pu ou pas su demander à leur parlementaire une visite de l'Assemblée.

En parallèle de son activité commerciale, l'Assemblée gagnerait à associer à la boutique un « Tiers lieu », qui accueillerait les citoyens pour leur proposer de découvrir, apprivoiser et embrasser la démocratie parlementaire et ses institutions via des méthodes de médiation pédagogique et civique qui pourraient mêler parlementaires, administrateurs et acteurs de la société civile. Si des initiatives individuelles de certains parlementaires ou de certains groupes politiques peuvent d'ores et déjà prendre cette direction, il semble indispensable de dépolitiser et dépersonnaliser un tel lieu pour concrétiser l'engagement citoyen en lui conférant une stature institutionnelle.

Score : 21 / Réponses : 3

----

S'appuyer sur les acteurs locaux, qui œuvrent depuis des années pour permettre à tout un chacun de s'approprier le numérique serait une solution. Les Espaces Publics Numériques et autres lieux de médiation numérique sont présents partout en France(http://annuaire.mediation-numerique.fr/annuaire.html). S'appuyer sur ces acteurs pour organiser des temps d'échanges qui pourraient être retranscrits par un médiateur permettrait une prise en compte de l'avis :
1- des personnes qui ne maitrisent pas suffisamment le numérique (ou l'écrit)
2- des personnes qui n'osent pas contribuer directement.
Bien animés, de tels temps d'échanges permettraient d'affiner les propositions et éviter d'éventuels doublons, et ainsi proposer des réponses plus construites et exploitables.
Une facilitation de l'organisation de tels temps d'échanges par le gouvernement (mise à disposition d'affiches, flyers, cartographie en ligne des évènements) assurerait une forme de reconnaissance de ces acteurs qui en ont bien besoin.

Score : 20 / Réponses : 3

----

**Encourager et faciliter l'accès physique aux débats de l'Assemblée en tribunes publiques**

Qu'il s'agisse d'Hadopi, du Grenelle de l'environnement, du Mariage pour tous ou encore des lois Travail, les grandes lois sociétales sont toujours l'occasion pour de nombreux citoyens de découvrir le Parlement et la procédure législative, d'apprendre à les comprendre et pour certains d'y trouver goût.

Il est possible pour les citoyens ayant la chance d'obtenir une invitation par leur député d'assister aux débats parlementaires. Des règles bien compréhensibles de discrétion et bienséance sont demandées aux citoyens présents dans les tribunes. En revanche, tous les appareils électroniques, y compris les outils de communication numérique (smartphone ou ordinateur), sont interdits. Or, ces outils sont devenus quasiment essentiels pour pouvoir bien consulter et comprendre les dispositions discutées, reconnaître les orateurs, ou partager ses impressions sur les débats à travers les réseaux sociaux. Alors même que les députés s’apprêtent à utiliser des tablettes numériques pour cesser d'imprimer des milliers de pages d'amendements, il semblerait contradictoire que les citoyens intéressés pour suivre les débats restent contraints pour leur part à imprimer ces liasses de papier.

De plus, alors que le calendrier parlementaire est très fluctuant, les citoyens doivent faire une demande au moins trois jours en avance pour obtenir le ticket donnant le droit à assister à une séance. Pour les personnes souhaitant suivre un débat particulier, il est donc quasiment impossible sans une expertise approfondie de l'organisation du Parlement d'assister physiquement aux débats, à plus forte raison encore lorsqu'elles ne résident pas en région parisienne.

Il convient donc de revoir les règles régissant l'accueil des citoyens souhaitant assister depuis les tribunes du public aux débats parlementaires afin de leur permettre de mieux découvrir et suivre les débats.

Enfin, les séances du soir restent du fait des horaires de travail classiques les plus accessibles à la fois en vidéo et physiquement. Supprimer ces séances comme cela a pu être évoqué causerait à ce titre un recul dans l'accessibilité et l'ouverture du débat parlementaire aux citoyens.

Score : 20 / Réponses : 2

----

**Participation numérique et participation présentielle**  

Pour la partie en ligne numérique, il faudrait bénéficier d'un **outil de débat de masse**.  

Ce système pourrait être un système de **question / réponse collaboratif** qui permettrait d’**afficher les réponses viables principales de manière synthétique** pour une question donnée avec la possibilité d’aller chercher les détails de cette réponse (justifications, sources, etc).  

L'objectif serait d'amener vers les **propositions les plus qualitatives possibles**, et ce notamment à travers une méthodologie d'accompagnement dans la démarche de participation.  

Nous pourrions également reporter sur cet outil les comptes-rendus des débats présentiels afin de pouvoir continuer le débat en ligne et éventuellement relancer d'autres débats présentiels via ces partages.  

C'est la réflexion que nous menons en développant l'outil Open Source ["Débattons"](https://docs.google.com/presentation/d/1UIsnLdP2XgO_Ii6g98lWW4FsMuDccD-TigsT5NSFKOU) (dont les [dernières infos sont dispos ici](https://www.linkedin.com/pulse/avanc%C3%A9es-sur-d%C3%A9battons-suite-au-hackaton-doctobre-anthony-ogier/)).

Score : 20 / Réponses : 1

----

Ceci est le résultat d'un atelier de inter comités de la LREM en Bretagne (Paimpont-Plélan, Mordelles, Goven, Beignon-Guer)

Propositions de notre groupe de travail sur la démocratie numérique et les nouvelles formes de participation citoyenne
Pour nous, une plateforme numérique avec des sondages apparaît une première piste de travail. Cependant, nous pensons que des questions ouvertes doivent être privilégies et, qu’il faut limiter autant que faire se peut, les réponses fermées. Cet outil devrait néanmoins être simple d’accès et d’utilisation pour toucher le plus grand nombre d’individus. 
Nous encourageons les consultations citoyennes qui permettent la liberté d’expression et intéresser le plus grand nombre de citoyens.
Nous nous interrogeons sur la manière de procéder afin ne pas exclure certaines catégories sociales et ainsi, obtenir une meilleure représentativité. (Ex suivi Audimat ou Médiamétrie)
D’autres formes nous apparaissent intéressantes à développer : 
•	La présence : 
Sur différents thèmes, lors de réunions publiques citoyennes par exemple, des ateliers pourraient être développés. 
Cela peut se traduire notamment au travers de l’actualité des lois, il nous faudra réfléchir à la façon de faire venir des citoyens autres que des adhérents de la majorité. 
Ainsi, nous pensons renouer avec la participation et l’implication de la population. 
•	Les sondages : 
Notre groupe évoque la possibilité de créer des équivalents de sondage dans la rue ou les centres commerciaux. Nous dynamiserions de cette façon les citoyens qui, se sentiraient écoutés. 
Cela pourrait se décliner sous forme de petit questionnaire en lien avec l’actualité (des petites marches)
•	Les canaux de diffusion : 
Via la radio ou les réseaux sociaux, nous pourrons toucher, sensibiliser et rentre attractif ces aspects pour le moment jugés incompréhensible pour tous. 
Notre idée concrète repose donc sur l’implication et, des citoyens et, des professionnels des secteurs concernés afin de permettre la co-construction des échanges et la diversité des acteurs. 
Cela serait éventuellement en lien avec les ateliers thématiques des départements par exemple

Score : 19 / Réponses : 0

----

En pensant à tous ceux qui disent :"de toute façon ils font ce qu'ils veulent" je voudrai réagir en proposant de consulter davantage les citoyens confrontés à de réelles difficultés quasi quotidienne. En effet j'ai toujours souhaité participer à ma manière au débat politique tel qu'il soit et en effet la révolution numérique en a laissé quelques uns de côté, mais ne serait-il pas bon de solliciter nos députés pour nous accorder des temps de parole avec des rapporteurs volontaires à l'aise avec le numérique, il peut s'organiser des réunions avec des citoyens dirigées par une personne responsable d'animer ce temps de parole pour recueillir les idées en réponse à leurs difficultés et pour sensibiliser les citoyens au débat démocratique. Trop peu de gens se sentent citoyens avec le rôle que cela implique dans notre société. A mon avis je pense qu'il faut agir par missions pour ceux qui souhaitent s'investir dans la participation numérique :"Acceptez-vous cette mission ?" et recueillir toutes les informations données pour les transmettre sous format numérique qui fait de réelles économies de temps et d'argent.

Score : 18 / Réponses : 1

----

* Contribution collective Café Citoyen : Présidée par Mme Nadia HAI Députée de la   11ème circonscription des Yvelines * 

**78-11Q5 La plateforme d'initiative citoyenne n'est qu'un outil, c'est le mode de construction de la Loi qui est l'enjeu à revoir.**

Voir l'ensemble des contributions 78-11 Q1 à 78-11 Q4.
L'enjeu et de **réussir à traiter, sélectionner, rationaliser un grand nombre d'idées**, de contributions ou de questions.
Le risque est de créer des fortes déceptions en cas de non réponse. **Expliquer les refus.**
Il faut en même temps **préserver la légitimité de nos représentants** en leur donnant un rôle **actif dans cette participation citoyenne** (voir notamment contribution 78-11-Q2).
Nouvelle mission officielle de nos députés d'**organiser cette concertation et participation citoyenne présentielle et numérique**.
**Rendre compte** par des synthèses et bilans du travail fourni par les citoyens, (analyse et indicateurs)
Prévoir un **budget participatif** national et local.
Créer une ou des **nouvelles commissions de citoyens** dans une ou plusieurs assemblées.
**Tirés au sort des citoyens** volontaires ou experts pour y participer.
Créer un **Compte Unique Citoyen** individuel sécurisé.
Sécuriser les votes en ligne pour éviter tout **piratage**.
Attention aux communautés actives et malveillantes expertes du net qui pourraient détourner les outils mis à disposition pour des desseins peu démocratiques.
Une **brigade policière de surveillance de la sincérité et la représentativité** des recueils citoyens dotés de moyens d'investigation et de contrôle doit être mise en place en simultané des projets de plateforme numérique, sinon on risque que le doute l'emporte sur l'initiative   dans le vécu citoyen.

Score : 17 / Réponses : 1

----

**Créer un "SAS Numérique", Service d'Accès Solidaire au Numérique**

La participation à la démocratie numérique implique que la majorité y ait accès sans ou avec le moins possible de discrimination. Or ce n'est pas le cas actuellement : fracture numérique, zones blanches, inégalités devant l'usage des outils numériques du quotidien et des démarches administratives en ligne, pour certaines catégories de populations, qu'il s'agisse de personnes en situation de précarité ou de générations entières n'ayant pas vécu dans la culture du numérique et qui en sont rejetées souvent par manque d'informations ou d'aide spécifique. 

D'où l'idée de créer une sorte de **Service d'Accès Solidaire au Numérique** qui représenterait un label regroupant un certain nombre d'actions et de dispositifs existants en les valorisant et en permettant de les répartir sur le territoire. Il s'agirait aussi de susciter un certains nombre d'idées et de vocations nouvelles sur ce vaste sujet.

Le **SAS Numérique** aurait  donc un double objectif, social et solidaire. Il pourrait prendre la forme d'une association nationale avec des antennes territoriales, mais commencerait par une expérience pilote locale; et pourquoi pas dans l'Hérault, mon département, qui regroupe cette diversité de lieux et de populations concernés. Si elle est concluante, l'expérience pourrait alors être étendue à d'autres territoires.

 

**Idées et dispositifs qui pourraient être mis en place**

- Développer les initiatives de **Bus numériques **comme [elles existent](http://www.agevillage.com/actualite-15505-1-Le-bus-numerique-vient-former-les-seniors-et-les-aidants.html), entre autres, en [Gironde](http://www.sas-svp.fr/bus-numerique-aquitaine/) et en [Côte d'or](https://www.cotedor.fr/cms/page6994.html), pour aller porter l'information et la connaissance dans les endroits ou à destination de populations qui en manquent ou qui n'ont parfois pas accès au numérique (certaines zones rurales ou semi-rurales, auprès des séniors, des aidants, ou encore des personnes en situation de handicap…).  

- Favoriser l'animation et l'implantation de clubs ou de formations aux outils numériques dans les municipalités (localités). Cela existe déjà dans certaines régions et communes, et ça devra être développé au maximum.  

- Permettre de fédérer et faire connaître les initiatives existantes sur l'accès au numérique ou au Cloud solidaire pour les personnes en situation de précarité. En particulier une belle initiative comme celle de [Reconnect](https://www.reconnect.fr/). Lire et écouter [le reportage sur le sujet](https://www.franceinter.fr/societe/le-numerique-qui-facilite-la-vie-des-sdf).  

- Favoriser l'implantation de bornes numériques dans les mairies (voir la proposition de [Celine Flament](http://consultation.democratie-numerique.assemblee-nationale.fr/topic/59d7325812406bdd5d6277d4#comment-59ef9704896da05921863977)).  

- Susciter la participation de citoyens de tous types - retraités, étudiants, lycéens - comme le montre la proposition d'[Isabelle Guyot](http://consultation.democratie-numerique.assemblee-nationale.fr/topic/59d7325812406bdd5d6277d4#comment-59ff8a7b82abf78a2231b9e5).  

- Monter une plate-forme téléphonique de réponse à des problématiques de base concernant l'usage des outils numériques, en soutien et accompagnement aux démarches de l'administration numérique. Cette plate-forme devra être bien encadrée car elle pourrait entraîner des coûts importants.  

 

Réaliser ces projets nécessitera des relais territoriaux, associatifs, municipaux, départementaux, régionaux.

 

C'est une contribution ouverte, écrite rapidement car j'ai découvert la plateforme il y a peu de temps : toutes les bonnes idées et remarques sont bonnes à prendre.

 

Le sujet le mérite, il me semble.

Score : 16 / Réponses : 3

----

La mission Société Numérique, pôle de l’Agence du Numérique, met en œuvre un programme d’action pour favoriser l’autonomie et la capacité de tous à saisir les opportunités du numérique et pour accompagner la transition des territoires.

Son Laboratoire d’Analyse et de Décryptage du Numérique propose des données et savoirs précis afin de renforcer l’information et la compréhension des usages numériques et orienter les politiques publiques.

Les enjeux de participation citoyenne numérique sont liés à ceux d’inclusion des publics éloignés des technologies numériques, souvent déjà exclus des dispositifs traditionnels de participation citoyenne. 

D’après le [Baromètre du Numérique (Agence du Numérique, CGE, Arcep)](https://laboratoire.agencedunumerique.gouv.fr/2016/11/10/barometre-du-numerique/), en 2016, **seul 60% des personnes ayant des bas revenus se déclaraient être compétents pour utiliser un ordinateur, contre 78% de hauts revenus**. De plus, lorsque l’on regarde la progression, on constate que ces inégalités ont tendance à s’accroître au fil des ans. 

De plus, 40% des français sont inquiets à l’idée de réaliser l’intégralité de leurs démarches administratives en ligne et 20% des français abonnés à internet (fixe ou mobile) ne sentent pas à l’aise pour l’utiliser.

L’Agence du Numérique co-finance également - avec l’Agence Nationale de la Recherche -  l’enquête Capacity, [une enquête pour confronter les promesses du pouvoir d’agir par les outils numériques à la réalité des usages et bénéfices tirés par les utilisateurs de ces outils.](https://laboratoire.agencedunumerique.gouv.fr/2017/03/23/premiers-resultats-capacity/)
 
Selon cette enquête ; « Quand on demande [aux internautes] **si Internet leur a donné des opportunités pour s’engager politiquement, 91% des internautes répondent par la négative. […] »**
 
Dans l’ensemble, les Français ne semblent pas voir dans Internet un moyen de participer davantage politiquement. Seul un quart est d’accord avec l’idée qu’Internet permet de mieux comprendre les questions politiques et seuls **29% avec le fait qu’Internet permet d’avoir un plus grand impact politique**. 

L’affirmation selon laquelle Internet permet de s’exprimer davantage sur les questions politiques remporte plus de suffrages : un peu plus d’un tiers des Français sont d’accord avec cette idée (20% plutôt d’accord et 15% tout à fait d’accord).

Score : 14 / Réponses : 1

----

Constat : 
La présence des citoyens dans le débat démocratique ne peut qu'être vivifiante dans un environnement politique qui se laisse facilement enfermer dans la gestion de problémes techniques ou des échéances électorales. Néanmoins, l'intervention individuelle du citoyen ou l'organisation spontanée des citoyens peuvent présenter des écueils : la méconnaissance du fonctionnement des institutions, des difficultés à comprendre le langage juridique, la méconnaissance des données macro, l'absence d'une vision globale de la société, un activisme de certains acteurs de la société civile qui emploient les mêmes méthodes de lobbying que des grands groupes, résultant en la confiscation de la parole des citoyens moins habitués à la prise de parole en public, une sur-représentation de certaines thématiques dans le débat publique, ...
Proposition : 
Distinguer deux types de débats démocratiques avec les citoyens : (1) le débat démocratique participatif et (2) le débat démocratique de négociation. Ce dernier se justifie (1) par le fait que les débats participatifs n'engagent pas les citoyens dans la faisabilité de leurs propositions; aussi, les propositions sont souvent théoriques et les discussions, chronophages et frustrantes, et (2) parce que dans les faits, le rôle uniquement participatif des citoyens présente peu d'enjeu dans les débats; aussi les acteurs de la mise en oeuvre des solutions (élus, sachants, opérateurs économiques,...) entrent peu en interaction avec eux pour résoudre les problèmes qu'ils soulèvent.
L'espace démocratique participatif est réservé à l'expression citoyenne spontanée sur une plateforme numérique nationale (i) qu'elle se manifeste individuellement ou (ii) qu'elle soit collective par le biais des pétitions et par le biais de conclusions issues des "café-débats citoyens". La plate-forme nationale peut être du type "Telabotanica" ou "Doctissimo". Elle doit cependant être animée  localement par les services déconcentrés de l'état et des réseaux de la société civile.
L'espace démocratique de négociation, nécessite l'intermédiation des collectivités et des institutions pour son organisation et son animation. 
Pour que la  présence des citoyens dans le débat soit opérante, il faut que leur nombre soit égal au  nombre d'élus dans les collectivités. Le corps des citoyens doit être composé par exemple de 2/3 de citoyens tirés au sort et de 1/3 de citoyens/société civile cooptés pour leurs expertises techniques.
Les citoyens participent aux travaux des collectivités (Communes, EPCI, Régions) en portant leur attention sur la pertinence et l'applicabilité des lois qui s'imposent aux collectivités et leurs administrés. Leur rôle est de soulever les problèmes, de comprendre les conditions nécessaires à leur résolution (cadre juridique, financier, ordre publique, ...) et de participer à la négociation qui permettra de résoudre les conflits d'intérêts. Si la partie négociation, en l'état du droit, se fait dans les collectivités, les propositions d'évolution des lois doivent se faire collectivement, c'est à dire avec l'ensemble du corps des citoyens, à l'échelle de la région.
Formation et information des citoyens : Il faudra préalablement des formations et des lieux dédiés à cette fin : les locaux vacants suite à la fusion des régions peuvent être mis à disposition pour des sessions de formations MOOC (hebdomadaires ou bi-mensuelles), (2) des ateliers d'élaboration de la loi en lien avec l'Assemblée Nationale et (3) des plennières avec les élus des différentes collectivités  et les députés (mensuelles ou trimestrielles).
L'animation de ces sessions de travail doit être dévolue au CESER rénové (déclinaisons régionale du CNESE) dès lors que le CESER joue son rôle de force de proposition et non de force politique.
De manière général tous les citoyens doivent avoir accès au big data de la collectivité qui les intéresse. Ce ne sera pas une duplication de l'Insee, mais davatage une ressource qualitative ou les indicateurs sont co-construits avec les citoyens. L'Open data est accessible par une simple application smartphone

Score : 12 / Réponses : 1

----

Comme nous en avons débattu hier soir à Mauguio, un élément important pour que les citoyens s'impliquent dans l'oeuvre législative repose sur la compréhension qu'ils peuvent avoir des lois en les lisant.
Pour cela il faut absolument modifier l'approche actuelle du législateur dans au moins deux domaines :
- les lois doivent être codifiées, si possible dès leur vote, et s'insérer harmonieusement dans le corpus législatif existant. Pour les lois qui ne sont pas (encore) codifiées, il est impératif d'accélérer les travaux de la commission de codification, qui fait ce travail à droit constant depuis plus de 20 ans mais n'arrive pas à suivre le rythme des réformes législatives. Peut-être en renforçant ses moyens humains et/ou son budget ?
- la loi doit être rédigée en langage clair et non plus en usant et abusant de références à d'autres textes : Par exemple on ne doit plus voter de lois qui incriminent les faits prévus à l'article 4 et les répriment des peines prévues à l'article 12 si ils sont commis avec une des circonstances visées à l'article 15..
(Rappelons qu'en matière pénale, en outre, nul n'est censé ignorer la loi..)
Il faut et il suffit de rédiger les nouveaux textes comme suit : l'article x du code y est remplacé par les dispositions suivantes : "celui qui commet tel acte dans telle circonstances est puni de telle peine" etc.;
Enfin pour que la connaissance de la loi soit utile au citoyen, encore faut-il qu'elle soit applicable. Or nombre de directives européennes sont d'application directe dans notre droit et entrent en conflit avec les lois françaises qui n'ont pas été adaptées. Les juristes les plus "pointus" savent les manier mais tel n'est pas le cas, évidemment, des citoyens. 
Il serait donc important d'accélérer le travail de transposition en droit français des directives européennes qui s'imposent, afin d'appliquer directement la loi française.

Score : 11 / Réponses : 0

----

Le numérique est une révolution,devrait permettre une libre expression à tout acteur de ce pays, de partager, d'exprimer une opinion , un questionnement,sur les possibilités de l'amélioration de sa condition de vie.
Mais comment pourrait-elle remonter jusqu'au législateur?
Nous savons que chaque région,par son emplacement géographique peut-être amené à définir une vision économique différente.
Il se trouve des problèmes locaux, et aussi des problèmes communs qui regroupent l'ensemble des régions.
Comment la population pourrait exprimer par ordre de priorités, les problèmes rencontrés au quotidien?
En exemple :l'installation de panneaux numériques digitales,à proximité des mairies.
Un panneau numérique tactile sur lequel apparaîtrait la formulation de différentes questions liés aux citoyens de la ville. ( Chaque citoyen  bénéficierait d'un code unique et personnel, pour le vote aux réponses des différentes questions).
Ex: Trouvez vous que les pistes cyclables ne sont pas suffisamment aménagés..
Ex: Subissez vous des nuisances sonores dans votre quartier.

Ainsi chaque citoyen peut répondre à certains problèmes rencontrés localement.

Cet exemple d'intégration de système peut-être aménagé de manière différente, sur des questions du gouvernement.

Score : 11 / Réponses : 0

----

MAINTENIR UNE REPRÉSENTATION INSTITUTIONNELLE

La démocratie ne peut fonctionner autrement que par le média d’une représentation des citoyens, au sein d’institutions politiques. Le numérique ne doit pas les remettre en cause, au risque de faire vaciller la démocratie elle-même.

Des plateformes numériques doivent être créées (http://consultation.democratie-numerique.assemblee-nationale.fr/topic/59d7335412406bdd5d6277e4#comment-59f7311ee8a9656c1a8e73d7), pour suivre et contribuer à l’élaboration de la loi, depuis le stade gouvernemental jusqu’à son application. Cela ne doit cependant pas remettre en cause le rôle du Gouvernement et du Parlement dans la procédure législative.

Pour cela, la loi doit déterminer les conditions dans lesquelles ces plateformes doivent et peuvent être créées, ainsi que les règles afférentes aux conséquences d’une participation citoyenne. Le principe fondamental étant que, à l’instar de toute démocratie, la décision revienne soit aux représentants élus, soit au peuple lui-même, dans son ensemble. Les citoyens, isolément ou en groupe, ne doivent pouvoir que l’influencer.

Autres propositions dans le cadre de cette consultation : http://constitutiondecodee.blog.lemonde.fr/2017/10/30/participation-numerique/

Score : 10 / Réponses : 3

----

Pourquoi ne pas créer un corps d'agents publics itinérants "Multi-cartes" dans les territoires victimes de la "fracture numérique". A l'heure du numérique justement, il n'est plus nécessaire de se déplacer avec des tonnes de documents, dossiers... Un simple ordinateur suffit la plupart du temps (avec une imprimante peut-être). 
Un agent public "multi-cartes" serait un intermédiaire pour:
- La participation des citoyens à l'élaboration de la loi et/ou de la participation aux débats publics.
- Les services préfectoraux numérisés (demande de permis, carte grises etc etc...)
- Les services d'état civil

Les agents auraient des "permanences" dans toutes les petites communes et seraient alors un lien entre les territoires aussi bien ruraux que urbains (certaines zones urbaines sont aussi "enclavées" mais pour d'autres raisons).
L'idée est de rassembler tous les services pour éviter un corps pléthorique et évidemment très dispendieux pour les finances publiques. Cela aurait aussi pour conséquence de libérer les agents des mairies de certaines tâches à l'heure où les dotations sont en question pour les collectivités territoriales. Cela permettraient donc à ces agents de se recentrer sur les problématiques locales.
Et donc pour le cas qui nous occupe se sera l'Assemblée Nationale qui ira vers les gens et non pas le contraire.

Score : 9 / Réponses : 8

----

Avant de parler de plateforme numérique, je pense que le lien des citoyens avec le parlement doit passer par ces députés ou sénateurs. Il serait intéressant de former ces élus (ou collaborateurs) à la concertation, le recueil d'avis (et pourquoi pas à la mise en place de plateforme numérique) pour les inciter à animer et mieux informer localement leurs citoyens. 
C'est notamment une des idées qui est remonté de ce travail en région centre val de Loire https://www.democratie-permanente.fr/ .

Score : 8 / Réponses : 0

----

Super idée d avoir prolongé la consultation de 48h. J' en profite pour faire une proposition.
L' articulation entre « participation numérique » et « participation présentielle » est important pour la réussite du projet.
Je suggère donc d'expérimenter la participation citoyenne directe et les articulations possible entre le numérique et le présentiel dès maintenant, dans la suite du processus de réforme mené par le groupe de travail des députés. 
La participation numérique se ferait par le biais de cette plate-forme et la participation présentielle au travers de relais territoriaux.
Dans un premier temps nous pouvons identifier quelques sites pilote animés par des volontaires.
Dans ces sites, les travaux du groupe de travail pourraient être suivis en temps réel (vidéo conférence) ou en différé (replay). Les retours de ces réunions citoyennes seraient remontés au groupe de travail de l AN.

Score : 7 / Réponses : 0

----

**Etre proactif**
Afin d'inclure le maximum de personnes, il ne suffit pas de mettre à disposition des outils de discussions ou de pétitions mais il faut également aller vers ceux qui n'ont pas le temps ou qui ne connaissent pas les mécanismes. Vers ceux qui pensent que leur avis ne compte pas.
Plusieurs possibilités existent : micro trottoirs, tirage au sort (avec un mécanisme de compensation comme aux assises) 
Il faut en tout cas s'assurer que le panel de personnes soit réellement représentatif.

Score : 6 / Réponses : 0

----

**Rendre la démocratie numérique souhaitable, faisable, soutenable.**

A- Rendre la démocratie numérique souhaitable:
A-1- Tirer les bénéfices des premières expériences:
Ce que l'on peut tirer des premières expériences conduites, et notamment de la concertation pour la loi pour une République Numérique, c'est que:
- ont contribué majoritairement des acteurs concernés plus que des citoyens épars, plutôt des habitués à la prise de parole qui ont donc trouvé là un canal supplémentaire, que des personnes rares dans le débat public qui auraient trouvé là l'occasion de contribuer.
- ont été apportés des éléments qui relevaient globalement de ce que pouvait être une feuille de route de l'action publique, sans que soit nécessairement identifié:
-- ce qui relevait de la politique publique, de la loi, du décret, voire de la circulaire.
-- ce qui relevait de l'échelon de l'Europe, de l'État et des collectivités
-- ce qui relevait des compétences de l'État régulateur et de l'État financeur.
- ont été peu partagés les critères d'arbitrages, de décisions, et de ré-orientations et de suivi des contributions. Ce qui ne signifie pas qu'il n'y en a pas eu.
- ont été peu tracés les process depuis la concertation jusqu'aux décrets d'application.
Il convient enfin de noter que:
- le sujet correspondait au medium : comment rendre possible la démocratie numérique sur des sujets qui ne traitent pas du numérique ?
- la plateforme n'était pas opensource : comment garantir la loyauté des algorithmes et l'usage des données et des identités ?

A-2- Garantir la controverse plutôt que l'affrontement des opinions:
En creux se joue un cap assez déterminant dans notre exercice démocratique. Entre notre modèle républicain et les modèles nord-américains. Ces derniers fonctionnent davantage par l'agrégation d'un grand nombre de personne (les communautés) et/ou d'un haut-niveau d'influence (les lobbys). La décision collective se fait par les rapports de forces entres les blocs qui se constituent sur la plupart des questions. Par opposition, notre modèle, pétrit d'une culture républicaine (au sens res publica), voire d'éducation populaire, prétend placer la "chose publique" comme quelque chose qui devrait transcender les intérêts particuliers. Il en résulte les concepts d'intérêt général, d'intérêt collectif. Avec des décisions qui parfois ne sont pas celles des groupes les plus influents mais de l'idée d'une forme de dépassement de chacun au nom d'un intérêt qui nous dépasse un peu et sur lequel on s'écharpe régulièrement.
Or, dans le même temps les technologies sont pour l'heure essentiellement issues de la culture nord-américaine. Elles sont structurées, par leurs lexiques, leurs codes et leurs algorithmes pour produire deux choses garantissant la fidélisation de leurs communautés:
- chaque usager se voit et se renforce dans ses avis plus qu'il n'élargit sa représentation du monde: les bulles de filtres
- ces technologies sont propices à créer des avis dominants : les topics trends
Il en résulte un risque majeur pour la démocratie : celui de la tyrannie de la majorité. (« Mais la majorité elle-même n’est pas toute-puissante. Au-dessus d’elle, dans le monde moral, se trouvent l’humanité, la justice et la raison ; dans le monde politique, les droits acquis. La majorité reconnaît ces deux barrières, et s’il lui arrive de les franchir, c’est qu’elle a des passions, comme chaque homme, et que, semblable à eux, elle peut faire le mal en discernant le bien. » — Tocqueville)
Il en résulte que:
- sans des plateformes numériques développées pour nos approches démocratiques, la démocratie numérique risque de s'apparenter à une démocratie d'opinions: il faut donc contribuer au développement de civic tech basées sur nos codes que d'importer les codes nord-américains en même temps qu'on importe leurs technologies.
- sans des stratégies de nature à "ré-enchanter" nos codes culturels et politiques, les habitants se comporteront d'avantage en usager qu'en citoyen: il faut donc engager de réelles stratégies d'appui à l'émergence des pratiques démocraties à l'ère numérique.

Score : 6 / Réponses : 6

----

Sur les outils de participation quelques sources intéressantes :

- un document incontournable de la FNH sur les outils de participation citoyenne : http://www.fondation-nature-homme.org/magazine/democratie-participative-guide-des-outils-pour-agir/
- un état des lieux d’expériences étrangères de processus constituant participatifs dans les annexes ici : http://www.fondation-nature-homme.org/magazine/osons-le-big-bang-democratique/
-  un guide méthodologique sur le dialogue avec les parties prenantes du comité 21 : http://www.comite21.org/le-projet-dialogue-parties-prenantes.html

Et pour une vision globale des collectifs citoyens travaillant sur la démocratie , un état des lieux publié dans un blog du Monde il y a un an : http://internetactu.blog.lemonde.fr/2016/07/02/quels-enjeux-pour-les-innovations-democratiques/

Score : 5 / Réponses : 0

----

Pour toute consultation numérique ou sondage, à l'initiative d'un mouvement politique (au moins celui au gouvernement), l'Etat s'engage à : 

- Créer une application ou site internet dédié et sécurisé, favorisant les échanges, et
- Transmettre les résultats à l'ensemble des Français, non en données individuelles pour respecter la confidentialité des données (CNIL) mais en données agrégées, à la plus petite échelle possible (commune/arrondissement).

Ces données ne devraient jamais être traitées par des organisations privées, ni par un parti quelqu'il soit, mais uniquement par un organisme public indépendant (du style Cour des Comptes)

Score : 5 / Réponses : 0

----

1) La nécessité d'une éducation et accessibilité aux outils numériques à rajouter dans les programmes scolaires généraux, notamment en primaire.
2) Identification et traçabilité des outils numériques : création d'un profil ou avatar virtuel pour tous citoyens (qui serait certifié par carte électorale ou d'identité, pour éviter la revente des données et protéger les citoyens).
3) Créer deux plateformes distinctes : une pour les discussions politiques traçables, et la seconde pour les votes des projets de lois.
4) Laisser une place au débat et aux relations humaines en présentiel, car indispensable, et complémentaire au numérique.

Score : 5 / Réponses : 1

----

Il serait intéressant de travailler sur un produit intuitif et accessible à tous ce qui n'est pas le cas, il me semble, avec  cette consultation numérique... Nous n'avons pas le temps de passer des heures ne serait ce pour comprendre le fonctionnement de cette consultation surtout quand on apprend son existence deux jours avant sa clôture!!!

Score : 5 / Réponses : 0

----

La diminution envisagée du nombre des député(e)s permettrait de faire des économies substantielles. Une partie de ces économies pourrait être attribuée aux député(e)s sous forme d'une enveloppe financière dont l'utilisation serait exclusivement destinée à employer 1 ou 2 collaborateurs(trices) supplémentaires qui seraient chargés de recevoir, analyser, synthétiser et transmettre à l'AN sous la signature de chaque député(e), les propositions des citoyens ayant émis des contributions sur des propositions et des projets de lois. Ces contributions pourraient être faites par voie dématérialisée ou par des courriers postaux, ce qui permettrait à tout citoyen à l'aise ou non avec le numérique de s'exprimer sans qu'aucun mode de participation ne soit privilégié. Pour garantir la rigueur et l'impartialité des consultations, toutes les contributions seraient stockées dans un cloud dédié sur la plateforme numérique de l'AN, répertoriées dans des espaces identifiés pour chaque député(e) afin de pouvoir être contrôlés par les services de l'AN.

Score : 5 / Réponses : 0

----

Là présence physique est à privilégier car favorise les échanges et la construction plurielle et complexe d une décision. A la suite de quoi un vote peut être proposé en numérique pour élargir la consultation à des citoyens n’en pouvant pas se déplacer.

Score : 4 / Réponses : 1

----

Je pense qu'une des urgences aujourd'hui est pour les citoyens de comprendre le travail parlementaire de nos députés. Le site https://www.nosdeputes.fr/ ne répertorie que la présence dans l'hémicycle. Hors le travail parlementaire ne se réduit pas qu'à l'Assemblée Nationale. La seule opportunité pour le citoyen est d'envoyer un mail aux députés mais je ne suis pas certain que cela soit la meilleure des solutions. Il serait intéressant d'ailleurs de savoir si les citoyens sont au courant qu'ils peuvent interpeller leurs députés assez facilement au final. Pourquoi ne pas faire en sorte de publier les agendas de chaque députés sur ce site? Cela permettrait à chaque citoyens de savoir quand son député est dans sa circonscription.

De plus, il manque clairement d’interaction entre les députés/ministres et des questions des citoyens. Ne serait-il pas pertinent de permettre lors des questions aux gouvernements/compte-rendu hebdomadaire du Conseil des ministres à une question citoyenne de pouvoir être poser. Cela peut paraître compliquer à mettre en place mais pas impossible grâce justement au numérique. Cela permettrait peut être d'attirer des citoyens loin de ce travail parlementaire aujourd'hui (je pense particulièrement aux jeunes).

Score : 4 / Réponses : 1

----

Un point fondamental pour le bon fonctionnement démocratique participatif est la propension à susciter l'intérêt de chacun pour les affaires publiques. 
Il faut redonner le goût de devenir un citoyen impliqué par des informations simples, compréhensibles par tous et d’une bonne dose de pédagogie. 

Ma proposition est que l’organisation et la participation à des échanges réguliers et planifiés avec les citoyens entre dans le périmètre des obligations des Elus. 

Les personnalités les plus à même d’avoir une résonance auprès de la population sont bien sûr les Elus. Ils ont été élus par le peuple pour les représenter. 
Les Elus se doivent d’avoir en retour un engagement de communication envers les citoyens (et pas seulement au moment des élections !).

Les Députés sont bien sûr les partenaires naturellement pour transmettre l’information, partager, expliquer mais aussi faire remonter les idées, les besoins, les ressenties des citoyens. 
Un certain nombre d’Elus appliquent déjà ces bonnes pratiques démocratiques mais pas tous ! 

Plusieurs pistes sont à développer pour permettre ces interactions.

Les outils numériques via des plateformes en ligne, des visioconférences, live streaming, Crowdsourcing permettent un échange ouvert à tous sans limite ni contrainte de temps. Les spécialises du numérique au travers de regroupement, de fablab, travaillent au développement d’outils numériques participatifs.

Cependant, ce type d’outils en ligne d’adresse plutôt à un public jeune, déjà enclin à ce type de communication participative. 

La fracture numérique est un facteur déterminant et un frein à la participation numérique, qu’elle soit territoriale ou fonctionnelle. Une tranche importante de la population est donc exclue de participation potentielle. 

C’est pour toutes ces raisons qu’il faut combiner les deux types d’actions Numérique et Présentiel afin de favoriser les échanges avec toutes les catégories de citoyens.

« DEBATTRE DES IDEES PROPOSEES EN LIGNE AUX EVENEMENT HORS LIGNE ET VICE VERSA »

Au travers du maillage territorial, l’organisation de réunions physiques diverses, conférences, débats, réunions publiques, cafés citoyens… permettent aux Elus d’informer, d’expliquer, d’échanger. 
Les Elus au sein des territoires restent au contact de la population et retrouvent plus de légitimité.
Les citoyens via ces rapports directs, simples et réguliers avec leurs Elus peuvent partager des idées, des préoccupations, contribuer à l’action collective et avoir le sentiment d’appartenance à la communauté.

Cette proposition, je tendrai à la généraliser à l’ensemble des Elus territoriaux.
Déjà au sein des communes, le besoin se fait sentir pour les citoyens d’avoir des échanges plus directs et réguliers avec les Elus. Ce sentiment est exacerbé lorsque l’on parle d’Elus de communautés de Communes, de Départements et Régions qui sont trop souvent inconnus de la population. 

Pour faire évoluer les mentalités, redonner un sens civique aux Citoyens, les Elus se doivent de redescendre communiquer en simple Citoyen parmi les autres Citoyens.

Score : 4 / Réponses : 1

----

Donner un rôle supplémentaire au député pour l'organisation de débats et consultations dans les lieux physiques et réels, sur le territoire. Cependant, ce rôle ne pourra être donner sans moyen et si le statut du député n'est pas clairement définit avec des devoirs obligatoires et un contre pouvoir citoyen de destitution.

Score : 3 / Réponses : 4

----

Pour assurer une bonne représentativité des innovations exposées dans les différents domaines, l'Assemblée nationale ne pourrait faire l'économie de la création d'un "lien" humain. Cela pourrait passer par un déplacement en son sein des participants ou l'organisation dans les grandes villes de séance de visioconférence avec les élus et experts à l'Assemblée.

Score : 3 / Réponses : 0

----

Dans le rapport e-inclusion remis à Fleur Pellerin par le Conseil National du Numérique, il est préconisé de faire le litteratie pour tous, le socle d'une société inclusive. 
Emmanuel Macron, en déplacement en Estonie pour un sommet sur le numérique", a assuré qu'il fallait "réduire la fracture numérique" en Europe, pour éviter de "nourrir les populismes et les extrêmes"." C'est aux pouvoirs publics de s'en charger", a-t-il déclaré.
Aussi il est indispensable de créer le lien entre cette concertation et le document cadre issu de la concertation #territoirenumérique.

Score : 3 / Réponses : 1

----

J’écoute, j’entends, et j’observe autour de moi beaucoup d’interrogations sur la façon de conduire la politique aujourd’hui le chemin sera long à parcourir pour regagner la confiance des citoyens. 
Alors, si l’on s’inspirait de la palabre Africaine ! 
Bien sur profitons des moyens du 21è siècle, pour que chacun, jeunes, moins jeunes, expérimentés ou non,  puissent apporter sa pierre à l’édifice, on ouvre des espaces de paroles aux citoyens pour enfin essayer d’obtenir un consensus sur les grands enjeux sociétaux.
Il faut faire simple et compréhensible : l’idée n’est pas de se substituer au politique, mais d’apporter la parole du terrain sur des sujets impactant leur vie.
Les municipalités riches de leur tissu associatif, ont développé des maisons de la vie associative bien connues de tous les bénévoles associatifs et des citoyens de la ville.
Tout en respectant le cadre juridique d’utilisation ne pourrait-on imaginer mettre toute cette force de communication et de rencontres pour réunir ateliers numériques ou visio-conférences et permettre la libre expression du plus grand nombre au-delà du débat partisan. 
Un référent des citoyens, serait désigné il pourrait alors conduire les échanges sur les sujets à débattre ou proposer.
Enfin, finaliser un projet synthétique de ces échanges pour le remonter au niveau national.

Score : 3 / Réponses : 1

----

Consultation citoyenne (20 personnes) au Lion d'Angers
- Prévoir un tirage au sort sur les listes électorales pour inviter des citoyens à consultation en présentielle.
- Limiter le nombre de caractères des réponses pour les consultations en ligne pour faciliter le traitement.
- Créer des points d'accès à internet dans les collectivités pour permettre à un maximum de citoyens de participer numériquement.
- Programmer des rencontres présentielles à des dates et horaires réguliers.
- Renforcer l'éducation sur le fonctionnement des institutions à l'école pour habiter les futurs citoyens à participer
- Communiquer de manière plus succincte et accessible sur les possibilités de participation des citoyens dans l'élaboration et l'évaluation de la loi.

Score : 3 / Réponses : 0

----

Comment articuler participation numérique et autres formes de participation? 

Le numérique exclut de facto une partie de la population : fracture numérique dans les usages, dans l’accès au haut débit, zones blanches...

On pourrait par exemple penser à la combinaison, à la fois :
- D’une plateforme internet comme celle sur laquelle nous répondons actuellement,
- Des formules plus courtes, en mode sondage, voir même avec une seule question,
- Des soirée, matinée, après-midi réponses… pas uniquement à PARIS, par exemple dans les mairies ou les associations locales, auprès d’un public manipulant mal internet. Elles sont alors organisées par des agents de la mairie ou des médiateurs volontaires.
- Des ateliers enfants,
- Des ateliers dans les collèges et les lycées, assurés par exemple par des médiateurs volontaires.
- Des stands avec goodies… écologiques : un petit cadeau pour ceux qui ont répondu à la consultation.

Lorsque j’évoque des médiateurs volontaires, je pense par exemple à :
- Des retraités, 
- Des personnes sans emploi, 
- Des personnes en service civique,
- Des étudiants,
- D’autres volontaires issus d’associations, de partis politiques…
Le volontariat doit être gratifié d’une manière ou d’une autre (par exemple : indemnité symbolique, livre offert, invitation à l’Assemblée nationale…) et les frais doivent être prise en charge (transport, hébergement éventuel et restauration). Le volontariat doit aussi être précédé d’une petite formation (par exemple en ligne) et donner lieu à la remise d’un certificat.
Le volontariat me parait une solution plus créative que le recours à des salariés. Il me semble que sur la durée, des volontaires sollicités ponctuellement resteront plus motivés et créatifs. De même, je pense que le recours à des volontaires et/ou à des vacataires pour l’élaboration des “kits” de consultation serait un gage de créativité et de renouvellement des pratiques dans la durée.

Une plateforme de mise en relation pourrait fédérer les volontaires et les demandeurs d’atelier de consultation (mairies, groupes de citoyens, écoles, associations de quartier…), dans des zones géographiques définies.

Score : 3 / Réponses : 5

----

A mon sens, la participation présentielle n'a pas lieu d'être envisagée concernant le citoyen lambda souhaitant apporter sa pierre à l'édifice législatif, pour la simple raison que cela implique des frais de déplacement et de logement que tous ne peuvent pas supporter, sauf à imaginer que l'Assemblée les prenne en charge, mais les parlementaires ont déjà tout cela de pris en charge. Toutefois, hormis le numérique, il pourrait être rendu possible à chacun de faire connaître son opinion sur tel ou tel aspect par courrier à l'attention du service de l'Assemblée compétent pour traiter de la question. 
Autrement, il conviendrait d'obliger les députés à tenir des réunions dans leurs circonscriptions afin de présenter aux citoyens les projets à venir et de recueillir leurs observations, étant entendu qu'elles devront être fidèlement rapportées à l'Assemblée.
Oui, les députés sont tenus de rendre des comptes aux électeurs de leur circonscription me dira-t-on, mais en pratique, cela ne se fait que très rarement. Pourquoi donc ne pas les y contraindre? Le citoyen serait alors considéré et aura eu l'opportunité de s'exprimer dans un sens comme dans un autre.
Vive la France, vive la liberté d'expression!

Score : 2 / Réponses : 0

----

La problématique de la participation est clairement celle qui pose le plus de problèmes dès qu'on parle de démocratie numérique. Des ateliers présentiels sont nécessaires mais ne suffiront pas. Il me semble que c'est principalement une question de générations, et que progressivement les citoyens participeront davantage sur internet car les modes de socialisation ont changé. De même, un jour (et même en principe bientôt) toute la France sera recouverte de la fibre. 

A mon sens le problème le plus important est la fracture sociale. On parle de processus de participation sur Internet qui sont en général écrits, où on valorise certains commentaires que l'on perçoit comme "objectifs", etc. C'est pourquoi nous ne sommes pas parvenus au moment de la généralisation de ces dispositifs et qu'il faut continuer à expérimenter et créer les outils les plus inclusifs possibles. Les députés, en tant que Représentants de toute la Nation (même si la participation est tellement faible qu'ils sont affaiblis) doivent justement combler cet écart et parler également pour ceux qui se taisent.

Score : 2 / Réponses : 1

----

Une borne numérique de consultation citoyenne en mairie avec appui des employés de mairie pour les personnes réticentes à ces nouvelles technologies.  Cette borne permettrait des votes, les démarches administratives en ligne etc

Score : 2 / Réponses : 1

----

Comme proposé dans de nombreux commentaire, créer un site/page/autre sur internet permettant de recueillir l'avis de tous.
Permettre aussi aux personnes directement concerné par les débats de siéger sur des sièges d'honneur à l'assemblé et de participer aux débats concrets.

Score : 2 / Réponses : 0

----

La co-construction de la loi avec les citoyens ne sera possible que si elle est accompagnée. Cela suppose la mise en oeuvre de moyens humains, logistiques et financiers pour que les citoyens puissent venir participer à des dynamiques de participation qui les concernent directement.

C’est pourquoi nous soutenons la mise en place d’un bureau ouvert à l’Assemblée Nationale. Un lieu où les citoyens seraient les bienvenus lors d’ateliers, de hackathons ou de conférences sur la manière de faire participer les citoyens à l’élaboration de la loi. Un exemple existe actuellement au Brésil. Il nous semble important d’offrir un cadre institutionnel à ces rendez-vous de co-construction pour assurer leur fréquence et l’aboutissement des projets qu’ils verront naître.

Score : 2 / Réponses : 0

----

L'Assemblée nationale doit être choisie à la proportionelle, du moins en partie.
Les scores de la présidentielle sont très différents de la configuration de l'Assemblée. 
Le système de vote actuelle fait que certaines voix ne sont pas pris en compte : par exemple, les circonscription de Haute-Savoie votent majoritairement à droite. Les voix de gauche, disons 30%, ne seront jamais représenté à l'Assemblée.
De plus, cette Assemblée est nationale, et doit donc être votée de façon nationale.

Score : 2 / Réponses : 0

----

Une opportunité se trouve dans l’utilisation des espaces de coworking  disponibles au sein de chaque circonscription. En effet, l’on pourrait organiser la présence physique des députés au sein de l’un des espaces de coworking de leur circonscription, au même moment : un rendez-vous mensuel organisé dans ce type de lieu de travail partagé hyper-connecté pourrait permettre de créer une dynamique d’écoute et de dialogue autour des travaux d’actualité à l’Assemblée Nationale, tout en permettant aux français de partir à la découverte de ces nouveaux lieux de lien social qui participent à lutter contre la fracture numérique, à laquelle doivent faire face trop de nos concitoyens.

Score : 2 / Réponses : 0

----

En Allemagne, le Bundestag dispose d'un camion qui permet :
- De distribuer de la documentation,
- D'organiser de petits rassemblements,
- De faire voyager des expositions...
Un camion de ce type pourrait permettre de recueillir les participations aux consultations. Ce pourrait être aussi le moyen d'apprendre à se servir des outils numériques de consultation pour être autonome la fois suivante.
 https://www.bundestag.de/besuche/ausstellungen/bundestagunterwegs/infomobil

Score : 2 / Réponses : 0

----

Il ne faut pas se contenter de la participation numérique car beaucoup de citoyens n'ont pas encore accès à Internet. Les députés devrait avoir un rôle puisqu'ils ont été élus pour représenter leurs concitoyens, ils devraient avoir des permanences mensuelles dans chaque grande ville de leur département pour que les citoyens puissent les interpeller (en général, on ne les voient jamais dans les petits villages !), mettre en place un système de "boîte à idée" à leurs permanence où chacun pourrait mettre son idée de façon anonyme, et le député se devra de la relever chaque semaine ou mois.....Mais surtout faire un vrai travail : lire toutes les propositions, en faire une synthèse et les faire remonter jusqu'au parlement. Pour s'assurer de l'impartialité et de la rigueur il serait souhaitable de faire appel à un regard externe ou plusieurs personnes dont un citoyen qui n'a pas contribué (3 pas plus)

Score : 1 / Réponses : 0

----

Une telle consultation, restée relativement confidentielle, avait été organisée par le Gouvernement en 2015. On reste pantois en relisant les réponses négatives apportées à certaines suggestions intelligentes. Au final, il n'en était rien sorti. 
Espérons que cette consultation soit un peu plus "sérieuse". Pour l'instant, on a un peu de mal à évaluer la motivation du Gouvernement.

Score : 1 / Réponses : 0

----

0/// commentaires sur l'outil ///                                         
1/// permettre d'identifier si les " - " (sur ce thème comme sur d'autres) sont proposés (a) en tant que critique ou réserve à l'encontre d'une proposition ou bien (b) comme moyen de creuser l'écart avec une proposition privilégiée (voire personnelle). ///
                       
2/// permettre à l'outil d'identifier et signaler - par lui-même - lorsqu'une idée similaire a déjà été exprimée. ///

Score : 1 / Réponses : 0

----

Bonjour, tous les députés devraient créer un projet de démocratie participative avec la possibilité pour tous leurs administrés de proposer, suggérer, discuter, échanger. 
Combien de personnes savent comment contacter leurs députés ? Un annuaire est indispensable (en dehors du site web de l'AN)

Score : 1 / Réponses : 0

----

La plupart des métiers du numérique de demain, d'ici 15 ou 20 ans ne sont toujours pas inventés. Sans entrer dans la pure anticipation voire la science-fiction, nous pouvons d'ores et déjà répondre à certaines problématiques propres à la société actuelle. Nous pourrions par exemple imaginer un "cyberparlement", qui serait toujours avec des élus (d'un genre nouveau peut-être via une consultation numérique par le biais d'un site internet, une application ou autres...) et des commissions d'experts travaillant de façon optimale (dématérialisation des votes, téléprésence des réunions etc ..) mais qui ne traiteraient que des problématiques liées aux nouveaux usages (et avec la participation des citoyens tout le long via des micro-referendum là aussi par le biais d'un site internet ou une application etc..) : cybercriminalité, cyberterrorisme, régulation des monnaies numériques, droits des objets connectés comme les robots ou les véhicules autonomes, défense des intérêts nationaux et européens (droits des brevets, gestion des infrastructures de communication, de certification, d'homologation etc...). La vie numérique de demain commence maintenant il faudrait que le "législateur" s'adapte et adapte ses usages.

Score : 1 / Réponses : 1

----

La participation numérique ayant actuellement des limites du fait de la couverture du territoire, il faut que les citoyens qui désirent participer puissent s'identifier par courrier directement vers le Parlement mais également en s'identifiant auprès des élus parlementaires.
Le tirage au sort est une solution mais risque de ne pas être représentatif de l'ensemble du territoire où les problématiques sont différentes.
Je ne pense pas qu'il soit opportun d'utiliser l'une ou l'autre de ces formes de participation mais plutôt  de mixer les deux.
La question de l'impartialité est importante, même si je suis convaincu qu'elle reste délicate à garantir. L'identification claire des contributeurs est un préalable et peut se faire par un questionnaire personnel : identification, localisation, situation professionnelle, niveau d'études, implication politique, associatif,....
Au final, la convocation pour participer au débat au sein des commissions parlementaires me semble primordial.

Score : 1 / Réponses : 0

----

Tout ceci me gene grandement. 
Comment peux t on parler de participation numerique alors qu'une grande partie de la population n'a pas acces techniquement à Internet ou n'a pas les compétences requises ?
Cela reviendrai de facto à avoir une population connectée ayant un droit (numerique) et le reste sans droits car n'ayant pas les moyens.

c'est tous sauf de la democratie.

Score : 1 / Réponses : 0

----

Convoquer les citoyens à des sessions de travail du Parlement ou des collectivités locales par le biais de convocations du types de celles des jurys de cour d’assises: il faut stimuler le civisme, offrir une meilleure vitrine de l’action politique, favoriser la proximité des élus et des citoyens.

Score : 1 / Réponses : 0

----

Penser aux garde-fous : 
- ne pas cultiver l'illusion de la démocratie directe qui pourrait faire croire qu'il suffit de s'exprimer via le numérique pour que ses propositions soient retenues. 
- Danger aussi des initiatives citoyennes qui peuvent brasser le meilleur comme le pire et peuvent devenir des instruments de démagogie et/ou de lobbying. 
- Problème aussi du recours aux experts : à la fois une nécessité (l'ignorance ne sert jamais à personne, et un danger (enfermer le débat public entre spécialistes, argument d'autorité).
- La mise en place de plateforme et de réseaux pose la question : quel relais pour quelles reformulations  en vue de quelle restitution ?
Pour être plus positif et compte tenu de la grande ignorance du fonctionnement de nos institutions et de l'élaboration de la loi, il serait bon que l'Assemblée nationale comme le Sénat se dotent d'outils de communication directement accessibles au quotidien. LCP et Public Sénat sont d'excellents chaînes, mais trop peu suivies. Il faut diversifier les supports pour endiguer la crise de la démocratie représentative.

Score : 1 / Réponses : 0

----

Des rencontres plus formelles entre les Députés et leurs électeurs seraient un plus, ceci afin d'échanger régulièrement sur l'état d'avancement du travail législatif et son interaction avec la politique locale.

Score : 1 / Réponses : 0

----

Election des representants via internet est aujourd hui necessaire, pour les personnes ne disposant pas de poste ou d identite numerique tel "service publique", les bureaux de vote pourront y pourvoir.
Je ne pense pas que les citoyens puissent democratiquement participer a l elaboration de la loi, ils ne seront jamais qu une representation de ceux qui peuvent, avoir le temps ou la connaissance...

Score : 1 / Réponses : 0

----

Une démocratie, c’est par le peuple… Alors il faut utiliser la proximité et l’outil informatique. Chaque citoyen devrait pouvoir poser LA question qui lui tient le plus à cœur sur un site de sa circonscription et visible par tous. Si cette question obtient un certain % ( par exemple 5% des inscrits), alors le député prends le relais en invitant un groupe constitué de 10 citoyens afin de lancer une consultation dans sa circonscription. Si cette dernière obtient un seuil à définir, alors elle est lancée dans la région puis si succès au niveau national. Nous avons les idées, les outils ALORS passons à l’action.

Score : 1 / Réponses : 0

----

Il faut, enfin, que le législateur prenne en compte les pétitions qui réunisse un certain nombre de signataires, par exemple 100000.

Score : 1 / Réponses : 0

----

Lors de son discours devant le Parlement réuni en Congrès à Versailles le 3 juillet dernier, le Président de la République a rappelé qu’il ne peut pas y avoir de réforme sans confiance. Or, depuis de trop nombreuses années, le lien de confiance qui doit exister entre les citoyens et leurs représentants s’est peu à peu transformé en un sentiment de méfiance, voire de défiance. 

Il était donc nécessaire d’agir, et vite ! Pour cela, dès le début de la législature, vous avez fait adopter les lois organique et ordinaire pour la confiance dans la vie publique. Ces mesures constituaient des engagements forts du Président de la République durant la campagne présidentielle, nécessaires à la rénovation de notre démocratie. 

Mais vous devez aller encore plus loin, en mettant en place tous les dispositifs et mesures de nature à retisser ce lien de confiance entre les citoyens et leurs représentants. 

Durant les précédentes législatures, l’absentéisme, qui pouvait s’expliquer notamment par la pratique du cumul des mandats, a causé un grand tort à notre institution et a contribué à une montée de l’antiparlementarisme dans notre pays. 

Alors même que tous les observateurs s’accordent à dire que les députés sont désormais plus assidus, sont relayés sur les réseaux sociaux des graphiques tirés du site www.nosdeputes.fr laissant à penser qu’un grand nombre de députés seraient peu assidus, voire totalement absents lors des débats dans l’hémicycle. 

En effet, en l’absence d’un véritable système de « pointage » assuré par l’Assemblée nationale lors des séances publiques, seules les participations aux débats sont prises en compte sur ce site pour détecter la présence d’un député lors d’une séance publique. Les confusions très fréquentes, faites entre participation aux débats et présence réelle dans l’hémicycle sur les graphiques rendus disponibles sur ce site, ont contribué et contribuent, sans le vouloir, à une montée de l’antiparlementarisme. 

Je souhaiterais attirer votre attention sur la nécessité de mettre en place des outils technologiques permettant d’apprécier de manière objective de la présence des députés lors des séances publiques et des réunions de travail des commissions permanentes.

Parce que le mandat du peuple est aussi le mandat de la confiance et de la transparence, ces informations, ainsi que celles relatives à la présence des députés lors des réunions de travail des commissions, pourraient être rendues publiques sur le site internet de l’Assemblée, sur la page dédiée à chaque député. Les députés pourraient ainsi se voir donner la possibilité d’indiquer la raison de leurs absences sur cette même page, ce qui permettrait à nos concitoyens d’obtenir une information objective quant à l’assiduité de leurs députés à l’Assemblée.

Connaissant votre attachement à la nécessité de retisser le lien de confiance entre les représentants de la Nation et les citoyens, je ne doute pas de la bienveillance que vous porterez à ce projet.

Score : 1 / Réponses : 0

----

Chaque député est un représentant de la nation et l'Assemblée nationale doit permettre à l'ensemble de la représentation nationale de participer aux débats et de voter lors des réunions en commissions et lors des séances publiques dans l'hémicycle. 

Pour permettre une participation effective de chacun des députés, il conviendrait de mettre en place des systèmes de visio-conférence afin que les députés élus dans des territoires lointains puissent participer à l'ensemble des travaux à l'Assemblée nationale.

Cela leur éviterait un grand nombre d'allers-retours, et permettrait de plus de faire des économies substantielles en matière de frais de transports.

Bien entendu, ce système de visio-conférence ne serait qu'un supplétif, lorsque les députés de territoires loins ne sont pas en mesure d'être présents à Paris.

Score : 1 / Réponses : 0

----

Effectivement le numérique pourait permettre de généraliser la participation au travaux du parlement, néanmoins il faudrait retirer le 49.3 et les votes par ordonnance qui n'ont rien de démocratique et détériore les relations entre le peuple et les politiciens.

Score : 1 / Réponses : 0

----

Pour qu'une démocratie soit la plus représentative des idées citoyennes, celle-ci  doit savoir lire et écouter les non politiques par le biais de plateforme Internet. Ainsi, il n'est pas compréhensible que les nombreuses pétitions signées sur les réseaux sociaux par les citoyens ne soient pas prises en compte dans l'évolution des textes et lois. Actuellement, seules les pétitions rédigées par écrit sont retenues. Cela doit changer.

Score : 1 / Réponses : 0

----

Pour garantir la rigueur et l'impartialité des consultations, il n'y a qu'une méthode : avoir un groupe de citoyens informés, tout en étant représentatifs du peuple. Pour obtenir cela, le vote par échantillon aléatoire nous donne les propriétés requises (mais d'autres possibilités existent) :
- un groupe de citoyen garanti représentatif (mathématiquement parlant)
- un anonymat complet de ses membres qui permet de les prémunir non seulement des tentatives de coercion, mais aussi de corruption, car il prévient l'achat même des voix
- un système en ligne entièrement public et transparent, où tout le monde peut faire un audit pour vérifier qu'il n'y a eu ni corruption, ni manipulation ni piratage.

Score : 1 / Réponses : 0

----

-	LREM : permanence : assistance, café citoyen
-	National : Service civique, mairies, services publiques (attention à la place de l’agent publique)

Contribution collective du comité LREM Rennes Centre Sud

Score : 1 / Réponses : 0

----

Pourrait on envisager que les députés participent aux débats en "semi-présentiel" et créer en régions  des salles de visio-conférence susceptibles d'accueillir du public ? car si les débats de l'Assemblée sont publics, les parisiens ont un avantage pratique certain. L'idée serait d'utiliser les outils numériques pour rapprocher la représentation nationale des citoyens. A priori les députés d'une région donnée seraient davantage attendus dans ces assemblées délocalisées, mais rien n'empêcherait un député de participer ponctuellement aux débats depuis une région qui ne serait pas celle de sa circonscription.

Score : 1 / Réponses : 0

----

La communication non numérique nous paraît essentielle afin de garantir la participation de tous, elle doit débuter par une information afin que tous les participants aient le même niveau de connaissance du sujet.

Les panels de citoyens seront tirés au sort au niveau national parmi les publics concernés :
Exemple : 
•	mesure sur le travail → cible : des actifs (salariés, artisans, profession libérales, patrons de TPE/PME…) 
•	mesures sur les retraites → cible : des séniors (plus de 50 ans)
•	mesures sur les associations → cible : des bureaux d’associations pour diffusion à leurs membres.
 
Nous pensons que la communication numérique doit être privilégiée pour les moins de 40 ans.

Afin de faire vivre ses 2 vecteurs de communications nous proposons que les mêmes questions soient posées à chaque communauté (numérique et non numérique), et que les résultats soient communiqués à tous.

Score : 1 / Réponses : 0

----

Penser à chaque étape aux citoyens de province car il n'y a pas que Paris. Les problématiques sont différentes en province et cela est compliqué de monter pour une réunion alors que nous sommes beaucoup à vouloir participer a aider nos élus députés. Donnez nous les moyens et considérez nous, merci pour votre écoute et considération.

Score : 1 / Réponses : 0

----

Créer une plateforme numérique accessible aux citoyens inscrits, et complémentaire de l'activité des députés ou sénateurs au parlement.

Score : 1 / Réponses : 0

----

Vous parlez de fracture numérique et proposez face à cela des visioconférences?... Par contre des débats publiques pourraient (devraient) facilement être organisés pour échanger face à face. 
Et une assemblée de citoyens tirés au sort est une idée depuis longtemps mise en avant mais jamais discuté sérieusement... Cela me parait pourtant une évidence démocratique! A l'image des jurys dans les tribunaux, il est possible de constituer un panel de citoyens aussi variés que représentatifs et compétents ce qui éviterait à toutes ces personnes en costards qui sortent de l'ENA de parler au nom du peuple qu'ils ne connaissent pas

Score : 1 / Réponses : 0

----

- Informer les citoyens à travers des campagnes officielles (via la télé, radio, presse) des outils de participation citoyenne. 
- Campagne d'affichage, avec une affiche envoyée aux mairies destinée aux affichages publics.
- Pour compenser la fracture numérique, permettre aux citoyens de disposer de formulaires "papier" à retirer en mairie et retourné directement à l'Assemblée Nationale.

Enfin, permettre (ou obliger) chaque trimestre, les parlementaires d'un même département, à organiser une réunion-débat conjointement, afin de présenter leurs travaux (bilan d'action, sachant que le parlementaire doit théoriquement rendre des comptes aux électeurs), d'y exposer leurs propres opinions et de permettre ainsi un débat d'idées. 

Enfin, pourquoi ne pas organiser des "ateliers décentralisés" de l'Assemblée Nationale. Des ateliers organisés dans chaque département ou le Président de l'Assemblée Nationale, les parlementaires locaux et quelques membres du bureau de l'AN présenteraient, le temps d'une matinée/journée ou le temps d'une réunion publique, l'organisation, le fonctionnement et les travaux de l'Assemblée Nationale, avec l'obligation de visiter, durant la législature, au moins une fois chaque département français. Un moyen concret et simple de rapprocher les citoyens de l'Assemblée.

Score : 1 / Réponses : 0

----

L'énorme question et enjeu démocratique pour demain : mettre la consultation à portée de tous. Pour les élections, on peut donner procuration. Peut-on imaginer une procuration pour une consultation numérique pour les personnes qui sont moins à même de le faire en autonomie?
Pour élargir la connaissance des outils numériques, marier le présentiel avec le numérique = des réunions lors desquelles les participants votent sur des outils simplifiés mais amorçant un début de familiarité avec les séquences de questions utilisées dans les consultations en ligne.
Poursuivre la formation pour tous au numérique par toutes les voies possibles de l'école à l'ephad en passant par les associations et la formation continue en entreprise. Créer un compte numérique pour chaque citoyen avec des crédits pour se former selon ses besoins?
Récompenser l'innovation dans le sens de l'accessibilité pour tous des outils.
On arrive à sécuriser les comptes bancaires, à faire sa déclaration d'impôts en ligne. On doit raisonnablement pouvoir authentifier les consultations et en assurer la rigueur. Procédure de relecture et validations.
Quant à l'impartialité, c'est un horizon mais une illusion. Même si nous vivons sur des bases d'accords entre nous par le biais des lois, notre point de vue reste subjectif et notre partialité naturelle.

Score : 1 / Réponses : 0

----

Informer donc créer une instance indépendante de l'Assemblée nationale qui donnerait les moyens de s'informer sur tous les sujets, pas seulement sur le travail législatif est le préalable. Le travail certes fondamental du pouvoir législatif, ne constitue qu'un des quatre maillons de la démarche démocratique, le deuxième précisément.

Rappelons que la démarche démocratique n'a pas été transcrite dans le contrat fondamental commun, qui est censé tous nous unir, à ce jour.

Il y a l'autre prémisse, le citoyen muni de "libre" arbitre, qui n'est pas actionnée.

Le libre arbitre n'est accessible qu'à partir de la formation complète du cortex pré frontal. D'où le préalable de la formation de l'intelligence"mind" en Anglais, exigible de tout citoyen susceptible d'apporter une contribution à un débat qu'il jugerait d'intérêt pour lui.
La structure que je nomme Pouvoir informatif n'existe pas à cette fin.
La sous structure que je nomme éducation n'existe que pour une fraction de la population, puisque qu'au moins 150 000 jeunes sortent sans  formation reconnue chaque année du système scolaire actuel. La maturité n'est pas un élément fondateur de la citoyenneté actuellement. Seul l'âge étant le critère retenu.
Les critères de base du contractant capable d'apporter son aval, ici au contrat fondateur, n'existent pas actuellement.
Rien n'est opposé au citoyen hors la violence institutionnelle à travers une oppression permanente médiatique notamment et une information-propagande contrôlée peu ou prou par un état quasi autoritaire.
En effet l'information reçue par le canal de l'Assemblée est le fait d'une structure dominée par une organisation partisane, donc par définition, tout sauf indépendante.

Score : 1 / Réponses : 0

----

Quel que soit le mode de discussion pour faire des lois, si le pouvoir n'est pas contrôlé, il est vain de discuter.
Pour améliorer un processus, il faut que le contexte dans lequel il se situe.
Or, en France, le pouvoir n'est pas démocratique car sans contre-pouvoir institutionnel (on peut me parler du système judiciaire 1) en principe il applique les lois, donc la volonté DU pouvoir, 2) bizarrement il est à peu près dans l'indigence pour ne pas dire la misère - donc malgré cela il est bridé par le pouvoir... ). 

Le numérique est un outil formidable puisqu'il rend possible des accès à des données, à des échanges autrefois réservés à une poignée de personnes. lesquelles étaient le pouvoir de par ces possibilités.
Là encore, l'accès aux données est partiel. En effet, il existe encore et toujours de nombreuses données cachées. Les lois et autres bobards pour faire croire à plus de transparence ne sont pas appliquées ou telles qu'il reste beaucoup d'informations inavouables inaccessible à tous. Voir dans le numérique un moyen pour un meilleur exercice de la  démocratie (on parle de cela ?) c'est réduire le débat à peu de chose.
Si il y a problème démocratique, c'est qu'il y a problème institutionnel.

En quoi un outil, aussi plaisant et pratique soit-il, résoudrait un problème institutionnel systémique ?

Cela-dit, on parle de cela depuis pas mal d'années et rien ne se fait.
Il suffit de voir cette consultation, QUI LA CONNAIT ?
La preuve de la VOLONT

Score : 1 / Réponses : 1

----

Quelsquesoit les outils de discussion numérique, je serais pour tant qu'ils seront 'libres'. Par contre je ne ferai jamais confiance au numérique concernant les votes, c'est à dire la prise de décision.
Il sera à jamais impossible à chacun de contrôler la fraude de machines.
Le vote papier, imparfait, est et sera toujours autrement plus sûr.

Score : 1 / Réponses : 1

----

Ce projet est un projet long terme et d'ici sa mise en place la fracture numérique deviendra marginale. Si ce n'est pas le cas il faudra compléter l'approche numérique, projet par projet suivant le sujet par une approche présentielle

Score : 0 / Réponses : 1

----

Les députés pourraient organiser dans leur permanence locale des formations, avec leurs collaborateurs, pour les personnes qui n'ont pas les moyens d'avoir un matériel informatique ou la chance d'avoir pu apprendre à s'en servir.

Les permanences devraient être dotées de matériels informatiques disponibles pour les citoyens qui veulent s'exprimer sur des plateformes officielles de démocratie participative. Ainsi les ordinateurs seraient configurés pour que les citoyens accèdent facilement aux sites gouvernementaux et parlementaires sur lesquels des informations d'actualité sont disponibles et des procédures participatives ou délibératives sont organisées.

Sans être la panacée, cela offrirait un accès à Internet à des personnes qui en sont dépourvu et qui restent par conséquent silencieuses alors que leur opinion est primordiale pour construire un vivre-ensemble correct.

Dans le même ordre d'idées, les députés devraient offrir des formations aux citoyens de leur circonscription pour leur expliquer le fonctionnement des institutions, le rôle et les compétences de certaines autorités publiques en fonction de l'actualité (par exemple quel est le rôle du Défenseur des droits, de la CNIL, etc.). 

Ils pourraient alors faire appel à des universitaires qui viendraient exposer dans les grandes lignes ces différents points et répondre aux questions que se posent les citoyens.

Score : 0 / Réponses : 0

----

Bonjour à tous, Il existe une grande différence entre les différentes formes de communication en dehors de l´aspect qui concerne l´accès et le publique cible. La communication écrite peut induire en erreur et 95 % de la communication est non verbale. L´orale permet donc une ligne médiane, mais le présentiel reste la communication la plus fiable. Toutefois s´il s´agit de consultation simple, sur des idées déjà énoncées, le numérique est la bonne solution. Ainsi le choix le plus judicieux, serait d´assurer la présence des plus créatifs, qui sont porteurs d´idées nouvelles durant des phases plus constructive et de rendre la consultation numérique possible par le biais d´une application téléphonique. Les personnes les plus défavorisé ayant souvent á défaut d´accès internet un accès téléphonique. Donc une méthodologie en deux temps, un premier temps pour les personnes ayant des idées qui ne sont pas encore reprise dans une liste préexistante, et qui pourrait en fonction de leur apport être évaluer pour justifier une présence lors des consultations suivantes (évitant ainsi la présence de personnes sans esprit de syntaxe, et n´apportant pas au débat) puis un second temps celui de la consultation numérique sous forme de choix multiples. Se pose évidemment la question des personnes apportant au débat et ne pouvant être sur place, alors la mise en ligne de vidéos d´une minute pour exposer l´idée générale, pourrait être une piste afin que la consultation de 60 vidéos ne dépasse pas une heure.  En résumé : Sur place (le week-end) pour ne pas favoriser les habitants de l´ile de France et donc orienter la consultation, pour les porteurs d´idées, ou en ligne sous forme de contribution visuel, auditive. Et enfin une application téléphonique pour une consultation sur base des choix multiples.

Score : 0 / Réponses : 0

----

Génial de voir une telle initiative qui mettrait l'Assemblée en mouvement! Ce projet est à concrétiser rapidement car pouvoir débattre de cette manière dans les territoires, au plus proche des gens et de ce qu'ils vivent, c'est vraiment faire société.
 Rien ne remplace des échanges en face à face et il y a une vraie complémentarité à instaurer entre une participation numérique et celle organisée dans des tiers lieux pour une parole responsable, amplifiée et fédératrice. Un bon moyen pour conserver  la connexion de nos élus avec la vie réelle et ceux qu'ils représentent!

Score : 0 / Réponses : 0

----

Les pratiques de participations des citoyen-ne-s devraient être soutenues par un service public dédié, qui force les élus à expliciter les choix et décisions prises au nom du peuple.
Les élus parlementaires ou territoriaux seraient tous concernés et confrontés non pas seulement à leurs pairs et à leur parti politique, mais à tous les citoyen-ne-s.
Les citoyen-ne-s seraient tous invités à participer à des assemblées locales, en proximité de leur lieux de vie, pour être informés des décisions publiques et questionner les fonctionnaires indépendamment des élus de l’exécutif.
Le service public dédié assurerai en ligne la transparence de la démarche, la participation à distance, la communication des échanges, la qualité des synthèses présentées à sous et toutes en langage compréhensibles.

Score : 0 / Réponses : 0

----

L'échange d'informations par Internet est un réel progrès. Néanmoins il ne faut pas pour autant supprimer les anciens moyens d'échange d'informations.
Les personnes âgées ou handicapées sont souvent incapables d'utiliser les moyens numériques. Les livres, les revues, le document papier demeurent des supports d'information à préserver. Ce n'est pas parce qu'on dispose de voitures, de trains, d'avions qu'on ne circule plus à pied ou à vélo ! De même ce n'est pas parce que le formulaire électronique existe qu'il faut supprimer le formulaire papier encore utile pour certains d'entre nous. Parfois, d'ailleurs on peut trouver de mauvaises utilisations du numérique; par exemple le livre reste plus pratique et plus écologique que la liseuse informatique qui suppose une grosse infrastructure énergivore et gourmande en ressources terrestres.
Le débat via Internet me semble souvent biaisé parce qu'on ne dispose pas d'une méthodologie de conduite du débat. Ce défaut se retrouve également en réunions publiques. Je voudrais donc en profiter pour suggérer une méthodologie venant des cercles de qualité et qui pourrait bien s'appliquer à la conduite d'un débat. Le principe de cette méthodologie est relativement simple. On part d'un sujet à traiter et un seul. On demande aux participants de formuler des propositions ou des réflexions sur le sujet. On les note toutes.
Puis on demande aux participants de mettre de l'ordre dans ces propositions, de les classer, de les hiérarchériser. On utilise le vote en cas de différent pour trancher. Ensuite on demande aux participants de choisir une des propositions ou groupe de propositions classées. ceci par un vote. 
Pour la proposition retenue, on demande aux participants de formuler des solutions ou ébauches de solutions qu'on note, qu'on classe comme ci-dessus. Puis vote sur le choix d'une solution ou groupe de solutions. On peu bien entendu poursuivre pour affiner. Mais il faut que le débat débouche sur une action concrète à faire.

Score : 0 / Réponses : 0

----

Les comptes-rendus de mandat des élus (parlementaires ou élus territoriaux) sont le plus souvent des actes militants qui permettent de conforter et fidéliser un électorat.
S’ils étaient encadrés par la loi, rendus obligatoires, pilotés par des modérateurs plus techniciens que partisans et adossés à des outils de suivi construits à partir des programmes et engagements des élus, ils pourraient trouver une efficacité autre que la simple justification de telle ou telle position.
Par ailleurs, même si l’expression des citoyens doit d’abord s’envisagera à travers une participation physique, par exemple aux réunions de compte-rendu de mandat évoquées ci-dessus, un bureau virtuel de propositions peut être mis en place,  dans chaque préfecture permettant sur telle ou telle proposition citoyenne de recueillir  le soutien d’une majorité d’électeurs. Le terme « majorité «  est important, car 5%, 10%  des électeurs ne sauraient remettre en cause une loi votée par un parlement à priori représentatif .

Score : 0 / Réponses : 0

----

PARRAINAGE CITOYEN
De belles initiatives associatives existent pour réduire la fracture numériques et sociale (Emmaus connect et diverses actions gouvernementales citées par "Société numérique"). Pour œuvrer au niveau local, pourquoi ne pas mettre en place  une application de parrainage entre citoyens de type "matchons nos compétences" où une initiation à l'emploi du numérique, à la vie publique, politique serait réalisée à des voisins proches contre un service quel qu’il soit ?!

Score : 0 / Réponses : 0

----

Pour une démocratie durable Ecouter et Informer: CET, CTE, et Le XXI siècle,

Tout le monde à vue Thalès, tout le monde à vue Guillotin, 
Tout le monde à vue Poubelle ?
Tout le monde reconnaît un échafaudage, tout le monde entend l’échafaud, Tout le monde y jette ce qui est faux ?
Tout C, tout E, Tout T ?
Tout Citoyen, tout Elu, Tout technicien ?
Tout doit être un piéfort pour le citoyen, Tout doit être un piédestal pour l’élu,
Tout doit être un piédroit pour le technicien.

La droite est une chose linéaire et il faut qu’elle fonctionne courtoisement, poliment. La droite doit inventer le triangle pour passer en trois points. La gauche est à l’autre bout de la droite, elle doit faire son agenda du vingt et unième siècle de la démocratie.
Ma sœur, masseur, sculpteur, j’ai peur, très peur que vous fassiez des bêtises à ne pas donner de logique au conseil de développement en démocratie. Ne pensez pas au volume d’une pyramide, ne dessiner qu’un triangle dont vous êtes le sommet ? Ne vous arrêter par en chemin en considérant que seule la droite est la plus courte distance qui sépare deux points.

Score : 0 / Réponses : 1

----

Nous élisons un député, il est donc normale de connaitre le vote de celui-ci. il faut donc que tous les scrutins soient publique et que sur une plateforme internet lié à notre numéro d'électeur, nous ayons les informations de vote, d'intervention, des lois proposées, de la présence de notre député que se soit en assemblée ou en commission. Cette plateforme pourra proposé l'envoi automatique de ces informations par mail. De là, le citoyen sera informé des actions de son député est pourra voté en son âme et conscience.

Score : 0 / Réponses : 0

----

La participation numérique et présentielle sont complémentaires car elles enrichissent à elles 2, les contributions en touchant un plus grand nombre de citoyens.
En effet, par exemple, le numérique pallie aux problèmes de temporalité et de mobilité des citoyens.
Cependant, dans ces 2 formes de participations de nombreux freins sont à travailler.
Pour les consultations numériques:
hormis les problèmes à l'usage, il ne faut pas sous estimer les besoins de compétences rédactionnelles qu'exige les contributions numériques pour argumenter ou tout simplement exprimer une idée.( je rejoins les renvoie faits par la société numérique vers des services d'inclusion au numérique pour l'apprentissage aux TICES)
De même pour le présentiel, il est nécessaire de prendre en compte la difficulté de prise de parole en public, le regard de l'autre ou de pouvoir répondre aux questions éventuelles.
C'est pourquoi, il me semble impératif de travailler sur ces freins pour développer la capacitation de chacun à participer aux différentes formes de consultations.
En ce qui concerne la garantie de l'impartialité et la rigueur des consultations, nous possédons des organismes qui ont fait leurs preuves comme la CNIL ou la CNDP.
N'est il pas plus pertinent de se tourner vers des organismes déjà existants?

Score : 0 / Réponses : 0

----

Bonjour,
 
Je suis une alliée dans le mouvement ATD Quart Monde.Ensemble, militants et alliésn nous luttons pour détruire la misère.
J'apporte une contribution personnelle à l'appel à paticiper à l'atelier législatif citoyen relatif à la démocratie numérique et les nouvelles formes de participation citoyenne.
 
 
Je veux raporter un écrit de Lucien Duquesne volontaire permanent d'ATD Quart Monde suite à un interview du sociologue Fréderic Viguier paru dans le journal d'ATD Quart Monde de Juin 2017 :
 
"A la question sur comment les regard des pauvres a changé avec Wresinski F Viguier répond : "avec lui les pauvres sont des acteurs à part entière de leur prise en charge".
Ce n'est pas juste de réduire à ce point la pensée de Joseph Wresinski explique Lucien Duquesne, il n'a cessé d'appeler à ce que les personnes les plus exclues puissent débattre là où débattent les autres hommes pour l'avenir de toute l'humanité et non pas pour l'amélioration de leur prise en charge ! Il n'a cessé de répéter que ceux qui ont subi les pires injustices depuis toujours ont une parole essentielle à dire pour faire avancer la justice pour tous et non pour eux seulement".
 
Frédéric Viguier dit que Wresinsdi a été le plus cohérent dans la volonté d'appliquer cette expérience jusqu'au bout, à toutes les étapes des politiques sociales. Là encore la penée de Joseph Wresinski est ratatinée. Il s'est battu pour que les citoyens les plus en difficulté aient leur mot à dire dans la conception et dans l'évaluation des politiques publiques pas seulement sociales !
 
Les personnes qui vivent l'exclusion sont pour beaucoup encore loin des outils permettant d'avoir accès au numérique.
 Comment faire alors pour que leur reflexion puisse être entendue et serve à faire avancer la justice pour tous.
Quels outils peuvent être mis en place pour que la pensée d'ATD Quart Monde fasse débat ?
 
Je vous remercie de prendre en compte ma c ontribution.

Michèle jobert
Givry

Score : 0 / Réponses : 0

----

Le numérique est un atout, qui peut se combiner avec des contributions présentielles, par exemple sous la forme d'un atelier animé par un modérateur qui compile les interventions et les synthétise (ce qui évite l'écueuil de l'aspect fourre-tout d'Internet, et la trollisation des débats). La représentativité pourrait être garantie par un double système de volontariat et de tirage au sort (sur la base des listes électorales), permettant d'optimiser l'efficacité et la représentativité du processus, et d'éviter les biais liés à la fracture numérique.

Score : 0 / Réponses : 0

----

Développer la médiation autour du travail parlementaire 

Afin de garantir l’égalité d’accès de tous les citoyen-ne-s à ces modes de contributions citoyennes, le Conseil national du numérique souligne l’importance de renforcer la médiation autour des travaux parlementaires. Plusieurs pistes pourraient être explorées, notamment :

Former les citoyen-ne-s aux nouveaux modes de contributions citoyennes dès le cycle secondaire. Ces formes de contribution sollicitent des compétences de littératie numérique et devront être pratiquées dans les environnements éducatifs ;

Améliorer la compréhension par tous des procédures parlementaires et des informations produites dans ce cadre par des outils de visualisation en ligne, dans une approche de legal design . Le Parlement devrait contribuer à une transparence administrative et à une meilleure compréhension et accessibilité du droit. D'ores et déjà, le Sénat fournit des efforts en ce sens : il a notamment expliqué de manière pédagogique et aisément compréhensible par tous la navette parlementaire avec une infographie et une vidéo. Plusieurs chantiers pourraient être lancés : notamment sur l’accessibilité des dossiers législatifs et des informations relatives aux processus législatifs via une plateforme unique. Des appels à contribution pourraient inviter les citoyens eux-mêmes à créer des infographies de vulgarisation ;

Faire émerger de nouveaux services d’accompagnement auprès des citoyen-ne-s, par exemple pour les aider à mieux comprendre la procédure parlementaire et les informations qui sont produites, ou encore à traduire leurs idées en langage juridique.

Source : https://cnnumerique.fr/wp-content/uploads/2017/07/Avis_CNNum_Confiance_Vie_Publique_vF.pdf

Score : 0 / Réponses : 0

----

SYNTHESE CONSULTATION CITOYENNE 5911

Fracture numérique : fracture générationnelle qui peut être prise en charge par le monde associatif et permettre de recréer du lien. (Essentiellement dans le but de lutter contre l'abstention entre des personnes âgées qui votent mais qui n'ont pas accès et des jeunes qui ont accès au numérique mais désintéressés de la politique).
Antennes relais dans les mairies et les lieux publics.
Création d’un Journal Officiel simplifié et accessible à tous.
Simplifier l’accès aux sites officiels souvent complexes pour accéder à l’information recherchée.
Utiliser les élections officielles pour mettre en place une « boîte à idées », un référendum numérique ou sondage.
Mise en place d’une plateforme numérique avec des filtres comme la popularité et des outils pour réglementer qui pourraient être des « signatures numériques » comme l’empreinte ou utilisation de boîtiers numériques comme c’est le cas en Suisse.
Accentuer la pédagogie notamment à travers des vidéos que feraient les députés pour expliciter les projets et les lois.

Score : 0 / Réponses : 1

----

Aujourd’hui, la participation numérique n’est pas représentative. La fracture numérique ne permet pas de consulter les citoyens, elle crée des biais et ne saurait remplacer le vote des députés.
Avant d’envisager la participation numérique des citoyens, il est crucial et nettement plus facile de commencer par l’information numérique des citoyens (cf mon commentaire sur la consultation en amont des textes). Cette information doit être fiable et aisément accessible.
Les panels citoyens tirés au sort ne sont pas compatibles avec la notion d’indivisibilité de la République (préambule de la Constitution). Pourquoi ceux-ci et pas d’autres ? Ce type de panels ne peut être que consultatif. En revanche, les députés représentent l’ensemble du peuple. A eux de consulter leur base, d’exercer leur jugement et de voter en pleine responsabilité.
Attention à la dictature de la bienpensance numérique. Les Français n’ont pas encore suffisamment confiance dans le numérique pour en faire un outil fiable d’expression de leur citoyenneté. Je rappelle ici que les Suisses ont introduit le vote électronique depuis chez soi en pensant réduire le taux d’abstention. Aucun effet sur l’abstention dans les villes, effet négatif sur l’abstention dans les zones rurales (hausse du taux d’abstention).
La confiance en la vie publique se gagnera sur la dimension humaine des rapports citoyens et notamment les rapports des citoyens avec leurs députés. Le numérique peut y contribuer par la diffusion d’une information fiable disponible pour tous.

Score : 0 / Réponses : 0

----

Historiquement, le propre de la démocratie représentative était d'avoir des dirigeants éclairés et de répondre aux contraintes du territoire: tous les citoyens ne peuvent se rendre à un même endroit tous les jours prendre des décisions pour tous.
A l'ère du numérique, ces deux contraintes disparaissent: les citoyens sont éduqués et sur internet disposent de ressources diverses et de tous les bords pour mûrir leurs opinions. De plus, l'outil numérique efface la contrainte de la distance. Pourquoi ne pas mettre en place une démocratie numérique? Certaines décisions seraient mises en ligne, à la manière d'un référendum, et seraient débattues puis votées par les citoyens. On pourrait imposer le fait que le Parlement suive les décisions adoptées mais pas forcément. A la manière d'un référendum ayant vocation à consulter, les décisions prisent en ligne créeraient un moyen de pression sur les dirigeants et devraient être fortement médiatisées afin qu'une décision importante soit soumise à l'avis éclairée du plus grand nombre. Néanmoins, laisser le Parlement choisir en dernier lieu pourrait éviter certaines abérations. Le rôle de cette consultation ne pourra être pleinement rempli que si elle est suffisamment mise en avant, si ces décisions sont généralement suivies et surtout si elle est mise en place régulièrement et qu'elle donne la sensation aux citoyens d'avoir un vrai pouvoir.

Score : 0 / Réponses : 0

----

- Présence obligatoire des députés aux séances de l'assemblée,

- interdiction des téléphones portables dans l'assemblée ; 

Que les députés commencent par faire sérieusement leur travail sous peine de sanctions comme par exemple retrait sur salaire pour toute absence voire travail d'intérêt général.

Qu'ils soient vraiment exemplaires dans la recherche de l'intérêt collectif.  

Sinon il y aura toujours cette défiance des citoyens vis à vis des députés et plus généralement vis à vis d'une classe politique considerée par beaucoup comme nantie et non soumise aux mêmes exigences que les autres citoyens. Comment justifier ces images de députés qui s'insultent, dorment, jouent sur leur portables, ne s'écoutent pas, ...? Nous représentent-ils vraiment ? 

> La moralisation commence par le devoir d'exemplarité.

Score : 0 / Réponses : 0

----

Les formes de participation numérique peuvent inclure : l'information largement diffusée à la télévision, sur les réseaux sociaux, type spot publicitaire, sur les sites de service public, par email via les députés, le débat en ligne sur des plateformes participatives, les propositions via ces plateformes ou une application dédiée, un portail participatif sur lequel tout citoyen possèderait son propre compte...

Les formes de participation présentielle peuvent inclure : l'information par les députés ou les élus locaux en circonscription ou dans les communes, les débats dans des réunions publiques de ce type ou ateliers citoyens, des débats entre "nouveaux citoyens" de 18 ans lors d'un parcours de rencontres étalé sur une année, en remplacement de la Journée Défense et Citoyenneté, animées par des députés et des spécialistes de sujets en cours, dans laquelle les nouveaux citoyens seraient sensibilisés aux nouvelles formes de participation citoyenne et apporterait leur participation aux débats du moment (cf. ma proposition dans la catégorie Consultation en amont des textes), les débats entre des citoyens volontaires et des citoyens tirés au sort qui constitueraient soit une troisième chambre ponctuelle (travaillant sur un sujet en particulier, le temps d'une loi), soit remplaceraient le Sénat.

Le bilan des participations numériques ou locales en présentiel doit être fait, avant d'être présenté aux deux assemblées et aux citoyens tirés au sort ou volontaires, qui peuvent s'appuyer sur ces propositions et apporter les leurs dans leurs propres débats. Ces participations citoyennes, à la fois en présentiel et numériques, au niveau local puis national, seraient donc incorporées au processus existant au Parlement. Elles serviraient à enrichir le débat, apporter une plus grande variété de points de vue, ainsi qu'à intéresser davantage les citoyens au débat public car ils se sentiraient capables d'être entendus. Il s'agit d'un processus au long cours, qui attirerait de plus en plus de personnes et gagnera en diversité et inclusivité au fur et à mesure. La sensibilisation à ce processus via des Journées Défense et Citoyennetés réformées permettra de toucher l'ensemble de la population, au fur et à mesure que le temps passe.

Score : 0 / Réponses : 0

----

J'ajoute, à la suite du débat d'hier soir à Mauguio, qu'étant juriste et utilisateur habituel des banques de données juridiques et de Légifrance, je suis prêt à participer à votre réflexion sur l'amélioration de ces sites afin d'en faciliter l'accès le plus large possible par les citoyens.

Score : 0 / Réponses : 0

----

Créer une seule et unique plateforme numérique de consultation. C’est déjà la deuxième créée afin de donner un avis au gouvernement et sa visibilité n'a pas été un franc succès. 
Cette plateforme (en .gouv) regrouperait différents vecteurs qui ont déjà été mis en place depuis le début du mandat d’Emmanuel Macron : Enquêtes, consultations, sondage… Chaque citoyen aurait un compte personnel avec des sécurité (n°d’élécteur?) car ici pour venir il suffit de se connecter à son compte facebook…j’en possède deux ce qui fait que je peux venir discuter avec les deux si j’en ai envie, en cas de sondage je pourrait voter deux fois etc… La mise ne place d’un tel outil demande quelques mois de travail mais il faciliterait grandement les échange tout en permettant à tous les citoyens de venir s’exprimer en leur nom (identifiant visible différent du nom/prénom pour ne pas donner lieu à des chasses aux sorcières).
De plus, cette plateforme ne serait pas seulement descendante. Elle pourrait servir à ouvrir des sujets sous différentes formes (sondage, pétition) qui à partir d'un certain niveau (pourcentage de vote, de voix, ou de post dans un fil de discussion par utilisateurs différents) remonteraient de manière visible au gouvernement/députés/sénateurs/maires...
Il faut également que tous les élus de la République ait un accès sur cette plateforme et puisse organiser des consultations plus locales en fonction de leurs besoins.

Score : 0 / Réponses : 0

----

Je pense qu'il ne faut privilégier aucune forme de participation. Pour ce qui concerne internet tous les citoyens n'y ont pas accès ou ne savent pas s'en servir. Il faut permettre au plus grand nombre de s'exprimer. Pour garantir l'impartialité et la rigueur des consultations il faudrait que cela se fasse par des organismes reconnus et labellisés.

Score : 0 / Réponses : 0

----

Je propose, à la réflexion de tous, divers éléments afin que la « contribution citoyenne » atteigne la meilleure efficacité.
1)	La circulation de l’information à double sens : entre le citoyen, les  communautés de citoyens locaux < = >  les élus de toutes sensibilités politiques.
2)	Des contributions  cadrées sur des thématiques – sous-thématiques (explicitées  en langage compréhensible par la majorité des citoyens et limitées dans le temps) :
a.	 en phase avec les travaux des commissions parlementaires du moment, 
b.	en amont des autres thématiques inscrites dans la législature,
précisant bien qu’il s’agit d’un travail collaboratif et non d’une interpellation concernant des cas particuliers,
une thématique – sous thématique close peut être ré-ouverte pour revoir des aspects spécifiques, de même que pour évaluer ultérieurement l’application de la loi.
3)	Les outils : une plateforme numérique et  du présentiel à l’initiative des communautés de citoyens locaux et/ou des élus (un rapporteur désigné  dans le cadre d’un atelier organisé en présentiel pourra apporter les résultats des travaux de cet atelier sous le même format qu’une contribution individuelle).
4)	Le traitement des contributions : par les élus et leurs équipes parlementaires, apporté ensuite dans le cadre des travaux de la commission à laquelle ils appartiennent.
5)	Le retour de l’information : synthèse  des contributions prises en compte par l’équipe parlementaire (via la plateforme créée à cet effet, repris en présentiel à l’initiative  des communautés de citoyens locaux et/ou des élus), leur répercussion au sein du travail législatif.
6)	La  formation du citoyen : en relation avec une thématique ou une sous-thématique via la création de microlearning,  sur la même plateforme ou via un lien sur une plateforme dédiée (exemple réalisé par En Marche : https://en-marche.fr/articles/communiques/larem-lance-son-premier-microlearning-sur-argent-public).
7)	D’où une organisation structurée :
a.	Simple d’accès (pas d’usine à gaz, pas de multiplication des plateformes),
b.	Utilisable sans perte de temps pour les citoyens,
c.	Optimisant le temps de nos élus et de leurs équipes.

Score : 0 / Réponses : 1

----

La question du lobbying est posée pour une participation numérique, qu'il faudra contenir par des filtres humains qualifiés et disponibles afin de limiter les dérives autocratiques, corporatistes, sectaires, communautaires... si l'on veut disposer in fine d'un travail constructif.

Score : 0 / Réponses : 0

----

OBSERVATOIRE CITOYEN

Réflexions sur l'opportunité qui nous est offerte concernant la Participation à la Démocratie Numérique et les nouvelles formes de participation citoyenne :
 
Pour redonner confiance dans la vie publique il nous est proposé :
Une consultation en amont des textes,
Une interaction avec la procédure législative
Une participation à l'évaluation de la mise en oeuvre des lois.
 
A ce jour le constat laisse perplexe... 
Pourtant il est écrit dans la constitution que " la loi est : pour le peuple et par le peuple ".
Avant tout projet il faudrait s'assurer de l'application des lois existantes sur le sujet et d'en avoir une évaluation.
Si le nouveau texte doit apporter une amélioration : prendre la décision de supprimer les strates inutiles. La lecture en serait plus simple.  
 2 projets de lois pour exemple posent problème : 
L'obligation des 11 vaccins. 
Cette proposition est déjà adoptée par l'assemblée elle n'a à ce jour concerné que des experts ; aucun débat public contradictoire n'a été engagé et comme par hasard aucune implication citoyenne n'a été demandée alors que nous sommes tous très concernés. 
Une grande majorité de nos concitoyens réclame : transparence, informations sur les adjuvants (l'aluminium qui est interdit aux USA pour  les vaccins chez les animaux).
 Le précédent gouvernement avait mis en route une concertation sur la vaccination qu'en est -il ?
  Peut-on exiger une étude sérieuse et indépendante menée sur l'association des 11 vaccins (effets secondaires et efficacité). 
Si des problèmes sont révélés : les laboratoires pharmaceutiques ou l'Etat s'engagent -ils à en assumer les conséquences ? 
Que devient le principe de précaution ? 
 
Loi contre le Harcèlement de rue : 
Réaction précipitée et purement émotionnelle (on a connu ça sous d'autres législatures...), harcèlement moral dans le monde du travail oublié...
Le sujet est délicat quel est le véritable objectif ? (souci de clarté.)
 
En conclusion ces décisions semblent arbitraires et précipitées
Il aurait été judicieux, pour remplir cet objectif de confiance qui fait défaut, de mettre tout en oeuvre pour engager cette participation citoyenne qui semble être la nouveauté, le changement de la nouvelle législature...

Score : 0 / Réponses : 0

----

Le numérique est devenu avec les résaux sociaux une formidable opportunité d’expression des citoyens, non seulement au moment des consultations électorales, mais aussi tout au long des mandats en cours. C’est ainsi que l’initiative citoyenne d’Arash Derambarsh, a permis de faire adopter en France de manière transpartisane, une loi sur la fin du gaspillage alimentaire par les supermarchés, qui aspergeaient de l’eau de javel sur les produits jetés, les rendant impropre à la consommation alors que leur péremption n’était pas atteinte, et qu’elle est en passe d’être adoptée au niveau européen.
Toutefois, pour qu’un texte d’origine citoyenne puisse venir en débat devant le Parlement, je pense qu’il faut mettre un certain garde-fou notamment en matière de nombre de signataires, pour éviter des propositions farfelues.
Je fais donc la proposition que comme pour le Conseil Economique Social & Environnemental pour qu’une consultation puisse être inscrite à l’ordre du jour de l’Assemblée Nationale, il faille qu’elle recueille au moins 500 000 signataires. C’est à la fois beaucoup, et peu au regard du nombre de citoyens. Ce serait un bon moyen d’associer aux travaux législatifs, les citoyens, qui ont trop souvent l’impression d’être tenus éloignés des décisions qui les concernent au premier chef.

Score : 0 / Réponses : 0

----

bonne idée sauf que je viens seulement de l'apprendre par mail...

Score : 0 / Réponses : 0

----

Chaque citoyen doit avoir accès à la vie du pays et aux décisions concernant les lois. Nous avons été floués lors de la consultation pour l' EUROPE mais nous ne voulons plus l' être. Je ne veux pas des accords avec le CANADA qui induirait l'arrivée de mal-bouffe des Etats Unis. Le peuple doit être consulté au lieu de décisions qui viennent du parlement européen et qui ne sont pas toujours bonnes pour nous. De même pour les décisions des députés, il y a à redire !

Score : 0 / Réponses : 0

----

La voix aux citoyens pour participer à la vie politique, quel bonheur....

Score : 0 / Réponses : 0

----

Ce qui manque le plus dans la liberté d'expression c'est celle de l'amour d'y  croire ! Et au vue de toutes les lois. Notre société kafkaïenne ne répond plus rien ! Et puis le principe de précaution, c'est fini ! De qui se moquent l'état français !  Le citoyen du futur serait t'il de plus en plus pauvre?! Et le savoir encore plus? Je ne veux pas que des entreprises ou sociétés nous dictent notre façon d'être. Un être humain digne! Respecter les autres et les autres te respecteront peut être !
Il nous faut changer toutes les façons de travailler ! Ne travaillons plus à l'encontre de notre belle et unique planète! Qui tous les jours qui passent en prends encore plus à chaque année passe et ainsi de suite ! De toutes façons l'humanité ne dois plus vivre avec crédit ! Car la planète donne !!!! En échange d'une eau, pure de l'air sain, une symbiose avec notre belle famille qu'est la nature! Les êtres humains laisseront derrière eux-mêmes le brevet de la connerie humaine! le vivant ne doit pas être brevetable !
Certes c'est un coup de gueule diront certains ! Mais au final? Ce sera notre réalité quotidienne qui en pâtira d'avoir laissé faire !

Score : 0 / Réponses : 0

----

Rien n'est pas parfait, ceci ne doit pas empêcher d'avancer. La consultation numérique n'est qu'un élément dans la prise de décision

Score : 0 / Réponses : 0

----

Pour que la voix des français soit réellement entendue ce qui n’est plus le cas depuis longtemps’ merci demos cratos 😉

Score : 0 / Réponses : 0

----

D'accord

Score : 0 / Réponses : 0

----

Et une EMISSION TV interactive ?

Bonjour ayant travaillé de nombreuses années dans le milieu de la TV, il me semble que l'outil internet devrait être associé à une émission de télévision intéractive sur la chaîne parlementaire et une web TV propre afin que les projets soient travaillé et soumis en amont de manière globale : Internet + TV afin que tout le monde puisse interagir même celles et ceux qui ne sont pas  l'aise avec le numérique ou n'y ont pas accès.

La construction s'articulerait sur la base d'internet et accompagnée en support explicatif et constructif via l'émission.

je pourrai développer le concept sans souci ICI ou directement.

Score : 0 / Réponses : 0

----

Après avoir participé à l'élection d'un député, nous n'avons plus de contact avec l'élu (homme ou femme bien sûr). Cela se comprend puisque l'élu ne peut pas "voir" tout le monde.
Par contre, je pense qu'il serait intéressant d'avoir une plateforme numérique qui permettrait de dialoguer et débattre avec les autres participants à l'élaboration d'une loi, à la suggestion d'idées, etc...
Sachant que c'est toujours l'élu qui vote selon sa conscience et donc ne satisfera pas tout le monde... A l'élu d'expliquer son vote avec des arguments.

Score : 0 / Réponses : 0

----

Dans quels cas privilégier l'une ou l'autre de ces formes ? 

La forme numérique doit être privilégiée lorsque l’on est certain que la population visée est équipée ou à même de l’être.
Dans les autres cas, il faut opter pour des solutions mixtes.

Score : 0 / Réponses : 0

----

Comment garantir l'impartialité et la rigueur des consultations ?

- Appliquer aux consultations des exigences de représentativité à l’instar de celles appliquées par les instituts d’étude et de sondage,
- Communiquer assez largement pour toucher un maximum de personnes,
- Mettre en oeuvre des moyens de participation les plus inclusifs possibles,
- Pratiquer un dépouillement systématique réunissant des représentants de tous les partis politiques et des observateurs neutres et non politisés, choisis en évitant tout conflit d'intérêt. Faire signer un contrat d’engagement à ce groupe de personnes.

Score : 0 / Réponses : 0

----

A quelques minutes avant la fin de consultation, il me semble important de parler de terminologie sur la participation citoyenne.
 Alors que celle ci est déclarée par le dispositif de participation citoyenne du ministère de l'intérieur ici : http://circulaire.legifrance.gouv.fr/index.php?action=afficherCirculaire&hit=1&r=33332
dont l'objectif est de renforcer la sécurité des communes , ce pouquoi j'ai crée une société pour informatiser ce dispositif, que je n'ai pas l'autorisation de parler ici.
La loi ne devrait pas utiliser cette terminologie autrement que pour ce dispositif décrit dans cette circulaire.
Ma plateforme ,liée à ce dispositif, utilise un terme raccourci entre participation et citoyenne pour faire le lien entre les 2 termes.
Le mélange synthaxique d'un terme avec 2 buts différents perturbent les utilisateurs (une recherche Twitter sur participation citoyenne le prouve).
Le but de ma plateforme, restante confidentielle pour chaque commune, est de faciliter et processer les déclarations des faits des habitants dont la participation citoyenne a été signée par la ville et par le prefet.
La sécurité à tous les niveaux étant au coeur des 2 objectifs de sécuriser les biens personnelles par la vidéo protection associée à la participation citoyenne entre habitants et communes (pour les communes ayant mis en oeuvre le protocole de participation citoyenne), et ce de maniére non excessivement cher pour les habitants et les communes.
Plusieurs plateformes existantes utilisent la participation citoyenne sans lien avec la circulaire du ministére de l'intérieur pour leur activité, ce qui me semble inadapté à ce jour.

Score : 0 / Réponses : 0

----

Le manque de représentativité numérique du à la fracture du même non pour de ne pas donner plus  de pouvoir directement aux citoyens n'est pas une bonne justification, car aujourd'hui, le groupe majoritaire à l'assemblée ne représente réellement que moins du 1/6 des electeurs au premier tour. Globalement l'assemblée en 2017 n'a pas eu le plébiscite assurant sa légitimité, sachant que son rôle est de représenter les français. D'un autre côté, une ordonnance macron sur le télétravail considère que l'employeur n'a pas à rembourser les frais d'accès à internet pour le télétravail car il est considéré que tout le monde y a accès. Personnellement, je n'ai pas vote au 2ème tour parce qu'il 'y avait personne qui me convenait ou à qui je pourrais faire confiance pour me représenter. L' assemblée nationale à perdu sa légitimité en opposant au vote des français sur l'Europe de 2005. Elle y a perdu sa crédibilité et devrait de ce fait être complètement modifiée.

Score : 0 / Réponses : 0

----

En s'appuyant sur ce qui existe pour la télémedecine dans les déserts médicaux, il serait intéressant de filmer des consultations citoyennes et de les retransmettre à l'Assemblée nationale, ou au sein de groupes parlementaires, afin de réduire la fracture numérique existant entre les citoyen-ne-s selon le lieu d'habitation et la couverture numérique potentielle dudit territoire.
Ainsi, le plus grand nombre aurait son mot à dire à travers une participation qui serait simultanément numérique et présentielle.

Score : 0 / Réponses : 0

----

- Proposition de Renaissance Numérique et la Fondation Jean Jaurès

Mise en place d’un portail web et d’une application des services publics
- Ils seraient relais également des concertations publiques par une information descendante personnalisée pour inviter à participer en ligne ou physiquement aux concertations mises en place pour l’élaboration des lois, en fonction des centres d'intérêts et « expertises » signalés par l’utilisateur. 

[Proposition issue du Livre blanc “Démocratie : Le réenchantement numérique” : http://bit.ly/2zAPucV]

Score : 0 / Réponses : 0

----

Pour un vrai mécanisme de financement public des initiatives démocratiques.

L'écosystème des initiatives démocratiques rassemblant les acteurs associatifs tout comme des entreprises privées, souffre d'un manque de financement chronique. Il n'y a pas en France de culture philanthropique privée dans ce domaine et les financements publics sont rares et assez opaques dans leur gestion. Dès lors, les acteurs ne peuvent aujourd'hui assumer pleinement leur responsabilité de reconnecter le citoyen avec le champ politique. Cela pénalise évidemment la bonne coordination et collaboration des acteurs en créant un climat de défiance et/ou suspicion lié à l'origine de leurs financements respectifs.

La suppression de la réserve parlementaire libère des moyens, près de 150 millions d'euros par an, pouvant être affectés en partie à un mécanisme de financement des initiatives démocratiques. Tout comme il existe un mécanisme clair et transparent de financement des partis politiques s'appuyant sur le nombre de voix obtenus aux élections, il est nécessaire de mettre en place un système pour les acteurs du champs des initiatives démocratiques, acteurs incontournables aujourd'hui. 

Les ratios et critères pourront être identifiés et discutés lors de cette consultation et du travail parlementaire qui suivra. Quelques pistes de réflexions: nombre de votes sur les plateformes, nombre de participants aux réunions et ateliers, nombres de signataires de pétitions etc.

Score : 0 / Réponses : 0

----

Programmes de sensibilisation (exemples du Royaume Uni)
Toutes les législatures britanniques ont des services de sensibilisation et d’éducation dont la mission est d’améliorer la connaissance des citoyen.ne.s sur le travail de leur législature et d’encourager l’engagement citoyen avec cette législature. Le travail des services de sensibilisation (outreach) consiste en particulier à expliquer aux citoyen.ne.s et aux organismes comment influencer le travail de la législature par le biais des consultations écrites. Ils organisent ainsi des sessions d’information sur les consultations parlementaires pour les publics intéressés. D’autre part, ils s’efforcent à engager les publics plus marginalisés, qui ont une connaissance limitée du parlement et de ses modes d’accès, en leur proposant directement des informations sur le parlement et les consultations parlementaires. Si ce travail est difficile, il est important afin de développer la diversité des personnes auditionnées par les commissions et ainsi s’assurer de la diversité des points de vue. 

Recommandations :
-	Développer les services de sensibilisation de l’Assemblée Nationale 
-	Proposer des sessions d’information sur la façon de participer aux consultations parlementaires
-	Développer des initiatives afin d’améliorer l’engagement citoyen avec l’Assemblée des groupes marginalisés et leur représentants (personnes en difficulté financières, demandeur.se.s d’emploi, minorités, etc.)

Score : 0 / Réponses : 0

----

Sujet difficile qu'il faut probablement scinder en deux, il y a :
- le choix du sujet
- le mode d'animation du sujet.

Concernant le choix du sujet, il y a les sujets qui vous intéressent et pour lesquels vous n'avez pas de connaissances particulières, ou au contraire vous êtes très pointus Chacun est libre de choisir le sujet qui l'intéresse et s'inscrit au groupe. Dans chaque groupe, un modérateur fait un ménage bienveillant.

Une fois que le groupe a épuisé son sujet (le groupe tourne en rond), on passe à l'étape 2. Le modérateur propose à quelques contributeurs qu'il a repérés, de se réunir (quelque soit le moyen de réunion) pour préparer une réunion qui sera proposée en vidéo. La réunion pourrait être calibrée : 1 h de présentation, discussion entre les participants qui auraient préparé suivi de 30 mn d'interactivité avec les participants à la vidéo.

Le résultat de cette réunion serait le résultat attendu proposé par le groupe des "préparateurs"

Score : 0 / Réponses : 0

----

Je débute et je ne suis pas certain d'étre dans la bonne rubrique , mais j'ai un commentaire à proposer suite à l'actualité de ces derniers jours . N'y aurait il pas 30 députés disponibles pour rendre 1 visite dans les maisons de retraite et autres ? Nous ne vous en voudrons pas de passer aprés les gens emprisonnés , Nous , gens honnétes et travailleurs . Eux qui n'ont peut étre pas travaillés , mais qui sont maintenant logés et nourris gratuitement alors que nous travailleurs et gens honnétes payons jusqu'à  presque 3000 E par mois pour des soins laissants à désirer ( en étant sympa ) et nourris.....Justice dites vous ??
Amicalement votre

Score : 0 / Réponses : 0

----

La participation présentielle implique déplacement, disponibilité, aptitude au débat, connaissance des rites et coutumes de l'instance concernée.
Je ne pense pas que ma présence réelle, matérielle, soit indispensable. L'important est que les bonnes décisions, lois, sortent du Pouvoir législatif.

.. afin que les réclamations des citoyens, fondées désormais sur des principes simples et incontestables, tournent toujours au maintien de la Constitution et au bonheur de tous..

J'ai déjà lu cela mais où?

Score : 0 / Réponses : 0

----

Bonjour et merci pour cette prolongation. Comme il a ete demontré lors de "Reinventez l'Assemblée nationale" le probleme n'est vraiment pas les plateformes numériques mais l'engagement, et la il faut faire appel a toutes sortes de competences d'accompagnement, personnel, coach, je cite par exemple Christine Marsan, Pedagie de l'Alterite et de la cooperation qui a  une vraie experience d'intelligence collective et propose des formations, ou encore Open Source Politics une experience d'accompagnement d'elus et d'administrations,  Enfin ne pas oublier que la fracture numerique concerne aussi les elus, entre les pro-actifs du numerique et les ...ignorants ou opposants, qui vont etre de moins en moins nombreux, il est vrai! Mais il en existe encore bcp trop en milieu rural.( En deplacement j'ecris avec un clavier anglais, désolee pour les accents. ) En conclusion, ne creons pas trop de regles  et de contraintes pour ces profils d'accompagnateurs qui peuvent etre aussi bien des jeunes de 20 ans que des seniors de 70 ans dans un meme village. J'ai suggere aussi une re-definition legale des missions des Espaces Pubics Numeriques qui existent dans chaque commune.

Score : 0 / Réponses : 0

----

A quelques heures de la clôture de la consultation #DémocratieNumérique initiée par la Présidence de l’Assemblée Nationale, on compte environ 1 300 contributions autour de la question centrale ; « en quoi le numérique peut il aider le citoyen à contribuer aux débats législatifs » ?
1300 contribution c’est ce chiffre qui doit nous alerter. Quelle que soit la qualité de ces interventions force est de constater qu’elles sont trop peu nombreuses au regard des 46, 6 millions d’internautes en France .
Le Président de la République s’est engagé à rendre 100 % des services publics accessibles en ligne d’ici la fin du mandat. Il y’ a quelques jours l’Agence du Numérique publiait sur sont site un rapport sur l’action sociale et l’impact du numérique.

« 
 
Il ressort de l’étude que le numérique est bien entré dans les métiers de l’Action sociale, même si des écarts subsistent selon les différents champs d’accompagnement investigués (budget, accès aux droits, insertion socio-professionnelle, logement, hébergement d’urgence, et accueil ponctuel).
•	75 % des professionnels affirment faire les démarches numériques « à la place de » l’usager
•	95 % déclarent que l’accès au numérique se fait très fréquemment par un accompagnement de l’usager.
•	83 % des intervenants sociaux interrogés jugent le numérique indispensable dans leur pratique professionnelle, et 58 % le jugent indispensable dans le parcours d’un usager.
La problématique numérique demeure cependant « le parent pauvre des politiques d’établissements au sein de l’Action sociale ».
•	Moins de 10 % des intervenants sociaux interrogés déclarent ainsi avoir reçu une formation au numérique dans le cadre professionnel ou au cours de leur formation initiale.
•	Seuls 30 % des intervenants sociaux sont en capacité de diriger un usager ayant des lacunes numériques vers un acteur proposant une formation adaptée.
•	Moins de 20 % des structures ont une procédure systématique de détection des difficultés numériques des usagers
Les travailleurs sociaux de terrain, tout comme les directions, subissent la dématérialisation et sont peu outillés pour anticiper sereinement la probable transformation de leur métier induite par le développement du numérique. »
 
Ainsi on estime que 40 % de la population Française n’est pas à l’aise avec les démarches courantes sur Internet. On peut légitiment supposer que pour participer à la fabrique de la loi cette part soit encore plus importante.
Aussi la question que nous devons nous poser est de savoir comment faisons nous pour réduire ce déficit de littératie numérique ? Comment faisons nous pour que construire une société numérique ET inclusive ? Plusieurs initiatives sont déjà en cours et notamment soutenues par la mission Société Numérique de l’Agence du Numérique. Quels sont les résultats de ces expérimentations ?

Quelle politique mettons nous en place pour que 100% des services publics en ligne soient accessibles à 100% des citoyens  que cela soit en terme d’infrastructure ou en terme d’usages ?

Score : 0 / Réponses : 0

----

Pourquoi seulement 1300 commentaires?
1 parce que ce sont les élus qui sont  chargés, délégués pour cela, la citoyen ayant d'autres préoccupations.
2 parce que l'information et les moyens de se faire entendre  ne sont pas connus. 
3 L'habitude millénaire de se faire représenter pas d'autres n'aide pas
c'est pour l'Elysée. Il y a une réponse automatique.
4. Le sujet est complexe et demande soit une connaissance, soit des capacités dont peu disposent.
92% de la population n'est pas éduquée, n'a pas appris à réfléchir.
La société est très hiérarchisée et cela n'est pas prêt de changer tant que la pyramide autoritaire actuelle ne sera pas remise en cause.
L'accès au libre arbitre implique une information pertinente. Noud en sommes loin.

Score : 0 / Réponses : 0

----

Notre expérience du débat est que les débats en présence et à distance se conjuguent parfaitement dans la durée.  Il serait très utile de faire discuter de faire participer à une loi sur l'enseignement  les étudiants et lycéens dans leurs propre sites. L'apport numérique  de la participation numérique est d'apporter plus de transparence, de subsidiarité aux multiples débats accompagnant la préparation d'une loi. Il est très important de disposer d'un "manifeste"  qui est une déclaration de l'assemblée sur le contenu du débat. Ce manifeste se distinguerait des restitutions du débat qui en sont toujours une interprétation biaisée par ceux qui la font.

Score : 0 / Réponses : 0

----

En France, malheureusement, il existe encore des secteurs géographiques non desservies par un service de communication électronique. Ces zones blanches peuvent faire l'objet d'un plan d'action spécifique pour permettre aux populations concernées de participer aux travaux du Parlement.
Des sessions en présentiel pourront être organisées en priorité sur ces sites animées par les députés des circonscriptions en question. Il faudrait assurer une communication via les médias classiques (presse, radio...) et ouvrir un registre d'inscription dans les mairies sur la zone considérée.

Score : 0 / Réponses : 0

----

Des parlementaires pourraient venir régulièrement en région exposer les travaux du parlement et recueillir des commentaires ou interrogations de citoyens, avec pourquoi pas retransmission en direct dans des cinémas. Ces parlementaires seraient de groupes politiques différents, ce qui pourrait permettre une retranscription objective.

Score : 0 / Réponses : 0

----

Utiliser la Chaîne parlementaire !

Score : 0 / Réponses : 0

----

Il faut bien évidemment associer l'IA à cette démarche, les outils purement collaboratifs, ou chacun donne son avis plus ou moins long et plus ou moins élaboré en termes de rédaction seront rapidement ingérables et/ou limités, à mon avis il faut rester sur un mode questionnaire, qui peut séduire et être accessible à un plus grand public, tout en laissant la possibilité d'une expression libre, qui elle devra être analysée par un programme d'IA suffisamment élaboré pour en extraire les concepts et préoccupations des contributeurs pour chaque thématique, la consultation ne peut passer que par un outil performant, et en ligne, pour ceux qui n'ont pas internet, des bornes publiques pourraient être envisagées, dans les mairies par exemple.

Score : -1 / Réponses : 0

----

Cette proposition me pose souci : oui il y a une forme de biais avec la seule consultation numérique.
Mais à contrario, il n'est pas possible sans le numérique actuellement de prendre connaissance des textes en discussion.
Proposer de pouvoir consulter en présentiel des panels d'électeurs n'est possible qu'à la seule condition que ce panel est aussi accès à l'information disponible.
Et il existera le même biais quel que soit la méthode retenue : le choix du panel tiré au sort pose le souci que l'on risque d'avoir dans le panel des personnes n'étant pas du tout concernées par le texte discuté.
Il me semble que si l'assemblée souhaite avoir une meilleure visibilité du grand public sur les textes prochainement en discussion, elle devrait plutôt mettre en place par voie de presse (notamment) des annonces sur les prochains textes en discussion, et la possibilité pour le public de faire parvenir à une commission départementale/régionale une synthèse des contributions.

Score : -1 / Réponses : 0

----

**De vrais outils simples**

Le markdown c'est très bien mais il faut que les plateformes soient accessibles et permettent à tout le monde de s'y retrouver. Pensez à des plateformes WYSIWYG !

Ou les sauts de lignes sont réellement pris en compte, etc. bref à du "word/openoffice like"

Score : -1 / Réponses : 2

----

N'importe quel lieu de service public y compris La Poste (dorénavant société de service de droit privé) doit disposer :
- d'un ordinateur
- d'un personnel d'accueil et d'accompagnement
pour l'ensemble des demandes des citoyens qui expriment le besoin de se connecter.
- Chaque facteur devrait être munis d'un LapTop et d'une appli Service-public dédiée lors de sa tournée afin de répondre aux demandes des citoyens qui n'ont pas internet. C'est un nouveau métier ?  Oui ! et c'est la beauté de la révolution numérique ...

La fracture numérique, c'était sous la présidence de Jacques Chirac, il y a 20 ans !

Score : -1 / Réponses : 0

----

Avec le numérique des milliers de gens peuvent participer . Avant le numérique à peu près personne ne pouvait . C'est tout de même une amélioration à ne pas rejeter bien que tout le monde n'y soit pas à l'aise ( moi par exemple!)

Score : -1 / Réponses : 0

----

Base de données participative "comment faire pour améliorer/simplifier?" questions(besoin daté) / réponses (oui/non quand et pourquoi) au gouvernement ( avec possibilité de voter pour / contre et de commenter) accessible immédiatement par les citoyens, les parlementaires et les ministères. Reformulation synthètique par des modérateurs après interactions avec le demandeur.
Afin d'éviter les doublons,  du travail aux élus et aux ministères, d'améliorer les indicateurs de  délais de réponse(entre la question et la décision puis sa mise en place), d'améliorer la transparence, d'être pratico-pratique (lien depuis et vers servicepublic.fr
Exemple dans un autre domaine : http://www.commentfaiton.com/

Score : -1 / Réponses : 0

----

Certains parlementaires organisent déjà des débats citoyens. Pourquoi ne pas les généraliser?

Score : -1 / Réponses : 0

----

Les Députés perçoivent 5800 euros d' IRFM pour exercer leur mandat correctement. Cette enveloppe doit servir à recevoir aussi le public à travers les permanences ou la location d'un autre local pour ce type d'initiative. 

L'Assemblée Nationale pourrait aussi ouvrir son hémicycle pour une session spéciale réunissant les contributeurs ce qui permettrait d'intéresser davantage le citoyen au Parlement.

Score : -1 / Réponses : 0

----

La French Tech a inauguré son programme French Tech Diversité visant à promouvoir la diversité sociale. Et si dans cet état d'esprit se lançaient vers des publics défavorisés de banlieue ou des publics isolés ruraux... des dispositifs de sensibilisation "Civic Tech Diversité" ?

Score : -1 / Réponses : 0

----

Proposons une CHARTE DE L'OMNICANALITÉ rendant obligatoire la publicité et la possibilité des échanges sur des supports variés et à égalité de traitement.
En effet, pour des raisons de représentativité, il est encore délicat de favoriser le numérique seul au risque du détriment des canaux traditionnels.
Il est impensable que sur la question démocratique l'aisance informatique puisse étendre le fossé numérique et que la fracture numérique puisse étendre à son tour la fracture sociale.

Score : -1 / Réponses : 0

----

M. CASTANER,  porte parole du gouvernement, a annoncé chez JJ.BOURDIN  sur RMC le 30.08.2017: : " la fin d'une époque."  affirmant:
“Et donc aujourd'hui on veut sortir de cette théorie des droits formels 
qui sont sur le papier mais qui ne sont pas la réalité."

Le CLIC (Comité de Liaison pour l'Initiative Citoyenne) a alors demandé au premier ministre de transformer  le droit théorique  de l'article3 de la Constitution: "la souveraineté nationale appartient au peuple" ... en un droit réel en complétant l'article 3 par le référendum d'initiative citoyenne en toutes matières y compris constitutionnelle et de ratification des traités. Il n'a pas encore eu de réponse.
Ce droit réel instaurera  tout simplement une véritable démocratie en France!

M. CASTANER,  porte parole du gouvernement, a annoncé chez JJ.BOURDIN  sur RMC le 30.08.2017: : " la fin d'une époque."  affirmant:

“Et donc aujourd'hui on veut sortir de cette théorie des droits formels 

qui sont sur le papier mais qui ne sont pas la réalité."

Le CLIC (Comité de Liaison pour l'Initiative Citoyenne) a alors demandé au premier ministre de
transformer  le droit théorique  de l'article3 de la Constitution: la souveraineté nationale appartient au peuple ... en un droit réel en complétant l'article 3 par le référendum d'initiative citoyenne en toutes matières y compris constitutionnelle et de ratification des traités. Il n'a pas encore eu de réponse.
Ce droit réel instaurera  tout simplement une véritable démocratie en France!

Score : -1 / Réponses : 1

----

Il faudrait transformé la carte électorale en un passeport électorale : ouvert dé l'âge de 16 ans à toute citoyennes et citoyens français.

Ainsi, dans se passeport électorale les jeunes obtiendront lors de leurs recensement leurs premier droit avant de pouvoir voter à 18 ans (l'âge de maturité).

Le Droit dans se passeport électorale de disposer d'un compte ou seront éditer leurs profit afin par la suite de pouvoir voter, faire des contributions, des propositions et argumenter sur la plateforme numérique du gouvernement.

Ainsi, dans chaque mairie en France, au niveau du service électorale : une borne internet serai mise à la disposition des citoyens ne disposent pas d'Internet et ainsi qu'une aide à travers un conseiller qui pourrait mettre en place des groupe de dialogue

Le Passeport électorale permettra : 
Le Droit à une consultation participative de l'ensemble des citoyens et des citoyennes français afin de recueilli les avis, les attentes et de partager la vision du peuple pour enfin créé cette interaction avec le gouvernement du moment et de nos représentants politique dans cette conception des lois français puisque sous le Principe que cette V ème République est un gouvernement du peuple, par le peuple et pour le peuple

(l'âge minimale : c'est l'âge de recensement afin de pouvoir commencer à faire les premier pas en tant que électeurs).

Score : -1 / Réponses : 0

----

ATTENTION...  tous les dispositifs de participation, d'échanges, de co construction...  doivent être "optimisés" par un volet éducation populaire 2.0 !

Score : -1 / Réponses : 0

----

Dans sa contribution "l'Humanisme numérique, vers un nouvel éveil de l'Humain au monde", le Conseil de développement durable de la métropole du Grand Nancy propose un  panorama des questions que posent les enjeux démocratiques et la place du citoyen dans un monde confronté à la complexité comme à la multiplication des échanges permis par le numérique. Le numérique est un progrès, il peut être un levier qualitatif pour le projet de Société mais à la condition qu'il remplisse des objectifs liés à la notion de Commun (intérêt collectif, gouvernance partagée...)
http://conseildedeveloppementdurable.grand-nancy.org/fileadmin/documents/agora/Fonctionnement/contributions/2016/2016-12-01_Lecture_rapide_Contribution_Humanisme_Numerique.pdf
Rapport complet :
http://conseildedeveloppementdurable.grand-nancy.org/fileadmin/documents/agora/Fonctionnement/contributions/2016/2016-11-23__contribution_finale_Humanisme_numerique.pdf

Score : -1 / Réponses : 0

----

Pour l'information "descendante"il faudrait contraindre, voir imposer, aux députés de tenir à minima une réunion publique par an dans leur circonscription et de publier, comme le président de la république ,leur agenda.
Pour le contact vers le député les moyens actuels me semble suffisants ,sachant que rien ne remplace le contact humain direct.

Score : -1 / Réponses : 0

----

Je souhaite, en préambule, exprimer ma reconnaissance au Président de l’Assemblée Nationale pour avoir initié cette consultation qui répond aux attentes citoyennes des Françaises et des Français. 
La volonté est donc d’offrir, à chacune et chacun de celles et ceux qui le voudront, la faculté de participer activement à la vie politique, en fonction de leurs souhaits et possibilités.
Ma contribution se résumera à la création d’un site informatique dédié à la consultation citoyenne, rattaché au Président de l’Assemblée Nationale, et conjuguée à la création d’un réseau de bénévoles territoriaux, en charge d’assurer, à toutes les personnes non connectées, la possibilité de participer si elles le souhaitent.
Pour le site informatique : 
-	inscription individuelle sécurisée, justifiée par la production de son identité,
-	utilisation d’un identifiant numérique (en exemples : numéro INSEE ou d’inscription sur la liste électorale de la commune de résidence, ou tout autre garantissant le bon utilisateur),
-	indication des thématiques, centres d’intérêt et domaines d’expertise,
-	engagement de se connecter de façon régulière (au moins hebdomadairement),
-	envoi de courriels récurrents aux inscrits, les informant de l’actualité du site.
La plateforme informatique pourrait ainsi permettre :
-	de déposer des pétitions en ligne à l’initiative d’une seule personne ou d’un groupe, et de les faire connaître à l’ensemble des personnes inscrites; la pertinence de la question soulevée en serait rapidement appréhendée,
-	de proposer de lois, des référendums, des modifications législatives, d’intervenir dans le processus législatif, d’évaluer les politiques publiques déclinées, et toutes autres initiatives qui pourraient se révéler utiles à l’usage,
-	de constituer des panels de citoyennes et citoyens pour des questionnements particuliers.

Pour les citoyennes et citoyens non connectés :
-	nomination d’un ou d’une Délégué.e de l’Assemblée Nationale par canton,
-	la fonction serait bénévole (remboursement éventuel des frais de déplacements sur justifications), 
-	la fonction pourrait être assurée, notamment, par des retraités de la fonction publique d’Etat (Ministères de la justice, des finances, de l’Intérieur,….) avec pour mission, d’une part de contacter les personnes non connectées et de leur expliquer le processus de participation, et d’autre part de procéder à leur inscription sur le site et d’assurer leur accompagnement dans son utilisation, 
-	une prestation de serment solennelle pourrait être envisagée,
-	les mairies pourraient mettre un matériel informatique à disposition.

Score : -1 / Réponses : 0

----

la société civile est pleine d'associations, de think tank et de groupes en tous genres qui réfléchissent chacun dans leur domaine d'activité aux impacts des réformes législatives, aux carences du droit, etc.
Il faudrait que le gouvernement ou les parlementaires, en amont de l'adoption des lois ou bien pour évaluer l'impact de réformes législatives déjà adoptées, saisissent ces entités de la société civile pour participer à une étude d'impact d'un texte. Une association de protection de l'environnement n'est-elle pas la mieux placée pour effectuer une évaluation sur une réforme environnementale adoptée depuis quelques années, une association de consommateurs ne devrait-elle pas directement participer à la rédaction d'une étude d'impact ?
Il conviendrait donc de systématiser (loi, charte?) le recours à la société civile remplie d'experts non médiatisés pour l'évaluation et les études d'impact. Cela éviterait d'avoir des évaluations orientées de la part des pouvoirs publics ou des études d'impact des lois qui n'en sont pas...

Score : -1 / Réponses : 0

----

Proposition collective des comités strasbourgeois En Marche ! :

Création d’un avis non-technique (dénommé ci-dessous ANT) permettant une consultation éclairée des citoyens-nes via une explication vulgarisée des projets/propositions de lois. Cet ANT serait élaboré par une commission indépendante et de facto non-partisane. Il permettrait de lister l’impact du projet de loi sur le système juridique en place.
Le tout serait disponible sur un ‘réseau social citoyen’ , une plateforme numérique créée à l’occasion, qui permettrait de « prendre le pouls du citoyen ». Cet outil permettrait de lutter de manière significative contre l’abstention en impliquant le citoyen en continu dans le processus législatif.
Chacun s’y connecterait avec son numéro d’ identifiant citoyen unique.

Afin d’appréhender la faisabilité généralisée sur tous les textes de ce type de consultation, et de préparer le citoyen, il faudrait conduire au préalable un essai sur des textes spécifiques définis en amont des sessions parlementaires. En effet, il a été évoqué l’idée d’encadrer la consultation et de ne pas nécessairement ouvrir celle-ci à tous les textes de lois débattus à l’Assemblée.
Une des priorités sera la lutte contre la fracture numérique. Afin qu’il n’y ait pas de laissés-pour-compte dans ce processus de démocratie numérique, il faudrait un accès possible à cet avis non-technique simplifié dans la permanence du député (voire en documents papiers à disposition dans les mairies par exemple).

Score : -1 / Réponses : 1

----

Le think tank point d'aencrage a publié son rapport Democratie, technologie & citoyenneté  cette année. 
http://pointdaencrage.org/2017/05/09/democratie-technologie-citoyennete-construire-nos-institutions-numeriques/
Parmi les 10 propositions, on trouve 
FORMER LES ACTEURS DE LA SOCIÉTÉ CIVILE ET DES PARTIS
POLITIQUES AU ‘COMMUNITY ORGANIZING’
Encore méconnu en France, le « community organizing » est une forme de mobilisation citoyenne basée sur une échelle de proximité (quartier, canton, village, voire ville) qui, par le développement d’une auto-organisation collective et autonome, vise à créer un rapport de force avec les institutions et à favoriser des approches ascendantes (bottom-up). Originaire
d’Amérique du Nord - c’est par cette méthode que le jeune Barack Obama a découvert l’engagement politique dans les ghettos de Chicago ! - ce mouvement dont la règle d’or stipule « qu’il ne faut pas faire pour les gens ce qu’ils peuvent faire par eux-mêmes » souhaite donc donner du pouvoir d’agir (empowerment) aux premiers concernés par des prises de décisions politiques en matière de logement, d’urbanisme, de services publics, d’emploi…
Le community organizing revêt des formes d’actions et mobilisations traditionnelles (pétitions, manifestations, contre-projets etc.) décidés de façon démocratique sous l’égide “d’organizers” qui se chargent aussi du recrutement de citoyens. En France, le rapport de
Marie-Hélène Bacqué et Mohamed Mechmache qui préconisait une rupture radicale des politiques appliquées dans les quartiers prioritaires s’inspirait du community organizing avec une volonté de donner concrètement du pouvoir à des conseils de quartiers (budget, avis sur les rénovations etc.). Alors que la mobilisation partisane s’épuise et que les associations sont souvent limitées à un champ de spécialisation, la formation et la mise en pratique des techniques de community organizing permettrait à la société civile de dépasser les constats de désillusion démocratique et de s’engager concrètement.

Score : -1 / Réponses : 0

----

Mise en place d'un "Conseil Citoyen" composé d'habitants de la circonscription ayant fait acte de candidature (tirage au sort dans l’éventualité d'un trop grand nombre de candidatures).
Les membres du "Conseil Citoyen" seraient amenés à:
- réfléchir sur la démocratie numérique au regard des institutions.
- s'exprimer,  débattre pour amender les projets de loi
- s'informer et contribuer au travail législatif de leur député(e)
- s'engager sur des dossiers et enjeux locaux.

Score : -1 / Réponses : 0

----

Je propose de monter des "colocations citoyennes" permettant à des jeunes de tous milieux de vivre ensemble au long de l'année en étant parrainés par des députés et en menant une réflexion intimement liée à la vie politique d' "En-Haut". Ces îlots seraient alors des centres dynamiques pour que les jeunes, sur le terrain, se réinvestissent en politique

Score : -1 / Réponses : 0

----

Déléguer à plusieurs associations (de jeunesse, de soutien aux démunis, etc...) l’organisation d’Universites Populaires Citoyennes et Politiques dans toute la France avec l’objectif affiché d’aller chercher les plus exclus et les plus éloignés.

Score : -1 / Réponses : 0

----

- les difficultés liées à la fracture numérique sont réelles : on pourrait prévoir une plateforme en Mairie pour permettre un accès au plus grand nombre
- par ailleurs la multiplication possible des profils et adresses pour une seule et meme personne peut nuire à la rigueur et l'impartialité de la consultation
Pourrait on travailler à partir d'un numéro d'identification unique (ex le num de sécurité sociale, numero fiscal .. ?) pour permettre à chacun d'intervenir de façon plus sûr ?

Score : -1 / Réponses : 0

----

Pourquoi pas des "battles d'idées interactives" entre citoyens organisées dans l'espace public avant l'examen des textes de loi ? Un format léger et innovant pour crowdsourcer et partager (via les réseaux sociaux) des propositions/arguments émis par des citoyens généralement éloignés du débat public. Voir un exemple de #LiveAgora par ici : bit.ly/LiveAgoraShort

Score : -2 / Réponses : 1

----

il faudrait utiliser le numérique pour permettre aux citoyens d'être mieux informés et éventuellement donner un avis....

Score : -3 / Réponses : 0

----

Afin de lier la participation numérique et physique il faudrait que les mairies ou les préfectures puisse recevoir les contributions sous format papier et l’envoyer à l’Assemblée nationale à un service compétent.

Pour l’impartialité et la rigueur des consultations je propose que soit mandaté parlementetcitoyens.fr

Score : -10 / Réponses : 1
