## Quel rôle pour les citoyens dans l’élaboration et l’application de la loi ?

### Consultation pour une nouvelle Assemblée nationale

# Interactions avec la procédure législative

<div style="text-align: justify;"><img src="https://consultation.democratie-numerique.assemblee-nationale.fr/an/interactions.png"></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">La participation ne se limite pas à la consultation en amont de l’examen des textes par le Parlement. Elle peut également porter sur la période pendant laquelle le Parlement délibère des textes dont il est saisi.</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">Pour l'heure, les citoyens qui le souhaitent ne peuvent faire valoir leur point de vue au cours du débat législatif qu'en écrivant à leurs parlementaires ou en manifestant. De nombreux outils pourraient être imaginés afin de favoriser une interaction entre les citoyens et les élus pendant le processus législatif (accès à l’ensemble des documents des Assemblées, auditions publiques, amendements citoyens, interaction avec les élus au moment de l’examen en commission ou en séance, droit de veto citoyen, budget participatif...).</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">&nbsp;</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">Les députés peuvent également organiser des ateliers législatifs citoyens en circonscription.</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">&nbsp;</span></div>
<div><i style="font-size: 11pt;"><span style="color: rgb(107, 36, 178);">Quelles procédures inventer pour faire davantage intervenir les citoyens qui le souhaitent dans l'écriture de la loi quand elle est discutée au Parlement ?</span></i></div>
<div><br></div>
<div><span style="font-size: 10pt; color: rgb(230, 0, 0);">Nota bene</span><span style="font-size: 10pt;">&nbsp;: par défaut, les contributions les plus populaires apparaissent en tête de la page par ordre décroissant. Vous pouvez si vous le souhaitez afficher les contributions les plus récentes ou les plus anciennes. Pour cela, modifiez le mode de </span><b style="font-size: 10pt;"><u>classement</u></b><span style="font-size: 10pt;"> à droite de «&nbsp;Commentaires&nbsp;» avant la 1ère contribution.</span></div>

#### Commentaires

Le moyen le plus simple de faire participer les citoyens au Parlement, c'est de les inviter à y entrer.

[L'an dernier au Canada](https://www.canada.ca/fr/campagne/comite-consultatif-independant-sur-les-nominations-au-senat/rapport-processus-permanent-juillet-decembre-2016.html), tout citoyen a pu déposer sa candidature pour être nommé au Sénat, et c'est un comité indépendant qui a sélectionné les personnes les plus aptes et méritantes.

La France devrait s'inspirer de cet exemple. Je propose d'**introduire une dose de tirage au sort aux sénatoriales**, qu'une partie des prochains sénateurs soient tirés au sort parmi des citoyens volontaires et jugés aptes selon un comité scientifique non partisan.

Ce serait la démonstration que la fabrique de la loi n'est pas réservée aux élus mais bien l'affaire de toutes et tous.

Score : 146 / Réponses : 20

----

Le citoyen doit pouvoir être force de proposition dans la loi, pour cela il devrait pouvoir soumettre des **Amendements Citoyens**.
En effet lors des discussions en commission, on peut être exaspéré par certains amendements, tout autant que l'on peut ressentir l'envie de faire entendre sa voix.

Pour ce faire, il faudrait : 

1) Créer une plateforme sur laquelle serait référencé l'ensemble des articles de lois qui vont être discutés en audition publique dans la semaine ou sur une période à définir.

2) Chaque citoyen (selon des conditions à déterminer, p.e. avoir ses droits civiques, être français, etc.) pourra proposer son amendement dans ses propres mots pour chacun des articles discutés.

3) Dans chaque commission des députés (des **"Rapporteurs citoyens"**) seraient chargés de filtrer et sélectionner les amendements les plus pertinents pour les soumettre à réécriture auprès des services juridiques de l'assemblée.

4) Durant les discussions sur les articles en question, ces mêmes députés porteraient ces amendements en mentionnant leur qualité d’amendement citoyen.

Beaucoup de critères sont à déterminer notamment vis à vis des conditions de soumission d'un amendement et du porte-parole mais il est à mon avis le meilleur moyen pour faire participer le citoyen directement au sein des commissions.

Score : 124 / Réponses : 9

----

**Les plateformes de consultation publique en ligne doivent être basées sur du logiciel libre**
  
*« La qualité des outils logiciels – plateformes dédiées, messagerie, outils collaboratifs... – est déterminante pour l'exercice. [...] Le logiciel doit, de préférence, être un logiciel libre dont le code soit accessible et puisse ainsi être audité par le public. Les choix méthodologiques de la consultation doivent être publics et doivent pouvoir être discutés. »*
Consultations ouvertes sur internet organisées par les administrations; un instrument précieux au service de la participation du public qui requiert une forte implication des organisateurs, [Novembre 2016, COEPIA](https://cdn2.nextinpact.com/medias/coepia_consultations_ouvertes_sur_internet_2016.pdf).
  
Seule l'utilisation de logiciel libre, intrinsèquement transparent et donc auditable par tous, peut offrir un niveau de confiance suffisant en préservant contre toute possibilité de manipulation. L'outil conditionne l'usage ;
*« Le code est loi »* disait Lawrence Lessig en 2001.  
  
Lire en ce sens ce billet de Regards Citoyens : [Le numérique ne pourra aider la démocratie sans en adopter les fondements](https://www.regardscitoyens.org/civic-tech-ou-civic-business-le-numerique-ne-pourra-pas-aider-la-democratie-sans-en-adopter-les-fondements/).
  
Par ailleurs, le modèle de gestion horizontale des logiciels libres s'inscrit parfaitement dans l'esprit d'une démocratie plus participative et plus proche des citoyens.

Dans cette objectif , garantir la possibilité de partage, de réutilisation et de reproductibilité des logiciels de consultation publique en ligne semble particulièrement opportune. On pourra ainsi imaginer des initiatives citoyennes locales, ainsi que des collectivités, qui pourraient avoir accès à des outils puissants, de confiance, et bénéficiant d'un investissement mutualiser.

Score : 96 / Réponses : 1

----

Faire travailler concrètement ensemble citoyens et députés

Créer une plateforme en ligne pour permettre aux citoyens de pouvoir déposer des propositions d’amendement en amont de l’examen des textes, aussi bien en commission qu’en séance. Ces amendements pourraient faire l’objet d’un vote des internautes et devraient être repris ou cosignés par les député.e.s pour pouvoir être examinés.

Un tel système permettrait d’ouvrir la décision à la créativité citoyenne tout en respectant la démocratie représentative : in fine, ce sont les députés qui déposent les amendements et les votent. Seulement, l’horizon de leurs possibles s’élargit avec un tel outil.

Par ailleurs, un tel système permettrait de ne pas surcharger les services de l’Assemblée nationale qui ne sont pas calibrés en termes d’effectifs et de moyens pour gérer des amendements citoyens à examiner sans filtre préalable.

Score : 71 / Réponses : 10

----

Il serait intéressant que les rapporteur(e)s des projets et propositions de loi (et même tous les députés participant à la réflexion sur un texte) utilisent fréquemment la pratique des Ateliers Législatifs Citoyens (ALC), voire que cette pratique soit généralisée. Ces Ateliers ont été mis en place par la députée de la 4e circonscription de Saône-et-Loire (Cécile Untermaier) et consistent, en amont de l’adoption d’une loi, à réunir les citoyens pour discuter de l’intérêt du texte, des raisons de son adoption et surtout des moyens de l’améliorer. 

Avec des citoyens directement intéressés par l’objet du texte, on réfléchit plus concrètement aux répercussions d’un projet de loi sur la vie pratique des individus et on peut l’améliorer. Le député qui anime cet atelier porte ensuite des amendements à l’Assemblée nationale en fonction de ce qui s’est dégagé des réflexions des citoyens. cette pratique constitue un procédé de démocratie participative intéressant car il donne la parole aux citoyens sans déposséder l’élu de son pouvoir législatif puisque c’est lui qui sélectionne et défend les amendements au projet de loi. De ce fait, ce même projet de loi, par l’action des députés, se trouve davantage relié aux citoyens là où les textes issus du Gouvernement sont parfois plus technocratiques et éloignés des préoccupations ou réalités citoyennes. 

Il faut donc promouvoir ces ateliers afin d’en généraliser la pratique dans les circonscriptions car ils garantissent une meilleure participation des citoyens et une meilleure représentations des députés.

Score : 54 / Réponses : 1

----

**Mettre en place des Assemblées représentatives par domaine sociétal**. 

Notre projet vise deux objectifs : 1/garantir une place à l’ensemble des composantes de la société dans le processus législatif et 2/ assurer les conditions d’une véritable représentativité. 

Nous proposons ainsi la création d’assemblées représentatives par domaine sociétal (Assemblée de la Santé, de l’Education, de la Justice, du Travail, de la Défense etc.) intégrées de façon systématiques dans le processus législatif (avis conforme) ainsi qu’en amont (elles pourront être force de proposition). 

Compte tenu du faible niveau de représentativité des organisations syndicales professionnelles comme des associations d’usagers, et afin de donner une voix à l’ensemble des composantes de la société, chacune de ces assemblées serait représentative des différentes composantes syndiquées et non syndiquées **des professionnels ainsi que des usagers** (associations et collectifs déjà structurés) et représentants de la société civile qu’elle concerne à travers un dispositif mêlant représentativité proportionnelle et tirage au sort. 

Notre projet reprend dans ce sens l’objectif initial du CESE en en renforçant le poids par une consultation systématique agrémenté d’un dispositif **d’avis conforme** ainsi qu’en en réformant la composition pour assurer une véritable représentativité. 

**Comment organiser une véritable représentativité ?**

Chaque assemblée représentative par domaine sociétal sera composée :  
-	des organisations structurées (syndicats, associations) qui désigneront pour chaque assemblée leur représentant en respectant la parité et une répartition équilibrée par tranche d’âge et région d’origine ;
-	des représentants des composantes non structurées (professionnels non syndiqués, société civile) qui seront désignés par tirage au sort avec une répartition équilibrée en fonction du sexe, des tranches d’âge et des régions d’origine. 
Chaque assemblée disposera d’une déclinaison régionale dont l’objectif sera de discuter des problématiques locales et d’adapter aux réalités de terrain les textes nationaux. Elles auront aussi pour mission de faire remonter les initiatives et idées émanant des régions. 
La responsabilité et l’engagement des membres de ces assemblées seront garantis à travers plusieurs mécanismes détaillés dans le projet complet (notamment l’obligation de statuer rapidement et des mandats renouvelables une seule fois). 

**Quelle place pour ces assemblées par domaine sociétal dans le processus législatif ?**

Allant au-delà du simple avis consultatif (tel que la Conférence nationale de santé ou le CESE), ces assemblées par domaine sociétal devront être consultées par le gouvernement et le parlement sur toutes questions les concernant afin d’émettre des avis conformes avant légifération définitive. 

En amont, elles pourront également être force de proposition et s’organiser en groupes de travail par thème. En aval, elles seront en charge de mettre en place l’évaluation de la politique concernée afin d’en assurer la cohérence à long terme.
Les technologies numériques occuperont une place centrale dans ce dispositif (mise à disposition des informations, interactions facilitées etc.).

En s’intégrant dans le processus législatif, les assemblées par domaine sociétal aideront de façon constructive les tutelles à faire les réformes nécessaires. Cette organisation donnera une légitimité nouvelle aux décisions publiques et permettra aux parlementaires de se concentrer sur les grandes orientations stratégiques de politique générale. Elle permettra enfin de dépasser la crise de la représentation en mettant le citoyen au cœur du projet démocratique. 

La mise en place de cette réforme nécessite au préalable la création d’une instance représentative dédiée (de type « Assemblée des institutions ») composée sur le mode proposé ci-dessus, qui finalisera le projet de réforme constitutionnelle.

**Pour lire le projet dans sa totalité, merci de cliquer ici** :  https://drive.google.com/file/d/1WazbSBhBXWag2BppeGKLZfpbM5PWoTaR/view?usp=sharing

Score : 39 / Réponses : 3

----

CRÉER UN SITE DU SUIVI DU PROJET DE LOI

Le site de suivi de l’avant-projet de loi [http://consultation.democratie-numerique.assemblee-nationale.fr/topic/59d7347212406bdd5d6277f7#comment-59f73030e8a9656c1a8e73c0] permettra, ensuite, de suivre son examen au Parlement. Cela donnera aux citoyens de réels moyens d’intervenir dans la procédure législative.

Aujourd’hui, des initiatives privées existent, qui offrent un suivi du débat législatif.

Il s’agit d’institutionnaliser cela, en prévoyant un site officiel (du type .gouv.fr) par projet de loi élaboré, rattaché aux services du Premier ministre. Il n’offrira pas le suivi des débats eux-mêmes, accessibles depuis les sites des assemblées, mais bien des moyens aux citoyens pour intervenir dans la procédure législative, dans des conditions fixées par la loi.

À partir de cette plateforme, ils pourront soumettre des amendements qui, s’ils sont soutenus par un certain nombre d’électeurs, devront soit être relayés par un rapporteur (spécialement désigné, par exemple), soit être directement examinés par la commission saisie du texte. Enfin, au cours d’un délai et dans des conditions fixées par la loi, ils pourront demander à ce que la loi adoptée par le Parlement fasse l’objet d’une ratification référendaire, afin de confirmer ou d’infirmer le choix du Parlement.

Autres propositions dans le cadre de cette consultation : http://constitutiondecodee.blog.lemonde.fr/2017/10/30/participation-numerique/

Score : 34 / Réponses : 2

----

Révision de la constitution: Pour toute modification de la constitution ou des traités européens, un recours au Referendum d'approbation devrait être obligatoire. (En respectant le résultat contrairement au référendum de 2005 bafoué par les traités de Lisbonne de 2007).

Score : 29 / Réponses : 0

----

Comme on ne peut rien s'interdire et innover, pourquoi ne pas passer au budget participatif ? Ce principe intéresse déjà les citoyens au niveau de leur commune. On pourrait, dans le domaine associatif où la réserve parlementaire a été bien souvent utile malgré une attribution qui manquait de transparence, expérimenter une participation citoyenne pour la répartition d'une enveloppe sur le budget de l’État. Par exemple, une dotation de l’État devrait être réservée aux associations et la répartition des crédits, faite par un jury de citoyens tirés au sort sous l'égide du sous-préfet et du député. Ce serait un vrai changement dans la méthode de travail et qui mieux que les citoyens peuvent juger des associations ?

Score : 24 / Réponses : 1

----

Droit de véto-référendum citoyen : 

Les députés sont censés être les représentants de la Nation. Or, peut-on encore parler de représentation lorsqu'une loi est votée à l'ombre de manifestations exprimant le désaccord d'un grand nombre de la population ? 
Dans ce cas, force est de constater que les députés n'agissent pas dans leur fonction de représentant de la Nation et que la loi ainsi votée n'exprime aucunement la volonté générale... 

Afin de corriger ce problème, dont les conséquences sont regrettables (notamment, désintérêt de la chose publique par les citoyens), il convient de donner un outil aux citoyens afin de sanctionner la trahison du mandat conféré aux élus. 

Un droit de véto-référendum devrait permettre aux citoyens de s'opposer au vote d'un projet ou d'une proposition de loi. 
Ce droit devra être exercé par un nombre déterminé de citoyen (500 000* me semble correct). 
Si 500 000 signatures ont été recueillies en ce sens, l'effet serait qu'un référendum devra être organisé pour savoir si les électeurs souhaitent ou non que le projet ou la proposition de loi en cause soit retiré(e). 

En outre, ce droit ne sera pas limité dans le temps, de manière à ce qu'il puisse être exercé de nouveau si un(e) autre projet ou proposition de loi venait à avoir le même objet. 

Ce nouvel outil a au moins quatre avantages :
-1) Permettre de s'opposer à un projet ou à une proposition de loi qui n'emporte pas la conviction du plus grand nombre. 
-2) Ne pas être trop radical en ce sens qu'il faille organiser un référendum.
-3) Réconcilier les citoyens à la chose publique (et non pas seulement les électeurs puisque le véto ne serait pas ouvert qu'aux seuls électeurs).
-4) Encourager l'exercice du droit de vote, puisque s'il est vrai que l'exercice du véto est ouvert à tous, la participation subséquente au référendum est conditionnée à une inscription sur les listes électorales. 
=> De cette manière, on s'engage sur la voie d'un renouvellement des institutions en pleine anomie, en associant les citoyens, tout en gardant intactes les prérogatives des parlementaires. 

(Si l'on veut rendre symbolique la participation citoyenne, il faudrait ajouter un Titre XVII à la Constitution qui s'intitulerait "De la participation citoyenne" et recueillerait toutes les dispositions éparses actuelles, notamment sur le référendum d'initiative partagée de l'article 11, les assemblées locales...)

* 500 000 signatures : dans d'autres pays, le référendum d'initiative populaire existe et est très abouti en ce qu'il permet l'abrogation d'une loi, avec ce fameux seuil de 500 000 signatures (Italie par exemple). En Suisse, c'est 50 000, mais la Suisse a une culture éminemment démocratique qui permet à mon sens un nombre aussi faible de signatures, ce qui n'est évidemment pas le cas de la France... 
L'on pourrait également prendre l'exemple de la Californie qui fixe un pourcentage, soit 5% des électeurs ayant participé à l'élection du Gouverneur.

Score : 21 / Réponses : 11

----

Le système des jurés d'assises est bien connu de tous les français et ancré dans le système judiciaire. Ces citoyens, tirés au sort, sont pour le temps d'un jugement, juges à part entière aux côtés des magistrats professionnels.  Ne pourrait-on pas s'inspirer de ce système pour faire entrer à l'Assemblée nationale des citoyens tirés au sort  et les faire participer à la procédure législative? Il ne s'agirait pas d'être citoyen député pendant 5 ans mais de participer durant un temps à l'élaboration de la loi, aux côtés des députés élus. Le système pourrait être expérimenté avec les projets de lois de finances car tous les citoyens sont très intéressés par la question de l’impôt et de la dépense publique.

Score : 19 / Réponses : 1

----

**Créer une procédure pour les lois d'intérêt citoyen**

Certaines lois sur des grands thèmes sociétaux comme Hadopi, le Grenelle de l'environnement, le Mariage pour tous ou encore les lois Travail, attirent parfois une forte attention des citoyens, qui s'intéressent alors massivement au fonctionnement législatif du Parlement. À ces occasions, de nombreux citoyens découvrent pour la première fois le fonctionnement quotidien des assemblées et de nombreuses incompréhensions peuvent se créer.

Afin de pouvoir profiter de l'intérêt des citoyens pour expliquer le fonctionnement législatif du Parlement, il faudrait ajouter à la Constitution une procédure de « loi d'intérêt citoyen » : une fois qu'un certain nombre de citoyens, par exemple 50 000, aura notifié son intérêt pour un Projet ou une Proposition de Loi à l'ordre du jour, et avant son premier examen en commission, la procédure d'intérêt citoyen se met en œuvre.

L'activation de cette procédure déclencherait les dispositions suivantes :
- abandon le cas échéant de la procédure accélérée pour ce texte ;
- suspension de tous les autres travaux des assemblées, notamment des commissions, durant l'examen du texte en hémicycle pour permettre à tous les parlementaires de participer aux débats sur ce texte ;
- suspension de tous les travaux parlementaires pendant par exemple une semaine avant l'étude du texte à l'Assemblée, afin de permettre aux parlementaires de rentrer en circonscriptions et discuter du texte avec les citoyens de leurs circonscriptions.

Un certain nombre de modifications non constitutionnelles devraient également être mises en place, comme la retransmission des débats sur les chaînes parlementaires, l'organisation d'une consultation dont le dépouillement précédera les auditions des rapporteurs, ou encore des modalités étendues d'audition des acteurs de la société civile par les rapporteurs, par exemple en hémicycle afin de permettre à l'ensemble des élus de prendre connaissance des enjeux présentés.

Cette proposition vise à rétablir le contact entre le travail parlementaire et les citoyens sur les dossiers les intéressant tout particulièrement. Sur ces sujets, le Parlement et la démocratie gagneront à prendre le temps de se mettre au rythme des citoyens qui ne s'intéressent qu'occasionnellement au travail législatif.

Score : 19 / Réponses : 2

----

Mettre en place le référendum abrogatif.

Il concernerait une disposition ponctuelle (une loi, par exemple) dont les citoyens pourraient obtenir l’abrogation par referendum.

Score : 16 / Réponses : 4

----

* Contribution collective Café Citoyen : Présidée par Mme Nadia HAI Députée de la   11ème circonscription des Yvelines *

**78-11-Q3 Une participation prévue, budgétée et une pédagogie de tous les instants**

Intro : Les recommandations et propositions des contributions 78-11 Q1 et 78-11-Q2 s'inscrivent pleinement dans cette question, le champ d'application couvre les textes sur les initiatives citoyennes, les projets de textes législatifs, les amendements y compris le Droit d'Amendement Citoyen proposé à la présente réflexion.
Le droit de véto citoyen, n'a pas été retenu comme pertinent et considéré comme un fort risque d'immobilisme par notre groupe de réflexion.
Une Commission d'assistance aux initiatives citoyennes (au-delà d'un assistant virtuel type FAQ, tutoriel ou intelligence artificielle) un pôle *d'écrivains publics* ou plutôt de *sachant législatif* pourrait être associé à la plateforme d'initiatives citoyennes pour permettre au citoyen lambda de mieux comprendre le travail législatif et de mieux formaliser ses propositions.
Prévoir un budget pour la ou les consultations citoyennes, pour la Commission d'assistance aux initiatives citoyennes, voire pour les auditions des citoyens et leurs implications en commissions.
Instaurer pour les jeunes un Service Civil Volontaire avec des fonctions officielles de représentations et participations au travail parlementaire.
Construire et diffuser des fiches de vulgarisation des projets de lois et des lois ou textes votés pour permettre aux citoyens de se sentir impliqués.
Former les futurs citoyens et les profs à *l'éducation civile, citoyenne et enjeux du développement durable* en tant que matière majeure à part entière (pas seulement 1h/ semaine au collège), et non plus comme un chapitre de l'histoire-géo. Et ceci dès l'école, au collège et au lycée. 
Pratiquer et organiser la pédagogie sur les textes votés pour permettre l'adhésion à l'impôt et l'acceptation de l'intérêt commun (comme au Danemark). 
Comme pour les instances actuelles, définir le domaine législatif réservé aux citoyens : certainement les questions de Société, sociétales et environnementales.

Score : 16 / Réponses : 0

----

Je suis toujours surpris par le méconnaissance de la vie réelle qui se manifeste à travers de nombreux textes et prises de position. On ne peut pas en vouloir au législateur de ne pas être omniscient mais peut être cette initiative pourrait elle déboucher sur la mise en œuvre d'un dispositif d'appel à témoignages.
Nombreux sont les cadres d'entreprise sachant mieux que quiconque comment sont prises les décisions qui impactent directement l'emploi ou le montant des impôts payés. 
Nombreux sont les "petits investisseurs", lassés de se voir abuser par des produits d'investissement conçus pour le seul bénéfice de leurs émetteurs, à s'être tournés vers l'investissement locatif sans pour autant en tirer des profits considérables. 
Nombreux sont les conseillers de pôle emploi qui savent mieux que personne la réalité de la situation des demandeurs qu'ils côtoient.
Nombreux sont ...
Combien d'entre eux sont consultés par les parlementaires au moment de prendre position ?
La liste est longue de ces "sachants" de terrain qui pourraient être utiles pour comprendre la réalité de la société d'aujourd'hui bien que mieux qu'à travers les rapports et les discours de hauts fonctionnaires et de ministres qui, quelques soient leurs qualités et leurs bonne intentions, n'ont qu'une vision globalisante, accordant une trop grande importance aux extrêmes et à une moyenne très virtuelle.

Il suffit de regarder les quelques vidéos qui illustrent les états généraux de l'alimentation pour voir à quel point les moyens utilisés pour comprendre sont inappropriés : comment savoir, comment comprendre en n'interrogeant que les dirigeants d'entreprises qui refusent de répondre aux questions ou qui n'y apportent que des réponses infiniment politiquement correctes ?

Score : 11 / Réponses : 2

----

L'utilisation des ateliers législatifs citoyens en circonscription me paraît être une bonne chose.
Je suis pour un système d'amendement citoyen majoritairement en commission qui est l'endroit ou le texte se décide et où le débat est le plus important.
On pourrait envisager un tirage au sort de citoyens souhaitant s'investir dans la vie locale sans représenter un parti.
Cette désignation pourrait être tournante de façon à renouveler régulièrement les participants. On peut également envisager une plate forme citoyenne, qui pourrait se créer à partir par exemple de France connect.
Enfin, l'utilisation du référendum pourrait être une chose utile, la consultation des citoyens ou citoyennes sur une question sensible pouvant se faire à moindre coût via les outils numériques.

Score : 10 / Réponses : 2

----

Tirer les sénateurs au sort. Toute personne tirée au sort peut refuser de siéger, on procède dans ce cas à un nouveau tirage.

Seuls [26% des Français](voter davantage sur des propositions que sur des personnes) rejettent cette idée (54% sont d'accord ou très d'accord).

Score : 8 / Réponses : 7

----

Créer le LégaLab, un service parlementaire de la donnée, ouvert aux élu-e-s et citoyen-ne-s pour multiplier les usages autour des données publiques parlementaires et soutenir les parlementaires dans leur travail d’instruction 

Les données générées en permanence par le travail parlementaire, stockées par les services des deux assemblées, sont nombreuses et variées. Ces données, à la condition qu’elles circulent et qu’elles soient facilement exploitables, notamment par les collaborateurs parlementaires, peuvent participer de l’amélioration du travail des élus. 

Le CNNum propose la création d’un « LégaLab », un service parlementaire de la donnée ouvert aux élu-e-s et aux citoyen-ne-s afin de promouvoir la place stratégique des données dans la transformation numérique du Parlement. À l’image de la mission Etalab dans les fonctions ministérielles d’État, ce service serait chargé de :

Accélérer l’ouverture des données publiques parlementaires sous un format ouvert, lisible par une machine et librement réutilisable (open data), notamment en répertoriant et nettoyant les jeux de données utiles, puis en assurant leur accessibilité à l’ensemble des citoyens en tant que « communs » ;

Soutenir le travail d’instruction des parlementaires : ce service pourrait mettre régulièrement à disposition des métadonnées sur les consultations, auditions, documents de travail des commissions, amendements déposés, afin d’améliorer sur le long terme la recherche d’informations et la performance parlementaire. Le LégaLab pourrait être également sollicité en amont de chaque examen d’un texte de loi par le rapporteur compétent pour émettre un avis sur les données utiles au débat public ainsi que leur état de disponibilité et de fraîcheur ;

Animer une communauté d’usage autour des données parlementaires : mise en place de procédure simplifiée pour demander l’ouverture de jeux de données (à l’instar du projet DODOdata.io de Datactivist), organisation de hackathons et soutien à des projets s’appuyant sur l’utilisation des données parlementaires (« startups de parlement »).

Source : https://cnnumerique.fr/wp-content/uploads/2017/07/Avis_CNNum_Confiance_Vie_Publique_vF.pdf

Score : 8 / Réponses : 1

----

l’élaboration du décret relève du gouvernement.
Quelquefois, le décret est inapplicable ou incomplet ou mérite d’être modifié à l’usage. Cela rentre pour moi dans le domaine du contrôle de la loi par le parlement
Le citoyen devrait pouvoir s'exprimer à ce moment sur une plateforme.

Score : 7 / Réponses : 0

----

Développer des outils pour améliorer le traitement et l’intégration des contributions citoyennes au sein des travaux parlementaires.

Traditionnellement, les parlementaires disposent de trois sources d’information principales lors de l’examen d’un texte de loi - l’expertise de l’administrateur, les rapports et autres sources documentaires, les auditions - auxquelles s’ajoutent de plus en plus les contributions citoyennes. Or il manque d’outils et de méthodes pour automatiser le traitement de ces contributions, leur donner de la cohérence d’ensemble de propositions, en extraire des données et des propositions pour améliorer la qualité du travail parlementaire. 

De nouveaux outils numériques au service du travail parlementaire sont à développer, notamment pour naviguer dans les bases documentaires, extraire et classer les passages utiles, équiper la lecture et la comparaison des amendements, intégrer les contributions citoyennes, ou encore modéliser les impacts d’une réforme. 

Pour le CNNum il est essentiel de garantir l’égalité d’accès aux services d’information de tous les parlementaires et de tous les groupes. Pour ce faire, il est nécessaire de renforcer les services généraux des deux assemblées (informatique, archives, assistance aux usagers) et de s’assurer de l’adéquation des outils et de l’interfaces aux besoins exprimés par les élus, leurs collaborateurs et les administrateurs parlementaires (UX design ).

Score : 7 / Réponses : 0

----

Voici une idée (qui n'est pas de moi, mais qui vient d'une discussion avec des amis), qui va à l'inverse de la plupart des propositions que j'ai lu ici : serait-il envisageable que en plus de proposer aux citoyens de se faire entendre par l'assemblée, on permette à l'assemblée de demander une contribution aux citoyens ?
Dans certains cas spécifiques, par exemple sur demande d'un député soumise au vote a l'assemblée, le question en débat serait adressée aux citoyens via la même plateforme que celle permettant aux citoyens de s'exprimer.

Ca soulève pas mal de questions :
- quels critères et quels seuils pour déclancher une telle procédure ?
- comment faire pour que le public répondant à la question soit le plus représentatif possible, et pas seulement une minorité de personnes informés ou impliquée sur la plateforme ?
- quelle valeur donner à la réponse apportée ?
- y a-t-il un risque de ralentir trop le travail de l'assemblée, et si oui, comment le palier ?

Malgré ces questions, je trouve cette possibilitée interessante, j'y vois plusieurs intérêts:
- une telle démarche pourrait sans doute contribuer à augmenter le nombre d'ateliers légistatifs citoyens si la plateforme propose les outils nécessaires, et donc à augmenter le dialogue entre les députés et ceux qu'ils représentent ;
- selon le moyen utilisé pour diffuser la question, ça pourrait peut-être donner envie à plus de personnes de s'impliquer : ce n'est plus au citoyen de faire la démarche active d'aller prendre contact pour s'impliquer, mais le "monde politique" l'invite à donner son avis.
- Si la plateforme utilisée pour répondre à la question rassemble tous les outils numérique de dialogue dont on discute dans cette consultation, elle deviendra connue par plus de personne, et s'en trouvera renforcée à chaque utilisation.

Qu'en pensez vous ?

Score : 6 / Réponses : 2

----

"Un citoyen mieux informé permet une meilleure démocratie"
Nous avons pu constater que les citoyens après l’élection de leurs représentants se retrouvaient très rapidement délaissés. La démocratie représentative telle que nous la connaissons, bien que portant ses fruits en permettant aux citoyens de s'exprimer d'une seule voix, a de plus en plus tendance à les éloigner du processus d'élaboration et de vote de la loi. Antoine Loisel, étudiant en droit a ainsi remarqué que l'assemblée nationale n'offrait pas suffisamment de transparence lors de la procédure législative, il a pris pour exemple le fait que la procédure à l'assemblée nationale se déroule majoritairement à huis clos et seules les séances de questions au gouvernement sont accessibles par les citoyens. Qu'en est-il de l’accès à l'ensemble des documents et des commissions ? Un citoyen mieux informé n'est-il pas un citoyen mieux intégré ? Ne faut-il pas envisager un essor de la démocratie directe au sein de notre Vème république ?
Nous proposons ainsi :
I. Afin de mieux informer les citoyens

- Un accès à l'ensemble des documents du parlement (avec cependant une restriction pour les dossiers sensibles) ainsi que des explications pour permettre aux citoyens de prendre position sur les choix effectués par leurs représentants. Un site web peut être créé à cette occasion. Nous pouvons prendre pour exemple les organes européens(1°) qui ont un devoir de transparence vis-à-vis des citoyens(2°).
- Une mise en avant des étapes de la procédure législative au travers des réseaux sociaux en temps réel. Exemple : (Commission économique : Le député M.X a pris la parole en posant la problématique suivante : "..."). Les citoyens sauront ainsi ce qu'il se passe au sein des commissions pour commencer à réfléchir...

II. Afin de développer la démocratie directe

- Une sensibilisation de la jeunesse et des personnes âgées sur l'ensemble des moyens mis à leur disposition afin de s'exprimer. 
- La création d'une assemblée citoyenne sous forme d’une AAI(3°) pouvant travailler en collaboration avec le CESE(4°) afin d'orienter les parlementaires sur les besoins des citoyens.
- La mise en place d'un droit de veto citoyen.
- La division de la voix d'un député. 50% appartiendra à ce dernier et 50% dépendra du vote des citoyens de la circonscription recueilli en ligne. Cette division s'opérera dans le cadre de votes importants. (Exception pour la saisine du Conseil Constitutionnel (Article 61 de la constitution de 1958(5°)) où chaque réputé comptera pour une voix pleine).

Nous tenons à vous remercier pour cette consultation tout en remerciant également nos contributeurs

Cordialement, 

Le comité LREM Quimper et deux étudiants de deuxième année de la faculté de droit de Quimper.

Liens utiles :
(1°) http://www.europarl.europa.eu/portal/fr
(2°)http://www.europarl.europa.eu/RegData/etudes/note/join/2013/493035/IPOL-LIBE_NT(2013)493035_FR.pdf
(3°) http://www.vie-publique.fr/decouverte-institutions/institutions/administration/organisation/etat/aai/qu-est-ce-qu-autorite-administrative-independante-aai.html
(4°) http://www.lecese.fr/
(5°) http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/la-constitution/la-constitution-du-4-octobre-1958/texte-integral-de-la-constitution-du-4-octobre-1958-en-vigueur.5074.html#titre7

Score : 6 / Réponses : 0

----

Il serait vraiment intéressant de partager nos idées, que les citoyens contribuent avec les députés à l amélioration des textes. Travailler en groupe, remonter les informations. Les citoyens sont des gens de terrain.

Score : 5 / Réponses : 0

----

Sur le principe des jurés d Assises, faire participer au vote de chaque loi d importance un groupe de citoyens tirés au sort. Pour que l exercice du pouvoir législatif et par conséquent la responsabilité de s y impliquer reposent aussi sur le peuple.

Score : 5 / Réponses : 0

----

Expérimenter de nouvelles formes de portage des contributions citoyennes, pour s’assurer de leur intégration sincère dans les travaux parlementaires. 

Les contributions citoyennes sont souvent ignorées de facto faute de portage politique par un-e élu-e ou un groupe politique. Dans son avis sur la confiance dans la vie publique (juillet 2017), le CNNum propose de permettre à des communautés, dans un cadre défini par le Parlement, de déposer des amendements citoyens, et d'être assisté par un administrateur pour arriver à la bonne expression formelle.

Source : https://cnnumerique.fr/wp-content/uploads/2017/07/Avis_CNNum_Confiance_Vie_Publique_vF.pdf

Score : 5 / Réponses : 0

----

Je pense qu'il serait très important que les citoyens puissent interagir avec les députés. Permettre aux citoyens de participer aux commissions par des auditions publiques aux même titre que les membres du gouvernement le font.

Score : 5 / Réponses : 0

----

Publier la loi qui est discutée sur un site interactif où chaque citoyen pourraient apporter des commentaires, des idées de modifications et avoir un retour (important !)

Score : 3 / Réponses : 0

----

Afin de favoriser l'interaction des plus jeunes avec la procédure législative, et plus largement les missions du  député, un système de parrainage serait intéressant. La députée de ma circonscription pourrait être la "marraine" de jeunes intéressés par les questions politiques : lycéens, jeunes étudiants pourraient faire remonter à leur marraine leurs questions, leurs enjeux et leurs propositions de solutions politiques. Les "filleuls ou filleules" de la ou du député auraient une mission officielle de remontée de solutions proposées par les jeunes. Ils ne pourraient pas se transformer en militants et distributeurs de tracts le temps des élections venu.

Score : 3 / Réponses : 0

----

L'accès à l'ensemble des textes est indispensable pour permettre la réflexion et faire part d'une proposition.
Si le lien direct avec nos élus me semble primordial, il ne garantit pas la prise en compte de l'élu pour la prise en compte lors des débats. Il me semble utile, voire primordial, que les élus participent à ce dispositif en organisant à leur niveau, dans leur circonscription, la présentation des textes et le débat. Ainsi certaines propositions pourraient émerger. Je crois savoir que c'est déjà le cas dans certaines circonscriptions où des Députés organisent ce type de rencontre. 
D'autre part, certains contributeurs, identifiés lors de ces rencontres citoyennes ou par dépose sur les plateformes dédiées et dont les propositions/remarques paraissent justifiées et argumentées pourraient être sollicités à participer à des auditions publiques au sein du Parlement, notamment dans le cadre des commissions.
Le droit de veto citoyen ne me semble pas opportun. Ce type de procédure traduirait alors un profond désaveu de la politique gouvernementale et/ou du Parlement qui nécessiterait alors le recours à de nouvelles élections comme le prévoit déjà notre Constitution.

Score : 3 / Réponses : 0

----

1) Désir d'un "calendrier de l'Avent" parlementaire pour interagir très en amont avec le député en fonction des lois ou des textes à venir.
2) Organiser une consultation sous forme d'ateliers par rapport au calendrier parlementaire, mais en présentiel pour ne pas exclure les citoyens non connectés.
3) Mettre l'accent sur la continuité des actions de terrain en acceptant les contradictions : marchés, porte à porte dans les quartiers... (en dehors des périodes électorales).
4) Consulter via des questionnaires numériques.
5) Lors des ateliers : éducation populaire via les échanges entre citoyens (que les gens se forment et s'informent entre eux).
6) En amont, se servir du fichier des adhérents pour cibler les profils et repérer les compétences.
7) Tirage au sort du citoyen ou organiser un service civique parlementaire pendant 1 ou 2 mois, type juré d'Assises, afin que les citoyens aient la possibilité de "vivre" l'élaboration de la loi.
8) Pendant la procédure législative, faire et diffuser des powerpoints pour vulgariser les idées et les projets afin que les citoyens puissent réagir et proposer des amendements.

Score : 2 / Réponses : 0

----

Le moyen le plus efficace pour contrôler ce qui  est voté, c’est de n’en pas donner de délégation de vote aux’ deputes pour toutes les questions importantes et faire voter le peuple pour ces thèmes importants. Déterminer ce qui est important doit être basé sur l’impact societal, économique, judiciaire, écologique et géostratégique.
Les technologies de block-Chain pourraient être utilisées pour faciliter les votes à distance. Les députés devraient passer un temps important à expliquer et répondre aux questions du peuple.
La république représentative n’est pas démocratique sans cela.

Score : 2 / Réponses : 0

----

Avec les moyens informatiques actuels, il est possible et souhaitable que les citoyens puissent donner leur avis lors de la procédure législative, notamment par la prise en compte d'avis ou de pétitions d'un grand nombre de citoyens.

Score : 2 / Réponses : 0

----

Idée : Un outil visuel d’accompagnement du consensus citoyen en faisant intervenir des citoyens sur la même problématique que les élus et grâce à l’aide d’un outil de visualisation. 
Le projet vise plusieurs objectifs et est en soi une expérience :
- Amener les citoyens à retravailler leurs points de vue en fonction de ceux des autres,
- Analyser si les citoyens arrivent à un consensus, pourquoi et comment,
- Détacher les échanges et le consensus des logiques politiciennes de luttes entre majorité et opposition et de voir à quoi l’on aboutit.

Le point de départ 
Lorsque l’on demande à plusieurs personnes de donner leurs propositions par rapport à un sujet/problème, on a potentiellement des points de vue très différents. Si l’on fait un vote, un point de vue remportera le plus grand nombre de votes mais ne sera pas forcément représentatif de la majorité. On peut procéder à un second tour en ne conservant que les deux points de vue ayant remporté le plus grand nombre de votes. C’est le principe des élections, pas celui du consensus. On fait donc souvent de nombreux déçus. 

De même, lorsque l’on répond à une consultation citoyenne il est :
-  Très long de lire toutes les contributions,
- Parfois difficile de les positionner par rapport à ses idées. L'étalonnage n’est pas simple à réaliser.
- Compliqué de retravailler ses propositions avec d’autres contributeurs (les outils ne sont pas forcément conçus pour).

Première décomposition des propositions
L’idée est alors de dépouiller les propositions et de distinguer pour chacune, ces trois volets :
- La manière de faire le diagnostic = d’analyser le problème (ses origines, ses manifestations actuelles),
- La manière d’envisager la réponse au problème = les objectifs que la proposition fixe,
- Les solutions opérationnelles et les dispositifs proposés pour parvenir aux objectifs.

Un outil de visualisation serait intéressant pour faire un premier rapprochement des propositions en fonction de chacun de ces volets.

Nouveau travail des propositions à partir des points de vue éloignés 
On propose ensuite aux auteurs de retravailler chacun des volets en prenant connaissance des propositions qui sont a priori les plus éloignées. On aura deux tendances : le rejet total ou une certaine influence. L’objectif est de développer la créativité, de confronter les auteurs à des idées qu’ils n’ont pas eu mais peuvent potentiellement les intéresser. 

On peut renouveler plusieurs fois l’opération. A chaque fois, on utilise l’outil de visualisation pour évaluer la proximité des propositions pour chaque volet.

Nouveau travail des propositions à partir des points de vue les plus proches 
L’étape suivante propose de faire travailler directement ensemble les auteurs ayant eu des idées assez proches pour chaque volet et de les faire aboutir à un consensus autour d’une seule proposition. 

On peut renouveler plusieurs fois l’opération. A chaque fois, on utilise l’outil de visualisation pour évaluer la proximité des propositions pour chaque volet.
 

Conclusions et vote éventuel
La dernière étape permet à tous les participants de prendre conscience du processus auquel ils ont participé grâce à une restitution. Un vote peut ensuite être organisé. Dans la mesure où l’on a eu davantage de boucles de travail et de consensus, le vote conduit normalement à moins de déception.
Sans même procéder à un vote, l’expérience est intéressante parce qu’elle permet à chacun de confronter ses idées au regard de celles des autres, mais en étant passé par des étapes de formulation et de structuration écrite et de retravail et grâce à l’aide d’un outil de visualisation.

Score : 2 / Réponses : 0

----

Pour nous l'enjeu ici est de co-construire avec les citoyens les outils et les procédures qui régissent la participation citoyenne numérique

La dynamique d'ouverture qui anime les institutions nous paraît bien évidemment nécessaire pour rétablir la confiance avec les citoyens. Mais il ne faut pas que cette ouverture se fasse à n'importe quel prix et que, dans l'urgence, des institutions publiques abandonnent à des acteurs privés leur souveraineté sur des procédures démocratiques essentielles.  

Nous saluons ainsi le fait que l'Assemblée Nationale ait choisi un outil open source pour sa consultation et une solution d'hébergement adaptée à ses besoins. De cette façon elle est la seule garante de la bonne sauvegarde de ses données, de leur publication en open data et du bon déroulé général de sa consultation.

A cet égard, les procédures qui entourent les dynamiques de participation citoyennes nous semblent au moins aussi importantes que les outils qui les mettent en oeuvre. Les dispositifs de consultation ne connaîtront du succès que si les institutions qui les mettent en oeuvre s'engagent à donner du pouvoir aux  citoyens.

Par exemple, sur la plateforme numérique de pétitions du parlement britannique, le gouvernement s'engage à répondre à toute pétition recueillant 10 000 signatures et à débattre au parlement de toute petition en recueillant 100 000.

Score : 2 / Réponses : 0

----

La consultation citoyenne envisagée en amont de l'examen des textes pourrait tout à fait être prolongée en plusieurs étapes à mesure de l'avancement des travaux parlementaires. Pourquoi pas, par exemple, rouvrir la même consultation entre la 1e et la 2nde lecture ou en amont de la commission mixte paritaire ?

Score : 1 / Réponses : 0

----

Excellente proposition que celle de m’en Bodin..

Score : 1 / Réponses : 0

----

Le sénat devrait disparaître au profit d'un vite citoyen. Avec le numérique qu'elle meilleure démocratie que celle de faire voter le peuple sur des décisions. Arrêtons cette fausse démocratie consistant à voter pour des décideurs, et votons pour des décisions. 
Quelle est cette démocratie ou vous devez voter pour des gens supposés vous représenter dans vos choix sans même savoir sur quoi ils vont être amenés à se prononcer ??? Ça rime à rien!

Score : 1 / Réponses : 0

----

Consultation citoyenne (20 personnes) au Lion d'Angers

- Avoir à disposition sur un portail numérique des supports numériques (vidéos, diaporamas...) qui permettent d'expliquer le texte et le résultat des discussions menées en commission de manière succincte.
- Mettre à disposition des supports informatifs accessibles par tous les citoyens
- Identifier les compétences, connaissances des citoyens dans différents domaines pour créer des panels de citoyens à solliciter suivant les thèmes débattus
- Non anonymat des participants
- Prévoir un filtrage réactif des contributions pour écarter rapidement les éléments hors sujet susceptibles de nuire à l'échange

Score : 1 / Réponses : 0

----

je suis prise de cours, je découvre à l'instant cette initiative et n'ai pas les connaissances suffisantes de notre fonctionnement juridique mais je tiens à laisser une trace car il est en effet urgent que le citoyen retrouve une place dans les décisions prises à travers un référendum ou un tirage au sort permettant à certains individus de peser dans la balance.

Score : 1 / Réponses : 0

----

Bonjour l'initiative me réjouit mais je la découvre  le 5 Novembre  avec change.org pourquoi cette plateforme n'à pas été créé avant de modification de la loi du travaille et comment intégrer les citoyens en ce qui concerne l'évaluation et le contrôle de la mise des lois ? un droit de référendum permettre aux citoyens de s'opposer  pour tout modification de loi  c'est ça la démocratie

Score : 1 / Réponses : 0

----

Le parlement appartient au peuple, il faut donc l'inviter à y entrer. Il va de soit qu'il faudra mettre les mesures sécuritaire qui vont avec. Le système français a besoin d'un zest de démocratie direct. Il ne faut pas trop de démocratie direct, mais à l'inverse il ne faut pas ne pas en mettre, juste la bonne dose. Exemple, sur des sujets assez gros et impopulaire, ne pas hésiter à consulter le peuple surtout si l'AN n'arrive pas à se mettre d'accord.

Score : 1 / Réponses : 1

----

Notre proposition: Les Français-es à partir de 15 ans pourraient prendre la parole une fois par semaine à l'Assemblée nationale pour témoigner de leur quotidien et proposer des lois à ces moments (4 interventions de 30 minutes par semaine). Les Français-es voulant participer devraient écrire un mail à l'Assemblée nationale, en expliquant les motivations de leur intervention, leur âge.

Pour déposer une proposition de lois, ces Français-es devront l'expliquer et la proposer à un groupe parlementaire. Si le groupe parlementaire accepte, il devra déposer cette proposition de loi au plus vite, et indiquer par qui elle a été proposée.

Score : 1 / Réponses : 0

----

Les lois votées doivent être moins nombreuses et plus simples. Elles ne doivent pas non plus sans arrêt bouleverser les lois antérieures. 

Le citoyen ne s'y retrouve plus.

Score : 1 / Réponses : 0

----

*Contribution collective Atelier Citoyen animé par Mme Fannette Charvier députée du Doubs*

3 pistes de réflexion se sont dégagées de la discussion collective :

__1/ La diffusion de la connaissance__

- Pour permettre aux citoyens d'interagir efficacement avec la procédure législative, il faut avant toute chose en expliquer mieux et plus simplement le fonctionnement. La loi et sa fabrique doivent être plus accessibles. La présentation très juridique des projets de loi les rend rébarbatifs voire incompréhensibles pour qui ne maîtrise pas le droit. Les études d'impact associées sont bien souvent denses et écrites dans un langage inusuel. 

- De plus, la procédure législative est méconnue; elle est certes disponible en fiches de synthèse sur le site de l'Assemblée mais là encore, présentée de manière peu abordable. Par conséquent le préalable est de rendre accessibles, le fonctionnement de la procédure législative; ainsi que les projets de loi. 
- Le groupe note que la présentation d'un texte de loi en texte plus simple, est souvent faite aujourd'hui mais par les partis politiques eux-mêmes, teintés donc d'une couleur politique. Cette traduction d'un texte juridique en texte compréhensible pourrait être réalisée par des fonctionnaires de l'Assemblée par exemple, ou par tout autre entité garantissant la neutralité des informations disponibles.

- Le groupe suggère également que l'éducation à la citoyenneté des enfants pourrait être améliorée à l'école afin d'éveiller la conscience citoyenne dès le plus jeune âge, de façon ludique et interactive.

__2/ L'interaction elle-même__

- Pour favoriser l'interaction elle-même, le groupe suggère de varier les modes d'expression, les supports. Le numérique peut y contribuer via des plateformes, des forums; toutefois il ne peut suffire car il ne saurait être représentatif à lui seul. De plus le numérique ne permet pas toujours les échanges d'idées et ne saurait se contenter d'être un "referendum" déguisé. Son utilisation doit être cadrée.

- Les ateliers citoyens pourraient être développés. Cela favoriserait les rencontres et les échanges, et permettrait ensuite soit de réaliser des contributions individuelles en toute connaissance de cause, soit des contributions collectives. Problème : au final il y a peu de temps entre le dépôt d'un texte de loi et sa discussion en commission puis en hémicycle faudrait pus de temps et moins de textes à l'étude en même temps pour pouvoir impliquer les citoyens en présentiel.

- Pour saisir les enjeux d'un texte, une suggestion consiste à le présenter de manière ludique, à la façon d'un "jeu dont vous êtes le héros". En fonction de chaque article du texte, le personnage incarné répond "oui" ou "non" en fonction de ce qu'il souhaite. Selon la réponse, le jeu interactif lui indique ce que cela change concrètement.

- Enfin, une dernière suggestion pour utiliser en premier lieu les outils déjà existants et à mieux en faire la promotion, avant de penser à en créer d'autres.

__3/ L'utilité de l'interaction__

Pour fidéliser les citoyens et éviter qu'ils se détournent, il faut que l'interaction débouche sur des actions concrètes. Quelques exemples :
- le dépôt d'"amendements citoyens"
- des questions citoyennes sur les projets de loi et leur évolution au fur et à mesure de la procédure : questions/réponses avec le rapporteur du texte par exemple
- questions citoyennes lors des auditions menées par la rapporteur
Là encore, le temps législatif et le temps citoyen doivent s'accorder et il faudrait allonger le temps de la procédure législative

Score : 1 / Réponses : 0

----

Sur la discussion et la révision constitutionnelle, un principe simple serait pourtant indispensable: Organiser obligatoirement un référendum pour toute modification du texte fondamental, et donc supprimer la révision par le Congrès (qui garderait son aspect symbolique lorsque le Président de la République souhaitera s'adresser solennellement aux députés et sénateurs réunis).  Une idée pour la prochaine révision constitutionnelle souhaitée par le Président Macron .

Score : 1 / Réponses : 0

----

Sur la loi, l’implication citoyenne doit se faire sur l’ensemble d'un processus transparent, de la proposition et du projet aux amendements. Il pourrait être imaginé une plateforme de contribution en ligne, animée par une équipe en charge d’accompagner des citoyen-ne-s contributeurs-trices, dont le rôle serait aussi de travailler sur la mise en forme et la recevabilité des propositions. Une présence sur l’ensemble du processus implique aussi la possibilité de demander le contrôle de constitutionnalité de la loi votée, dès lors que ce contrôle porte sur un argumentaire juridique cohérent et fondé.

Score : 0 / Réponses : 1

----

Il me semble que la seule consultation possible des citoyens sur les projets ou propositions de lois, dans un système de démocratie représentative, ne peut se faire qu'en amont des textes, de façon à préserver le rôle des Représentants. 

Autrement, il me semble qu’on risque de créer davantage d’opposition entre citoyens et parlementaires. Les premiers risqueraient de proposer des choses qui se verraient de suite rejetées, car irrecevables en droit parlementaire, et ils ne comprendraient pas forcément pourquoi (de manière générale, si on consulte davantage les citoyens sur les lois, il va être difficile de leur expliquer à chaque fois les conséquences de l’article 40 de la Constitution et l’irrecevabilité financière, donc peut-être faudrait t-il assouplir cet article).  Si l’idée c’est de créer un dialogue entre citoyens et parlementaires, et grâce à ce dialogue enrichir les lois, il faut faire la consultation en amont et qu’ensuite les arbitrages des parlementaires soient clairement expliqués. 

(Je parle ici uniquement de l’intégration des propositions citoyennes. Cela n’enlève rien à la possibilité de créer davantage d’interaction avec les parlementaires quand ils sont par exemple en hémicycle.)

Score : 0 / Réponses : 0

----

En raison de la crise de la représentation et de l’insuffisance du mécanisme référendaire, le recours systématique à la consultation publique dans le processus d’élaboration des lois d’envergure serait une alternative pour remédier à la crise par la prise en compte des avis exprimés par le public sans remettre en cause le système représentatif.
L’élection est un procédé essentiel dans le système démocratique. Mais il semble déficient dans son effet producteur de la crise de la représentation. La crise affecte principalement les institutions de la République et provoque la concentration des pouvoirs entre les mains de l’Exécutif. Le contrôle parlementaire et juridictionnel apparaît important mais insuffisant pour corriger les défaillances du système. La loi sur « le mariage pour tous » de 2013, la loi « Macron » de 2015, la loi « El khomri » de 2016 et les ordonnances « Macron » de 2017 ont successivement provoqué un soulèvement populaire inquiétant pour le maintien de la paix sociale.
Le référendum d’initiative présidentielle est prévu à l’article 11 de la Constitution française pour tenter de remédier à la crise. Il a permis aux citoyens d’intervenir juridiquement sur l’adoption des normes conventionnelles et législatives. Mais depuis le « non » au traité établissant une Constitution pour l’Europe en 2005, ce mécanisme démocratique semble tombé en désuétude (voir Francis HAMON, Le référendum, LGDJ, 2012). Le référendum peut tout de même être réactivé. Mais pour éviter qu’il soit un instrument plébiscitaire, son utilisation devrait être précédée par une délibération parlementaire.
L’initiative partagée (mi-parlementaire mi-populaire) est introduite dans la Loi fondamentale depuis la révision constitutionnelle du 23 juillet 2008. Les lois d’application ont également été adoptées. L’utilisation de cet instrument démocratique serait utile pour pallier les insuffisances actuelles du système représentatif français. Mais compte tenu du caractère restrictif des conditions, c’est une technique difficilement utilisable (Cf. Yvan BLOT, La démocratie directe : une chance pour la France, économica, 2012).
La consultation publique peut, en revanche, être retenue comme une formalité à rendre substantielle dans le processus d’adoption des lois d’envergure pour tenter de remédier à la crise de la représentation. L’article 7 de la Charte de l’environnement prévoit la participation du public à toute décision publique ayant une incidence sur l’environnement. Ce principe dont la mise en œuvre à été prévue par une loi de 2012 constitue une avancée démocratique considérable. Mais il reste limité.
L’obligation de consultation devrait ainsi être inscrite dans la Constitution et étendue à toutes les lois d’envergure afin de renforcer leur légitimité démocratique. Cependant, il serait peu efficace de procéder à des consultations populaires récurrentes pour recueillir les avis de tous les citoyens. À cet égard, plusieurs pistes de solutions sont mises en avant (Cf.par exemple, Dominique ROUSSEAU, Radicaliser la démocratie, propositions pour une refondation, Seuil, 2015).
L’on sait néanmoins que le Conseil économique social et environnemental (CESE) constitue bien une Assemblée consultative de rang constitutionnel. Au lieu de multiplier les organismes consultatifs, le CESE pourrait être maintenu comme l’institution servant à mettre en œuvre l’obligation de consultation publique. Cette Assemblée consultative devrait néanmoins être réformée en profondeur afin qu’elle puisse représenter toutes les forces vives de la nation. Pour renforcer l’indépendance de ses membres, leur désignation devrait être déconnectée de l’Exécutif.
Quant à l’outil informatique, il peut être utilisé comme un complément pour diffuser l’information et recueillir les avis du public. Mais il ne saurait en réalité remplacer le travail efficace qui puisse résulter des délibérations du CESE.
Les avis résultant du processus législatif devraient, en principe, être obligatoirement pris en compte. Au cas échéant, la motivation de la loi serait une alternative.

Score : 0 / Réponses : 0

----

En matière de transition numérique, l'empirisme et l'expérimentation sont la règle. Je vois revenir souvent dans cette consultation l'Atelier Citoyen Législatif. Il faudrait demander à Etalab, dont c'est la vocation, de développer une plate forme itérative (version 0) d'ACL.

Il faut passer de l'idée à l'objet.

Score : 0 / Réponses : 0

----

Le vote de chaque député lors de l'examen final d'un projet de loi pourrait être choisi via un vote électronique des électeurs de sa circonscription.

Score : 0 / Réponses : 0

----

Bonsoir, 
Ce que je propose n'a pas été proposé jusque là. 
Des anciens de ce débat pourront vois confirmer que j'y participe depuis le début même si toutefois je n'y est plus participer depuis quelques temps. La raison de mon silence est la réflexion. En effet, j'ai formé un groupe de discussion autour de notre projet. 
Prenant ce rôle d'un sérieux inébranlable j'ai donc recueillis le plus d'avis possible du plus grand nombre de personnes possible .

Score : 0 / Réponses : 1

----

Je vous préviens : si vous mettez en place ces c... de démocratie participative au niveau nationale, je ne voterai plus !  Citoyen, je vote pour désigner un représentant qui décide en notre nom. Je n'ai ni le temps, ni les moyens, ni les compétences de "co-construire" la loi. Ouvrir à chacun la possibilité de déposer des amendements, c'est changer de régime et faire entrer dans l'hémicycle tous les intérêts particuliers. Quel intérêt aurai-je alors à me déplacer une fois tous les 5 ans ?

Score : 0 / Réponses : 0

----

SYNTHESE CONSULTATION CITOYENNE 5911
Intervenir sans freiner le processus législatif.
Admettre un minimum de temps pour les citoyens : jours légaux de congés pour répondre à une convocation du député sur une thématique particulière.
Possibilité d’intervention dès le début du travail en commission.
Point de passage obligé e la commission auprès des citoyens (exemple par dizaine d’articles). A ce point de passage, le député va chercher les citoyens qui ont été retenus.
L’avis contraire des citoyens doit être inscrit.
Mise à disposition des textes, auditions publiques, amendements citoyens non évoqués par manque de temps.
Droit de veto rejeté.

Score : 0 / Réponses : 0

----

SYNTHESE CONSULTATION CITOYENNE 5911

Intervenir sans freiner le processus législatif.
Admettre un minimum de temps pour les citoyens : jours légaux de congés pour répondre à une convocation du député sur une thématique particulière.
Possibilité d’intervention dès le début du travail en commission.
Point de passage obligé e la commission auprès des citoyens (exemple par dizaine d’articles). A ce point de passage, le député va chercher les citoyens qui ont été retenus.
L’avis contraire des citoyens doit être inscrit.
Mise à disposition des textes, auditions publiques, amendements citoyens non évoqués par manque de temps.
Droit de veto rejeté.

Score : 0 / Réponses : 0

----

Création d'une possibilité d'amendement citoyen : 
- Il faudrait un quota réservé aux citoyens soit chaque amendement de député nécessiterait un certain nombre de signatures de citoyens, soit un certain nombre d'amendements devrait obligatoirement être proposé par des citoyens pour un texte donné. 
- Les amendements citoyens devront être portés soit par un rapporteur populaire qui aurait la faculté de les présenter et les défendre au sein de l'hémicycle, soit portés par un citoyen tiré au sort inscrit régulièrement sur les listes électorales. 
- La construction d'un amendement citoyen peut émerger soit par les réseaux sociaux en voyant que d'un coup une idée "buzz" soit grâce à un outil permettant une convergence d'idées citoyennes et ainsi parvenir à l'élaboration d'un seul amendement consensuel. 
- Formellement l'amendement devra contenir le nom, prénom de la personne qui l'a déposé. Chaque dépositaire devra être possession de ses droits civiques. 

Quelques réserves sont à noter contre lesquelles il conviendra de trouver un mécanisme pour s'en prémunir : 
- un risque d'inflation d'amendements ; 
- une absence de représentativité de l'ensemble de la société.

Score : 0 / Réponses : 0

----

La création d'un vrai droit à l'information sur le travail réalisé en commission et en hémicycle : 
- Il faudrait rendre public les votes des députés ; la conséquence est la mise en place d'une possibilité de participer à distance aux débats, les députés n'ayant pas la possibilité d'être en permanence en hémicycle. 
- Il faudrait créer une plateforme numérique permettant un accès total aux informations de l'Assemblée nationale. La plateforme devra être lisible et accessible : aujourd'hui difficulté à trouver les amendements, suivre les débats en hémicycle. La possibilité d'avoir une newsletter du travail réalisé au sein de l'Assemblée en fonction de sujets qui intéressent le citoyen constituerait aussi un pas vers cette ouverture. 
- Il faudrait rendre obligatoire dans les missions dévolues aux députés celle d'expliquer leur travail. Ainsi un certain nombre de documents serait rendu public car deviendrait transmissible aux citoyens. 
- Lors du travail en commission, les députés ont eux-aussi besoin d'un meilleur droit à l'information. Les auditions sont des monologues, il conviendrait d'organiser des débats contradictoires afin d'avoir dans une audition les tenants et aboutissants de la question et que la position du député soit plus éclairée.

Score : 0 / Réponses : 0

----

La création d'un budget participatif national : 
- L'article 40 de la Constitution empêche les parlementaires d'engager de nouvelles dépenses, or ce sont les représentants des citoyens. Il conviendrait donc que les citoyens puissent redevenir acteurs du budget proposé par le gouvernement chaque année. 
- Il faudrait réserver une ligne budgétaire aux citoyens ou alors prévoir un pourcentage du budget qui serait affecté à ce budget participatif. 
- Le budget participatif pourrait aussi être limité dans le temps : pour un an, deux ans etc. 
- Les projets qui pourraient prétendre au budget participatif national devront être pré-sélectionnés, tous ne peuvent pas être proposés (problème de faisabilité, poursuites d'intérêts personnels,...). A ce titre devrait être mise en place une commission spéciale composée pour moitié de députés et de citoyens afin de décider quels projets seraient pré-sélectionnés. 
- Le budget participatif devra se cantonner à la possibilité de réaliser des dépenses d'investissement et non de fonctionnement.

Score : 0 / Réponses : 0

----

Les citoyens devraient pouvoir déposer des amendements aux textes en étude.
Une plateforme dédiée, qui récolterait en même temps les citoyens votant, le permettrait. Le vote se ferait électroniquement, en s'identifiant avec son numéro de sécurité sociale ou de contribuable.
Pour être présenté, un amendement devrait récolter 10'000 signatures

Score : 0 / Réponses : 0

----

Lors de l'examen d'une loi, un groupe de citoyens devraient pouvoir émettre des doutes sur la conformité constitutionnelle ou aux lois supérieures (européennes, traités internationaux) et demander en amont l'avis de la juridiction adéquate (Conseil Constitutionnel, CJUE,,,).
La France s'est trop souvent retrouvée condamnée ces dernières années et cela nous a coûté très cher (arrêt de Ruyter, taxe des 3% par exemple)

Score : 0 / Réponses : 0

----

Je souhaiterais proposer un modification de l'usage référendaire. 
Pour des questions politiques de la plus haute importance, le jugement populaire ne peut être interprété comme un simple tirage à pile-ou-face. 
Je propose d'imposer un corum minimum pour que le résultat d'un référendum puisse être considéré comme représentatif. 
L'option majoritaire ne s'imposerait au législateur que si elle avait rassemblé plus de 50% des électeurs inscrits. Avec le taux résiduel habituel d'abstention cela nécessite un véritable écart entre les options portées au suffrage.
Je prends un exemple:  avec 10% d'abstention:
Option A obtient 51% des inscrits soit un résultat 57% 
Option B obtient 39% des inscrits soit un résultat de 43%

Avec un taux d'abstention autour des 20-25%, l'écart en serait d'autant plus décisif. 
Par ailleurs cela corrigerait une perversité du suffrage populaire qui est malheureusement souvent exploitée par les formations politiques. Habituellement les partis comptent sur la mobilisation des esprits partisants et sur l'apathie des sociologies qui ne leur seraient a priori pas favorable.

Avec le système majoritaire des inscrits, l'enjeu est de rassembler les fameux 50% des inscrits. Cela demande un effort de mobilisation
Le choix du peuple obtiendrait une vraie majorité et l'écart ne serait pas contestable.
Si la barre des 50% n'était pas atteinte le résultat aurait de valeur que consultative et l'assemblée nationale - voire le congrès - aurait le dernier mot.

Score : 0 / Réponses : 0

----

Les commissions des lois doivent être non seulement publiques, mais aussi ouvertes avec participation citoyenne sur inscription durant la semaine précédant l'examen de la loi, auprès de l'Assemblée. Ces inscriptions seront motivées par un projet d'amendement de la loi débattue. La participation de celui ou celle qui l'a proposé est une obligation, même si la mise en forme relèverait surtout de la Commission et des conseillers juridiques membres.

Score : 0 / Réponses : 0

----

Le souci à demander au Citoyen c'est qu'il n'a pas tous les éléments pour décider à cause du fameux problème d'asymétrie d'informations. Souvent son avis est biaisé par son quotidien donc il faut le sensibiliser bien plus tôt à l'école à cette relation avec les organes du pouvoir et en parallèle mettre en place des plateformes d'échanges et ensuite lui proposer deux ou trois choix avec les conséquences positives et négatives de ces choix et lui demander de se prononcer.

Score : 0 / Réponses : 0

----

C'est une belle idée qui demanderait tout de même qu'au lieu d'une langue vivante LV2, tous le monde ait des cours de droit. Même pour les nouveaux députés LaREM la loi peut être complexe et ils bénéficient de l'aide d'administrateurs aguerris et formés à cet exercice.
De plus, quand nous votons pour nos représentants, nous leur déléguons notre droit de vote et de création des lois. Il y aurait peut-être aussi du travail pour rappeler déjà aux français pourquoi ils votent et pourquoi s'abstenir leur est préjudiciable.
Rendre accessible les documents de préparation des lois est une bonne idée, mais s'il y a des gens qui travaillent à plein temps dessus, comment imaginer que les citoyens soient en mesures de s'en saisir à temps pour participer?
rendre publique les auditions est aussi une bonne idée, certaines le sont déjà ce qui pose plutôt la question de pourquoi pas toutes? Il y a certainement des raisons qu'il faut d'abord questionner.
L'interaction avec les élus au moment de l'examen donnerait pour moi un beau cafarnaum...quant à un droit de véto citoyen...pourquoi élire des députés alors?
Impliquer les citoyens c'est une bonne chose mais qui ne sera possible que si on leur donne les moyens : 
De comprendre la loi
De comprendre les institutions et leur fonctionnement
De se présenter aux élections législatives

Un moyen intéressant pourrait être de former des panels régionaux ou les citoyens travailleraient avec leurs députés sur les lois, même si la encore tous les députés ne travaillant pas sur tout le choix serait limité aux sujets traités par les députés à l'échelle du département ou de la région. Les réunions de ces panels peuvent être physiques ou virtuels.

Score : 0 / Réponses : 0

----

Le parlement élu en 2017 est une farce! Sa représentativité est plus faible que celle des centrales syndicales, c'est vous dire. L'AN est une armée d'androïdes au service d'un méprisant cyborg. La France est gouvernée par une caste malfaisante qui TRUQUE toutes les élections à son profit.

Score : 0 / Réponses : 0

----

- Pour une 3eme chambre législative constituée de citoyens tirés au sort pour une durée limitée (1 an ou 2) ; 
- un revenu minimum pour ceux qui seraient tirés au sort  y siégeraient temporairement. 

Le but est d'avoir un débat législatif plus large, où les citoyens sont vraiment représentés et impliqués, où les partis et les politiciens capteraient moins le pouvoir, où un peu de bon sens pourrait être bienvenu. 
L'enjeu est une démocratie plus directe.
Il s'agit aussi de motiver les citoyens pour l'intérêt collectif, sans qu'ils perdent en revenus le temps consacré à ce devoir.

On a bien des jurés tirés au sort parmi les citoyens et convoqués pour assister le pouvoir judiciaire en assise, pourquoi pas des citoyens pour assister le pouvoir législatif ?

Score : 0 / Réponses : 1

----

Tout comme pour la déclaration d'impôts, on rend le vote obligatoire via une plateforme, et le peuple valide ou pas. S'il ne se soumet pas à la procédure, il paye une amende, comme la majoration pour les impôts sur le revenu.

Score : 0 / Réponses : 4

----

Bonjour.
J'y suis complètement favorable.
De très bonnes idées ont été émises par tous mes prédécesseurs pendant la consultation et donner ainsi de bonnes propositions.
Cordialement.

Score : 0 / Réponses : 0

----

Oui je soutiens cette proposition pour une véritable prise en compte de l'opinion des citoyens qui jusqu'à présent n'ont servi que de caution pour que la classe politique puisse continuer à exister sans souci du bien public.

Score : 0 / Réponses : 0

----

En tout cas pour moi, des citoyens tirés au sort doivent renforcer les assemblées existantes pour participer à la rédaction des textes et amendements, mais en aucun cas les voter : ils ne représentent qu'eux mêmes contrairement aux elus qui vote selon un mandat. De même cela ne doit pas être une raison pour diminuer le nombre d'élus : plus il y a d'élus, plus il y a de représentativité et de democratie

Score : 0 / Réponses : 0

----

Les ateliers législatifs citoyens sont une très bonne proposition, mais s'ils arrivent pendant que le débat a lieu au parlement, comment organiser une remontée réactive qui puisse être prise en compte dans les temps,  quand on voit la réalité des emplois du temps des députés, qui déjà participent aux commissions nationales et n'arrivent pas toujours à être présents en même temps :-)  dans l'hémicycle pour voter les lois ? 
Si on laisse de côté cette question de planning, chaque député pourrait effectivement mettre en place une plateforme interactive de recueil d'avis et propositions, avec en parallèle des ateliers citoyens,   et sur la base des derniers échanges en cours à l'assemblée, faire émerger des propositions vraiment pertinentes par rapport à l'actualité du débat en cours. 

La notion d'expert est aussi importante me semble-t-il, et pas seulement experts nationaux mais experts citoyens, experts salariés ou libéraux, en recherche d'emploi , étudiant, retraité...c'est à dire solliciter en premier ceux qui vont vivre la loi au quotidien. 
Par exemple, si on crée un projet de  loi sur la protection de l'enfance (cf un des commentaires sur la procédure de  signalement des enfants en danger), il s'agirait de consulter et interagir avec les professionnels de l'aide sociale à l'enfance en priorité, d'identifier avec leur aide les méthodes donnant des résultats intéressants et s'en inspirer.   
Autre exemple, si le projet de loi concerne le glyphosate et les produits de substitution, consulter les agriculteurs qui arrivent à s'en passer pour voir avec eux quels produits "marchent", et qu'est ce qui bloque pour qu'ils puissent être homologués....

La volonté est là, mais le système est souvent grippé, donc rétablir de nouveaux canaux pour que les catégories de population concernées  soient plus légitimes pour s'exprimer  serait un plus.

Score : 0 / Réponses : 0

----

Et une EMISSION TV interactive ?

Bonjour ayant travaillé de nombreuses années dans le milieu de la TV, il me semble que l'outil internet devrait être associé à une émission de télévision intéractive sur la chaîne parlementaire et une web TV propre afin que les projets soient travaillé et soumis en amont de manière globale : Internet + TV afin que tout le monde puisse interagir même celles et ceux qui ne sont pas  l'aise avec le numérique ou n'y ont pas accès.

La construction s'articulerait sur la base d'internet et accompagnée en support explicatif et constructif via l'émission.

je pourrai développer le concept sans souci ICI ou directement.

Score : 0 / Réponses : 0

----

Transformer le Sénat pour rendre l'Assemblé Nationale plus efficace.

Il faut transformer le Sénat pour en faire une assemblée véritablement représentative de toutes les composantes de la société française. Une assemblée qui serait complémentaire de l'Assemblée Nationale (dont les députés semblent parfois déconnectés de la vie réelle). Les sénateurs seraient ,pour moitié, de "simples" citoyens tirés au sort et représentant l'ensemble des catégories socio-professionnelles. L'autre moitié serait constituée d'élus locaux, tirés également au sort. Cette assemblée aurait deux rôles principaux : 

- Faire remonter jusqu'à l'Assemblée Nationale les problèmes concrets rencontrés sur le terrain par l'ensemble ou une partie des citoyens avec l'obligation pour les députés de débattre régulièrement de sujets soumis par les sénateurs.

- Donner son avis par un vote  sur l'ensemble des lois votées à l'Assemblée Nationale. Une loi n'ayant pas obtenu la majorité au Sénat devrait repasser par l'Assemblée Nationale.

Score : 0 / Réponses : 0

----

En rejoignant Garlann Nizon, et en s'inspirant de l'outil démocratique Taiwanais, on pourrait avoir deux plateformes. La première recenserait les idées proposées par les citoyennes et donnerait la possibilité de commenter librement ou de rajouter de nouvelles propositions/
Chaque proposition atteignant un certain seuil de soutien populaire serait alors débattue dans l'assemblée (ou obtiendrait une réponse explicite si ce n'est pas faisable, par exemple si elle est incompatible avec des traités internationaux). 
Sur la deuxième plateforme, les citoyennes pourront suivre les différentes étapes de discussion de la loi, tout en ayant accès aux ressources (compte-rendu ou vidéo des débats à l'assemblée, différents rapports, position officielle des partis, avis d'experts et du gouvernement). A chaque étape, les possibilités ouvertes aux citoyens devraient être explicitées (par exemple les date de séance publique à l'assemblée, ou les modalités de participation et de contact). 
Enfin, un acteur indépendant devrait estimer la performance du gouvernement et son respect des engagements, pour éviter qu'une loi passe discrètement durant l'été comme arrive si fréquemment.

Score : 0 / Réponses : 0

----

-	Mise à disposition simple et fiable des procédures législatives en cours
-	Du député à l’équipe de terrain en présentiel et via des forums (télégram)
-	Réactivité au fil de l’eau
-	Présentation publique physique, vidéo
-	Création d’un site style « WikiLoi », réceptacle des projets, amendements et qui vulgariserait le projet pour une meilleure compréhension

Contribution collective du comité LREM Rennes Centre Sud

Score : 0 / Réponses : 0

----

Gérard GREBERT, citoyen « Lambda »
 Une « idée » concernant : La Participation des citoyens aux travaux de l’Assemblée Nationale
-	Au-delà  des outils de communication numériques et des circuits locaux (comités, groupes de travail….) ouvrant la possibilité à tout citoyen  d’émettre des avis et propositions à l’Assemblée, une participation « active et physique » de citoyens « Lambda »  est souhaitée dans tous les travaux de l’Assemblée (commissions, groupes de travail, voir … ?  études d’impact précédant la mise en place des lois… ?).
-	Dispositif :
Pour ce faire, l’ensemble des élus de l’Assemblée pourrait disposer d’un « VIVIER de Citoyens » motivés et volontaires dans lequel, lors du déclenchement de chaque étude ou concertation, quelques citoyens  pourraient être invités à participer.
-	Création du vivier :
Chaque citoyen pourrait s’inscrire en se positionnant sur 2 ou 3 thèmes pour lesquels il se
sent concerné ou particulièrement positionné pour apporter son expérience ou son expertise. Ces thèmes seraient à choisir parmi une liste préétablie de 25 ou 30 thèmes pouvant faire l’objet de travaux de groupes de l’Assemblée Nationale, plus, éventuellement un pré-positionnement sur un thème libre non listé.
               Lancement du projet :    Afin que l’ouverture soit maximale :
-	Présentation du projet à l’ensemble des Députés afin que toutes les tendances politiques soient impliquées. 
-	Information, par leurs circuits propres, par les partis, les mouvements, les associations …. auprès de leurs adhérents et sympathisants.
-	Messages d’information via les médias. Messages très courts (mais diffusés en boucle) à
des heures de grande écoute ( 19H55 ?) sur les chaines d’information télévisées (TF1, France2, France3, BFMTV, LCI ….) invitant les citoyens à s’inscrire dans le fichier de façon très simple : Coordonnées Thèmes choisis .(1 mini à 3 thèmes de la liste + 1 libre facultatif).
          Activation du Vivier : »
           Dés lors, ce Vivier constitué (les inscriptions restant valables 2 ans) et restant ouvert (pas de date limite, afin que le vivier reste « vivant » et que tout citoyen puisse s’inscrire quand il le souhaite), les comités, commissions, groupes de travail de l’Assemblée pourront puiser le nombre souhaité de participants « citoyens Lambda » à leurs travaux. Pour ce faire, dans un premier temps, le comité procédera au tirage au sort de 6 régions. Puis à l’intérieur de ces régions tirera au sort 6 citoyens parmi ceux qui se seront positionnés sur le thème étudié. (Prévoir des suppléants en cas de désistement des citoyens retenus ou de leur « éviction » ?? suite à une vérification éventuelle des capacités citoyennes et de la bonne moralité des personnes retenues  ?  Casier judiciaire ?..). Par ailleurs, rechercher la parité Hommes/Femmes (par exemple en retenant les 3 premières régions pour le tirage au sort des citoyennes et les 3 autres pour les citoyens….). 
           Les citoyens retenus seront contactés et confirmeront ou infirmeront leur volonté de participer aux travaux concernés.
           Rôle des citoyens cooptés : 
-	Participation aux travaux de la commission les ayant retenus.
-	Intervention sur le terrain afin de remonter un maximum de renseignement sur l’avis des citoyens quand une question se pose à la commission et que l’avis des citoyens « de base » est souhaitable (intervention directe auprès des citoyens, des associations, des élus, des acteurs de la vie économique… ; pourquoi pas la réalisation de mini sondages ou mini referendums auprès des citoyens de la région ?....selon les besoins de la commission).
-	Compte rendu à la commission des interventions sur le terrain.
-	Retour à la »base » de l’avancée des travaux de la commission.
-	Participation au Rapport final des travaux de la commission et diffusion en région ??.....
 NOTA : Voir l’intérêt …et la Constitutionnalité  d’un tel dispositif ?

Score : 0 / Réponses : 0

----

Pour intéresser un maximum les citoyens à la politique il faut les faire participer. 

Une part de l'assemblée pourrait être réservée aux citoyens. De 1 à 5 %, en début de quinquennat on annoncerait le nombre de sièges disponibles et chacun pourrait postuler. 
Il y aurait ensuite un tirage au sort qui désignerait les députés citoyens. 

Ils seraient contractuels de la fonction publique pour un CDD de 5 ans non renouvelable. 

Les citoyens se sentiraient vraiment représentés par des personnes considérées comme similaires.

Score : 0 / Réponses : 0

----

Il faut mieux promouvoir les consultations citoyennes, trop peu de personnes s'intéressent à la vie du pays, pourquoi ? 

Ils ne se sentent pas concernés ?

Souvent ils ne sont simplement pas informés !

Le service publique devrait être obligé d'informer les citoyens lors d'une grande consultation (A quoi sert la redevance?) 

Mais aussi les chaînes privées et les réseaux sociaux, il devrait y avoir une obligation légale d'offrir des espaces publicitaires aux informations citoyennes.

Score : 0 / Réponses : 0

----

Proposition : introduire une démarche de Co-Design dans le travail des commissions. 

Le rapporteur pourrait organiser des ateliers participatifs regroupant plusieurs types d'acteurs concernés.  Cette démarche est différente des auditions dans leur forme actuelle, où le rapporteur interroge successivement des experts ou des représentants. Ici, on chercherait à rassembler, lors d'un ou plusieurs ateliers, des acteurs de terrain et surtout les faire travailler ENSEMBLE lors des ateliers pour CO-CONSTRUIRE le projet.

Ces démarches favorisent l’intelligence collective et permettent de faire émerger des solutions innovantes. De nombreuses méthodes peuvent être utilisées par des facilitateurs externes, qui n’interviennent pas sur le fond du dossier mais posent un cadre qui  permet à chaque participant de mieux comprendre le points de vue des autres, et de converger vers des solutions qui conviennent à tous.

Exemple : Pour la rédaction d’une loi sur la réforme de la pédagogie en lycée, organiser des ateliers qui regrouperaient des lycéens, des enseignants, des proviseurs, des experts en pédagogie, des assistants, des concepteurs de manuels scolaires… etc  pour les faire réagir ensemble sur les projets.

Score : 0 / Réponses : 0

----

Il me semble que l'un des premiers objectifs à atteindre est celui de l'accessibilité et de la lisibilité des projets ou propositions de lois, mais également des amendements dès avant leur passage en commission. Afin que les citoyens soient impliqués régulièrement dans les travaux législatifs, j'aimerais proposer qu'au niveau de chaque groupe parlementaire soit imposé de recueillir un certain nombre de "parrainages citoyens" pour certains amendements déposés par le groupe (votes via la plateforme de l'assemblée, sous réserve de certains contrôles pour éviter les fraudes). Le nombre de parrainages est à débattre mais on pourrait fixer 3 à 5000 votes pour  5 à 10 % des amendements déposés dans l'année. En complément de ce système, des amendements d'initiative citoyenne pourraient également être proposés, toutefois, compte tenu du temps législatif, il faudrait être en mesure de proposer des "alertes" thématiques via la plateforme ainsi qu'une aide rédactionnelle pour que les idées proposées soient traduites en termes juridiques.

Score : 0 / Réponses : 0

----

Entre internet et les permanences parlementaires, il parait relativement facile d'échanger avec nos députés... Encore faut-il qu'ils soient à l'écoute et qu'eux-mêmes puissent se faire entendre dans ce cirque de l'Assemblée Nationale où de plus en plus les décisions sont prises avant que le débat parlementaire ait lieu!

Score : 0 / Réponses : 0

----

**Le dossier législatif unique**

Aujourd'hui, chaque assemblée parlementaire met en ligne, pour chaque texte de loi, un "dossier législatif", assez bien fait, l'un étant un miroir partiel de celui réalisé par l'autre assemblée, mais aucun ne donnant une vision complète du processus législatif. Il faut par exemple consulter le dossier de chaque assemblée pour accéder aux amendements, avec des interfaces de sélection différentes. Il semble aussi que les avis du CESE n'y figurent pas. 

En outre, ces dossiers, bien que publiés sur le Web, n'exploitent pas vraiment les possibilités du Web : on n'y trouve pas, par exemple, des liens hypertextes, depuis les comptes-rendus des séances vers les amendements ou vers les articles modifiés par le texte en discussion, ni depuis les amendements vers le texte en discussion, etc. L'insertion de ces liens peut être facilement automatisée et systématisée. De plus, l'ensemble des documents, même quand il est téléchargeable, est difficilement exploitable, du fait de l'absence de sémantique définie. Enfin, l'intelligibilité des documents est réduite, du fait du style de rédaction, faisant référence aux seules parties modifiées de textes existants, même si des liens permettaient d'y accéder. 

Il est donc souhaitable que l'Assemblée nationale et le Sénat s'accordent pour réaliser un dossier législatif unique en ligne. Ce dossier doit utiliser pleinement les possibilités du Web en incluant des _liens hypertextes_ systématiques entre ses différents documents ; des liens vers les documents pertinents provenant d'autres institutions (avis du CESE, rapports de la Cour des comptes, etc.) doivent évidemment être fournis par ce dossier. Les documents doivent aussi être pourvus d'un _balisage sémantique_ (utilisation de la technologie du web sémantique) au moyen d'un vocabulaire standardisé (par exemple pour indiquer quel parlementaire est l'auteur de quel amendement, quel article modifie quel autre article, etc.), de manière à pouvoir plus facilement exploiter l'ensemble des informations qu'ils contiennent ; le service Legifrance le fait déjà avec le vocabulaire standardisé ELI. Le dossier doit aussi présenter des _versions consolidées_ des textes en discussion, avant/après adoption du texte, pour comprendre plus facilement l'effet du texte sur la législation existante. 

Une fois ce dossier unique constitué (avec liens, balisage sémantique et consolidation), il peut être le socle d'un dispositif participatif, soit interne en permettant le dépôt de commentaires, de sous-amendements, de "like/dislike", etc., soit externe, en permettant simplement le partage d'éléments du dossier sur les réseaux sociaux usuels.

Score : 0 / Réponses : 0

----

Les interactions entre les citoyens et les élus pendant le processus législatif ne pourront évoluer qu’à l’aide de réformes structurelles.
En premier lieu, une réforme du Conseil économique social et environnemental (CESE) envisagé par le Président de la république est indispensable. Celle-ci pourrait porter sur les attributions du CESE notamment sur sa fonction consultative optionnelle ou obligatoire prévue par la Constitution. L’objectif serait d’intégrer le CESE au moment des discussions en commissions et en séance sur les projets ou propositions de lois et de limiter les avis ou rapport qui ne sont jamais lus.  A titre d’exemple, on pourrait envisager un droit d’amendement par le CESE pour impliquer les citoyens dans l’élaboration des textes.
Mais qui dit implication des citoyens par l’intermédiaire du CESE imposerait une autre réforme du CESE : la composition du CESE.  Cette instance composée essentiellement de syndicalistes (salariés et patronaux) et aujourd’hui assez peu connu par les citoyens. Il faudrait lancer une consultation publique pour recueillir des candidatures de personnes de la société civile et leur réserver ainsi au moins 50 % des sièges

Score : 0 / Réponses : 0

----

Moderniser et rendre plus interactifs les dispositifs de suivi des lois
- Aujourd’hui, l’Assemblée nationale propose de suivre l’avancée des lois via les « dossiers législatifs ». Les dossiers législatifs sont des outils précieux mais s’adressent à un public expert sinon confirmé. Dans son ensemble, le grand public ne connaît pas cet outil. Nous proposons donc de simplifier leur consultation en généralisant un format plus pédagogique et interactif, mobilisant des infographies et des ressources hypertextes pour faciliter la compréhension et contribuer à une meilleure pédagogie sur l’activité législative. Cette mobilisation des dossiers législatifs demande également de mieux faire connaître l’outil au grand public à travers une large campagne de communication. L’avantage de cette démarche est de s’intégrer directement dans les pratiques des institutions, qui se voient simplifiées et « ouvertes » à la participation du public.

[Proposition issue du Livre blanc " Démocratie mise à jour" : http://bit.ly/2ixDvSv]

Score : 0 / Réponses : 0

----

permettre aux citoyens de voter en ligne et en direct pour des projets de lois et que ce vote soit pris en compte lors de la décision.

Score : 0 / Réponses : 0

----

Lorsque le meilleur moyen aura été trouvé, l'ancien système sera totalement obsolète.

Le meilleur moyen n'existe peut être pas non plus de manière stable dans le temps. Il faudra donc un système qui ne tente pas de se figer dans le temps mais qui a en lui l'établissement du consensus.

Compte tenu de la diversité de préférences des uns et des autres, il faut un système qui permette la prise en compte des expressions quelque soit leur format. Evidemment, celui qui s'exprime devra au minimum le faire au bon endroit.

Les ébauches de démocraties "liquides" (je donne ma voie à X qui pourra la donner à Y (ou pas), à moins que je ne l'utilise), semblent de bons moyens pour faire remonter des sujets/opinions/problèmes avec une base de communication très hétérogène.

En bout de chaîne, celui qui souhaite voter (député) suit un protocole de vote et de communication bien précis. 

Le bordel du bas, fini par s'organiser en haut, avec un minimum de contraintes.

Score : 0 / Réponses : 0

----

Il me paraît indispensable que ce soit le peuple qui écrive les lois. En ce sens, toutes les propositions de participation citoyenne me paraissent bonnes à prendre. Nous devons œuvrer, tous ensembles, à les multiplier.

Je propose à tous les citoyens intéressés par ces principes de commencer dès à présent à constituer dans leurs communes — dans leur cantons, ou dans leur quartiers — des comités locaux, des groupes de travail, destinés à l'écritures de propositions de lois réellement en adéquations avec notre réalité à nous, le peuple.

Score : 0 / Réponses : 0

----

Les parlementaires sont élus pour faire et voter les lois, ils sont donc les plus légitimes pour travailler en ce sens. Mais si l'Assemblée veut ouvrir la discussion et la consultation pendant la période des travaux législatifs, il serait souhaitable, dans un premier temps, de créer une plateforme recensant l'ensemble des documents, annexes et travaux législatifs dont disposent les parlementaires, au bénéfice des citoyens souhaitant contribuer au débat. De sorte que chacun puisse être éclairé et proposer en connaissance de cause.

Ensuite, pourquoi ne pas créer des "amendements citoyens", toujours sur cette plateforme, avec un modèle type d'amendement (aide pour l'exposé des motifs et la rédaction de l'article). Une fois comptabilisés et "triés" pour leur pertinence, ces amendements pourraient être débattus en commission et défendus/présentés par le rapporteur général.

De plus, créer un jury citoyen serait en effet une bonne solution, par le tirage au sort ou en fonction des candidatures et du profil de chacun. Mais cette idée me paraît difficilement applicable vu l'existence du CESE (à moins de le fusionner avec l'AN ou le Sénat ou en faire un VRAIE représentation citoyenne). Ou alors, procéder à un tirage au sort via cette plateforme et lors des discussions en commission, un panel de citoyen serait choisi pour y siéger, au titre de la "représentation citoyenne" (pour différencier de la représentation nationale).

Enfin, donner pouvoir aux parlementaires de créer dans leur circonscription, une/des commission(s) citoyenne(s) serait peut-être la solution la plus simple et efficace.

Score : 0 / Réponses : 0

----

Pour permettre la participation citoyenne les projets de textes ne devraient plus pouvoir être modifiés par une myriade d'amendements les amenant parfois loin de l'épure initiale (ou même à sortir du sujet via des cavaliers). Trop amender un texte montre que le projet n'est pas suffisamment stabilisé. Ne faudrait il pas limiter le nombre d'amendements possibles ?

Score : 0 / Réponses : 0

----

Public visé : Parlementaires, lobbies

Contexte : 
Les parlementaires sont soumis à des règles déontologiques en ce qui concerne les groupes de pression, mais beaucoup de dérives existe, notamment liée au fait qu’il n’y est pas d’encadrement précis de leurs pratiques sur les élus. Ainsi de nombreux colloques sont organisés à l’initiative d’un député/sénateur (ce n’est pas toujours le cas*), auxquels prennent part les industriels en les finançant. Plus la participation financière est élevée plus l’entreprise ou du groupe peut être mis en avant à travers des interventions orales, écrites ou de simple affichage. Si l’expertise de ces acteurs du marché est indispensable et doit être entendue, son expression ne peut se faire au détriment de l’indépendance des élus et ne doit dépendre en aucun cas d’intérêts financiers.

Nous proposons :
La création de « Chambre d’écoutes », ouvertes aux groupes de pression, industriels, scientifiques et représentants de la société civil, qui pourront s’exprimer librement et faire valoir leurs positions sur les sujets en discussion à l’Assemblée nationale. Les lobbies n’auront le droit de s’exprimer avec les élus uniquement dans ce cadre (sauf pour les « groupes d’études ») sous peine d’être exclu de ces chambres.

Score : 0 / Réponses : 0

----

1) Comment être informé et avoir accès à la procédure ?

2) Comment et quand intervient-on ?

3) Pourquoi intervient-on?

- Citoyens force de propositions (voir initiatives citoyennes et consultations en amont des textes)
- Impact concret sur le quotidien / « Qu’est-ce que çela nous apporte ? »
- Quels moyens pour informer l’électeur sur les impacts d’une loi -> mobilisation des moyens en conséquence (tracts …)
- Connaissance de l’agenda parlementaire
- Lien avec le député
- Via la plateforme
- Relais d’informations / plaidoyer et pédagogie sur le terrain / Remontée de ressentiment fondé

Contribution de l'un des 4 groupes LREM des comités Villeurbanne Centre-Zola-Richelieu

Score : 0 / Réponses : 0

----

Il me semble que la meilleure façon de faire participer les citoyens au processus législatif serait le tirage au sort. Bien que critiqué, c'est le moyen le plus juste pour désigner des personnes. Pas de surenchères électorales, de beaux discours ou autres. Seulement un système de tirage au sort grâce auquel on pourrait désigner un nombre de citoyens qui serait le plus adapté pour s'occuper d'un projet de loi de A à Z sur une thématique donnée. Ce groupe de citoyens travailleraient alors sur un projet précis et pourraient proposer un projet de loi à l'Assemblée nationale et puis participer aux débats sur le texte. 
On resterait dans le système représentatif avec une dose de tirage au sort, le choix des Dieux comme disaient les Grecs anciens.

Score : 0 / Réponses : 0

----

Un citoyen doit pouvoir déposer un texte pour examen. 

La sélection des propositions d’origine citoyenne reste à définir, mais un système de vote (des citoyens) basé sur un nombre de points (un système du type Formule 1 ou Eurovision) me semble être une bonne formule. Si le quota de proposition d’origine citoyenne n’est pas atteint, alors, on laisse le créneau aux parlementaires. 

Le créneau des ordres du jour laisse très peu de peu de place aux initiatives citoyennes : Il faut empiéter sur le créneau de 1 semaine sur 4 dévolus aux parlementaires pour qu’un texte déposé par un citoyen puisse être inscrit à l’ordre du jour. 

Un citoyen doit pouvoir voter les textes, surtout en dernier ressort (lorsque les assemblées ne se mettent pas d’accord) 
En théorie, il est impossible de définir l’intérêt général à partir des choix individuels, (théorème d’impossibilité de K. ARROW) : Un nombre restreint de votes ne peut donc pas agréger la volonté de tout un peuple. La réduction de nombre de parlementaires ne va donc pas améliorer les choses … 

Seul un grand nombre de choix individuels (ceux des citoyens) peut donc prétendre rendre compte de l’intérêt général. Le poids des votes des citoyens devrait avoir un poids suffisamment représentatif de la population sans pour autant diminuer celui des parlementaires. C’est le dilemme auquel doit répondre le processus de démocratie participative. 
Je propose que les parlementaires votent en première lecture (première assemblée) et les citoyens en seconde lecture. Ceci impose un processus fluide et rapide. En cas de blocage on revient au processus classique (système de navettes)

Les textes doivent être connus suffisamment à l’avance, pour permettre au plus grand nombre de s’exprimer. Normalement, entre le dépôt du texte l’examen en seconde assemblée, il se passe 10 semaines. Il existe des systèmes numériques qui permettent d’être notifié dans l’heure qui suit la mise à disposition du texte. 

Le processus actuel de gestion des amendements impose une présence en temps réel lors de l’examen du texte. Les nouveaux outils numériques permettent un système de signature/validation de document électronique asynchrone ce qui permet de gagner en fluidité.

Score : 0 / Réponses : 0

----

S’il est difficile de ne pas considérer comme un progrès démocratique l’intrusion du citoyen « ordinaire »  dans le processus de fabrication de la loi, il est **important de se garder des biais que pourraient introduire une participation se basant uniquement sur le volontariat.**
En effet, effectuée de façon volontaire, une implication sérieuse dans le processus d’élaboration de la loi demande un investissement en temps qui est très souvent associé rapport avec une « mission supérieure » que chacun est en droit de poursuivre. Cependant, **la loi se doit d’être élaborée de façon impartiale pour la société** et non pour prendre fait et cause pour une vision particulière.
Sachant que tout citoyen, disposant de ses droits civiques, peut être amené à siéger lors d’une session de cour d’assises pour **concourir à l’application de la loi par tirage au sort.** Il semble dès lors intéressant d’envisager une démarche identique pour la **création de la loi en invitant par tirage au sort des citoyens** afin qu’ils expriment leurs remarques et partagent leur compréhension de l’impact qu’aura le texte sur leur quotidien. Cette démarche ne saurait être généralisée à toutes les lois, nombre d’entres elles portant sur des sujets demandant une technicité ou une expertise pointue mais elle pourrait dans un premier temps s’appliquer aux sujets dits de société (travail, famille, fiscalité, éducation,…).

Score : 0 / Réponses : 0

----

Interactions avec la procédure législative (exemples du Royaume Uni)
Les législatures britanniques ont toutes une période de consultation durant l’étape d’examen de la loi par les commissions parlementaires ; celles-ci qui permettent aux citoyen.ne.s et organismes intéressé.e.s de donner leur opinion sur tout projet ou proposition de loi examiné en commission. Les commissions publient un appel à contributions écrites dans la presse, sur leur site internet et par autres voies numériques (notamment Twitter). Le personnel des commissions et les élu.e.s contactent aussi certains groupes ou personnes qu’ils pensent pourraient avoir une contribution à apporter à la discussion. Les élu.e.s jouent un rôle important dans ce processus grâce à leur connaissance de leur circonscription. 
Les pages internet des commissions expliquent aussi comment répondre à une consultation parlementaire (notamment en termes de longueur, format, et style). Ceci permet de familiariser les citoyen.ne.s à une procédure avec laquelle ils ne sont pas familiers. Les pages expliquent aussi le rôle de la consultation et la façon dont les commissions utilisent les contributions écrites. Expliquer comment participer de façon efficace et comment les contributions sont intégrées dans la procédure parlementaire est très important pour la légitimité de la procédure et aide les citoyen.ne.s à comprendre la valeur de leur engagement avec la législature. 
Le personnel de la commission examine et résume l’ensemble des contributions à la consultation et la commission parlementaire passe en revue leur rapport. Sur cette base, la commission décide une liste d’auteurs de contributions à auditionner lors de la deuxième partie de la consultation. C’est sur la base de ces deux étapes de consultation, ainsi que sur les questions et contributions des député.e.s, que les député.e.s examinent les projets de loi et peuvent proposer des amendements.

Recommandations :
-	Réformer la procédure d’examen des textes de loi afin d’inclure une période de consultation lors de l’examen en commission ;
-	Dès qu’un projet ou proposition de loi est transmis à une commission, celle-ci devrait ouvrir un processus de consultation et s’assurer que cet appel reçoit la publicité la plus large possible, notamment via publication dans certains journaux et/ou publications spécialisées appropriées, sur le site internet de l’Assemblée Nationale et la page de la commission, sur La Chaine Parlementaire et Public Sénat, et via les réseaux sociaux ;
-	Création de comptes Twitter pour chaque commission afin de rendre plus visibles les travaux des commissions et les appels à contributions lors des consultations parlementaires ;
-	Création d’un portail sur le site de chaque commission permettant aux citoyen.ne.s et organismes de soumettre leurs contributions par internet ;
-	Réformer l’étape d’examen des lois en commission afin de permettre l’audition publique de certains contributeur.rice.s à la consultation sur la base de la qualité et la pertinence de leur contribution écrite.

Score : 0 / Réponses : 0

----

Ayant plusieurs fois voulu suivre le parcours d'une loi pendant les navettes législatives, c'est très peu accessible ou en tout cas assez compliqué. Il faut absolument rendre cet exercice plus fluide pour permettre aux citoyens de suivre les différentes étapes de la procédure législative. Si l'on veut donner envi aux citoyens de s'intéresser à la politique, il faudrait que ceux qui s'y intéressent déjà aient la possibilité de suivre le parcours de la loi. 

On pourrait mettre en place un système d'abonnement comme pour une newsletter, selon les thématiques qui nous intéressent. Lorsqu'une loi serait sur le point d'être discuté, on recevrait des emails régulièrement pour pouvoir suivre les discussions, les amendements. 
Les citoyens devraient également pouvoir s'inscire comme personne resource sur certaines thématiques. Ainsi, la commission en charge d'une proposition de loi tirerait au sort un certain nombre de ces personnes ressources pour avoir leur avis.

Score : 0 / Réponses : 0

----

l'idée d'ateliers citoyens en circonscription est intéressante, elle est à un niveau démultiplicateur de propositions réaliste

Score : 0 / Réponses : 0

----

N’hésitez pas à exploiter la consultation réalisée sur ce sujet en novembre dernier par Patrice Martin-Lalande & Luc Belot sur la plateforme Parlement et Citoyens ici : Généraliser les consultations en ligne : https://parlement-et-citoyens.fr/project/generaliser-les-consultations-en-ligne/consultation/consultation-29

Score : -1 / Réponses : 0

----

Je propose une base de de données de citoyennes et de citoyens volontaires pour participer à l’écriture de la loi en commission avec possibilité de tirage au sort.

 Ainsi, les personnes tirées au sort pourraient influer directement sur les députés afin de lier la souveraineté populaire et la souveraineté nationale

Score : -1 / Réponses : 0

----

On pourrait favoriser l’organisation d’une opposition citoyenne pour chaque projet ou proposition de loi à l’aide d’une structure pouvant être une plateforme numérique. La structure permettrait de sélectionner les questions et contre-propositions qui seraient portés par leurs auteurs dans un échange (public et retransmis) avec les porteurs du projet ou de la proposition de loi.

Score : -1 / Réponses : 2

----

Avant de réfléchir à une possible facilitation des moyens de communications entre citoyens et députés, il serait à mon sens plus intéressant que l'assemblée améliore déjà ses outils de communication. J'entends par là qu'il est difficile voir impossible régulièrement de suivre une séance tant en commission qu'à l'hémicycle par les liens vidéos disponibles sur le site de l'AN en raison d'un temps de retard important notamment.

Score : -1 / Réponses : 2

----

On se souvient en effet qu'en 2016, les manifestants contre la loi travail n'avaient plus d'autre choix que de harceler les députés. Après le recours au 49.3, quelques courageux ont tenté de faire signer [une motion de censure citoyenne](https://nuitdebout.fr/blog/2016/07/17/motion-de-censure-citoyenne/), en vain. Puisque les députés ne remplissent pas leur rôle de contrôle, il convient d'équilibrer les pouvoirs de l'exécutif avec un nouveau contre-pouvoir citoyen.

Je propose de compléter l'article 20 de la Constitution ainsi : « [Le gouvernement] est responsable devant le Parlement **et le peuple** », et l'article 49 en y ajoutant une **motion de censure d'initiative citoyenne**, qui pourrait causer la démission du gouvernement (et la dissolution de l'Assemblée) si une majorité d'électeurs la soutient.

Techniquement, les signatures pourraient être collectées sur [la plateforme développée pour l'article 11](https://www.referendum.interieur.gouv.fr/).

Score : -1 / Réponses : 0

----

Ce n’est pas’une Possibilité mais une obligation pour le député de consulter ses
Concitoyens. En tant que représentant politique et institutionnel de sa circonscription , il doit porter la parole citoyenne. Pour ce faire toute délibération, tout vote, toute prise de décision,  doit être mise en concertation citoyenne dans chaque territoire de députation. Un vote ou une délibération prenant en compte la construction d un consensus entre citoyen devra être mené. Et la décision prise devra être portée par le député à l assemblée et diriger son vote. 
Ce processus démocratique sera soumis à vérification, à contrôle par le conseil citoyen du territoire et de l état.

Score : -1 / Réponses : 2

----

Mettre en place une plate-forme numérique permettant à un député de consulter ses électeurs sur les différents articles d’une loi.
L’idée est que le député soit au courant de ce que pensent ses électeurs de chaque article de loi. Il n’est pas contraint de suivre l’avis formulé (notamment s’il y a un biais parce que peu de contributeurs par ex.), mais cela se ressentira à la fin de son mandat.
La plate-forme doit donc contenir la proposition de loi article par article (si possible mise à jour en continu en fonction des différents amendements). Le député pourra y inscrire son avis sur chaque article. Les citoyens de sa circonscription pourront voter pour ou contre chaque article, et éventuellement laisser un commentaire. Différents tableaux de bord permettront de suivre:
-pour chaque article les votes des citoyens (pour ou contre) par circonscription ou au niveau national
-les votes des différents députés
-le taux “de suivi” de chaque député des votes exprimés par ses citoyens

Score : -1 / Réponses : 1

----

Amendements citoyens : De ces travaux émergera surement une plateforme de participation numérique sur laquelle les citoyens pourront proposer des   des amendements pour chaque loi en chantier. On présentera les dix amendements ayant reçu les meilleurs scores à l'Assemblée nationale (en commission et en séance si non accepté en commission). Les députés auront la décision finale, qu'ils devront justifier auprès de nous.

Score : -1 / Réponses : 1

----

très bonne idée !

Score : -1 / Réponses : 0

----

Amendements: Lorsque des amendements sont rejetés,  si un groupe de citoyen (nombre à définir) estime qu'il n'avait pas à l'être, qu'il soit ré-examiné avec le groupe de citoyen à l'origine de la demande de révision.

Ecriture de la loi pour les citoyens qui souhaitent intervenir: Les citoyens qui le souhaitent devraient pouvoir s'enregistrer comme "volontaires pour écriture de la loi sur X" en présentant les grandes idées en quelques lignes. La sélection pourrait être faite par tirage au sort de 4 à 8 personnes. Ces personnes pourraient donc être force de proposition en présentant leurs idées communes d'une même voix.

Score : -1 / Réponses : 0

----

Une opportunité d'interactions se présente actuellement à l'occasion du débat sur le budget 2018 : budgéter un appel d'offre européen et provisionner la mise en place dès 2018 d’une assurance dépendance universelle, comme dans d’autres pays: mutualisée, elle nous protégerait tous et serait plus économique pour chacun, pour sa famille, pour les départements (que l’APA), l’Etat, la Sécurité sociale. 67% des français sont pour. La CNSA mise en place en 2004 propose des critères de choix. La promesse de mise en place date de la campagne présidentielle 2007 et créerait des formations et des emplois. “Près de trois Français actifs sur quatre ignorent comment financer une éventuelle perte d’autonomie, et près d’un retraité sur deux ne pourrait y faire face financièrement.”. Et vous, pour vous, vos parents ou grands-parents?
Merci de voter pour cette idée et de partager/faire tourner sur les réseaux sociaux, d’en parler avec votre famille et vos amis

Score : -1 / Réponses : 1

----

Parmi les moyens d'impliquer les citoyens dans le processus législatif, on pourrait imaginer une meilleure présentation des textes de lois et des amendements proposés.

Dans le monde de l'informatique, et notamment dans l'open source, de nombreuses solutions existent pour favoriser la collaboration. Et comme pour un texte de loi, les informaticiens travaillent sur du texte (ce que l'on appelle un code source).

Je vais prendre comme exemple la plate-forme Github.
Voilà une proposition d'évolution sur un code source, avec le rendu de la plate-forme : https://github.com/roboconf/roboconf-platform/pull/877/files

Non seulement on peut proposer des évolutions ou des corrections, mais on peut les commenter et expliquer pourquoi on les propose. Et d'autres peuvent répondre, contre-argumenter, critiquer ou féliciter. Ce genre de solution se prêterait parfaitement bien aux amendements. Non seulement pour les parlementaires eux-mêmes, même aussi à destination du grand public. Sur le fond, un texte de loi peut nécessiter une connaissance poussée du domaine. Cela ne changerait pas avec un tel outil. Mais la forme en serait dynamisée, car à l'heure actuelle, lire un amendement et comprendre son impact sur le texte global relève de la gageure pour les non-initiés. Et parfois même pour les initiés eux-mêmes. C'est flagrant avec le texte des ordonnances (qui certes viennent du gouvernement et non du parlement, mais qui se ramènent à l'évolution de textes législatifs - et rien que la forme est rédhibitoire).

Une meilleure présentation du travail législatif, via une plate-forme similaire, permettrait une meilleure accessibilité et une meilleure compréhension du travail législatif.

Au passage, si Github est public, il faut bien comprendre que c'est avant tout une interface graphique. La brique de base derrière peut être privée, répliquée, etc. Elle garantit aussi la traçabilité des contributions et leur historisation. Dans le cadre d'une version grand public, on peut vouloir masquer certaines subtilités techniques, en ne fournissant qu'une interface web simplifiée (notamment pour la gestion des contributions). Accessoirement, baser un outil législatif et collaboratif sur Git aurait un retentissement mondial.

N.B. : la question de fond serait l'identification des contributeurs, notamment des « simples citoyens ». Il me semble qu'au contraire des forums et réseaux sociaux, l'utilisation d'un identifiant officiel (par exemple numéro fiscal ou carte ID ou de passeport) est impérative. Non pas que ces informations seront affichées en public (on peut opter l'affichage d'un pseudonyme). Mais le système informatique connaîtra l'identifiant (identifiant et pseudonyme sont deux choses différentes). Un contributeur devra être conscient que s'exprimer sur cette plate-forme revient à s'exprimer en public et en son nom, pas de manière anonyme ou masquée. Cela ne servira qu'en cas de dérive. Les sites de presse en ligne ont opté pour ce système (commentaires réservés aux abonnés), ce qui a incroyablement réduit les trolls et commentaires injurieux. Contribuer au processus législatif, c'est participer « à la vie de la cité », mais ça présuppose un certain savoir-vivre et un peu de retenue dans ses propos.

Score : -1 / Réponses : 0

----

Lorsqu'un texte de loi est examiné par le Parlement, les citoyens pourraient exercer un droit de veto à l'encontre d'amendements -du gouvernement ou du Parlement- ayant pour objet d'étendre le champ du texte en lui faisant perdre son homogénéité, sa cohérence et son unité de départ. 
Ainsi, les citoyens veilleraient-ils et seraient-ils associés à la légistique et au maintien de la cohérence de l'oeuvre normative.

Score : -1 / Réponses : 0

----

Autres idées ?
Il serait bon de recourir à des méthodes favorisant la délibération démocratique sur des sujets à débattre bien identifiés ; nous  vous proposons la méthode de construction des (dés)accords féconds (voir sur le site du Pacte civique).
http://www.pacte-civique.org/OCQD (se reporter à l'annexe 8 du rapport 2013 OCQD consultable sur le site Pacte civique)

Score : -1 / Réponses : 0

----

Toute interdiction sera transgressée toute permission sera outrepassée. Plus une législation est abondante et complexe, plus elle offre d'opportunités au contournement de la loi et à l'abus de droit.
Les missions du législateur devraient être de légiférer le plus simplement et le moins souvent possible et surtout d'abroger le maximum de textes inutiles ou redondants.

Score : -1 / Réponses : 0

----

Afin de passer d'une démocratie représentative à une démocratie directe, il semble nécessaire de mettre en place le *mandat impératif* comme le préconisait Rousseau et de permettre aux citoyens de *révoquer leurs élus*. Ces mesures seraient définies et encadrées par des règles obtenues après un travail de discussion. Ainsi, il serait possible de révoquer un élu qui aurait choisi de ne pas respecter ses engagements devant le peuple. Par ailleurs, cela s’inscrit dans une démarche d’élargissement des contre-pouvoirs.

Score : -1 / Réponses : 0

----

Envoyer à tous citoyens qui est inscrit sur une liste électorale les avences d une loi même si ils ne donnent leur avis ils voient que l état et les élus les considèrent et les consultent

Score : -1 / Réponses : 0

----

Plusieurs modalités pourraient être envisagées :
a) Faire voter les citoyens sur les amendements déposés par les parlementaires : cela permettrait de valoriser le travail parlementaire et de décloisonner les logiques partisanes tout en donnant un pouvoir immédiat au citoyen sur la fabrique de la loi. Bien entendu, l'amendement (ou les amendements) devra être débattu et soumis au vote de l'Assemblée nationale
b) Favoriser le dépôt d'amendements (avec soutien d'au moins 500 000 citoyens) sur une plateforme participative
c) Tirer au sort, parmi des volontaires, 50 citoyens qui auront la mission de contribuer à l'élaboration de la loi pendant un an.

Score : -1 / Réponses : 0

----

Pourquoi ne pas s’inspirer des débats télévisés qui permettent aux citoyens de poser leurs questions en direct? Ce serait là un bon moyen de faire participer les citoyens aux débats parlementaires.
Mais il est clair qu’une certaine modération devra être réalisée pour éviter la malveillance et favoriser des interventions constructives.
Tout le monde ne pouvant intervenir en direct, la mise à disposition d’une plateforme capable de concatèner les questions similaires serait très utile, un rapporteur posant les questions qui en résultent.

Score : -1 / Réponses : 0

----

Utiliser des plateformes logicielles pour l'intelligence collective... Quelques exemples :
- https://www.stackoverflowbusiness.com/enterprise
- https://www.discourse.org/
- https://fr.atlassian.com/software/confluence/questions
- http://bluenove.com/en/
- https://www.seemy.com/fr/

Score : -1 / Réponses : 0

----

Inclure la participation citoyenne dans la procédure législative pourrait s’effectuer dans la continuité des consultations qui seraient effectuées en amont des textes. 
Si un tirage au sort de citoyens souhaitant participer à l’élaboration d’un texte peut ensuite permettre d’introduire une participation plus directe des citoyens tirés au sort dans l’élaboration des propositions de loi, il pourrait être judicieux d’effectuer ce tirage au sort parmi les citoyens ayant participé aux consultations en amont du texte. Ainsi, les citoyens tirés au sort auront déjà eu l’occasion de mûrir leur réflexion en amont sur le sujet abordé, ce qui pourrait être un atout pour la qualité de leurs contributions.
Par ailleurs, un tirage au sort à chaque texte permettrait à la fois de renouveler la participation citoyenne et d’inciter les citoyens à participer à chaque proposition de loi, voire d’augmenter le nombre de citoyens souhaitant apporter leur contribution, ceci afin d’avoir une participation citoyenne la plus large possible.

Score : -1 / Réponses : 0

----

Les lois évoluent au gré des différents partis, lorsque dans une loi des interdictions sont rendues, des solutions doivent être apportées avant de changer radicalement des habitudes.

Score : -1 / Réponses : 0

----

Contribution collective des comités strasbourgeois En Marche ! :
Constat : Il est impératif de rétablir un équilibre entre projets et propositions de lois, les citoyens étant exclus de la production législative avec des pouvoirs limités pour impacter la procédure législative (grève, pétition…).
Propositions :
- Les citoyens pourront faire des propositions qui seront soumises à une commission, sorte de comité stable et permanent chargé de sélectionner les projets qui pourraient devenir des lois.
- L'instauration d'une instance parallèle au Parlement. Elle prendrait la forme d’un comité de citoyens bénévoles, tirés au sort parmi des volontaires. Ils auraient accès aux documents de travail du Parlement pour pouvoir expliquer et vulgariser auprès du grand public l’action de ce dernier mais aurait également un droit d’amendement. Pour fonctionner cette instance disposerait d’un budget participatif, pour ne pas dépendre exclusivement de l’Assemblée.
Son avis sur les textes serait obligatoire.
Ce travail pédagogique se voudrait ludique et de préférence sans attache partisane.

Score : -1 / Réponses : 0

----

Aucune invention parmi ce qui suit mais l'assurance de faire intervenir les citoyens :

Constituer l’Assemblée Nationale de citoyens tirés au sort répondant au mêmes critères qu’un juré de cours d’assises (23 ans, nationalité…)

Préparer chacun et chacune à l’exercice du pouvoir législatif (à l’école ou bien par des formations faisant partie intégrante de l’Appel à la Défense)

(Supprimer le Sénat, dont le pouvoir législatif est nul. Une alternative resterait à trouver quant au successeur du président de la République en cas de force majeur.)

Score : -1 / Réponses : 0

----

Pourquoi ne pas accorder à l'assemblée une partie des sièges à des intervenants dont le projet de loi leur est destiné ?
Je me rappellerai toujours, un soir, après avoir clos les débats sur le mariage pour tous, le président de l'assemblée est passé au sujet suivant: celui-ci s'avérait porter sur la pêche et une nouvelle réglementation assez précise sur le métier et je me suis dit "mince, que connaissent les députés à la pêche ?" et en effet, à moins d'étudier à travers divers témoignages la veille du vote d'un projet de loi, le député n'a que très peu de chances de maîtriser le sujet. Pourquoi ne pas alors accorder, sur des débats bien spécifiques, une part des sièges à des intervenants dont le sujet discuté les concerne (ou du moins qu'ils le maîtrisent parfaitement). Exemple: une loi sur l'environnement, faire intervenir des personnes de green peace, une loi sur la définition du cours du blé -> des agriculteurs céréaliers. Ça permettrait de faire intervenir dans le débat des individus qui maîtrisent leur sujet, limiter ainsi les plaintes qui suivent une loi et ça ferait en sorte que les intérêts de l'État et des français concernés soient respectés ou au moins que des compromis soient fait.
Mettre en place ce système conserverait le vote démocratique (on laisserait une majorité des sièges aux députés) et ajouterait un vote rationnel lié à des réalités et des besoins que ne connaissent pas forcément les députés. 
Merci de m'avoir lu.

Score : -1 / Réponses : 0

----

Les rapporteurs et fonctionnaires ne doivent pas être laissés seuls face à la complexité des projets de loi.

Au delà des "personnalités compétentes" qui restent à l'initiative des rapporteurs, les citoyens volontaires et compétents dans le domaine du projet de loi doivent pouvoir se faire connaitre.

Cet annuaire de "personnalités compétentes" ainsi constitués permettrait d'élargir le champ de vision des rapporteurs et leur donner, s'il le souhaite, de nouveaux horizons (au delà de ceux fixés par les lobbyistes ?).

Score : -1 / Réponses : 0

----

Chaque sénateur ou député pourrait se voir attribuer un citoyen, volontaire ou désigné (à la manière de jurés), dans l'examen d'un projet ou d'une proposition de loi en particulier. Ce citoyen assisterait aux séances où le sujet est discuté, le député ou le sénateur jouerait un rôle de formateur, expliquerait au citoyen les questions techniques. Tous ces citoyens seraient rassemblés régulièrement, au long de l'examen du sujet, pour débattre de leur côté et élaborer leurs propres amendements citoyens. Ils pourraient interagir avec "leur" sénateur ou député pour formaliser les amendements. Ces amendements seraient ensuite introduits par les députés ou sénateurs volontaires. 
Ainsi, de nombreux citoyens seraient constamment associés, en présentiel, à l'élaboration de la loi, avec un biais de participation réduit car la désignation serait le mode de sélection d'une partie des citoyens. Le volontariat est cependant également souhaitable car il faut encourager les bonnes volontés.

Score : -1 / Réponses : 0

----

Le moyen le plus simple de faire participer le citoyen au Parlement c'est de lui permettre d'être en relation avec les députés de son département de pouvoir évoquer certains sujets et que les députés remontent ces discussions au Parlement. Par la suite si un projet de loi, le citoyen doit être informé de ce texte par les députés qu'il aurait contacté pour proposer ce projet. Par la suite, il doit pouvoir bénéficier ce de texte une fois voté afin de s'assurer que ce qui aura été discuté avec les parlementaires soit bien effectif dans la loi ou modifié. Si le citoyen s'engage dans cette démarche, il ne doit pas avoir connaissance de ce texte par les différents média mais bien en direct par le service communication de l'assemblée ou par ses députés.

Score : -1 / Réponses : 0

----

Créer une chaîne tv interactive avec son et peut être connection camera (via un boîtier qu'il faudrait créer ? 1 par foyer) avec l'A.N. et pourquoi pas les mairies, départements, régions. Ça résoudrait le problème du numérique pour tous (ruralité, personnes âgées etc). Je ne connais pas le nombre de foyers ayants une box Internet mais j'aimerai voir une telle chose exister. En plus les écrans TV sont plus grands que les ordinateurs et les téléphones. C'est une proposition un peu folle mais qui c'est ?

Score : -1 / Réponses : 0

----

Notre groupe de réflexion n’est pas favorable à l’instauration d’un droit de véto ou d’amendement citoyen, car nous pensons qu’un tel droit pourrait paralyser le fonctionnement législatif.

Notre groupe propose de développer les consultation des députés auprès de leur circonscription par 
•	la tenue d’atelier avec le député ou un des membres de son équipe.
•	le développement de plateforme en ligne de consultation,
•	la création de Facebook live hebdomadaire pour chaque député

Score : -1 / Réponses : 0

----

Question  au webmestre de l'Assemblée : ce que je ne comprends pas, c'est comment les choses vont se passer, si cette consultation démocratique numérique ferme dans un jour, c'est-à-dire, demain 6 novembre. Il faut, m'a-t-on dit, ouvrir un compte ? D'ailleurs, ai-je ouvert un compte, ou bien est-ce mon compte e-mail qui sert ? Je n'en sais rien. Ensuite que faire d'un compte si le site est fermé ? Est-ce que cela veut dire QUE LA CONSULTATION DEMOCRATIQUE NUMERIQUE EST TERMINÉE ? Je n'ose le penser !

Score : -1 / Réponses : 0

----

La proposition d'Assemblées représentatives par domaine sociétal peut paraître utopiste. Cependant, un peu d'attention à des propositions de citoyens transmises à Madame ou Monsieur le Ministre à  Monsieur le Rapporteur mériterait un peu plus d'attention si des corporatismes ont trop de pouvoir dans certaines administrations.
Par exemple, Madame la Ministre de la Santé a répondu à la lettre que je lui avais transmise en mars 2015 avant la discussion parlementaire du texte de loi de modernisation de la Santé. Le sujet de la lettre était relatif aux soins précoces en santé mentale. Pour mémoire, il y a eu 2 plans successifs sur la Santé Mentale après la préparation de rapports sur ce sujet par l'Assemblée Nationale, le Sénat, la Cour des Comptes et plus récemment par Monsieur Laforcade. La disparité des soins, soulignée dans ces rapports, est toujours très importante.
Il est surprenant d'entendre les propos de membres de familles qui appellent le service d'écoute des associations de proches de malades (Argos 2001, Schizo-Oui, UNAFAM...) : "Attendez que votre fils (fille) commette un délit, nous nous (les soignants) en occuperons ensuite". Les soins sans consentement existent dans tous les pays de l'OCDE, mais une mise en place de réponse graduée n'a pas été prévue : seule l'hospitalisation est mentionnée. La circulaire sur les équipes mobiles mentionne uniquement les personnes âgées ou précaires.  Dans les colloques de psychiatrie, tous les intervenants soulignent l'importance des soins précoces. 
Il existe souvent une inertie des services sectorisés de psychiatrie mais, curieusement, le pouvoir des psychiatres est très important puisqu'ils ont réussi à faire rejeter par une majorité de députés le projet de loi proposant l'interdiction de la psychanalyse dans les soins de l'autisme au printemps 2017.
L'Education Nationale publie régulièrement des résultats. Il existe également les rapports PISA qui permettent de comprendre qu'il existe une grande disparité de résultats à l'Education Nationale.
En complément à la proposition d'assemblées, je propose que des publications de résultats d'accompagnement, de formation...soient préparées par des personnes membres d'associations sensibles à la fragilité (élèves handicapés, personnes âgées en EHPAD, personnes atteintes de troubles psychiques, adultes handicapés....)

Score : -1 / Réponses : 0

----

Intégrer au processus législatif un mécanisme de pétition permettant de déclencher au delà d'un certain nombre de signatures (100 000 par exemple), l'obligation de réunir, au préalable du passage en commission, une conférence de consensus (ou de citoyens selon l'appellation). 

Le rapport rendu par cette dernière ne serait pas contraignant mais permettrait d'éclairer les élus sur les attentes et propositions des citoyens sur le sujet qu'ils auront à traiter. 

Cela ne nécessite pas forcement une réforme constitutionnelle et permettrait de redonner une place centrale aux citoyens dans le processus législatif sans pour autant être systématique.

Score : -1 / Réponses : 1

----

Propos collectés lors d'un atelier citoyen à Saint-Jean-de-Luz organisé par Vincent Bru, député des Pyrénées Atlantiques et les comités locaux LaREM:

En matière de consultation des citoyens,  prendre en considération, ne serait-ce qu'a minima en donnant une réponse d'attente, les questions posées.
 
Interagir, contrôler et évaluer les lois ne serait possible que si les textes étaient rendus publics lors de leur premier dépôt et mis à jour en ligne au fil des amendements. Cette expérience fut tentée timidement par le précédent gouvernement avec le projet de loi Claeys-Leonetti sur la fin de vie. Sans la généraliser, il serait très utile de mettre en place cette pratique pour les lois qui touchent aux libertés publiques, au travail, aux questions de société et aux grands projets ayant un impact environnemental, au minimum.
Une évaluation des lois qui se veut utile et porteuse de sens doit nécessairement commencer par une évaluation ex ante (bien sûr suivie d'une évaluation ex post lors de la phase d'expérimentation). La loi ne doit JAMAIS être conçue dans la précipitation, l'expérience montre TOUJOURS que la justification de cette précipitation par une prétendue urgence est systématiquement très coûteuse à terme. 
 
Un exemple catastrophique parmi bien d'autres: la loi sur la taxe nationale sur les véhicules de transport de marchandise (dite écotaxe).
Les ordonnances sur la loi travail sont à ce titre porteuses de très gros risques. La main mise de l'exécutif sur les lois serait la pire des dérives antidémocratique; attention, c'est le Parlement qui doit faire la loi conformément aux attentes des citoyens, bien sûr selon les orientations de politique générale du gouvernement.
 
L'article 11 de la Constitution pourrait permettre cette participation du public, sous réserve d'être rendu applicable, ce qu'il n'est pas actuellement comme le disent bien des constitutionnalistes. Que pense notre exécutif et nos parlementaires de cet article, comptent-ils en user utilement? Cet article devrait permettre de "soumettre au référendum tout projet de loi portant sur l'organisation des pouvoirs publics, sur des réformes relatives à la politique économique, sociale ou environnementale de la nation et aux services publics qui y concourent, ou tendant à autoriser la ratification d'un traité qui, sans être contraire à la Constitution, aurait des incidences sur le fonctionnement des institutions." Sa rédaction est si alambiquée (à dessein...) qu'il n'a jamais été appliqué, pourtant un projet fut présenté récemment à ce titre par 120 parlementaires de la précédente législature suite à la préparation d'un traité sur l'interdiction des armes nucléaires.
 
Consulter les citoyens utilement, autrement que par un vote manichéen, serait donc possible avec les outils actuels, bien sûr à aménager. Mais ce qui compte, c'est surtout de démonter la volonté d'engager ces consultations.

Score : -1 / Réponses : 0

----

Une plateforme citoyenne permettrait d'amender certains textes ou de voter certains textes de lois dans des domaines précis

Score : -2 / Réponses : 6

----

Tous les parlementaires ne sont pas toujours au fait des difficultés rencontrées, au quotidien, dans leur circonscription, ni de l'impact des lois sur le quotidien de M. et Mme tout le monde.
Par conséquent, il est important de permettre à chaque citoyen n'étant pas destiné à une carrière politique (c'est-à-dire qui n'a pas vocation à devenir parlementaire), de s'exprimer et de soulever, au moment où le Parlement délibère des textes dont il a été saisi, les problématiques qu'il rencontrerait si ce texte devait être adopté.
Cela se ferait via une plate-forme qui permettrait aux citoyens de proposer des amendements. Bien sûr, pour ce faire, cela nécessiterait un minimum de connaissances sur les dossiers présentés. Il conviendrait alors de permettre aux citoyens de prendre connaissance des documents de l'Assemblée, liés à l'élaboration du texte.
On pourrait aussi considérer que, pour obtenir le droit de participation (partant de l'idée que cette participation suggère l'accès aux documents préparatoires), chaque citoyen devrait fournir un extrait de casier judiciaire afin de s'assurer qu'aucune condamnation n'ait été prononcée à son encontre.
Vive la France, vive la liberté d'expression!

Score : -2 / Réponses : 2

----

Une organisation de votation populaire (système suisse) pour toute modification de la Constitution.

Score : -2 / Réponses : 0

----

Il serait intéressant d avoir un accès  (lecture) au logiciel eliasse de gestion des amendements. Ce logiciel mentionné dans un rapport d entretien avec le responsable de l ifnor donne accès aux amendements de manière numérique qui sont nombreux. Il semble techniquement simple (remativement) d ouvrir cet accès aux citoyens.

Note comme a à chaque fois j encourage à créer un accès nominatif du citoyen. Pas besoin de prouver son identité mais pas de login sous des pseudos masques qui permettent aux haters comme sur Twitter de surcharger la plateforme

Score : -2 / Réponses : 1

----

A partir d'une plateforme de consultation en amont des textes, on constitue une communauté de citoyens qui auront été sélectionnés à partir de leurs contributions sur cette plateforme. Le profile des participants peut être affiné au fur et à mesure des consultations. On peut distinguer ainsi les ideateurs, les stratèges, les fact checkers etc... Ces citoyens auront ainsi un accès facilité aux commissions parlementaires pour co construire la loi avec les élus. Ce dispositif permet de mobiliser les personnes qui ont le plus de valeurs ajoutées.

Score : -2 / Réponses : 0

----

Il pourrait être intéressant d'avoir une dizaine de citoyens tirés au sort, qui serait obligés de donner leur avis sur les lois. Une possibilité serait de s'aligner sur le modèle des jurés lors des assises populaires avec un avis obligatoire.

Score : -2 / Réponses : 0

----

J'espère que cette plateforme ne sera pas de la poudre aux yeux ou un outil pour faire semblant. 
L'année dernière, sur le tour de passe-passe sur la TVA effectué par les opérateurs de téléphonie avec les abonnements obligatoires à leur service presse: minimum 150M€ de pertes pour l'Etat,  j'ai écris:  
à mon député : pas de réponse
aux  membres du bureau des commissions des affaires économiques et du budget: pas de réponse.
Au président de l'Assemblée Nationale: pas de réponse
Cette année , sur ce même sujet, dans le cadre de la préparation du budget, j'ai écris au Premier Ministre (site du gouvernement) : pas de réponses, même pas un accusé de réception.
Je vais donc essayer cette plateforme.....et on verra
D'une manière générale, il y  la loi qui donne le cadre, mais les décrets d'application sont tout aussi importants car c'est là que les lobbyes font leur travail. 
Ce volet sur les décrets d'application devraient faire l'objet d'un chapitre particulier sur cette plateforme.

Score : -3 / Réponses : 2

----

Il faut simplifier l’ensemble es codes. Une solution simple: Pour tout nouveau texte ou article de lois, décrets, etc., il faut en supprimer le même nombre. Ainsi au moins, cela ne compliquera pas nos lois.

Score : -3 / Réponses : 0

----

Baisser le groupe parlementaire à 10 députés

Score : -3 / Réponses : 0

----

Quelles procédures inventer pour faire davantage intervenir les citoyens : c’est super simple. Faire un site Web pour pouvoir déposer nos Web amendements et faire voter en séances ceux qui ont reçu un avis a >=66% des citoyens-internautes ( avec enregistrement unique sécurise via le numéro de sécu ).

Score : -5 / Réponses : 7
