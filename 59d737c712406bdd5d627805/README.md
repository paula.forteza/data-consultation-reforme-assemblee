## Quel rôle pour les citoyens dans l’élaboration et l’application de la loi ?

### Consultation pour une nouvelle Assemblée nationale

# Bienvenue sur cette plateforme

<div><img src="https://consultation.democratie-numerique.assemblee-nationale.fr/an/bienvenue.png"></div>
<div><span style="color: rgb(107, 36, 178);"><b>Quels sont les objectifs de cette consultation&nbsp;?</b></span></div>
<div style="text-align: justify;"><br></div>
<div style="text-align: justify;">La présente consultation, ouverte du 9 octobre au 8 novembre 2017 à 21h, porte sur la manière de renforcer la participation des citoyens à l’élaboration et à l’application de la loi. Les contributions permettront d’alimenter en propositions le <a rel="noopener noreferer" target="_blank" href="http://www2.assemblee-nationale.fr/qui/pour-une-nouvelle-assemblee-nationale-les-rendez-vous-des-reformes-2017-2022/democratie-numerique">groupe de travail sur la démocratie numérique et les nouvelles formes de participation citoyenne</a> dans le cadre du processus <a rel="noopener noreferer" target="_blank" href="http://www2.assemblee-nationale.fr/qui/pour-une-nouvelle-assemblee-nationale-les-rendez-vous-des-reformes-2017-2022">« Pour une nouvelle Assemblée nationale&nbsp;: les rendez-vous des réformes 2017 -2022&nbsp;»</a><span style="color: rgb(0, 176, 80);">&nbsp;</span>&nbsp;lancé par le Président de l’Assemblée le 20 septembre dernier. </div>
<div style="text-align: justify;">&nbsp;</div>
<div><span style="color: rgb(107, 36, 178);"><b>Quelle est la plateforme de consultation&nbsp;?</b></span></div>
<div><br></div>
<div style="text-align: justify;">Il s’agit d’une plateforme <i>DemocracyOS</i> hébergée sur le site de l’Assemblée nationale. Elle est disponible en <i>open source </i>c'est-à-dire<i>&nbsp;</i>que son code source est distribué sous une licence permettant à quiconque de lire, modifier ou redistribuer ce logiciel.</div>
<div style="text-align: justify;"><br></div>
<div><span style="color: rgb(107, 36, 178);"><b>Quels sont les thèmes ouverts à vos contributions&nbsp;?</b></span></div>
<div><span style="color: rgb(107, 36, 178);"><b>&nbsp;</b></span></div>
<div>Les quatre premières thématiques ouvertes portent sur les grandes étapes de la procédure législative : le droit d’initiative des citoyens, leur consultation sur les projets de texte, leur participation au travail d’écriture de la loi et à son évaluation, quand cette dernière a été votée. Ces étapes concernent aussi bien les lois ordinaires que les lois constitutionnelles. La cinquième porte sur les différentes modalités de participation, qui peuvent reposer sur le numérique, mais aussi sur des réunions, des panels de citoyens, etc. Enfin, un espace est ouvert pour les idées qui ne rentreraient pas dans ces cinq thématiques.</div>
<div><br></div>
<div><span style="color: rgb(107, 36, 178);"><b>Comment la consultation va-t-elle se dérouler ?</b></span></div>
<div><br></div>
<div>La consultation va se dérouler en quatre phases.</div>
<div><br></div>
<li><span style="color: rgb(0, 97, 0);"><b>Phase n° 1 : consultation en ligne</b></span></li>
<div><br></div>
<div>La plateforme de consultation est ouverte du 10 octobre au 6 novembre 2017. La participation à cette plateforme est ouverte à tous : particuliers, associations, organisations, collectifs, … Vous pouvez si vous le souhaitez organiser des ateliers contributifs et utiliser notre <span rel="noopener noreferer" target="_blank"><a rel="noopener noreferer" target="_blank" href="http://www2.assemblee-nationale.fr/static/reforme-an/democratie/Kit-de-concertation-interactif.pdf">guide</a></span>.</div>
<div><br></div>
<div>La modération de la plateforme est effectuée par <i>Open Source Politics</i>. Elle a uniquement pour but de retirer les contenus contraires à la <a rel="noopener noreferer" target="_blank" href="http://www2.assemblee-nationale.fr/static/reforme-an/democratie/charte-moderation.pdf">Charte de la consultation</a>. <i>Open Source Politics</i>, ainsi que ses membres, se sont engagés à ne pas participer en leur nom à la consultation. </div>
<div><br></div>
<div> Les contributions reçues seront publiées en <i>open data</i>.</div>
<div><br></div>
<div>Par défaut, les contributions les plus populaires apparaissent en tête de la page par ordre décroissant. Vous pouvez si vous le souhaitez afficher les contributions les plus récentes ou les plus anciennes. Pour cela, modifiez le mode de <b><span style="color: rgb(230, 0, 0);">classement</span></b> à droite de «&nbsp;Commentaires&nbsp;» avant la 1ère contribution<span style="color: rgb(31, 73, 125);">.</span></div>
<div>&nbsp;</div>
<li><span style="color: rgb(0, 97, 0);"><b>Phase n° 2 : synthèse et sélection des contributeurs</b></span></li>
<div><br></div>
<div>Une synthèse des contributions reçues sera réalisée sous le contrôle du comité scientifique et rendue publique.</div>
<div>Le comité scientifique sélectionnera une vingtaine de contributeurs qui seront invités pour la phase n° 3.</div>
<div><br></div>
<li><span style="color: rgb(0, 97, 0);"><b>Phase n° 3 : ateliers à l’Assemblée nationale</b></span></li>
<div><br></div>
<div>Les contributeurs sélectionnés seront invités à l’Assemblée nationale afin d’échanger avec les parlementaires membres du groupe de travail au cours d’ateliers thématiques. &nbsp;Ces ateliers seront l’occasion d’affiner leurs propositions et de les analyser d’un point de vue juridique.</div>
<div><br></div>
<li><span style="color: rgb(0, 97, 0);"><b>Phase n° 4 : restitution publique</b></span></li>
<div><br></div>
<div>Une restitution publique sera organisée à l’issue de la journée d’ateliers. Elle permettra de rendre compte, notamment aux contributeurs et à l’ensemble des députés, des propositions retenues et mises en forme.</div>
<div><br></div>
<div><span style="color: rgb(107, 36, 178);"><b>Qui compose le comité scientifique et quel est son rôle&nbsp;?</b></span></div>
<div style="text-align: justify;"><br></div>
<div style="text-align: justify;">Le comité scientifique est composé de&nbsp;:</div>
<div>–&nbsp;&nbsp;M. Christian Leyrit, président de la Commission nationale du débat public&nbsp;;</div>
<div>–&nbsp;&nbsp;Mme Emilie Frenkiel, maître de conférences à l’université de Créteil&nbsp;;</div>
<div>–&nbsp;&nbsp;M. Daniel Lebègue, ancien président de Transparency international France.</div>
<div style="text-align: justify;"><br></div>
<div style="text-align: justify;">Sa mission est de vérifier que la méthodologie rendue publique par l’Assemblée nationale est bien respectée, de s’assurer que la synthèse des contributions est loyale et de choisir les contributeurs invités pour l’événement à l’Assemblée nationale.</div>
<div style="text-align: justify;"><br></div>
<div><span style="color: rgb(107, 36, 178);"><b>Comment les contributeurs invités à l’Assemblée nationale seront-ils choisis&nbsp;?</b></span></div>
<div><br></div>
<div style="text-align: justify;">Un panel diversifié de contributeurs sera choisi par le comité scientifique sur le fondement de&nbsp;: </div>
<div>–&nbsp;&nbsp;la pertinence,&nbsp; la qualité et le caractère novateur de leurs contributions&nbsp;;</div>
<div>–&nbsp; la popularité de leurs publications sur la plateforme&nbsp;;</div>
<div>–&nbsp;&nbsp;leur dynamisme pour participer à la consultation, notamment sur la plateforme. </div>
<div style="text-align: justify;"><br></div>
<div><span style="color: rgb(107, 36, 178);"><b>Comment sont traitées les données personnelles&nbsp;?</b></span></div>
<div><br></div>
<div style="text-align: justify;">Les informations recueillies sur cette plateforme&nbsp; sont enregistrées dans un fichier informatisé par le service de la Communication et de l’information multimédia de l’Assemblée nationale pour recueillir les contributions des citoyens dans le cadre du&nbsp; groupe de travail sur la démocratie numérique et la participation citoyenne.</div>
<div style="text-align: justify;">Elles sont conservées pendant une durée de deux ans à compter de la fin de la consultation et au plus tard jusqu’à la fin de la législature ; elles sont destinées au service de la Communication et de l’information multimédia de l’Assemblée nationale, au prestataire de la plateforme et au secrétariat du groupe de travail.</div>
<div style="text-align: justify;">&nbsp;Conformément à la loi « informatique et libertés », vous pouvez exercer votre droit d'accès aux données vous concernant et les faire rectifier en contactant le service de la Communication et de l’information multimédia de l’Assemblée nationale (<a rel="noopener noreferer" target="_blank" href="http://webmestre@assemblee-nationale.fr">webmestre@assemblee-nationale.fr</a>).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </div>
<div><br></div>

#### Commentaires

J'ai discuté avec plusieurs personnes depuis quelques jours, et personne n'avait entendu parler de cette consultation.
Compte tenu de l'intérêt qu'elle devrait susciter, je trouve la participation très faible et donc ses résultats potentiellement illégitimes.
Une telle consultation devrait être assortie d'annonces quotidiennes et de débats télévisés réglementés un peu comme aux élections, et d'organisation de rencontres par exemple hebdomadaires dans chaque mairie de France.
Sa durée devrait alors être étendue.

Score : 142 / Réponses : 11

----

Bonjour. 
L''initiative me rejouit. Mais je la decouvre le 4 Novembre a 2 jours de la cloture. Avec seulement 1300 contributions pour presque 70 millions de Francais. Je demande que la consultation soit prolongee au moins 3 mois et que les Francais soient informes par tous les media afin qu'elle prenne consistence et legitimite. Merci

Score : 117 / Réponses : 6

----

Initiative intéressante mais qui manque de communication auprès du grand public pour être représentative. Pourquoi avoir limité à 1 mois l'ouverture des forum ? C'est lorsque le bouche à oreille aura fait décoller cette initiative que tout sera clôturé !!!

Score : 109 / Réponses : 3

----

Je pense qu'il faudra être particulièrement vigilant sur la sélection des "contributeurs invités à l’Assemblée nationale" à propos de "la popularité de leurs publications sur la plateforme" car il est très fréquent que de très bonne idées soient proposées à la fin de la consultation et donc, qu'elles n'aient que très peu de votes.

Score : 97 / Réponses : 8

----

il faut  absolument augmenter le temps des inscriptions d'un trimestre et le rendre public sur les médias le plus tôt possible.
C'est un devoir démocratique.
Cessons de multiplier la perpétuelle et justifiée insatisfaction du peuple. 
Nous avons enfin un bon moyen de nous faire entendre il faut donc que cette action devienne réelle par la diffusion la plus étendue à tous et à chacun.
Merci.

Score : 52 / Réponses : 1

----

# Critique sur la plateforme
Après quelques heures d'utilisation de la plateforme, j'ai pas mal de points négatifs sur cette consultation qui doivent, je pense, être évoqués.

Avant cela, je trouve très bien l'utilisation d'une plateforme open source/libre et les explications de chaque sujet sont claires. Aussi, je trouve que les séances du groupe du travail avec les témoignages d'universitaires, chercheurs ou autres, sont très intéressantes.

Concernant la plateforme pour la consultation, plusieurs choses la rendent très moyenne pour notre utilisation :
- Le vote pour chaque proposition est binaire (pas de vote "Mitigé").
- Le nombre de votes n'est pas indiqué (si une proposition est fortement clivante, elle aura un score proche de 0 car beaucoup de personnes ont mis des votes positifs et d'autres négatifs, par exemple).
- Les propositions sont uniquement textuelles (et il faut savoir qu'on peut utiliser Markdown, c.f. page "Aide/FAQ").
- Impossibilité d'amender ou de proposer de modifications sur les propositions des autres, autrement que par une réponse.
- Impossibilité de faire de recherche parmi les propositions (ce qui sera un gros point négatif pour ceux qui vont faire un récapitulatif car il pourrait y avoir beaucoup de doublons)

Score : 49 / Réponses : 9

----

C'est une belle initiative qui faut amplifier et surtout populariser. Pourtant au fait de la vie politique, je n'ai appris que de la naissance de ce site par le passage du Président de l'Assemblée sur une émission de télévision le 14 octobre ... Le Bureau de l'Assemblée aurait dû transmettre consigne aux Députés de transmettre à leur tour l'information en circonscriptions.

Score : 39 / Réponses : 1

----

Bonjour à tous. Je découvre l'existence de cette plateforme. 
Ma 1ère réaction (à froid) est la suivante : bonne initiative, mais comment ai-je pu ne pas la découvrir avant ?

En effet, il me semble que la nature des contributions attendues (participation au débat sur l'évolution de l'AN) va au-delà de la simple bonne idée à avoir et j'aurais aimé pouvoir contribuer sans me sentir pris de court.

Une campagne nationale d'information a-t-elle eu lieu sur l'existence de la plateforme ? Pas vu.
Un exemple de plus - s'il en fallait un -  que (vraie) démocratie doit aller de paire avec (bonne) information.

Promis, je m'informerais mieux la prochaine fois ;-)

Bon débat à tous !

Score : 35 / Réponses : 0

----

Globalement, je suis assez sceptique  sur l'efficience  de cette consultation. Ainsi que plusieurs autres participants. Alors que je pense que "re"-créer du lien entre élus et citoyens est sans doute l'enjeu majeur de nos démocraties, je m'étonne que cette opportunité de faire participer les citoyens et donc de légitimer l'action politique ne soit pas mieux communiquée et mieux formalisée. Comparée à tous les forum divers et variés, la navigation ici ne permet pas de repérer facilement les propositions et d'y contribuer en adhérant ou en apportant des idées complémentaires. Les propositions sont complètement noyées au milieu des commentaires...dommage!

Score : 30 / Réponses : 1

----

Je viens juste de prendre connaissance de cette initiative transmise par un député du Morbihan et nous sommes le 3/11 ,dommage mais merci quand même .
Je pense que tout doit être mis en route pour que la parole des français puissent se faire entendre ailleurs que dans la rue ,ailleurs que dans les médias qui se laissent emporter .Oui il faut refaire du lien avec les élus et les simples citoyens regroupés peuvent faire poids dans la balance face aux lobbies qui mêlent les pouvoirs financiers à la réflexion.
C'est à la croisée des chemins que l'on se rencontre là où il y a de la place!!!
Fabriquer une loi demande des compétences que le simple citoyen n'a peut être pas mais il faut absolument trouver ensemble à nouveau des espaces de confiance mutuelles . Tous les conflits sont des problèmes de place de chacun 
prenons le temps ensemble d'y réfléchir la délégation de pouvoir ne peut que grandir avec ce projet.

Score : 26 / Réponses : 0

----

Ã quelques jours de la fermeture de la consultation je découvre son existence par internet sur le site du journal "le monde".
C'est évidemment de ma faute mais je trouve que la publicité pour cette initiative n'est pas populaire. Je vais donc être attentif aux prochaines publications afin de participer, ou tout du moins tenter de participer aux débats démocratiques. C'est peut-être un moyen de faire avancer une certaine forme de démocratie participative qui pourrait atténuer l'égémonie du parlement au profit de la parole du peuple, pour ne pas dire de la rue. Ã suivre donc.

Score : 25 / Réponses : 1

----

Excellente initiative. Je suis un peu "perdu" sur le mode de fonctionnement et d'échange sur la plateforme (et la question ou les questions qui sont posé(e)s).  mais j'adhère a tout ce qui peut rapprocher le politique du citoyen et au possibilité sur des sujets précis d'avoir des échanges via ce type de plateforme numérique.

Score : 23 / Réponses : 1

----

Idée concernant la plateforme :
Afin d'éviter les redondances, et de pouvoir appuyer efficacement les idées qui nous intéressent, il faut à minima une fonction recherche (comme proposé par David Libeau). Un système complémentaire serait de rechercher en temps réel, au fur et à mesure de la rédaction d'une proposition (comme ce que je suis en train de faire), et d'afficher dans un cadre, à côté de la zone de texte, les contributions déjà postées pouvant correspondre. C'est un système utilisé notamment par le forum www.debian-fr.org.

Score : 20 / Réponses : 1

----

Première remarque : la formule "renforcer la participation des citoyens à l’élaboration et à l’application de la loi." ne me parait pas la bonne. 

Car l' ELABORATION de la loi, c'est le travail du parlementaire, et son APPLICATION c'est le travail des pouvoirs publics. Le citoyen est censé la connaître et est tenu de la respecter. 

Le législateur a donc une mission qui consiste à inscrire les intérêts particuliers des citoyens dans un cadre contraignant qui fait émerger l'intérêt général, moteur d'une société démocratique, qu'on appelle la loi.

Dans ce contexte nous voyons surgir une innovation technologique qui bouleverse la société tout entière : Internet.

Internet permet à chaque citoyen d'exposer publiquement ses intérêts et les outils numériques donnent les moyens aux parlementaires d'exercer leur activité de façon innovante ; collecter les avis, dégager des synthèses et produire du sens. 

Deuxième remarque : se saisir des outils numériques change les mœurs bien au delà de la seule exécution de tâches pratiques : d'une part le législateur va faire évoluer sa connaissance des citoyens, électeurs, contribuables, et cette connaissance va influencer son action. D'autre part le citoyen-électeur-contribuable va voir son rôle et son pouvoir transformés par les outils numériques. C'est le "web tribunicien".

Pour ces raisons et pour favoriser l'émergence de bonnes pratiques du "web tribunicien", il faut documenter l'action de cette consultation en ligne. Cette action de documentation est indispensable pour :
- constituer le corpus théorique de la transition digitale publique
- faire connaître l'action des acteurs de la transition, 
- inscrire cette transition dans le récit national

Troisième remarque : tout usage ou introduction de l'Internet pose une question préalable non résolue : la question de la donnée personnelle et de l'anonymat. La persistance  de la non-résolution de cette question et l'absence de stratégie politique maintient les initiatives digitales à un niveau subalterne. 
A cet égard, la création de principes de droit accompagnée des indispensables expérimentations et essais empiriques donnera sans conteste à la France un crédit d'autorité au sein des observateurs et parlementaires européens.

Est-il envisageable d'ouvrir à Etalab dont c'est la vocation le champs de recherche-développement de ce "web tribunicien" naissant ?

Cette consultation n'est donc pas seulement un événement circonstanciel mais aussi et surtout un enjeu de société.

Score : 14 / Réponses : 2

----

! Remarque importante !: Le code postal de l'inscription à la plateforme ne reconnait que les codes postaux français. Quid des 2 millions de Français vivant à l'etranger et pouvant partager des idees nouvelles de leurs experiences locales? Merci de corriger.

Score : 14 / Réponses : 1

----

Cette consultation est une excellente initiative, même si elle peut apparaître imparfaite et insuffisante. C'est un bon commencement. Merci au Président de l'Assemblée Nationale de l'avoir lancée.

Score : 11 / Réponses : 0

----

Je pense qu'il serait bien que cette consultation soit un processus qui se fasse dans le temps. Je veux dire réitérer la même consultation plus tard.

L'idée est vraiment bien et je pense que beaucoup de personnes se sentirait intéressé. Par contre la visibilité et la durée sont vraiment limité si celle ci n'était pas réitérer.

Score : 10 / Réponses : 0

----

Excellente initiative qui va dans le sens d'une expression citoyenne libre et de la démocratie participative. Bravo

Score : 7 / Réponses : 0

----

C'est vraiment dommage qu'on ne puisse pas avoir accès aux réponses apportées aux propositions ; ça éviterait les répétitions et ça faciliterait le dépouillement . (au fait comment ça se passe ? )

Score : 7 / Réponses : 3

----

Idée concernant la plateforme : Les propositions nouvellement postées sont noyées sous celles déjà évaluées. On peut penser un système qui soumet à l'internaute qui vient de s'identifier, trois propositions qui n'ont pas reçu beaucoup de votes. Et je suggère de ne pas afficher leur score afin de ne pas influencer.

Score : 6 / Réponses : 0

----

Idée pour la plateforme : Proposer une troisième option de vote "Mitigé" et une option pour signaler un hors sujet (comme il peut y en avoir beaucoup). Afficher le score des votes sous forme de diagramme circulaire (comme sur le site Parlement & Citoyens) et non pas sous forme de solde comme aujourd'hui. Voilà à quoi cela pourrait ressembler: [lien vers l'image](https://www.noelshack.com/2017-43-2-1508854571-proposition-vote.png).

Score : 6 / Réponses : 1

----

A la question « Comment intégrer les citoyens »  en ce qui concerne l’évaluation et le contrôle de la mise en œuvre des lois,  voici le résultat de nos réflexions :
En amont l’Assemblée Nationale devra communiquer sur cette ouverture aux citoyens (par voies de presse, son site, LCP…)
Modalités
1 - le citoyen volontaire s’inscrit sur le site de l’assemblée dédié (en préambule il signe une charte de probité)
2 – il communique avec l’AN par voie informatique (plateforme d’échange similaire à celle mise en place pour la consultation actuelle)
3 – l’AN envoie systématiquement les thèmes sur lesquels le citoyen peut apporter sa contribution
4 – le citoyen y répond ou pas selon l’intérêt qu’il porte à la thématique
5 – dans tous les cas l’AN continue à le tenir informé de l’avancement des travaux par voie informatique
6 – si le citoyen choisit de travailler sur  un sujet il peut :
a)	Simplement transmettre ses retours à l’AN sans avoir à se déplacer
b)	S’il souhaite participer à des réunions « en personne » il s’inscrit sur une 2nd liste où il sera tiré au sort
c)	Ce tirage au sort pourrait être graduel :
-	1er temps : 1 citoyen par région
-	2ème temps : (afin d’éviter 1 surnombre entrainant des frais conséquents) tri parmi les citoyens présélectionnés pour arriver à 5/6 personnes maximum.
-	Ce tirage au sort serait valable pour la durée de la thématique. Il serait assorti d’un planning
7 – Comment traiter les retours des citoyens ?
a)	Par le biais d’une Intelligence Artificielle capable de regrouper les réflexions identiques et de faire le tri
b)	Grâce à des personnels dédiés aptes à faire la même chose que l’IA mais qui « ressentent mieux » les choses
c)	Dans tous les cas il faudra « in fine » des personnes pour affiner ces retours, en faire une synthèse et la rapporter à l’Assemblée et aux citoyens inscrits ou pas par tous les moyens possibles
8 – Quels thèmes ?
	Un  citoyen inscrit pourrait proposer un thème et s’il est également évoqué par d’autres citoyens (nécessité de définir un nombre) l’Assemblée pourrait le mettre à l’ordre du jour.

Score : 5 / Réponses : 0

----

Prendre le temps d'écouter, de sentir réellement quels sont les aspirations et les envies des gens qui veulent vraiment que notre grand pays sorte de sa torpeur, de son pessimiste ,et sortir enfin des idéologies éculées des parties de droite et de gauche qui ont conduit le pays dans une impasse.

Score : 5 / Réponses : 0

----

Voilà un outil fort indispensable qui ravivera j'en suis sûr, la passion de la fabrication de la lois et l'intérêt pour la démocratie participative.

Score : 4 / Réponses : 1

----

Je demande au "webmaster "s'il serait d'accord - sur demande expresse des intervenants sur cette plateforme - pour constituer une liste alphabétique avec mail  des  participants   à cette "consultation numérique" qui pourraient être communiquée à tous ceux inscrits sur cette liste.
Cela permettrait de garder le contact après le 6 novembre ..avec ceux qui le souhaitent évidemment!

Score : 4 / Réponses : 3

----

Je serais agréablement étonné si cette consultation contribuait à un renouveau, il reste que c'était un moment sympathique avec des échanges intéressants et interactifs, mettant en évidence les difficultés (qui ne sont pas insurmontables, loin de là) d'une participation constructive qui ne soit pas un simple défouloir. Certaines idées populaires apparaissent dans un grand nombre de propositions, par suite le nombre de leur soutiens est dilué dans toutes ces propositions et elles se situent artificiellement derrière d'autres . J'espère qu'il en sera tenu compte lors de l'analyse .

Score : 4 / Réponses : 0

----

Félicitations pour ce travail

Score : 3 / Réponses : 0

----

Il y'a un petit moment de cela j'ai eu la même idée, j'ai donc créé un site internet son nom est the-referendum, j'au aussi créé une page Facebook qui porte le même nom.Bref je ne suis pas la pour faire de la pub mais pour dire que l'idée est très bien cependant le plus difficile c'est d'une part de la faire connaitre est d'autre part de donner envie aux gens de participer à l'élaboration des lois par exemple. Il suffit de regarder les nombreux commentaires laissé sur la page Facebook de mon site pour voire que beaucoup préfère insulter les politiciens que proposer des idées.

Score : 3 / Réponses : 1

----

Pour info, le clic sur le pavé "consultation en amont des textes" renvoie vers une page blanche!
C'est dommage car faire participer les citoyens à l'élaboration de textes de loi est une idée intéressante

Score : 3 / Réponses : 1

----

L’Assemblée Nationale, par la voix de son Président, vient d’ouvrir une consultation publique en vue de renforcer la participation des citoyens à l’élaboration et l’application de la loi.
Cette initiative, à priori, interessante, ne manquera cependant pas d’inquiéter tous ceux qui demeurent attachés au principe de la démocratie représentative.
Est-il véritablement nécessaire, à un moment où l’antiparlementarisme connait un très net  regain, d’alimenter les doutes sur la capacité du Parlement à élaborer des lois justes et efficaces ?

Si on souhaite s’orienter vers un système politique du type suisse, où chaque décision du Parlement peut, à tout moment, être remise en cause par un vote populaire, il conviendrait de créer une mission d’information sur le fonctionnement réel de ce système et sur les dérives auxquelles il conduit.

Comme le notait, Marcel Gauchet, dans la « Démocratie Contre Elle-Même », plus on parvient à élargir les prérogatives de l’individu dans une société, moins celle-ci devient cohérente, intelligible et gouvernable.
Tous ceux, qui ont eu le privilège de participer au processus législatif, savent combien le travail est lent, difficile et combien il demande d’expérience et de réflexion.
N’en déplaise à certains démagogues, on ne s’improvise pas législateur, le temps où un « honnête homme » pouvait avoir un avis sur tous les sujets est révolu. Il suffit de demander aux nouveaux députés, le temps qu’il leur a fallu pour être opérationnels.
Faire intervenir, directement, d’autres acteurs dans le processus législatif, conduirait à l’obscurcissement des prises de décision et à la dilution des responsabilités.

Une démocratie, qui fonde son fonctionnement essentiellement sur l’écoute de ses différentes composantes, n’est plus apte à définir les grandes orientations ni à mettre en place les instruments dont elle a besoin.
Sur un plan pratique, l’introduction d’une dose de démocratie directe dans le processus législatif conduirait , en réalité, à  renforcer les pouvoirs des minorités agissantes.
Comment peut-on croire que se sont de simples citoyens qui rédigeront des propositions de loi ou des amendements ?

Les lobbyistes, les contestateurs professionnels et les représentants des minorités s’empareront des mécanismes, dès leur mise en place, pour tenter de faire triompher des intérêts particuliers.
Si le pouvoir des lobbyistes est bien connu, on fait semblant d’ignorer qu’une très grande partie des ONG et des associations ne sont pas composées de bénévoles dévoués, mais de professionnels salariés qui pour se maintenir en place doivent, en permanence, chercher à disqualifier et même souvent discréditer les autorités constituées et les responsables politiques.

Les différentes instances de concertation citoyenne existantes : enquêtes publiques, Débat public, conférences de consensus ….connaissent des échecs manifestes : soit le sujet n’intéresse personne, soit le débat est confisqué par des minorités agissantes, comme ce fut le cas récemment lors du Débat public sur le projet de centre de stockage souterrain des déchets radioactifs.
De toute façon, ces propositions de loi et ces amendements « citoyens » ne pourraient concerner que des sujets sans grande portée politique.
Il serait, en effet, inimaginable de ne pas leur appliquer l’article 40 de la Constitution qui interdit aux parlementaires d’augmenter les dépenses publiques ou de réduire les recettes.
Actuellement, même si la Délégation du Bureau chargée de la recevabilité financière des propositions de loi applique l’article 40 avec moins de rigueur que le Président de la Commission des Finances pour les amendements, le champs laissé libre à l’initiative des parlementaires est extrêmement restreint.
Cela explique certainement le peu d’intérêt de la majorité pour ces propositions.

En ce qui concerne le droit de pétition, il faut rappeler que celui-ci est déjà prévu par les articles 147 à 151 du Règlement de l’Assemblée Nationale, sans que cela n’ait donné, jusqu’ici, de résultat tangible.

Score : 3 / Réponses : 2

----

STOP AU TRAQUAGE !

Les pages de cette plateforme web contiennent de nombreuses ressources externes problématiques : gravatar.com, cloudflare.com, facebook.com, googleusercontent.com, fonts.googleapis.com, polyfill.io, google-analytics.com, etc.

Chaque accès à ces ressources externes collecte des informations de navigation sur les visiteurs et donc sont autant de traceurs indélicats et illégaux.

Pourquoi un site de l'Assemblée nationale ne respecte-t-il pas la loi cookie qui exige le consentement préalable des visiteurs, Y COMPRIS pour des traceurs via les ressources externes ?

Ceci est d'autant plus grave que l'Assemblée nationale ne peut absolument pas garantir que ces collectes de données ne seront pas utilisées à des fins répréhensibles ou non désirées, puisque stockées sur des serveurs hors de France et d'Europe, soumis à des lois incontrôlables.

Plus choquant encore, le site héberge les opinons politiques des visiteurs. Comment se fait-il que l'Assemblée nationale se rende complice du traçage d'opinions politiques ?

Bravo pour l'initiative et pour l'utilisation d'un logiciel libre mais puisqu'il s'agit d'un site web, pourquoi ne pas avoir choisi une licence GNU AGPL ?

C'est pourquoi je demande la correction de ces points purement techniques et pour lesquels des solutions existent.

Score : 3 / Réponses : 0

----

Excellente initiative! que je découvre seulement la veille de sa fermeture. Dommage !! Je vais être plus vigilant pour pouvoir apporter ma modeste contribution aux futurs débats.
Cependant, je n'ai pas vu de publicité ou d'informations sur cette brillante initiative alors que je pense qu'elle aurait dû être relayée par nos députés, particulièrement ceux de la majorité.

Score : 2 / Réponses : 0

----

Je ne découvre cette plateforme que le dernier jour. J'adhère à la demande de prolongation d'au moins trois mois qu'a proposée Jean Paul Curtay, avec une plus grande information des citoyens, comme l'expriment bien des personnes ci-dessous

Score : 2 / Réponses : 0

----

Il est impossible de filtrer  sur cette plate-forme les propositions d'un contributeur spécifique. C'est pourtant une fonctionnalité intéressante qui était disponible sur d'autres plate-formes et qui permet de trouver rapidement des propositions avec lesquelles on est en accord en fonction du contributeur sans avoir à lire l'ensemble des contributions.

Score : 2 / Réponses : 0

----

Une démocratie qui ouvre à chaque citoyen la possibilité de participer à
l'élaboration de la loi est un événement très significatif. ll est dommage qu'aucun politique n'en ait parlé, pas plus que les journaux télévisés.

Score : 2 / Réponses : 1

----

Une énièmes boîte de Pandore qu’on prendra bien soin de ne jamais véritablement ouvrir. Alors que l’ère du numérique qui permet au citoyen contribuable de s’aquitter de ses devoirs fiscaux via internet sous couverture d’un numéro fiscal unique.Pourrait aussi via la même rigueur et la même sécurité se voir directement interrogé sous la forme de référendums citoyens pour tous les sujets engageant notre société.Cela pourrait être le vrai chemin vers la démocratie participative, alors que l’on nous propose un simulacre de cette dernière et cela dans la plus grande confidentialité.

Score : 1 / Réponses : 0

----

Si l'objectif porte réellement "sur la manière de renforcer la participation des citoyens à l’élaboration et à l’application de la loi", alors il faudrait d'abord s'en donner les moyens. Comme l'écrivent beaucoup de commentateurs, cette consultation est quasiment clandestine. Cela augure mal du résultat.
C'est parfaitement dans l'esprit de "Jupiter" et de ceux qui lui obéissent au doigt et à l'oeil, et nous expliquent que nous avons voté pour l'application des mesures qu'il a déjà engagées. Pourtant, il a eu ma voix, comme des milliers d'autres, uniquement par défaut et semble l'oublier totalement.
Démocratie, oui. Mais pas seulement les apparences de la démocratie !
Ceci étant je suis également d'accord sur les précautions à prendre à propos de la popularité de certains contributeurs. 
Quand je suis né (il y a très longtemps), un personnage était très populaire auprès d'une grande majorité de français. Vous savez quoi ? Il s'appelait Philippe Pétain...

Score : 1 / Réponses : 0

----

Excellente initiative que je n'ai découverte qu'aujourd'hui par le biais d'une pétition ! Pourquoi n'y a-t-il jamais de communication sur ce genre d'initiative ? Pour que cela reste une affaire d'initiés et non de citoyens ?

Score : 1 / Réponses : 0

----

pas de sport télévisé pour cette consultation ! cela semble confirmer qu'on consulte sans consulter....

Score : 1 / Réponses : 0

----

le 5 novembre j'apprends une consultation qui me parait majeur. Elle se termine le 6... Quelle publicité informative y a t il eu ?

Score : 1 / Réponses : 0

----

Pourquoi ne pas faire simple : centraliser les pétitions qui fleurissent sur toutes les plateformes type avaaz, change.org etc. pour les porter officiellement et régulièrement aux politiques concernés ? Il me semble qu’il manque une instance qui fasse le lien entre pétitions populaires spontanées et l’assemblée nationale...

Score : 1 / Réponses : 0

----

Je vous rejoins dans vos remarques : Comment se fait-il qu'aucune "publicité" ne soit parue dans les médias traditionnels ? Comment se fait-il que les réseaux sociaux se soient tus ? Comment se fait-il que les sites dédiés aux pétitions n'aient pas relayés plutôt l'information ?

Score : 1 / Réponses : 0

----

Je confirme que le manque d'information évident des citoyens à propos de cette initiative, comme on l'a vu souvent avec d'autres (exemples des consultations publiques), augure assez mal de la suite... Ce type de démarrage se poursuit généralement par la constitution d'un club d'experts non-représentatifs et se finit par une absence totale d'avancée, une disparition des radars de l'initiative et en argument du type "on a déjà essayé et ça n'a rien donné"... Signal d'alarme !

Score : 1 / Réponses : 0

----

Les sujets vitaux comme l'élémentaire survie de l'humanité doivent passer en priorité dans l'ordre du jour politique de nos instances décisionnaires. C'est pourquoi sans sectarisme il faut avant tout pérenniser la présente plateforme comme une institution et sans faiblesse imposer l'omniprésence de notre voix

Score : 1 / Réponses : 0

----

Cette consultation que je découvre aujourd'hui à un jour de sa clôture m'interroge : quelle légitimité alors qu'aucune info n'a été relayée dans les médias . Ainsi elle ne représente qu'un groupe restreint d'initié. Quelle représentativité des différents courants politiques au sein du comité scientifique, qui les a désignés et sur quels critères ? Aucun débat n'a été initié sur le sujet qui aurait permis aux citoyens de s'emparer du sujet et d'y participer massivement et donc de légitimer une consultation citoyenne. Quelle légitimité donne t'on alors aux elus ( députés et sénateurs) ? N'ont ils plus qu'à être des chambres d'enregistrement ? Je suis très favorable à une participation citoyenne mais réfléchie, et organisee pour être un renfort de la démocratie et non sa spoliation par quelques uns.  Il me semble que tout est fait à l'envers et qu'un débat doit d'abord avoir lieu à l'assemblée nationale pour en définir les contours

Score : 1 / Réponses : 0

----

Je partage l'avis précédent. Je découvre par hasard et la veille de la clôture l'existence de cette consultation. Quelle a été la publicité pour cette démarche par ailleurs très importante? Oui, ceux qui subisse dans leur quotidien les effets des réformes, ceux qui les mettent en place dans "la vraie vie" doivent pouvoir participer, ancrer la loi dans le réel. Mais sans information préalable, sans diffusion large de la marche à suivre pour participer, ce n'est que du vent, une manière de dire : Nous vous avons donné la parole et vous ne l'avez pas prise! Dommage

Score : 1 / Réponses : 0

----

oui  fort  bien,   je viens  de  tenir un  atelier  sur  le  thème, j'ai  préparé  le  compte  -rendu  et  je  fais  comment  pour  rentrer  dans le  système pour  transférer  le  résultat ?

Score : 1 / Réponses : 0

----

on  s'inscrit où  pour  transférer les  résultats   des   ateliers  réunis  sur  ce  thème ?

Score : 1 / Réponses : 0

----

Voter une fois tous les 5 ans n'est plus suffisant pour un public de plus en plus informé et intéressé. La consultation citoyenne est une évolution remarquable à laquelle je participerai régulièrement Bravo pour cette initiative. et espérons qu'elle répondra à nos attentes.

Score : 1 / Réponses : 0

----

Nous voilà à la fin de la consultation qui aurait dû être nationale. Comme l'ont signalé Samuel Balle et Alain Dervieux, plusieurs problèmes fondamentaux sont présents. Vu le nombre de commentaires, j'estime que nous ne sommes probablement que quelques centaines à être arrivés sur cette plateforme, peut-être quelques milliers si le taux de participation est particulièrement bas, mais j'en doute. Contrairement à ce qu'annonce Samuel Balle, je pense que cela ne rend pas les résultats illégitimes, car cette initiative n'est hélas que consultative. 
Cependant, pour éviter les problèmes de ce type et progresser vers une démocratie plus citoyenne, il y a plusieurs méthodes (dont certaines furent mentionnées):
- tout d'abord avoir des informations disponibles dans toutes les mairies de France à ce sujet. 
- Pour éviter que les plateformes officielles se multiplient (entre plateformes globales et locales), essayer de centraliser cela, et organiser des débats sur des plateformes locales si elles le souhaitent, tout en indiquant sur chacune d'entre elles la présence de cette consultation nationale. 
- Dans ce sens, s'inspirer des progrès récents faits à Taïwan, qui a réussit avec vTaiwan à atteindre un taux de participation populaire très élevé sur de très nombreux sujets.
- Pour autant, cela signifie aussi un engagement de la part du gouvernement, non seulement à écouter les propositions faites (et pas uniquement celles qui vont dans son sens), et à avoir une obligation de réponse (dans un délai de deux semaines à Taïwan dès qu'un seuil de signatures est atteint). 
- Enfin, d'autres méthodes permettent d'atteindre une représentativité sans avoir à impliquer toute la citoyenneté, avec des systèmes aléatoires sécurisés et publiquement vérifiables.

Score : 1 / Réponses : 0

----

c'est  étrange  de  découvrir  que  c'est la commission qui  doit  recueillir  les  contributions  sur  la  participation  numérique  qui  ne  présente pas  des   zones  pour   copier les  compte  rendus  des  ateliers  réunis  sur   ce   sujet.  Je me  suis  égarée  sur  une page  OUVERTURE  et j'ai  trouvé  là   des  modalités  d'expression  numérique  beaucoup  plus  accueillantes  que  la page  dédiée   au  rôle  du  numérique  dans  l'élaboration de la  loi . étrange !

Score : 1 / Réponses : 0

----

Bonjour, effectivement il est dommage qu'une telle initiative ne soit pas relayée par le gouvernement avec plus de "tapage". A croire que c'est voulu pour en limiter l'impact. 
En attendant, je viens de la découvrir - dimanche 5/11 à 19h - et ai donc très peu de temps pour étudier tout ça.

Score : 1 / Réponses : 0

----

Bonsoir,
Je suis au courant moi aussi très tard, mais je serais enthousiasmée par l'idée que les pétitions lancées pourraient être obligatoirement considérées et discutées avec soin à l'assemblée à partir d'un certain nombre de signatures, par exemple.

Des assemblées de citoyens devraient aussi exister, pour proposer à l'assemblée leurs thématiques, faire remonter des problèmes à résoudre et surtout pour être consultées par le parlement avant la promulgation d'une loi. Mais je ne suis pas persuadée qu'internet soit le bon moyen et je pense que des assemblées physiques sont préférables dont au moins une partie des membres seraient tirés au sort dans leurs milieux sociologiques plutôt que choisis, car cela fausse à mon sens leur représentativité. Tirés au sort pour ne pas laisser de stratégies incontrôlables, ni d'effets pervers exister : Actuellement par exemple des groupements politiques voire des lobbies inondent les forums de commentaires qui vont tous dans le même sens, ou bien des contre-pétitions en réaction à une initiative citoyenne remportent plus de signatures que la pétition de départ, une fois les réseaux ( Facebook par exemple ) mobilisés...
Les "meilleurs" contributeurs remportant le plus d'avis positifs sont-ils donc toujours les plus pertinents ? L'effet "moutons de Panurge" sur internet me semble un danger.
Si un tel dispositif n'est pas plus réellement ouvert, on n'aura qu'une apparence de consultation citoyenne...

Score : 1 / Réponses : 0

----

je suis d'accord avec les précédents contributeurs : il faut allonger la période pendant laquelle cette consultation peut avoir lieu : par exemple jusqu'à début janvier 2018 et communiquer dans les médias pour solliciter le grand public à donner son avis. Je pense qu'il manque de démocratie à l'Assemblée Nationale, car les députés et les sénateurs ne nous représentent plus vraiment : c'est le chef de leur parti politique qui leur donne les consignes de vote pour les lois et les amendements, et pas ce que pensent les citoyens qui ont élu un député ou un sénateur. Un député qui veut voter "non" à une proposition de loi ou à un amendement, et si le PARTI  lui dit de voter "oui", et que le député désobéit, il peut subir des représailles. Un exemple flagrant du non -respect de la démocratie a été donné en 2015  lors du vote de la loi "Clayes-Léonetti", par le trio Dr LEONETTI (UMP) + Mr CLAYES (PS) + la Ministre de la santé Marysol TOURAINE.. En effet Claude BARTOLONE, alors Président de l'Assemblée Nationale avait suggéré une consultation citoyenne et ouvert un accès sur le site internet de l'Assemblée Nationale pendant quelques semaines qui a engendré une forte participation (avec des gens pour et d'autres contre cette loi, mais des deux côtés il y avait des remarques et des propositions intéressantes) : et bien le Dr LEONETTI , Mr CLAYES et Marysol TOURAINE ont refusé de tenir compte de cette consultation citoyenne pour amender leur projet de loi  = déni de démocratie... Ce n'est pas tout : lorsque des députés et des sénateurs ont voulu aménager cette loi, par des amendements, vu le risque d'euthanasie sauvage sans consentement du patient ou du risque de souffrance infligée au patient et autres amendements :  le trio TOURAINE + CLAYES + LEONETTI s'est arrangé pour les refuser presque tous ... Quand des députés et des sénateurs ont demandé qu'il y ait un compte-rendu à l'Assemblée Nationale chaque année sur l'application de cette loi : le trio TOURAINE + CLAYES + LEONETTI a refusé. Puis, vu le désaccord entre Assemblée nationale et Sénat, il fut organisé une "commission mixte" (je ne suis pas sûre du nom) : et le trio TOURAINE + CLAYES + LEONETTI ont choisi des députés et sénateurs acquis à leur projet, pour le faire voter  comme eux voulaient que cette loi soit votée, à la virgule près  : tout cela démontre qu'on n'est pas en démocratie... Aujourd'hui, on sait que la loi LEONETTI est détournée de sa vocation première,  qu'elle est utilisée pour tuer des patients âgés ou malades, qui voulaient vivre et être soignés, mais que les hôpitaux ne veulent pas soigner et auxquels ils ne demandent pas leur accord, ni leur consentement...N'importe quel médecin qui a mal soigné un patient, voire maltraité un patient, peut le tuer avec la loi Léonetti  (2005) et la loi Claeys-Léonetti (2015) en mettant ensuite la faute sur le dos du patient (en prétendant qu'il ne s'est pas rétabli : mais si le patient ne s'est pas rétabli c'est à cause du médecin ou de l'hôpital. Rappelons que les anti-euthanasie et les pro-euthanasie étaient contre le projet de loi " CLayes Léonetti" de 2015, chacun pour ces raisons : il est étonnant que le Président HOLLANDE et Marysol TOURAINE, les députés et les sénateurs n'ai pas entendus les citoyens, car s'ils les avaient entendu, cette loi Claeys -Léonetti n'aurait pas été votée puisqu'elle ne convenait ni aux pro-euthanasie, ni aux anti-euthanasie. Cet exemple prouve le déficit de démocratie en FRANCE, puisque les députés et les sénateurs peuvent être entravés dans leur mission, et les lois votées sans aucun rapport avec ce que pensent les citoyens français.

Score : 1 / Réponses : 0

----

L'initiative est intéressante,  à voir

Score : 1 / Réponses : 0

----

prolongation de la consultation pour les retardataires et les pas très aux courant comme moi si posible svp merci

Score : 1 / Réponses : 0

----

Cette consultation n'a fait l'objet à ma connaissance d'aucune publicité que ce soit. Comment l'entendre? est-ce à dire que finalement il ne s'agirait que de donner l'illusion qu'on prend bien en compte l'avis des citoyens ou plutôt de pouvoir affirmer que nous avons bien été consulté? Tout cela ne semble pas vraiment sérieux si l'on considère que cette "opération" est menée par l’État lui même avec tous les moyens dont il dispose. 
Autant dire, que cette consultation ne représente finalement pas grand monde et n'a donc qu'une très faible légitimité. C'est dommage ! mais une fois encore les moyens n'ont pas été engagés à la mesure des attentes du peuple et on a un peu le sentiment qu'on se moque de nous. (Une fois de plus?)

Score : 1 / Réponses : 0

----

De même que beaucoup d'autres ici je suis déçue de n'avoir entendu parler de cette initiative, pourtant intéressante, qu'aujourd'hui. Serait-il envisageable de la prolonger ? (et surtout d'en faire la promotion ?)

Score : 1 / Réponses : 0

----

Je viens de prendre connaissance de cette initiative que j'attendais pourtant de mes vœux, étant un membre d'en Marche depuis longtemps. Je rejoins toutes les demandes lues ici pour prolonger la consultation de plusieurs semaines et le faire savoir largement.

Score : 1 / Réponses : 0

----

Belle idée que cette consultation citoyenne. Mais faire un tri pour ne retenir que  les commentaires les plus populaires est pour moi discutable. Bon nombre de commentaires "sans like" sont néanmoins plis intéressants que certains commentaires "populaires" qui sont souvent très "militants".

Enfin, le gros bémol est le manque de communication autour de cette consultation et donc de visibilité pour les citoyens. Beaucoup de personnes n'ont tout simplement jamais eu connaissance de cette initiative. Ce qui est regrettable vu l'importance et la rareté d'un tel "événement".

Néanmoins, heureux que le bureau de l'Assemblée Nationale ai souhaité consulter les citoyens dans le cadre de la modernisation de l'institution. Espérons que ces avis soient maintenant réellement considérés et pris en compte ...!

Score : 1 / Réponses : 0

----

Bonjour. Je suis très contente qu'une telle consultation soit mise en place. Cependant je la découvre aujourd'hui 8 novembre à 17h15 et je lis en première ligne qu'elle sera clôturée ce soir à 21h, plus loin dans l'explication du déroulement, je peux lire « Phase n° 1 : consultation en ligne
La plateforme de consultation est ouverte du 10 octobre au 6 novembre 2017 ». 
Du fait, de la divergence entre les dates, je ne sais pas si je vais pouvoir participer ou non.

Je trouve dommage qu'une telle initiative n'ai pas été relayée plus largement dans les médias et les réseaux sociaux.

Score : 1 / Réponses : 0

----

Commentaire 1 
Questionnement autour de la consultation publique portant sur l’association des citoyens à l’élaboration de la loi

Tout observateur républicain et démocrate (à moins qu’il ne soit démocrate et républicain ?) de la vie politique française ne pourra qu’accueillir avec satisfaction une proposition de réforme de l’une des composantes des institutions françaises.
Fruit du « chamboule tout » (dans l’esprit ?) de l’atypique élection présidentielle qui a porté au pouvoir Emmanuel Macron et son mouvement En marche, cette réforme proposée par le Président de l’Assemblée Nationale François de Rugy, semble, a priori, répondre, aux attentes de nombreux citoyens .
Le mouvement « dégagiste » qui s’est initié à cette période en est une illustration concrète.
Nantis des moyens dont ils disposent, temps, compétences, informations, les citoyens ne désespèrent pas de reprendre leur destin en main… et se questionnent légitimement sur le fonctionnement démocratique de nos institutions fondées, entre autres, sur la représentation démocratique, et le bicamérisme.
Ce document reflète la position de l’Observatoire Citoyen de la 1ère circonscription de la Loire ). 
Désireux de contribuer positivement aux thèmes ouverts notre groupe s’est néanmoins interrogé sur la finalité de la démarche et l’articulation des thèmes proposés.
1 – de l’information sur le projet.
Seuls les « veilleurs » de groupes tels que le nôtre ont eu l’information, relayée, c’est notre cas, par le député de la circonscription.
Quid d’une démarche de développement d’une forme de démocratie participative avec aussi peu de communication vers le grand public ?
2 – du sens de la fonction représentative du député versus l’approche par son statut.
On peut s’étonner, à la lecture des textes introductifs des thèmes de réflexion, que ne soit pas questionnée la fonction représentative et son positionnement effectif,  entre un exécutif tout puissant dans l’élaboration du calendrier parlementaire, dont l’envahissement réduit à peau de chagrin la capacité de prise en compte des propositions de loi, d’une part, et d’autre part la relation avec la société civile, et sa capacité à entendre la voix de cette dernière et élaborer les propositions de loi pouvant en émaner.
3 – du statut des attachés parlementaires versus les administrateurs et autres fonctionnaires de l’institution.
Un récent article d’Acteurs Publics interroge la relation « concurrentielle » entre les deux statuts et le déséquilibre d’influence des administrateurs par rapport aux attachés parlementaires.
Quels impacts sur le bon fonctionnement et l’efficience de l’institution dans l’élaboration des textes (fond et forme) ? 
4 – de l’articulation entre élaboration de la loi et information des citoyens.
Au-delà des quelques lois sociales ou sociétales qui , pour les plus polémiques monopolisent media et réseaux sociaux (en faisant abstraction des textes de lois organiques ou techniques), comment transforme-t-on des textes le plus souvent illisibles (cf. Légifrance par exemple) en documents lisibles, exprimant clairement les finalités (de l’Esprit des lois !), les règles, etc.. et les communique-t-on aux citoyens ? 
« Nul n’est censé ignorer la loi ! »
Quelle articulation existe-t-il entre le système d’éducation (cf. Condorcet), de communication, et la production surabondante des lois ? N’est-il pas du rôle des assemblées de se préoccuper de l’information des citoyens et de vérifier que les moyens de communication et d’éducation sont efficients (pédagogie, intégrité des textes etc…)?

suite dans le commentaire 2

Score : 0 / Réponses : 0

----

commentaire 2 

suite...

5 – de l’implication des citoyens dans l’élaboration des lois, du suivi de leur mise en œuvre et de leur efficience.
Le développement du numérique offre indubitablement des technologies en capacité d’améliorer le fonctionnement démocratique de nos institutions et la participation des citoyens à l’élaboration et à la mise en œuvre des lois qui sont censées contribuer au meilleur fonctionnement de la société.
Ne faudrait-il pas, en écho aux points 1 et 4 de notre questionnement, trouver les leviers qui pourraient faire se mouvoir la masse des citoyens en prenant en compte, notamment les quatre conditions bien connues :Vouloir, Pouvoir, Savoir, Devoir, et s’échapper de la logique jacobine entretenue et acceptée des démarches descendantes (« top down »), sans aller vers un illusoire ascendantes (« bottom up ») pour imaginer un utopique d’égal à égal (« peer to peer ») entre citoyens (lesquels ?) et les pouvoirs publics (législatif, exécutif) ?
En conclusion…
Une question de nature téléologique, synthèse brutale des points exprimés ci-dessus (qui appelleraient indubitablement quelques développements) : cette initiative ne serait-elle pas purement cosmétique dans une réponse conjoncturelle aux questionnements portés lors des dernières campagnes électorales ?
Espérant que ce n’est pas le cas nous apporterons nos modestes contributions.

Pour l’Observatoire Citoyen.
Bernard CAUBERE

Score : 0 / Réponses : 0

----

je n'ai pas tout compris des possibilités que va nous offrir ce système pour intervenir sur les décisions de l'Assemblée nationale. C'est vrai qu'en 2-0 je suis assez nulle. Par contre comme beaucoup l'ont dit je n'ai vu sur mon courriel qu'aujourd'hui l'annonce de ce nouveau "pouvoir" donné au peuple. 2 jours avant la fermeture, c'est un peu court. et ça limite les inscriptions. Je souhaite aussi que les explications soient plus claies, plus courtes et accessibles à ceux qui n'ont pas bac + 8.. Merci

Score : 0 / Réponses : 0

----

Pour de multiples raisons cette initiative parvient à la connaissance des gens beaucoup trop tard,Il est essentiel d'allonger le délai après le 6 novembre.

Score : 0 / Réponses : 0

----

Intéressante initiative. Nous verrons bien si nous pourrons bien travailler ensemble et faire changer les choses pour que nos  concitoyens puissent vivre mieux surtout les plus modestes et vulnérables!!

Score : 0 / Réponses : 0

----

L'initiative me semble intéressante. Les remarques que je souhaitais faire ont déjà été faites, alors à quoi bon...

Score : 0 / Réponses : 0

----

Excellente initiative qui malheureusement n'est absolument pas connue du public. Il faudrait que cette information soit relayée sur les chaînes de télévision publiques.

Score : 0 / Réponses : 0

----

Je souhaite que de telles consultations aient lieu périodiquement, à un rythme d’une à deux par an, et que la population soit informée à l’avance de leur existence et de la possibilité pour tous d’y participer.

Score : 0 / Réponses : 0

----

Je découvre au moment où  l 'inscription est fermée !  Dommages! Pour une citoyenne autodidacte, il sert nécessaire d 'avoir plus de temps pour analyser cette proposition! Plus de temps! Il faudrait l.'annoncer publiquement et prolonger la période des inscriptions! Dans le principe, j 'adhère  complètement !

Score : 0 / Réponses : 0

----

excelente initiative j espere que nous serons tres nombreaux nous sommes tous concernés , bon mais tous n ont pas vraiment la possibilité de se deplacer et il faut la laisser en ligne bcp plus longtemps  le temps apartis pour s inscrire est trop court le bouche a oreille vas fonctionner mais il faut plus de temps et puis pourquoi les commentaire ne sont autorisé que si l ont est inscrit sur faceboock moi je n y suis pas PAR CHOIX PERSONNEL  donc je ne peut avoir acces a tous  !!!! a part google plus et change org je ne vais pas par conviction sur les reseaux sociaux senssible comme fb  un site dangereux dont ont ne peut jamais suprimer quoique çe soit et dela pour moi c est non par contre les commentaire les avis de change org m interressent au plus au point

Score : 0 / Réponses : 0

----

Bonjour à tous et à toutes ainsi qu'à nos législateurs,
Pourquoi un  délai si court pour une consultation d'une si grande importance dans sa finalité ? Je n'ai découvert celle-ci que le 4 novembre pour une clôture au 6 novembre..... A-t-on réellement envie d'améliorer la démocratie directe en France  ?  Je souhaite que le délai d information sur cette consultation soit prolongé au moins de 2 mois, que l'on utilise à minima les médias publiques pour diffuser cette information durant cette période ainsi que bien sûr le net. A ce propos parler de démocratie numérique c'est bien, mais a-t-on pensé qu'il existe une fracture numérique en France  ? Il n'est pas à la porter de tous de s'équiper en box, pc.. abonnement...etc... Et quand on l'est, mieux vaut se situer en zone desservie ou couverte...... 2 sujets qu' il faut absolument aborder si l'on ne veut pas laisser une partie d'entre nous sur le bas côté de l'Égalité...
Salutations citoyennes...

Score : 0 / Réponses : 0

----

Urgence ne donne aucune garantie à  la démocratie  participative et ressemble à un rideau de fumée pour mieux nous rendre muet

Score : 0 / Réponses : 0

----

Bonjour. Je viens seulement de m'inscrire sur cette plateforme citoyenne. Non pas que j'ai longuement hésité mais tout simplement, je n'étais pas du tout informé, alors que je "furète" beaucoup sur les sites d'information. Pourquoi n'avoir pas lancé une campagne d'information sur tous les médias ? Une campagne d'affichage ? Nous sommes bien dans une initiative citoyenne qui vise à donner plus de démocratie à notre République ? Je ne veux pas croire que cette initiative ne soit pas acceptée par nos élus(es)...

Score : 0 / Réponses : 0

----

Bonjour,
Cette initiative correspond aux attentes fortes des électeurs. Dommage cependant qu'il y ai très peu de communication sur le sujet que je découvre seulement grâce à la communication de notre députée.
Pour être légitime et être un vrai succès, il paraît nécessaire de prolonger la consultation  et de communiquer de façon importante sur tout type de médias ,via nos parlementaires , nos mairies et lieux
publics à forte affluence.
Sur le fonctionnement de la plateforme.
- sans moteur de recherche il va être rapidement difficile de s'y retrouver et surtout d'exploiter les propositions et réponses aux propositions formalisées. Sur le seul sujet initiative citoyenne, il est déjà difficile d'avoir une synthèse, demain lorsque les sujets seront libres comment s'y retrouver et contribuer efficacement et à nouveau exploiter.
- la plateforme est numérique, de ce fait certains électeurs sont exclus. Quel dispositif pour ceux qui n'ont pas accès au numérique, quel dispositif pour les malvoyants, sourds... (intégrer le vocal?, autres?).
- les documents en annexes sont nécessaires , leur forme ´fiche technique est  accessible à toute personne ayant l'habitude de lire de tels documents, mais trop rébarbatifs pour de non  initiés, nos startups utilisent des techniques  de communication bien plus visuelles .
-comme pour toute plateforme numérique, un cadre de bon usage et donc une surveillance seront sans doute  nécessaires.

Score : 0 / Réponses : 0

----

Une consultation mise trop confidentiellement en place de façon que peu de personnes ne soient au courant ?

Score : 0 / Réponses : 0

----

Quel dommage de ne pas avoir pu faire connaitre cette initiative au plus grand nombre. il faut communiquer et prolonger.

Score : 0 / Réponses : 0

----

Très bonne innovation

Score : 0 / Réponses : 0

----

Bonjour, 
Je rejoins les derniers propos : il n'y a pas eu de communication suffisante autour de ce dispositif qui est novateur et va dans le sens de la démocratie participative.Dans cet esprit, pour que les contributions soient le reflet de l'ensemble des strates de notre société, il faudrait donner la possibilité que des groupes de travail sans étiquette politique puissent se tenir sur l'ensemble du territoire, l'objectif étant de permettre à eux qui sont éloignés des nouvelles technologies puissent s'exprimer.Un rapporteur désigné pourrait alors poster la synthèse de ces travaux sur ce type de plateforme.

Cordialement
Gilles

Score : 0 / Réponses : 0

----

Cette consultation ne devrait pas avoir de date limite. Cela devrait fonctionner tout le temps, de sorte que 100% des gens aient la chance de participer. Et participer continuellement année après année

Ce pays appartient à tout le monde pas seulement une petite minorité qui nous gouvernent

Où est l'égalité? la fraternité? la liberté? Quand seulement une petite minorité participe ?

J'ai entendu parler de cette consultation le 4 novembre, le 6, il se termine! Comment cela peut-il être une consultation sérieuse? 67 millions de personnes dans le pays et seulement quelques milliers participent tout ce que je peux penser est que

BIEN TROP SOUVENT
LA DEMOCRATIE 
C'EST LA TYRANNIE 
DE LA MINORITE
DRAPEE DANS LE MANTEAU
DE LA MAJORITE

Score : 0 / Réponses : 0

----

Bonjour à tous.
Je suis pour cette initiative afin que les députés (et pas nos députés) connaissent l'opinion des citoyens des Français.
À cela, je me permets d'émettre des nuances ou des rectifications ou des compléments à ce qui a été écrit dans des commentaires en rapport évidemment à mes convictions.
-1- Nous n'élisons les députés pour être nos portes paroles. Nous votons untel ou untel car il se rapproche le plus de nos opinions politiques, sociétales. Faut-il encore que les candidats soient francs du collier et pas prêt à devenir godillot.
Cela signifie donc qu'ils ne chercheront à représenter ceux qu'ils les ont installés dans leur siège de députés mais la ligne du parti et/ou du gouvernement en place !
De fait, les citoyens deviennent des sujets pour la RF.
Cependant, nous pouvons et nous devrions toujours écrire, maintenir la pression auprès de nos députés pour ne pas leur faire oublier le sens commun !
-2- Je dis attention "la somme des intérêts particuliers ne vaut pas (toujours) l'intérêt général pour ne pas dire presque jamais". Cela à tout échelons : de la commune à la nation. Un individu répondra, clamera par rapport à ses connaissances sur le sujet plus selon ses besoins. Nous n'avons pas toujours les moyens de prendre la hauteur nécessaire et le temps nécessaire pour définir ce qui peut-être nécessaire pour un groupe de citoyens ou la nation entière.
Pour cela, il existe des personnes qui étudient tels ou tels sujets, qui rencontrent du monde leur permettant de pouvoir prendre du recul et donner la solution la plus adaptée à la situation pour un temps donné. Nommons les experts dans le monde économique, syndical, ..... S'ils le font avec une intégrité intellectuelle, donnons leur ce mérite et les remerciements.
De fait, l'utilisation par nous tous, des pétitions et la remontée au niveau du CESE, de l'Assemblée Nationale et du Sénat. Par ricochet, au niveau du gouvernement est bien, si ce n'est important mais il faut les remettre à leurs juste places parmi tant d'autres. J'appellerai cela : interpellation, lobbying populaire, #nefaitepasn'importequoi, #lepeupleexiste. Ces pétitions ont leur places, mais attention donnons leur une bonne place et pour chaque citoyen qui la signera, avec en ligne de mire : le sens commun, l'esprit générale, et ne pas exclure des personnes qui constituent le corps de la nation française. 
Ne faisons pas ce que font actuellement et le président de la RF et les membres du gouvernement : le favoritisme pour une seule classe de la population française sans aucune contrepartie (cette deuxième partie de la phrase est le plus important) : les personnes aisées à très aisées financièrement !
Cordialement à vous tous.

Score : 0 / Réponses : 0

----

plutôt discret ? je prends connaissance de cette initiative ce jour !

Score : 0 / Réponses : 0

----

Très bonne initiative que je découvre aujourd'hui après une absence de deux jours seulement sans ordinateur ! Je ne comprends pas pourquoi ces propositions n'ont pas été exposées au public plus tôt afin de nous donner le temps d'en prendre connaissance, d'y réfléchir et de trouver le temps d'en discuter pour, finalement, donner une réponse. On n'arrivera jamais à quoi que ce soit de positif si on fait les choses dans la précipitation et sans penser à tous les à côté. Il m'a manqué du temps pour analyser le tout et voter. Dommage.

Score : 0 / Réponses : 0

----

Je suis d'accord avec Christophe Cazin.
Pour ma part je crains d'avoir des difficultés à naviguer sur ce site, en particulier je n'ai pas trouvé moyen de joindre à ma proposition un dossier sous forme jpg ou png.

Score : 0 / Réponses : 0

----

Je n'ai appris l'existence de cette consultation citoyenne qu'il y a 2 jours Pourtant, je suis très" connectée" sur ce genre d'initiatives: il me semble aussi qu'elle n'a pas été suffisamment relayée auprès du grand public, et qu'il faudrait lui laisser un peu plus de temps pour" décoller".  Merci en tous cas de l'avoir lancée.

Score : 0 / Réponses : 0

----

Saluons et soutenons ce que cette initiative a de pertinent: 
> Consolider et renforcer la prise en compte des pétitions en ligne par les représentants élus;
> Définir les critères de recevabilité;
> Participation des auteurs aux auditions parlementaires ad hoc.

En revanche sachons être réalistes et efficaces pour éviter de  transformer le travail parlementaire en travail d'examen d'une multitude de propositions spécifiques plus ou moins bien étayées:
> Puisque nos députés sont nos représentants, ils diposent d'une délégation de notre part, non pas au niveau de leur circonscription (ceci relève du mode d'élection) mais au niveau national. Il ne faut donc les solliciter que pour trancher. Par contre un rôle renouvellé du Sénat pourrait être défini pour l'examen des pétitions recevables et transmission à l'Assemblée, avec avis,  pour éventuelle proposition ou amendement législatif.
>  Le seuil de recevabilité doit être raisonnable pour éviter qu'un groupe de pression, un lobby ne se serve du dispositif pour encombrer, infléchir, paralyser le travail parlementaire. Le seuil de 1 million de signatures (soit moins de 2,5% des électeurs) parait de nature à éviter les débordements. Ce seuil pourrait éventuellment être abaissé sur recommendation motivée de la commission d'examen des pétitions, lorsque la pétition concerne un sujet de société particulièrement aigu et débattu.
> Ce n'est qu'au delà d'un seuil de 15% des électeurs inscrits que la référendisation de la demande pourrait être envisagée sur proposition commune d'au moins 25% des députés et sénateurs adressée à l'exécutif.

Score : 0 / Réponses : 0

----

Je découvre cette consultation quelques heures avant sa fermeture grâce à un billet sur le réseau social Diaspora.  Dommage car je m'intéresse à la participation des citoyens au débat législatif, notamment à regardscitoyens.org et nosdeputes.fr.  Ne peut on pas poursuivre la consultation quelques semaines?

Score : 0 / Réponses : 0

----

Reçu l'information sur cette plateforme aujourd'hui mardi 7 novembre 2017 et je constate que la consultation était ouverte du 10 octobre au 6 novembre 2017. Ce n'est pas avec un temps de consultation aussi court et aucune communication sur les médias que vous arriverez à développer une démocratie participative si tel est votre objectif. Permettez moi d'en douter vu l'inexistence de moyen mis en œuvre.

Score : 0 / Réponses : 0

----

Nos libertés sont de plus en plus restreintes,  pas moins de 11 vaccins obligatoires, malgré tous les scandales  sanitaires!  Nous ne savons pas ce que nous mangeons, les lobbies font la loi, Monsanto détruit la planète, appauvrit les paysans, ruine notre santé, les banques détiennent tous les pouvoirs. Donnons un statut aux pétitions qui sont notre seul moyen de pression, pour le moment.

Score : 0 / Réponses : 0

----

Pour moi, les problèmes abordés aujourd'hui par le législateur appellent des solutions si techniques ou complexes qu'il n'est plus à la portée de l'individu lambda de s'improviser "fabricant des lois". On peut tomber facilement dans le "Yakafokon". L'autre écueil est le nombre : pour avoir réalisé des centaines de synthèses d'entretiens, j'imagine la charge de travail de ceux qui auront à gérer des milliers de propositions et de réponses ouvertes. Embolie assurée de travail, de temps et de coûts ! Le rôle d'une telle plateforme devrait se limiter à des sondages/référendums automatisés pour éclairer certains points litigieux ou incertains des textes de loi. Les propositions de loi devraient être plutôt adressées par chacun à son député, en ligne mais dans le respect d'un process et d'un format identiques pour tous les parlementaires  (par expert,  format d'un cahier des charges type).

Score : -1 / Réponses : 1

----

La démocratie "participative numérique" me semble dangereuse pour la Nation. Outre, les questions de sécurité liées au numérique, il reste la compétence effective des citoyens sur les questions qui seront posées. Une limitation "capacitaire" devrait elle être imposée ? Si oui, sous quelle forme ? Par ailleurs, la question de l'information "objective et complète" du citoyen doit être posée avant d'étudier toutes les modalités participatives. Enfin, ne risque t on pas de "dévaluer" le rôle des députés en organisant une sorte de concurrence directe de leurs pouvoirs et ainsi d'affaiblir notre démocratie ? Ne pourrait on pas se limiter à un référendum d'initiative populaire par lequel à partir d'un certains nombre de signatures il y aurait saisine des députés sur un sujet particulier mais les députés garderaient la main sur le fabrication de la loi?

Score : -6 / Réponses : 7
