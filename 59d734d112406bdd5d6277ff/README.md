## Quel rôle pour les citoyens dans l’élaboration et l’application de la loi ?

### Consultation pour une nouvelle Assemblée nationale

# Initiatives citoyennes

<div style="text-align: justify;"><img src="https://consultation.democratie-numerique.assemblee-nationale.fr/an/initiatives_citoyennes.png"></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">Aujourd’hui, l’agenda politique et l’ordre du jour législatif sont majoritairement décidés au sein des institutions. En dehors des périodes d’élections, les citoyens ont peu d’occasions de soumettre des sujets à la discussion publique ou de rythmer le calendrier des travaux parlementaires.</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">&nbsp;</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">Dans de nombreux pays, le numérique a inspiré de nouvelles méthodes ou facilité le recours à des mécanismes existants pour permettre l’initiative citoyenne et susciter l'intérêt de chacun pour les affaires publiques. Des plateformes de pétition en ligne ont par exemple été créées à cette fin.&nbsp;</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;"><span style="color: rgb(107, 36, 178);">&nbsp;</span></span></div>
<div><i style="font-size: 11pt;"><span style="color: rgb(107, 36, 178);">Pétitions en ligne, référendum d’initiative populaire, propositions de loi citoyennes, questions au gouvernement citoyennes… : quelles sont les propositions à mettre en œuvre de manière prioritaire ? </span></i></div>
<div><br></div>
<div style="text-align: justify;"><span style="font-size: 10pt;"><span style="color: rgb(230, 0, 0);">Nota bene</span>&nbsp;: par défaut, les contributions les plus populaires apparaissent en tête de la page par ordre décroissant. Vous pouvez si vous le souhaitez afficher les contributions les plus récentes ou les plus anciennes. Pour cela, modifiez le mode de </span><u><b><span style="font-size: 10pt;">classement</span></b></u><span style="font-size: 10pt;"> à droite de «&nbsp;Commentaires&nbsp;» avant la 1ère contribution.</span></div>
<div><br></div>

#### Commentaires

Le droit de pétition à l’Assemblée nationale, qui existe depuis 1958, doit être clarifié et renforcé de la façon suivante : 

1. Prise en compte des pétitions en ligne pour tenir compte des usages actuels en terme de mobilisation citoyenne en France
2. Définition de critères transparents sur la recevabilité des pétitions (exemple : lorsqu’elles portent sur des propositions de loi en cours ou en débat, lorsqu’elles apportent un point de vue différent de celui des organisations syndicales, patronales ou des associations, lorsqu’elles fournissent une expertise technique ou citoyenne supplémentaire) 
3. Réponse obligatoire à toutes les pétitions déposées et communication claire et motivée lorsqu’une pétition est jugée non recevable
4. Définition d’une fréquence d’examen des pétitions (par exemple : une pétition étudiée chaque mois)
5. Invitation des auteurs des pétitions examinées à participer aux auditions organisées lors des travaux parlementaires, afin de valoriser leur expertise citoyenne 

Depuis août 2017, le Conseil Économique Social et Environnemental a décidé de prendre en compte les pétitions en ligne : 
http://www.lecese.fr/content/lecoute-des-citoyennes-mission-fondamentale-du-cese

Aujourd’hui, c’est au tour de l’Assemblée nationale de donner un débouché législatif aux pétitions citoyennes, à travers les modalités proposées ci-dessus.

Score : 3385 / Réponses : 139

----

Instauration du Référendum d’Initiative Citoyenne :
En démocratie, le peuple doit être le législateur en dernier ressort. Seul le référendum d'initiative citoyenne nous permettra de reprendre la parole pour décider de ce qui nous regarde, et ainsi de devenir pleinement citoyens.
  
Nous vous proposons une nouvelle formulation de l’article 3 de la Constitution : 
“La souveraineté nationale appartient au peuple qui l’exerce par ses représentants et par la voie du référendum d’initiative citoyenne en toutes matières, y compris constitutionnelle et de ratification des traités. Cet article ne peut être modifié que par voie référendaire.” 
 
(En savoir plus : http://www.article3.fr/informations/definition-du-ric)

Score : 748 / Réponses : 33

----

Obligation pour l’Assemblée nationale d’étudier une pétition dès qu’elle atteint 100 000 signataires comme en Grande-Bretagne.
 Organisateurs et organisatrices de la pétition doivent être invités à l’Assemblée nationale pour être écouté en audition et travailler dans les commissions en rapport avec la pétition.

Score : 654 / Réponses : 28

----

LA QUESTION CITOYENNE AU GOUVERNEMENT :
Il faut que, chaque semaine à l'Assemblée nationale, les citoyens puissent poser une question au Gouvernement.

Score : 367 / Réponses : 41

----

Comme en Suisse : des citoyens peuvent proposer directement une initiative de loi, si le nombre de signatures de la pétition est suffisant alors l'Assemblée et le Gouvernement doivent impérativement se prononcer.
S'ils refusent, alors un référendum populaire est obligatoirement proposé, et les citoyens peuvent soit accepter la proposition de loi soumise par les pétitionnaires, soit accepter l'éventuelle contre-proposition du Gouvernement, soit refuser les deux - et le résultat du référendum est souverain.

Score : 339 / Réponses : 18

----

**L’article 3 : pour une reconnaissance constitutionnelle du pouvoir citoyen des votations et référendums obligatoires**

L’article 3 de notre constitution réserve tous les pouvoirs à nos représentants élus, en effet le référendum aujourd’hui défini dans la constitution est toujours à l’initiative des élus, y compris le référendum d’initiative partagé de l’article 11 qui est de fait à l’initiative des parlementaires. Si nous voulons donner aux citoyens la possibilité de s’immiscer dans l’agenda politique il faut donc déverrouiller ce monopole des élus. Ceci est vrai au niveau national mais aussi au niveau local ou le droit de pétition de l’article 72-1 ne contraint pas les élus locaux à mettre en place des référendums.
Un point majeur est la définition des règles et de l’espace commun, à savoir les évolutions constitutionnelles ou les transferts de compétences y compris au niveau local. A ce sujet il me parait indispensable que toutes les décisions de changement de cadre doivent faire l’objet de référendum.

**Proposition d’article 3 :**

La souveraineté nationale appartient au peuple qui l'exerce, à tous les niveaux de la république, par ses représentants élus et par la voie du référendum ou de la votation d'initiative citoyenne en toutes matières : constituante, législative, décisionnelle, budgétaire, révocatoire et abrogatoire.

**__En conséquence d’autres articles de la constitution doivent être modifiés, sans être exhaustif on peut citer :__**

**L'article 11** qui doit être modifié pour remplacer le référendum d’initiative partagée par les principes de fonctionnement des initiatives citoyennes.

Le premier alinéa de **l'article 24** qui pourrait devenir : « La loi est votée par le Parlement ou par votation d’initiative citoyenne… ».

De même pour le premier alinéa de **l'article 39** : « L’initiative des lois appartient concurremment au Premier ministre, aux membres du Parlement et aux citoyens. ».

**L'article 60** serait ainsi modifié : « Le Conseil Constitutionnel veille à la régularité des opérations de referendum et de votation prévues aux articles 3 et 89. Il en proclame les résultats. ».

**L’article 72** alinéa 3 pourrait être : « Dans les conditions prévues par la loi, ces collectivités s'administrent librement par des conseils élus ou par les citoyens et disposent d'un pouvoir réglementaire pour l'exercice de leurs compétences. »

**L’article 72-1** alinéa 1 transformerait le droit de pétition en initiative citoyenne avec obligation de prise en compte ou de référendum

L’article 72-1 alinéa 3 rendrait la consultation des citoyens obligatoire

Le premier alinéa de **L'article 89** pourrait être : « L’initiative de la révision de la Constitution appartient concurremment au Président de la République sur proposition du Premier ministre, aux membres du Parlement et aux citoyens. » et le paragraphe permettant la ratification par le congrès serait supprimé.

Score : 247 / Réponses : 7

----

Il pourrait être créée une plateforme officielle et protégée de l'Assemblée Nationale qui permettrait à chaque personne ayant le droit de vote (identification avec le n° de la carte électorale par exemple) , de proposer des idées, faire des propositions de loi. Sur les propositions ayant reçu par exemple  1 millions de votes, l'Assemblée Nationale serait obligée de débattre le texte.

Score : 189 / Réponses : 15

----

De manière générale, pour donner de l'attrait à un outil d'initiative populaire quel qu'il soit, il me semble crucial de donner un accès facile à l'état d'avancement, et a terme à la conclusion finale.

On peut faire un parallèle avec KissKissBankBank (ou KickStarter), qui propose de soutenir des projets en cours de réalisation : une page permet de présenter le projet, une page permet d'avoir des nouvelles de l'avancement du projet (actualités), et un résumé très synthétique de l'état actuel est affichés en permanence.

Cette capacité à suivre l'évolution d'une proposition participe à la transparence du système, et donc le rendre plus attractif, pousser les citoyens à participer plus (ou révéler que le système ne fonctionne pas si tel est le cas).

Il faudrait également que tous les outils mis en place se conforment à la politique "open data" mise en place par le gouvernement (accès aux données dans un format facilement exploitable), autant pour les propositions elles même que pour leur état d'avancement.

Score : 136 / Réponses : 3

----

Instaurer le référendum d'initiative citoyenne et le droit des citoyens de proposer une loi

Score : 124 / Réponses : 1

----

Cette consultation repose sur une plateforme libre dont le code source est ouvert, ce qui est une très bonne chose. Un guide, ou un outils à disposition, permettant d'implémenter facilement ce type d'outils par des petites collectivités permettrait d'assurer une consultation en ligne régulière sur des sujets locaux également. Ce type d'outil pourrait également être mis à disposition d'associations ou de collectifs de citoyens pour pouvoir échanger, confronter et ainsi faire des propositions co-construites et enrichies par un grand nombre. 

D'autre part, une pétition ne vaut pas grand chose s'il est impossible de justifier de l'identité des signataires (notamment pour s'assurer qu'il n'y a pas des signatures à la chaine d'une même personne), mais les outils qui le permettent sont inaccessibles (coûts, difficultés techniques), l’État doit  proposer des systèmes de vérification de l'identité pour mettre en place des pétitions ayant un caractère valable (carte d'identité électronique, France Connect). Ceci permettrait :
1- De s'assurer que la mise en ligne de pétition est possible par tous (citoyens, associations, collectifs) quels que soient leurs moyens techniques et financiers
2- Éviter que des géants privés du secteur n'exploitent les données personnelles ainsi récoltées

Score : 119 / Réponses : 6

----

Il faudrait d'avantage organiser des référendums d'initiative populaire concernant certains projets de lois et même au niveau local (régions, départements, communes...). On pourrait demander aux citoyens leur avis sur la sortie ou non du nucléaire par exemple ou sur une modification de la Constitution (au lieu de réunir le Congrès, cela impliquerait certainement moins de frais). Ensuite un autre point qui me semble important : lors de la réforme territoriale voté sous le mandat de M. Hollande, à aucun moment les citoyens ont été consulté pour savoir s'ils étaient d'accord ou non avec ce changement. Il aurait été judicieux par le gouvernement d'organiser des consultations pour connaître l'avis du peuple français. En Allemagne et e  Suisse, ce type de consultation fonctionnent très bien alors pourquoi pas en France ? Si vous voulez faire participer les français a la vie politique du pays c'est bien un des premiers changements qu'il faudrait envisager de faire. Notre pays, pour mieux fonctionner, devrait d'avantage donner une plus large autonomie aux régions. L'exemple allemand est à suivre de ce côté là. Une révision de la Constitution serait envisageable. Le pouvoir ne doit pas être exercé par un seul groupe mais bien par l'ensemble des français en âge de voter. Chacun peut apporter sa pierre à l'édifice de notre Nation afin qu'elle se renforce au niveau national et international.

Score : 93 / Réponses : 18

----

Référendum: Pour toute modification de la constitution ou des traités européens, un recours au Referendum d’approbation devrait être obligatoire.

Score : 91 / Réponses : 7

----

A la question posée ci-dessus en violet:
"Quelles sont les propositions à mettre en œuvre de manière prioritaire ?"
La réponse me semble s'imposer: Le référendum d'initiative citoyenne en toutes matières y compris contitutionnelle et de ratification des traités; 
C'est en effet une réforme tout à fait singulière  puisqu'avec elle les citoyens pourront obtenir TOUTES les autres. MAIS exclusivement si elles sont bien " "l'expression de la volonté générale" constatée par un scrutin classique.                          Tous les participants de cette plateforme devraient la soutenir ne serait-ce que pour pouvoir proposer "utilement" leur revendication spécifique jugée "importante"  par eux et qui l'est peut-être dans la population et pas parmi les élus.. Exemple le suicide médicalement assisté, refusé depuis plus de 30 ans par le Parlement et qui obtient des scores énormes dans les sondages.

Score : 77 / Réponses : 4

----

Il y a plein d'idées intéressantes. Mais il me semble qu'il faut privilégier celles qui favorisent le débat : c'est à dire des systèmes où les citoyens échangent pour arriver - ou non - à une position commune, où ils peuvent changer d'avis grâce aux autres participants. 
C'est la raison pour laquelle je m'oppose à ces systèmes clivants, comme le référendum, où on crée de l'opposition, alors que la solution n'est pas forcément oui ou non (d'autant plus qu'on choisi rarement la question). On a tendance à résumer la démocratie au vote, alors que c'est bien plus que ça. Il faut qu'on mette en place des instruments qui complexifient la démocratie et l'enrichisse. 

Comme l'écrit le constitutionnaliste Dominique Rousseau  "L’air du temps politique est à une équation simple : référendum = expression directe de la volonté du peuple = démocratie. Au risque de provoquer des orages, il faut pourtant mettre en doute ou à tout le moins à l’épreuve cette affirmation."

http://www.laviedesidees.fr/L-equivoque-referendaire.html

Score : 70 / Réponses : 21

----

Je suis un peu étonné de la forme de cette consultation : tout le monde sait depuis des années qu'une très large majorité de Français est tout à fait favorable à l'instauration de référendum d'initiative populaire . Plusieurs sondages l'ont mis en évidence d'une manière que personne n'a contesté .
 Il n'y a pas non plus de problème technique insurmontable à cette instauration puisque plusieurs pays l'utilisent de façon habituelle .
 Le seul frein est de nature politique : en France les politiciens professionnels n'ont pas envie que la population se mêle des décisions politiques . 
 La question n'est donc pas d'interroger les gens sur ce qu'ils veulent mais de mettre en place ce qu'ils demandent !
(Pour en savoir plus, lisez "Pour une démocratie directissime ?", Robert Boulloche)

Score : 69 / Réponses : 3

----

Interdir tout cumul de mandat.

Score : 67 / Réponses : 4

----

INSTITUER UNE PLATEFORME DE L’INITIATIVE CITOYENNE

Une plateforme numérique centralisant toutes les possibilités d’initiatives citoyennes doit être créée.

Quant à l’initiative citoyenne, la question n’est pas tant celle du type d’initiative (pétition citoyenne, initiative législative, demande de débat législatif, etc.) que celle de sa gestion.

Un site institutionnel, rattaché au Premier ministre, sera chargé de la gestion de toutes les initiatives possibles, en fonction de leurs types et de leurs sujets. Grâce à cette centralisation, le citoyen saura où s’adresser pour lancer une initiative. Quant au Gouvernement, il pourra les traiter de façon unique, systématisée et organisée.

D’abord, la loi précisera les différents types d’initiatives offertes aux citoyens et les cas ou les sujets dans lesquels il est possible d’y recourir. 
Ensuite, les citoyens s’identifieront sur la plateforme numérique, grâce à leur identification unique [http://consultation.democratie-numerique.assemblee-nationale.fr/topic/59d731a712406bdd5d6277d1#comment-59f72e80e8a9656c1a8e73b3]. Ils pourront alors consulter les initiatives en cours, en lancer de nouvelles, ce qui ouvrira une possibilité de recueil des soutiens.

Enfin, dans les conditions fixées par la loi, ces initiatives déboucheront sur un débat au Parlement, le dépôt, voire l’examen d’une proposition de loi, l’organisation d’un referendum ou, à l’inverse, seront abandonnées si les conditions nécessaires ne sont pas remplies.

Autres propositions dans le cadre de cette consultation : http://constitutiondecodee.blog.lemonde.fr/2017/10/30/participation-numerique/

Score : 52 / Réponses : 1

----

M. CASTANER, porte parole du gouvernement, et pas encore à l'époque " délégué général" du parti du Président, a annoncé chez JJ.BOURDIN sur RMC le 30.08.2017: : “ la fin d’une époque.” affirmant: “Et donc aujourd’hui on veut sortir de cette théorie des droits formels qui sont sur le papier mais qui ne sont pas la réalité.”

    Le CLIC (Comité de Liaison pour l’Initiative Citoyenne) a alors demandé au premier ministre de transformer le droit théorique de l’article 3 de la Constitution: “la souveraineté nationale appartient au peuple” … en un droit réel en complétant l’article 3 par le référendum d’initiative citoyenne en toutes matières y compris constitutionnelle et de ratification des traités. Nous avons eu une " réponse" le 30.10.2017. Le 1er minitre à pris connaissance de notre courriel! Il doit donc être en réflexion...Le CLIC réécrira...
 Ce droit réel qu'est la RIC en toutes matières, instaurera tout simplement une véritable démocratie en France!

  83% des Français sont favorables au RIC selon un sondage IFOP focus n°158 d'avril 2017, qui ne fait qu'en confirmer deux autres plus anciens à 82 et 88%...

Score : 45 / Réponses : 3

----

S'inspirer de systèmes qui fonctionnent déjà ailleurs :
1)  Créer une carte d'identité numérique infalsifiable et communicante (Estonie)
2)  Mettre en place le vote électronique sécurisé par blockchain (Estonie)
2)  Développer les "votations" ou référendum d'initiative populaire (Suisse)
3)  Rendre électifs les postes clé de l'administration : procureurs, chefs de la police, juges ... (USA)
4)  Rendre obligatoire la consultation des citoyens sur les budgets, à tous les niveaux de la structure de l'Etat : communes, départements, régions, état.

Score : 43 / Réponses : 12

----

Les conditions qu'impose la constitution sur le référendum d'initiative partagée sont proprement irréalisables sur le temps restreint d'une mandature électorale imposée par la cinquième République. Un cinquième des parlementaires c'est-à-dire 185 parlementaires (députés ou sénateurs) associés à 4 570 000 électeurs c'est-à-dire 10% des citoyens inscrits sur les listes électorales. Ceci explique que cette procédure n'ait pas encore été utilisée. Si l'initiative du Président et de sa majorité actuelle est de réellement donner accès à l'initiative citoyenne, alors le droit doit évoluer vers une procédure qui soit réaliste et réalisable. Si les citoyens doivent disposer de cette possibilité c'est justement parce que les parlementaires ne proposent pas les lois que les citoyens pourraient proposer. Devoir associer 185 parlementaires à cette initiative c'est rendre quasiment caduque cette procédure. 
Par ailleurs, le dernier référendum auquel les citoyens ont participé sur le traité européen n'a pas fait l'objet d'une prise en compte de l'avis des citoyens qui avaient voté par la négative. Il y a donc là matière à réfléchir sur la portée que les élus de la cinquième République donnent à l'initiative citoyenne. 
Si l'initiative citoyenne se résume à sélectionner quelques avis de citoyens de façon opaque et orientée pour apporter aux actions du gouvernement et de sa majorité une caution de "démocratie citoyenne" sans que les citoyens ne puissent exercer aucun pouvoir réel sur l'orientation des lois, alors ce processus au lieu de renforcer la participation citoyenne opérera comme un phénomène au mieux à la marge au pire comme une nouvelle démonstration que l'initiative citoyenne ne peut représenter un réel levier politique à l'usage des citoyens, ce qui sera préjudiciable à la démocratie, éloignant encore davantage les citoyens de la vie politique de leur pays.

Score : 41 / Réponses : 4

----

Il faut obliger l'Assemblée Nationale à étudier une proposition de loi lorsque celle-ci recueille un certain nombre de parrainages citoyens.

De la même manière, les citoyens doivent pouvoir révoquer un élu et l'obliger à repasser devant le suffrage populaire si celui-ci faillit à son rôle de représentant.

Score : 40 / Réponses : 0

----

Cette initiative dans l'air du temps presuppose que le gouvernement s'attelle, en parallele, a reduire la fracture numerique. Selon Eurostat, seulement 63% des Francais utilisent internet. Un pourcentage certainement bien inferieur correspond aux citoyens en age de voter. Les populations rurales et les CSP precaires seraient sous-representees dans le dialogue numerique. Il me semble que ceux n'ayant pas acces a internet devraient beneficier d'un espace de decouverte/capacitation a la plateforme participative au sein de leur commune.

Score : 38 / Réponses : 5

----

Instaurer le référendum d’initiative citoyenne.

Pour mettre des sujets à l’ordre du jour de l’Assemblée et reconnaître le droit de pétition numérique (à partir de 10 % voire 15% des inscrits). On pourrait envisager la mise en place d’une carte électorale numérique sécurisée (sous contrôle de la CNIL).

Score : 38 / Réponses : 3

----

Des plateformes gouvernementales de pétition ont en effet été créées aux [États-Unis](https://petitions.whitehouse.gov/), au [Royaume-Uni](https://petition.parliament.uk/), au [Canada](https://petitions.ourcommons.ca/), en [Jamaïque](http://opm.gov.jm/participate/jamaica-house-petition/)... La France a bien ouvert [un site pour l'article 11](https://www.referendum.interieur.gouv.fr/) de sa Constitution, mais il n'est malheureusement pas utilisé. La première chose à faire serait donc de permettre aux citoyens d'y déposer des propositions de loi, la seconde d'y associer un devoir de réponse du gouvernement une fois passé un certain nombre de signatures.

Score : 37 / Réponses : 2

----

LUTTE CONTRE ABTENTION ÉLECTORALE 
Je pense qu il serait intéressant de pouvoir voter de n’importe où en France grâce à sa carte d’électeur et sa carte identité ou passeport. Par le biai d’un fichier national des électeurs ( fichier qui serait numérique) disponible dans toutes les mairies de Métropole et d’outre mer un électeur partant en vacance aurait la possibilité de pouvoir voter sans se soucier dans quelle bureau de vote. 
Le vote serait alors sous le même format papier que nous utilisons aujourd’hui mais la feuille d’émargement serait alors numérique.

Score : 37 / Réponses : 6

----

Pourrions nous chosir des citoyens pris au hasard pour débattre des sujets type legislatifs permettant aux députés d'avoir l'avis du " peuple" sans le filtre des médias?   Ils pourront echanger. Ces échanges devront avoir lieu au sein de l'assemblée. Symobliquement, la democratie y gagnera. Les échanges permettront peut être d'introduire un peu de " bon sens populaire" sans pour autant tomber dans le populisme. Il est evident que ces echanges devont être bien cadrés.

Score : 37 / Réponses : 2

----

Sur les initiatives citoyennes, n'hésitez pas à vous inspirer du modèle Suisse. Pour ce faire vous pouvez consulter le **Guide de la démocratie directe**  : https://issuu.com/inxtensis/docs/iri_guide_demoractiedirecte_fr_2007 et notamment les recommandations exprimées dans les annexes p 184 à 189

Score : 36 / Réponses : 0

----

Référendum: 
La Constitution prévoit dans son article 11, § 3, la possibilité de propositions de 
loi initiées en partie par les citoyen·ne·s. Il faut que celles-ci soient « à l’initiative 
d’un  cinquième  des  membres  du  Parlement  »  (soit  185  parlementaires),  et  
« soutenues par un dixième des électeurs inscrits sur les listes électorales » 
(soit environ 4 millions de personnes). Jugez du peu. Si la proposition de loi n’a 
pas été examinée (et possiblement enterrée) par les deux assemblées dans un 
délai de six mois, elle est soumise à référendum.
En  réalité,  
tout  a  été  fait  pour  que  cet  article  n’ait  aucune  chance  d’être  
utilisé  un  jour  tant  l’initiative  populaire  est  verrouillée. 
Une évolution vers une VIe république nous permettrait de traiter ce genre de point afin de redonner une réelle importance aux initiatives citoyennes (Referendum d'Initiative Citoyenne (R.I.C.) en premier lieu, Révocation des élus, Pétitions en ligne par associations françaises loi 1901,...).

Score : 36 / Réponses : 5

----

La nouvelle Constitution dont la France a besoin doit être radicalement nouvelle, y compris dans sa méthode d'écriture : elle ne peut être un simple rafistolage de la 5e République, ni se résumer à quelques réformes octroyées par le président de la République. C'est le peuple lui-même qui doit s'emparer de la question et s'impliquer tout au long d'un processus constituant. Organiser la convocation d'une assemblée spécifiquement chargée de rédiger une nouvelle Constitution sous le contrôle des citoyens : une Assemblée constituante. Nous soumettrons à ses travaux des propositions pour une 6e République démocratique, égalitaire, instituant de nouveaux droits et imposant l'impératif écologique.

Score : 36 / Réponses : 5

----

Attention à la tentation de réduire le débat à des stratégies démagogiques... Le référendum oui/non appauvrit et idéologise toute problématique. Ce dont nous avons besoin ce sont d'outils de construction de propositions complexes, comme http://liquidfeedback.org par exemple. Il existe des recherches sur le sujet, il faut regarder ce qui se fait déjà.

Score : 36 / Réponses : 9

----

Suite à une réunion d'environ 40 citoyens de Paris, le premier constat réalisé est le fait que les initiatives citoyennes, dans le cadre de l’élaboration et l’application des lois, sont mal connues par les citoyens.  Alors même que des réformes constitutionnelles ont permis de mettre en place des initiatives citoyennes, il n’y a pas de communication sur les processus d’initiatives possibles.

Actions : 
-	Permettre au citoyen de se sentir concerné et lui donner une vraie place dans  l’élaboration et l’application des lois : 
o	Accentuer la communication sur les dispositifs déjà existants.
o	Mettre en place des communications spécifiques sur la compréhension de la citoyenneté adaptées à chaque âge de la citoyenneté : plateforme numérique, réunions/débats, évènements éducatifs, musée interactif de la citoyenneté.
-	Proposer une part de budget de l’Etat en budget participatif : donner la possibilité aux citoyens de proposer des projets à compétence étatique/législatif puis vote des citoyens sur une plateforme numérique et en préfecture

Score : 20 / Réponses : 0

----

*Contribution collective Café Citoyen : Présidée par Mme Nadia HAI Députée de la   11ème circonscription des Yvelines * 

**78-11-Q1 L'indispensable * plateforme d'initiative citoyenne* numérique et présentielle de recueil et organisation de ces initiatives**

La question n’est pas tant celle du type d’initiative (pétition citoyenne, initiative législative, demande de débat législatif, etc.) que celle de sa gestion, l'objectif est de ne pas créer de la frustration pour les citoyens. Les nouveaux instruments doivent **enrichir la démocratie** et veiller à ne pas la (sur)complexifier.
L'objectif principal est de favoriser le débat et les échanges,  ce sont des **outils de co-construction** qui sont recherchés numériques et présentiels (ateliers, forums, rencontres, débats). 
Réserver l'utilisation des référendums, qui clivent et opposent les citoyens, à des questions binaires finalement très exceptionnelles.
Les pétitions en ligne doivent être étudiées par une commission adhoc ou citoyenne (représentativité et pertinence, suites à donner).
Le CESE aujourd'hui (désigné pour recevoir et évaluer les pétitions), n'a qu'un pouvoir consultatif aujourd'hui, cela limite la portée de ses propositions. Le **rôle du CESE devrait être élargi à des initiatives législatives en amont** (vision, enjeux, objectifs, portés de projets de textes) qu'il transmettrait à la 9 eme commission permanente nouvelle de l'Assemblée Nationale (voir contributions 78-11-Q2). 
La question citoyenne au Gouvernement ne doit pas être « un gadget » . Un vote citoyen organisé doit permettre de désigner la question à poser. Les questions écrites au Gouvernement et à son député doivent être également prises en compte dans la gestion des réponses.

Score : 13 / Réponses : 0

----

Transparency International France recommande d’instaurer un droit de pétition national :
• En mettant en place une plateforme en ligne permettant aux citoyens d'inscrire des questions ou des propositions à l'ordre du jour des Assemblées parlementaires.
Sur le modèle des initiatives européennes ou locales, la plateforme de pétitions en ligne permettra aux citoyens de mettre à l’ordre du jour une question ou une proposition sur toute matière d’intérêt général entrant dans le champ de compétence des Assemblées s’ils recueillent au moins 350 000 signatures (soit 0.5% de la population). Ces conditions de recevabilité doivent être clairement définies (citoyens concernés, territoire, objet de la pétition, délais de traitement, caractère contraignant de la proposition validée) dans le dessein principal d’éviter de perturber  le  travail des assemblées  tout en assurant un processus transparent et de véritables obligations à la charge de ceux qui  en sont destinataires. 
• En s’assurant que les propositions validées fassent ensuite l’objet d’un suivi particulier avec un accompagnement juridique.

En Finlande, la plateforme « Open Ministry » les accompagne de l’émergence de l’initiative jusqu’à son dépôt officiel : formalisation juridique des propositions, évaluation de leur  impact et même définition de la stratégie de mobilisation.

Score : 10 / Réponses : 1

----

Remplacer le Sénat par un Conseil des Cinq Cents Citoyens tirés au sort qui examinerait tous les projets de lois d’initiative citoyenne.

Score : 4 / Réponses : 0

----

**Groupe de travail Initiative Citoyenne, réuni au Liberté Living Lab le 2/11/17 (15 citoyens)**
La question de la concurrence induite entre corps intermédiaires et citoyens par les nouveaux modes de participation numérique a été soulevée par le groupe de travail. Pourtant, les associations sont des acteurs incontournables pour les français et peuvent jouer un rôle majeur dans l’accompagnement de l’initiative citoyenne vis à vis du parlement.
Il est ainsi proposé de créer une plateforme pour organiser ce dialogue ; l’intégralité des associations du territoire pourraient être pré-inscrites et qualifiées via les bases SIRENE et Intérieur ouvertes. Un double mécanisme de concertation et de proposition pourraient être mise à l’oeuvre, les associations se faisant le relai sur le terrain, auprès de leurs membres ou de leurs publics cibles, répondant à un objectif d’impact social et d’égalité territoriale, écueil des démarches de participation en ligne. L’Assemblée Nationale pourrait ainsi solliciter les associations sur des concertations générales ou avec une granularité territoriale ou thématique. En retour, les associations pourraient se constituer en collectif pour émettre des propositions -selon des seuils ou modalités à définir- ou saisir les commissions de l’Assemblée compétentes sur le sujet posé.

Score : 4 / Réponses : 0

----

Bonjour, 
les citoyens sont très friands de pétitions pour manifester leur soutien ou leur indignation sur des sujets, qui peuvent aussi être des sujets politiques. 

Or, ces pétitions sont majoritairement hébergées sur des plateformes extérieures, qui font commerce des données personnelles. 

Si tant est qu'on prenne en compte la pétition dans la construction législative, il faudrait qu'il existe un portail unique, géré par l'Etat ou par les Chambres.

De la même manière, tout ce qui relève de la chose publique - référendum, proposition de loi, pétitions, amendements - d’initiative citoyenne ne doit pas transiter ailleurs que par les Chambres. 

Donc adapter notre droit pour prendre mieux en compte, notamment le droit de pétition, mais avec la sécurité que ces contributions ne tombent dans un espace commercial.

Score : 4 / Réponses : 0

----

Mettre en place une __plateforme internet de proposition citoyenne__ avec une obligation d’examen par l’Assemblée Nationale dès lors que cette proposition est signée :
- par 0,5% des électeurs français (**_moins de 250.000_**), et,
- **_présents dans les 8 circonscriptions_** selon le découpage pour les élections européennes en respectant des minima d’électeurs dans chacune des circonscriptions.

===

__Pourquoi 0,5% des électeurs français ?__ 

===

Un minimum d’électeurs doit être obligatoire afin d’__éviter les initiatives partisanes__. Les plus gros partis politiques comptent autour de 200.000 adhérents. Ce chiffre fluctue énormément notamment en fonction des présidentielles. l’UMP (aujourd’hui Les Républicains) revendiquait 370.247 adhérents en 2007, environ 100.000 de moins l’année suivante, 100.000 de moins encore en 2011

===

__Pourquoi dans les 8 circonscriptions des élections européennes ? __

===

Un maillage territorial me paraît nécessaire afin d’__éviter que la plateforme ne soit utilisée par des acteurs organisés__ (ONG, associations, …)

Score : 3 / Réponses : 0

----

Ce n'est pas seulement lorsqu’elles apportent un point de vue différent de celui des organisations syndicales, patronales ou des associations que les pétitions doivent être considérées comme recevables car les pétitions initiées ou soutenues par ces structures ou des pétitions convergentes sont un moyen d'évaluer les implications de soutien mais aussi des compléments d'argumentation.
Dans tous les cas, la recevabilité des pétitions en ligne suppose que leur système de collecte de signature soit sécurisé notamment par un système de confirmation évitant qu'une signature soit décomptée à l'insu du détenteur de l'adresse électronique donnée ou , autant que possible, qu'un même internaute signe sous plusieurs noms et adresses.

Score : 3 / Réponses : 0

----

Un dispositif de vote en mairies pour faire remonter l'avis citoyen au député de la circonscription.

Score : 3 / Réponses : 0

----

UN MÉCANISME NON CITÉ : CELUI DES PACTES.

Les pétitions, référendums, commentaires des articles de projet de loi, etc., c'est bien. Mais les premiers acteurs du travail législatif sont les élus (députés et sénateurs). Or, c'est paradoxale, mais qui connaît la position de ces élus ? Comment fait un citoyen pour découvrir l'avis de ceux qui vont voter pour ou contre ? Apparaît la nécessité d'un mécanisme pour le citoyen de solliciter l'avis des parlementaires sur une question précise et non nécessairement un article de loi.

La solution d'envoyer un courriel à chaque parlementaire est possible mais l'expérience montre que : a) beaucoup ne prennent même pas la peine de répondre, b) certains ne lisent jamais leur boite aux lettres de parlementaire, c) on se fait classer en spam si on envoie le même courriel à tous les parlementaires, d) les hébergeurs de boites aux lettres limitent le nombre d'envois de courriel par heure… Bref, ça ne fonctionne pas et le résultat n'est pas rendu public.

En 2007, l'association April (http://www.april.org/) a lancé le Pacte du logiciel libre : http://candidats.fr/documents/Le_Pacte_Du_Logiciel_Libre_candidats_legislatives_2017.pdf

C'est un document qui propose à un candidat ou un élu de prendre une position claire sur un sujet précis et concis. L'idée c’est de recréer du lien entre les  gens et les élus et c’est aussi un moyen de faire passer un message de  sensibilisation auprès des futurs élus qui font la loi… L'April a même construit une plateforme pour collecter et partager les réponses :  http://candidats.fr/ 

PROPOSITION : pouvoir, pour un citoyen, déposer un pacte ou une question simple (réponse OUI/NON/PEUT-ÊTRE/JOKER) et d'obliger les parlementaires à répondre dans un délais court (10 jours) et de publier les réponses. Ça serait un moyen complémentaire pour stimuler les parlementaires et les relier aux préoccupations des citoyens.

Score : 3 / Réponses : 0

----

Mettre en place le référendum révocatoire pour tous les élus pour permettre un véritable contrôle citoyen des élus.

À partir de mi-mandat, et d’un certain pourcentage de signatures d’électeurs (10 % des inscrits par exemple), il serait possible de provoquer un référendum pour décider de révoquer un élu. Selon le résultat il serait amené à démissionner (de nouvelles élections sont convoquées) ou à poursuivre son mandat jusqu’à son terme. Par exemple en Suisse, si le référendum révocatoire n’existe pas au niveau fédéral, plusieurs cantons le prévoient. Il est convoqué sur l’initiative d’un nombre de signataires variant, selon les cantons, entre 2 % et 7 % des citoyens. Le nombre de référendum révocatoire pourrait être limité à 2 par mandature (afin d’éviter un éventuel recours abusif). 
Rendre inéligibles les élus condamnés pour des faits de corruption ou détournement de fonds publics.

Score : 2 / Réponses : 18

----

Mettre en place le référendum d'initiative populaire est indispensable.
Tous les citoyens ont un fort besoin de s'exprimer et de se sentir impliqués.
Il y a une très forte demande des citoyens de démocratie directe que les pétitions et les manifestations de rue de remplacent pas .
A l'heure d'Internet et de l'information en temps réel il est aberrant que le fonctionnement de la démocratie ne soit pas plus directe .
Aucun référendum national n'a eu lieu depuis 12 ans en France
Pas étonnant que les citoyens s'éloignent de la chose politique  et se méfient de leurs élites.
L'organisation de nombreux référendums évitera que ceux-ci ne tournent  au plébiscite pour ou contre le gouvernement 
L'organisation du référendum doit avoir des règles  simples pour que les citoyens l'adoptent.

Score : 2 / Réponses : 1

----

Il serait fondamental de mettre en oeuvre les règles suivantes :

1/ une modification de la Constitution aux fins de l'introduction d'une procédure de modification ou d'abrogation de la législation sur l'initiative des citoyens, initiative adressée au Parlement, qui serait obligé d'inscrire à l'ordre du jour le texte issu de cette initiative populaire ;

2/ une modification de la Constitution afin de permettre l'instauration d'un veto populaire après l'adoption de lois par le Parlement selon la procédure de droit commun.

Score : 2 / Réponses : 0

----

> RENDRE ACCESSIBLE, POUR TOUS, LA FABRICATION DES LOIS 

La question de la participation citoyenne à l’élaboration des lois, telle qu’elle est aujourd’hui instituée, s’adresse de fait à un nombre très restreint de citoyens informés, impliqués, intéressés...
 
La plupart des français ne sont pas dans cette logique de participation, pour de multiples raisons : culturelles, sociales, économiques. Mais au-delà, c’est le sentiment même d’illégitimité  face à un  fonctionnement qui apparait éloigné, fermé et un processus de création des lois qui semble et qui est, de fait, très complexe. Beaucoup de citoyens ont, même, intériorisé que cette opacité était voulue. Avant de penser à la participation, il nous apparait donc que l’étape préalable est l’accessibilité aux informations liées à l’Assemblée Nationale et à ses travaux. Si cette étape de formation/information n’est pas prise en compte, il se peut que les dispositifs de participation souhaités par l’Assemblée Nationale renforcent encore l’exclusion démocratique de ceux qui sont déjà au bord du chemin et creusent encore un peu plus le fossé avec tous les autres. 
 
Le microsite «Parlement des enfants» constitue, de ce point de vue, une première étape de vulgarisation. Comment peut-il y avoir un élargissement de ce travail dans une logique de «facile à comprendre». Certaines ressources publiées par France.tv sur le fonctionnement de l’Assemblée s’inscrivent, d’ailleurs, directement dans cet esprit. 
 
Les associations d’Education Populaire pourraient tout à fait jouer un rôle dans cette dynamique de compréhension par le plus grand nombre du fonctionnement de l’Assemblée et des principales lois qui y sont travaillées. 

> Proposition Une nouvelle alliance Assemblée Nationale – associations d’Education Populaire 
 
Les associations d’Education Populaire sont pleinement acteurs du CESE. Cela n’empêche pas, bien au contraire, qu’un lien puisse se tisser aussi avec l’Assemblée Nationale. 

Ces associations pourraient travailler sur deux axes prioritaires : 
-	relayer l’information auprès des jeunes accueillis dans nos réseaux (jeunes bénéficiaires ou jeunes animateurs/bénévoles/volontaires)
-	remonter, auprès des députés, les évolutions sociales et sociétales constatées dans notre pratique territoriale

Les traductions opérationnelles possibles sont nombreuses : 
-	inscrire dans le cadre de la formation civique et citoyenne prévue pour les jeunes en service civique une rencontre systématique avec un député
-	co-construire des outils pédagogiques spécifiques
-	faciliter l’organisation au sein de l’Assemblée de temps de débats et de plaidoyers portés par ces associations 
-	…

Une convention cadre pluriannuelle pourrait être construite avec chacun de ces mouvements d’Education Populaire.

Score : 2 / Réponses : 1

----

En participant à ce projet j'ai remarqué quelque chose qui pourrait malheureusement nuire à l'initiative. En effet, sur les différents forums ouverts par thématique, les gens qui participent à la discussion semblent souvent très au fait des dispositifs légaux et suivre régulièrement les débats à l'Assemblée et les avancées des lois ... ce qui me pousse à m'interroger sur la sélection naturelle qui s'est imposée ici.

Score : 2 / Réponses : 0

----

**Groupe de travail Initiative Citoyenne, réuni au Liberté Living Lab le 2/11/17 (15 citoyens)**
Instaurer un droit d’initiative citoyenne ne suffit pas à ce que les citoyens s’en saisisse. Il est ainsi proposé d’instaurer dès le collège des travaux pratiques citoyens où les élèves seront appelés à apprendre à utiliser les technologies citoyennes, se former à la recherche d’information sur internet et à les critiquer, à participer en groupe aux concertations en ligne et à mobiliser les données ouvertes pour construire leur raisonnement. Il s’agira de mettre en pratique les pratiques citoyennes  tout en apprenant le fonctionnement des institutions et se familiarisant aux nouvelles technologies.

Score : 2 / Réponses : 0

----

**Groupe de travail Initiative Citoyenne, réuni au Liberté Living Lab le 2/11/17 (15 citoyens)**
Instaurer un droit d’initiative citoyenne ne suffit pas à ce que les citoyens s’en saisisse. Il est ainsi proposé de créer un tableau de bord de l’initiative citoyenne, où, sur le site de l’Assemblée Nationale, citoyens, associations, entreprises, puissent visualiser leurs différents points d’interaction possibles avec l’Assemblée Nationale, leurs droits et suivre les étapes en cours de leurs saisines.

Score : 2 / Réponses : 0

----

Les consultations citoyennes devraient être obligatoires pour toutes les décisions engageant l'avenir du pays, que ce soit sur le plan financier, social, ou de la politique extérieure. Il faut redonner toute sa place au citoyen, dans les affaires du pays, de la région, du département ou de la commune. Nous ne votons pas pour élire des maitres mais pour que ceux qui sont désignés, mettent en oeuvre les moyens nécessaires à la réalisation des aspirations du peuple qui les payent pour cela. Ce qui sous-entend que le peuple doit aussi pouvoir les congédier, comme ferait n'importe quel patron, insatisfait des performances de son employé.

Score : 2 / Réponses : 0

----

Une assemblée composée de citoyens tirés au sort permettrait une démocratie plus directe et plus représentative.
En effet, l'élection de représentants est nécessaire, mais elle doit être completée par une assemblée réellement populaire.
Le taux d'absention est de plus en plus haut, il a atteint des sommets aux législatives. Une assemblée de citoyens permettrait de représenter ces abstentionnistes.

Score : 2 / Réponses : 0

----

Bonsoir à toutes et à tous. 
Une soixantaine d'années pour arriver à envisager pouvoir se faire entendre comme il se devrait en démocratie, belle avancée ! En souhaitant que ce ne soit pas une nouvelle chimère ? Pour rester positif et je le souhaite vraiment, je suis heureux de cette initiative qui me va droit au cœur. Souhaitons enfin que nos idées du quotidien soient entendues et surtout considérées, qu'elles ne restent pas lettres mortes !!! Pour terminer, merci à change.org ainsi qu'à tous les citoyens qui œuvrent, en back-office et dans l'anonymat dans ce pays, afin d'essayer, autant que faire ce peut, d'améliorer notre belle démocratie. Pour une FRANCE humaniste, apaisée et constructive, cordialement, Frédéric.

Score : 2 / Réponses : 1

----

Comme beaucoup, je découvre cette initiative à quelques jours de sa fermeture !
Moins de 30 jours pour une initiative de cette importance, c'est tout a fait insuffisant.
Il y a eu trop peu d'information dans les médias sur son l'existence.
Il serait fort honnête de la prolonger de plusieurs mois et de la faire connaitre largement via les médias.

Score : 2 / Réponses : 1

----

Idées intêressantes.Soutenons le projet mais il faudra être vigilant......
C'est déjà dommage de découvrir ce scrutin 2 jours avant la cloture.

Score : 2 / Réponses : 0

----

Il me paraît indispensable de simplifier et d'améliorer l'accès des citoyens aux débats parlementaires et à l'élaboration de la loi. La vitalité régulièrement exprimée sur les plateformes de pétition citoyenne est un bon moyen d'ouvrir des débats, de nourrir des réflexions et de bénéficier de l'expertise du plus grand nombre. Alors soutenons le renforcement de la place des pétitions citoyennes dans le droit français.

Score : 2 / Réponses : 1

----

Une démocratie, c’est par le peuple… Alors il faut utiliser la proximité et l’outil informatique. Chaque citoyen devrait pouvoir poser LA question qui lui tient le plus à cœur sur un site de sa circonscription et visible par tous. Si cette question obtient un certain % ( par exemple 5% des inscrits), alors le député prends le relais en invitant un groupe constitué de 10 citoyens afin de lancer une consultation dans sa circonscription. Si cette dernière obtient un seuil à définir, alors elle est lancée dans la région puis si succès au niveau national. Nous avons les idées, les outils ALORS passons à l’action.

Score : 2 / Réponses : 0

----

La démocratie, ce n'est pas seulement une élection tous les cinq ans, c'est aussi écouter les citoyens au quotidien. Les pétitions en ligne sont un bon moyen d'expression et doivent être entendues.

Score : 2 / Réponses : 0

----

Une démocratie, c’est par le peuple… Alors il faut utiliser la proximité et l’outil informatique. Chaque citoyen devrait pouvoir poser LA question qui lui tient le plus à cœur sur un site de sa circonscription et visible par tous. Si cette question obtient un certain % ( par exemple 5% des inscrits), alors le député prends le relais en invitant un groupe constitué de 10 citoyens afin de lancer une consultation dans sa circonscription. Si cette dernière obtient un seuil à définir, alors elle est lancée dans la région puis si succès au niveau national. Nous avons les idées, les outils ALORS passons à l’action.

Score : 2 / Réponses : 0

----

“La démocratie, c’est le gouvernement du peuple, par le peuple, pour le peuple.” Abraham Lincoln.
La démocratie est le plus souvent un régime politique dans lequel les citoyens ont le pouvoir. La prise en compte des pétitions en ligne pour tenir compte des usages actuels en terme de mobilisation citoyenne en France va dans le bon sens.

Score : 2 / Réponses : 0

----

La démocratie directe est à construire pour sauver la République en grands périls aujourd'hui 
Voilà un excellent moyen à notre disposition pour enrichir les débats et participer au contrôle nécessaire des pouvoirs législatifs et exécutifs

Score : 2 / Réponses : 0

----

Oui, je soutiens cette proposition de Change.org même si cela demande beaucoup de disponibilité et de temps car l'élaboration ou la modification des lois sont fort complexes et que cela ne fonctionne pas comme " y'a qu'à faut qu'on ! "

Score : 2 / Réponses : 0

----

Le point numéro 2 sur la recevabilité des pétitions me semble essentiel : en particulier sur le nombre de signatures, et pour des pétitions déposées en ligne sur un site garant de son indépendance et du respect des informations personnelles déposées.

Score : 2 / Réponses : 0

----

Quelles sont les propositions à mettre en œuvre de manière prioritaire ? 

Pétitions en ligne
Les pétitions en lignes sont déjà très utilisées. Le fait qu’elles soient “en ligne” exclut une partie des répondants potentiels. Toutefois, elles peuvent permettre de dégager des priorités à mettre à l’agenda parlementaire.

Référendum d’initiative populaire
Le référendum d’initiative populaire me semble plus démocratique puisqu’il est organisé selon le mode électoral classique. Il peut être lancé à l’issue d’une pétition ayant rencontré un succès important. Même s’il est un outil d’engagement, il reste assez ponctuel et il ne me paraît pas le plus intéressant pour amener les citoyen à s’impliquer dans la fabrication de la loi.

Questions au gouvernement citoyennes
L’idée me semble très bonne, à explorer dans sa mise en oeuvre concrète :
- Avec une modération au préalable ? 
- Si oui, de qui ? Du député de la circonscription ? D’un service ad hoc de l’Assemblée nationale ?
- Comment ne pas éventuellement paralyser l’Assemblée nationale avec ces questions ?
- Comment éviter des questions des partis politiques “déguisées” en questions citoyennes ?

Propositions de loi citoyennes
Les propositions de loi citoyennes me semblent un peu ambitieuses mais pas intéressantes. Il faudrait faciliter en amont la recherche au sein de la législation actuelle pour permettre aux citoyens de positionner un projet au regard des limites actuelles du droit.
La participation à un projet de loi me paraît en revanche très intéressante.

Score : 2 / Réponses : 0

----

L’initiative citoyenne, j'en avais une vision claire. A la suisse. Jusqu'à cet exercice. A force d'y réfléchir, de consulter mes collègues à mon lieu de travail, et même ce soir encore dans une discussion en ligne organisée par un ami, force est de constater que cela demandera un effort certain pour trouver la bonne équation pour ceux qui auront l'opportunité de s'investir plus encore dans cette belle aventure de transformation de notre société, mais également pour le citoyen qui va se voir doté d'outils d'expression nouveaux. 
C’est donc un sujet qui demande de la passion froide. Et chacune des solutions a plusieurs facettes, c'est pourquoi il est plutôt complexe d'affirmer qu'une solution est à mettre en place de façon prioritaire. Cependant, et pour faire synthétique :

La pétition en ligne est très pratique car accessible depuis chez soi.
Elle excluait pourtant ceux qui n’ont pas accès à internet ou ne sont pas encore familiers avec les TIC, 
et demande une sécurité informatique et une plateforme dédiée. 

Le referendum d’initiative populaire
demande une définition de son périmètre (consultatif ? normatif ? abrogatif ?) et à ce qu’il soit temporisé pour éviter les demandes de referendum trop émotionnels (temporiser pour tempérer).

Les propositions de lois citoyennes
demandent un travail en amont très bien structuré pour qu’elles soient réalistes (application technique, coût, conséquences sociales et sociétales,...) et que le citoyen soit avant tout un citoyen averti et avisé pour l'intérêt général. Un outil en ligne pourrait l'aider à structurer son projet, comme un business plan de politique citoyenne.
Elles risquent aussi de susciter des frustrations si la proposition ne se concrétise pas par un acte législatif ou se retrouve diluée dans dans une loi «consensuelle», si loi doit être proposée.

Les questions au Gouvernement citoyennes semblent être relativement faciles à réaliser par le biais des députés; il s’agirait simplement de repenser le lien entre nos élus et les citoyens de leur circonscription, ainsi que leur rôle ont un risque de ne pas être soumises au Gouvernement puisqu’un filtre sera mis en place par les cabinets ministériels ou par l'actualité elle-même.

CEPENDANT
Les propositions présentent toutes un intérêt. 
Et un outil n’exclut pas forcément l’autre. On peut imaginer que l’un soit une étape préalable pour l’autre ou un outil issu du mélange de plusieurs d’entre eux.

Par exemple, la pétition en ligne peut être le pré-requis pour un referendum d’initiative populaire ou une proposition de loi citoyenne.
Une question au Gouvernement peut être le fruit d'une concertation d'un(e) député(e) avec les citoyens de sa circonscription suite à l'élaboration d'un projet citoyen ayant fait l'objet d'une publication officielle en mairies, relayé par les médias locaux et supporté par un nombre plancher de signatures. 
Là encore, cela dépend du périmètre accordé : abrogatif, normatif ou consultatif.

Score : 2 / Réponses : 0

----

1ère proposition
Afin de développer les initiatives citoyennes nous proposons la création d'une plate-forme en ligne dédiée, permettant à chaque citoyen de poser une question ou une suggestion à l'Assemblée Nationale, sur un sujet national.
Pour chaque sujet posté, la plateforme permettra à chaque citoyen de soutenir ou non le sujet. 
Si le nombre de soutiens dépasse un certain seuil, par exemple 100 000 soutiens, le sujet sera soumis à l’Assemblée Nationale.
La ou les réponses apportées par l’Assemblé Nationale seront inscrites sur la plateforme, permettant ainsi à chaque citoyen d’avoir un retour.

Le même outil pourrait être mis à la disposition de chaque député pour les sujets locaux dépendant de leur circonscription.

Afin de garantir l’information auprès de tous les citoyens, nous suggérons que la mise en place de la plateforme face l’objet d’une campagne nationale de communication : journaux télévisés, journaux nationaux, spot publicitaire, affichage...  

2ème proposition
La création d’un Facebook live d’interaction entre le président de l’Assemblée Nationale et les citoyens à l’image de celui proposé par le 1er ministre, Edouard PHILIPPE.

Score : 2 / Réponses : 0

----

Vive la démocratie numérique ! Emparons-nous de cet outil pour en faire apparaître le meilleur.

Score : 2 / Réponses : 0

----

Il faut revoir en profondeur le mode de scrutin des députés et des élus en général.
Ils sont élus par le peuple et doivent en conséquences prendre l'avis de celui ci pour tous votes, l'influence personnelle, des partis ou dirigeants influent doit être banni. Dans ce conteste on pourra parlé d'une vrai démocratie.
Nous devons revoir la constitution pour donner au peuple une vrai responsabilité et participation à nos lois.
Certains d'entre nous ont de très bonnes idées de réforme allant dans le sens de l'égalité (terme de notre devise complétement oublié) ou  donnant-donnant et non pas orienté pour une certaine catégorie de citoyens.
Lors des élections législatives ne votons plus pour un homme mais,  à la proportionnelle intégrale, pour un groupe de personnes de toutes horizons politique, associative et Privée  qui désigneront leur représentant à l'assemblée. Ainsi il sera obligé de voté en fonction des décisions de ce que l'on pourrait appelé le conseil des sages.
Le referendum populaire doit être un outil également  beaucoup plus utilisé et suivi.

Score : 2 / Réponses : 0

----

Initiative citoyenne (exemples Royaume Uni)
De nombreuses législatures ont développé sur leur site internet une page permettant aux citoyens, citoyennes et organismes (associations, société, institutions, syndicats, organismes de charité, etc.) de présenter une pétition à la législature compétente. Les citoyens, citoyennes, résidents et résidentes britanniques peuvent aussi envoyer une pétition au Parlement, à l’Assemblée galloise et au Parlement écossais sans passer par leur député.e ou leur élu.e régional.e, envoyer une pétition papier à leur législature et pétitionner le gouvernement britannique et les trois législatures. Le Parlement britannique partage le site de pétitions du gouvernement. De plus, le Parlement écossais et l’Assemblée galloise permettent à leurs citoyen.ne.s et résident.e.s de créer une pétition directement sur leur site internet. 
Les trois sites permettent également de suivre l’évolution des pétitions. La transparence du processus et la libre information sur les suites données aux pétitions sont des éléments importants pour la confiance des citoyen.ne.s et la légitimité du processus.
Ces trois législatures ont une commission permanente qui reçoit et évalue les pétitions. Ces commissions peuvent décider de contacter l'auteur.e de la pétition pour plus d’information, l'inviter à présenter ses arguments devant la commission, organiser un débat en chambre, ou transmettre la pétition à la commission parlementaire appropriée.
Les trois législatures ont des procédures différentes relatives au nombre de signatures nécessaires pour que la pétition soit examinée. Toute pétition adressée au gouvernement britannique qui est soutenue par au moins 10000 signatures reçoit une réponse du gouvernement. Si une pétition reçoit plus de 100000 signatures, elle peut donner lieu à un débat à la Chambre des Communes. La commission des pétitions de la Chambre examine aussi les réponses du gouvernement et peut lui demander de clarifier sa réponse. Elle peut aussi décider d’inclure le sujet d’une pétition à un débat déjà prévu à la Chambre même si elle a reçu moins de 100000 signatures. L’Assemblée galloise requiert 50 signatures pour qu’une pétition soit considérée par la commission parlementaire ; les pétitions de plus de 5000 signatures donnent lieu à un débat en chambre plénière. Le Parlement écossais considère que le nombre de signatures n’est pas gage de qualité et sa commission examine toutes les pétitions jugées recevables. 

Recommandations :
-	Créer une page internet dédiée aux pétitions sur le site de l’Assemblée Nationale ; la page devrait inclure aussi des informations sur le style et le langage à utiliser, les conditions d’admissibilité, le processus d’examen, les pétitions en cours, et le résultat des pétitions précédentes ;
-	Les pétitions papier doivent aussi être maintenues afin de permettre aux citoyen.ne.s sans accès internet ou ordinateur ou aux citoyen.ne.s qui pensent pouvoir recevoir plus de signatures avec un support papier d’avoir accès à l’Assemblée ;
-	Créer une commission parlementaire dont la tâche est d’évaluer la compétence de la pétition et de déterminer les suites à donner à chaque pétition;
-	La commission parlementaire doit disposer d’un personnel parlementaire approprié afin d’avoir les moyens de répondre aux pétitionnaires et de les tenir informé.e.s des progrès de leur pétition (acceptée ou rejetée et motifs, prochaine étape, recommandations si un autre niveau est le niveau compétent) et de maintenir la page internet informant le public des suites données aux pétitions;
- La France a une population de taille similaire à celle du RU, un seuil de 10 000 voix avant que le Parlement ne doive examiner une pétition semble raisonnable. Cependant, la commission devrait examiner la liste des pétitions en cours de façon régulière afin d’observer les tendances de l’opinion et de détecter les bonnes idées. Ainsi la commission pourrait décider d’examiner une pétition de moins de 10000 signatures  qui semble sensible ou répondant à des préoccupations déjà dans le programme législatif.

Score : 2 / Réponses : 0

----

Réserver une place régulière pour les propositions de loi émanant des pétitions citoyennes (par exemple un par semaine). Les propositions retenues seraient évidement celles qui reçoivent le plus de soutien. Nous éviterions ainsi de spéculer sur le nombre idéal de signataires en fixant directement la quantité (fréquence) idéale de propositions citoyennes.

Score : 1 / Réponses : 0

----

Pour les initiatives populaires, il pourrait être pertinent que certains sujets capitaux, comme l’environnement, puissent bénéficier d’un seuil de participation plus faible, afin de faciliter leur prise en compte dans un circuit législatif souvent obstrué par les différents groupes de pression industriels, peu soucieux dudit environnement.

Score : 1 / Réponses : 0

----

La mise en place d'un référendum correctif

Si un groupement de citoyens réunit en l'espace de plusieurs semaines (6 semaines par exemple) un nombre de signatures équivalent à 1% ou 2% de l'électorat (500 000 ou 1 000 000), une loi votée par le Parlement doit être soumise à référendum. 

Celui-ci n'est valide que si 25% des électeurs inscrits minimum se déplacent pour y participer. Le résultat ne serait pas contraignant, il oblige simplement le Parlement à rediscuter la loi. 

Pourquoi un tel outil ? 
- Renforcement du lien entre les élus et les électeurs (via les débats référendaires exposant le travail législatif)
- Les députés conservent leurs prérogatives législatives
- Meilleure prise en compte des aspirations des citoyens 
- Résolution pacifique et démocratique des tensions politiques

Score : 1 / Réponses : 7

----

Pour une contribution citoyenne la plus riche possible, l'engagement, la réflexion et la délibération  sont nécessaires. C'est la base du fonctionnement parlementaire et elle mérite d'être appliquée à la participation citoyenne. Il s'agirait, par exemple, de permettre aux citoyens de participer activement et sur une certaine durée à l'activité d'un groupe de travail thématique (sur une plateforme web ou lors de tables rondes territoriales), qui disposera de l'accès aux ressources documentaires utiles, sera éventuellement accompagné dans son travail par un expert et/ou un animateur, et devra produire une analyse et des propositions à destination des parlementaires. 
Au lieu de liker ou de cosigner les idées que d'autres ont su imposer, cet exercice donne à chacun les moyens de contribuer véritablement à la réflexion et de se trouver en responsabilité. 
L'idée de @Sébastien Piarulli d'une plateforme spécifique avec un compte unique citoyen pourrait être un parfait support.

Score : 1 / Réponses : 3

----

L'implication des citoyens est très nettement insuffisantes. Actuellement. 2 outils sont complémentaires pour éviter l'exclusions de la participation.
 Les Suisses nous montrent l'exemple avec le référendum populaire par un vote classique au bureau de vote.
Le vote numérique est un bon outil  pour les pétitions qui peuvent être prise en compte au delà d'une certaine quantité. La participation possible est déjà plus restreinte.
Le vote numérique sur un projet de loi restreint encore plus la participation.
Je souhaite que les réponses à cette consultation soit analysée en fonction des catégories d'âges et retraités..

Score : 1 / Réponses : 0

----

Le referendum d’initiative populaire me paraît le plus important. En effet les citoyens sont confrontés tous les jours aux faits societaux et ce seraient grâce à eux, que le droit pourrait évoluer plus vite et efficacement, afin d’arranger ce qui ne pourrait plus être d’actualité.

Score : 1 / Réponses : 0

----

Très urgent. le 22.10.2017
 Objet : Rectification du document :" Petition_et_referendum.pdf"
En application de la "charte de consultation"  qui indique que sont proscrits:
les affirmations graves non prouvées ou notoirement inexactes "
 je demande que le titre erronné :"Référendum d'initiative partagée " soit remplacé par " référendum d'initiative parlementaire".                                                        Il est en effet factuel que l'initiative de cette procédure sui generis est d'initiative EXCLUSIVEMENT parlementaire. 
Il suffit de lire l'article 11.
Cordiales salutations citoyennes.

Score : 1 / Réponses : 3

----

QUESTIONS ECRITES AU GOUVERNEMENT

Je vous fait part d'abord de mon expérience personnelle à la fois décevante et frustrante .
Aprés avoir soumis une proposition concrète à plusieurs députés et sénateurs sur les frais bancaires de succession suite au décès de mon père,trois députés et une sénatrice ont pris ma demande en considération et ont ouvert des questions écrites au gouvernement entre Juin et Nov 2016.
A ce jour ,malgré plusieurs relances  des parlementaires aucune réponse du gouvernement aux quatre questions écrites.
J'ai adressé ma demande par le portail du ministére des finances en Juillet 2016 et malgré un accusé d'enregistrement et une relance je n'ai pas eu de réponse.
Mettre en place des passerelles de communication et ne pas avoir de réponses ne fait qu'éloigner les Français de leurs représentants

Je propose donc par respect des citoyens à l'initiative de demandes constructives et des parlementaires qui les relayent en question écrites au gouvernement le principe suivant: AUCUNE QUESTION ECRITE NE DOIT RESTER SANS REPONSE.

1)Réduire les délais:
Inciter le gouvernement à mettre en place un comité de suivi des questions écrites permettant de dynamiser les groupes de travail en leur apportant une méthodologie et en faisant des points réguliers sur l'état d'avancement des questions (prise en compte,actions planifiées,études engagées,points bloquants .....).
Prioriser les actions de plus de 3 mois non soldées.

2)Informer les députés et sénateurs:
Mise à disposition informatiquement d'une base de données traduisant la  synthése de l'état d'avancement en temps réel des questions écrites.

3)Informer les émetteurs,proximité avec les citoyens.
En l'absence de réponse du gouvernement au bout de 3 mois les députés et sénateurs doivent renouveler la question en informant systématiquement l'émetteur de l'état d'avancement de la question.

4)Continuité et aller jusqu'au bout:
Les questions posées ne doivent pas etre retirée meme en cas de fin de mandat,de changement de législature ou de décès.
Derrière ces questions il y a un ou des citoyens qui se sont investis pour essayer d'améliorer la vie quotidienne des Français et qui attendent une réponse des membres du gouvernement.

Score : 1 / Réponses : 3

----

Le constat des dénis de démocratie et propositions pour y remédier 
1) Les dénis de démocratie
1 Qu'est ce que la démocratie ?
	La démocratie est la doctrine politique selon laquelle la souveraineté doit appartenir à l’ensemble des citoyens. On en dénombre deux formes : 
	la démocratie directe ou chaque citoyen exerce directement sa souveraineté, à tout instant, en toutes circonstances. 
	La démocratie représentative ou les citoyens choisissent des représentants lesquels exerceront la souveraineté pour le peuple.
2 La démocratie n'existe plus en France
2.1 Les institutions
	Le système démocratique du pays est basé sur le principe de l'élection
	Pour chaque institution, mairie, département, communauté de communes, région, assemblée nationale, chef de l'exécutif, des élections sont organisées.
	Les partis politiques présentent des listes de candidats éligibles classés dans un ordre défini de telle sorte que le premier de la liste obtenant la majorité des voix devienne très probablement le maire, le président de conseil général, le président de communauté, le président de région, le président de la République. Pour l'assemblée nationale, c'est un peu différent : aucun des élus n'a de prérogatives particulières. Il est un des députés de l'assemblée qui vote des lois que fera appliquer l'exécutif.
	Le sénat, le conseil d’État, le conseil constitutionnel, le CSA, … sont encore d'autres institutions dont le rôle peut être considéré comme mineur. Ce sont des sortes de placards pour d'anciens élus notoires. Ils sont quasi inutiles pour la démocratie. Quoique ?
2.2 Le fonctionnement et ses dérives
2.2.1 Le choix des éligibles
	Ce choix n'est pas fait par les électeurs citoyens mais par chaque parti politique. Dans la mesure où ce parti persiste dans le temps, ainsi que chacun de ses membres, il y a un risque certain et avéré de tractations, de népotisme, de salades internes à ce parti qui n'ont rien à voir avec la volonté des citoyens électeurs. 
Du coup, il est fréquent que les listes proposées par les partis aux électeurs ne correspondent en rien à l'opinion de ces électeurs, d'où un premier déni de démocratie.
Les 'primaires' instaurées depuis 2007 par le PS et copiées en 2016 par les LR permettent aux électeurs de choisir un candidat à la présidence de la République parmi chaque parti qui organise une primaire. Néanmoins ce choix très restreint n'améliore pas beaucoup les choses ; en réalité, c'est l'impossibilité de désigner un leader du parti qui incite à utiliser cette pré-élection au sein de chaque parti qui le souhaite.
2.2.2 Le programme proposé
	Chaque liste, affiliée à un parti voire à un groupe de partis, propose un programme, normalement un projet complet et cohérent que les futurs élus s'engagent à suivre. En réalité ce programme a plusieurs défauts :
- d'abord il est établi, non pas par les seuls éligibles de la liste, mais par les membres du parti les plus influents, puis accessoirement amendé par les éligibles de cette liste qui tentent parfois d'adapter le programme aux contingences locales. En effet, chaque parti tient à montrer sa marque de fabrique et impose donc un certain nombre de concepts, de principes qui lui sont chers et qui, pense t il, le démarque nettement des partis concurrents. 
- ensuite, voulant ratisser large et intéresser un maximum d'électeurs, les programmes sont soit très riches en multiples propositions précises, soit au contraire constitués de propositions trop généralistes. Dans le premier cas, il est quasi impossible de réaliser les propositions précises dans la durée du mandat, dans le second cas, difficile de savoir ce qui sera effectivement réalisé.
Le programme n'est pas obtenu par concertation importante des électeurs et il y a donc peu de chance que même profondément adapté par les éligibles, il puisse correspondre à l'attente légitime des électeurs. C'est un deuxième déni de démocratie.

... à suivre sur commentaire suivant

Score : 1 / Réponses : 1

----

2.2.3 Le vote
	Les conditions de vote en France me semblent très correctes ; en effet dans chaque bureau de vote il y a toujours au moins un membre de chaque parti qui surveille (et participe) aux opérations de vote. Si ces personnes font bien leur travail à toutes les étapes et particulièrement lors du comptage des voix, la possibilité de triche est très faible.
L'organisation du vote respecte le principe démocratique.
2.2.4 Le scrutin
	Le vote a eu lieu, il s'agit maintenant d'attribuer des élus pris dans chaque liste ayant obtenu un minimum de voix. Ce minimum est fixé à 5 % des votes exprimés ou blancs. Cette limite, assez haute, est prétendue nécessaire pour limiter le nombre total de listes et d'élus. Ensuite, dans le mode de scrutin majoritaire utilisé actuellement en France, le parti ayant obtenu la majorité des voix obtient un nombre d'élus beaucoup plus grand que le parti en minorité. Cela a été voulu pour éviter les débats sans fin en cas de 51 % pour le parti majoritaire et 49 % pour l'ensemble des partis minoritaires. 
Autant, cette prime donné au parti majoritaire est valable s'il est majoritaire de peu, autant elle est injuste si le parti majoritaire l'est largement. Cette règle pratique est grossière et ne respecte pas correctement le vote des électeurs. C'est un troisième déni de démocratie.

2.2.5 Le respect du programme
	Une fois élus, les représentants des électeurs en font trop souvent à leur guise ; d'abord, ils ne respectent que rarement le programme proposé lors du vote, ensuite ils considèrent qu'ayant été élus légitimement, ils peuvent prendre toutes décisions sans informer, sans rapporter strictement, sans écouter leurs électeurs.
Il s'agit ici du quatrième déni de démocratie sans doute le plus grave ; en effet, tout élu représentant des électeurs n'est rien d'autre que le serviteur de ces électeurs ; durant tout son mandat, il devrait sans cesse rapporter l'état de ses actions aux électeurs. Il devrait faire appel aux électeurs pour tout projet qui n'est pas inscrit ou qui est contraire au programme. Toute inaction ou action contraire au programme de l'élu devrait entraîner son éviction pour faute grave !

2.2.6 Le respect et l'utilisation des compétences de l'opposition
	Les élus du parti majoritaire ont presque toujours la majorité absolue et peuvent donc décider des actions à faire sans tenir compte des partis minoritaires. Du coup, trop souvent, les élus minoritaires ne sont pas associés aux actions entreprises.
C'est un cinquième déni de démocratie puisque les élus minoritaires (dits d'opposition) représentent une partie des électeurs qu'on ne saurait écarter ou ignorer. De plus il est idiot de perdre les idées, les intelligences et la bonne volonté de ces personnes.
2.2.7 La carrière ou caste des élus
	Puisque les partis existent depuis longtemps et ont une histoire et une certaine force, leurs membres ont tout intérêt à se créer des liens serrés voir des alliances au sein de ce parti. En effet, ces liens, ces alliances leur seront très utiles pour se retrouver en bonne place lors de prochaines élections. 
De plus, notre système électoral autorise chaque élu à proposer à nouveau sa candidature à l'élection suivante et cela autant de fois qu'il le désire (à de rares exceptions près).
Pire encore, le système autorise le cumul des mandats ; par exemple, il est courant qu'un maire soit également élu dans plusieurs  institutions différentes (communauté de communes, département, région, assemblée nationale, sénat).
Ces pratiques sont anti démocratiques ; puisque l'élu du mandat actuel doit penser à sa future réélection lors du mandat suivant ou bien à son élection cumul dans une autre institution. On comprend bien que cet élu ne fera rien qui puisse compromettre sa future réélection dusse t il oublier de réaliser certains éléments de son programme ou pire encore renier ce programme qui pourrait nuire à sa carrière. C'est un sixième déni de démocratie très grave.

... A suivre dans le commentaire suivant

Score : 1 / Réponses : 0

----

2.2.8 La rémunération des élus et lobbying
	Les élus sont rémunérés durant leur mandat pour plusieurs raisons ; d'abord pour leur survie puisque le temps consacré à leur mandat est autant de temps non travaillé dans une entreprise ou une administration. Ensuite parce que cette rémunération permet de payer des frais de représentation ou d'études. Enfin, paraît il, pour les mettre à l'abri des pressions exercées par des lobbies ou des notables locaux très influents.
	On remarquera que ce sont des élus de l'Assemblée Nationale qui votent les lois permettant de rémunérer les élus. Ainsi les élus sont ils juges et partie de leur rémunération.
Ceci est contraire à toutes les bonnes règles de gouvernance. puisque l'électeur ne peut pas décider et contrôler directement les rémunérations et les dépenses des élus.
Enfin chacun sait bien que même avec une rémunération hors du commun, l'élu corrompu n'hésitera pas à profiter des largesses d'un lobby en utilisant si possible les paradis fiscaux !
C'est un septième déni de démocratie.
2.3 Le système d'information (médias)
	Le journalisme constitue normalement un solide rempart contre la corruption et contre la pression exercée par les grands groupes capitalistes du monde. Malheureusement, la plupart des grands médias de ce pays ont été achetés par de grands capitalistes ; même s'ils s'en défendent, les journalistes ne sont donc plus libres d'informer sans craindre les foudres du propriétaire du média.
	Même les journaux télévisés des chaînes publiques sont devenues partiaux et accordent trop d'importance aux sujets de peu d'intérêt pour l'humanité. Par exemple la mort de célébrités du show business est largement développée de même que les actions ou réactions sécuritaires. A l'inverse les sujets de fond (système politique, système économique, système social, solidarité entre les gens) ne sont pas suffisamment développés ou bien le sont dans le cadre de débats stériles dans la mesure où au final, il n'en ressort aucune vision et aucun plan d'actions positives.
	De manière générale la plupart des médias font appel à une source d'information commune, l'Agence France Presse, et sélectionnent souvent les sujets impressionnants. Cette source d'infos unique et cette tendance des rédactions à privilégier l'événement frappant nuisent gravement à l'authenticité des médias.
 Les dernières péripéties du Petit Journal ou des Guignols de Canal+ devenu propriété de Vincent Bolloré, illustrent bien cette main mise des capitalistes sur les médias.
C'est un huitième déni de démocratie.

... A suivre dans le commentaire suivant

Score : 1 / Réponses : 0

----

3 Comment rétablir la démocratie ?
3.1 Le principe directeur du changement
	Pour rétablir une réelle démocratie, il faut supprimer tous les dénis de démocratie listés ci dessus, à savoir :
	1 Proposition des listes d'éligibles non démocratique,
	2 Programme élaboré sans concertation des électeurs,
	3 Scrutin majoritaire trop clivant,
	4 Non respect de l'exécution du programme,
	5 Non participation active et non utilisation des élus minoritaires,
	6 Carriérisme, caste des élus,
	7 Non contrôle des rémunérations et autres avantages par les électeurs,
	8 Non indépendance des médias.
3.2 Les résistants ou opposants
Si l'idée de supprimer tous les dénis de démocratie est simple, sa mise en œuvre sera sans doute nettement plus complexe dans la pratique à cause de la résistance que vont opposer ceux qui profitent bien du système actuel à savoir les élus cumulards dans le temps et en nombre de mandats simultanés et les capitalistes puissants de ce pays ou du monde.
3.3 La transition nécessaire
Par ailleurs, il semble judicieux de mettre en œuvre une transition depuis l'état actuel non démocratique vers un état final réellement démocratique. En effet la plupart des citoyens électeurs n'ont pas une conscience aigüe du dysfonctionnement de notre système politique ou ne sont pas enclins à un quelconque changement par paresse, découragement ou écoeurement. 
La transition est sans doute nécessaire pour avoir le temps de sensibiliser tout particulièrement les électeurs et leur permettre d'adopter sans faille les diverses étapes de cette transition vers la vraie démocratie. Mais cette transition a également un inconvénient de taille : elle laisse du temps à ceux qui veulent s'y opposer par tous les moyens fussent ils illicites.
Le Brexit des anglais montre bien que les puissants oeuvrent pour tenter de faire avorter le processus et revenir dans le giron européen. A suivre !
3.4 Le but final
Même si la transition semble nécessaire, il nous faut d'abord fixer clairement le cap final avant de dégager une liste d'étapes menant à ce but.

... A suivre dans le commentaire suivant

Score : 1 / Réponses : 1

----

3.4.1 Eradiquer le lobbying
Pour limiter au maximum le lobbying néfaste à la démocratie (dénis 7 et 8), il paraît judicieux d'interdire tout cumul de mandats et même d'instaurer le mandat unique pour toute une vie. Ainsi tout citoyen peut effectuer un mandat de gouvernance d'une institution une seule fois dans sa vie.
On appellera ce citoyen, Membre de la structure de pilotage de l'institution.
Difficulté : le nombre des citoyens prêts à se consacrer à un devoir civique majeur comme celui-là est malheureusement très limité semble t il. En effet, dans une très petite institution comme un conseil syndical ou une association, on constate que ce sont toujours les mêmes qui se présentent aux votes ; on constate également que le 'recrutement' de nouveaux est très difficile.
Il faut donc trouver un moyen contraignant pour renouveler largement les membres des institutions du pays.
Le vote ne semble pas être le bon moyen ; en effet, cela suppose de faire des listes de candidats.
Qui établit ces listes de candidats ? Ce ne peut être les partis politiques qui n'ont pas dans leurs rangs suffisamment de personnes nouvelles (encore plus vrai pour les petits partis) surtout si on met en œuvre le mandat unique. De plus le choix de candidats effectué par les partis est un déni de démocratie ! (déni 1)
Qui établit le programme ? Dans un système réellement démocratique, ce ne peut être les candidats de la liste alors que ce programme doit être la volonté des électeurs, sinon déni 2.
Comment font les partis pour informer des électeurs sur un choix de candidats alors que ces candidats, toujours nouveaux, sont donc mal connus et des partis et des électeurs ?
Le tirage au sort semble un bon moyen ; en effet ce tirage est bien plus simple à organiser qu'une élection. La garantie de nouveau candidat et futur membre est facile à vérifier grâce au fichier national des identités existant.
Le tirage peut être représentatif des différentes couches sociales du pays. En effet grâce à l'Insee nous connaissons les différentes couches sociales et leur pourcentage de représentation. Par la connaissance de la situation actuelle de chaque individu, on peut donc effectuer un tirage au sort qui respecte la représentation des couches sociales. Les 3 millions de chômeurs actuels auront donc des candidats dans chaque institution à concurrence de leur pourcentage. Les plus pauvres seront largement représentés, les plus riches beaucoup moins en fonction de leur pourcentage de représentation dans le pays.
Difficultés : on ne peut sans doute pas représenter finement toutes les classes sociales car le nombre de candidats futurs membres des institutions doit rester limité pour permettre un débat constructif ; il faudra donc inventer une liste simplifiée des classes sociales ; par exemple, chômeur, ouvrier, employé, cadre, profession libérale, retraité, actionnaire, soit 8 classes seulement.
Il faut également tenir compte strictement de la parité homme-femme.
Les tirés au sort peuvent refuser leur mandat ; dans ce cas on prend le suivant sur la liste. Pour éviter de retirer au sort plusieurs fois en fonction des désistements, on tire au sort beaucoup plus de personnes que nécessaire en respectant la représentativité des classes et la parité homme-femme. Si malgré cela il y a trop de désistements, on prend des 'volontaires d'office' en respectant la représentativité.
Avantage immédiat : le tirage au sort élimine les dénis de démocratie 1 (pas de listes établies par les partis), 3 (pas de scrutin majoritaire), 5 (non utilisation de l'opposition) et 6 (pas de carrière ni caste des élus)

... A suivre dans le commentaire suivant

Score : 1 / Réponses : 2

----

3.4.2 Eliminer les dénis de démocratie 2 (programme non conforme à l'attente des citoyens) et 4 (non respect du programme)
Les membres tirés au sort pour exercer un mandat ont un double rôle quelque soit l'importance de l'institution qu'ils pilotent :
	1. Un rôle dit législatif, d'élaboration de lois ou programmes
	2. Un rôle dit exécutif, de mise en application des lois ou programmes.
La première mission des membres d'une institution, tirés au sort selon les règles ci-dessus, est d'établir un programme en n résolutions qu'ils soumettent par vote aux citoyens du territoire de l'institution. 
Par le vote les citoyens valident ou non chacune des n résolutions. Toute résolution qui n'obtient pas au moins de 55 % des voix des électeurs votants est rejetée. Toute résolution qui obtient plus de 55 % des voix est acceptée. Si sur les n résolutions, 55 % de celles-ci sont rejetées, le programme et donc le vote sont à refaire.
Une résolution ou loi comporte un certain nombre de caractéristiques majeures comme :
- la durée d'application ou d'activité
- le domaine d'application
- l'objet
- la logique (une résolution ou loi ne peut pas être contraire, logiquement, à une autre qui a déjà été votée et est encore active)
On espère ainsi obtenir un programme conforme à l'attente des citoyens (déni 2 éliminé).
La deuxième mission des membres est de faire exécuter les résolutions ou lois qui ont été retenues par les électeurs du territoire de l'institution.
Tous les 6 mois, les membres d'une institution sont tenus de rendre compte aux citoyens des résultats obtenus. A l'inverse les citoyens, à l'issue d'une pétition recueillant au moins 5 % des voix, peuvent faire faire un audit des résultats du programme ou encore obtenir la destitution d'un membre en faute grave. Ils peuvent également destituer l'ensemble des membres pour non respect du programme.
Les membres destitués sont immédiatement remplacés par un nouveau tirage au sort. Si tous les membres ont été destitués, les remplaçants doivent élaborer un nouveau programme.
On espère ainsi éliminer le déni 4 (non respect du programme).
Remarque 1 : Il n'y a plus besoin de séparer le législatif de l'exécutif puisque les deux rôles sont bien contrôlés par les citoyens.
Remarque 2 : Le président est toujours élu et choisi par les membres et eux seuls. A part ses capacités personnelles, ce président n'a pas plus de pouvoir que chacun des membres. Le président de l'institution nationale peut être considéré comme le président de l’État.
Remarque 3 : Le gouvernement ancien (exécutif) et le parlement (législatif) sont fondus dans la seule institution nationale. Il n'y a plus ni Assemblée nationale, ni Sénat. Il n'y a plus de ministres ou secrétaires d'état. Et donc plus d'avantages divers et variés exorbitants alloués. L'institution nationale fonctionne selon les mêmes règles que toute autre institution.
Remarque 4 : L'institution nationale est sous le contrôle de tous les citoyens du pays. Les résolutions ou lois imaginées sont mises au vote de tous les citoyens. L'institution nationale se doit ensuite de faire appliquer les lois adoptées par les citoyens.
Remarque 5 : Une loi ou une résolution d'un programme peut avoir une durée limitée au mandat ou une durée plus longue voire perpétuelle. La Constitution qui définit les règles de fonctionnement ici décrites est un exemple de loi à durée perpétuelle. Les membres sont tenus de faire appliquer toutes les lois ou résolutions actives.

... A suivre dans le commentaire suivant

Score : 1 / Réponses : 0

----

Qualification et compétence des membres : les nouveaux membres découvrent le job ; tout se passe comme s'ils étaient en apprentissage. Pendant les premiers six mois de leur mandat ils sont donc dépendants des autres membres et relativement influençables. Néanmoins, la durée du mandat étant de 3 années, ils disposent d'un crédit temps estimé comme suffisant pour acquérir leur libre arbitre.
Par ailleurs, la formation continue, valable pour tous les citoyens dont les membres d'institution, devrait aider à l'acquisition des connaissances et des moyens nécessaires.
Certains membres, de la classe ouvriers, par exemple sont considérés par les intellectuels comme inaptes à l'exercice d'un mandat ! D’aucuns tels les membres de la classe médiatique, considèrent ces ouvriers comme des crétins ! C'est faux bien évidemment : tout individu quel que soit son niveau d'instruction est capable de prendre une décision de bon sens et celle-ci doit être respectée par les autres membres ; bien entendu,  le débat entre les membres, les explications, les notes d'information devraient aider à prendre les décisions les plus justes. La volonté de représentativité proportionnelle des classes sociales donne plus de poids aux classes chômeur, ouvrier, employé, retraité qu'aux classes cadre, profession libérale ou actionnaire. C'est voulu pour une réelle démocratie. Remarquons que les intellectuels de toute classe sont présents parmi les membres et donc capables d'élever le débat si nécessaire.
Structure de pilotage des membres et fonctionnaires : Le système démocratique proposé se limite à priori à la structure de pilotage. On peut imaginer que le système traite également du fonctionnariat c'est à dire des personnes gérant les institutions à temps plein, sélectionnés sur leurs compétences et dont la durée de mandat serait assez longue pour assurer la stabilité de ces institutions.
En effet, ce personnel fonctionnaire est payé par l’État tout comme les membres. Ce personnel travaille à temps plein pour l'institution. Doit il pour autant travailler toute sa vie pour la même institution ? C'est vraisemblablement peu souhaitable, tant pour le renouveau de l'institution que pour l'évolution de carrière de l'individu. Du coup, le nouveau fonctionnaire pourrait avoir un contrat à durée assez longue mais limitée, par exemple de 7 années. Le fonctionnaire et le membre sont rémunérés par l’État. Néanmoins, le fonctionnaire n'est pas tiré au sort, mais choisi, embauché en fonction de ses compétences. Ce qui le différencie nettement du membre de la structure de pilotage. C'est d'ailleurs cette compétence qui permet au fonctionnaire d'apporter au membres l'éclairage nécessaire pour prendre les décisions en connaissance de cause.
Difficulté : Il est possible que les fonctionnaires spécialisés dans leurs taches et plus pérennes que les membres de la structure de pilotage, puissent influencer ces derniers. Néanmoins, la soumission au vote des citoyens de toute loi/résolution ainsi que le contrôle de bonne exécution de celles-ci devrait limiter ce risque d'influence.

Fin des commentaires me concernant.

Score : 1 / Réponses : 3

----

Interactivité réelle entre les membres de commission et les citoyens :
CHAQUE COMMISSION aurait UN ESPACE SUR LA PLATEFORME où chaque citoyen pourrait proposer idées et lois, poser des questions, ce qui permettrait d'anticiper en amont les problèmes.

Score : 1 / Réponses : 3

----

Une loi balai confiée à un jury citoyen ! A la fin de chaque législature un jury citoyen pourrait rédiger un texte soumis à l'approbation de l'Assemblée Nationale - sans cavalier législatif. Le jury serait ainsi dans la production d'un débat public en exprimant une priorité dans les préoccupations des français, et surtout l'initiative de cette production échapperait au Gouvernement et aux forces politiques structurant les rapports de force parlementaires.

Score : 1 / Réponses : 4

----

Pétitions en ligne, réferenda d'initiative populaire, propositions de loi citoyennes, questions au gouvernement, toutes ces initiatives doivent afficher clairement les motivations des porteurs de ces projets et leur contribution à   l'intérêt général de la nation.

Score : 1 / Réponses : 0

----

Presque toutes les questions politiques qui peuvent se poser comportent un aspect technique qui excède les compétences de l'intervenant moyen .
 S'il n'a pas la possibilité de se faire éclairer par des experts et s'il ne prend pas le temps d'approfondir, l'intervenant moyen va apporter des réponses médiocres et c'est à bon droit que les politiciens professionnels pourront disqualifier son intervention et montrer ainsi que la participation populaire est une lubie inefficace et qu'il est bien préférable de laisser le pouvoir à ceux dont c'est le métier . 
 Ainsi, paradoxalement, des consultations citoyennes mal conduites aboutissent à disqualifier les citoyens . J'espère que ce ne sera pas le cas pour celle-ci .

Score : 1 / Réponses : 0

----

Pour faire progresser la démocratie les citoyens doivent avoir la possibilité de co-construire la loi, les outils numériques , les actions pour apporter les éléments du débat au plus proche de la population par l'intermédiaire des conseils régionaux par exemple, l'apprentissage de la participation citoyenne dans les écoles, lycées.

Score : 1 / Réponses : 0

----

Référendum pour les questions européennes: traités de libre échange, compétence exclusives de la commission, glyphosate, insecticides tueurs d'abeilles, evasion fiscale etc...

Score : 1 / Réponses : 0

----

Les citoyens français(e) doivent être invités à arbitrer les priorités national par une consultation généralisé, pour constater leurs difficultés et leurs suggestions, et s’impliquer concrètement dans l’amélioration de leurs vie.

Score : 1 / Réponses : 0

----

Redonner un contrôle de l'agenda législatif aux citoyens, en lien avec les députés. Faire de la loi une inspiration citoyenne tout en redonnant du sens à la fonction de député :
Rééquilibrer les propositions et les projets de lois. Sur 449 lois adoptées durant la dernière législature, 339 sont proposées par le gouvernement contre 110 par le Parlement (et seulement 62 par l'Assemblée Nationale). Il faut donc une réglementation du nombre de projets de lois sans doute par une réforme constitutionnelle.
Recréer du débat à l'Assemblée Nationale alors que tous les projets de lois sont adoptés en première lecture. Les citoyens peuvent laisser à un député ou au gouvernement la proposition de la loi mais les documents doivent être accessibles et compréhensibles pour que les citoyens aient la possibilité de proposer des amendements sur une période déterminée (sauf en cas de procédure anticipée) sur une plateforme numérique officielle de l'Assemblée Nationale. Ces amendements doivent être étudiés par les groupes parlementaires qui peuvent s'en saisir puis obligatoirement en commission.
Un système de Question citoyenne hebdomadaire doit être mis en place. Ce système et sa légitimité doivent être garantis par un député tiré au sort (ou élu) parmi les députés en séance.

Score : 1 / Réponses : 0

----

Le parlement élu en 2017 est une farce! Sa représentativité est plus faible que celle des centrales syndicales, c’est vous dire. L’AN est une armée d’androïdes au service d’un méprisant cyborg. La France est gouvernée par une caste malfaisante qui TRUQUE toutes les élections à son profit.

Score : 1 / Réponses : 0

----

Il est temps de faire pressions sur les acteurs qui dirigent avec des motivations de moins en moins en équation avec les peuples...Surtout en France...

Score : 1 / Réponses : 0

----

Les propositions les plus intéressantes ont déjà été formulées.
Je rejoins totalement les personnes qui ont souligné le manque de communication autour de cette consultation, et l'échéance trop rapprochée qui a été fixée pour les contributions.
Pour être positif, il s'agit néanmoins d'un premier pas de cette nouvelle Assemblée - il faut donc se saisir de cette opportunité, et il ne fait évidemment aucun doute qu'elle prendra ces remarques en considération...
Sur le fond, évidemment le référendum d'initiative populaire doit être enfin adopté. Des propositions intéressantes ont été faites en ce sens :
* prise en compte des pétitions ayant recueilli un certain nombre de signataires (entre 100 et 500 000 : à définir) en vue d'être transposées en projets de loi à adopter par le Parlement
* audition obligatoire des organisateurs de la proposition par les Commissions concernées du Parlement
* référendum obligatoire dès lors que les pétitionnaires le demandent, et que la pétition a recueilli au moins 500 000 signatures.
Pour sa part, le référendum devra être organisé selon la procédure habituelle, avec vote à bulletin secret. Il s'agit peut-être là d'une évidence, mai un détail semble avoir été oublié des promoteurs du "tout numérique" : l'appétence pour internet, les possibilités matérielles d'y accéder, les freins multiples qui s'opposent à la participation de tous les citoyens via cet outil (habitudes, niveau socio-culturel, aspects générationnels, volonté délibérée de ne pas être connecté sans arrêt, etc...) doivent aussi être pris en compte - y compris lors de la diffusion des pétitions...
Un contributeur évoquait la représentativité de ce type de consultation, et donc sa légitimité : il a tout à fait raison. 
Je suis d'accord avec la proposition de rédaction de "Article 3".

Score : 1 / Réponses : 0

----

La démocratie n'est pas un modèle figé . A l'école , dans les associations , on a l'impression qu'on es des citoyens passifs , qu'on attend que des gens prennent des décisions à notre place .
La politique , ce n'est pas une affaire de professionnels : cela devrait concerner tout le monde .
Oui à la démocratie participative 
Il y a d'abord une prise conscience de leur pouvoir 
Nous pouvons changer les choses 
Par des manifestations , pétitions ect 
L'engagement politique ça s'apprend

Score : 1 / Réponses : 0

----

je soutiens la proposition de change.org pour que les pétitions soient intégrées aux débats du parlement.

Score : 1 / Réponses : 0

----

Pourvu que cela se mette en place

Score : 1 / Réponses : 0

----

Oui, très bonne initiative vers plus de participation citoyenne, et soumission des iDs et pétitions à / par nos représentants Député(e)s.
Peut-être pourrons-nous aussi, par ce biais, nous insurger qd on constate que certaines décisions sont prises à l'Assemblée Nationale par 65 voix POUR et 14 voix CONTRE ! Où étaient les autres 500 et qqs députés ! 
C'est ça la représentativité du Peuple ?

Score : 1 / Réponses : 0

----

Les parlementaires ne doivent jamais oublier qu'ils sont nos représentants. Le vote des 11 vaccins qui va nous être " imposé " est un des exemples où les parlementaires (en l'occurence les députés) ont oubliés de voter en respectant le voeux des citoyens.

Score : 1 / Réponses : 2

----

Comme la constitution le permet les pétitions que nous citoyens signons doivent être toutes lues et débattues à l'assemblée nationale et nous devons aussi avoir nôtres mots à dire que nos députés (es) nous laisse le droit à leurs poser des questions aux sujets des pétitions que nous signons

Score : 1 / Réponses : 0

----

Redonnons le pouvoir au Peuple et en finir avec cette Monarchie Républicaine. Mettre tous les mandats électifs pour une durée de 5 ans en respectant obligatoirement la parité hommes-femmes.

Score : 1 / Réponses : 0

----

Une très belle initiative pour redonner au peuple le pouvoir qui est manifestement bafoué par des élections dirigé par la finance et les médias.

Score : 1 / Réponses : 0

----

C'est la seule chose à obtenir pour une Vraie Démocratie : http://consultation.democratie-numerique.assemblee-nationale.fr/topic/59d734d112406bdd5d6277ff#comment-59f7251ae8a9656c1a8e7390

Score : 1 / Réponses : 0

----

La démocratie en France s'est construite à une époque où les outils de communication étaient loin d'être ceux d'aujourd'hui, beaucoup moins développés et rapides, en corollaire, avec des citoyens nettement moins informés. 
Tout a changé côté communication ; la démocratie participative doit faire partie intégrante des processus de décision des politiques.

Score : 1 / Réponses : 0

----

A l'instar d'autres contributeurs, je demande que cette consultation soit prolongée d'un trimestre et relayée davantage par tous les médias...

Score : 1 / Réponses : 0

----

Instauration d un droit voire d un devoir de signalement des textes legislatifs trop complexes par un groupe de citoyen a definir, afin que l assemblee synthetise dans un texte unique les principes generaux retenus pour les lois concernes. Les dispositions detaillees demeurant au niveau reglementaire, cela constituerait egalement une aide pour le juge et contribuerait a une veritable accessibilite a la loi pour la population.

Conscient que ce chantier n est pas mince mais en donnant l initiative au peuple, c est celui ci qui determinera les urgences.

Score : 1 / Réponses : 0

----

Pour que des lois aussi idiotes que celles annonçant des réductions d’impots Sur l’achat des répartiteurs de chauffage ne soient plus votées Car ces répartiteurs n’existent qu’en location.....un citoyen copropriétaire mieux informé qu’un député qui ne peut pas tout connaître......

Score : 1 / Réponses : 0

----

une trés bonne idée à laquelle j adhère totalement et qui doit etre connue

Score : 1 / Réponses : 0

----

Suppression pure et simple du Comité Économique Social et Environnemental qui nous coûte cher, ne sert à rien et dont les rapports jamais suivis d'effet servent seulement à caler les meubles à l'Assemblée Nationale ou au Sénat.

Score : 1 / Réponses : 0

----

Mon idée: Au lieu d'une consultation aléatoire des citoyens  sur des sujets dans lesquelles ils ne sont pas spécialisés, il serait intéressant que l'état mette en place une application ou un site internet où chaque citoyen est inscrit et renseigne ses spécialisations (études, profession,...) En fonction, il serait envoyé sur une page où il peut proposer, ou approuver des projets dans sa spécialité. Les projet de lois les plus aimés par la communauté en question serait votés à l'assemblée nationale tous les mois par exemple. Ainsi, les médecins  pourront donner leur avis sur les 11 vaccins, et les artisans pourront donner des idées pour revaloriser leur corps de métier, par exemple.

Score : 1 / Réponses : 0

----

Je suis comme vous signataire de nombreuses pétitions et si certaines aboutissent à un net résultat (voir celle pour le chat Chevelu") bien d'autres n'

Nous vous proposons une nouvelle formulation de l’article 3 de la Constitution : “La souveraineté nationale appartient au peuple qui l’exerce par ses représentants et par la voie du référendum d’initiative citoyenne en toutes matières, y compris constitutionnelle et de ratification des traités. Cet article ne peut être modifié que par voie référendaire.”

Score : 1 / Réponses : 0

----

Zut c'est parti trop vite. Je reprends : Je suis comme vous signataire de nombreuses pétitions et si certaines aboutissent à un net résultat (voir celle pour le chat Chevelu”) bien d’autres n’aboutissent pas. C'est totalement décourageant ! Et en dehors du débat sur un site privé ou pas concernant le lancement de ces pétitions, je suis à 120% d'accord avec la modification de l'article 3 : “La souveraineté nationale appartient au peuple qui l’exerce par ses représentants élus et par la voie du référendum d’initiative citoyenne en toutes matières, y compris constitutionnelle et de ratification des traités. Cet article ne peut être modifié que par voie référendaire.”    Il est essentiel de redonner de la démocratie à notre pays et de faire entendre la voix des citoyens. Il reste à débattre des modalités pour que ces pétitions soient effectivement prises en comptes à l'AN : sur quels critères de nombres de signataires, de fréquence, par quel comité objectif et neutre, etc. Ceci est aussi tout un débat en soi !!!!

Score : 1 / Réponses : 0

----

Il me semble que le sujet sur lequel on consulte les citoyens est aussi important que le moyen de mettre en place cette consultation. Le problème aujourd'hui est de faire en sorte que les citoyens se sentent concernés par la consultation, qu'ils en voit l'impact concret dans leur vie au quotidien.  Or, il a été constaté depuis ces dernières années une inflation des lois, sans que les décrets d'applications ne soient toujours  pris ou appliqués, qui font parfois l'objet  d'évaluations, même restent peu médiatisées.
 Il est clair qu'aujourd'hui voter une loi ne suffit pas; les français  le ressentent et se désintéressent de la machine démocratique grippée.

Donc pour commencer une consultation sur un sujet donné, en prenant en compte tous les judicieux conseils donnés dans les autres commentaires (ex : plateforme dédiée  sur le site de l'assemblée nationale), je pense aussi qu'il faudrait d'abord commencer à produire un bilan sur le sujet en question :
 les lois déjà votées, leurs décrets d'application, ce qui marche, combien ça a coûté aux finances publiques, les freins, des exemples d'autres pays , bref un bilan concret et pédagogique, qui explique pourquoi on en vient à une consultation citoyenne sur le sujet. 
Le système d'évaluation des lois en France reste encore trop confidentiel, et la consultation numérique serait un bon vecteur pour contribuer à partager plus généralement cette pratique. 

 Tout l'enjeu est donc d'être capable de rendre compte et d'évaluer les effets des lois mises en place précédemment sur le sujet traité dans la consultation, de manière objective, sans  partir dans une usine à gaz que personne ne lira;  

La consultation devrait partir de la vie des citoyens pour exprimer le sujet  et la question posée, pour capter leur intérêt, avant de proposer un développement plus complexe.

Enfin, même si aujourd'hui, l'outil de consultation numérique est le moyen le plus évident à déployer,  je pense qu'il faut toujours croiser les méthodes pour toucher la cible. 
D'ailleurs ne dit-on pas que pour être bien informé et se forger une opinion sur une question de société,  il ne faut jamais s'en tenir qu'à une seule source d'information? 
Ainsi, une consultation lancée sur la plateforme dédiée de l'assemblée  nationale pourrait aussi faire l'objet en parallèle d'annonces informatives et de débats contradictoires,  à la radio, à la TV ou dans la presse, et  qui renvoient sur le site.

Score : 1 / Réponses : 0

----

Souhaitons, comme le disent certains commentaires, que cette action ne soit pas reprise par quelque mouvement obscur. Mais l'initiative pris est très intéressante pour le peuple. Il faut y croire.

Score : 1 / Réponses : 0

----

Tous les moyens permettant de donner la parole aux citoyens sont bons et doivent être mis en place sans délai.

Score : 1 / Réponses : 0

----

Démocratie = gouvernement du peuple, PAR le peuple, POUR le peuple

Score : 1 / Réponses : 1

----

Cela pourrait changer le vote "godillot"

Score : 1 / Réponses : 0

----

L'état doit prendre en compte l'avis des citoyens avec les moyens modernes à disposition aujourd'hui. 
Les pétitions citoyennes doit avoir un réel poids.. 
Une manière de communication en prise directe entre le peuple et les élus/dirigeants (souvent un peu déconnecter de la vie réelle).

Score : 1 / Réponses : 0

----

Récompenser la participation citoyenne.
Nous sommes dans une société du jeu et les éditeurs et publicitaires de toutes sortes l'ont bien compris et affublent leurs outils, campagnes de récompenses régulière afin de garder captif leur utilisateur. Pour inciter le citoyen a participer aux référendums, élections et autres actions participatives, pourquoi ne pas utiliser les mêmes recettes que les éditeurs de logiciels ou autres vendeurs en "gamifiant" le vote.
Cela consisterait, pour chaque vote ou participation effective à récompenser le votant par, par exemple, via un crédit d'impôt de quelques dizaines de centimes pour une élection ou un référendum et de quelques centimes par contribution ou vote contributif. Pour être "numérique", l'émargement du votant pourrait être inscrit dans une blockchain étatique, autorisant le vote physique de n'importe où. Pour ce qui est de la participation au débat, la récompense supprimera l'effet vote multiples d'une même personne, un compte en banque étant associé à un utilisateur et un seul.

Score : 1 / Réponses : 0

----

A défaut des référendum populaires qui terrorisent tant nos "représentants" politiques, ( Cherchez l'erreur !!! ) il s'agit peut-être de la seule solution pour que le peuple se fasse un peu entendre d'une castre, souvent sourde et aveugle à ce qui ne touche pas ses intérêts...
Merci à "change" !

Score : 1 / Réponses : 0

----

d'abord le CESE, puis l'AN, manque plus que le Sénat est la boucle sera bouclée !!!
une bouffée d'oxygène pour notre démocratie en danger dans une république à l'agonie...

Score : 1 / Réponses : 0

----

DEVEROUILLER LE REFERENDUM D’INITIATIVE PARTAGEE
Le référendum d’initiative partagée a été inséré dans l’article 11 de notre Constitution en 2008 par Nicolas Sarkozy mais est resté inutilisable depuis car les seuils de déclenchement ont été fixés trop haut. Pour qu’il devienne possible à un groupe de citoyens de l’utiliser sur une question précise, il suffirait de baisser, par simple décret, le nombre de citoyens nécessaires pour le demander (par exemple de 4,6 millions à 2 millions de personnes) et le nombre de parlementaires à rassembler pour le soutenir (par exemple de 185 à 100 parlementaires).

Score : 1 / Réponses : 0

----

L'initiative citoyenne doit être intégrée au processus démocratique afin que des liens plus directs, plus fréquents et plus ténus s'établissent entre les citoyens et leur représentants. Les conditions de cette initiative doivent être clairement définies, formalisées et les outils nécessaires à sa mise en oeuvre doivent être crées et disponibles à tous. Le processus de suivi, d'acceptation ou de rejet doit être clairement définit également. Il en va de la crédibilité de la représentativité de nos élus tout au long d'une mandature et de la prise de conscience des citoyens dans la complexite des prises de positions et de décisions. Ce processus devrait, à mon avis, comprendre notamment les étapes suivantes :
. La collecte en ligne d'un contenu de questions/propositions/remarques émanant des citoyens.
. La mise en forme d'un texte cohgérent regroupant un nombre maximun de ces idées.
. La présentation de ce texte aux citoyens pour validation/adhésion avec travail commun sur ce texte.
. La mise en oeuvre d'un pétition sur la base de ce texte.
. La transmission d'un suivi de cette pétition étape par étape, interlocuteur par interlocuteur, ainsi que du processus de décision.
. La communication des votes, de la décision et des recours

Score : 1 / Réponses : 0

----

Pétition citoyenne change.org, 350.org, mes opinions.com, sauvons la foret.org, + assocs, ONGs prit en compte par le cese récemment qui pourrait aussi par l'assemblée nationale par une nouvelle commission avec 1/2 élus 1/2 de citoyens par tirage au sort modifié ts les ans. Référendum initiative citoyenne comme ICE (europe- 1 millions) pour CETA, les accords commerciales, les projets de lois pour le climat, transition énergétique (augmentation renouvelables, arrêt charbon, nucléaire, hydrocarbures...) écologique (protection de l'air, eau, forêt, biodiversité sauvage, protection de l'environnement, zones humides) au profit des projets d'infrastructures inutiles qui a cause de bcp de lobbys bloquent les lois au parlement, donc les citoyens auront du pouvoir pour agir concrètement que lors des élections qui aujourd'hui ne suffit plus/pas, pour avoir du poids pour soutenir nos élus, l'avis de la population pour l'intérêt générale et non que profit économique donc aboutir/réussir à promulguer ces lois . "la république est le gouvernement du peuple, par le peuple et pour le peuple" que cette expression prenne son sens aujourd'hui.

Score : 1 / Réponses : 0

----

Bonjour ayant travaillé de nombreuses années dans le milieu de la TV, il me semble que l'outil internet devrait être associé à une émission de télévision intéractive sur la chaîne parlementaire et une web TV propre afin que les projets soient travaillé et soumis en amont de manière globale : Internet + TV afin que tout le monde puisse interagir même celles et ceux qui ne sont pas  l'aise avec le numérique ou n'y ont pas accès.

La construction s'articulerait sur la base d'internet et accompagnée en support explicatif et constructif via l'émission.

je pourrai développer le concept sans souci ICI ou directement.

Score : 1 / Réponses : 0

----

Au sein de notre comité, après en avoir débattu, nous pensons que l’initiative citoyenne pourrait se concrétiser au niveau d’une plateforme numérique qui serait relayée pour permettre une participation citoyenne la plus large possible.

Les contributions pourraient être hiérarchisées : Citoyen seul = niveau 1 / comité = niveau 2… Plus l’idée aurait été travaillée par un grand nombre de personnes, plus elle monterait de niveaux. 
L’initiative citoyenne ne peut être efficace que si l’on peut s’assurer que les idées remontées soient effectivement discutés au Parlement. C’est pourquoi il serait pertinent de définir un seuil à partir duquel obligation serait faite aux parlementaires de se saisir du sujet.

L’idée de la pétition citoyenne en ligne peut suffire pour les thématiques simples. En fonction du degré de sensibilité ou d’importance de la question à aborder, il peut être opportun de basculer sur un autre vecteur de proposition (référendum d’initiative citoyenne, question au gouvernement...).

Score : 1 / Réponses : 0

----

Une plate-forme de consultation des citoyens doit être ouverte sans interruption. Une nouvelle commission formée de plusieurs députés aurait pour mission de faire régulièrement remonter aux autres députés les idées, inquiétudes, interrogations, incompréhensions que les Français auraient soumises sur cette plate-forme.

Score : 1 / Réponses : 0

----

Il faut une prise de conscience individuelle puis collective sur l'importance de la démocratie en France et ailleurs.

Score : 1 / Réponses : 0

----

Avant tout, pour que des citoyens puissent proposer quelque chose ou réagir sur des débats parlementaire, ils doivent être sur un même pied d'égalité que le législateur. Cela passe en premier lieu par l'accès à une information synthétique complète et fiable. Cela suppose que l'ensemble de l'appareil politique mette à disposition des éléments pédagogiques de qualité sur les motivations et le sens des propositions, sur les conséquences, sur les alternatives (et pourquoi elles ne sont pas privilégiées), sur ce qui existe déjà

Score : 1 / Réponses : 0

----

Vers un autre mode de scrutin plus représentatif  du peuple et qui serait la solution pour vaincre l'abstention

Score : 1 / Réponses : 0

----

Pour qu'il existe une initiative citoyenne équitable, il faut qu'une majorité de français s'intéresse à l'elaboration des politiques et se sente légitime pour y contribuer. Ce n'est malheureusement pas le cas aujourd'hui. Cette posture existe au sein d'un monde plutôt urbain et connecté à Internet ou à des collectifs tels que partis, syndicats, associations militantes. Mais quid des autres ? Outre l'accès à Internet, qu'il convient de développer partout, il faut faire vivre cet esprit citoyen dans la vie locale (y compris et surtout monde rural) et, en premier lieu, le faire naître. L'éducation est un facteur majeur de l'esprit citoyen : savoir décider des affichages politiques,comprendre des programmes politiques, le fonctionnement concret des institutions, le rôle du citoyen là-dedans, savoir analyser de manière critique. Le tout de manière concrète,  avec des  échanges  avec des  parlementaires, des cas  concrets, etc. Les marges de progrès existent et sont importantes en matière d'éducation nationale.

Score : 1 / Réponses : 0

----

-	Accessibilité : ruralité, populations en situation de précarité, trop âgées, les applicatifs.
-	Formation : méthodologie du numérique, fonctionnement de la politique, institutions.
-	Accompagnement : accompagner les citoyens à la dématérialisation, service civique.

Contribution collective du comité LREM Rennes Centre Sud

Score : 1 / Réponses : 0

----

Ca serait bien que chaque texte de loi proposé fasse l'objet d'une étude d'impact pour en mesurer les effets prévisibles, études réalisées évidemment par des cabinets indépendants. Ces études seraient réalisées sur un laps de temps défini et court (il ne s'agit pas d'enterrer la démocratie mais seulement les fausses bonnes idées).

Score : 1 / Réponses : 0

----

Vu le peu de participation à cette consultation citoyenne, je doute que la grande majorité des citoyens que nous sommes en ait eu connaissance. En conséquence, je souhaite que celle-ci soit prolongée d'au moins 15 jours et qu'une large information de son existence soit diffusée dans les médias les plus consultés ou regardés.

Score : 1 / Réponses : 0

----

Mais tout ceci ne devrait être que l'aboutissement d'autres engagements :
-obligations d'aller voter : petit rappel des résultats officiels :
Elections présidentielles : 1er tour : Macron (18.19%) : abstentions (22.23%) : votes blancs (1.39%)
Elections présidentielles : 2éme tour : Macron (43.61%) : abstentions (25.44%) : votes blancs (6.35%)
-obligation de participer à la vie sociale des entreprises : aujourd'hui avec moins de 10% de personnes syndiquées, c'est donc 90% qui ne s'intéresse jamais à autre chose qu'à eux-mêmes dans un environnement professionnel qui occupe 80% de leur vie active.
Et donc pour revenir à l'intérêt de la prise en compte des pétitions, que pensez vous qu'il devrait se passer si les pourcentages de désintérêts sont aussi élevés que les 2 précédents qui devraient les concerner de près.
Comme je suis non seulement un citoyen qui vote mais aussi qui a pris largement de son temps pour défendre ses petits camarades, je ne peux qu'être pour une intégration de la pétition dans le débat parlementaire.
Sans grand espoir que cela change vraiment la mentalité des français.

Score : 1 / Réponses : 0

----

Contribution effectuée par un groupe de citoyens de la 13e circonscription des Hauts-de-Seine ( Antony, Bourg-la-Reine-Châtenay-Malabry, Sceaux)

Les pétitions en ligne du type « change.org » sont devenues très populaires et engendrent des dizaines, voire des centaines de milliers de signatures. Le problème avec ce type d’outil est qu’il n’y pas de contre-pouvoir car pas de débat. Il suffit que la personne qui créé la pétition la tourne d’une certaine façon pour avoir un maximum d’adhésions. 

Le référendum d’initiative populaire n’est pas prévu par la Constitution. Par ailleurs, ce type d’outil est risqué, comme cela a été le cas dans plusieurs cantons en Suisse. En effet, si des personnes veulent faire sortir la France de l’UE ou décident de légaliser le port d’arme, la majorité d’un groupe qui ne représente pas la majorité des Français pourrait voter et faire passer un projet. 

En ce qui concerne les propositions de loi citoyennes, il existe plusieurs freins. En effet, il n’est pas facile de comprendre tous les tenants et les aboutissants d’une proposition de loi. Et la rédiger est un métier. Seule une petite frange de la population serait à même de le faire. 

La question au gouvernement citoyenne est un bon outil pour avoir un rapport direct avec un élu, à l’image du Facebook live d’Edouard Philippe. Il est tout de même important d’avoir un modérateur car il faut tout de même sélectionner les questions dans le cas où il y en aurait en trop grand nombre. 
Le problème est que l’élu peut être accusé de ne répondre qu’aux questions qui l’intéressent. Reste qu’aucun outil n’est parfait et celui-là reste un bon moyen de développer un contact direct entre un élu et les citoyens.

Dans le cas d’un député, le Facebook live peut-être limité aux initiatives citoyennes et non ouvert à toute question sur tout sujet.

Score : 1 / Réponses : 0

----

Je pense qu’il est temps de réellement taxer beaucoup moins le travail (100 euros au total charges patronales et salariales ?) et de réduire le temps de travail si l’on veut lutter contre le travail non-déclaré qui coûte 30 milliards par an à la France et voir le chômage baisser fortement.
Baisse des charges contre diminution du temps de travail vers les 32 heures avec création d’emplois.
Les baisses de 2% sont des fausses mesures qui ne donnent aucun résultat.
On doit en contre partie taxer les mouvements boursiers.
C’est de l’argent qui profite à une toute petite minorité et ne produit ni travail ni richesse naturelle.

Score : 1 / Réponses : 0

----

Pour une citoyenneté active !

Score : 1 / Réponses : 0

----

Le choix qui s'offre à nous aujourd'hui est simple:
- Soit les personnalités politiques acceptent de nous entendre en tenant compte des pétitions que nous leur adressons et dans ce cas, s'ouvrira pour toutes et tous une vraie chance de vivre dans une Démocratie saine.
- Soit elles font le choix inverse et les conséquences seront les suivantes :
a) De plus en plus de gens seront tentés par l'abstention.
b) L'abstenton augmentera par contre-coup les scores de l'extrême-droite, comme d'habitude...

J'espère que les parlementaires réfléchiront soigneusement avant de prendre leur décision.

I have spoken.
#KingOfCats

Score : 1 / Réponses : 0

----

Faire vivre la démocratie au quotidien pour que les citoyens puissent être le moteur de la vie démocratique :
Plusieurs initiatives sont possibles, tout d'abord, organiser des ateliers citoyens où chaque citoyen peut proposer de nouvelles idées qui seraient ensuite traduites en loi ou en proposions de loi par des élus. 
Ensuite, développer des outils numériques comme les forums ou les pétitions pour permettre à chaque citoyen d'enrichir le débat et de faire valoir ses idées car la vie démocratique ne peut pas se réduire au seul moment de vote.
Enfin, ces initiatives citoyennes ou ces consultations devront porter sur les grands projets qui touchent la Société, l'Environnement, le Territoire et l'Europe afin d'éviter un engorgement du travail législatif.

Score : 1 / Réponses : 0

----

Je propose les suivants 15 projets des lois (partie 1) le 5 novembre 2017 à la Consultation.Democratie-Numérique.Assemblée-Nationale.FR

1)Les demandeurs d’emploi devraient bénéficier dès que possible des avantages/facilités fiscales y compris les charges sociales et sans d’être imposés (ou imposées) fiscalement, pendant minimum les premiers 5 ans de la création d’une entreprise ou de la reprise d’une entreprise ;

2)La CAF +Pôle Emploi+ les autres institutions publiques+chaque les employeur d’Etat et privé soit obligés (et obligées) de s’interconnecter et de se fournir/délivrer entre elles/entre eux tous les documents prouvant (attestations d’employeurs, contrats de travail, bulletins de salaires, préavis de licenciement, ruptures de contrats de travail, décisions de licenciement, décisions de démissions, certificats de travail, certificats de congé médical, etc.) l’embauche et l’historique à jour de chaque salarié afin de décharger de ces obligations trop stressantes, fatigantes, énergofagues, qui sont au présent mises sur les épaules des citoyens et chaque telle institution (énumérées plus haut) soit obligée d’envoyer gratuitement et régulièrement à chaque occasion (par semaine ou par mois), PAR EMAIL ET PAR LA POSTE, à chaque personne citoyenne concernée tous les documents et attestations prouvant leur situation personnelle (mise à jour) professionnelle et financière, y compris leur historique professionnel et financier mis à jour : les effets seront bénéfiques pour : A)L’écologie, en diminuant ainsi l’effet de serre et le réchauffement au niveau de la France ; B) L’élimination importante d’un des facteurs de stress et de la dépression qui causent TROP de suicides en France ; C) ainsi sera réalisée une meilleure et plus démocratique inter-connectivité entre les services de L’Etat avec les citoyens ; D) Sera ainsi assurée la meilleure ou la parfaite gestion et mise à jour de l’immense BASE DE DONNEES NATIONALE DE LA France ; E) Sera réalisée ainsi une GRANDE SATISFACTION ET GRATITUDE ET CONFIANCE DE CHAQUE CITOYEN auprès du system, des institutions de L’Etat et du Gouvernement ;

3)La création et l’installation des bornes ou des pôles d’internet (ayant aussi une imprimante et un téléphone publique gratuite pour les appels en France) utilisables NON-STOP et partout en France, sur les domaines publiques, sur les rues, sur les autoroutes, dans les marchés publiques, dans l’intérieur et dans l’extérieur de chaque ville et commune de la France ;

4)La Loi « LA TABLE DE LA NATION FRANCAISE » : par laquelle soit crée dans chaque ville / commune des restaurant-cantines de L’Etat (soutenus financièrement par l’Etat et par des fonds de L’Union Européenne et par les grandes entreprises françaises et étrangères siégées en France et par les milliardaires et millionnaires particuliers français et étrangers imposés en France) un repas chaud (Menu complet) par CHAQUE JOUR 7jr/7jr aux demandeurs d’emploi et à toutes personnes démunies pas imposables fiscalement ;

5)La Loi « LES VOITURES ELECTRIQUES » : par laquelle le Gouvernement va offrir chaque année progressivement à chaque citoyen (en commençant aves les demandeurs d’emploi et les personnes démunies financièrement) une voiture électrique SANS LE DROIT DE LA VENDRE, en obtenant et attirant dans ce but des fonds des grandes entreprises françaises et étrangères en échange de les promouvoir leur produits/services ;
***Va suivre toute de suite, en bas, la deuxième partie :

Score : 1 / Réponses : 0

----

2ème partie de mes 15 projets des lois que je propose le 5 novembre 2017 à la Consultation.Democratie-Numérique.Assemblée-Nationale.FR

6) La Loi « AUTOROUTES SOLAIRES FRANCAISES » :   installer, monter et interconnecter SUR LES 2 COTES DE CHAQUE AUTOROUTE de France des PANNEAUX PHOTOVOLTAÏQUES SOLAIRES qui auront 4 surfaces exposées au soleil (la côté droite de l’autoroute aura 2 surface : par l’extérieur une et la 2ème par l’intérieur de l’autoroute, idem pour la côté gauche) + des PANNEAUX PHOTOVOLTAÏQUES SOLAIRES centraux au milieu et tout au long de chaque autoroute, qui auront –ils aussi 2 surfaces exposées au soleil : l’énergie solaire électrique GRATUITE obtenue ainsi sera vendue et va crée un grand ou gros revenu qui pourrait être distribué aux investisseurs et au Gouvernement, en baissant ou annulant les taxes des autoroutes (valables cet avantage seulement pour les citoyens de la France, pas pour les touristes étrangers) et en baissant beaucoup le prix du gasoil, aussi ;

7) La Loi « LA TRANSPARENCE PUBLIQUE DU RECRUTEMENT » : chaque année, dans chaque agence Pôle Emploi, chaque entretien ( programmé ou pas programmé ) avec chaque  demandeur d’emploi devra être enregistré audio et vidéo en utilisant les vidéo-caméras qui sont déjà installées dans chaque agence Pôle Emploi dans le hall de l’accueil (et cela est très bon) et chaque demandeur d’emploi devras avoir le droit de recevoir gratuitement et sur place à sa demande, à la fin de son chaque entretien(programmé ou ad-hoc) avec son conseiller Pôle Emploi ou à la fin de son dialogue avec le fonctionnaire de l’accueil de l’agence Pôle Emploi la copie de l’enregistrement audio-vidéo ;

8) La Loi du « BENEVOLAT AU BENEFICE DES INSTITUTIONS DE L’ETAT : Préfecture, CAF, Pôle Emploi, Service d’Assistance Sociale, Mairies, La D.D.T., etc. » : par laquelle chaque citoyen pourra offrir gratuitement dans une manière bénévole ses services par ses capacités/compétences diverses au bénéfice des institutions de L’Etat ;

9) La Loi « LE SERVICE CIVIQUE DE TOUS LES CITOYENS, Y COMPRIS DE PERSONNES PLUS AGEES DE 25 ANS » : le Service Civique actuel devrait s’élargir et s’ouvrir à tout le monde, y compris aux personnes plus âgées de 25 ans, afin de pouvoir ainsi bénéficier chacun d’entre eux correctement et honnêtement d’un revenu sûr de minimum 500 euros net par mois, durant de missions de minimum 3 mois ou 6 mois, comme gratitude pour leur travail offert au bénéfice de la société française, y compris les diverses institutions de L’Etat ou diverses entreprises, chacun d’y sortant gagnant-gagnant ;

10) La Loi « L’ETAT en chat sur ligne (online) avec les citoyens » : Toutes les institutions et services publiques de L’Etat (en commençant prioritairement par les services de l’Assistance Sociale) devraient commencer bénéficier NONSTOP et utiliser NONSTOP dès que possible des connections de chat sur l’internet avec le public (pour les messages offline et pour les messages online), afin de dégrever ainsi les services publiques actuels qui sont encore trop agglomérées par les demandes des entretiens physiques de la part du public : par exemple, au présent une Assistante Sociale peut offrir un entretien programmé seulement en 2 ou 3 mois ( !), cela signifiant qu’elle peut assister, recevoir, communiquer directement et emphatiser directement avec la personne qu’elle assiste seulement 4 ou maximum 5 fois par an( !) ;

11) La Loi « LIBERTE D’ENTREPRENDRE ET DE CREER DES ENTREPRISES AUSSI POUR LES AVOCATS DE LA France » : Dès que possible, tous les Maîtres devraient commencer bénéficier du droit de créer et d’avoir leur propre entreprise en France (comme ailleurs dans des autres pays), personne, aucune partie, ni les Maîtres, ni L’Etat, n’ayant rien à perdre, tout le monde aura à gagner, y compris les centres d’impôt ; 
***Va suivre toute de suite, en haut, la dernière partie (la troisième partie) :

Score : 1 / Réponses : 0

----

3ème partie de mes 15 projets des lois que je propose le 5 novembre 2017 à la Consultation.Democratie-Numérique.Assemblée-Nationale.FR

12) La loi « LIBERTE D’ENTREPRENDRE AU MAXIMUM ET SANS LIMITES EN FRANCE » : par laquelle tous ceux qui désirent créer une entreprise vont pouvoir avoir le droit de déclarer et d’utiliser TOUTES LES CODES CAEN pour pouvoir dérouler légalement à tout moment, n’importe quelle activité existante dans le nomenclateur des activités légales de France, afin de mettre en pratique à tout moment une, deux, huit, vingt, ou «N»-ième activité déclarée déjà dans le statut de son entreprise, en fonction du marché, du marketing et en fonction des opportunités des affaires et de ses compétences professionnelles et de ses ressources (du temps, d’énergie et d’argent), en sachant que chacune de ses éléments de l’équation peuvent varier d’un jour à l’autre et afin de ne pas limiter -–comme à présent— les entrepreneurs de déclarer et de dérouler SEULEMENT MAXIMUM 5(cinq)activités différentes, spécialement qu’il y a ailleurs dans L’UE des pays plus petits et plus pauvres que la France qui offrent cette liberté entrepreneuriale et ses opportunités légales à leurs entrepreneurs, les aidant ainsi de gagner et de booster ainsi  leur entreprise, leur revenu et profit, contribuant ainsi à booster l’économie et L’Etat ;
***************************************************************************
13) La Loi « LA BOURSE DES IDEES ET PROJETS DES CITOYENS DE LA FRANCE » :  par laquelle chaque personne domiciliée en France va pouvoir contribuer à l’émergence de la France et du bien-être de la société française par son apport intellectuel, en proposant ses idées et ses projets qui seront récompensés par L’Etat par l’argent : par des Prix provenant des sponsors privés, y attirés à participer par leur contribution financière, en échange d’obtenir de la part de L’Etat la promotion publique (partout) de leur compagnies/services/produits ;
***************************************************************************
14) La Loi « SOYEZ BIEN REVENU(E)S EN FRANCE AUX GRANDS FORTUNÉS » : loi par laquelle les grands fortunés et fortunées ne soit pas plus taxés/taxées (imposé/imposées fiscalement) pour qu’ils reviennent en France, avec la condition qu’ils investissent en France, qu’ils recrutent en France en créant des emplois et qu’ils créent des autres prochaines entreprises seulement en France ; 
***************************************************************************
15) La Loi « FRANCE 2018 PARADIS FISCAL » : loi par laquelle à partir de janvier 2018 LA FRANCE DEVIENNE pour une période temporaire de temps et expérimentale un PARADIS FISCAL afin d’ATTIRER VITE (immédiatement) TOUTES LES INSTITUTIONS BANCAIRES ET TOUTES LES AUTRES ENTREPRISES ETRANGERES ET LES ENTREPRENEURS (Businessman et Businesswomen) étrangers qui, à présent, veulent partir (quitter) vite de la Grande Bretagne, de Catalogne et d’autres pays et afin aussi de faire revenir en France tous les patrons français et étrangers partis à l’étranger.

Vive LA France, Cordialement, Adriana ANDRONIC

Score : 1 / Réponses : 1

----

Tous les points me semblent effectivement importants. La législation en vigueur cependant n'est guère appliquée. Il faut donc compléter par un processus de contrôle de l'enregistrement des pétitions, de leur traitement et d'un délai de réponse. Le point 2, fixer les conditions de recevabilité des pétitions, mériterait par ailleurs lui-même un débat: il faut éviter les abus (le législateur ne peut examiner n'importe quelle demande), mais les citoyens engagés doivent être assurés d'être écoutés.

Score : 1 / Réponses : 0

----

Je remercie vivement les forces actives de l'équipe de Change.org et les soutiens, collaborations diverses associées et motivées qui ont contribué à ce que ce système démocratique de contact, de regard, d'interactivité avec l'Assemblée Nationale et les députés ...puissent voir le jour. C'est pour moi un début de Démocratie Citoyenne un peu plus présent et appuyé pour que le peuple français puisse interagir avec les politiques afin de se faire entendre, consulter, réagir, proposer, avoir droit de regard de transparence et finalement se faire force de proposition ...
Merci à Change.org et à toutes les forces vives de notre France d'en être arrivé à ce stade ... BRAVO A NOUS TOUS ET TOUTES ... ON PEUT ETRE FIERS DE NOUS ... VIVE LE PEUPLE

Score : 1 / Réponses : 0

----

Quand nos avis seront-ils vraiment pris en compte  ?

Score : 1 / Réponses : 0

----

Pour que les élus se sentent comptables de leurs décisions et liés aux électeurs tout au long de leur mandat et pas seulement en campagne électorale. Non à la "pédagogie", nous ne sommes pas des enfants, mais oui aux explications et aux comptes rendus régulièrement.

Score : 1 / Réponses : 0

----

C'est une initiative simple et réfléchie. Un bon moyen pour redonner a chacun confiance dans la démocratie.

Score : 1 / Réponses : 0

----

J'approuve totalement les propositions d'Article 3 sur le Référendum d'Initiative Citoyenne (RIC), présentées ci-dessous par Yvan Bachaud.

Score : 1 / Réponses : 0

----

Soyons acteur de notre démocratie. utilisons les nouveaux moyens permettant de le faire.

Score : 1 / Réponses : 0

----

Je soutiens cette initiative qui me semble fondamentale pour une meilleure démocratie.

Score : 1 / Réponses : 0

----

pour être un peu plus dans une vraie démocratie :
Obligation pour l’Assemblée nationale d’étudier une pétition dès qu’elle atteint 100 000 signataires comme en Grande-Bretagne. Organisateurs et organisatrices de la pétition doivent être invités à l’Assemblée nationale pour être écouté en audition et travailler dans les commissions en rapport avec la pétition.

Score : 1 / Réponses : 0

----

- Proposition de Renaissance Numérique et la Fondation Jean Jaurès

Les citoyens peuvent soumettre à discussion de l’Assemblée nationale une proposition de leur choix :

- Comme cela s’opère déjà dans d’autres démocraties européennes, les citoyens doivent pouvoir mettre à l’ordre du jour des propositions pour discussion à l’Assemblée nationale, sur la base de ses compétences. Comme les questions au gouvernement, un moment du temps de la vie de l’Assemblée est consacré à ces questions, afin de se prononcer sur la proposition et la commenter. Si la proposition suscite l’intérêt ou l’adhésion de l’Assemblée, elle est alors transformée en décision formelle (proposition de loi, délibération), et adoptée selon les formes réglementaires. 

- Pour pouvoir être présentée aux institutions concernées, le texte ou la question doit recueillir au moins 5 % de signatures de l’électorat français concerné, réparties dans quatre régions sur treize pour une loi. 

[Proposition issue du Livre blanc "Démocratie : Le réenchantement numérique" : http://bit.ly/2zAPucV]

Score : 1 / Réponses : 0

----

Groupe n°1 sur les initiatives citoyennes :

- Droit de proposition à travers une pétition des citoyens. (31 voix)

- Boîtes à idées numériques et physiques. (17 voix)

- Consultations citoyennes régulières. (14 voix)

- Parlement local avec représentation du député. (10 voix)

- Intervention citoyenne possible dans l’hémicycle. (9 voix)

- Consultation régulière du parlement des jeunes. (7 voix)

- Bornes de consultation en mairie par exemple au sujet de certains dispositifs législatifs. (7 voix)

- Sollicitation régulière des citoyens sur des sujets précis. (7 voix)

- Collecte régulière et organisée, à l'initiative des élus, des enjeux citoyens. (5 voix)

- Processus clair des initiatives législatives, mettre en place un calendrier avec des phases de concertation clairement identifiées. (2 voix)

Score : 1 / Réponses : 0

----

INITIATIVE CITOYENNE: que chaque institution (mairie, département, région, assemblée) ait un cahier de proposition en ligne sur sa page web.

Score : 1 / Réponses : 0

----

Espèrons que cela n'est pas que poudre aux yeux et que les avis des citoyens seront consultés et débattus ...

Score : 1 / Réponses : 0

----

Une excellente initiative ! En ces temps politiques fragiles, il est important de renouer le lien entre peuple et élus représentants, en insufflant plus de démocratie. Cela passe par une plus grande prise en compte de l'avis citoyen, des pétitions et consultations publiques. Le peuple souverain a des avis et des idées ! :)

Score : 1 / Réponses : 0

----

La démocratie semble complètement noyée dans la mer politique. Ecoutez encore les voix de vos citoyens/citoyennes. C'est bien possible.

Score : 1 / Réponses : 0

----

Excellente initiative. Mais pourquoi une communication si tardive et si discrète ?

Score : 1 / Réponses : 0

----

Hier l'Etat pouvait s'abriter devant la complexité de mise en oeuvre de la consultation citoyenne pour s'en affranchir, aujourd'hui la technologie permet cette consultation, indispensable pour que la démocratie commence à s'exercer en France.

Score : 1 / Réponses : 0

----

Le droit de pétition à l’Assemblée nationale, qui existe depuis 1958, doit être clarifié et renforcé de la façon suivante :

    Prise en compte des pétitions en ligne pour tenir compte des usages actuels en terme de mobilisation citoyenne en France
    Définition de critères transparents sur la recevabilité des pétitions (exemple : lorsqu’elles portent sur des propositions de loi en cours ou en débat, lorsqu’elles apportent un point de vue différent de celui des organisations syndicales, patronales ou des associations, lorsqu’elles fournissent une expertise technique ou citoyenne supplémentaire)
    Réponse obligatoire à toutes les pétitions déposées et communication claire et motivée lorsqu’une pétition est jugée non recevable
    Définition d’une fréquence d’examen des pétitions (par exemple : une pétition étudiée chaque mois)
    Invitation des auteurs des pétitions examinées à participer aux auditions organisées lors des travaux parlementaires, afin de valoriser leur expertise citoyenne

Depuis août 2017, le Conseil Économique Social et Environnemental a décidé de prendre en compte les pétitions en ligne : http://www.lecese.fr/content/lecoute-des-citoyennes-mission-fondamentale-du-cese

Aujourd’hui, c’est au tour de l’Assemblée nationale de donner un débouché législatif aux pétitions citoyennes, à travers les modalités proposées ci-dessus.

Score : 1 / Réponses : 0

----

Pour une meilleure collaboration citoyenne; rendre les citoyens plus responsables.

Score : 1 / Réponses : 0

----

Pour signer la petition:
http://www.article3.fr/actions/petition-pour-l-instauration-du-referendum-d-initiative-citoyenne-en-france

Score : 1 / Réponses : 0

----

Le droit de s'exprimer fait partie de la démocratie et de plus, le peuple doit avoir le droit sur beaucoup plus de points en ce qui concerne le fonctionnement du gouvernement.

Score : 1 / Réponses : 0

----

Contrairement à la plupart des commentaires ci-dessous, je suis opposé à toute généralisation du référendum. Le référendum est un leurre pour la démocratie. Sous sa fallacieuse simplicité, il cache des processus très complexes d'influence, surtout quand il est à l'échelle de plusieurs millions de citoyens soumis au monde médiatique. Il est dangereux car les résultats deviennent tabous et au lieu de résoudre les problèmes il les bloque et les amplifie. Les résultats des derniers référendums (par exemple Brexit, Catalogne, ND des Landes, Maastricht, ect.) devraient faire réfléchir les zélateurs. D'autres formes d'initiatives populaires sont à considérer plus sérieusement : pétitions, contrôle de l'activité parlementaire, débats citoyens locaux.

Score : 1 / Réponses : 0

----

Obliger chaque collectivité territoriale (commune, interco, département, régions) à proposer un dispositif de participation citoyenne, numérique et présentiel, et rendre compte des propositions / pétitions / votes qui en sortent.

Score : 1 / Réponses : 0

----

Bonjour, 
Le think tank point d'aencrage a publié cette année son rapport démocratie, technologie & citoyenneté. 

http://pointdaencrage.org/2017/05/09/democratie-technologie-citoyennete-construire-nos-institutions-numeriques/

Parmi les 10 propositions du rapport on trouve en particulier:  
OUVRIR LES CONSEILS MUNICIPAUX À L’INITIATIVE ET LA CO-CRÉATION DES
HABITANTS

Nous avons été particulièrement inspirés par les innovations de la ville de Grenoble autour des petitions citoyennes examinées en conseil municipal et par celles du village de Saillans qui a complètement ouvert son conseil municipal et ses instances de décision aux citoyens.
L’organisation des débats en conseil municipal est régie par le Code général des collectivités territoriales (articles L. 2121-7 à L. 2121-27-1) et, dans les communes de plus de 3 500 habitants, par un règlement intérieur. Ces textes ne donnent pas de place à la parole citoyenne. Nous proposons d’expérimenter ou généraliser quelques mesures simples :
‣ Permettre à un collectif citoyen de convoquer le conseil municipal - aujourd’hui, seul le
maire peut le faire.
‣ Laisser place à la remontée de questions et pétitions des habitants devant le conseil
municipal lorsque celles-ci sont portées par une part significative de la population.
‣ Encourager l’initiative des citoyens et la co-création avec les élus en leur ouvrant le
droit de faire des propositions aux différentes commissions municipales.
‣ Renforcer la transparence des décisions. Les procès-verbaux du conseil municipal, les
budgets, les comptes de la commune et les arrêtés municipaux doivent être mis à
disposition en open data et diffusés de manière proactive aux citoyens.

Score : 1 / Réponses : 0

----

La prise en compte de la parole citoyenne via l'étude des pétitions en ligne permettrait peut être de redynamiser la participation aux élections !

Score : 1 / Réponses : 0

----

Tant pis si c'est trop tard le jour où je déciderais de vous rejoindre.Mais j'ai une très mauvaise opinion de l'Assemblée nationale en ce moment.Elle est  devenue une salle d'enregistrement et non une assemblée démocratique.je ne crois pas à cette liberté démocratique par référendum via l'Assemblée.On à vu lors du dernier référendum que les Français ont voté.on a voté NON et qu'à fait l'Assemblée elle a voté le contraire du peuple.Donc cette assemblée  est non démocratique et ne suivra pas les décisions du vote des référendums qui ne seront pas du même avis .mon point de vue.je regrette...

Score : 1 / Réponses : 0

----

De nombreux commentaires convergent pour mettre en place une plateforme (nourrie par des mouvements pré-existants tels que Change.org ou autre) officielle de l'Assemblée Nationale où au-delà d'un certain nombre d'adhésions, la demande est prise en compte par les députés. Cette plateforme permettrait:
- De se prononcer contre un projet de loi,
- De proposer des amendements ou même des lois, 
- De mettre à l'ordre du jour des assemblées tel ou tel sujet à débattre,
- De mettre en lien les groupes de travail avec des comités citoyens, etc.

Score : 1 / Réponses : 0

----

Le salaires des députés doit être calqués sur le salaire moyen de la circonscription où ils ont été élus pour une meilleure représentativité. Ainsi, ils se rendront compte des problèmes du Français moyen, et non du Français qui gagne 6000€ par mois (si non-cumul de mandats...)

Score : 1 / Réponses : 0

----

Depuis deux siècles nous sommes représentés, un peu comme le sont les mineurs. Il me semble que nous avons grandi, non ?

Score : 1 / Réponses : 0

----

DU CHOIX BINAIRE VERS LES ALTERNATIVES SUR LE FOND : Il faut élargir la notion de référendum, oui ou non n'offre qu'une faible sensation de participation du citoyen au débat démocratique. Selon les thèmes abordés, le citoyen pourrait voter en ayant le choix parmi  un pannel de 2 à 5 propositions de solutions mises en place par le gouvernement, assisté par un collège d'expert issus de la société civile. Cela permettrait d'enrichir le débat citoyen au-delà du "tu es pour ou contre ?" en amenant le fond du questionnement directement vers  le "pourquoi as-tu choisi cette solution ?

Score : 1 / Réponses : 0

----

Un comité d'initiative qui proposerait une loi, disposerait de x jours pour récolter xx xxx signatures, si un conseil juge l'initiative recevable, elle est soumise à une votation populaire. 
Le parlement peut proposer un contre projet. 
Les citoyens s'expriment sur les deux, puis expriment leur préférences. (système suisse)

Score : 0 / Réponses : 2

----

Je ne suis pas sur d'être au bon endroit.
Je souhaite qu'il y ait une consultation peut-être par une plate forme numérique concernant le sujet des liaisons entre les citoyens hors des moyennes villes. Je suis d'une génération qui montait dans un trains en mettant son vélo dans le train et je pouvais aller au fin fond des campagnes, via trains, et cars.
Il est urgent de reconstruire un réseau de circulation par moyens de locomotion autres que individuels.
Il y va de la survie des jeunes de la campagne pour rejoindre les centres de formation, des demandeurs d'emplois vers le travail.
C'est à cause de ce manque que les gens ont rejoints les métropoles où ils ne sont pas heureux, sans travail, et loin de leur ressourcement (familles, amis, lieux).

Score : 0 / Réponses : 1

----

Faire appel à des collectifs constitués de spécialistes , d'experts issus de la société civile et de citoyens pour mener une réflexion commune sur des projets de lois . La participation pourrait être volontaire mais aussi faite sur la base du tirage au sort dans chaque circonscription ou d'une incitation à définir . Le principe est de faire remonter les idées de la base citoyenne vers le député . Il s'agit de fournir des diagnostics territoriaux , une compilation des consultations et concertations , utilisables comme outil de travail parlementaire . Chaque synthèse produite pourrait être évaluée par d'autres jurys citoyens ou soumises au vote en ligne. La présentation d'amendements en commissions pourrait en être l'aboutissement.

Score : 0 / Réponses : 2

----

Il faudrait créer une obligation que tous les Français soient sondés via une grande consultation en ligne lorsque le sujet d'une pétition atteint plus de 1 000 000 de signataires. Cette consultation n'imposera pas que la loi soit adoptée, il faudra encore que les députés votent cette loi et qu'elle soit promulguée. On définira alors un pourcentage minimum de participation des Français à cette consultation pour qu'elle soit étudiée par les députés. Naturellement, si la réponse des Français à la question de la consultation est négative, elle ne sera pas étudiée ni votée par les députés.

Score : 0 / Réponses : 0

----

les avancées des technologie de l'information permettent aujourd'hui des avancées dans l'expression d'un démocratie directe, n'est-ce pas la finalité de la démocratie. Cette plateforme montre qu'il existe une prise de conscience des élus en ce sens , soyons modernes !

Score : 0 / Réponses : 0

----

La démocratie participative a plusieurs niveaux, me semble-t-il, tout d'abord communal, intercommunal ou agglomeration, regional et enfin national.
Au niveau communal, intercommunal : lors des conseils municipaux, il existe le 1/4 citoyen. Normalement, il est possible de poser des questions au conseil municipal hors force est de constater que dans la grande majorité des cas ceci n'est pas respecté, la plupart du temps le conseil municipal ne répond simplement pas. Mes propositions sont:
De rendre ce 1/4 citoyen obligatoire et respecté; les questions devant être soumises au plus tard 48H avant le conseil municipal. Ces questions seront visibles ainsi que les réponses sur le site internet de la commune ainsi que publiés sur le journal local à la suite du résumé du conseil municipal.

L'adjoint aux Finances devra chaque trimestre exposer l'état du budget selon les critères définis par la loi soient 6 ratios. En faire l'analyse en public et indiquer l'orientation sur l'année. Ces ratios seront publics sur le site internet de la commune ainsi que publié sur le journal local. Aujourd'hui, je défis tout citoyen de décrire la santé financière de sa commune et ainsi que son avenir.

Score : 0 / Réponses : 2

----

Il faudrait purement et simplement interdire le cumul de mandats dans le temps. Si un politique n'agit plus pour être réélu mais dans l'intérêt général, ce sera alors la fin de la démagogie et la démocratie pourra devenir participative.

Score : 0 / Réponses : 0

----

Au niveau regional: Les grands projets choisis pour le futur ( transport, urbanisation, Santé...) doivent être soumis pour avis consultatif dans le cadre d'un referendum. Avec toutefois un verrou, si 65% des avis est négatif ( à débattre) sur un quorum de participants au referedum ( à définir) alors ce resultat a force de veto sur le projet qui a été Soumis.
Au niveau national: s'inspirer du système suisse qui intègre la voie participative et la voie parlementaire. L'Election Sénatoriale devrait être au suffrage universel pour éviter des accords caché inter-partis. Aujourd'hui, le Sénat ne représente absolument pas ni la Présidentielle ni les législatives. Les maires pourraient presenter leur choix de représentants au Sénat mais l'élection se ferait auprès de la population.

Enfin, la démocratie participative ne peut exister entre autre que par la transparence de gestion, celle ci fait défaut à tous les étages. La Cours des comptes devrait être non pas seulement "un expert comptable" mais bien un tant le directeur Financier. La cours des comptes devrait mettre en place un contrôle de gestion au niveau municipal, intercommunal, regional et national. Ce contrôle de gestion, chaque trimestre, émettrait un avis favorable, neutre ou défavorable, quant à la gestion en cours. Au bout de 3 avis défavorables, un audit serait déclencher sur la gestion avec soumission du rapport au Préfet, ou à Bercy. Ce rapport serait rendu public.

Voici pour le moment, mes 1ères reflexions

Score : 0 / Réponses : 1

----

Dans un monde idéal, un citoyen désireux de proposer une nouvelle loi n'aurait qu'à en parler à son parti politique qui chargerait ses élus de déposer la proposition à l'Assemblée.

En pratique, c'est plus compliqué, parce que l'offre des partis ne rencontre plus la demande des citoyens : ceux-ci sont de plus en plus nombreux à ne plus se sentir correctement représentés.

Pourtant, il suffit de regarder l'effervescence des initiatives citoyennes pour constater que l'intérêt politique est bel et bien là. Ce qui bloque, c'est qu'il est presque impossible de lancer un nouveau parti politique dans notre pays, principalement à cause des règles électorales qui protègent l'oligopole des partis existants. Comme en économie, ouvrir l'Assemblée aux nouveaux partis stimulerait la concurrence et forcerait les acteurs politiques à s'adapter pour mieux répondre aux attentes des citoyens.

Bonne nouvelle, le président Macron a promis l'introduction d'une dose de proportionnelle. J'espère qu'il s'agira d'un système mixte **avec compensation** et **sans seuil**, de manière à permettre l'éclosion de partis politiques innovants tels que #MaVoix.

Score : 0 / Réponses : 0

----

Information, formation citoyenne.
L’Etat a la responsabilité de donner au citoyen les éléments lui permettant de participer, faire des choix.
Cet apprentissage doit commencer dès l’école avec l’instruction civique et se poursuivre dans la vie d’adulte par des publications Internet, voir des émissions pédagogiques dans l’audiovisuel.
La sensibilisation à la participation des citoyens, voire le vote obligatoire avec comptabilisation des bulletins blancs dans les suffrages exprimés,  va dans ce sens. 
De même que l’ouverture des données publiques («Open data) et l'initiative présente.
Il faut mettre en place l'environnement pour que le citoyen se sente concerné.
La démocratie participative est une voie indispensable pour reconquérir la confiance du citoyen dans ses élus et dans ses institutions.
Information, formation citoyenne.

Score : 0 / Réponses : 1

----

la restitution des débats est très rébarbative pour que les citoyens puissent suivre, comprendre et s'investir : il faudrait faire des videos résumant les débats comme sait très bien le faire France Info tv! un peu de moyens certes mais beaucoup pur un public plus large!

Score : 0 / Réponses : 0

----

Il faudrait donner plus de pouvoir de décision au conseil CITOYEN des quartiers populaires et prioritaires de la politique de la ville. 
Décision et signataires des protocoles d'accord de rénovation urbaine. 
ET faire un concours des meilleurs conseil CITOYEN. 
SUR la base de critères  .
Ceux qui ont le mieux réussie pour améliorer leur quartiers en termes de 
: sécurité, délinquance, chômage, amélioration de l'habitat, et de la réussite scolaire. Et redonner une image positive de leurs quartiers. 
Avec une finalité et présentation à l'assemblée nationale devant le président de la république et les députés.

Score : 0 / Réponses : 0

----

Obligation d'étudier une pétition qui atteint 100 000 signatures à l'Assemblée Nationale et au Sénat. Elle pourrait être étudiée soit en commission qui est en rapport avec la pétition ou en session plénière. Il faut également que le président donne plus la parole au peuple grâce au référendum qui devrait être utilisé plus souvent...  Et il faudrait réellement interdire toute personne avec un casier judiciaire non vierge de se présenter aux élections législatives car mes français ont besoins de représentants  exemplaires !  Pour terminer je dirais qu'il faut mettre une sorte de limite d'âge pour être député en plus de la loi sur le non cumul des mandats dans le temps !

Score : 0 / Réponses : 5

----

Mettre en place un référendum d'initiative populaire sur le modèle suisse. Un seuil de 500 000 signatures me semble raisonnable pour que la proposition législative soit prise en compte. Il faut par ailleurs limiter les secteurs d'activités pour lesquels cette initiative sera possible afin d'éviter l'inflation législative.

Score : 0 / Réponses : 2

----

LA COMMISSION DES INITIATIVES CITOYENNES. 
Cette nouvelle commission permanente (qui serait donc la 9ème commission permanente de l'AN) aurait plusieurs missions.
D'abord, réaliser un travail de veille permanent sur l'état de l'opinion citoyenne. Cette commission serait ainsi destinataire de toutes les pétitions de plus de 1000 signatures (ce chiffre est indicatif, l'idée étant de favoriser au maximum l'expression des citoyens). Elle devrait systématiquement etre saisie pour avis sur tous les projets ou les propositions de lois. Elle disposerait de tous les pouvoirs dévolus aux commissions permanentes. Elle pourrait notamment auditionner les auteurs des pétitions. Toutes les propositions ou amendements proposés par la commission ne pourraient etre qu'inspirés par les pétitions recues. La commission serait ainsi la plume des citoyens et traduirait donc dans la procédure législative les sollicitations des citoyens. 

Cette idée s'inspire de la commission des pétitions du Parlement européen. La Belgique dispose aussi d'une commission des pétitions au sein de la Chambre des Représentants. L'AN au Québec a mis sur pied une commission des relations avec les citoyens. 

La France a fait le choix de confier au CESE la resposabilité de filtrer les pétitions. La revalorisation de la place du citoyen impose de réévaluer ce choix et de confier, à l'instar d'autres grandes démocraties, à une commision parlementaire permanente la responsabilité de prendre en compte la parole des citoyens;

Score : 0 / Réponses : 1

----

Bonjour, il faudrait instituer une réelle initiative citoyenne en amont. Qu'un nombre raisonnablement faible de voix - disons l'équivalent du nombre de voix nécessaire à l'élection d'un député au regard des dernières élections- sur l'ensemble du territoire puisse déposer une proposition de loi, qui soit ensuite examinée par le Parlement (Assemblée nationale ou Sénat en première lecture). Ce groupe d'initiative aurait alors autant de légitimité qu'un parlementaire pour proposer un texte. Le Parlement aurait ensuite la légitimité de la représentation nationale (l'élection législative) pour accepter le texte tel quel, l'amender ou le rejeter. Toutefois, pour que cette initiative fonctionne, il faut s'assurer que les propositions ne soient pas juste déposées et enterrées dans le travail des commissions en amont, et qu'elles puissent réellement atteindre l'hémicycle avec, pourquoi pas, un système protecteur de quota.

Score : 0 / Réponses : 0

----

Mieux intégrer les pétitions citoyennes au calendrier des questions au gouvernement de l'AN, par le biais d'une plateforme publique gérée par l'Assemblée nationale et des civic techs dont le but est de sélectionner les plus populaires et de contrôler la transparence du processus. Toutefois, ce procédé nest pas suffisant. Les questions au gouvernement, si elles symbolisent la responsabilité du gouvernement devant l'Assemblée nationale, ne l'engagent pas réellement dans des travaux de mise en oeuvre. Leur vertu première est de forcer le gouvernement à s'expliquer publiquement, ce qui est déjà pas mal dans une perspective démocratique.

Score : 0 / Réponses : 0

----

La communication politique officielle dans les grands média TV et réseaux sociaux doit exister tout au long de l'année et pas seulement en période électorale. Des temps d'antenne réguliers doivent être dédiés à faire un état des initiatives citoyennes lancées afin que la population puisse s'en emparer. Une plateforme numérique doit aussi proposer des abonnements à des newsletters informant sur ces initiatives citoyennes.

Score : 0 / Réponses : 0

----

Au risque d'être redondant (il y a beaucoup d'idées et de propositions à lire), le fait d'avoir à disposition une plateforme comme celle-ci, permettant à la fois une consultation des projets de loi et d'émettre un avis (favorable ou défavorable) serait une excellente chose. A partir d'un nombre déterminé d'avis défavorables, il faudrait considérer que le projet doit être retravaillé voire abandonné (dans le cas d'une très forte proportion d'avis défavorables). Il faudrait également laissé aux citoyens la possibilité de soumettre des propositions d'amendements. Un système de discussion "chat" avec les députés serait également intéressant (on peut prévoir une programmation "type programme TV" indiquant quel député sera en ligne, à quelle date/heure et sur quel sujet) avec un modérateur pour éviter certains écueils. Un MOOC complet déposé sur la plateforme expliquant le fonctionnement global de l'institution et le processus de construction de la loi (cadre réglementaire et règlement intérieur compris) serait un plus. Par pitié, soyez ludiques et pédagogues ! Nous aurions beaucoup a gagner de monter en compétences nos compatriotes dans ce domaine, cela participerait à améliorer notre compréhension et la qualité des propositions. Pour la construction de cette plateforme, constituez un groupe de travail associant des citoyens (vous avez parmi toutes les personnes ayant déposé une idée sur ce site le vivier nécessaire !) c'est très utile pour améliorer les fonctionnalités et l'expérience utilisateur.

Score : 0 / Réponses : 0

----

Dans toutes les mairies de France, il devrait y avoir des urnes pour glisser dedans les propositions citoyennes. Chaque mois, celles-ci sont ramassées par le député de la circonscription afin d'être acheminées vers l'assemblée nationale.

Score : 0 / Réponses : 5

----

Faire des propositions de résolution ou des propositions de loi CITOYENNE: Chaque citoyens doit être apte à faire des propositions de résolution ou des proposition de loi.

Score : 0 / Réponses : 0

----

Ouvrir l'espace de débats parlementaires aux "extérieurs" doit répondre à un but de rapprocher les citoyens de la vie politique et de la vie parlementaire. 

Dans un premier temps, il serait intéressant, lors des séances de "questions au gouvernement", de réserver 2 "questions citoyennes" par exemple. Grâce à une plateforme numérique comme celle-ci, chacun pourrait poser une question (limiter le nombre de mots). Une première sélection pourrait se faire au sein de la plateforme en retenant les cinq questions les plus "populaires" (laisser une journée après le dépôt des questions pour que chacun puisse voter pour la question qu'il préfère). Ces cinq questions pourraient être analysées par les services de l'Assemblée et un nouveau vote citoyen (pendant une journée) nous permettrait de choisir les 2 questions sélectionnées pour être posées au gouvernement. Les citoyens actifs de la plateforme pourraient ainsi poser leur question, choisir les questions posées tout en ayant un contrôle de l'Assemblée Nationale quant à la pertinence des questions.

Autre proposition, la pétition d'initiative citoyenne. Toujours sur cette plateforme citoyenne de l'Assemblée nationale, créer un espace dédié à la création et au développement des pétitions citoyennes répondant soit à une question posée aux parlementaires, soit une demande formulée pour que le Parlement se saisisse d'un sujet (par exemple). Cette plateforme permettrait ainsi, de contrôler le contenu et le sérieux des pétitions proposées mais aussi de vérifier l'identité des signataires (éviter qu'une même personne puisse signer plusieurs fois avec des identifiants différente). Une fois ce cadre de contrôle institué, ces pétitions disposeraient d'un caractère réellement légitime et à partir de 100 000 ou  500 000 signataires (seuil à définir), la pétition devra obligatoirement être étudiée par l'Assemblée Nationale en commission.

Enfin, sur le même modèle que les pétitions citoyennes, créer une ouvrir la possibilité de créer une "proposition de loi citoyenne". Si le vote et l’élaboration de la loi est de la compétence exclusive des parlementaires, les députés sont également nos représentants. Pourquoi ne pas permettre aux citoyens, en mettant à disposition un "guide pour rédiger une proposition de loi", de proposer une loi. Lors des débats portant sur un domaine précis (par exemple, prochainement, la réforme sur l'apprentissage et la formation), serait ouvert un espace ou des propositions de lois citoyennes pourront être déposées. Si le seuil de cosignataires citoyens (à définir) est atteint avant la fin du délais fixé (en relation avec le travail en commission), la proposition de loi citoyenne devra obligatoirement être défendue (par un parlementaire proche de la philosophie de la proposition citoyenne, ayant manifesté sa volonté de défendre ce texte). Un modèle qui pourrait être transposé pour les amendements par exemple.

Enfin, pour le référendum d'initiative populaire, celui-ci ne pourrait être légitime que si le nombre de signataires représente une part non négligeable du corps électoral. Si cette idée est intéressante, pour ma part, le seuil minimal d'1/3 du corps électoral signataire serait judicieux pour déclencher une procédure.

Score : 0 / Réponses : 0

----

LA PETITION: UN DROIT À DÉVELOPPER 
Encore une bonne  tentative d'initiative de démocratie directe. Notre première analyse et réaction est que le champ de cette initiative est trop vaste et trop technique.  Nous aimerions cependant y adhérer et la récupérer pour inciter mes concitoyens à participer. Nous allons nous heurter aux obstacles habituels quand il s'agit de  démocratie participative.  La complexité de cette consultation va décourager les bonnes volontés citoyennes à un moment où une avalanche de textes décourage. La seule lecture des 5 ordonnances sur la réforme du droit du travail atteste de la limite d'une telle consultation.. Par la voie de la pétition nous avons pu faire passer quelques propositions à nos élus. Nous voudrions pouvoir collectivement participer à cette consultation et à la faire vivre. Les membres de l'association citoyenne locale à laquelle nous appartenons  pratiquent cette forme de communication avec les  élus. Mais  il faut pour chaque thème mis en débat poser une question simple, une question qui touche un thème sensible mobilisateur et avoir un suivi et des réponses à proposer en retour aux pétitionnaires. .

Score : 0 / Réponses : 0

----

on pourrait, comme en Suisse, organiser un vote si une proposition par un citoyen(ne) obtient un certain nombre de signataires
En ce qui concerne les pétitions, obligation de la soumettre à l'assemblée à partir d'un certain nombre de signataires (150 000 ?)

Score : 0 / Réponses : 0

----

Propositions de loi citoyennes: Classées par thèmes, qu'il soit possible de proposer une loi en ligne, avec 2 boutons "Pour" et "contre". Si plus de 50% est pour, l'Assemblée Nationale doit analyser la demande. Un nombre minimum de votants sera à définir.

Score : 0 / Réponses : 0

----

Bonjour, l'initiative citoyenne doit pouvoir se développer à tous les niveaux de notre organisation géo-politique (commune, intercommunalité, département, région, état), mais aussi en fonction des bassins de vie et des zones d'impact des projets. 
Cette initiative doit être portée sur un site officiel (CNDP par exemple), et l'identité des auteurs et contributeurs doit être visible et certifiée. Toutes les initiatives doivent être publiques.
Le droit d'initiative peut être ouvert à tout Français jouissant de tous ses droits civiques. Pour éviter la prolifération de sujets, une première phase peut être de recueillir un avis favorable d'une faible proportion d'élus concernés avant l'ouverture de la consultation au public concerné (nécessité d'une modération des droits à contribuer).
L'initiative devrait, dans un premier temps être une phase de recueil et de partage d'avis, tout contributeur étant informé de toute nouvelle contribution. Dans un second temps, l'auteur propose au vote (ouvert à tous les électeurs concernés) un texte d'un niveau d'écriture tel qu'il puisse être soumis à l'assemblée délibérante concernée. Si le texte recueille un avis favorable significatif, il sera obligatoirement débattu au sein de l'assemblée concernée, son auteur étant appelé à défendre son texte.

Score : 0 / Réponses : 0

----

La démocratie diffère selon les pays en fonction des cultures, des coutumes, des usages et de la pratique constitutionnelle et politique. 

Notre Constitution rappelle '' que la souveraineté nationale appartient au peuple qui l'exerce par ses représentants et par la voie du référendum ''.

C'est à dire qu'il faut attendre les élections au suffrage direct et de l'éventualité d'un référendum, faculté du Premier Ministre par décret du Président de la République. 

Aujourd'hui, dans une époque hyper-connectée, la pratique politique et le désir citoyen ne sont plus en phase avec les textes constitutionnels. 

Sans pour autant souhaiter un changement de République, typiquement dans la culture nationale, il serait opportun de réviser l'article 3 de la Constitution et d'ajouter : '' la souveraineté nationale appartient au peuple qui l'exerce par ses représentants, par la voie du référendum ou par votation populaire ''.

La Suisse est un bel exemple de démocratie directe. La votation populaire est récurrente car les Suisses se sont appropriés cet outil d'initiatives populaires. Il mêle les procédures de pétitions californiennes et le référendum français. 

Ainsi pourrait être voté des législations qui intéressent directement le peuple sur son initiative directe. Ce qui ne retire en rien la prérogative de l'exercice du pouvoir législative délégué aux parlementaires qui par cet outil, trouverait l'avantage de ne pas traiter des sujets impopulaires. 

En pratique, la procédure devrait être celle-ci : une pétition réunissant 10% du corps électoral doit être transmise au Conseil Constitutionnel pour subir les validations nécessaires. Celui-ci transmet au Gouvernement la pétition ou l'initiative populaire qui doit mettre en place la votation populaire puis appliquer la décision du souverain qu'est le peuple.

Score : 0 / Réponses : 0

----

On pourrait imaginer, à l'instar de ce qui existe notamment aux États-Unis, la création d'une procédure de recall (cela exige toutefois une révision de la constitution). Cette procédure serait individuelle et non collective. 

Le recall consiste à permettre de révoquer le mandat d'un parlementaire lorsqu'une pétition demandant cette révocation a obtenu suffisamment de signatures. Dans cette hypothèse, le député révoqué à la possibilité de se représenter à nouveau et d'être réélu éventuellement. 

Il faudrait empêcher l'usage abusif du recall pour ne pas créer une instabilité politique chronique, pour cela il faudrait l'interdire avant deux ans de mandat et prévoir un nombre de signatures assez important.

Cela permettrait plusieurs choses : un député qui s'est fait élire sur un programme qu'il ne respecte pas du tout pourrait être ainsi révoqué par ses électeurs. Même si le nombre de signatures n'est pas atteint, la tentative de révocation est déjà un signal pour lui qu'il doit répondre davantage aux attentes de ses électeurs.

De même, lorsqu'un député connaît des affaires ou est impliqué dans un scandale et que le Président de l'Assemblée ne lève pas son immunité, les électeurs aurait alors avec leur pouvoir de révocation la possibilité de l'obliger à se soumettre à la justice 

Il serait donc responsable tout au long de son mandat devant les citoyens de sa circonscription, même si il faut être conscient qu'il ne faut pas installer un mandat impératif non plus...À réfléchir!

Score : 0 / Réponses : 0

----

Mettre en place une plateforme numérique de d'élaboration et de vote des lois accessible à tous les citoyens en âge de voter.

Score : 0 / Réponses : 1

----

Plaidoyer pour l’adoption de questions au Gouvernement citoyenne (initiative à soutenir !)

En disposant que « la souveraineté nationale appartient au peuple qui l'exerce par ses représentants et par la voie du référendum », l’article 3 Constitution a consacré la démocratie représentative. Or, le lien entre le peuple souverain et ses représentants s’est semble-t-il distendu. 
L’organisation de la présente consultation en atteste et invite à penser des modalités d’exercice direct de la souveraineté. Dans ce cadre, il est impératif de définir un cadre au sein duquel le détenteur primaire de la souveraineté (le peuple donc selon les termes de la Constitution) peut interpeller les représentants auxquels il a donné mandat. Ces représentants sont de deux natures : d’un côté l’exécutif, qui reçoit l’onction souveraine au travers l’élection au suffrage universel direct du Président de la République, de l’autre, le Parlement composés de membres élus au suffrage direct ou indirect. 
Le cadre de l’article 3 de la Constitution autorise un exercice plus direct des représentants du pouvoir exécutif par le biais du Parlement. Cet exercice s’exerce déjà par le biais des questions au Gouvernement prévu par l’article 48 de la Constitution. Compte tenu de la défiance qui a pu s’exprimer à l’égard du système représentatif, l’octroi d’une faculté d’interpellation directe aux citoyens est nécessaire : elle prendra la forme de questions aux Gouvernement citoyenne (QAGC). 

1.	Objet de la QAGC 

Par analogie avec le dernier alinéa de l’article 48 de la Constitution, la QAGC portera sur tout sujet sur lequel un citoyen souhait interpeller le Gouvernement. Un encadrement du champ des questions serait illégitime et reviendrait à donner une faculté plus étendu aux parlementaires qu’à leurs mandataires. 

2.	Traitement et recevabilité de la QAGC 

Dès lors qu’il est admis que la question peut traiter de tous les sujets, il importe de déterminer à qui sont adressées la question et sur quels critères est examinée sa recevabilité. 
(i)	Le Parlement est l’intermédiaire naturel entre l’exécutif et le citoyen, aussi il paraît légitime de lui confier le soin de réceptionner la QAGC et d’en examiner la recevabilité avant de l’inscrire à l’ordre du jour de la prochaine séance des questions au Gouvernement ;
(ii)	L’examen de recevabilité se bornera à vérifier l’adéquation entre le thème de la question et le membre du Gouvernement auquel elle s’adresse ainsi que l’absence de propos injurieux ou de menaces

3.	Lecture de la QAGC en séance 

Afin de maintenir une étanchéité parfaite entre les QAG d’initiative parlementaire et les QAG d’initiative citoyenne, il est proposé de faire lire ces dernières exclusivement par le parlementaire assurant la Présidence de séance.

4.	Questions de la récurrence des QAGC 

Le temps de séance consacré aux questions au Gouvernement étant limité, il est complexe de déterminer sans discussion préalable avec les conférences des Présidents des assemblées le nombre de QAGC lisibles au cours d’une séance. 
Le nombre de questions étant actuellement de 15 par séance, il pourrait être imaginé que la majorité et l’opposition rendent un nombre de questions à due proportion de leur poids dans l’hémicycle, dans la limite de 3 questions, le temps ainsi dégagé pouvant être réalloué aux QAGC.

Score : 0 / Réponses : 0

----

Il serait judicieux que le travail du pouvoir législatif ne puisse pas être remis en cause ultérieurement par le pouvoir exécutif par le biais d'amendements ou de décrets (ce qui s'est passé pour la réforme notariat par exemple). Il faudrait au pouvoir législatif une assurance que son travail ne sera pas remis en cause sans son aval.

Score : 0 / Réponses : 0

----

Depuis 2008, la modification de l'article 11 n'a abouti à aucune initiative populaire; il semble urgent d'y remédier. Afin d'éviter tout excès de referendum populiste, il serait préférable que nous, citoyens, apprenions à l'utiliser localement en premier lieu. Les décisions qui concernent la vie des habitants de la cité devraient pouvoir être interrogées régulièrement  (du bio ou pas à la cantine, un nouveau stade, un sens de circulation, etc). Des réunions publiques de quartiers, l'épanouissement de cette vie citoyenne doit et peut être encouragée par les élus, par la loi. Ces mêmes citoyens pourront bien entendu proposer eux-même des initiatives. Le quorum pour les faire devenir referendum dépendra bien entendu de la taille de la commune. 
Il serait dommage de réserver les initiatives à quelques sujets nationaux, certes importants. Dans les pays qui, comme la Suisse, l'ont mis en application au niveau local puis national nous constatons un sens civique plus important que chez nous.

Score : 0 / Réponses : 0

----

Il faudrait inscrire dans la constitution l'obligation de tenir un referendum annuel (ou alors simultané avec chaque scrutin national, pour réduire la facture) sur la question qui aura eu le plus de soutien populaire (pétition sur ce portail par exemple).

Score : 0 / Réponses : 0

----

La justice devrait être plus transparente et plus démocratique on a le sentiment d'un truc opaque et laxiste : où est le peuple là-dedans ?

Score : 0 / Réponses : 1

----

Les initiatives citoyennes devraient être soutenues par un service public dédié, qui force les élus à expliciter les choix et décisions prises au nom du peuple. Les élus parlementaires ou territoriaux seraient tous concernés et confrontés non pas seulement à leurs pairs et à leur parti politique, mais à tous les citoyen-ne-s.
Les citoyen-ne-s seraient tous invités à participer à des assemblées locales, en proximité de leur lieux de vie, pour être informés des décisions publiques et questionner les fonctionnaires indépendamment des élus de l’exécutif.

Score : 0 / Réponses : 0

----

LA PARTICIPATION CITOYENNE à la vie publique et à la la vie Législative suppose une information en amont des débats sur les thèmes et une pédagogie favorisant la compréhension des enjeux et la définition du cadre tant en terme d'ouverture que de limites.
Une deuxième étape doit permettre à tout citoyen le souhaitant une appropriation faisant apparaître les possibilités et les contraintes précises du domaine objet du débat.
L'étape suivante doit permettre l'intervention et la proposition sous forme soit de contribution soit d'amendement par exemple. Elle nécessite à ce stade que les relais soit institutionnels : les Elus de la République puissent être saisis et soient ainsi les relais citoyens soit politiques à travers la vie des mouvements politiques, citoyens, associatifs ou encore syndicaux .

Score : 0 / Réponses : 0

----

Les députés représentent les citoyens et de fait posent des questions que l'on peut qualifier de citoyenne ! CEPENDANT...  Proposition faite à contre-coeur mais qui semble s'imposer : il faut SUPPRIMER les questions d'actualité au gouvernement ! En effet, constatant ce qu'est de fait cette séquence avec tous ses effets induits, il paraitrait bienvenu de la supprimer. La supprimer dans un premier temps pour, peut-être, dans un second temps trouver une nouvelle manière de faire !

Score : 0 / Réponses : 0

----

Faire de la pédagogie dans la mise en place d'une loi comme cela est en train d'être réalisé sur le logement par le secrétaire d'État Julien Denormandie qui a présenté sa stratégie du logement avec quelles propositions qui sont testés via une consultation nationale. Avant d'utiliser la restitution pour rédiger le projet de loi sur le logement.

Score : 0 / Réponses : 0

----

Je viens d'arriver sur ce site et après un survol rapide, je m'aperçois que l'idée d'une consultation des citoyens suscite toujours autant de commentaires critiques ou élogieux, mais il me semble qu'on n'aborde jamais la condition fondamentale qui est celle-ci : une vraie consultation citoyenne doit s'adresser à l'ENSEMBLE des citoyens. Tant que cette condition n'est pas remplie, toutes les formes de démocratie participatives ne concerneront que les initiés qui continueront de discuter entre eux comme on le fait ici et comme ça s'est toujours fait. Alors toute une fraction de la population continuera de se sentir exclue, s'abstiendra ou votera pour les extrêmes. C'est avec cette idée que je m'efforce depuis plusieurs années de faire connaître un projet de consultation régulière des citoyens, l'OCC,  que j'ai défendu dans des livres, dans un blog et grâce à une pétition qui en résume l'essentiel. Pardon si je parle ici de ces projets très personnels, mais il me semble qu'ils sont au cœur du sujet qui nous préoccupe. C'est pour cette raison que je me permets de vous inviter à consulter le texte de cette pétition et les pages de mon blog destinées à défendre ce projet :

http://www.mesopinions.com/petition/politique/consultation-reguliere-citoyens/4865

http://pouvoir-citoyen.centerblog.net/134-la-solution-pour-aujourhui-et-pour-demain

http://pouvoir-citoyen.centerblog.net/133-des-idees-pour-agir

Score : 0 / Réponses : 4

----

«  Ma circonscription ! »
L’évolution de la société et les progrès de la démocratie se font par des textes de loi, mais aussi par la modification des comportements de chacun.
Les parlementaires seraient bien avisés d’oublier certaines formulations qui appartiennent à un autre âge. Il est pesant de les entendre parler de « ma circonscription » ou de « mon département »…. Le possessif sied mal à la notion de représentation. 
« La circonscription et le département dans lesquels je suis élu » conviendrait mieux au non cumul et au non renouvellement des mandats…

Score : 0 / Réponses : 5

----

Pour l’interdiction aux détenteurs d'un casier judiciaire (B2, B3) d'effectuer un mandat électoral.

Score : 0 / Réponses : 0

----

Je suis totalement favorable à l'idée d'une consultation citoyenne automatisée qui remettrait ainsi la parole du citoyen au coeur de la décision publique. Néanmoins je m'interroge beaucoup sur les modalités. On parle énormément de l'utilisation des outils numériques mais il faut prendre en compte le fait que seule une partie de la population est réellement à l'aise avec ces outils ou y à accès. Il serait donc dommage d'exclure d'emblée une partie de la population comme les personnes âgées ou les personnes n'ayant pas d'ordinateurs personnels. Et pourtant, il s'agit pour moi de la meilleure façon d'améliorer la participation citoyenne étant donné que pour les autres propositions avancées comme les propositions de lois citoyennes ou encore les questions au gouvernement citoyennes, cela nécessite un travail de recherche et d'information conséquent de la part du citoyen ainsi qu'éventuellement son déplacement pour les discussions en amont ... Ces mesures sont difficilement conciliables avec la vie professionnelle de la plupart des Français. Ne faudrait-il pas alors s'interroger autant sur l'idéologie de la démocratie participative que sa faisabilité pragmatique ?

Score : 0 / Réponses : 0

----

La mise en place de referendum citoyen et donc du vote me semble être l'étape ultime de la démocratie. Comment imaginer qu'un vote sur un sujet complexe parfois bien méconnu par des citoyens puisse être le gage d'une bonne décision prise ? La phase de débat, d'échanges de position fondés sur des éléments factuels, entre sachants, facilitateurs et novices me semble incontournable pour véritablement parler de démocratie. Etape, certes chronophage mais imparable sur la réussite de la mise en œuvre de la décision. Des résultats de mise en œuvre à évaluer et à diffuser largement... PLACE AUX CONSEILS CITOYENS ECLAIRES !

Score : 0 / Réponses : 1

----

QUESTIONS ECRITES  AU GOUVERNEMENT

Certaines questions récurrentes qui dépendent de plusieurs acteurs(commissions,syndicats,associations ….) nécessitent une étude approfondie qui peut prendre du temps. Je propose qu’au delà de 3 mois ,les membres du gouvernement se doivent par le biais des députés et sénateurs d’informer le ou les émetteurs de l’état d’avancement de la question,des actions entreprises et s’engagent sur un délai objectif de fin de traitement. il est important avant tout d’avoir une réponse et que celle ci soit claire et argumentée. Cette exigence d’information permettra de rapprocher les citoyens de leurs représentants et améliorera le coté participatif de la démocratie

Score : 0 / Réponses : 0

----

1) Organiser des pétitions pour transmettre les propositions et les préoccupations des citoyens avec obligation de répondre pour les députés. 
2) Pouvoir interagir, interroger, éclairer les députés lors des commissions par le biais d'interventions d'experts et de citoyens.
3) Le député s'entour d'un groupe relais de citoyens qui soit en dehors d'une activité partisane.
4) Avoir accès à une base de données type "MOOC" pour expliquer les lois avec précision.
5) Ecrire une charte du citoyen qui précise les droits et devoirs d'engagement et de participation à la vie démocratique.

Score : 0 / Réponses : 0

----

Le droit de pétition actuel n'est pas moderne (500 000 signatures papier !) et peu contraignant. A l'opposé, le référendum d'initiative partagée est très engageant mais très lourd. Sur ces deux axes, on peut imaginer des voies médianes permettant notamment de moderniser le droit d'interpellation des parlementaires. Sur le mode d'interpellation, par signature électronique (ce serait l'occasion de mettre en oeuvre cette procédure qui existe dans des pays Européens bien plus avancés comme l'Estonie), avec un mécanisme de filtre pour contrôler la validité des interpellations (signataires, constitutionnalité, etc..). Et sur la nature de l'interpellation, on peut imaginer un memorandum, qui ne serait pas une simple question fermée soumise à référendum, mais un ensemble de mesures sous une forme simplifiée, qui pourrait faire lui-même l'objet d'une co-construction par les signataires.

Score : 0 / Réponses : 0

----

Au fil des élections on ne peut que constater et déplorer le manque d’intérêt de nos concitoyens pour la politique, qui se traduit par un taux d'abstention élevé. Il apparaît donc important de remettre le citoyen au centre du projet politique.
Dans ce contexte et face à cet objectif, il me semble judicieux de développer de nouvelles formes de participation, notamment le droit à l'initiative populaire.
L'utilisation du numérique pourrait permettre cette implication citoyenne, même si, la couverture du territoire limite l'accès de certains, notamment dans les zones rurales. Le plan gouvernemental dans ce domaine est capital, mais nécessitera plusieurs années pour améliorer l'accès à internet.
Le référendum existe déjà et s'avère délicat à utiliser par le pouvoir politique, notamment du fait que les électeurs ne répondent pas nécessairement à la question posée, mais en font un outil de soutien ou pas à la politique gouvernementale.
Le référendum populaire peut être également envisagé. Il ne doit pas se substituer au référendum existant et nécessite des modalités de mise en oeuvre très encadrées : Nombre de demandes citoyennes pour être validées, type de sujet possibles, réception des demandes (numériques, écrites, pétitions citoyennes,..).
La France est une démocratie où le peuple procède à l'élection de ses représentants. Les Députés nous représentent, ils ont obtenu un mandat du peuple et il me semble capital qu'aucune nouvelle forme de consultation ne puisse remettre en cause ce principe démocratique.
Si rédiger une proposition de loi me semble trop technique, les citoyens doivent pouvoir proposer des idées, des sujets pouvant faire l'objet d'une proposition de loi.
De même il ne me semble que, dans le cadre du travail parlementaire, des questions au gouvernement puissent être posées, directement ou par le biais du parlementaire élu qui en serait le porte parole.
Tout cela nécessite la mise en oeuvre de plateformes dédiées, consultables en ligne, ouvertes à tous pour le dépôt de ce type de contribution.

Score : 0 / Réponses : 0

----

Bien sûr ok pour le RIC avec une facilité accrue, la modification de l'article conservant les "représentants" est ennuyeuse mais la démocratie réelle ne s'est pas faite en un jour... 😉

Score : 0 / Réponses : 0

----

Je plaide pour une assemblée citoyenne où le citoyen doit être au coeur de l'élaboration de la loi.
Pour y parvenir, plusieurs solutions sont possibles : les pétitions en ligne ( on ne pourra pas toucher tous les citoyens ), le tirage au sort des citoyens comme c'est le cas dans la justice et enfin le sondage pour savoir si une loi est favorable à la population ou pas.
Tout de même l'outil numérique reste incontournable.

Score : 0 / Réponses : 0

----

budget participatif avec élaboration citoyenne dans lésbica commissions

Score : 0 / Réponses : 0

----

Demander un casier judiciaire vierge pour toute personne souhaitant solliciter un mandat électif, tout comme cette prérogative est exigée dans de nombreux corps de métiers.

Score : 0 / Réponses : 0

----

Proposition collective des comités strasbourgeois En Marche ! :
Constat : Il faut agir, car le pays est « en panne », il faut infléchir le processus politique actuel. Il faut rechercher un ascendant dans le processus de fabrication de la loi. Comment passer d’une situation bloquée à une situation citoyenne ? Les lois se succèdent à vitesse élevée : difficile de suivre pour le citoyen. Trop souvent on vote par défaut, il faut un vote par conviction.
Propositions :
1. Une navette citoyenne entre député et citoyen ?
- Organiser des réunions publiques avec annonces dans la presse, réunions à thème, avec ateliers.
- Rechercher un contact régulier pour rendre compte de ce qui est fait et expliquer ce qui est en cours. La présence du député n’est pas nécessaire, les équipes parlementaires peuvent aussi les organiser. Exemple réussi : la consultation numérique sur le logement, suivie d’une synthèse sur des restitutions sur Youtube.
2. L’initiative de la loi, comment mettre les citoyens au coeur du processus législatif ?
Comment mettre les citoyens au coeur de la législation qui les concerne ? Possibilité de déclencher un référendum d’initiative citoyen sur un sujet, sans passer par les parlementaires de l’assemblée ? Les citoyens ont souvent un temps d’avance sur la classe politique (exemple de l’écologie). Le député
accompagnerait, encadrerait, assisterait, les personnes de sa circonscription souhaitant le référendum… Il aurait un rôle « facilitateur ». Cela permettrait de rapprocher la loi du citoyen et de mieux centrer le travail du député sur l’élaboration de la loi mais en écriture collaborative avec les citoyens. Le cadrage juridique est cependant à travailler pour éviter les dérives.
3. Une initiative citoyenne en circonscription ?
Permettre aux citoyens de saisir le parlement sur un sujet à prendre en considération : saisine du parlement. Le député aurait pour mission de démocratiser l’initiative citoyenne en facilitant l’émergence d’une saisine de qualité. Accompagnement des citoyens pour rendre l’initiative citoyenne
efficiente.

Score : 0 / Réponses : 0

----

Attention au danger des groupes d’interets en capacité de mobiliser des citoyens afin de mettre en débat une question spécifique qui ne serait ni d’interet national ,ni une priorité du moment. Le peuple ne participe que très partiellement aux élections, pourquoi participerait-il aux référendum divers et variés ? Les lois sont faites par les députés sur la base de programmes de gouvernement. Alors plutôt que de rajouter un moyen démocratique  essayons de faire en sorte que le peuple comprenne et utilise les moyens existants. Le vote obligatoire serait une première mesure à mettre en place.

Score : 0 / Réponses : 0

----

Pour un referendum d'initiative citoyenne digne de ce nom ( et non pas cette caricature inscrite dans la loi depuis 2008 mais qui est à la fois infaisable et non contraignante pour le Parlement car il peut y répondre par une une simple discussion sur le sujet).
Le champ d'application de ce type de  referendum doit pouvoir couvrir tout sujet.

Score : 0 / Réponses : 0

----

Ne serait-il pas temps, dans le cadre de la prétendue "moralisation" de la vie publique, de mettre à plat tous les innombrables privilèges que se sont octroyés nos parlementaires au fil de leurs mandats successifs et ceci quelle que soit la majorité en place ? On peut concevoir quelques avantages liés à la fonction mais il est indispensable de fixer des limites notamment au regard de la situation budgétaire du pays. D'énormes économies sont à réaliser tout en conservant aux élus une rémunération décente. On ne peut pas demander aux citoyens de se serrer encore un peu plus la ceinture pendant que ceux qui sont censés les représenter votent une augmentation importante de leurs propres indemnités, ce qui a été le cas il y a quelques années. 
Il serait également fort utile de s'interroger sérieusement sur l'utilité de l'existence du CESE et de ses satellites, à part accueillir des élus ou autres membres du gouvernement "recalés" et qui attendent des jours meilleurs dans des conditions confortables aux frais du contribuable.
Les sources d'économies de manquent pas, il suffit d'une volonté politique.
Bernard Ravine

Score : 0 / Réponses : 0

----

Et si au lieu de participer à l'inflation législative, les citoyens occupaient un rôle de premier rang dans le travail de dépoussiérage, de simplification et d'amélioration des TRÈS nombreux textes de loi. Les citoyens pourraient s'emparer des dispositions en vigueur depuis plus de 20 ans et proposer une réécriture ou - à tout le moins - attirer l'attention du législateur sur les paragraphes à réécrire. Avec une obligation faite au législateur; réduire la masse des textes.

Score : 0 / Réponses : 0

----

Bonjour,
La mise en place de consultation populaire semble porter dans les pays qui la pratique, une plus forte implication du peuple dans sa propre gouvernance et dans l'élaboration des règles qui l'organisent. 
Plusieurs intérêts de mon point de vue.
Il est difficile aux porteurs ou promoteurs d'un projet de ne pas adhérer à ce dernier. 
Donner la possibilité aux citoyens de construire le cadre de leurs vies, c'est leur donner la possibilité d'expérimenter ce que revêt l'idée d'emporter l'adhésion (valoriser le rôle de la représentation à l'Assemblée ou au Sénat) . 
Ne plus  cantonner ces mêmes citoyens à un rôle de spectateurs (électeurs) mais un rôle moteur. 
Contribuer à créer de la cohésion entre le monde politique et la population, Apprendre à échanger pour apprendre à connaitre les points de vue de l'autre dans une idée de construction.  

Enfin, il sera difficile de dire ou faire dire que la parole n'appartient au peuple si ce dernier ne se bouge pas  et ne porte pas les idées qui naissent en son sein.

Score : 0 / Réponses : 0

----

Proposition de loi citoyennes et questions au gouvernement citoyennes me semblent à mettre en œuvre de façon prioritaire. Ce ne doit pas être que les députés et le gouvernement qui puissent le faire.  Ceci peut redonner  un intérêt aux citoyens si la parole leur ait donné.

Score : 0 / Réponses : 0

----

une transparence accru sur l influence des lobbys industriels. Des noms leurs propositions   leurs contacts a l assemblée  les demandes qu ils font..
exemple le gouvernement durci le contrôle technique pour les voitures.  Pour moi c'est sur l'influence des lobbys automobiles  pour vendre plus de voitures  car l'on sait très bien que les accidents mortelles sur les routes sont le fait de la vitesse et des jeunes conducteurs et pas de l'état des voitures. en faisant cela on crée de l'obsolescence programmé pour les voitures
je site cet exemple mais il y en a des tas d'autre et j ai de plus en plus  l impression d’être gouverné par des lobbys  qui tirent la ficelle des députés. Donc je veux une plus grande transparence de l assemblée sur ce point.

Score : 0 / Réponses : 0

----

Nous votons pour ceux qui dirigent notre pays il est normal et indiscutable que ceux là même entendent la voix des citoyens et agissent enfin pour nos droits et libertés...en cessant de les bafouer !

Score : 0 / Réponses : 0

----

Pour faire une nouvelle assemblée au senat et a l assemblée. Les présidents de régions deviennent les sénateurs. Les députés sont élus ont parmis les presidents des communautés de communes

Score : 0 / Réponses : 0

----

Instaurer le droit de révoquer ses élus : si 10% du corps électoral le demande, un référendum est organisé afin de décider si oui ou non l'élu concerné poursuit son mandat. Pour éviter que cet outil de contrôle soit détourner, on peut imaginer qu'un référendum ne pourra être convoqué qu'à partir de la mi-mandat, ou une fois par demi-mandat maximum, etc.

Score : 0 / Réponses : 0

----

En plein accord avec l article 3

Score : 0 / Réponses : 0

----

très important la démocratie avec l'écoute de personnes peu importe sa religion ou sa culture

Score : 0 / Réponses : 0

----

en espérant que les cela fasse avancer la démocratie dans le bons sens

Score : 0 / Réponses : 0

----

Nous sommes en 2017 au 21eme siècle et on essaie encore d'y croire. L'histoire se répète car c'est le Marquis de Condorcet qui serais l’inventeur du référendum dit d’initiative populaire, proposé la première fois dans son Plan de Constitution présenté à la Convention nationale les 15 et 16 février 1793 et dont la proposition s'intitulait « De la Censure du Peuple sur les Actes de la Représentation Nationale, et du Droit de Pétition » . Ce mathématicien, statisticien, philosophe et révolutionnaire ne veut pas laisser le pouvoir dans les seules mains d’une élite représentative. Voilà quelqu'un qui ne se faisait pas d'illusions sur la probité des élites dirigeantes. Allez on y croit et on fait tout pour que cela fonctionne.

Score : 0 / Réponses : 0

----

Prendre en compte les pétitions à partir d'un seuil raisonnable pour ne pas paralyser le travail parlementaire

Score : 0 / Réponses : 0

----

Peut-être un chemin vers un peu plus de démocratie.

Score : 0 / Réponses : 0

----

Personnellement, je ne crois plus à cette notion complètement dévoyée de démocratie. Les gouvernements se suivent et se moquent toujours aussi ouvertement des citoyens. Il suffit de voir comment sont traités les Français en difficulté à l'étranger, notamment dans des pays prétendument amis devant lesquels la France se prosterne de manière indécente, leur offrant des conditions fiscales iniques pour les Français qui payent le prix fort, donnant la légion d'honneur à des gens n'ayant jamais levé le petit doigt pour notre pays. Et il ne s'agit que des affaires étrangères...  Il en reste à dire sur ce qui se passe chez nous.
Toutefois, je soutiens ce mouvement qui ne peut aller que dans le bon sens. S'il existe une chance de réinstaurer la démocratie dans notre pays, je la soutiens sans réserve.
Je regrette toutefois que Change.org soit une entité totalement déshumanisée qu'il est tout à fait impossible de contacter pour demander de l'aide même dans des cas critiques allant jusqu'à des questions de vie ou de mort.

Score : 0 / Réponses : 0

----

Remettre le peuple au cœur de l'action politique confisquée par les appareils

Score : 0 / Réponses : 0

----

évident que en Démocratie que l'on soit a l'écoute des doléances du PEUPLE . Que les pétitions soit donc écouté et analyser et suivi d'effet , il en es de bon sens et de démocratie ! . J'en profite pour faire une parenthése vous rappeler le principe de Liberté  qui est/était BASE de notre Pays  Alors laissez la Liberté aux Maman le CHOIx d'injecté OU NON les 11 substances que vous voulez OBLIgé de FORCE (cela est contraire a nos lois !)  Beaucoup pense et je pense qu'il y aura de terrible et gravissime effet secondaire dans le futur / Alors Liberté l'Enfant appartient a la MAMAN et pas a la France ! Merci !!!!!!!

Score : 0 / Réponses : 1

----

Enfin un moyen pour le citoyen de saisir l'opportunité de créer la loi sans courir après un mandat de député ou sénateur !

Score : 0 / Réponses : 0

----

Très belle initiative, encore faut-il que ce ne soit pas une démarche démagogique et que ces pétitions soient RÉELLEMENT prises en compte!?

Score : 0 / Réponses : 0

----

Bonsoir oui ça serait un plus de pouvoir changer et améliorer cette démocratie

Score : 0 / Réponses : 0

----

on va finir par arriver enfin à une démocratie directe

Score : 0 / Réponses : 0

----

Belle initiative pour faire vivre la démocratie populaire, c'est tout simplement du bon sens.

Score : 0 / Réponses : 0

----

Adérrez en masse, car c'est un bon moyen de ce faire entendre, car nos politiques ne nous écoute plus.

Score : 0 / Réponses : 0

----

Si il existe autant de pétitions c'est que les choses ne vont pas et il faut que nous en tant qu'acteurs économiques et sociaux nous faisons entendre notre voix via les pétitions en ligne par exemple. Nous représentons la France et devons être écoutés.

Score : 0 / Réponses : 0

----

Je trouve que c'est un très bon moyen pour que l'assemblée s'intéresse un peu aux désidérata des citoyens  au lieu de ne s'occuper que des projets du Président.

Score : 0 / Réponses : 0

----

La "Vox populi" doit rester la "Vox Dei" et le fondement de toute Démocratie (Demo Cratos = pouvoir du Peuple), et ce en permanence.

Score : 0 / Réponses : 0

----

De très bonnes initiatives et propositions peuvent être proposée via Internet tel que ce site. Je doute simplement que nos élus aient la volonté de lire ce qu'ils n'écoutent pas quand ils entendent le grondement de citoyens découragés par leur surdité. Le dépouillement des avis déposés ne tombera-t-il pas encore une fois dans les mains d'une obscure commission ?

Score : 0 / Réponses : 0

----

Il faut que nos pétitions aient un sens et soient " entendues " !

Score : 0 / Réponses : 0

----

La France a beaucoup de retard en ce qui concerne la prise en compte de l'opinion directe des citoyens. Si certaines pétitions sont irréalistes voire contraires à la législation pour quelques unes, beaucoup sont l'expression des citoyennes et citoyens lambda qui seraient enfin heureux d'être entendus et écoutés par les élus dont le rôle est de les représenter. 
Je suis vraiment très favorable à cette initiative.

Score : 0 / Réponses : 0

----

Aujourd'hui les citoyens s'expriment sur les réseaux sociaux. Notre démocratie doit s'adapter pour éviter que les  citoyens et leurs élus ne vivent dans 2 mondes séparés. Si les consultations électorales semblent montrer que les citoyens se désintéressent de plus en plus de la politique, ce n'est qu'une apparence : ils ont juste besoin d'être entendus et de pouvoir participer dans un dialogue avec ceux qui sont sensés les représenter.

Score : 0 / Réponses : 0

----

Agissons au lieu de subir

Score : 0 / Réponses : 0

----

Un outil de plus pour faire valoir son opinion.

Score : 0 / Réponses : 0

----

Même si on sais que dans bien des cas les lobbies font la loi, on peut toujours rêver pour les sujets qui ne concernent pas leurs intérêts!

Score : 0 / Réponses : 0

----

Belle initiative mais on aurait aimé en être informé plus tôt, je ne l'ai su qu'aujourd'hui, 2 jours avant la clôture !

Score : 0 / Réponses : 0

----

Je n'y crois pas trop....mais j'espère.

Score : 0 / Réponses : 0

----

Qui va s'assurer de l'authenticité des résultats de la pétition???

Score : 0 / Réponses : 0

----

Si la contrainte républicaine du vote du second tour nous pousse à voter pour un Président par forcément représentatif de la pensée générale, il serait bon de contrebalancer notre vote contraint par un système de pétitions pour mettre aux débats et/ou aux votes d'autres préoccupations que la majorité aurait mésestimées.

Score : 0 / Réponses : 0

----

Les modalités sont à préciser, mais il faut absolument mettre en place quelque chose qui contraigne l'Assemblée nationale à prendre en compte les préoccupations des citoyens, et à délibérer sur des propositions en provenance de la société, dès qu'elle atteignent un certain niveau de soutien.

Score : 0 / Réponses : 0

----

Je pense que le ministère concerné par le sujet, étudie le sujet de la pétition et tenir au courant le lanceur  de la possibilité de l'adaptér au réalité sans la vider de sasubstance

Score : 0 / Réponses : 0

----

Excellente initiative

Score : 0 / Réponses : 0

----

Comment ne pas être d'accord ? C'est l'axe principal de ma proposition publiée dès avril 2016...

http://yves.carl.pagesperso-orange.fr/CPIC/Principc-1.html

Score : 0 / Réponses : 0

----

Bonne initiative...Merci !

Score : 0 / Réponses : 0

----

J'aimerais pouvoir participer aux décisions qui impactent ma vie de tout les jours, mon travail, ma famille, ma santé, mon argent!...

Score : 0 / Réponses : 0

----

Le démocratie doit être renforcée à travers une meilleure prise en compte de l'expression du peuple

Score : 0 / Réponses : 0

----

soyons acteur de notre vie !!!

Score : 0 / Réponses : 0

----

La véritable démocratie est participative, non représentative.
Le pas proposé va dans ce sens. 
C'est une orientation favorable. 
Nombre de citoyens y sont prêts.
Le serez-vous ?

Score : 0 / Réponses : 0

----

OK, il faut moderniser...

Score : 0 / Réponses : 0

----

Soyons enfin vraiment citoyens !

Score : 0 / Réponses : 0

----

La démocratie a été vidée de sa substance par les élus de la République ces 50 dernières années. Il faut la restaurer.

Score : 0 / Réponses : 0

----

Tout cela pourrait encourager beaucoup de personnes qui ne s'intéressent vraiment à la vie du pays qu'à l'occasion d'élections, à participer tout le temps.
Quant au fait que cela vienne de change.org, je ne sais quoi en penser à l'instant mais je vais investiguer sur ce mouvement.
Saluts à toutes et tous.

Score : 0 / Réponses : 0

----

L'étonnant est que, dans un pays dit démocratique, il faille faire ce genre de démarche pour que les citoyens soient (peut-être) écoutés ... quant à être entendus, pour le moment rêvons encore : la caste des "mandatés" n'a pas encore fini de tenir les citoyens en lisière des décisions qui les concernent !

Score : 0 / Réponses : 0

----

je pense que c'est un bon projet

Score : 0 / Réponses : 0

----

Pour préciser mon commentaire, les élus ont fait de la République une pompe à fric en s’attribuant des avantages indécents et en votant des lois pour échapper à toute poursuite. Ils ont créé une carapace de protection anti- démocratique avec les lois électorales fermant la porte aux citoyens et donnant un droit de parole aux seuls partis politiques. Ceux ci se sont arrangés entre eux sur notre dos pour faire fonctionner au mieux la pompe à fric.

Score : 0 / Réponses : 0

----

Je souhaiterais que les citoyens puissent voter et proposer : 
1) l'ordre des priorités de l'agenda du gouvernement. 
2) des textes, des réformes en temps réel
Pour cela, il faudrait faciliter l'accès aux informations politiques et, que des citoyens volontaires puissent représenter le peuple à 95% des voix, au sein de l'assemblée nationale.

Score : 0 / Réponses : 0

----

Il faut en effet que le citoyen dont on se souci de son existence uniquement au moment des élections puisse se faire entendre par ceux qui nous dirigent. C'est la volonté du peuple qui les a porté là où ils sont et ils ont le devoir de nous représenter.

Score : 0 / Réponses : 0

----

je suis ok

Score : 0 / Réponses : 0

----

On a encore jamais vu en France les citoyens avoir un quelconque pouvoir sur les lois une fois les gouvernements élus. Mais on peut rêver (de démocratie ....),
c'est pourquoi j'ai signé, l'avenir dira si cela aura était utile ?
Bien à tous

Score : 0 / Réponses : 0

----

Article 3 ...participation citoyenne ..

Score : 0 / Réponses : 0

----

Le référendum ayant été bâillonné depuis des lustres, les sondages faisant dire tout et son contraire au peuple, il est grand temps de lui rendre la parole libre afin que les décisions de nos institutions soit le véritable reflet de la volonté majoritaire du peuple français et non celles de groupe de pression et/ou d'argent minoritaire.

Donc je soutiens sans réserve cette initiative constructive que j'aurais pu proposer moi-même. Il est bien que les bonnes idées soient partagées par tous.

je pense même que selon un rythme qui reste à définir, la pétition la plus populaire soit systématiquement examiné et transformé en projet de loi soumis au vote lorsqu'elle est recevable et conforme à notre constitution.

Score : 0 / Réponses : 0

----

Tout à fait d'accord.

Score : 0 / Réponses : 0

----

Tres bonne initiative.

Score : 0 / Réponses : 0

----

En espérant que votre initiative aboutira à un vrai retour à la démocratie, en renforçant la participation citoyenne aux décisions.

Score : 0 / Réponses : 0

----

Veiller toutefois à ce que l'usage d'un tel nouveau droit ne soit pas pollué par des pétitions  de rébellion stérile ou de suggestions démagogiques ; assortir toute pétition de propositions, utiliser des outils exclusivement publics et des structures de modération sur le modèle de la com.nationale du débat public.

Score : 0 / Réponses : 0

----

Rousseau a montré il y a 245 ans que la démocratie exigeait que les lois soient conformes à la volonté générale.

Internet permet enfin d'exprimer cette volonté.

Il suffit de réfléchir un peu pour le comprendre et d'avoir quelques connaissances de base en informatique pour réaliser un outil adéquat. 

Par contre, les pétitions n'expriment pas la volonté générale, mais une opinion moyenne. 

Il faut donc reconstruire les outils informatiques mis en place par l'Assemblée Nationale, dont celui-ci, pour accéder enfin à la démocratie.

Score : 0 / Réponses : 0

----

Il y aura ainsi peut-être un espoir de passer outre la force des lobbies pour que la Raison et le Bien commun soient les moteurs des changements

Score : 0 / Réponses : 0

----

enfin un moyen de s'exprimer, espèront seulement que cela ne soit pas qu'un leurre histoire de nous faire croire que nous pouvons devenir acteurs de la vie politique.

Score : 0 / Réponses : 0

----

Le citoyen a un droit d’opinion car il est concerné directement et il doit donner son avis pour établir un équilibre.

Score : 0 / Réponses : 0

----

puisque change.org possède le code postal des pétitionnaires, ne faudrait-il pas faire porter la pétition non seulement par l'initiateur mais encore par les députés du code postal (regroupés si besoin) qui a eu le plus de pétitionnaires ? Ne sont-ils pas là pour cela ?

Score : 0 / Réponses : 0

----

Un tout petit pas vers la démocratie participative.

Score : 0 / Réponses : 0

----

Je  soutiens la proposition de change.org

Score : 0 / Réponses : 0

----

Oui à une vie politique active.

Score : 0 / Réponses : 0

----

excellente idée; Un espoir pour la démocratie

Score : 0 / Réponses : 0

----

Faisons entrer les bons aspects d'Internet dans notre démocratie !

Score : 0 / Réponses : 0

----

La Démocratie ne s'arrête pas à une date d'élection quelle qu'elle soit. La véritable Démocratie, c'est tous les jours qu'elle agit et ce sont des Citoyens qui peuvent agir réellement et non pas donner un simple avis consultatif. Cette action de prise en compte des pétitions faites par des Citoyens hors des organisations dites institutionnelles serait une réelle avancée pour la Démocratie.

Score : 0 / Réponses : 0

----

Indispensable pour améliorer la démocratie participative

Score : 0 / Réponses : 0

----

Il serait temps que la voix du peuple soit entendue par nos députés , donc je trouve que c'est une très bonne initiative.

Score : 0 / Réponses : 0

----

Question idéologie, j'aimerais qu'on en arrive un jour à une vraie Démocratie (c'est à dire que là on est en République : système Démocratique très indirecte ou nous élisons quelques personnes qui décident ensuite, et que la Démocratie ça veux dire que toute la population gouverne ensemble sans qu'il y ait un ou des grands chefs au-dessus)
Ainsi, je pense que nous devons lancer cinq reformes :
* Augmentation du poids et de la fréquence des référendums (au moins consultatif, au mieux décisif) surtout sur les lois qui ont une grande importance, qui touchent toute la société. Ainsi que le fait que le referendum puisse être d'initiative populaire
* Prise en compte en assemblée de toute pétition dépassant un certain nombre de signatures (100 000 par exemple)
* Présence d'intervenants extérieurs, directement concerné par l'ordre du jour de l'assemblé (si ça touche au statut de tel corps de métier, inviter un ou plusieurs représentant du dit corps, etc…)
* Permettre régulièrement des questions citoyennes (en nombre limité) portant sur la politique que l'assemblé devra prendre en considération
* Enfin et surtout, permettre la proposition de loi citoyenne sur un système similaire à celui des pétitions évoqué plus haut

Score : 0 / Réponses : 0

----

Je souhaite que mon avis de citoyen soit pris en compte de manière directe

Score : 0 / Réponses : 0

----

Arrêtons de dire " si je pouvais, je ferais ..... " agissons !

Score : 0 / Réponses : 0

----

bonjour dans un premier temps je pense qu il faut remettre tout le monde au meme niveau de reconnaissance , car actuellement il y a trop de différence entre eux et nous et à partir de là on peut pas discuter et nous faire entendre . Un exemple simple , comment voulez vous qu une personne qui touche 11000 euros par mois peut écouter et surtout comprendre celui qui touche 1200 euros par mois . Alors commençons par ça et je peux vous dire que l on auras fait un grand pas si on y arrive . Et bien d autre sujets pourrons etre abordés . Et non je ne suis pas un reveur , j en ai juste marre de ces injustices .

Score : 0 / Réponses : 0

----

Nous sommes en face de nos responsabilités citoyennes pour participer dans les décisions dont nos sommes tous concernés pour un avenir juste : Liberté Égalité Fraternité!!!!

Score : 0 / Réponses : 0

----

Il est urgent de nous permettre, à nous les citoyens, de nous exprimer et de proposer. C'est chose faisable par la création d'un référendum d’initiative citoyenne notamment et en nous donnant le droit de proposer une loi. Qui mieux que nous vivons la réalité du terrain...

Score : 0 / Réponses : 0

----

bonne idée, apprenons à nos politique la vraie vie des citoyens français qui votent pour eux et qui j'espère seront entendus en leur donnant quelques idées

Score : 0 / Réponses : 0

----

Prendre en compte les avis de la base, population mal représentée au sein de l emicycle

Score : 0 / Réponses : 0

----

Sauvons notre démocratie et ne laissons pas a une poignée d'individu décider pour nous et il doive pas gouverner notre
Pensé

Score : 0 / Réponses : 0

----

Excellente initiative. J'ai toujours de l'espoir comme tous nos anciens malgré les échecs à nous faire entendre écouter approuver... et nous sommes pourtant des millions de gens certes beaucoup plus nombreux que tous ces parasites en cols blancs inhumains méprisants incultes qui nous dirigent !

Score : 0 / Réponses : 0

----

Chaque voix compte dans les pétitions et l'assemblée nationale doit les entendre

Score : 0 / Réponses : 0

----

Totalement en accord avec le commentaire de C. change.org.
Les pétitions citoyennes en ligne sont à prendre en considération au même titre que le Référendum... car c'est l'expression de l'avis et la position du peuple.
Nous avons élus des Dirigeants, et nous attendons d'eux que nos revendications trouvent une réponse rapide et application concrété dans nos vie.

Score : 0 / Réponses : 0

----

S'il y a des pétitions c'est que les citoyens ont des choses à faire entendre, des avis à donner sur leur vie de tous les jours, leur environnement, leur avenir et c'est normal et essentiel de les écouter, d'en tenir compte.

Score : 0 / Réponses : 0

----

CE sont nos élus qui auraient du prendre cette initiative pour soutenir les valeurs pour lesquelles nous les avons délégués ..Bravo pour l'action que vous menez!

Score : 0 / Réponses : 0

----

Et oui, vive la démocratie participative.

Score : 0 / Réponses : 0

----

Obligation faite à l’assemblee nationale D’examiner une pétition des qu’elle recueille un certain nombre de signataires ( 100 000?500 000?) à définir.
Obligation de faire intervenir les initiateurs en commission adjointe. Obligation pour l’assemblee De porter la question devant le gouvernement et à celui ci d’y répondre. ( session des quesrions au gouvernement). Envisager une reconnaissance constitutionnelle de ce droit des citoyens à s´exprimer via le recours à un état pétition en ligne.

Score : 0 / Réponses : 0

----

A défaut d'une assemblée constituante issue de citoyens ordinaires, faisons entrer un peu de démocratie participative à L'Assemblée nationale.

Score : 0 / Réponses : 0

----

En ces temps sombres où l'on passe des lois par ordonnances en se passant des assemblées, il est certain qu'il nous faut plus de démocratie.

Score : 0 / Réponses : 0

----

J'ai vu le film "Des clics de consciences " qui met en avant le pouvoir citoyen, quand il y a mobilisation citoyenne.
Si Depuis août 2017, le Comité Économique Social et Environnemental a décidé de prendre en compte les pétitions en ligne, aujourd'hui, mobilisons nous pour que ce soit au tour de l’Assemblée nationale de donner un débouché législatif aux pétitions citoyennes, à travers les modalités proposées ci-dessus. Aussi n'hésitons pas à signer  la proposition de Change.org pour que nos  pétitions soient intégrées au débats au parlement, et partageons largement, car ne doutons pas de notre pouvoir citoyen

Score : 0 / Réponses : 0

----

Si les députés élu à l'assemblée nationale  veuleut écouté la voix  de leurs citoyens il faut qui accepte cette initiative; car sa peut aussi rendre le droit de manifestation nule vu que le peuple va exprimé son points de vu par la signature de pétition ....

Score : 0 / Réponses : 0

----

Depuis le temps que j'attends une reforme de ce genre, j'ai l'impression qu'il y a une trop grosse deconection entre le peuple et le gouvernement. Des initiatives de ce genres permettent d'être a nouveau entendu et ça fait vraiment du bien.

Score : 0 / Réponses : 0

----

Cette année 2017 a, me semble t-il, marqué un tournant dans notre pays : la volonté de tous nos concitoyens de mettre fin à un système politique professionnalisé et gangrené par les affaires, le copinage, les magouilles de toutes sortes !!  
Je ne suis malheureusement pas sûr que le nouveau Président est pris toute la mesure de cela !
Je suis donc favorable à plus d'initiative citoyenne, qui obligerait nos élus, de tous niveaux, à tenir réellement compte de l'avis du peuple.
Donnez-nous, donnons-nous les moyens d'être acteur de notre vie, dans une société plus juste et solidaire.

Score : 0 / Réponses : 0

----

Oui il est grand temps que le peuple puisse s'exprimer.
Les pétitions sont a ce jour un moyen qui peut être efficace si relayé auprès des commissions correspondantes aux sujets abordés.

Score : 0 / Réponses : 0

----

Merci de nous rassembler autour de cette belle initiative!

Score : 0 / Réponses : 0

----

L'intégration de la consultation citoyenne dans l'exercice du pouvoir législatif est l'essence même d’une démocratie respectueuse de l'avis des citoyens qu'elle gouverne.

Score : 0 / Réponses : 0

----

Notre engagement citoyen à  travers les réseaux sociaux pour influer sur les décisions qui engagent notre avenir doit être pris en compte pour que vive la démocratie

Score : 0 / Réponses : 0

----

Une excellente initiative!

Score : 0 / Réponses : 0

----

Pour qu'un pouvoir financier soit alloué au nombre de petitionnaires

Score : 0 / Réponses : 0

----

Pour que la raison du plus fort ne soit plus une fatalité et que la politique retrouve le sens du bien commun.

Score : 0 / Réponses : 0

----

Je vais paraphraser en citant un comique qui faisait mouche à chaque fois qu'il invoquait les politiciens. "Si votez servait à quelque chose, cela ferait longtemps que ce serait interdit". Aujourd'hui c'est moins drôle parce qu'il avait raison.

Score : 0 / Réponses : 2

----

Absolument nécessaire

Score : 0 / Réponses : 0

----

Le peuple reprend la main.Il existe tellement d'injustice.Surtout concernant les petites retraites, les calamités qui font que  des contribuables âgés de plus de 75 ans, exonéré d'office de la taxe d'habitation,avec comme revenu en moyenne égal à1200€/mois, sont normalement exonéré de la taxe foncière  (plus de 75 ans) , mais dépasse  le revenu fiscal (environ 11000€ de 30€. Dans les campagnes  la taxe foncière représente en moyenne 700€   ce qui est énorme par rapport aux revenus mensuels. Il faut relevé le revenu  fiscale afin, que  ces petits retraités ne soient plus assujettis à payer cette taxe foncière. Pour 30€ de trop le petit retraite va payer 700€  ;Ce qui est énorme par rapport aux revenus mensuels qui frise le seuil de pauvreté. Cet exemple est celui d'une personne célibataire. Il serai urgent que  l'on  se préoccupe de ce grave problème.

Score : 0 / Réponses : 0

----

Oui à la prise en compte de la parole citoyenne à l'assemblée sans l'obligation de passer par des partis qui ne représentent plus qu'eux-mêmes et sont en sous représentation

Score : 0 / Réponses : 0

----

Deviendrait un complément indispensable visant à améliorer le processus de prise en compte des sujets qui reste aujourd'hui l'apanage de nos députés et sénateurs pour qui nous avons voté, mais avec les difficultés que l'on connaît dans la prise en compte des demandes des citoyens.

Score : 0 / Réponses : 0

----

le citoyen électeur existera enfin en dehors des rendez vous électoraux

Score : 0 / Réponses : 0

----

Je suis pour la mise en place de ce dispositif au sein de l'Assemblée Nationale afin de prendre en compte toutes les voix.

Score : 0 / Réponses : 0

----

TOUS contribuacteurs !

Score : 0 / Réponses : 0

----

Il faut que la parole citoyenne soit mieux prise en compte. Cette démarche va en ce sens. Soutenons la!

Score : 0 / Réponses : 0

----

Une heureuse initiative à soutenir

Score : 0 / Réponses : 0

----

Bonjour,
A l'heure où les institutions de tout poil poussent tout le monde vers le "tout numérique" -allant jusqu'à occasionner des discriminations : pour qui vit en zone blanche, qui ne peut s'équiper (coût du matériel et des abonnements qui ne cessent d'augmenter), qui n'est pas "armé" pour y faire face ( notamment les "anciens jeunes")...- il ne me  parait pas concevable que les pétitions numériques n'aient pas autant de poids qu'une forme admise de contestation et/ou d'expression. 
Si l'état exige de ses administrés semblables efforts, il se doit d'être exemplaire lui aussi ; ne pensez-vous pas ?

Score : 0 / Réponses : 0

----

oui cette obligation pour nos députés de tenir compte et de débattre les sujets d'une pétition si elle recueille un grand nombre de signature doit être obligatoire. Mais cette obligation devrait également être imposées aux autres collectivités que sont la région, le département, et la mairies, car bien souvent, voir tout le temps les pétitions quelle qu'elles soient sont passées sous silence si elle ne satisfont pas la majorité politique des élus à tous les niveaux.......

Score : 0 / Réponses : 0

----

Si seulement c'était aussi facile.....Je veux y croire.

Score : 0 / Réponses : 0

----

Pour une écoute des citoyens.

Score : 0 / Réponses : 0

----

Naturellement je soutiens cette initiative, mais je n'ai pas compris comment il faut faire pour apporter ce soutien. Si quelqu'un a compris merci de m'indiquer la procédure à suivre.
Plus jamais d'ordonnances ni de 49.3 !

Score : 0 / Réponses : 0

----

Le droit à pétitions doit être inscrit dans la loi citoyenne.
Mon combat les pollutions sonores , atmosphériques et l'insécurité pour les riverains des routes à grandes circulation (+ 20 000 véhicules/jour)

Score : 0 / Réponses : 0

----

une prise en compte réelle des préoccupations des citoyens devrait etre évidente dans une vraie démocratie

Score : 0 / Réponses : 0

----

Mes ami(e)s sont "déboussolé(e)s par des inerties récurrentes (plus de 60 ans) de promesses!!!!

Score : 0 / Réponses : 0

----

Face aux décideurs, locaux ou autres le citoyen "lambda" est démuni et subit étant "désarmé".Responsable d'une asso,"combat" est très, très difficiles surtout pour fournir des études prouvant le bien fondé des demandes d'amélioration!

Score : 0 / Réponses : 0

----

Oui, je veux que la démocratie soit plus forte. Et oui je trouve dommage que ce soit un site privé qui y contribue.

Score : 0 / Réponses : 0

----

Enfin une possibilité pratique et facile de s'exprimer et que cela soit pris en compte.

Score : 0 / Réponses : 0

----

La souveraineté appartient au peuple. Belle initiative!

Score : 0 / Réponses : 0

----

Si ce droit existe il est donc essentiel que le parlement le mette en oeuvre

Score : 0 / Réponses : 0

----

Avec cette initiative. De plus ne laissons plus les boîtes à sondage  décider des opinions publiques. A nous de décider de nos engagements, de nos refus, de nos propositions par voie pétitionnaire qui soit habilitée à être prise en considération par "nos" élus.

Score : 0 / Réponses : 0

----

Bravo. Nous allons pouvoir aborder les sujets qui dérangent et "peut être " éviter les "exceptions ou dérogations" qui tapissent nos lois. La majorité silencieuse pourra "enfin "se faire entendre.

Score : 0 / Réponses : 0

----

Les pétitions change.org ont du sens et permettent aux citoyens de défendre une idée, une cause. Lorsqu'elles prennent une ampleur importante, l'Assemblée Nationale devrait en tenir compte car c'est l'expression des citoyens (exemple : par rapport à une injustice ou bien une loi qui devrait être modifier).

Score : 0 / Réponses : 0

----

Inscription sur le site validé. Nous allons voir si nos propositions sont prises en compte où pas !

Score : 0 / Réponses : 0

----

Si cette chose était mise en place, idéalement pour moi , j'ouvrirai ce site , j'irai sur le thème qui m'intéresse, car l'entrée se ferait par commission parlementaire puis par sous domaine, puis j'aurais accès aux travaux parlementaires en cours selon leur stade d'avancée  et  il y aurait une entrée pour les initiatives ou propositions innovantes .Je saurai qu'elle est la composition de l'équipe qui reçoit et traite les informations sous la houlette de chaque président de commission concerné .Des attachés parlementaires pourraient être mis à contribution pour exploiter ces propositions .Ils  proviendraient de tous les partis.Le citoyen contributeur saurait si sa contribution est retenue pour ensuite être soumise à  pétition ou pour être proposée ou soutenue par un parlementaire, faire l'objet d'une étude d'impact. Les contributeurs citoyens dont le projet est remarqué pourraient être entendus par une commission parlementaire.dans ce cadre, ils bénéficieraient d'une confidentialité et d'une protection juridique à l'égard de leur hiérarchie  notamment pour les fonctionnaires en tant que participant à une mission de service civique. Il arrive fréquemment d'avoir de belles idées , innovantes et l'incapacité de les faire arriver à bon port même en s'adressant aux élus locaux .Le contributeur devrait comme pour toute proposition de loi ou d'amendement donner des pistes de financement.

Score : 0 / Réponses : 0

----

La prise en compte par les représentants du peuple à l'Assemblée Nationale des pétitions émises par les électeurs est un pas indispensable pour une vraie Démocratie. Ce n'est pas parce qu'un député pense une chose en adéquation avec son groupe politique que ses électeurs pensent la même chose... ce n'est pas parce qu'il a été élu qu'il doit oublier qu'il est au service de tous.

Score : 0 / Réponses : 0

----

la démocratie c'est le pouvoir par le peuple, vu que le peuple sauf ma génération ne sort plus dans la rue, les pétitions sont le meilleur moyen de savoir ce que veux le peuple

Score : 0 / Réponses : 0

----

Les Conseils de développement, instances participatives composées de bénévoles issus de la société civile et le Conseil Économique Social et Environnemental Régional le Grand Est ont expérimenté pour la première fois un travail, une méthode commune de réflexion et d’élaboration d’un document partagé. Nos instances ont co-élaboré et collaboré à l’écriture d’un Manifeste « vers de nouvelles formes d’expression citoyennes »  sur la place et le rôle de la société civile pour renouveler les pratiques démocratiques.
Face au constat d’un système politique à bout de souffle, face aux défiances récurrentes des citoyens vis-à-vis des élus et des corps constitués, et inversement, face à l’émergence de nouvelles formes d’implication citoyenne dans la vie publique, nous apportons notre regard sur le rôle que nos instances peuvent jouer en faveur de la démocratie de demain, de la participation citoyenne, du projet de société que nous voulons.
Le croisement de nos visions, de nos expertises, de nos cultures et de nos méthodes de travail avec le citoyen pallie le chaînon manquant entre l’expertise classique et la décision publique.
Ce manifeste sera rendu public le 28 novembre prochain lors d’une conférence de presse et nous ne manquerons pas de vous l’adresser. Les pistes de réflexions et les expériences que nous y relatons au niveau de notre territoire pourraient être inspirantes et faire montre d’exemple et d’expérimentation future au niveau national.

Score : 0 / Réponses : 0

----

Exprimons-nous pour diminuer les contraintes réglementaires, augmenter les libertés individuelles, agir avec du simple bon sens, et de la bienveillance. Il faut fortement réduire le pouvoir des élus, chaque élu devenant un petit roi dans notre système issu de l'après royauté et de l'après bonapartisme !

Score : 0 / Réponses : 0

----

Pour une démocratie participative, et l'abolition de la "démocrature" qui s'impose progressivement en France...

Score : 0 / Réponses : 0

----

Oui, modernisons la démocratie et renforçons la participation des citoyens en ligne

Score : 0 / Réponses : 0

----

Pour une démocratie plus juste !

Score : 0 / Réponses : 0

----

Excellente initiative pour que les préoccupations des citoyens soient prises en compte.

Score : 0 / Réponses : 0

----

Après vérification de l’identité des signataires.

Score : 0 / Réponses : 0

----

Bonne idée, afin que soient pris en  considération les désidératas du peuple.

Score : 0 / Réponses : 0

----

Que l'assemblée accepte le principe d'examen obligatoire, avec des conditions à négocier, des pétitions électroniques.

Score : 0 / Réponses : 0

----

Bien dommage que je n'apprenne l'existence de cette initiative que 2 jours avant sa  clôture mais je vais rester attentive maintenant.

Score : 0 / Réponses : 0

----

Ce droit permettrait à chaque citoyen ce s'insérer de manière plus logique et concrète dans le droit existant de notre République !! Il rentrerai en corrélation avec l'objectif à valeur constitutionnelle d’inéligibilité et de clarté de la norme.

Score : 0 / Réponses : 0

----

J'avais proposé  à tous les intervenants motivés   de cette consultation de constituer une liste de ceux qui voudraient continuer à échanger sur des sujets évoqués  dans cette consultation .
Chacun choisira bien sûr les sujets sur lesquels il veut s'exprimer...
La liste n'étant comuniquée qu'aux inscrits bien sûr.
Je n'ai eu qu'une seule réponse... 
Je redonne quand même mon mail  :-)    bachaud.yvan@free.fr

Score : 0 / Réponses : 0

----

Qui ne tente rien, n'a rien...
A voir, j'ai quelques doutes sur le poids de notre "contribution"...

Score : 0 / Réponses : 0

----

Pour suivre l'évolution des modes de vie, modernisons les débats.

Score : 0 / Réponses : 0

----

Lisez Agnès Cueille, tout en bas : c'est la réponse nécessaire pour rendre votre tentative réalisable.

Score : 0 / Réponses : 0

----

Je signe parfois des pétitions proposées par Change.org, .... ou pas !
je propose qu'à l'envoi de chaque pétition aux habituels destinataires de ces pétitions, la possibilité soit offerte de dire: Je ne suis pas d'accord. 
Ne serait-ce pas là, la vrai démocratie ?

Score : 0 / Réponses : 0

----

Dans une démocratie moderne, la collégialité des décisions doit pouvoir se faire par intervention directe des citoyens actifs

Score : 0 / Réponses : 0

----

je suis favorable à cette initiative qui redonnera peut-être du "pouvoir" aux citoyens.

Score : 0 / Réponses : 0

----

Pour redonner du sens à la politique, donner envie aux citoyen(ne)s de s'impliquer et combattre l'abstention qui gangrène les élections.

Score : 0 / Réponses : 0

----

Je suis une victime des lois et du fonctionnement actuelle de la JUSTICE. Ma triste expérience m’a obligé à regarder en face les fonctionnements déviants de diverses corporations qui ont mis en place leurs propres règles et prétendent rendre une « justice » privée, dans un silence des plus opaques, ayant pour seul but de ne pas nuire à la réputation de leurs corporations.
J’ai contacté plusieurs organismes des plus connus et là aussi c’est le silence. Quant au Défenseur des Droits, ce n’est pas son problème et dit ne pas pouvoir intervenir. Les victimes sont SEULES et méprisées dans la démocratie Française.
OUI j’ai beaucoup de choses à dire et à faire connaître.
J’ai publié une petite vidéo sur facebook pour faire connaître mon avis sur la justice mais elle ne semble pas intéresser grand monde. C’est déconcertant et décevant

Score : 0 / Réponses : 0

----

Le point 4 apporte une restriction qui va à l'encontre de toute cette belle initiative : il n'y a aucune raison d'imposer une fréquence d'examen, surtout un texte par mois, c'est trop peu, car c'est la porte ouverte à des réponses aux pétitionnaires du style : "votre proposition n'a pas pu encore être examinée " et à des délais inadmissibles. Quand la pétition a regroupé un nombre très important de signatures, elle doit passer en priorité à l'examen et rapidement, sans attendre des mois, voire des années. Idem quand une pétition apporte un plus très intéressant à une loi déjà existante, dans ce cas il faut qu'elle soit examinée rapidement même si elle n'est signée que par peu de personnes (des experts par exemple).

Score : 0 / Réponses : 0

----

bonjour,
ce qui me navre le plus c'est de savoir que nous somme le pays d'une révolution qui a su inspirer aux autres des idées de changement ,des lois humaines ,des codes, une fraternité etc et qu'aujourd'hui on nous cite systématiquement le fonctionnement des autres nations européennes. ce qui est bien pour elles ne l'est pas forcement pour nous ..Alors essayons de nous creuser les méninges. Abolissons chez nous le "faites ce que je vous dis et non ce que je fais "pour déjà essayer de comprendre ce que la France a besoin et éviter de penser qu'être présent ou élus dans une assemblée n'est que le sésame a l'enrichissement personnel. De représenter les gens doit rester un honneur et géré a ce titre. Avant de crier haro sur les retraités ou tout autre corporation, nos élus ont le devoir de créer l'emploi et d'occuper notre jeunesse pour éviter son écoeurement aux politiques de tous bords. Basta du désordre, de l'éclatement des régions, des changements religieux ,des débats stériles flatteurs ou prometteurs, donnons nous aujourd'hui l'envie de vivre dans ce pays en lui redonnant confiance face a des responsables capables de les écouter et de les entendre surtout..........tant mieux donc si ces actions populaires créent du renouveau et ré-ouvrent les débats constructifs.

Score : 0 / Réponses : 0

----

Pour ma part, je pense humblement qu'il serait temps de revoir la base même de nos textes de loi, à savoir la Constitution, via la création d'une Assemblée Constituante de 2000 citoyens tirés au sort. Le but de ce processus serait de proposer l'instauration d'une VIe République au fonctionnement moins pyramidal et plus horizontal, transparent et participatif.

Score : 0 / Réponses : 0

----

Pour compléter ce bel outil parlementaire, relancer la reconnaissance juridique des actions de groupe (class actions) serait pertinent pour contrebalancer quelque peu notre formatage individualiste...

Score : 0 / Réponses : 0

----

Beaucoup d'espoir pour les citoyens de participer activement à la vie démocratique...

Score : 0 / Réponses : 0

----

Je soutiens cette idée  qui ne peut qu'améliorer la démocratie.

Score : 0 / Réponses : 0

----

Une bonne idée pour que soit mieux représenté le citoyen !!!

Score : 0 / Réponses : 0

----

Une démocratie, c’est par le peuple… Alors il faut utiliser la proximité et l’outil informatique. Chaque citoyen devrait pouvoir poser LA question qui lui tient le plus à cœur sur un site de sa circonscription et visible par tous. Si cette question obtient un certain % ( par exemple 5% des inscrits), alors le député prends le relais en invitant un groupe constitué de 10 citoyens afin de lancer une consultation dans sa circonscription. Si cette dernière obtient un seuil à définir, alors elle est lancée dans la région puis si succès au niveau national. Nous avons les idées, les outils ALORS passons à l’action.

Score : 0 / Réponses : 0

----

il serait souhaitable d'avoir un ministere qui rassemblerait et ferait le point sur toutes les pétitions qui vont dans le même ordre et la même demande afin de centraliser et d'être plus performant

Score : 0 / Réponses : 0

----

Parce que pour autant qu'il soit légitime, un Président élu par moins de 13 % de la population de son pays (résultat du 1er tour / 66 990 826 INSEE fin 2016) ne peut démocratiquement accéder au respect de son peuple que s'il  le rassemble sous son opinion; il doit donc en retour accepter de considérer ce que l'expression populaire lui exprime légitimement à mesure de la mise en application de ses propositions. La démocratie doit s'efforcer de se rapprocher du respect de l'opinion majoritaire pour renforcer son propre respect. Le droit de pétition à l'Assemblée nationale représente un moyen très évident d'expression démocratique en temps réel.

Score : 0 / Réponses : 0

----

Bonjour à tous,

Les propositions prioritaires à mettre en œuvre de : 
- réduction des lourdeurs administratives et process des organismes publiques de l'état (principe du président MACRON, 2 textes de loi supprimés pour 1 nouveau créé)
- simplification voire refonte des process de l'état
  (ex. : aujourd'hui, lorsque que l'on veut faire construire une maison individuelle, l'architecte Bâtiment de France ne respecte pas/ ne suit pas le PLan d'Urbanisme de la ville et donne des critères de construction qui lui sont propres).
- Promouvoir l'initiative citoyenne en réduisant les interfaces / les multiples institutions de l'état à contacter pour un même sujet ou pour avoir une réponse.
- Permettre aux citoyens de porter des propositions d'amendement, de suppression et de création de loi et de participer aux votes.
  (ex. : Si proposition facile à mettre en œuvre, simplifie, améliore et ne coute rien au citoyen alors s'est adopté...)

Score : 0 / Réponses : 0

----

Une idée que je crois efficace pour faire réellement vivre la démocratie au quotidien.

Score : 0 / Réponses : 0

----

Évidemment que je suis pour, les initiatives citoyennes sont souvent bien en avance sur la conscience politique. Elles enrichissent le débat et accélérent la transition-indispensable- qui s'annonce.

Score : 0 / Réponses : 0

----

Cette proposition nous permettrait d'aller plus loin et de pouvoir faire entendre nos voix à l'assemblée. J'y suis totalement favorable.

Score : 0 / Réponses : 0

----

Sa devrait être comme sa dans tout les pays pour baisser au maximum les abus de pouvoir de quelques personnes qui ne vevi pas les conséquences de leur acte.

Score : 0 / Réponses : 0

----

Cela nous permettrait de faire entendre nos voix à l'assemblée, j'y suis totalement favorable.

Score : 0 / Réponses : 0

----

La consultation des citoyens sur ce thème est bien la seule consultation inutile, il y a bien longtemps qu'une loi aurait dû être proposée et votée à l'assemblée permettant la prise en compte des idées citoyennes dans le débat public. Pétitions en ligne, référendum d’initiative populaire, propositions de loi citoyennes, questions au gouvernement citoyennes… : leurs mises en œuvre n'ont pas à être priorisées, toutes peuvent être mises en place rapidement avec les moyens technologiques actuels. Cependant s'il faut choisir, c'est le referendum d'initiative populaire qui m'apparait le plus à même d'imposer à l'assemblée nationale et au gouvernement la discussion d'un sujet de loi ou de modification de celle-ci

Score : 0 / Réponses : 0

----

Les citoyens de ce pays vont récupérer le pouvoir dont ils ont vicieusement été dépouillés depuis 50 ans par les grands imposteurs de l'économie de marché et par nos maîtres corrompus qui leur sont soumis. Le pouvoir retournera irrémédiablement au peuple comme l'Histoire nous l'a toujours prouvé. La balle est dans votre camp messieurs les députés, le choix vous appartient désormais de faire que les choses se déroulent de manière civilisée ou dans la violence, car nous éduquons nos enfants à désobéir au pouvoir illégitime, à anéantir tout celui qui le défend ou qui en profite ainsi qu'à l'effondrement de ce système inégalitaire dont ils comprennent qu'ils seront esclaves. C'est à vous de décider de quelle façon l'Histoire se souviendra de vous, nous mourrons peut-être pauvres mais assurément libres, quel qu'en soit le prix. La tyrannie des 1% touche à sa fin, on vous aura prévenus.

Score : 0 / Réponses : 0

----

La consultation des citoyens sur ce thème est bien la seule consultation
inutile, il y a bien longtemps qu’une loi aurait dû être proposée et votée à
l’assemblée permettant la prise en compte des idées citoyennes dans le
débat public. Pétitions en ligne, référendum d’initiative populaire,
propositions de loi citoyennes, questions au gouvernement citoyennes… :
leurs mises en oeuvre n’ont pas à être priorisées, toutes peuvent être mises
en place rapidement avec les moyens technologiques actuels. Cependant
s’il faut choisir, c’est le referendum d’initiative populaire qui m’apparait le
plus à même d’imposer à l’assemblée nationale et au gouvernement la
discussion d’un sujet de loi ou de modification de celle-ci

Score : 0 / Réponses : 0

----

METTRE EN PLACE UN DROIT DE PETITION
Comme au Royaume Uni ou aux Etats-Unis, le droit de pétition devrait être institutionnalisé avec la mise à disposition d’une plateforme dédiée sur internet, contrôlée et protégée par les services de l’Etat. Si une pétition atteint le seuil 1 (10 000 en UK, à relever pour la France vers 500 000 ?), le gouvernement doit y répondre par écrit. Si elle atteint le seuil 2 (100 000 en UK, à relever pour la France vers 1 million ?), elle doit faire l’objet d’un débat et d’un vote au Parlement.

Score : 0 / Réponses : 0

----

Vote sur ton téléphone !
Il serait tellement facile aujourd'hui d'instaurer une application Démocratie.
Référendums, pétitions, élections...
Nous passons tellement de temps sur nos appareils, et tellement de temps à nous plaindre !
Faisons de ces deux pertes de temps, une seule et même action !
Et pour ceux qui n'ont pas de smartphone (non à l'obligation à la technologie) il reste l'ordinateur et les cybercafés.

Score : 0 / Réponses : 0

----

La Suisse reconnaît déjà le droit de pétition.  La population est ainsi plus impliquée .

Score : 0 / Réponses : 0

----

Notre constitution nous enseigne que réside le pouvoir par le peuple et pour le peuple. Il serait bon que le peuple ait donc un droit de proposition par pétition. Lui permettant de faire une sorte de "contre-pouvoir" aux élus si jamais ces derniers changent d'opinion une fois l'élection passé comme cela s'est déjà vu...
Ceci dit, il faut sécuriser ce nouveau système de pétition car l'AN ne pourrait analyser des millions de pétitions par jour, les députés restent des hommes, pas des robots. Il faudrait fixer des conditions: nombres de signatures, par exemple.

Il faut laisser se pouvoir à tout instant entre les mains de tous les Français et sur tous les sujets. Un exemple: une pétition sur le retrait d'un médicament qui vient à faire scandale aurait parfaitement sa place.
Ou encore une pétition sur des sujets très importants.

Ce genre de recours pourrait avoir lieu notamment si l'élu n'est pas à l'écoute de ses citoyens, car ci ce dernier fait son job, techniquement il n'y aura pas besoin de lancer une pétition, c'est l'élu qui s'en chargera.

Score : 0 / Réponses : 0

----

Enfin de la démocratie pour tous

Score : 0 / Réponses : 0

----

Initiative avec bien de potentiel pour la santé de nos procedures politiques.  La vigilance est sur l'ordre du jour, aussi, car de tels moyens facilitent aussi l'idéologie, des passions passageres, de la 'mode' politique.  Ne pas perdre de vue; la politique c'est souvent l'art de compromettre.

Score : 0 / Réponses : 0

----

Bravo pour cette décision démocratique qui donne aux Français et à leurs élus les moyen de communiquer par le biais des technologies de notre ère !

Score : 0 / Réponses : 0

----

Au moins, nous serions peut-être mieux écoutés et avoir des réformes qui nous conviennent mieux à tous.

Score : 0 / Réponses : 0

----

Bravo pour cette initiative. Il est temps que  l'on donne à tous les français le pouvoir de se sentir  concernés et la possibilité de participer aux grandes orientations de notre Pays.

Score : 0 / Réponses : 0

----

ça aurait du ce faire depuis longtemps!!!

Score : 0 / Réponses : 0

----

L'élaboration d'un cadre législatif pour la prise en compte des pétitions en tant que nouvelle forme de participation citoyenne au fonctionnement de notre démocratie ne peut être qu'une bonne chose. Mail elle soulève bien des questions. Par exemple qui seront les "modérateurs" mentionnés dans la charte de consultation ? Pourquoi limiter le nombre de pétitions prises en compte et sur quels critères établir leur ordre de priorité ? Pourquoi appliquer des règles différentes aux collectifs citoyens et aux associations ou ONG internationales ? 

Mon point de vue est que la prise en considération par le Parlement des pétitions qualifiées de "recevables" sur la base de critères qui restent à définir, qui instituerait une nouvelle forme de participation citoyenne au fonctionnement de notre démocratie, représente une réforme d'une telle ampleur qu'elle ne peut être envisagée que dans le cadre de l'élaboration d'une nouvelle constitution et donc d'une VIème République, car elle suppose une toute nouvelle conception du pouvoir législatif et ne peut pas se déployer tant que le pouvoir judiciaire n'est pas complètement indépendant du pouvoir exécutif, ce qui n'est toujours pas le cas. 

De plus, il ne faudrait pas ouvrir la voie au pouvoir de l'émotion du moment au sein de l'opinion publique, émotion volatile par essence. Trouver le juste équilibre sera une mission délicate. Mais les repères doivent rester stables : la séparation des pouvoirs telle que l'a conçue Montesquieu, d'une part, et l'association de citoyens qui ne soient pas des professionnels de la politique aux débats sur les enjeux cruciaux auxquels la Nation et le Peuple sont confrontés.

Score : 0 / Réponses : 0

----

Excellente initiative pour éclairer ceux qui doivent prnedre des décisions.
Toutefois le populisme est à éviter. Rappelons-nous qu'en 1981, l'idée d'abolir  la peine de mort n'était  pas du tout majoritaire en France : il a fallu une volonté politique de certains pour l'obtenir. Aujourd'hui peu nombreux sont ceux qui souhaitent revenir en arrière.

Score : 0 / Réponses : 0

----

Pour un retour à une vraie démocratie, enfin !

Score : 0 / Réponses : 0

----

Je souscris volontiers à cette proposition, mais je crains fort que même correctement formulée sur le plan légal et selon une éthique démocratique, elle se révèle un cautère sur la jambe de la démocratie bien malade. En effet, la  Ve constitution n'a rien de démocratique si on l'examine attentivement, et elle est d'autant plus aggravée dans sa nature par le scrutin majoritaire à  deux tours. Le scrutin sénatorial n'arrange pas grand chose non plus. Ajoutons à cela que l'exercice du vote n'est pas obligatoire en France malheureusement, et que le député a perdu globalement toute crédibilité, non pas parce que c'est un traître à sa parole ou un profiteur, mais parce que constitutionnellement il ne peut-être qu'un "godillot" ou un râleur impuissant in fine. Cette proposition de reconnaissance du référendum d'initiative populaire  vient en fait frontalement en contradiction avec notre constitution. Soit la Présidence suggèrera un détournement de la rédaction de la proposition ceci au nom de 'l'en même temps" pour garder un semblant de "démocratie", soit l'assemblée Nationale y verra un risque de suicide à venir quant à la légitimité de son élection ( et le Sénat avec elle par solidarité), et de ce fait refusera ce nouveau droit, les deux chambres confortablement installées pour 4 années encore dans leur légitimité constitutionnelle et organique ; quitte à ressortir une future promesse d'adoption faîte à l'électeur  pour s'assurer une réélection.

Score : 0 / Réponses : 0

----

Plutôt que de gouverner en fonction des sondages, il serait utile que nos élus se penchent sur les sujets qui préoccupent vraiment leurs administrés

Score : 0 / Réponses : 0

----

En générale, nos chers élus (une fois élus) ont une fâcheuse tendance à ce déconnecter de la réalité de notre vie quotidienne. La pétition est notre seule arme démocratique pour leur faire comprendre les conséquences collatérales de leurs décisions. Nous simples citoyens nous choisissons une personne afin de nous représenter et être la plus apte à sauvegarder nos intérêts. Malheureusement, cette dernière une fois élue fait souvent l'inverse de ce nous attendons d'elle (au nom de la raison d'état mais en réalité, souvent par ignorance et par orgueil. La pétition est vraiment le seul moyen démocratique pour faire avancer les choses dans le bon sens.

Score : 0 / Réponses : 0

----

Voilà peut-être un moyen de sortir de la politique politicienne - qui relève d'une gueguerre entre egos des classes dites supérieures, et pas classe du tout - et de raviver l'intérêt des Français pour des préoccupations politiques.

Score : 0 / Réponses : 0

----

D'accord

Score : 0 / Réponses : 0

----

Pour renforcer la voix du peuple, il y a la pétition et la consultation sur des sujets d'actualité, de société, etc. : modernisons le système car plus de transparence et d'efficacité sont souhaitables.

Score : 0 / Réponses : 0

----

Je soutiens cette initiative !

Score : 0 / Réponses : 0

----

LA QUESTION CITOYENNE AU GOUVERNEMENT : Il faut qu'un panel citoyen représentatif de la société française puisse poser une question au Gouvernement chaque semaine. Ce panel sera désigné par tirage au sort après inscription sur cette plateforme et renouvelé une fois les questions posées. Le citoyen mentionnera lors de son inscription sur la plateforme la thématique qu'il souhaite aborder. Cette inscription sur la plateforme devra être accompagnée de la signature d'une charte des valeurs républicaines afin d'éviter des questions inappropriées.

Score : 0 / Réponses : 0

----

Un bon moyen d'éviter la guerre civile...

Score : 0 / Réponses : 0

----

Mes suivants 15 PROJETS des LOIS(partie 1) que je propose le 5 novembre 2017 à la Consultation.Democratie-Numérique.Assemblée-Nationale.FR
1)Les demandeurs d’emploi devraient bénéficier dès que possible des avantages/facilités fiscales y compris les charges sociales et sans d’être imposés (ou imposées) fiscalement, pendant minimum les premiers 5 ans de la création d’une entreprise ou de la reprise d’une entreprise ;
2)La CAF +Pôle Emploi+ les autres institutions publiques+chaque les employeur d’Etat et privé soit obligés (et obligées) de s’interconnecter et de se fournir/délivrer entre elles/entre eux tous les documents prouvant (attestations d’employeurs, contrats de travail, bulletins de salaires, préavis de licenciement, ruptures de contrats de travail, décisions de licenciement, décisions de démissions, certificats de travail, certificats de congé médical, etc.) l’embauche et l’historique à jour de chaque salarié afin de décharger de ces obligations trop stressantes, fatigantes, énergofagues, qui sont au présent mises sur les épaules des citoyens et chaque telle institution (énumérées plus haut) soit obligée d’envoyer gratuitement et régulièrement à chaque occasion (par semaine ou par mois), PAR EMAIL ET PAR LA POSTE, à chaque personne citoyenne concernée tous les documents et attestations prouvant leur situation personnelle (mise à jour) professionnelle et financière, y compris leur historique professionnel et financier mis à jour : les effets seront bénéfiques pour : A)L’écologie, en diminuant ainsi l’effet de serre et le réchauffement au niveau de la France ; B) L’élimination importante d’un des facteurs de stress et de la dépression qui causent TROP de suicides en France ; C) ainsi sera réalisée une meilleure et plus démocratique inter-connectivité entre les services de L’Etat avec les citoyens ; D) Sera ainsi assurée la meilleure ou la parfaite gestion et mise à jour de l’immense BASE DE DONNEES NATIONALE DE LA France ; E) Sera réalisée ainsi une GRANDE SATISFACTION ET GRATITUDE ET CONFIANCE DE CHAQUE CITOYEN auprès du system, des institutions de L’Etat et du Gouvernement ;
3)La création et l’installation des bornes ou des pôles d’internet (ayant aussi une imprimante et un téléphone publique gratuite pour les appels en France) utilisables NON-STOP et partout en France, sur les domaines publiques, sur les rues, sur les autoroutes, dans les marchés publiques, dans l’intérieur et dans l’extérieur de chaque ville et commune de la France ;
4)La Loi « LA TABLE DE LA NATION FRANCAISE » : par laquelle soit crée dans chaque ville / commune des restaurant-cantines de L’Etat (soutenus financièrement par l’Etat et par des fonds de L’Union Européenne et par les grandes entreprises françaises et étrangères siégées en France et par les milliardaires et millionnaires particuliers français et étrangers imposés en France) un repas chaud (Menu complet) par CHAQUE JOUR 7jr/7jr aux demandeurs d’emploi et à toutes personnes démunies pas imposables fiscalement ;
5)La Loi « LES VOITURES ELECTRIQUES » : par laquelle le Gouvernement va offrir chaque année progressivement à chaque citoyen (en commençant avec les demandeurs d’emploi et les personnes démunies financièrement) une voiture électrique SANS LE DROIT DE LA VENDRE, en obtenant et attirant dans ce but des fonds des grandes entreprises françaises et étrangères en échange de les promouvoir leur produits/services ;

Score : 0 / Réponses : 0

----

La continuation de mes suivants 15 PROJETS des LOIS que je propose (partie 2) le 5 novembre 2017 à la Consultation.Democratie-Numérique.Assemblée-Nationale.FR
6)La Loi « AUTOROUTES SOLAIRES FRANCAISES » :   installer, monter et interconnecter SUR LES 2 COTES DE CHAQUE AUTOROUTE de France des PANNEAUX PHOTOVOLTAÏQUES SOLAIRES qui auront 4 surfaces exposées au soleil (la côté droite de l’autoroute aura 2 surface : par l’extérieur une et la 2ème par l’intérieur de l’autoroute, idem pour la côté gauche) + des PANNEAUX PHOTOVOLTAÏQUES SOLAIRES centraux au milieu et tout au long de chaque autoroute, qui auront –ils aussi 2 surfaces exposées au soleil : l’énergie solaire électrique GRATUITE obtenue ainsi sera vendue et va crée un grand ou gros revenu qui pourrait être distribué aux investisseurs et au Gouvernement, en baissant ou annulant les taxes des autoroutes (valables cet avantage seulement pour les citoyens de la France, pas pour les touristes étrangers) et en baissant beaucoup le prix du gasoil, aussi ;
7)La Loi « LA TRANSPARENCE PUBLIQUE DU RECRUTEMENT » : chaque année, dans chaque agence Pôle Emploi, chaque entretien ( programmé ou pas programmé ) avec chaque  demandeur d’emploi devra être enregistré audio et vidéo en utilisant les vidéo-caméras qui sont déjà installées dans chaque agence Pôle Emploi dans le hall de l’accueil (et cela est très bon) et chaque demandeur d’emploi devras avoir le droit de recevoir gratuitement et sur place à sa demande, à la fin de son chaque entretien(programmé ou ad-hoc) avec son conseiller Pôle Emploi ou à la fin de son dialogue avec le fonctionnaire de l’accueil de l’agence Pôle Emploi la copie de l’enregistrement audio-vidéo ;
8)La Loi du « BENEVOLAT AU BENEFICE DES INSTITUTIONS DE L’ETAT : Préfecture, CAF, Pôle Emploi, Service d’Assistance Sociale, Mairies, La D.D.T., etc. » : par laquelle chaque citoyen pourra offrir gratuitement dans une manière bénévole ses services par ses capacités/compétences diverses au bénéfice des institutions de L’Etat ;
9)La Loi « LE SERVICE CIVIQUE DE TOUS LES CITOYENS, Y COMPRIS DE PERSONNES PLUS AGEES DE 25 ANS » : le Service Civique actuel devrait s’élargir et s’ouvrir à tout le monde, y compris aux personnes plus âgées de 25 ans, afin de pouvoir ainsi bénéficier chacun d’entre eux correctement et honnêtement d’un revenu sûr de minimum 500 euros net par mois, durant de missions de minimum 3 mois ou 6 mois, comme gratitude pour leur travail offert au bénéfice de la société française, y compris les diverses institutions de L’Etat ou diverses entreprises, chacun d’y sortant gagnant-gagnant ;
10)La Loi « L’ETAT en chat sur ligne (online) avec les citoyens » : Toutes les institutions et services publiques de L’Etat (en commençant prioritairement par les services de l’Assistance Sociale) devraient commencer bénéficier NONSTOP et utiliser NONSTOP dès que possible des connections de chat sur l’internet avec le public (pour les messages offline et pour les messages online), afin de dégrever ainsi les services publiques actuels qui sont encore trop agglomérées par les demandes des entretiens physiques de la part du public : par exemple, au présent une Assistante Sociale peut offrir un entretien programmé seulement en 2 ou 3 mois ( !), cela signifiant qu’elle peut assister, recevoir, communiquer directement et emphatiser directement avec la personne qu’elle assiste seulement 4 ou maximum 5 fois par an( !) ;
11)La Loi « LIBERTE D’ENTREPRENDRE ET DE CREER DES ENTREPRISES AUSSI POUR LES AVOCATS DE LA France » : Dès que possible, tous les Maîtres devraient commencer bénéficier du droit de créer et d’avoir leur propre entreprise en France (comme ailleurs dans des autres pays), personne, aucune partie, ni les Maîtres, ni L’Etat, n’ayant rien à perdre, tout le monde aura à gagner, y compris les centres d’impôt ; 
12)La loi « LIBERTE D’ENTREPRENDRE AU MAXIMUM ET SANS LIMITES EN FRANCE » : par laquelle tous ceux qui désirent créer une entreprise vont pouvoir avoir le droit de déclarer et d’utiliser TOUTES LES CODES CAEN pour ...

Score : 0 / Réponses : 0

----

La continuation de mes suivants 15 PROJETS des LOIS que je propose (partie 3) le 5 novembre 2017 à la Consultation.Democratie-Numérique.Assemblée-Nationale.FR
12)La loi « LIBERTE D’ENTREPRENDRE AU MAXIMUM ET SANS LIMITES EN FRANCE » : par laquelle tous ceux qui désirent créer une entreprise vont pouvoir avoir le droit de déclarer et d’utiliser TOUTES LES CODES CAEN pour pouvoir dérouler légalement à tout moment, n’importe quelle activité existante dans le nomenclateur des activités légales de France, afin de mettre en pratique à tout moment une, deux, huit, vingt, ou «N»-ième activité déclarée déjà dans le statut de son entreprise, en fonction du marché, du marketing et en fonction des opportunités des affaires et de ses compétences professionnelles et de ses ressources (du temps, d’énergie et d’argent), en sachant que chacune de ses éléments de l’équation peuvent varier d’un jour à l’autre et afin de ne pas limiter -–comme à présent— les entrepreneurs de déclarer et de dérouler SEULEMENT MAXIMUM 5(cinq)activités différentes, spécialement qu’il y a ailleurs dans L’UE des pays plus petits et plus pauvres que la France qui offrent cette liberté entrepreneuriale et ses opportunités légales à leurs entrepreneurs, les aidant ainsi de gagner et de booster ainsi  leur entreprise, leur revenu et profit, contribuant ainsi à booster l’économie et L’Etat ;
13)La Loi « LA BOURSE DES IDEES ET PROJETS DES CITOYENS DE LA FRANCE » :  par laquelle chaque personne domiciliée en France va pouvoir contribuer à l’émergence de la France et du bien-être de la société française par son apport intellectuel, en proposant ses idées et ses projets qui seront récompensés par L’Etat par l’argent : par des Prix provenant des sponsors privés, y attirés à participer par leur contribution financière, en échange d’obtenir de la part de L’Etat la promotion publique (partout) de leur compagnies/services/produits ;
14)La Loi « SOYEZ BIEN REVENU(E)S EN FRANCE AUX GRANDS FORTUNÉS » : loi par laquelle les grands fortunés et fortunées ne soit pas plus taxés/taxées (imposé/imposées fiscalement) pour qu’ils reviennent en France, avec la condition qu’ils investissent en France, qu’ils recrutent en France en créant des emplois et qu’ils créent des autres prochaines entreprises seulement en France ; 
15)La Loi « FRANCE 2018 PARADIS FISCAL » : loi par laquelle à partir de janvier 2018 LA FRANCE DEVIENNE pour une période temporaire de temps et expérimentale un PARADIS FISCAL afin d’ATTIRER VITE (immédiatement) TOUTES LES INSTITUTIONS BANCAIRES ET TOUTES LES AUTRES ENTREPRISES ETRANGERES ET LES ENTREPRENEURS (Businessman et Businesswomen) étrangers qui, à présent, veulent partir (quitter) vite de la Grande Bretagne, de Catalogne et d’autres pays et afin aussi de faire revenir en France tous les patrons français et étrangers partis à l’étranger.
Vive LA France, Cordialement, Adriana ANDRONIC

Score : 0 / Réponses : 0

----

Oui aux pétitions en ligne mais attention à:
- l'authenticité des contributions,
- le changement des outils actuels pour avoir a le droit de voter contre!

Score : 0 / Réponses : 0

----

La modalité de validation pour soutenir cette initiative a dû découragé plus d'une personne qui soutient cette idée. C'est dommage. Bravo malgré tout de travailler en ce sens.

Score : 0 / Réponses : 0

----

Prendre les pétitions en compte quand elles atteignent un  nombre important - 500.000 - pour le CESE pour mettre le sujet à l'ordre du jour.

Score : 0 / Réponses : 0

----

Le Sénat comme instrument efficace de l'initiative citoyenne. 

Il faut transformer le Sénat pour en faire une assemblée véritablement représentative de toutes les composantes de la société française. Une assemblée qui serait complémentaire de l’Assemblée Nationale (dont les députés semblent parfois déconnectés de la vie réelle). Les sénateurs seraient ,pour moitié, de “simples” citoyens tirés au sort et représentant l’ensemble des catégories socio-professionnelles. Un quart serait constitué d’élus locaux, tirés également au sort. Le dernier quart serait formé de représentants syndicaux (salariés et patronat). 

Les nouveaux sénateurs, véritablement représentatifs des Français,  pourraient régulièrement imposer un ordre du jour aux députés et donc jouer le rôle de lanceurs d'initiatives citoyennes.

Score : 0 / Réponses : 0

----

En 2017, je trouverai normal qu'à partir d'un certains seuil de signatures, le Parlement examine les pétitions proposées en vue de créer une proposition de loi ou amender une loi existante. Je soutiens donc la position défendue par Change.org ci-dessous.
Ce travail parlementaire doit toutefois aussi tenir compte des avis des personnes qui ne pourraient s'exprimer par voie électronique en raison de la fracture numérique qui est encore bien réelle.

Score : 0 / Réponses : 0

----

Oui, il est très important que nos avis, nos attentes de citoyen-nes soient effectivement pris en considération.

Score : 0 / Réponses : 0

----

Enfin une vrai possibilité de donner du pouvoir au peuple. Vive la démocratie vive la République

Score : 0 / Réponses : 0

----

L'engagement et l'investissement dans le débat politique ne devrait jamais être une profession et rémunéré en tant que tel. Les professionnels de la politique ne sont plus des citoyens mais bien ces rats installés dans leur fromage, croqués sans complaisance, déjà au XVIIe siècle, par Jean de La Fontaine. Comment croire, au regard des avantages toujours croissants qu'ils s'octroient, que leur quête unique soit le bien commun pour la défense duquel ils ont été élus ?

Score : 0 / Réponses : 0

----

L'espoir de devenir enfin acteur dans la démocratie

Score : 0 / Réponses : 0

----

il faut, qu' enfin , nos députés (ées) qui nous représentent, prennent en compte nos idées et nos revendications.

Score : 0 / Réponses : 0

----

Pour un lobby citoyen!

Score : 0 / Réponses : 0

----

d´accord

Score : 0 / Réponses : 0

----

On nous parle de démocratie, mais avec 3 référendums en 50 ans on a des progrès à faire! Surtout lorsque le résultat n'est pas suivi (en 2005 NON au traité de constitution européenne, remplacé par le traité de Lisbonne ratifié sans referendum après modification de la constitution française... merci Sarko!). Il serait si simple de demander leur avis aux français une fois par an sur 2-3 questions précises, mais cela enlèverait le pouvoir à quelques puissants.... Mais soyons optimistes et demandons des réferendums d'initiatives citoyennes pour recentrer la politique sur des débats de fonds

Score : 0 / Réponses : 0

----

Il faudrait étudier toute pétition atteignant 100 000 signatures, en invitant les  organisateurs de la pétition à l’Assemblée nationale pour être auditionnés et travailler dans les commissions en rapport avec leur pétition quitte à en créer s'il n'en existe pas déjà.

Score : 0 / Réponses : 1

----

Quand l'on voit les dernières élections (et non pas seulement celles de cette années qui n'ont jamais été autant anti-démocratiques), il est URGENT que le pouvoir décisionnel d'envergure pour le pays (et en commençant par le pouvoir local) revienne à tous les citoyens.

Score : 0 / Réponses : 0

----

Les pétitions sont les seules possibilités pour se faire entendre, alors soyez à notre écoute.

Score : 0 / Réponses : 0

----

Bonjour, en terme de priorité, il me semble qu'il faut viser le succès.
Les clés du succès sont.
La gestion des contributions citoyennes  et leur réponses associées avec une totale visibilité sur les actions. 
la facilité d'accès aux citoyens. 
Élaborer une proposition le loi citoyenne est plus complexe que rédiger une pétition qui pourra être retravaillée par la suite et évoluer vers une proposition de loi.
Les deux priorités seraient.
Poser une question citoyenne argumentée , et  intégrer les pétitions,.
Il serait utile d'avoir plus d'informations sur les débats déjà planifies a l'assemblée, de façon à pouvoir contribuer en temps utile. Il est actuellement possible de visualiser l'ordre du jour sur environ 1 mois, mais le titre de l'ordre du jour n'est pas assez significatif par rapport à son contenu.
Les domaines où pourront intervenir les citoyens doivent être clairement définis.
La qualité et sécurité du site internet (des sites si duplication au niveau régional,communal...) est importante pour donner confiance à l'ensemble des acteurs de ce nouveau dispositif.

Score : 0 / Réponses : 0

----

Peut être un début de vrai démocratie!!

Score : 0 / Réponses : 0

----

Remettre le citoyen au coeur des débats sociaux peut les réconcilier avec la politique ! Parler des droits mais surtout des DEVOIRS des citoyens peut aussi les responsabiliser pour les encourager à respecter ses concitoyens, son environnement et les espaces publics ! Tenir compte de son opinion pour pouvoir en contrepartie exiger du respect et responsabilité civique! Il est temps de responsabiliser le peuple !

Score : 0 / Réponses : 0

----

Pour une fois qu'on peut avoir notre mot à dire !

Score : 0 / Réponses : 0

----

Système qui permettrait enfin à nous citoyens, d’agir et de faire connaître nos réelles préoccupations. De faire entendre nos positions et nos idées.

Score : 0 / Réponses : 0

----

Les citoyens ne sont pas que des électeurs. Les députés, passées les élections, oublient bien vite leurs attentes.
L'obligation de prendre en compte nos avis via les pétitions est une initiative intéressante, et, j'espère, efficace

Score : 0 / Réponses : 0

----

Il est important pour ne pas dire essentiel que la parole des citoyens relayée par les pétitions soit prise en considération dans le débat parlementaire. Les citoyens démontrent qu'ils s'impliquent et souhaitent intervenir de plus en plus dans les décisions qui touchent leur présent mais aussi les générations futur.

Score : 0 / Réponses : 0

----

Indispensable!!!

Score : 0 / Réponses : 0

----

Bravo pour cette initiative en faveur de la démocratie participative!

Score : 0 / Réponses : 0

----

pour que chacun puisse s'exprimer

Score : 0 / Réponses : 0

----

agir pour une démocratie réelle, soyons acteurs !!!

Score : 0 / Réponses : 0

----

Au delà du droit de pétition à réformer, il y aurait d'autres mesures indispensables à instaurer ou réformer : 

    ● La démocratisation et la professionnalisation du Conseil constitutionnel et du CSA.

    ● Une réforme du statut des élus.

    ● La suppression de la procédure du Congrès (article 89 de la Constitution).

    ● Toute réforme constitutionnelle ne pourra plus être adoptée que par référendum (article 11 de la Constitution) à la majorité des électeurs inscrits.

    ● Le rétablissement, dans l’article 68 de la Constitution, des crimes :
        - de « complot contre la sûreté de l’État » (supprimé par la loi constitutionnelle nᵒ 93‑952 du 27 juillet 1993, neuf mois après le référendum sur le traité de Maastricht) ;
        - de « haute trahison du Président de la République » (supprimé par la loi constitutionnelle nᵒ 2007‑238 du 23 février 2007).

    ● L’instauration des référendums d’initiative populaire.

    ● La reconnaissance pleine et entière du vote blanc.

Ces points capitaux figuraient pourtant au programme de l'un des candidats à l'élection présidentielle, je vous laisse chercher de qui il s'agît...

Rendez-vous compte à côté de quoi nous sommes passés ?

Score : 0 / Réponses : 0

----

Soyons de vrais acteurs de notre démocratie.

Score : 0 / Réponses : 0

----

Il y a toutefois un problème de taille avec ces principes de démocratie directe. En admettant que l'on inscrive celui du référendum d'initiative populaire, avec pour principe qu'une proposition soutenue par — par exemple — 1 000 000 de citoyens, il me paraît inévitable que — comme ce fût le cas dans les préceptes de la démocratie en Grèce antique — des groupes de pressions (que l'on nommerait aujourd'hui lobbies) apparaîtraient immédiatement pour nous pousser à signer pour tel ou tel projet.

Mes questions sont :

    ● Quels outils, ou moyens de contrôle, pourraient être mis en place afin d'éviter ce phénomène inévitable ?

    ● Faudrait-il par exemple, avoir recours à une assemblée populaire de citoyens tirés au sort — comme lors de la constitution d'un juré d'assises — afin d'avaliser a priori les propositions ?

    ● Faudrait-il également ajouter un alinéa à l'article posant le cadre du référendum d'initiative populaire, instaurant l'interdiction et la pénalisation des groupes d'influences dans ce domaine d'action ?

Merci de vos réponses.

Score : 0 / Réponses : 0

----

Que nos représentants soient plus à l'écoute.

Score : 0 / Réponses : 0

----

l'Assemblée Nationale tout comme le Sénat, est constituée d'élus, plus ou moins réfractaires aux désirs des citoyens. Les plus novices sont confrontés à l'énarchie qui occupe les postes essentiels. Dans de telles conditions  la "démocratie à la française" élit un président, ainsi que tous les autres élus, à une minorité de fait. Cette minorité avec 20 ou 30% des voix, s'autorise avec certains articles d'une constitution fabriquée dans la période trouble de la guerre d' Algérie et de ses suites, de légiférer pour la France. C'est un abus de pouvoir, que les pétitions, même importantes en nombre, ne changeront pas.
Pourquoi et quels intérêts ces soit disant représentants, trouveraient en acceptant les pétitions citoyennes?

Score : 0 / Réponses : 0

----

je soutiens les pétitions de Change.org, Article 3 , Romain Riboldi ,Axel Dauchez

Score : 0 / Réponses : 0

----

Il y a une certaine incohérence à vouloir donner  du poids aux initiatives citoyennes alors que dans le même temps on dresse le diagnostique d'une inflation législative et que de ce fait le calendrier du Parlement s'en retrouve surchargé.  Cela me parait d'autant plus compliqué dans la mesure où les taux d'abstention aux différentes élections affichent des niveaux records. Que cela soit développé au niveau local me parait une évidence mais je doute que cela soit pertinent au niveau national. Ce droit de pétition existe depuis  1958 pour quel résultat ? Et même la modification constitutionnelle de 2008 n'a pas eu d'impact positif. 
Mais si d'aventure, ces initiatives citoyennes étaient renforcées, il me semble important que la participation des citoyens à ces initiatives découlent de leur participation aux différentes élections.

Score : 0 / Réponses : 0

----

Belle initiative qui redonnera de la force à la société civile. Espérons quelle soit retenue.

Score : 0 / Réponses : 0

----

Mettre en place une Charte des dispositifs numériques de participation, notamment pour bannir l'opacité et l'utilisation commerciale des données collectées.

Score : 0 / Réponses : 0

----

Merci de laisser la place un peu aux citoyens d'autant que cela fait 2 quinquenats que nous n'avons plus un seul référendum... Et pourtant je vous assure que beaucoup de citoyens sont plus motivés pour aider à notre démocraties que... certains politiques qui sont plus bornés à conserver cette parcelle de pouvoir. Vox populi, vox Dei.

Score : 0 / Réponses : 0

----

Oui, je participe, mais n'oublions pas non plus que plein de gens n'ont pas ou ne souhaitent pas avoir accès à internet , soit par fracture numérique sociale ( génération, jeunes qui n'ont plus assez pour se payer un abonnement etc).
Le progrès c'est avoir le choix, et non l'exclusion. Il faut donc aussi conserver les moyens normaux ( écrit, par exemple) et qu'ils soient aussi pris en compte pour les résultats. Sinon, ça devient une dictature numérique et de nouveau un problème d'exclusion sociale...

Score : 0 / Réponses : 0

----

Excellente initiative, ceux qui s'expriment doivent être écoutés !

Score : 0 / Réponses : 0

----

Tout d'abord définissons ce qu'est l'initiative citoyenne, plus qu'une simple idée relayée par un nombre suffisant de citoyens, suffisamment dispersés, il semble opportun que le projet en lui-même tienne la route. Est-ce qu'une section du conseil d'Etat ne pourrait pas être mise à disposition pour aider des groupes de citoyens à aller au-delà de la simple idée ? Ou bien n'est-ce pas à une piste de réforme du CESE dont l'activité actuelle est concentrée sur la production de rapports et d'idées par auto-saisine. 
Ne pourrait-on pas ouvrir le CESE à des citoyens qui seraient proposés par un nombre suffisant de leurs compatriotes, pour prendre 6 mois à élaborer une proposition ?
Et pourquoi ne pas avoir un ordre du jour qui soit organisé par thématiques, pour concentrer les bonnes volontés par périodes. Par exemple 6 mois pour 3 sujets comme l'éducation, la défense et l'agriclture, puis 6 mois pour la culture, le logement et l'Europe, etc. 
Sachant qu'un thème pourrait faire l'objet de l'examen de 5 idées, ce qui fait donc 3 x 5 = 15 idées tous les 6 mois.

Score : 0 / Réponses : 0

----

Contribution collective sur les initiatives citoyennes:
3 ASPECTS: 
-Fixer le cadre (qui peut juger l'initiative citoyenne?)
-Fixer des règles(comment une initiative citoyenne peut devenir une initiative  retenue?)
-Donner les moyens(comment arriver à la finalité de l'initiative citoyenne?)

PROPOSITIONS:
-Intégrer l'initiative citoyenne à l'agenda parlementaire.
-Proposition de travailler sur un theme (consultation thématique)
-Créer une dynamique En Marche pour faire une cohésion.

Score : 0 / Réponses : 0

----

Il est important que le citoyen soit au cœur des débats. La difficulté est de définir le seuil à partir duquel une pétition doit être analysée par l'AN.
Déclencher un référendum à 500 000 signature comme proposé dans un commentaire ferait que, en gros, avec un peu plus de 1 % des votants (environ 42 millions) on déclenche un référendum !!!
Il faut trouver la bonne limite, mais c'est à mettre en place.

Score : 0 / Réponses : 0

----

Toute pétition doit recevoir réponse même si ce travail est considéré comme chronophage...
La réponse doit obligatoirement être rendue publique et jugée cohérente et étayée.... et réétudiée tant qu'il n'y a pas clarté absolue.
La pétition doit également avoir un rôle de contre-pouvoir lorsque l'intérêt d'une majorité de citoyens est bafoué par une minorité...

Score : 0 / Réponses : 0

----

Etablir une plateforme numérique à l'usage des citoyens,pour s'exprimer et questionner le gouvernement ,les députés,etc...:comment ça marche les institutions;mais à partir de laquelle les citoyens pourrait être sollicites sur des thèmes identifiés et précis.
Se mettre à la portée du plus grand nombre par des éléments de langages simples à comprendre et favorisant le dialogue et la nécessaire écoute des citoyens.
Adjoindre aux projets et propositions de lois des initiatives législatives d'origine citoyenne..

Score : 0 / Réponses : 0

----

Il serait normal que les avis d'un grand nombre de citoyens soient pris en compte....

Score : 0 / Réponses : 0

----

Messieurs tenez compte de l’opinion des citoyens qui vous on nommes à travers leur vote pour l’election présidentielle ! Il est temps que le monde change ! Ne nous décevez pas encore une fois! Nous vous offrons un nouvel outil ne l’ignorez pas !

Score : 0 / Réponses : 0

----

J'encourage cette initiative qui n'a pourtant pas fait grand débat sur les supports médiatiques, car elle permet aux gens de devenir citoyen au sens propre du terme, c'est-à-dire acteur de la vie de la cité. Pour aller plus loin, j'invite les fervents de la démocratie (Dêmos, Kratos: pouvoir au peuple) de s'accaparer entièrement du processus en réclament une constituante afin d'écrire nous-même les règles de la cité et d'instaurer des tirages au sort parmi les citoyens afin d'avoir une réelle représentation dans l'hémicycle.

Score : 0 / Réponses : 0

----

Encore aujourd'hui, il y a une déresponsabilisation du "citoyen" vis-à-vis des processus de votation de lois et de prise de décision à l'échelle nationale. Pour investir le citoyen, et toute la population dans toute sa diversité d'individus, je propose la mise en place d'une assemblée citoyenne tirée au sort parmis toute la population française et qui aura pour but pendant 1 ans ou deux, d'examiner et de discuter les projets de loi et les propositions de lois, pour donner un avis favorable ou défavorable avec justification et prise en compte de l'ensemble des citoyens qui compose l'assemblée citoyenne. En cas d'avis défavorable, le projet de loi et la proposition de loi doit revenir à l'assemblée nationale et au sénat pour être discuté de nouveau, avec les explications et les arguments de l'assemblée citoyenne. Une dernière navette sera permise vers l'assemblée citoyenne qui validera ou non le projet de loi ou la proposition de loi. Si il y a un refus de validation, malgré le vote de l'assemblée nationale et le sénat, alors le projet de loi ou la proposition de loi est adoptée "temporairement" pendant 2 ou 3  ans et fera l'objet d'un suivi par l'assemblée nationale ou tout autre assemblée ou commission pour évaluer les conséquences et les avis de la population sur ces nouvelles lois. A l'issu de ce suivi, l'assemblée nationale et l'assemblée citoyenne devront ensemble décidé d'une nouvelle votation ou non des lois mise en application "temporairement". 

Cette mise en place d'une assemblée citoyenne tirée au sort nécessitera de changer la constitution.

Les citoyens tirés au sort seront rémunérés et pourront refuser de prendre part à l'assemblée citoyenne en cas d'impossibilité ( modalités à voir )

Score : 0 / Réponses : 0

----

Il me semble nécessaire que ces plateformes ne soient pas uniquement numériques pour que tout français, quel que soit son origine sociale et sa situation géographique, puisse profiter de cette innovation. Pour cela je propose de mettre en place des panneaux interactifs sur les places des villages ruraux, des banlieues. Un moyen de sensibiliser et d'atteindre un public qui est de base oublié par ce type de consultations.

Score : 0 / Réponses : 0

----

On si perds dans tous ces commentaires, propositions, etc.. Il en ressort tout de même un point central le désir du Peuple de vouloir reprendre ces droits de citoyens afin qu'une politique juste soit mise en place et non la continuité de la gabegie organisée.
Pour cela il faut le faire savoir, sans syndicalisme, sans appartenance de parti, un mouvements citoyens serein et déterminé à faire changer les choses puisqu'ils ne nous entendent pas. N'oublions pas les abstentionnistes que nous sommes aussi peut-être parmi nous tous.

Cdt

Score : 0 / Réponses : 0

----

il faudrait 
- créer une plateforme dédiée
- ouvrir le choix pour chacune des initiatives citoyennes évoquées : référendum, propositions de loi, questions au gouvernement, pétition ..
- communiquer pour permettre un accés simple à tous
- Informer en retour du résultat 

Charte de communication et modérateur indispensables
Délai précis par ex en cas de travail sur une proposition de loi ..

Score : 0 / Réponses : 0

----

Proposer dans le cadre de la réforme du bac à l'horizon 2021  en combiné avec les TPE une formation numérique et citoyenne aux Lycééns, voire l'intégrer aux cours d'éducation civique avec des quizz à valider afin de les former à l'essentiel de leurs devoirs et obligations de citoyens, mais également de les informer quant à leurs droits et les moyens mis à leur disposition (plateformes de consultation,  pétitions....) afin de les faire valoir.

Je trouve également que si l'on doit utiliser des plateformes de pétitions en ligne, on doit pouvoir créer de nouveaux outils. Les plateformes actuelles générant énormément d'argent via divers canaux (publicité, partenariats, pétitions sponsorisées, revente des données aux publicitaires..) sans pour autant faire avancer réellement les causes pour lesquelles le pétitions ont été créées. Je  propose donc que l'on mette en place une plateforme de pétition citoyenne d'état ou les revenus générés par les pétitions soient attribués à la défense de ces causes citoyennes ( hors frais de fonctionnement de la-dîte plateforme).

Abaisser le seuil de prise en compte d'une pétititon il faut à l'heure actuelle 1/5 des parlementaires et 4,5 millions de participants, peut étre qu'un seuil de 2,5% des inscrits sur les liste électorales et 1/10 des parlementaires serait suffisant.

Enfin et cela à sûrement été dit préalablement une solution de vote et de consultation par blockchain comme il en existe en Amérique Latine ( où de nombreux pays sont intéressés par cette solution mise en oeuvre recemment lors d'un référedum) où en Estonie.

Score : 0 / Réponses : 0

----

Les pétitions  sont des outils formidable. Et les référendums d'initiatives populaires devraient pouvoir exister de même. A défaut d'avoir une portée normative ou abrogative, il pourrait au moins peser par leur portée consultative. Les récents événements de Catalogne mettent la question directement sur la table. Le problème reste de trouver un processus qui rende significatif le résultat par l'assentiment général sur la méthode de référence et la valeur légale des participations. Cette consultation pour être valable doit être proposée à tous les français avec la mise en place d'un travail de co construction et de pédagogie sur le contenu de façon que chacun s'exprime en connaissance de cause. C'est un travail de plus longue haleine mais qui e vaut sûrement la peine.

Score : 0 / Réponses : 0

----

Il est important que tous puissent prendre en considération les thèmes et les origines des initiatives citoyennes : Il faut des cartes lisibles pour en localiser l'origine, le thème et la nature des interactions en cours avec les parlementaires.

Score : 0 / Réponses : 0

----

Le site de l'Assemblée Nationale pourrait permettre via une plateforme, telle celle-ci, de proposer des idées, auxquelles pourraient répondre tout citoyens et parlementaires.

Score : 0 / Réponses : 0

----

Les initiatives citoyennes comme celle ci ne sont pas suffisamment  prises en compte   https://www.mesopinions.com/petition/politique/desenclavement-communes-vallee-lathan-roumer-37/11449

Score : -1 / Réponses : 0

----

**Loi d'initiative citoyenne** (0) :

Cette proposition est plébiscitée sur cette plateforme. Voyons selon quelles modalités elle devrait être inscrite dans la Constitution (je fais la synthèse d'autres messages).

Expression des opposé.e.s : pour que seules les propositions davantage soutenues qu'opposées soient examinées, il faut préférer **un vote** (en ligne a priori, mais aussi possiblement en mairie) sur les propositions citoyennes (1), **plutôt qu'une pétition**. Je propose le **jugement majoritaire** (2), qui permet aux citoyens de donner leur avis fidèlement (une échelle de sept valeurs allant de  "À rejeter" à "Excellent").

Quotas de signature : ne sachant pas combien de citoyens participeraient à des pétitions si elles étaient suivies d'effet, il est difficile de choisir un seuil qui déclenche un nombre raisonnable de procédures législatives. La solution est d'**ordonner les propositions et que le Parlement étudie les plus appréciées en premier** (3). L'agenda des sessions parlementaires déterminerait le nombre de propositions étudiées.

Amendements :  trois procédures d'amendements sont possibles, et compatibles entre elles. La première consiste à améliorer collectivement le texte en ligne sur la plateforme (avant et/ou après le vote en ligne) (4). La deuxième consiste à faire amender le texte par une assemblée de citoyens tirés au sort (5) ; et la troisième, par le Parlement (6). Nous pourrions voter (au jugement majoritaire) pour choisir la combinaison souhaitable de procédures.

Déclenchement ou non d'un référendum : je pense que **les lois d'initiative citoyenne doivent être adoptées par la même procédure que les autres propositions/projets de loi** (7). Cela dit, je suis favorable à ce que les lois importantes soient adoptées exclusivement par référendum (8). Le rôle du Parlement consisterait alors essentiellement à amender les textes, et celui-ci serait incité à accoucher d'une version acceptable par la majorité. Un référendum annuel pourrait avoir lieu, pour voter sur toutes les lois importantes de l'année ; les lois jugées peu importantes par le Parlement étant rassemblées dans une ultime question du référendum.

Initiative législative : j'irais encore plus loin, en proposant que l'initiative des lois se fasse exclusivement selon la procédure décrite précédemment (9) : si le gouvernement ou un parlementaire veut proposer une loi, il devra la proposer sur la plateforme, et obtenir suffisamment de votes favorables pour que la proposition soit bien classée.

Problème : les points (8) et (9) posent un risque concernant le budget, car l'issue du référendum contraint le budget de l'État, par exemple en prévoyant plus de dépenses que ne le souhaiteraient les citoyens. Une solution est de maintenir l'interdiction de proposition de loi avec un coût positif (article 40 de la Constitution), il faudrait alors autoriser le gouvernement à émettre de tels projets de loi, comme actuellement (10). Ou bien, les parlementaires auraient à leur charge de combiner les propositions de lois de l'année dans plusieurs budgets possibles (dont chacun inclurait certaines propositions et en exclurait d'autres), à charge aux citoyens de départager entre les différents budgets lors du référendum (11). Pour toutes les propositions de loi non coûteuse, les points (1) à (9) peuvent être appliqués sans problème.
**Vous pouvez utiliser la numérotation de mes propositions dans les commentaires.**

Enfin, notons que [79% des Français](http://adrien-fabre.com/sondage/resultats.php#_ed) pensent qu'il faudrait faire voter les réformes importantes par référendum, et 84% pour voter davantage sur des propositions que sur des personnes.

Score : -1 / Réponses : 4

----

Il existe le Conseil économique social et environnemental.
A partir de là, il est dommage que cette institution ne puisse pas présenter de propositions de lois après étude sérieuse des besoins dans plusieurs domaines.
Il faut aussi qu'on arrête sérieusement avec la modification continuelle de la loi en France : nous avons plus besoin d'une loi claire, compréhensible, facilement "exécutable" et surtout réfléchie.
On vit tout simplement dans une forme d'insécurité totale en France : alors que nul n'est sensé ignoré la loi, aucun n'est capable aujourd'hui de connaitre la loi...
Il faudrait que des initiatives citoyennes puissent signaler à leur élu que tel ou tel texte n'est pas compréhensible et devrait être modifié. Par exemple que l'on prenne un seuil de "signalements" à 1 million.

Score : -1 / Réponses : 0

----

Au niveau national, une première mesure serait de donner aux citoyens le droit d’inscrire à l’ordre du jour du Parlement une proposition de loi ou une question, liée à au bien commun, à l’intérêt collectif : environnement, transition énergétique, aménagement du territoire, santé, transparence ….
Dans ce cadre, des mouvements citoyens, des associations pourraient être fédératrices et porteuses du projet.
Parmi les conditions d’éligibilité de la pétition, il y aurait la signature de plus de 1% des électeurs soit 500.000 voix, avec un seuil minimum au niveau de chaque région administrative pour s’assurer de sa bonne représentativité nationale.

Score : -1 / Réponses : 0

----

Dans 26 pays sur 28, le mode de scrutin aux législatives est la proportionnelle avec mixité ou non avec l’uninominal majoritaire.
Le scrutin proportionnel offre la meilleure représentation de la diversité d'opinion des électeurs, les Partis sont équitablement représentés.
En revanche, il a des difficultés pour former un gouvernement de coalition qui gouverne, et celui-ci peut facilement être renversé et on tombe dans les travers de la 4ème République.
Les élections de 1986 se sont déroulées pour la première fois sous la 5ème République intégralement au scrutin proportionnel à un seul tour. Elles ont provoqué une première « cohabitation».
Une dose de proportionnelle aux élections législatives de 10 ou 20% est une mesure raisonnable de démocratie vraiment représentative et responsable.

Score : -1 / Réponses : 0

----

Un débat a lieu entre liberté d’aller voter ou non, droit et/ou devoir citoyen…
En complément, il faut noter que les forts taux d’abstention délégitiment les élus qui gagnent les élections avec un % très faible des inscrits. Les citoyens et électeurs ne se considèrent pas correctement représentés.
Un sondage Louis Harris d’avril 2015 montrait que 67% étaient favorables à l’instauration du  vote obligatoire s’il était  assorti de la reconnaissance du vote blanc, au même titre que les autres votes exprimés.  54% étaient favorables à des sanctions si le vote devenait obligatoire en France.
Rappelons que le vote obligatoire est instauré en Belgique et que le taux d’abstention est de l’ordre de 10%.
Le vote obligatoire avec comptabilisation des bulletins blancs dans les suffrages exprimés est une mesure pour relancer, rénover la démocratie française et remotiver l'intérêt des Français pour la politique clé de toute décision.

Score : -1 / Réponses : 1

----

je pense que la constitution est l’affaire des Français, des électeurs. Donc toute modification doit obligatoirement et uniquement passer par un référendum. Ainsi, après une campagne courte d’information, chacun prendra ses responsabilités et la majorité l’emportera . Comme cela, on évitera les votes de posture des partis politiques

Score : -1 / Réponses : 0

----

Des débouchés politiques aux initiatives citoyennes doivent être envisagés et sont nécessaires au processus législatif, mais il convient de prévoir des mécanismes de contrôle. Il y a plusieurs écueils à éviter.
Le premier porte sur les sources et l'origine de l'initiative. Sur de nombreuses plateformes participatives, des militant-e-s proches de partis et mouvements politiques organisés et souvent extrémistes peuvent prendre une place importante, et le faire avec d'autant plus de motivation qu'il y aurait un débouché possible sur le plan de politique, médiatique et institutionnel. Ce risque ne peut être négligé, il est difficile à prévenir.
Dès lors, il ne faut pas non plus donner une impression que certaines paroles sont légitimes, et d'autres moins, en fonction d'où elles viennent. Pour éviter ces écueils, en cas de mobilisations citoyennes, il pourrait être intéressant de proposer un mode d'expression public reconnu, mais un mode d'expression pondéré de paroles complémentaires, nuancées, voire antagonistes. Il faut également que cette prise de parole soit conforme à une charte de valeurs, de respect des principes républicains, et de non-discrimination. Il faut enfin que cela se fasse dans un cadre de transparence exigeante, s’appliquant d’ailleurs dans des termes identiques à tous les acteurs intervenant sur la rédaction de la loi.
Peut-être, pour éviter des dérives, est-il nécessaire de circonscrire aussi ces expressions à certains domaines. Ainsi, dès lors qu’un mouvement s’organise contre les droits d’une partie de la population, cette demande ne serait pas recevable. Il s’agit d’éviter que les mouvements contre le mariage pour tous, contre l’avortement, contre les droits des migrants, contre certaines religions et autres, puissent trouver une place excessive et légitimée dans l’arène politique et médiatique. Il pourrait être aussi imaginé une forme d’irrecevabilité financière à certaines mesures, à l’image de ce qui existe pour les parlementaires.
Un autre risque est celui de la privatisation de l'expression citoyenne. C'est déjà partiellement le cas avec des plateformes de pétitions en ligne qui peuvent fonctionner dès lors que quelqu’un paye. Il convient d'être vigilant à ne pas donner la parole à celles et ceux en capacité de payer, et d'encadrer le fonctionnement et l'éthique de ces plateformes militantes. L’Assemblée nationale doit être vigilante à contrebalancer ces déséquilibres.
Enfin, dans tous les cas, si l’Assemblée nationale décide d’organiser l’expression citoyenne, il est indispensable que cette expression ait des effets. Elles ne peuvent être systématiquement rejetées, avec plus ou moins de bienveillance, sans quoi la seule conséquence sera une aggravation de la défiance pour le système politique.

Score : -1 / Réponses : 1

----

Comment s'articule le travail de consultation que tout député est sensé effectuer sans sa circonscription avec cette plateforme ?
Celui-ci peut remonter des problèmes qui sont locaux, particulier à sa région.
Je propose que chaque intervenant mentionne la circonscription d'élection de son député pour qu'un lien puisse se créer entre les électeurs et leur représentant.
Pascal le RUDULIER : 5ème circonscription Saône et Loire.

Score : -1 / Réponses : 0

----

Bonjour,
Favoriser la participation active des citoyens passe par un recentrage de la vie politique autour des préoccupations singulières mais aussi globale de chaque français. Pour ce faire il me paraît important dans un premier temps de replacer le débat politique au niveau des politiques publiques locales (commune, département et région) en instaurant des consultations citoyennes actives et décisifs. Il me paraît centrale également de retisser le lien entre le député et ses concitoyens de circonscription  en permettant les échanges en direct, par voie numérique,
Mais aussi et surtout en favorisant la consultation de ces citoyens par le député. Député qui ne serait qu un porte parole des décisions prises, des choix votés par son canton et qu’il portera à l’assemblée.
Aussi et pour finir, un conseil citoyen, à
L’image d un conseil d état, ayant un pouvoir consultatif pour l’elaboration Des textes législatifs  et de vote  des lois passant à lassemble nationale (1 citoyen = 1 vote ).

Conseil représentant de manière proportionnelle les résultats des
Votes au législatives. Citoyen donc inscrit sur les listes électorales et d un parti politique. 

Cdt

Score : -1 / Réponses : 0

----

Quelques idées (dissociées en plusieurs contributions) partant du principe qu'actuellement, le seul grand moment d'expression des citoyens est l'élection, alors qu'il faudrait mettre en place des canaux de discussion continus entre les citoyens et leurs élus.

La proposition est de mettre en place une plate-forme numérique permettant à un député de consulter ses électeurs sur les différents articles d'une loi.

L'idée est que le député soit au courant de ce que pensent ses électeurs de chaque article de loi. Il n'est pas contraint de suivre l'avis formulé (notamment s'il y a un biais parce que peu de contributeurs par ex.), mais cela se ressentira à la fin de son mandat.

La plate-forme doit donc contenir la proposition de loi article par article (si possible mise à jour en continu en fonction des différents amendements). Le député pourra y inscrire son avis sur chaque article. Les citoyens de sa circonscription pourront voter pour ou contre chaque article, et éventuellement laisser un commentaire. Différents tableaux de bord permettront de suivre:
- pour chaque article les votes des citoyens (pour ou contre) par circonscription ou au niveau national
- les votes des différents députés
- le taux "de suivi" de chaque député des votes exprimés par ses citoyens

Score : -1 / Réponses : 0

----

Quelques idées (dissociées en plusieurs contributions) partant du principe qu’actuellement, le seul grand moment d’expression des citoyens est l’élection, alors qu’il faudrait mettre en place des canaux de discussion continus entre les citoyens et leurs élus.
La proposition consiste, sur le modèle de certaines municipalités (Paris par ex.), à imposer à chaque assemblée élue (conseil municipal, conseil général, conseil régional,  ...) à attribuer une partie de son budget d'investissement à un (ou plusieurs) projet(s) décidés par les citoyens (pourcentage à définir).
L'idée est que les citoyens peuvent pousser à la réalisation de certains projets qui leur paraissent plus importants et/ou urgents que ceux des élus.

Score : -1 / Réponses : 0

----

Au risque de ne pas être en accord avec tous, je suis pour ma part assez circonspect sur le principe de la "démocratie participative" généralisée. Elle conduirait à un "brouhaha politique", multiplierait les initiatives d'opposants (de tous bords) aux gouvernements en place pour de simples raisons politiques ; bref elle bloquerait encore plus un "système" déjà très lourd et une administration à grande inertie que nous connaissons (et dont on lui fait reproche régulièrement)... Les gens sont élus sur un programme, la démocratie s'est déjà exprimée. Il est alors facile à des opposants politiques d'obtenir un certain nombre de "clics" (ou signatures) pour tenter de revenir sur un point de ce programme. Et nous entrerions alors dans une ère de "double démocratie" où nous ne ferions plus que voter ; c'est la promesse d'un blocage absolu. Et je ne parle même pas des coûts d'organisations d'élections.
Si "referendum" (y compris d'initiative populaire) il doit y avoir à mon sens, c'est sur des sujets d'importance nationale seulement (à définir), et qui n'ont pas été abordés par les candidats lors des campagnes électorales.
Pour que notre pays avance un peu, il faut je crois au contraire réduire les élections intermédiaires (aux présidentielles) qui "paralysent" à chaque fois pendant des mois. Alors pourquoi pas (un peu à l'américaine) un "regroupement global électoral" (hormis les européennes) ? Le même jour on vote pour le Président, les sénateurs, les députés, les Maires, les conseillers départementaux et régionaux... Pour cinq, six ans (peu importe mais six me paraît mieux), et sur des programmes précis et détaillés (bien plus qu'aujourd'hui). On peut même imaginer dans le même temps (au niveau régional) un vote sur des grands projets prévus (la construction ou pas de tels ou tels travaux, investissement, etc...). 
Triple avantage démocratique : on donne un vrai sens à la politique nationale et régionale dont veut le pays, la démocratie s'exprime et on laisse le temps aux élus qui l'ont été sur un schéma précis - On ne paralyse plus le pays par les élections intermédiaires  - Et on règle enfin un problème récurrent, la participation électorale : car là, les gens se déplaceront (ils en auront même hâte !)... 
Et si on n'est pas content ensuite ? Et bien la démocratie s'exprimera de nouveau dans six ans.

Score : -1 / Réponses : 4

----

L'AMENDEMENT D'INITIATIVE CITOYENNE : Chaque citoyen pourrait proposer à l'Assemblée nationale un amendement à une proposition de loi, parrainé par un député. L'amendement devrait recueillir un certain nombre de signatures pour être présenté en Commission ou en plénière. Cela se ferait via une plateforme dédiée.

Score : -1 / Réponses : 0

----

Faire des propositions de résolution au nom du peuple à l'Assemblée Nationale.

Score : -1 / Réponses : 0

----

Il faudrait organbiser plus de référendums d'initative locale pour des projets locaux avec saisine du Maire au dessus d'un nombre d'electeurs à définir et filtrés par une commission pour evacuer les idées farfelues.

Score : -1 / Réponses : 0

----

En préambule, peut-être faut-il revenir à la source et déjà déterminé ce qu'est un citoyen, ou tout du moins en matière numérique comme définir que l'auteur est un citoyen.
en l’occurrence, rien que sur cette plateforme 3 possibilités d'identification et aucune à même de garantir l’identité de l'auteur. et pourtant nous disposons tous d'un identifiant unique le NIR.
pour conclure ce préambule, nécessitant de la définition d'une identité numérique du citoyen qui, de mon point de vue, relève du pouvoir régalien.
Une incidente: une initiative citoyenne doit -elle s'ouvrir explicitement à l'intervention d'acteurs pouvant constituer des regroupements de citoyens? association, entreprises et plus globalement lobby. doit-on clairement identifier leurs contributions comme telles ou les interdire et les laisser recourir à des water army qui vont paraphraser la même proposition et tronquer en un sens le débat. personnellement je suis en faveur de la première option.
enfin le coeur du sujet,  les initiatives citoyennes. le principe toute proposition sous forme de pétition recueillant, sur une plateforme à définir, un nombre de votes/likes/réponses supérieur à un nombre pouvant être considéré comme représentatif statistiquement et géographiquement devrait être porté par le parlement dans les mêmes conditions qu'une proposition de loi.
De mon point de vue, les questions citoyennes au gouvernement pourrait suivre la même règle dans le cadre des questions orales au gouvernement. à ce titre, elle serait porté par un député lors d'une session hebdomadaire, ledit député étant à désigner par le président de l'assemblée.
les données associées à cette plateforme devront relever de l'open data et permettre un contrôle citoyen afin d'éviter toute éviction de proposition
c'est l'état de ma réflexion sur le sujet.

Score : -1 / Réponses : 0

----

PARTICIPATION AU PROJET DE LOI : Possibilité aux citoyens de donner leur opinion sur un projet de loi avec pour base une argumentation prenant en compte différents facteurs ; facteurs qui pourraient être affinés sur la base de cette participation citoyenne.
Une deuxième phase, donnant lieu à une synthèse des trois principales argumentations retenues, suivi d'un vote accessible à ceux qui ont contribué à la première phase.
Un rapporteur de cette consultation pourrait exposer l'argumentation dégagée sous la forme d'une question lors d'une séance à l’Assemblée nationale.
Enfin un compte rendu publique de la question et sa réponse par un ministre du gouvernement en fonction des projets de loi.

Score : -1 / Réponses : 0

----

Je vous recommande la page FB "observatoire des IC et pétitions à l'international" aussi connue sous l'entrée "green ECI" dont je tire ce texte suggestif (attendez... Je vais le chercher)...

Score : -1 / Réponses : 0

----

il faut viser un mécanisme, simple, accessible, médiatique et donc mobilisateur. La question citoyenne au gouvernement répond à cet objectif.

Score : -1 / Réponses : 0

----

question citoyenne au gouvernement

Score : -1 / Réponses : 0

----

Avant la législation de certains loi, l'assemblée nationale dois donner l'opportunité pour les citoyen de faire  leur débat ou contribuer leur opinion  dans le sujet et avec la résultat de sondage ou débat Ça va donner un idée àu parliament la réaction ou l'effet de la loi proposée ou comment la loi doit être construit pour la bénéfice de les citoyene et la France.

Score : -1 / Réponses : 0

----

Tous les mois cela suffirait; il faut le temps de les faire sélectionner! par les citoyens bien sûr!! Il faut étudier comment car il peut y avoir des dépôts en masse pour paralyser le système;
il y a plus urgent quand même.. pouvoir , comme en Suisse ou en ITALIE, imposer un référendum pou TENTER d'abroger une loi estimée inappropriée ou injuste!!

Score : -1 / Réponses : 0

----

Faire entrer le numérique dans l'hémicycle ! Au moment des questions au  gouvernement on réserve un moment ou  un robot par la voie d'un écran relaie les pétitions qui ont eu le plus de succès sur une plateforme indépendante ! Ce qui permet d'introduire de la représentativité direct dans l'hémicycle !

Score : -1 / Réponses : 0

----

En ce qui concerne l'initiative gouvernementale je propose d'introduire une consultation populaire sous forme de référendum consultatif en ligne après débat entre le gouvernement et les partenaires sociaux compétents (sur la question objet de l'initiative) en direct sur le site de l'assemblée nationale !

Score : -1 / Réponses : 0

----

En ce qui concerne l'évaluation de la mise en oeuvre des lois ! Je propose un débat entre les différents acteurs (représentants du gouvernement ou du parlement et partenaires sociaux) avec un moment de réponse aux questions populaires qui ont eu le plus de succès (sur une plateforme en ligne de questions avec notation de ces questions par le peuple)  et après le débat  un vote sous forme de référendum en ligne avec la question simple : approuvez- vous la mise en oeuvre ou non ! Si le résultat est non le représentant légal du gouvernement ou parlement devra représenter dans un délai décidé lors du débat un nouveau plan prenant en compte les remarques du débat ! Les personnes qui participent a ce genre d'initiative populaire pourraient le faire avec un identifiant lié à leur papier d'identité !

Score : -1 / Réponses : 1

----

Proposez à chaque citoyen de voter chaque loi sur ce site , et lorsque qu'un quorum de par exemple 1/5 de la population majeure a voté , alors la position majoritaire est adoptée.
exemple : Moralisation de la vie publique : 1/5 de la population a voté et il y 51%  de contre , en l'état , la loi n'est pas adoptée

Score : -1 / Réponses : 0

----

Je pense qu'il faudrait avoir deux possibilités offertes aux citoyens : 
1 - La possibilité d'une initiative populaire, en réformant l'article 11 de la Constitution en obligeant le Parlement ET le gouvernement à se prononcer sur des propositions citoyennes avec 1/20 des électeurs inscrits sur les listes électorales
2 - Un veto citoyen, avec le même quorum, possible dans un délai donné (ex : pendant les débats de l'assemblée)

Score : -1 / Réponses : 2

----

Nombre de membres : il est fonction du schéma simplifié des couches sociales d'une part, de l'importance du territoire d'autre part. Ce nombre est au minimum de 3 (très petite commune par exemple) et au maximum de 50 (assemblée nationale) ; le nombre moyen pour une commune moyenne par exemple correspond au nombre de couches sociales retenues pour faire le tirage au sort soit 8 selon notre exemple précédent.
Remarque 1 : dans le cas d'une très petite commune il est quasi impossible de respecter la représentation de chaque couche sociale et même la parité homme – femme sur un mandat. Dès lors la parité homme_femme et la représentativité des couches sociales devra se vérifier sur plusieurs mandats selon une 'rotation' ou une 'alternance'.
Remarque 2 : dans chaque commune, le tirage au sort se fera par rapport aux classes sociales existantes de la commune. Par exemple s'il n'y a ni chômeur, ni retraité dans une commune, le tirage au sort portera sur les classes sociales restantes. De même si une commune ne comportait que des femmes, la parité homme-femme ne peut plus être respectée.
Remarque 3 : toute commune d'importance, une communauté de communes, un département, une région, la nation est réputée contenir toutes les couches sociales et vérifier la parité homme-femme.
Remarque 4 : 50 membres maximum pour une grande institution peut sembler peu. En fait c'est largement suffisant pour que le débat ne s'éternise pas lors de l'élaboration des résolutions. C'est également suffisant pour faire exécuter ces propositions et communiquer les résultats aux citoyens du territoire. En effet, les membres sont une structure de pilotage qui s'appuie sur des fonctionnaires spécialisés et compétents de l'institution.

Durée de travail dans le mandat : dans la plupart des cas les membres sont à temps partiel dans leur mandat ; néanmoins pour de grandes institutions, le mandat peut exiger un temps plein. La durée du mandat est proposée au vote des électeurs qui en décident au final.
Le mandat à temps partiel est compatible avec le maintien de l'actuelle activité du membre tant que le taux de ce temps reste < 50 % . Au delà il semble intéressant de passer au mandat à temps plein. Mais le mandat à temps plein exclue toute autre activité du membre et ne peut convenir qu'à des personnes qui acceptent de quitter totalement leur activité durant la durée du mandat.

Durée du mandat : 3 ans en rotation ; toutes les institutions ne changent pas de membres par tirage au sort en même temps ; tous les membres d'une institution ne sont pas renouvelés en même temps mais au contraire en rotation.
Remarque 1 : à l'initialisation du processus on tirera au sort 1/3 des membres, plus un autre 1/3  l'année suivante et ainsi de suite pendant 3 ans. La 4ème année le premier 1/3 sera renouvelé et ainsi de suite.
Remarque 2 : tout départ de membre pour faute grave, maladie grave ou décès, rejet par les élus,  est remplacé immédiatement. Sauf, cas de cette nature, tout membre reste en fonction 3 années au plus.

Rémunération des membres : les membres sont subventionnés par l’État à hauteur de leur rémunération avant mandat et au prorata du temps effectué. Les membres à temps plein retrouvent de plein droit leur job à l'issue de leur mandat. Les membres à temps partiel gardent leur activité et peuvent voir leur rémunération actualisée en fonction d'une éventuelle augmentation de salaire durant le mandat.
Les membres retraités avant mandat gardent leur retraite entière et ne sont pas subventionnés par l’État. Les membres actionnaires avant mandat ne sont pas subventionnés par l’État.

Frais des membres : pour l'exécution du programme, les membres peuvent avoir des frais ; ces frais sont remboursés sur justificatifs et sont plafonnés. Tout dépassement de ce plafond ou toute justification falsifiée, erronée ou manquante est considérée à priori comme une faute grave pouvant être sanctionnée au minimum par une destitution demandée par les électeurs (5%).

... A suivre dans commentaire suivant

Score : -1 / Réponses : 0

----

Pourquoi pas inscrire "Le Droit à une consultation participative des citoyens et des citoyennes qui serai requis à chaque Quinquennat", dans la Constitution  au niveau de l'article 3 qui dit que : La souveraineté national appartient au peuple qui l'exerce par ses représentants et par la voie du référendum et dont également par la voie d'une consultation participative à travers la plateforme numérique du gouvernement.

Cela accompagné d'une modification de la carte électorale - vers un passeport électorale dé l'âge du recensement : 16 ans afin de faire participer l'ensemble des citoyens français et limiter cette progression dans cette société du refus de voter et de se prononcer sur le choix de nos représentants politique (qui va s'amplifier dans le temps avec cette Présidentielle 2022).

Score : -1 / Réponses : 1

----

Il serré plus judicieux que se soit l assemble qui demande à un panel de citoyens  au hasard leur avis sur une nouvelle loi où il y a t il une loi à faire sur tel  sujet car pour moi vous croulerez sous le nombre de propositions

Score : -1 / Réponses : 1

----

Ça peut sembler prétentieux mais j'ai pondu il y a quelques temps un document sur ce sujet, qu'on peut découvrir ici :
http://yves.carl.pagesperso-orange.fr/CPIC/Principc-1.html
Je le communique ici, pour le cas où quelques uns de ses ingrédients seraient exploitables pour la réflexion actuelle. Je pense en particulier au filtre préalable des medias agréés pour que les propositions les plus farfelues ne viennent pas saturer de bonnes résolutions.

Score : -1 / Réponses : 0

----

De nombreux scientifiques affirme que la civilisation dans laquelle nous vivons est insoutenable et va certainement s'effondrer. Le réchauffement climatique, le declin des energie fossiles et des matiéres premières, les inégalités dans le partage des richesses, l'erosion des sols, et bien d'autres menaces comme la malbouffe et le retour des maladies sont des problèmes interdépendants qui prennent sens lorsque les analyse dans un cadre systémique. Le modéle du systeme terre standard world3 élaboré par des chercheurs du MIT révelé par le club de Rome en 1972 est conforme à la réalité depuis sa création et prévoit le début des grands retournements de tendances en 2020-2030. A l'instar du changement climatique, il est trop tard pour inverser la tendance et le déclin.
A l'heure des politiques à court terme et des lobbys, comment se préparer ? En favorisant l'emergence de micro-systèmes autonomes et résilients, les plus à même de survivre aux crises successives et à l'effondrement qui se profile.  Plus d'informations et une demonstation sont disponibles sur l'effondrement en suivant cette breve conférence de l'Adrastia http://shorttt.com/A4BHdk

Dans ce cadre, qu'attendre de l'AN ?  De rendre le pouvoir aux citoyens grace a une assemblée constituante tirée au sort et par des referendums d'initiative populaire.

Score : -1 / Réponses : 0

----

Une consultation locale à l'initiative du député pourrait être créée, lui permettant ainsi de solliciter l'avis des électeurs de sa circonscription à l'occasion des grandes réformes nationales non encore adoptées et qui auraient des répercussions sur son territoire.
La consultation ne porterait que sur un objet d'envergure nationale afin de ne pas interférer avec les intérêts locaux. Une telle perspective renforcerait la légitimité d'une proposition de loi déposée par un parlementaire à la suite de la consultation et constituerait un outil entre les mains des parlementaires pour convaincre le pouvoir exécutif du bien-fondé d'une proposition de loi. L'objectif étant de susciter le débat.

Score : -1 / Réponses : 0

----

Ne faudrait-il pas en premier lieu se poser la question de l'abstention avant d'instaurer un référendum d'initiative citoyenne? Quels seraient ces citoyens participant à un scrutin ou une consultation d'une pétition pour faire des propositions de lois? Ma crainte est la récupération politique voire syndicale qui pourrait en être faite. Oui la parole doit être donnée aux citoyens pour favoriser les changements. Il faut trouver des formes innovantes mais cadrées pour éviter les dérapages ou les déballages comme sur les réseaux sociaux.
Je suis de ceux qui prônent la liberté de proposer sur des thématiques précises sous forme de consultation d'un panel de Français (différent en fonction du sujet) préinscrits en fonction de leur appétence et compétences, ces Français qui ont cette envie de participer à la vie de nos institutions républicaines mais sans un engagement politique obligatoire. La création d'une plateforme numérique serait une solution à retenir pour organiser ces consultations citoyennes.

Score : -1 / Réponses : 0

----

Afin de favoriser les initiatives citoyennes et des "réflexes" de participation au débat démocratique, plusieurs mesures existantes pourraient être optimisées, d'autres venir s'ajouter aux dispositifs de création de la loi :
a) mise en place de plateformes de pétitions en lignes-applications  (en prolongement du dispositif existant) avec proposition de plusieurs modalités de traitement au-delà d'un seuil de mobilisation citoyenne (au-delà par exemple de 500 000 signataires) : mise en place de groupes de travail citoyens et parlementaires, mise en forme juridique du texte, études d'impacts, soumission des travaux au vote citoyen (avec quorum de participation à atteindre). Si ces étapes sont franchies, le texte peut-être considéré comme une proposition de loi et suivre le cheminement classique.
b) Numériser le référendum d'initiative populaire 
Cela sort du cadre, mais le processus pourrait être sécurisé par blockchain.
Enfin, si cela fonctionne à l'Assemblée, ce type de dispositif pourrait-être ensuite décliné au niveau local.

Score : -1 / Réponses : 0

----

La société civile regorge d'experts dans les associations, les groupes de réflexion , etc. Il faudrait inciter ces experts de la société civile (qui ne sont pas nécessairement médiatisés mais qui ont beaucoup de choses à dire en raison de leur expérience de terrain et de leur point de vue original par rapport à des experts ultra médiatisés) à formaliser des rapport, des textes ou des chartes (peu importe la forme) et à les transmettre au Bureau de l'Assemblée nationale et aux groupes parlementaires ainsi qu'aux commissions.

Ces rapports pourraient être mis en ligne sur le site de l'Assemblée pour permettre aux citoyens de voter en ligne sur les rapports les plus intéressants, lesquels seraient ensuite soumis à un débat en hémicycle ou bien au sein de la commission intéressée (dans ce cas, la réunion doit être retransmise publiquement). Les auteurs du rapport viendraient le présenter et discuter avec les parlementaires sur les perspectives qui sont développées. 

Cela permettrait d'enrichir l'expertise des parlementaires et éventuellement de faire émerger des propositions de loi qui viendraient alors d'un vrai besoin du terrain. 

Cela permettrait aussi que l'expertise parlementaire ne provienne pas toujours des mêmes personnes que l'on retrouve dans toutes les auditions mais de citoyens éclairés qui ne sont pas entendus, écoutés par les autorités nationales.

Score : -1 / Réponses : 1

----

Contribution collective suite réunion publique organisée par Bruno Studer, député LREM 3ème circonscription du Bas Rhin.

Forte préférence du groupe pour des plateformes de pétition en ligne plutôt que des référendums populaires jugés beaucoup plus vulnérables par rapport aux manipulations par des groupes/lobbies minoritaires mais très organisés et très motivés.

Dans le même ordre d'idée, il s'agit de laisser les législateurs faire ce pourquoi ils sont élus, c'est à dire analyser les sujets à fond avant de prendre des décisions qui vont dans le sens de l'intérêt général. 

Par contre, forte demande pour disposer de cette possibilité pour se faire entendre, notamment pour donner une voix aux catégories de la population mal représentées par des syndicats et autres groupes de pression organisées. 

Concernant les plateformes de pétition en ligne, on suggère des plateformes dédiées, gérées par les différents échelons politiques: national, régional etc.. comme celle utilisée par le gouvernement britannique https://petition.parliament.uk/

Prenant toujours exemple sur ce que fait le gouvernement britannique et d'autres, établir des seuils (nombres de signatures)  à partir desquels un sujet faisant l'objet d'une pétition doit être débattu obligatoirement par l'instance gouvernementale à laquelle la pétition est adressée. 

Si le nombre de pétitions dépassant les seuils établis dépasse la capacité de traitement de l'instance gouvernement concernée, les traiter par priorité décroissante avec un nombre minimum à traiter au cours de chaque période (année,  semestre, trimestre...)   

Dans tous les cas, établir des règles du jeu claires dès le départ et les assumer par la suite!

Score : -1 / Réponses : 4

----

SYNTHESE CONSULTATION CITOYENNE 5911
Création d’une chambre qui regroupe les idées des citoyens : des élus au contact direct des citoyens pour recenser les problématiques.
Réalisation d’enquêtes publiques consultatives et retour officiel.
Optimisation des moyens existants : réseaux sociaux, site Parlement et Citoyens : Empowerment
Coordonner les actions citoyennes sur le territoire (actions au niveau des villes, métropoles)

Score : -1 / Réponses : 0

----

SYNTHESE CONSULTATION CITOYENNE 5911

Création d’une chambre qui regroupe les idées des citoyens : des élus au contact direct des citoyens pour recenser les problématiques.
Réalisation d’enquêtes publiques consultatives et retour officiel.
Optimisation des moyens existants : réseaux sociaux, site Parlement et Citoyens : Empowerment
Coordonner les actions citoyennes sur le territoire (actions au niveau des villes, métropoles)

Score : -1 / Réponses : 0

----

Aujourd'hui, il a été prouvé que l'échelle municipale était la meilleure pour instaurer des mécanismes de démocratie participative. De nombreuses initiatives existent déjà, comme les budgets participatifs dans quelques villes comme Paris, Rennes ou Toulouse. Cependant, afin que davantage de villes voient naître ces mécanismes de participation citoyenne, je propose que l'on élabore une loi obligeant toutes les municipalités à les mettre en place.

Score : -1 / Réponses : 0

----

attention aux fausses bonnes idées. l'initiative populaire mal calibrée peut aboutir à des  surenchères génératrices d'un climat de tension publique aboutissant à une abstention de "fatigue démocratique ". oui aux débats ; oui au recueil d'avis citoyen ; oui aux "adresses citoyennes" à l'intention des représentants élus ;  oui au droit de "remontrances citoyennes". Mais laissons le pouvoir législatif à ceux qui sont mandatés . 
Il faut veiller à la responsabilité claire ,donc "traçable"   de chacun et non organiser la confusion des rôles .

Score : -1 / Réponses : 0

----

La démocratie est en perpétuelle construction. Toute initiative qui permet le respect de chaque être humain dans sa diversité culturelle ne pourra qu'être bonne pour la nature qui est dans un état castastrophique,pollution de l'air ,de la terre, de l'eau. Dû principalement à la non maitrise par le peuple de son mode de vie imposé par le pouvoir de ceux qui dirige le monde en imposant la compétition plûtôt que la collaboration.

Score : -1 / Réponses : 0

----

Une nouvelle manière de faire fonctionner la Démocratie

Score : -1 / Réponses : 0

----

Gratuit et très simple ! Pour une meilleure représentativité et plus proche de nos vrais préoccupations.

Score : -1 / Réponses : 0

----

les questionS soumiseNT à la signature citoyenne doivent être posée au gouvernement obligatoirement lors des question à l'assemblée; (Bien sur il faut qu'elle est eu un nombres de signature à définir.
Les pétions qui ont recueilli  X Milliers de signature doivent être trans mise au collision concernée et lu en assemblée plénière. Pour des propositions de réforme structurelle des centaines de millier de signature doivent aboutir, après débat à l'assemble et au sénat, à devenir questions référendaire /

Score : -1 / Réponses : 0

----

Enfin un pas en avant pour mieux nous faire entendre dans ce brouhaha médiatique. Je pense aux rares pétitions que j'ai soutenu, en particulier sur "l'aide active à mourir" de Marie Godard qui se heurte au "curseur" d'une ministre de la santé plus préoccupée par son budget que par le respect de l'opinion de 92% de la population, celle d'Anne Circé dont le violeur reste encore impuni...après tant d'années de souffrances.
Il faudra par contre veiller à ce que nos signatures ne soient pas détournées au profit d'actions politiques qui dépassent le cadre de la pétition d'origine. et qu'il soit possible de se faire entendre AUSSI par change.org pour que les créateurs de ces pétitions puissent être recadrés, retoqués... s'ils s'égarent avec nos signatures en poche. J'ai un exemple précis de dérapage avec le statut de première dame...
Donnez enfin le vrai lien entre Assemblée et Citoyens.
merci donc pour cette initiative

Score : -1 / Réponses : 0

----

Pour que nos élus soient à notre écoute.

Score : -1 / Réponses : 0

----

Les élus ne doivent pas rater les problématiques qui sont soulevées par des dizaines / centaines de milliers de citoyens

Score : -1 / Réponses : 0

----

Nous ne sommes pas qu un bulletin de vote quand les politiques ont besoin d un simulacre de democratie.
La vraie democratie se realise avec le peuple qui doit etre imperativement consulte.

Score : -1 / Réponses : 0

----

Et pourquoi pas ? celui  qui ne tente rien n'a rien !

Score : -1 / Réponses : 0

----

Si la parole des citoyens est importante, je vous l'accorde mais pas besoin d'en mettre des tartines . avec cinq lignes maxi cela suffirais amplement  pour expliquer ce que veux le peuple . place à la parole de tous citoyens ;
( La culture c'est comme la confiture plus ont l'étale  moins c'est important )

Score : -1 / Réponses : 0

----

Faisons tous ensemble que cette belle utopie démocratique puisse un jour devenir... Réalité !!!

Score : -1 / Réponses : 0

----

La démocratie populaire en discussion...

Score : -1 / Réponses : 0

----

Pourvu que ca marche...

Score : -1 / Réponses : 0

----

Voilà qui devrait permettre une plus grande participation citoyenne aux décisions politiques, qui nous concernent tous ! Parce qu'en termes de démocratie, il y a du boulot !

Score : -1 / Réponses : 0

----

Je viens juste de m'inscrire sur ce site, et il est tard, alors je me contenterai dans ce commentaire de tester le "système", par exemple en demandant à Myriam Molin de faire un effort pour différencier, sur son clavier, la virgule de l'apostrophe, histoire de clarifier ses écrits, ah! ah! ah!
Cordialement.

Score : -1 / Réponses : 0

----

De nombreuses pétitions dénoncent partout la maltraitance sans limite des animaux "chosifiés" par deux causes essentielles : le productivisme et ses cruautés abominables  dans l'élevage et l'abattage massif industrialisés ; l'indigence actuelle de l'éducation familiale et scolaire sans transmission des règles de morale de base et de savoir vivre, dont le respect. Ces pétitions doivent donc être prises en compte sérieusement pour faire évoluer positivement les mentalités et faire mieux respecter le bien-être et le droit animal, par plus de contrôles de terrain efficients et aussi à travers la justice. C'est un enjeu sociétal majeur pour une politique qui se  veut "progressiste".

Score : -1 / Réponses : 0

----

pourquoi parler de la suisse, on est en France et non pas dans un autre pays on as notre histoire et ils ont eux on la leurs, c est comme quand on se compare constamment de l Allemagne mais il non pas de SMIC eux ou il est si bas que j ai l impression que notre RSA est plus élever que leurs SMIC alors que l on ne nous comparent pas a ces pays qui on une vie autre que la notre

Score : -2 / Réponses : 0

----

On pourrait imaginer, à l’instar de ce qui existe notamment aux États-Unis, la création d’une procédure de recall (cela exige toutefois une révision de la constitution) concernant un ministre. Un ministre controversé ou qui ne satisfait pas les citoyens, ce qui est possible dans la mesure où ils ne sont pas élus, pourrait être révoqué par les électeurs.

Le recall consiste à permettre de révoquer le mandat d'un gouvernant lorsqu’une pétition demandant cette révocation a obtenu suffisamment de signatures. Dès lors que le nombre de signatures est atteint, alors le ministre doit démissionner et un autre ministre est nommé à sa place.

Il faudrait empêcher l’usage abusif du recall pour ne pas créer une instabilité politique chronique, pour cela il faudrait prévoir un nombre de signatures assez important pour ne pas que chaque ministre soit entravé constamment dans son travail par des pétitions.

Le ministre serait donc responsable tout au long de son mandat devant les citoyens.

Score : -2 / Réponses : 0

----

J'ai toujours le même regret en consultant les commentaires, je ne vois jamais aborder cette  condition qui me semble fondamentale : une véritable consultation citoyenne honnête et sincère doit s’adresser à l’ENSEMBLE des citoyens. Tant que cette condition n’est pas remplie, toutes les formes de démocratie participatives ne concerneront que les initiés qui continueront de discuter entre eux comme on le fait ici et comme ça s’est toujours fait. Alors toute une fraction de la population continuera de se sentir exclue, s’abstiendra ou votera pour les extrêmes. C’est avec cette idée que je m’efforce depuis plusieurs années de faire connaître un projet de consultation régulière des citoyens, l’OCC, que j’ai défendu dans des livres, dans un blog et grâce à une pétition qui en résume l’essentiel. 

http://pouvoir-citoyen.centerblog.net/134-la-solution-pour-aujourhui-et-pour-demain

http://www.mesopinions.com/petition/politique/consultation-reguliere-citoyens/4865

Score : -2 / Réponses : 1

----

Liste de citoyens motivés

A ceux qui souhaiteraient continuer à échanger sur nos institutions, je propose de constituer une "liste privée de volontaires motivés".
La liste ne sera communiquée qu'à ceux qui y seront inscrits avec engagement de ne pas la divulguer. Cela dès que l'opération "Consultation démocratie- numérique " sera clôturée...
Si vous souhaitez vous inscrire, merci de m'envoyer votre mail
à: bachaud.yvan@free.fr

Score : -2 / Réponses : 0

----

Remettre de la chaleur humaine et la participation: les hommes ou les femmes, allongé ou debout , la nuit ou le jour, les vieux ou les jeunes, les actifs ou les proactifs.......le numérique seul n'est pas démocratique, les statisticiens ne pourront pas transformer durablement les citoyens. Nous resterons hétérogène.

Score : -2 / Réponses : 0

----

Une planification de la démocratie avec le Monde, l'Europe , la France, ma région, mon agglo, mon village. un représentant local à chaque étage avec un seul mandat.

Score : -2 / Réponses : 0

----

Projet de conseil citoyen

40 personnes volontaires tiré aux sort qui représente  de façons proportionnel 
les résultats du premier tour des présidentiel

4 fonctions :
1. Rôle consultatif pour les projets de lois
Outils. Plate forme numérique de consultation (forum , web conférence) , 
plus action de consultation citoyenne sur le terrain (table ronde)

2. Rôle pédagogique
Emmètre des versions simplifié ou vulgarisé des textes de lois et décret
Utiliser la plate forme numérique pour mettre des vidéos d explication des lois,
et des action sur le terrain

3. Rôle de contrôle de l application des lois et de l 'efficacité de celle ci
Passé un délai d un an , toutes lois qui ne rempli ces objectif peut être renvoyé devant le parlement, après un vote positif. Le gouvernement peut accepter ou repousser la demande de 6 mois a 1 an si il estime de c est trop tôt ou que celui ci prépare une modification.
Passée ce délai supplémentaire la lois repart aux parlement . Les 40 conseillers citoyens ont droit de voter sur une révision de lois initier par le conseil citoyen

4. Rôle de proposition
Le conseil peut émettre une consultation citoyenne pour proposer une lois ou un décret , etc
Proposition qui suivra le procédé habituelle pour finir par un vote au parlement

Score : -2 / Réponses : 1

----

L'instauration du Référendum d'initiative Citoyenne doit impérativement tenir compte des gens qui n'ont pas forcement d'opinion, de l'abstention ou du vote blanc ou nul.
Il est indispensable que la majorité nécessaire soit la majorité des électeurs et non des votants ; à charge pour les initiateurs des projets de convaincre la population de voter.
La partie de la population qui veut réformer les institutions ne doit pas pouvoir imposer ses vues à une majorité de Français qui ne seraient pas prêts à voir changer les institutions sauf à conquérir le pouvoir en respectant les institutions qui offrent des garde-fous à de nombreux niveaux (Assemblée Nationales, Sénat, Présidence...)

Score : -2 / Réponses : 0

----

Je suis entièrement d'accord avec tout cela.

Score : -2 / Réponses : 0

----

Pas de référendums à tout va surtout pas mais des initiatives "populaires" pourquoi pas.

Score : -3 / Réponses : 0

----

Constitutionnaliser et rendre effectifs les droits au travail et au logement

Score : -3 / Réponses : 0

----

Fixer le droit de vote à 16 ans, instaurer le vote obligatoire et la reconnaissance du vote blanc comme suffrage exprimé et généraliser la représentation proportionnelle

Score : -3 / Réponses : 0

----

Trop de démocratie tue la démocratie. Par les élections présidentielles puis les élections législatives les français ont les moyens d'exprimer leurs opinions. Ils le font pour bon nombre d'entre eux en toute connaissance de cause. Les programmes des candidats sont publics. Donner au peuple ( donc pas forcement tous des personnes inscrites sur les listes électorales) par des pétitions en ligne, référendum d’initiative populaire, propositions de loi citoyennes, questions au gouvernement citoyennes c'est instaurer de l'instabilité dans le fonctionnement des institutions. Donner plus de poids aux députés pour pouvoir être de vrais relayeur des revendications des territoires me semble plus approprié.

Score : -5 / Réponses : 3
