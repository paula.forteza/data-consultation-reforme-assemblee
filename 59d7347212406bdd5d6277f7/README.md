## Quel rôle pour les citoyens dans l’élaboration et l’application de la loi ?

### Consultation pour une nouvelle Assemblée nationale

# Consultations en amont des textes

<div style="text-align: justify;"><img src="https://consultation.democratie-numerique.assemblee-nationale.fr/an/consulations_textes.png"></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">Nombre d'expériences de consultation des citoyens en amont de l’examen de projets ou propositions de loi ont eu lieu ces dernières années. Certaines de ces consultations ont été organisées par l'Assemblée nationale, d'autres par le gouvernement et les dernières par l’écosystème dit des «&nbsp;technologies civiques » ou </span><i><span style="font-size: 11pt;">civic tech</span></i><span style="font-size: 11pt;">.&nbsp;</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">&nbsp;</span></div>
<div style="text-align: justify;"><span style="font-size: 11pt;">Des députés se sont d’ores et déjà saisis, à titre individuel, de ces possibilités et des bonnes pratiques sont apparues mais rien n’a pour l’heure été institutionnalisé, hormis la possibilité de déposer des contributions sur les études d’impact des projets déposés par le Gouvernement.</span></div>
<div><span style="font-size: 11pt;">&nbsp;</span></div>
<div style="text-align: justify;"><i style="font-size: 11pt;"><span style="color: rgb(107, 36, 178);">Ces consultations préalables doivent-elles être repensées ? systématisées ? Selon quelles modalités ? Qui doit les organiser ? Par ailleurs, comment s'assurer que les contributions recueillies sont bien étudiées en vue de la décision finale ?</span></i></div>
<div style="text-align: justify;"><i style="font-size: 11pt;"><span style="color: rgb(107, 36, 178);">&nbsp;</span></i></div>
<div><i style="font-size: 11pt;"><span style="color: rgb(107, 36, 178);">Une question spécifique se pose s’agissant des projets de réforme constitutionnelle, qui touchent aux «&nbsp;règles du jeu&nbsp;» démocratique et qui suivent une procédure d’adoption particulière. Faudrait-il envisager, pour les plus importants d’entre eux, des modalités spécifiques de participation citoyenne&nbsp;? </span></i></div>
<div><br></div>
<div><span style="font-size: 10pt; color: rgb(230, 0, 0);">Nota bene</span><span style="font-size: 10pt;">&nbsp;:  par défaut, les contributions les plus populaires apparaissent en tête  de la page par ordre décroissant. Vous pouvez si vous le souhaitez  afficher les contributions les plus récentes ou les plus anciennes. Pour  cela, modifiez le mode de </span><b style="font-size: 10pt;"><u>classement</u></b><span style="font-size: 10pt;"> à droite de «&nbsp;Commentaires&nbsp;» avant la 1ère contribution.</span></div>

#### Commentaires

Je pense qu’il faudrait, dans un 1er temps, réussir à rendre la “lecture” de la loi, du projet de loi, des amendements, un peu plus “lisibles” pour la majorité de la population. 
Si je prends par exemple la fiche sur la “fixation de l’ordre du jour par la conférence des présidents”. J’ai pu prendre connaissance de cette fiche sans trop de soucis. Elle est compréhensible sur le fond mais sur la forme elle est un peu “rebutante”. 
Pourquoi ne pas réformer cet usage en créant par exemple des “infographies” sur ces différents sujets. 
Si nous voulons la participation du plus grand nombre, si nous voulons que d’avantage de citoyens proposent des lois ou aident à leur construction, il faut réussir (sans dénaturer et trop schématiser évidemment) à rendre ces textes moins austères et donc inciter un maximum de personnes à accéder à cette information. 
Pour proposer du nouveau, il faut connaitre ce qui existe. 
Je propose donc l’établissement d’un service ( composé de toutes les sensibilités politiques) chargé de “vulgariser” (bien que je n’aime pas ce terme) les lois, projets de lois, fonctionnement de l’AN, afin de rendre la plate-forme plus accessible et utilisable par le plus grand nombre. Il s’agira ainsi d’éviter que ces réformes ne touchent que ceux qui sont déjà investis et motivés par la vie politique et la vie du pays.

Score : 225 / Réponses : 18

----

Il serait intéressant que les rapporteur(e)s des projets et propositions de loi (et même tous les députés participant à la réflexion sur un texte) utilisent  fréquemment la pratique des Ateliers Législatifs Citoyens (ALC), voire que cette pratique soit généralisée. Ces Ateliers ont été mis en place par la députée de la 4e circonscription de Saône-et-Loire (Cécile Untermaier) et consistent, en amont de l'adoption d'une loi, à réunir les citoyens pour discuter de l'intérêt du texte, des raisons de son adoption et surtout des moyens de l'améliorer. 

Avec des citoyens directement intéressés par l'objet du texte, on réfléchit plus concrètement aux répercussions d'un projet de loi sur la vie pratique des individus et on peut l'améliorer. Le député qui anime cet atelier porte ensuite des amendements à l'Assemblée nationale en fonction de ce qui s'est dégagé des réflexions des citoyens. cette pratique constitue un procédé de démocratie participative intéressant car il donne la parole aux citoyens sans déposséder l'élu de son pouvoir législatif puisque c'est lui qui sélectionne et défend les amendements au projet de loi. De ce fait, ce même projet de loi, par l'action des députés, se trouve davantage relié aux citoyens là où les textes issus du Gouvernement sont parfois plus technocratiques et éloignés des préoccupations ou réalités citoyennes.

 Il faut donc promouvoir ces ateliers afin d'en généraliser la pratique dans les circonscriptions car ils garantissent une meilleure participation des citoyens et une meilleure représentations des députés.

Score : 122 / Réponses : 7

----

**Simplifier les textes**

Actuellement les propositions de loi sont vues comme des modifications de textes existants et sont donc très difficiles à lire : "un alinéa xx est inséré au zz"
Il faudrait une vraie simplification à la fois des textes existants (arrêtés l'empilement) et surtout que les lois soient en annule et remplace afin de gagner en lisibilité

A minima il faudrait que l'on puisse voir en marque de révision les inserts ou suppressions prévues afin de faciliter la lecture. De nombreux outils permettent de comparer deux versions d'un document et de mettre en exergue les différences ce qui permettraient de gagner du temps.

Score : 107 / Réponses : 7

----

Je suis personnellement extrêmement favorable à la consultation des citoyens en amont de la loi, mais uniquement si la volonté de consultation est réelle, et qu'il ne s'agit pas de légitimer une proposition de loi ou un projet de loi en disant "voilà ce que pense les citoyens", alors qu'en réalité on parle d'une toute petite partie de citoyens.

Ces consultations souffrent d'un bais de participation immense :  en général ce sont des CSP+ ou très connectés, qui s'expriment correctement à l'écrit. 

C'est la raison pour laquelle je pense que nous ne sommes pas encore arrivés au stade de la généralisation des consultations sur les lois. Le faire, ce serait à mon sens créer une démocratie moins représentative, alors que le but est de la rendre plus représentative (Lawrence Lessig : http://www.lemonde.fr/pixels/article/2017/04/23/le-principal-probleme-de-la-democratie-actuellement-c-est-qu-elle-n-est-pas-representative_5115774_4408996.html ) 

Il faut donc continuer à expérimenter et apprendre d'une consultation à l'autre, aller chercher les citoyens par des conférences ou des ateliers comme d'autres le proposent ici, mais aussi les ramener vers Internet. Il faut également être très vigilant car il est facile d'afficher une participation importante de citoyens sur une consultation, alors qu'en réalité on parle de quelques contributeurs actifs. Le risque de manipulation des résultats de ces consultations est important (même s'il existe évidement beaucoup de bénéfices : donner une possibilité aux citoyens d'être actifs, faire monter un sujet peu à même d'être traité par le Gouvernement, créer une délibération sur un sujet particulier et apprendre des autres contributeurs, etc). Il me semble qu'il faut donc être prudent et avancer pas à pas. 

Par ailleurs, ces consultations doivent être intégrées dans le temps législatif : elles ne peuvent pas durer quelques jours et s'achever la veille du passage en commission. Autrement il n'y a pas de temps pour une intégration dans les conclusions du rapporteur. Le groupe de travail sur la démocratie numérique devrait peut-être en discuter avec le groupe de travail traitant de la procédure législative.

Enfin, la consultation doit à mon sens être organisée par les assemblées elles-mêmes (ou un organe parallèle, mais public). Mais ceci ne fonctionnera que si elles le font aussi bien que les autres plateformes de consultations (et pas ce qu'on a vu jusqu'à présent, avec une expérience utilisateur nulle, la possibilité simplement de faire des commentaires libres sur une loi, sans interactions, sans visibilité de ce que disent les autres, sans même savoir ce que cette participation va devenir).

Score : 68 / Réponses : 4

----

Faciliter la diffusion du contenu législatif, via des canaux innovants et/ou plus classiques (vidéos explicatives ou déclarations de députés). 

Plusieurs créateurs de contenus à large audience (ex : HugoDécrypte) ou des sociétés de production proposent des vidéos courtes et informatives sur divers sujets. L'Assemblée nationale pourrait s'en inspirer ou faire directement appel à leurs services pour diffuser de courtes vidéos expliquant les principales dispositions législatives votées et leurs conséquences. 

Pourquoi ne pas imaginer faire intervenir acteurs et députés pour expliquer concrètement les changements impactant le quotidien des Français. Également tenter d'impliquer les chaînes d'informations du service public dans ce projet, pour que ces contenus ludiques et informatifs puissent toucher l'ensemble de la population, et pas seulement ceux ayant accès à internet.

Score : 53 / Réponses : 4

----

Les consultations citoyennes sont une vaste blague. Et ce n'est pas un citoyen désabusé depuis le début qui parle mais bien un adepte de la démocratie participative, qui participe activement à toutes les consultations citoyennes qu'elles soient du gouvernement (République numérique, EGalim…) ou des députés via une plateforme comme "parlement-et-citoyens.fr". C'est une blague car le système parlementaire est incompatible avec la démocratie participative. Le calendrier fixé par le gouvernement est presque tout le temps beaucoup trop rapide et ne permet déjà aucun débat parlementaire. L'utilisation d'ordonnances ou de l'article 49 alinéa 3 de la constitution ainsi que la rédaction systématique des lois par le gouvernement tue toute démocratie. Déjà, les propositions de loi des groupes de l'Assemblée Nationale ne sont jamais étudiées. Une simple réforme comme vous semblez vouloir la faire ne sera que de la communication alors que le gouvernement utilise toujours les méthodes d'autrefois, signant les lois dans les bureaux dorés de l'Elysée, devant les caméras. Les lois doivent être écrite et signée par les députés. C'est déjà une base sur laquelle commencer. Ensuite, on pourra parler du rôle des citoyens et des lobbys pour aller vers une démocratie participative voir inclusive avec un système de démocratie liquide.

C'est bien tout le système démocratique qui doit être revu, non pas avec une simple réforme mais avec un changement de constitution.
Donc, on pourra systématiser les consultations citoyennes autant que vous voulez, mais si aucun rôle est défini dans une organisation totalement nouvelle, nos contributions resteront toujours lettre morte, comme elles le sont actuellement.

Score : 37 / Réponses : 15

----

-	Avant toute consultation, la préoccupation principale concernant l’initiative des lois apparait être l’évaluation des textes précédents avant d’en proposer de nouveau. L’impression qui en ressort est le manque de cohérence entre les lois et une perte de temps. 
-	La seconde préoccupation des intervenants du groupe a été la qualité de l’information disponible pour les citoyens. Afin de permettre une plus grande implication des citoyens, ces derniers doivent avoir avant tout une meilleure information. 
-	La discussion a ainsi rapidement dévié sur le thème de l’information à destination des citoyens. 

Les principales idées et réflexions qui en ressortent :

* 	Afin de permettre une certaine sérénité des débats, il faudrait trouver un système « dépolitisant » les informations concernant les textes pour ne pas que ces dernières soient accusées d’être partisanes.

*	On peut dès lors imaginer un service de communication spécifique à l’Assemblée Nationale dédié à la création de support de communication clair, court et pédagogique sur l’ensemble des textes présentés ; 

* 	Ce dernier serait également en charge de la diffusion de ces informations sur l’ensemble des canaux de communication avec un objectif d’en maximiser l’audience.

*	A cet effet, on peut imaginer, en plus des moyens numériques, une diffusion télé sur le modèle des spots de candidats diffusés avant les élections à des heures de grandes écoutes comme avant les journaux télé.
*	La chaine parlementaire est également jugée un peu archaïque, pas assez interactive et avec un public trop ciblé. Il faudrait pouvoir inventer de nouveaux formats plus accessibles (exemple de l’émission « Au Tableau » sur C8)
*	La création d’une plate-forme internet officielle sur laquelle le citoyen pourrait être consulté facilement et rapidement et donner son avis sur différents sujets est souvent évoqué mais elle rencontre deux difficultés : les risques inhérents à Internet si le site est basé sur l’anonymat et une faible affluence si les internautes doivent s’identifier afin de s’exprimer par la crainte de voir leur opinion politique être possiblement enregistré sur un site officiel. 
*	Une réflexion revient cependant souvent : il ne faut pas que les solutions numériques soient les seules déployées et trouver des moyens de communications plus classiques afin d’éviter les problèmes liés à la fracture numérique.

Score : 31 / Réponses : 1

----

Pour ce qui est de a consultation des citoyens en amont des textes législatifs, une idée judicieuse serait peut-être, dans un premier temps, de créer sur cette plateforme des "avis citoyens". Des domaines seraient proposés lors de l'inscription en ligne des citoyens (agriculture, économie, industrie, budget, enseignement supérieur etc...) et en fonction des thèmes choisis, les citoyens seraient consultés sur les textes relatifs à ces domaines. Des consultations pouvant prendre la forme de questionnaires, de participation par commentaires ... Ensuite, les parlementaires pourront enrichir leurs débats grâce à ces réponses et pourquoi pas se servir des contributions pour amender les textes.

Autre idée, permettre aux députés plus largement de créer des "comités citoyens" en circonscription. Toujours sur la base d'une plateforme numérique, chaque citoyen désireux de contribuer aux débats pourrait déposer sa candidature, en sélectionnant ses domaines de prédilection (ou en indiquant sa catégorie socioprofessionnelle), un comité qui pourra ainsi être divisé en "commissions", sur le modèle des commissions de l'Assemblée nationale. Ensuite, le député pourra librement réunir l'ensemble de son comité citoyen  pour débattre des prochaines réformes ou alors, réunir les commissions intéressées. Une belle façon de rapprocher les citoyens de leurs parlementaires, en plus de rapprocher le citoyen des affaires publiques.

Score : 28 / Réponses : 4

----

Il est nécessaire de simplifier les textes afin que chacun soit en mesure de bien les assimiler, ce qui peut et doit par la même susciter davantage d'intérêt dans l'implication à la vie politique. Proposer une possibilité d'expression en ligne, type référendum afin de prendre meilleure considération l'opinion celles et ceux qui se sentent concerné mais qui pour l'heure ne peuvent donner leur avis, les députés devront évidement promouvoir cette possibilité aux habitants de leur circonscription , cela est basique mais il reste tant à faire.

Score : 20 / Réponses : 0

----

CRÉER UN SITE DU SUIVI DE L’AVANT-PROJET DE LOI

Dès qu’un projet de loi est en cours d’élaboration, un site Internet de suivi doit être créé, dans les conditions fixées par la loi.

Ouvert aux citoyens, ce site leur permettra d’alimenter le débat dès la phase de préparation de l’avant-projet. Ils pourront s’identifier grâce à leur identifiant unique [lien : http://consultation.democratie-numerique.assemblee-nationale.fr/topic/59d731a712406bdd5d6277d1#comment-59f72e80e8a9656c1a8e73b3] et formuler des contributions qui, pour leur bonne gestion, pourraient être limitées en nombre (une par citoyen) et en taille (3.000 signes), tout en requérant une synthèse en 100 caractères. Ces contributions seront traitées par les services du ministère concerné, lequel s’engage à réagir à un nombre prédéterminé d’entre elles, dès lors qu’elles font l’objet d’un certain soutien. Elles permettront également d’alimenter l’étude d’impact qui doit accompagner chaque projet de loi.

Certains projets de loi pourront être exclus (autorisation de ratification d’accords internationaux, par exemple) et certaines informations pourront ne pas être transmises ou l’être de façon différée, pour des raisons d’intérêt général.

Autres propositions dans le cadre de cette consultation : http://constitutiondecodee.blog.lemonde.fr/2017/10/30/participation-numerique/

Score : 20 / Réponses : 0

----

**Consultations en amont des textes**  

Dans le cadre d'une consultation en amont des textes, pour la partie en ligne numérique, il faudrait bénéficier d'un **outil de débat de masse**.  

Ce système pourrait être un système de **question / réponse collaboratif** qui permettrait d’**afficher les réponses viables principales de manière synthétique** pour une question donnée avec la possibilité d’aller chercher les détails de cette réponse (justifications, sources, etc).

L'objectif serait d'amener vers les **propositions les plus qualitatives possibles**, et ce notamment à travers une méthodologie d'accompagnement dans la démarche de participation.  

C'est la réflexion que nous menons en développant l'outil Open Source ["Débattons"](https://docs.google.com/presentation/d/1UIsnLdP2XgO_Ii6g98lWW4FsMuDccD-TigsT5NSFKOU) (dont les [dernières infos sont dispos ici](https://www.linkedin.com/pulse/avanc%C3%A9es-sur-d%C3%A9battons-suite-au-hackaton-doctobre-anthony-ogier/)).

Score : 20 / Réponses : 1

----

Transparency International France recommande de promouvoir la consultation en ligne des citoyens et des parties prenantes sur les projets et propositions de loi :
• En créant une plateforme permettant aux acteurs concernés de soumettre directement leurs propositions d’amendements ou d’articles, en amont de l’examen du texte au parlement.
Les projets ou propositions de loi seront  mis  en  ligne sur  une plateforme qui permettra aux acteurs concernés de soumettre directement leurs propositions d’amendements ou d’articles. Les personnes inscrites sur le registre des représentants d'intérêts seront informées de l’organisation de ces consultations. 
En Croatie,  une  plateforme  officielle  permet  aux  citoyens  de  commenter publiquement toute proposition de loi et de suivre l'avancée des travaux. Sur les 15,000 commentaires envoyés lors des consultations publiques, 41% ont été totalement ou partiellement acceptées en 2014.

• En s’assurant que les critères retenus pour le dépouillement soient transparents et publics.
Pour que tous les  acteurs  puissent y  recourir,  il  est   nécessaire  d’en  définir  précisément  le périmètre et les modalités tout en s’assurant qu’elles respectent des principes d’équité d’accès, d’intégrité et de transparence. Les modalités de ces consultations devront  également être précisées : délais suffisamment longs, publication de l’ensemble des contributions reçues et de la liste des organisations participantes…

Score : 19 / Réponses : 2

----

* Contribution collective Café Citoyen : Présidée par Mme Nadia HAI Députée de la   11ème circonscription des Yvelines *
**78-11-Q2 Du terrain au Gouvernement, des citoyens sollicités, consultés, impliqués et acteurs**

Niveau 1 : Sur le terrain les **initiatives sont portées librement par la Société Civile**, les associations, syndicats, mouvements et partis politiques par tous moyens d'implications des citoyens adaptés (mode associatif, portes à portes, marchés, débats, contributions volontaires)

Niveau 2 : **Élus et parlementaires organisent le recueil des avis de leurs électeurs**. Notamment la mission du député est élargi à cette **obligation de concertation** mais son mode reste au libre choix du député (exemple des ateliers législatifs citoyens ou rencontres citoyennes).  Seule nouveauté, le Député doit produire un **bilan annuel public** de cette concertation et des actions législatives qui en ont découlés ou pas. 

**Niveau 3 CESE** : Le CESE étudie et si nécessaire précise ou reformule les propositions de textes émanent des citoyens (voir 78-11-Q1). Il met à disposition ses compétences en terme d'expertises et conseils techniques ou juridiques pour élaborer des pistes de textes législatives ou pré-projets. Il doit avant tout  se concentrer sur les buts, enjeux, objectifs, voir proposer des indicateurs de mesure d'efficacité. Il a toute latitude pour impliquer les porteurs originaux de l'initiative. Il peut faire évaluer ces nouvelles versions aux citoyens via la plateforme d'initiative citoyenne. Il transmet directement, ou après la consultation précédente, à la 9 eme commission permanente de l'assemblée nationale ou au Gouvernement. 

**Niveau 4 la 9eme Commission AN **: Une neuvième commission permanente de l'assemblée nationale est créée afin de traduire en termes législatifs appropriés les propositions émanant des citoyens venues via le CESE, via tout parlementaire à commencer par les députés (ou via la plateforme d'initiative citoyenne). Cette 9eme Commission pourrait auditer les porteurs de l'initiative voire les auditionner ou les impliquer dans la rédaction de cette loi ou texte législatif. Cette commission devra veiller à éviter l'inflation législative (redondance des textes, sécurité juridique, stabilité législative) et garantir une lisibilité des textes.
Des citoyens pourraient être tirés au sort parmi les citoyens volontaires pour participer aux différentes commissions de l'assemblée nationale. 

**Niveau 5 un Sénat citoyen **: Le travail d'analyse des textes, la rédaction des amendements, la relecture des projets législatifs du Sénat pourraient être utilement renforcés en associant   ponctuellement des citoyens tirés au sort parmi des volontaires sans conflit d'intérêt avec les projets étudiés. Ces citoyens tirés au sort sur le mode des tribunaux d'assisses seraient mobilisés sur des périodes courtes pour un ou plusieurs projets de texte et seraient indemnisés pour le temps passé au service de la Nation. Ils pourraient participer à l'ensemble des travaux du Sénat pendant leur mission y compris l'évaluation des lois.

Score : 17 / Réponses : 0

----

Organisation de "conférences de citoyens" pour soutenir le travail parlementaire

Les instituts de sondage développent depuis longtemps des dispositifs d'enquête qualitatifs allant plus loin que les classiques "études d'opinion" que l'on diffuse dans les médias. Ces dispositifs permettent de réduire les coûts de consultation sans perdre l'intérêt de la représentativité des opinions recueillies. 

Ces consultations réunissent un groupe de citoyens ensemble pendant une ou deux journées dans le but de rencontrer experts et spécialistes sur une question donnée. A l'issue de cette formation, on recueille leurs opinions sur la question. 

Ce dispositif pourrait être mis en place, en interne de l'Assemblée nationale, pour évaluer une proposition législative. Experts et députés pourraient venir exposer leur point de vue à ce groupe de citoyens qui rendrait à la fin un texte commun rassemblant leurs principales propositions qui serviraient de documents de travail aux députés.

Score : 16 / Réponses : 0

----

--> Créer un Hub Politique
L'idée ici est de pouvoir prendre en considération le plus rapidement possible l’avis d’un plus grand nombre de citoyens et citoyennes sur des sujets précis, et ce sans avoir à lancer de gigantesques sondages et/ou des longues campagnes d’ateliers participatifs à travers la France.

--> Quel est le constat à date ?
De plus en plus de personnes débattent, commentent, critiquent les engagements et volontés politiques via les réseaux sociaux mais peu d'entre elles pensent que leurs avis comptent vraiment. Ce postulat vient du fait que : 
- soit ils ne savent pas vraiment comment "interpeller" le monde politique 
- soit parce qu'ils pensent que parce qu'ils font parti d'une autre famille politique que celle de la majorité actuelle alors ils ne peuvent pas être écoutés et entendus
Nous pouvons donc  imaginer une multitude d'outils pour encourager et permettre au plus grand nombre de participer au débat législatif mais comme une large majorité de citoyennes et citoyens sont dans le schéma précédemment cité alors leur avis continuera à être ignoré. Cela continuera alors à générer un sentiment de frustration et de rejet vis à vis d’un monde politique qui pourtant veut les entendre.

-->A court terme, comment prendre en compte la parole des citoyens et citoyennes qui dans leur grande majorité ne viennent pas à la politique ?
L'idée est donc la suivante : créer un hub numérique qui ira se plugger sur les APIs des différents réseaux sociaux existants (Twitter, FB, LinkedIn, etc.) et ira faire de la "pêche" aux informations sur des thématiques données. Les thématiques seront décidées selon le calendrier législatif et ainsi chaque temps (semaines, mois) sera consacré à une thématique précise. 
Il sera ainsi possible d’atteindre le plus grand nombre d’avis sur un sujet et de voir les mots clefs qui y sont associés. A noter, il faudra prévoir dans l’algorithme permettant le plugging aux réseaux sociaux de ne pas prendre en compte les trolls.
Une fois ces requêtes faites, il sera possible d’alimenter des travaux législatifs.
Par ailleurs, le hub politique permettra aux élus de « tâter le pouls » du terrain sur certains sujets qu’ils souhaiteront travailler et/ou d’avoir des retours terrain sur certains sujets déjà légiférés. Si besoin, des travaux de pédagogie pourront alors être menés sur cette base en circonscription (via permanence et/ou associations)

--> A moyen terme il sera possible d’aller plus loin dans notre démarche :
En nous appuyant sur les résultats des requêtes faites via le Hub, il sera également possible de mettre en place des rdv citoyens récurrents sur des temps de rencontre que nous pourrions définir comme des « cafés politiques ». Ces cafés politiques n’ont pas vocation à se dérouler en un même et unique lieu. L’idée est de les mettre en place :
- en permanences parlementaires en circonscription
- au sein des associations locales volontaires qui remonteront aux permanences de leur circonscription le retour sur leur café politique

Les cafés politiques rendront compte alors à l’assemblée nationale des remontées terrains et en retour donneront aux terrains une vue sur l’avancée des travaux parlementaires.

Score : 16 / Réponses : 4

----

L’assemblée nationale devrait créer une application où serait centralisés toutes les contributions. 

Une fois une nouvelle consultation publiée , les citoyenne et les citoyens apportent leur contributions avec un système de votes. De ce fait, les 3 premières qui remportent le plus de votes sont retenues. Les autres sont mises dans une liste d’attente afin de ne subir aucune perte de contributions, laissant ainsi une chance à tout le monde.

Score : 15 / Réponses : 6

----

Le problème principal pour la mise en place d'une consultation préalable est celui de la participation. En effet demander au public de s'exprimer de ses propres mots sur un projet ou une proposition de loi ne peut qu'amener à la participation des plus connectés ou des plus éclairés d'entre nous. Ceux qui n'ont pas l'habitude au quotidien de s'interroger sur ces enjeux ne participeront pas car ils ne sauront comment exprimer leurs pensées et opinions. 
Le problème avec les consultations publiques est qu'elles ne laissent pas de place au "contributions faibles" que le web a pourtant permis. En effet qui d'entre nous ne s'est pas contenté d'un "like" ou d'un commentaire court sur un sujet?  Notre société s'oriente vers la culture du clic et du "like" et il serait fort dommage de ne pas en tenir compte. Les citoyens ne souhaitent plus , pour la majorité, co-rédiger des textes mais seulement valider ou refuser des options.
Les initiateurs de telles consultations devraient d'abord définir des options cliquables intuitives et interactives pour décider des grandes orientations que devra prendre le texte. Ces options devront pouvoir être élargies (avec une faculté de proposition ) , remplacées, ou supprimées au fur & à mesure de la consultation. Et c'est seulement une fois que les orientations auraient été définies et qu'un prototype de projet/ proposition de loi pourrait être proposé avec un espace de discussion sur chacun des points du texte permettant cette fois des contributions rédigées. 
En cas de désaccord majeur entre les contributeurs, ce point pourrait être débattu pendant les débats parlementaires. 

Ensuite vient le problème de la publicité. Créer un espace de consultation intuitif & interactif n'est pas forcément suffisant. Il faut aussi que les citoyens puissent en entendre parler. C'est là que les réseaux sociaux doivent être utilisés ainsi que les médias traditionnels. La démarche a certes été lancée mais il faut à présent la pérenniser et la renforcer

Score : 15 / Réponses : 1

----

Proposition à partir d’un exemple concret : participation des personnes en situation de handicap.

Une instance consultative, le CNCPH (Conseil National Consultatif des Personnes Handicapées), est une instance à caractère consultatif chargée d’assurer la participation des personnes handicapées à l’élaboration et à la mise en œuvre des politiques les concernant. Il est constitué des principales associations représentatives des personnes handicapées et des parents d’enfants handicapés ainsi que de représentants des organisations syndicales, des collectivités territoriales, de grandes institutions comme la Mutualité Française, la Croix-Rouge, …

Comme le prévoit la circulaire du 4 septembre 2012 relative à la prise en compte du handicap dans les projets de loi, chaque projet de loi doit être accompagné d’une étude d’impact puisque « Des dispositions spécifiques aux personnes en situation de handicap ont en principe vocation à figurer dans chaque projet de loi. ». 

Proposition 1 : appliquer la circulaire

Le CNCPH devrait systématiquement être sollicité en amont des textes afin qu’il puisse proposer des modifications au moment de l’élaboration du texte. 

Proposition 2 : solliciter cette instance en amont

Lorsque le texte est stabilisé, il est présenté devant le CNCPH qui émet un avis éclairé et argumenté. Les demandes de modifications non retenues sont ainsi exposées.  

Proposition 3 : communiquer cet avis avec le projet de loi pour que chaque député puisse avoir les informations sur cette consultation

Proposition 4 : définir des instances consultatives représentatives qui pourraient être sollicitées sur ce modèle (ponctuellement ou systématiquement)

Score : 15 / Réponses : 1

----

N'hésitez pas à exploiter la consultation réalisée sur ce sujet en novembre dernier par  Patrice Martin-Lalande & Luc Belot sur la plateforme Parlement et Citoyens ici : Généraliser les consultations en ligne : https://parlement-et-citoyens.fr/project/generaliser-les-consultations-en-ligne/consultation/consultation-29

Score : 12 / Réponses : 2

----

L'Assemblée nationale pourrait installer une plateforme numérique sur laquelle les citoyens pourraient s'exprimer sur une proposition de loi lorsque le député qui en est l'auteur veut consulter les citoyens sur le texte qu'il a déposé ou qu'il veut déposer (en amont, ça serait évidemment mieux). 

Ce type de consultation existe pour certains projets de loi, il faudrait le généraliser pour les propositions de loi. Cela permettrait de valoriser l'initiative législative du Parlement qui est étouffée par les initiatives du Premier ministre. Cela permettrait aussi d'enrichir la proposition de loi soumise à consultation, les propositions de loi manquant trop souvent de substances.

Les avis et opinions émis par les citoyens pourraient constituer le support à d'autres réflexions dans le cas où la proposition de loi n'aboutit pas à une adoption. Ils pourraient enrichir la réflexion des parlementaires et, au-delà, être sélectionnés pour les plus pertinents afin d'intégrer une sorte de recueil  d'idées sur lesquelles un débat pourrait être organisé par la même plateforme numérique.

 Celle-ci servirait donc aussi de lieu de discussion citoyenne et serait éventuellement animée par un groupe de députés issus des différents groupes parlementaires (de la majorité comme de l'opposition) avec qui des échanges pourraient occasionnellement avoir lieu...

Score : 11 / Réponses : 1

----

Les consultations en amont d'une loi concerne aussi les experts sur le sujet. On a vu certaines lois sortir contre l'avis de ces experts. Cette consultation doit devenir obligatoire et prise en compte dans l'élaboration de la loi.

Score : 11 / Réponses : 1

----

**consultation et contributions par défaut**
Pour chaque texte de loi il faudrait qu'une plateforme soit mise en place où la société civile puisse proposer ses contributions soit sous forme de commentaires remarques sous chaque partie soit sous forme de cahier d'acteurs.

Score : 11 / Réponses : 0

----

Une consultation des citoyens dans leur ensemble serait particulièrement souhaitable lorsqu'il y a projet ou proposition d'une loi au fort impact sociétal – tout ce qui concerne notamment l'école, les familles, etc. – lois qui donnent lieu à de prétendus "débats publics" le plus souvent confisqués par un battage médiatique qui empêche le vrai débat. Tout ce qui devra être vu comme des "faits de société" auxquels la loi doit répondre devrait donner lieu à une consultation "de la société", et non pas être laissé à l'initiative de quelque lobby ou mouvance politique. Seule voie pour que "la société" adhère ensuite.

Score : 11 / Réponses : 0

----

En raison de la crise de la représentation et de l’insuffisance du mécanisme référendaire, le recours systématique à la consultation publique dans le processus d’élaboration des lois d’envergure serait une alternative pour remédier à la crise par la prise en compte des avis exprimés par le public sans remettre en cause le système représentatif.

L’élection est un procédé essentiel dans le système démocratique. Mais il semble déficient dans son effet producteur de la crise de la représentation. La crise affecte principalement les institutions de la République et provoque la concentration des pouvoirs entre les mains de l’Exécutif. Le contrôle parlementaire et juridictionnel apparaît important mais insuffisant pour corriger les défaillances du système. La loi sur « le mariage pour tous » de 2013, la loi « Macron » de 2015, la loi « El khomri » de 2016 et les ordonnances « Macron » de 2017 ont successivement provoqué un soulèvement populaire inquiétant pour le maintien de la paix sociale.

 Le référendum d’initiative présidentielle est prévu à l’article 11 de la Constitution française pour tenter de remédier à la crise. Il a permis aux citoyens d’intervenir juridiquement sur l’adoption des normes conventionnelles et législatives. Mais depuis le « non » au traité établissant une Constitution pour l’Europe en 2005, ce mécanisme démocratique semble tombé en désuétude (voir Francis HAMON, Le référendum, LGDJ, 2012). Le référendum peut tout de même être réactivé. Mais pour éviter qu’il soit un instrument plébiscitaire, son utilisation devrait être précédée par une délibération parlementaire.

L’initiative partagée (mi-parlementaire mi-populaire) est introduite dans la Loi fondamentale depuis la révision constitutionnelle du 23 juillet 2008.  Les lois d’application ont également été adoptées. L’utilisation de cet instrument démocratique serait utile pour pallier les insuffisances actuelles du système représentatif français. Mais compte tenu du caractère restrictif des conditions, c’est une technique difficilement utilisable (Cf. Yvan BLOT, La démocratie directe : une chance pour la France, économica, 2012).
 
La consultation publique peut, en revanche, être retenue comme une formalité à rendre substantielle dans le processus d’adoption des lois d’envergure pour tenter de remédier à la crise de la représentation.  L’article 7 de la Charte de l’environnement prévoit la participation du public à toute décision publique ayant une incidence sur l’environnement. Ce principe dont la mise en œuvre à été prévue par une  loi de 2012  constitue une avancée démocratique considérable. Mais il reste limité.

L’obligation de consultation devrait ainsi être inscrite dans la Constitution et étendue à toutes les lois d’envergure afin de renforcer leur légitimité démocratique. Cependant, il serait peu efficace de procéder à des consultations populaires récurrentes pour recueillir les avis de tous les citoyens. À cet égard, plusieurs pistes de solutions sont mises en avant (Cf.par exemple, Dominique ROUSSEAU, Radicaliser la démocratie, propositions pour une refondation, Seuil, 2015).

 L’on sait néanmoins que le Conseil économique social et environnemental (CESE) constitue bien une Assemblée consultative de rang constitutionnel. Au lieu de multiplier les organismes consultatifs, le CESE pourrait être maintenu comme l’institution servant à mettre en œuvre l’obligation de consultation publique. Cette Assemblée consultative devrait néanmoins être réformée en profondeur afin qu’elle puisse représenter toutes les forces vives de la nation. Pour renforcer l’indépendance de ses membres, leur désignation devrait être déconnectée de l’Exécutif. 

Quant à l’outil informatique, il peut être utilisé comme un complément pour diffuser l’information et recueillir les avis du public. Mais il ne saurait en réalité remplacer le travail efficace qui puisse résulter des délibérations du CESE. 

Les avis résultant du processus législatif devraient, en principe, être obligatoirement pris en compte. Au cas échéant, la motivation de la loi serait une alternative.

Score : 10 / Réponses : 2

----

Recourir au référendum (Art 89) pour les réformes constitutionnelles devraient être automatique, permettant d’allier l’entièreté de la population à la définition de la norme suprême. 
Concernant les consultations, elles permettraient d’avoir des informations fiables sur l’avis des Français, au lieu de recourir aux sondages souvent manipulés/manipulables.

Score : 10 / Réponses : 1

----

1) Désir d'un "calendrier de l'Avent" parlementaire pour interagir très en amont avec le député en fonction des lois ou des textes à venir.
2) Organiser une consultation sous forme d'ateliers par rapport au calendrier parlementaire, mais en présentiel pour ne pas exclure les citoyens non connectés.
3) Mettre l'accent sur la continuité des actions de terrain en acceptant les contradictions : marchés, porte à porte dans les quartiers... (en dehors des périodes électorales).
4) Consulter via des questionnaires numériques.
5) Lors des ateliers : éducation populaire via les échanges entre citoyens (que les gens se forment et s'informent entre eux).
6) En amont, se servir du fichier des adhérents pour cibler les profils et repérer les compétences.
7) Tirage au sort du citoyen ou organiser un service civique parlementaire pendant 1 ou 2 mois, type juré d'Assises, afin que les citoyens aient la possibilité de "vivre" l'élaboration de la loi.
8) Pendant la procédure législative, faire et diffuser des powerpoints pour vulgariser les idées et les projets afin que les citoyens puissent réagir et proposer des amendements.

Score : 9 / Réponses : 0

----

Le travail des commissions, préparatoires  aux textes législatifs  ou non, est déjà une source largement méconnue .  En faciliter la prise de connaissance et le recueil de réactions et participations citoyennes serait déjà un grand progrès; à condition toutefois que les compte -rendus soient lisibles par les non spécialistes. Un décodage du vrai et du faux sur une matière controversée ou simplement en débat serait déjà une bonne introduction.

Score : 9 / Réponses : 0

----

La difficulté principale des textes est qu'ils sont difficilement lisibles pour tous les citoyens. Les lois votées sont souvent des dispositions modifiant des lois des lois antérieures. Ainsi si la loi se matérialise dans un texte, en revanche dans le code il est indiqué sont modifiés comme suit.
Il y a donc un travail de lisibilité de la loi à effectuer 
Je prends l'exemple des circulaires qui 
L'outil informatique offre de nombreuses possibilités s'agissant de la consultation citoyenne. 
Il me semble donc impératif de s'en saisir.
J'ajoute que cette consultation doit être effective. Je vais prendre l'exemple des enquêtes publiques qui malheureusement bien souvent ne tiennent pas compte de l'avis des participants.
Il faudrait donc dans cette réflexion définir un seuil de participants qui,une fois atteint, oblige les parlementaires à tenir compte des propositions (sous le forme d'une question citoyenne par exemple avec une réponse apportée et/ou un amendement proposé)
Vous pouvez peut être aussi vous inspirer des conseils de développement qui pour certains commencent à mettre en place des démarches de co construction des textes. (Loi Maptam)

Score : 8 / Réponses : 1

----

Rendre les textes plus accessibles

Dans le prolongement de la suggestion de M. Fournaise, ne pourrait-on pas adjoindre aux exposés des motifs associés aux articles de loi modifiant un code ou changeant une loi existante, les versions complètes du texte avant et après modification (sauf erreur de ma part, on ne «voit» actuellement que les modifications).

Appliquée à l’article 2 du projet de loi de finances pour 2018 (indexation du barème de l’impôt sur le revenu sur l’inflation) modifiant les articles 196B et 197 du code général des impôts, cela donnerait (l’exemple ne porte que sur une partie de l’article) :

article 197 (avant modification)
[...]
1. L'impôt est calculé en appliquant à la fraction de chaque part de revenu qui excède 9 710 € le taux de :
– 14 % pour la fraction supérieure à 9 710 € et inférieure ou égale à 26 818 € ;
– 30 % pour la fraction supérieure à 26 818 € et inférieure ou égale à 71 898 € ;
– 41 % pour la fraction supérieure à 71 898 € et inférieure ou égale à 152 260 € ;
– 45 % pour la fraction supérieure à 152 260 €.
[...]
article 197 (après modification)
[...]
1. L'impôt est calculé en appliquant à la fraction de chaque part de revenu qui excède 9 807 € le taux de :
– 14 % pour la fraction supérieure à 9 807 € et inférieure ou égale à 27 086 € ;
– 30 % pour la fraction supérieure à 27 086 € et inférieure ou égale à 72 617 € ;
– 41 % pour la fraction supérieure à 72 617 € et inférieure ou égale à 153 783 € ;
– 45 % pour la fraction supérieure à 153 783 €.
[...]

Ceci rendrait plus lisibles les propositions et ne devrait pas se traduire par un surcroît de travail en amont trop important. La même procédure pourrait être étendue aux amendements.

Score : 8 / Réponses : 2

----

Une plateforme devrait être mise en place donnant la possibilité de discuter des projets de loi et d’en suggérer.
Pour que les discussions soient constructives, outre une synthèse de l’étude d’impact, il serait nécessaire qu’elles prennent pour point de départ une synthèse lisible par tous comparant objectivement avant/après et les avantages/inconvénients. Les citoyens pourront ainsi se positionner et faire part de leurs inquiétudes et soumettre des suggestions à partir de cas concrets.
J’ai personnellement expérimenté cette approche mais sous forme d’ateliers et elle a rencontré un grand succès. Mais pour toucher tous les citoyens, une plateforme est nécessaire.

Pour susciter la participation des citoyens, il est crucial de les convaincre en premier lieu qu’il ne s’agit pas d’une approche démagogique. La prise en compte de leur observations doit être démontrée, de même que la neutralité de l’approche et de la restitution.
Aussi, l’accès aux discussions en parallèle à la restitution qui en est faite serait nécessaire.

Cette dernière devrait faire part de ce qui va être pris en compte et expliquer les raisons pour lesquelles sont écartées les autres observations ou suggestions. Ce qui permettra une meilleure compréhension et favorisera une meilleure adhésion.

Autre point : la consultation ne devrait pas se faire alors que les discussions parlementaires ont débuté...

Score : 8 / Réponses : 1

----

- Une consultation d'un panel de la population représentatif, en sus d'experts de la question, afin d'avoir un "avis populaire" sur la question traitée.
- Sur les questions constitutionnelles, il serait essentiel d'obliger au référendum, en y ajoutant un vote obligatoire. Cela permettrait de prendre en compte les impacts importants des modifications centrales de la vie publique.

Score : 5 / Réponses : 0

----

Quel beau paradoxe : le gouvernement ayant utilisé les ordonnances, pour imposer une loi dont les Français ne voulaient pas, essaye de se racheter une bonne conscience en organisant un simulacre de consultation citoyenne... Ce quinquennat a démarré par un déni démocratique ; il finira tel qu'il a commencé.

Score : 4 / Réponses : 0

----

C'est une bonne idée.Une diffusion des projets de loi éviterait que les journalistes ne fassent des résumé tendancieux

Score : 3 / Réponses : 0

----

Atelier citoyen par le comité de Fulham, En Marche UK

En faveur de l'institutionnalisation des consultations

 
Afin de rendre une consultation pertinente et démocratique, il s’agirait de répondre aux critères suivants:

 
1) La mise en place d’une consultation
 

Les consultations sur un projet ou une proposition de loi doivent avoir lieu en amont de sa présentation devant le parlement : c’est donc le projet du projet qui doit être examiné. Pour qu’une consultation soit représentative et réfléchie, elle devrait durer entre 1 et 2 mois, soit assez de temps pour que la communication autour d’elle soit établie et que les citoyens aient le temps de s’organiser si besoin.

 
Pour lui permettre d’identifier ses intérêts, le citoyen a besoin d’avoir une visibilité sur le calendrier parlementaire. Ce dernier doit être communiqué pour l’année, permettant ainsi une meilleure information sur les futurs projets de loi, hors texte issu de session extraordinaire.
 

Le consulat ou la préfecture se devront de communiquer ces informations en amont à la liste électorale. L’envoi d’une proposition de consultation doit s’accompagner d’une note explicative sur l’origine du problème que la loi vient fixer et sur le détail des dispositions législatives ainsi qu’une étude d’impact. Une banalisation du projet peut aussi être envisagée.
 

Les parlementaires sont libres d’informer leurs administrés sur la tenue d’une telle consultation, mais il revient au pouvoir public de s’assurer du bon déroulement de la consultation à travers notamment une communication et une information suffisante.

Le questionnaire de la consultation doit permettre d’identifier l’électeur à travers un numéro d'électeur. Il doit aussi mentionner au préalable que l’avis citoyen sera consultatif: ce dernier doit permettre à chacun de faire entendre sa voix et d’expliciter une position qui aurait pu échapper au parlementaire. Mais, dans le respect d’une démocratie représentative, le citoyen ne se substitue pas au parlementaire dans l’élaboration de la loi.

 
2) Le contenu du questionnaire
  

Dans cette optique, le questionnaire de la consultation doit s’articuler autour d’question sur l’avis du citoyen sur le projet/la proposition de loi:
- Le citoyen peut estimer que ce projet va dans le bon sens mais qu’il néglige tel ou tel aspect qui doit être inclus.
- Le citoyen peut estimer que ce projet ne va pas dans le bon sens due à telle ou telle raison.
Dans les deux cas, l’avis du citoyen et la proposition qui en découle doivent s’accompagner d’une motivation sur les raisons.

3) Analyse des réponses par un groupe de travail

 
Pour que cette consultation citoyenne soit prise en compte, il est nécessaire d’y allouer un budget permettant la création d’un comité d’études des consultations citoyennes. Un tel comité, placé au sein de l’Assemblée Nationale, se composerait d’administrateurs parlementaires et des fonctionnaires de l’INSEE. Ce comité devrait aussi inclure un citoyen, tiré au sort, qui serait témoin de l’étude des avis. Ce comité, qui aura produit au préalable la documentation nécessaire à la participation citoyenne, analyserait les résultats de la consultation et trierait les demandes pertinentes.
 

4) La prise en compte de la consultation
 

Parallèlement à l’introduction du projet de loi, la synthèse de la consultation, et notamment ses idées les mieux argumentées, doit être présentée par le comité à la commission permanente responsable du projet de loi au sein de l’Assemblée. Les députés seraient tenus d’écouter puis de débattre des propositions citoyennes. Si des propositions parviennent à obtenir une majorité au sein de la commission, alors elles devraient être incluses dans le projet de loi.

 
5) Le retour sur la participation citoyenne

 

Chaque personne ayant pris part à la consultation a le droit à un retour sur les propositions retenues et à un compte-rendu statistique sur les résultats de la consultation. Le comité serait chargé de rédiger ceci et l’Assemblée nationale serait tenue de le publier sur son site.

Score : 3 / Réponses : 0

----

Réflexion du comité local Des Essarts le Roi, Le Perray en Yvelines, Levis Sasint Nom :
Il est mentionné que de nombreuses expériences de consultation ont eu lieu mais nous n’avons les uns et les autres pas de souvenir de consultation et nous n’y avons pas participé sauf à la dernière sur la stratégie logement … 
Une première recommandation est donc dans la communication plus large, plus massive sur ces consultations et surtout une unicité de temps dans tout le pays pour traiter le sujet (le mois de la consultation sur le logement par exemple). Elles doivent être à l’initiative du/des Ministère(s) qui vont porter la loi et pourra se décliner en plusieurs événement
1.	Un questionnaire accessible sur le site du Ministère et sur les sites des partis/mouvements/associations qui souhaitent relayer cette consultation. Ce document définit le contexte qui amène à légiférer et ensuite pose des questions ouvertes et fermées sur le vécu actuel et les enjeux. Ce questionnaire permettrait à chacun individuellement ou collectivement de donner son avis sur les enjeux et les axes prioritaires que devra traiter la loi.
2.	Des réunions publiques organisées par des associations partenaires/ le député/ le ministère pour débattre du sujet mais avec toujours comme objectif de soumettre ensemble une contribution (questionnaire ou document de synthèse).
La consultation en amont des textes doit être pour nous systématique même si certaines lois susciteront moins d’engouement que d’autres. Les citoyens doivent être consultés lors de la discussion des projets et propositions ainsi que des différents amendements afin d'enrichir le débat et de susciter l'engouement des électeurs lors de l'adoption des textes législatifs.  

La numérisation des démarches administratives va forcément nécessiter la mise en place d’accès à internet dans les services publics (bornes interactive, PC en libre-service, …), les consultation devront également pouvoir être accessibles sur ces équipements mis à disposition du public.
Une autre condition du succès est la transparence et la large diffusion des résultats de la consultation avec ses limites (représentativité du panel ayant répondu à la consultation par exemple, …) et comment ces résultats vont être pris en compte dans le travail d’élaboration de la loi. Quelles sont les pistes retenues, les axes privilégiés suite à la consultation, etc … 
Avec les moyens informatiques nous avons maintenant la possibilité de traiter un nombre important de données que ce soit des questions fermées ou ouvertes (algorithme utilisé pour La Grande Marche et toutes les consultations d’En Marche) en un temps court, une instance indépendante pourra rédiger à partir du traitement de ces données une synthèse de la consultation qui sera commentée et annotée par le/les Ministères porteurs du projet de loi et diffusé à l’ensemble des personnes ayant répondues à la consultation et sur le site du Ministère et des partis/mouvements et associations y ayant participé

Score : 3 / Réponses : 0

----

** 1/ Mieux informer et former les citoyens**
Il est impératif de mieux informer les citoyens sur les consultations en cours, notamment dans un soucis de représentativité des avis exprimés. Il convient de diffuser davantage l’information (trop de citoyens ne sont pas au courant de l’existence de ces démarches et en sont de fait exclus) mais également d’accompagner les citoyens dans la compréhension des projets ou propositions de Loi sur lesquels ils sont consultés. 

Pour cela, nous proposons :

* De déployer des moyens de communication adaptés aux différents types de publics : chatbot / agent conversationnel, cartographie sémantique, visuels, FAQ, réseaux sociaux et messageries instantanées grand public, médias 	traditionnels (télévision, journaux locaux) 
* De garantir l’accessibilité des ces démarches,
* D’hybrider la participation en ligne et les moments d’animation et d’échange en présentiel	
* De s’appuyer sur les échelons locaux au maximum : par exemple, instaurer des commissions citoyennes locales avec des citoyens tirés au sort, instituer des cahiers de doléance numériques dans les mairies, proposer des séances de débats régulières en circonscription (en ligne et en présentiel) autour de l’élaboration des propositions de loi… 
* Faire travailler ensemble acteurs de la civic tech, qui proposent des outils numériques, et les acteurs de la médiation numérique, qui accompagnent les citoyens dans leur adoption des usages numériques, dans le cadre de la réforme de l’Assemblée Nationale, 
* Recourir davantage au tirage au sort : par exemple, à l’issue d’une consultation, une journée délibérative pourrait être organisée pour travailler autour des résultats de celle-ci, impliquant des citoyens tirés au sort (n’ayant pas pris part à la consultation). Autre exemple : mettre en place des commissions locales de participation citoyenne, qui coordonnent et relaient les consultation au niveau du territoire, composées en partie de citoyens tirés au sort.

Score : 3 / Réponses : 0

----

Le monde associatif doit pouvoir s'exprimer car il a des compétences pratiques que n'ont pas les députés  ni peut-être les représentants membres du CESE ... surtout dans le domaine social et caritatif largement confié -ou plutôt abandonné- au bon vouloir de leurs bénévoles.
BL, responsable d'équipe Secours Catholique.

Score : 2 / Réponses : 0

----

Les députés ne doivent plus décider seuls, il faut qu'ils écoutent l'opinion publique et prennent en compte les pétitions lancées par les citoyens. Ce n'est pas pour rien que de plus en plus de pétitions en ligne voient le jour.

Score : 2 / Réponses : 0

----

Je suis bien d'accord avec vous. Car j'ai à ce sujet de nombreuses questions, telles :
a) comment est fixé l'ordre du jour de l'Assemblée Nationale ?
b) qui rédige les propositions de lois, et sous la responsabilité de qui ? du gouvernement ? de l'Assemblée Nationale ?
c) un député est-il obligé de lire et de comprendre (!) ce que dit une proposition de loi avant de la voter ? combien de temps lui est donné pour ce faire ? y a-t-il "une consigne de parti" pour le vote de chaque proposition de loi ?
d) de nombreuses lois votées ne sont pas mises en application par non-publication des décrets. Pourquoi cette procédure ? Qui rédige ces décrets ? qui décide de leur publication ou de leur non-publication ? N'y a-t-il pas là un moyen subtil pour l'administration pour contrôler pour les décisions de l'Assemblée Nationale ?
e) La proposition de supprimer au moins une loi chaque fois qu'une nouvelle est mise en application, est-elle réalisée ?
f) il est inévitable que l'empilement des règlements et lois soit truffé de contradictions. D'où un autre empilement, celui de la jurisprudence ! Tout cela encombre la justice. Il y aurait un vaste travail de clarification à faire. La volonté politique y est- elle ?

Score : 2 / Réponses : 0

----

Suite à un atelier de réflexion pour la réforme de l'Assemblée Nationale avec la participation de 6 adhérents de LaREM, il nous parait important d'amplifier le rôle des chaines de télévision publiques pour vulgariser les projets de loi, les lois, amendements afin de sensibiliser un nombre plus important de nos concitoyens. Cette sensibilisation au niveau nationale, par une campagne, qui sera complétée par des réunions locales sur le même thème, devrait permettre d'augmenter le nombre de participants et la motivation pour la transformation de la vie politique.

Score : 2 / Réponses : 0

----

LA SIMPLIFICATION DU DROIT: UNE MESURE ABSOLUMENT NECESSAIRE POUR LA COMPREHENSION ET L'APPLICATION DU DROIT!
Propositions: 
1) Elaborer des principes fondamentaux à intégrer directement dans la Constitution et par conséquent applicable quel que soit le contexte juridique. Exemple: le principe de dignité humaine est posé dans plusieurs code (Civil, santé publique), alors qu'il est indiscutable et devrait directement être intégré dans la constitution. Idem pour la primauté de la personne, idem pour le libre choix de toute personne de consentir aux actes importants de son existe, idem pour l'obligation d'information nécessaire au recueil du consentement
2) Poser des principes clés dans chaque code en fonction des spécialités du code. 
Ex: dans le code de la santé publique
- prévoir les principes essentiels d'information et de consentement comme principes essentiels dans tous les actes de prévention, de soins préventifs et palliatifs; 
- Prévoir des directives anticipées pour permettre d'exprimer à toute personne ses souhaits et pas seulement ceux relatifs à la fin de vie. 
- Prévoir la définition de la personne de confiance et les rapports avec les autres proches; 
- Prévoir les règles générales de responsabilités.
L'objectif serait ainsi de permettre aux professionnels de santé de prendre connaissance de l'ESSENTIEL du droit puis ensuite d'aller dans les parties du code pour prendre connaissance des règles plus spécifiques relevant de spécialités particulières (ex: don d'organes, assistance médicale à la procréation, ect). 
3) Poser la définition de chaque concept utilisé comme préalable pour permettre une meilleure utilisation et surtout une application homogène sur tout le territoire national
4) Interdire tout renvoi  à d'autres textes car certaines dispositions actuelles sont illisibles, incompréhensibles et inapplicables. 
TROP DE LOIS TUE LE DROIT
TROP DE LOIS TUE LA COMPREHENSION DU DROIT
TROP DE LOI TUE L'APPLICATION DU DROIT

Score : 2 / Réponses : 0

----

mes idées pour  la consultation en amont du projet:
- rendre plus "compréhensible" le projet de loi pour les novices des termes juridiques et surtout en pensant les moyens en fonction des différents citoyens (jeunes, adultes, personnes malentendantes et malvoyantes, personnes très âgées): mettre en ligne un lexique juridique FALC, présenter le projet de loi en bandes dessinées pour attiser la curiosité des plus jeunes (futurs électeurs), utiliser les médias de formation nouveaux, type télévisuels et informatiques (MOOC, chaine Youtube, présentation par le rapporteur de la loi).
- Mixer la consultation présentielle et distancielle en créant une "commission citoyenne participative" par grande région ou en local en prenant soin de créer des binômes jeunes-personnes âgées: cela permettrait aux jeunes de former les personnes âgées à l'utilisation des outils informatiques pour se renseigner sur le projet de loi et voter en ligne et le deuxième effet Kiss cool est que cela permettrait à de nombreuses personnes âgées seules de rompre avec l'isolement et la solitude; nommer un rapporteur qui irait présenter au Parlement le résultat de la consultation et les éventuels amendements proposés
- Création d'une charte de fonctionnement: cadre légal, règles de fonctionnement, répartition des rôles, 
- Cadre légal: dans le cadre de la future révision de la Constitution, il serait utile de prévoir qu'à compter d'un certain nombre de votes (nombre à déterminer) issus de la consultation, la proposition et/ou l'amendement soit obligatoirement pris en compte dans le projet de loi
- Faire en sorte d'avoir des indicateurs relatifs à la prise en compte de la consultation citoyenne: nombre d'amendements pris en compte ou rejetés (et l'obligation de motiver la décision), création d'un observatoire extérieur pour observer le fonctionnement réel de la consultation (pourquoi pas avec des binômes parlementaires et citoyens + membres issus du droit)

Score : 2 / Réponses : 0

----

je voudrais faire partager une expérience avortée de tenter d'infléchir un processus législatif en cours sur des aspects très pragmatiques:
 les professionnels de l'enfance ont été informés au jour le jour de l'état des débats parlementaires sur la loi de mars 2016 sur la protection de l'enfance.C.e qui est une excellente chose. Au cours de ce débat, j'ai alerté physiquement à l'occasion d'un colloque les personnes concernées au niveau national d'un "bug" concernant certaines dispositions , et fait remonter une proposition concrète , simple via ma hiérarchie  .La machine étant lancée , rien ni personne n'a répondu en blanc ou en noir à cette proposition qui concerne affaires sociales , justice et intérieur: un décret de novembre 2013 puis la loi ont interdit la diffusion sauvage France entière sur les adresses mail des CRIP (cellules départementales de recueil des informations préoccupantes ) de situations d'enfants en danger ou supposés l'être ayant quitté leur département d'origine sans être localisés précisément et réglemente le transfert des IP entre départements.
La suppression de cette pratique très attentatoire aux libertés et à la vie privée était nécessaire.Les situations diffusées ne justifiaient pas cela. Ceci a été remplacé par une information au parquet du TGI de départ et c'est tout; Or ce dispositif non encadré permettait de retrouver quand même très souvent des enfants en grand danger:.Les plus jeunes déscolarisés avec des parents en fuite ou en errance, les nourrissons en danger  ,.le signalement via le parquet permet à tout le moins de retrouver des fugueurs dans des commissariats à condition qu'un raison particulière les y amène  mais il y a peu de chance qu'une mère ayant besoin de lait , de couches ou de logement ne vienne dans un commissariat.et même si c'est le cas , est elle finalement dans un fichier comme ayant avec elle ou lui des enfants en danger ?Donc ce dispositif sauvage mais qui avait une réelle utilité n'a été remplacé par rien.Le droit au respect de la vie privée a pris le pas sur la protection de l'enfance .Nous avions proposé que le GIP 119 reçoive ces signalements puisque c'est déjà sa mission et fasse le tri de ce qui pourrait relever d'une diffusion offrant ainsi la garantie qu'il s'agissait d'une protection prioritaire .Cette proposition ne coûtait pas un euro à qui que ce soit .le défenseur des droits ou la CNIL ou la CADA pouvaient en dire quelque chose .Comme retour d'expérience , j'avais pu identifier et nommer 6 situations d'enfants retrouvés et protégés dans l'année précédant cette mesure de suppression .
ce petit exemple suffit à montrer que l'accès au processus législatif même pour des initiés est impossible .
A titre de test , j'aimerai savoir si ceci peut remonter jusqu'aux intéressés.

Score : 2 / Réponses : 0

----

GENERALISER LA PLATEFORME PARLEMENT-ET-CITOYENS.FR
Cette plate-forme web créée en 2013 par un ex collaborateur parlementaire permet à chaque citoyen de donner son avis sur les textes de loi en discussion au Parlement et de soumettre des amendements. Elle a fait ses preuves en permettant d’améliorer certains textes de loi avant leur examen au Parlement (Loi sur la République Numérique d’Axelle Lemaire, Loi sur les pesticides du sénateur Joël Labbé…). Elle devrait être systématiquement utilisée pour TOUS les projets et propositions de loi. Son code informatique devrait être contrôlé et protégé par les services de l’Etat.

Score : 2 / Réponses : 0

----

S'assurer en premier lieu de la volonté telle de prendre en compte les réactions des citoyens : chaque sollicitation doit préciser les modalités et critères de prise en compte des avis. Ensuite, les contributions doivent être sollicitées sur une base pragmatique : on ne demande pas au citoyen un avis militant, mais un avis critique et éclairé. Attention, donc, à la question. Les politiques doivent mettre en pratique les connaissances accumulées par les experts de la concertation

Score : 2 / Réponses : 0

----

Groupe n°2 sur la consultation des citoyens en amont des textes :

- Créer une plateforme d’échanges entre citoyens et députés, ainsi qu’une sorte de réseau social sur les propositions de lois. (27 voix)

- Faire des enquêtes publiques numériques et physiques. (19 voix)

- Faire arbitrer les citoyens en commission mixte paritaire. (19 voix)

- Organiser des réunions publiques pour consulter les citoyens en présentiel. (15 voix)

- Créer un abonnement thématique avec réponse par mail ou courrier pour informer les citoyens sur les projets et propositions de lois. (9 voix)

- Créer un calendrier législatif consultable par les citoyens. (7 voix)

- Consulter plus largement les associations et entreprises, et en faire une obligation constitutionnelle. (6 voix)

- Réserver les consultations citoyennes pour les questions sociétales en question fermée. (6 voix). 

- Distinguer les questions économiques, sociétales et régaliennes. Orienter les consultations uniquement sur certains thèmes. (5 voix)

- Faire de la pédagogie en amont des textes. (3 voix)

- Solliciter les citoyens par des boîtes à idées dans les lieux publics. (1 voix)

Score : 2 / Réponses : 0

----

Comme l'ont souligné d'autres commentateurs, les consultations citoyennes sont frustrantes car elles dépendent toujours du bon vouloir des députés qui, la plupart du temps, rejettent les contributions qui ne vont pas dans leur sens.

Je réponds donc à la question « Qui doit les organiser ? » : les partis politiques. [Certains le font d'ailleurs déjà](https://en-marche.fr/article/consultation-citoyenne-strategie-logement). Ainsi on peut voir comment les idées sont reçues par les uns et les autres puis voter aux élections en connaissance de cause.

Ce que pourrait faire l'État pour soutenir la participation citoyenne, ce serait de soutenir des initiatives comme le comparateur de [Voxe](http://www.voxe.org) pour centraliser les connaissances et les positions des uns et des autres sur différents sujets, afin d'aider chacun à identifier le parti dont il pourrait se rapprocher.

Score : 1 / Réponses : 2

----

Notre législature devient, à force d'ajouts de lois, semblable à un vrai maquis. Chaque nouvelle loi semble être ajoutée sans réfléchir à ce qui existe déjà.
La proposition est d'obliger l'élaborateur d'une loi à se poser la question de la loi existante d'un point de vue théorique. Par exemple, il serait obligatoire d'émettre un argumentaire permettant de justifier que la loi passe par un nouvel article plutôt que par la modification ou la suppression d'autres articles.

Score : 1 / Réponses : 1

----

On empile souvent des lois sans se soucier de celles qui existent déjà, de leur application, de leur impact et de leur retour d'expérience (je pense notamment aux lois sur la sécurité et aux lois travail)
Pour chaque proposition de loi, il faudrait obliger à faire un rapport sur la loi précédente pour l'évaluer (il s'agit pour l'instant simplement d'une possibilité). Il deviendrait donc obligatoire d'attendre cette évaluation avant de proposer une nouvelle loi.

Score : 1 / Réponses : 0

----

Une consultation systématique en amont de l'examen des textes, à laquelle les parlementaires pourraient participer directement aux côtés des citoyens, peut être mise en place très facilement grâce aux outils numériques. Il n'y a donc a priori aucune raison de ne pas le faire. Il appartiendrait ensuite aux commissions concernées de consulter le contenu produit et d'en verser les éléments les plus utiles au débat en commission.

Score : 1 / Réponses : 0

----

Bonjour, je souhaite rappeler qu'il existe déjà nombre de plateformes de consultation en ligne et d'interaction entre élus et citoyens initiatives de la société civile. Onze d'entre elles ont été présentées lors d'un récent débat organisé par make.org ou le Président de l'Assemblée nationale François de Rugy est venu marquer son approbation à la consultation citoyenne. Par ailleurs le groupe de travail Démocratie numérique a consulté des associations comme "Parlement & Citoyens" qui est opérationnel sur le sujet. Pour l'accompagnement et l'évaluation on peut citer Open Source Politics qui travaille avec les institutions et administrations,  Regards Citoyens qui a déjà l'expérience d'accompagnement du dépouillement des consultations citoyennes. .  "Citoyens et élus : interaction bienvenue!" sur Le Blog de la Ménagère http://www.leblogdelamenagere.info/post/2017/10/04/Citoyens-et-%C3%A9lus-%3A-interaction-bienvenue%21

Score : 1 / Réponses : 0

----

REGAGNER LA CONFIANCE DES CITOYENS / ELARGIR LES PROFILS PARTICIPANT
Pour envisager une participation active d'une base élargie et diversifiée des citoyens, il s'agit de leur redonner confiance envers les politiques et d'oeuvrer à la diminution de la fracture sociale et numérique. Lourde tâche ! En complément des commentaires de Mathieu Roy et Sophie Iza que je partage, commençons par éduquer nos enfants à l'art de l'échange bienveillant. Ensuite, impliquons davantage les citoyens sur des sujets locaux touchant à leur quotidien au travers de conseils citoyens de quartier à intensifier. Peu sont véritablement au fait pour prendre position, pire, à voter, sur des projets de loi...

Score : 1 / Réponses : 0

----

Préalable : Reconnaître la diversité des modes de contribution citoyenne dans la fabrique de la loi. 

La consultation des citoyens sur un texte de loi, en amont ou au cours de l’examen au Parlement, n’est pas l’alpha et l’oméga de la participation citoyenne. Il serait nécessaire de faire un inventaire des différents modes de participation citoyenne possibles à chaque étape de la loi, que ce soit le lancement d’un débat en ligne sur un thème donné, l’ouverture des études d'impact aux commentaires, l’enrichissement des documents de travail des commissions parlementaires, etc. Cet inventaire permettrait de mieux identifier les caractéristiques de chaque mode de contribution citoyenne : les besoins spécifiques auxquels il répond, les conditions de l’engagement citoyen, et la valeur ajoutée qu’il apporte

Score : 1 / Réponses : 0

----

Mieux cibler les modes de participation citoyenne en fonction à la fois de la nature des textes de loi examiné et des objectifs poursuivis. 

Compte-tenu de l’importance croissante de l’exécutif dans l’initiative législative, le CNNum considère que les projets de loi par construction, beaucoup plus descendants que les propositions de lois - pourraient être les supports privilégiés de nouvelles formes de participation citoyenne. Dans son avis sur la Confiance dans la vie publique (juillet 2017),  il propose de faire des contributions citoyennes sur les projets de loi un principe, en donnant au Gouvernement la responsabilité de justifier les exceptions.

Source : https://cnnumerique.fr/wp-content/uploads/2017/07/Avis_CNNum_Confiance_Vie_Publique_vF.pdf

Score : 1 / Réponses : 0

----

Rendre l’exercice de synthèse des contributions citoyennes plus transparent :

Si la synthèse des contributions citoyennes peut être facilitée par l’emploi d’outils de web sémantique, les règles de cet exercice restent souvent opaques pour les citoyen-ne-s. Il serait opportun de réfléchir à des processus pour expliciter des choix éditoriaux qui sont faits, et mieux inclure les parties prenantes (groupe de travail, atelier dédié)

Score : 1 / Réponses : 0

----

SYNTHESE CONSULTATION CITOYENNE 5911

L’Assemblée doit informer en amont sur l’objectif de la loi.
Mise en place de collèges citoyens et/ou de sachants éventuellement par tirage au sort
Différencier les consultations selon le type de loi ou de texte, les lois sociétales (type PMA), les lois qui concernent l’ensemble des citoyens (logement, écologie...) et les lois plus techniques (ex : réforme du dialogue social).
Mise en place de consultations élargies et dématérialisées, consultation de groupe pour permettre réflexion et débat.

Score : 1 / Réponses : 0

----

Concernant le biais de participation à la consultation, qui implique  que seulement les personnes les plus intéressées par le sujet, aptes au débat et à l'expression, déjà fortement intégrées à la vie publique, ne donneraient leur avis : 
On pourrait réformer la Journée Défense et Citoyenneté et la transformer en un parcours réparti sur l'année des 18 ans. Chaque nouveau citoyen devrait participer à 4 rencontres (par exemple) avec un groupe d'autres nouveaux citoyens, animées par des députés et des spécialistes des sujets débattus dans les lois en cours d'élaboration. Durant ces rencontres, les nouveaux citoyens en apprendraient plus sur la fabrique de la loi, seraient sensibilisés aux nouvelles techniques de participation citoyenne (cf. propositions d'application ou de plateforme en ligne pour participer, ateliers citoyens en circonscriptions, hub de participation...) et pourraient participer à leur élaboration en débattant ensemble et en écrivant des propositions sous une forme élaborée. 
Ces rencontres auraient deux finalités : être directement prises en compte par les députés (ce serait un des canaux de participation, en plus de la participation en ligne, via une plateforme ou une application, ou via des ateliers citoyens en circonscription pour les citoyens plus anciens), mais aussi sensibiliser les nouveaux citoyens à la fabrique de la loi, les faire réfléchir à leur élaboration, les faire progresser d'une rencontre à l'autre, et leur donner le goût de continuer par la suite. Les ateliers citoyens devraient être généralisés pour que ceux à qui l'exercice a plu puissent continuer dans cet exercice de citoyenneté.

Score : 1 / Réponses : 0

----

Toute réforme constitutionnelle devrait être soumise au peuple après un large débat dans le pays. Le service public de l'information devrait enfin jouer un rôle constructif à condition qu'il sorte de ses œillères politiciennes.

Score : 1 / Réponses : 0

----

La consultation en amont des textes ne sert à rien si le Parlement reste une chambre d'enregistrement comme il l'est depuis 5 mois.

Score : 1 / Réponses : 0

----

A chaque objectif national envisagé, pourrait être organisé des consultations publiques préalables (à l'instar de ce qui est fait pour les enquêtes d'utilité publique). Un dossier comprenant les objectifs recherchés ainsi qu'éventuellement les moyens d'ores et déjà envisagés serait mis à disposition du public (web, mairie,etc.) pour consultation et étude dans un premier temps. Puis des réunions publiques seraient organisées par les députés afin d'alimenter la réflexion sur le sujet envisagé et de permettre également au député d'alimenter à son tour le débat à l'assemblée nationale.
Sue ce sujet se pose la question réelle du sens du député : à quoi sert-il, comment doit-il exercer sa mission et avec quels moyens ?

Score : 1 / Réponses : 0

----

La conférence de citoyens

L’autorité politique, qu’elle soit étatique ou régionale, diffuse largement dans la presse un avis demandant aux personnes intéressées par le sujet de la conférence (sur des thèmes sociétaux ou autres) de se manifester. Pour éviter une trop forte concentration sociologique, il faudrait également adjoindre d'autres citoyens tirés au sort. Dans un premier temps, un panel de citoyens tirés au hasard parmi ces personnes se réunit avant la conférence. Avec l'aide de formateurs et d'animateurs, le groupe se familiarise avec la thématique de la conférence, élabore les questions qui seront posées aux experts et choisit ces derniers. Dans un second temps, la conférence elle-même se déroule, s’étalant sur 3 à 4 jours et accueillant tous ceux qu’il le souhaite. Les experts répondent aux questions des citoyens, qui se retirent parfois pour clarifier certaines questions ou en préparer d'autres. Ensuite, le panel délibère à huis clos et ses membres rédigent avec l'aide d'un secrétariat un rapport de quelques dizaines de pages qui est ensuite rendu public, envoyé aux médias et aux parlementaires. 
Ce type de conférence a déjà été élaboré dans des pays du nord de l’Europe (Norvège, Danemark, Allemagne) et même en France à la fin des années 1990, autours de la question des OGM. Elles pourraient permettre de faire émerger les points de vue et avis présents dans la société sur un thème donné, et ainsi nourrir constructivement le débat parlementaire.

Score : 1 / Réponses : 0

----

Respecter les députés, élus du peuple, en évitant que les commissions et les séances plénières aient lieu en même temps ; voter les lois à des heures décentes et non pas pendant la nuit ; donner le temps aux échanges entre les députés des différents groupes ; représenter la diversité des agents économiques dans les auditions et ne pas privilégier les groupes de pression...
Bref, avoir un fonctionnement démocratique de l'AN qui n'a pas à voter des lois sous la pression, ni dans l'urgence, ni dans la confusion, ni en présence de quelques dizaines de députés seulement.
Un tel fonctionnement est indigne.
En quoi la consultation directe des citoyens sur l'élaboration des textes de loi peut-elle améliorer la situation ? N'est-ce pas une mascarade qui vise à réduire encore un peu plus le rôle  des députés ?
De gros doutes sur les arrières pensées qui conduisent à cette consultation qui se dit démocratique (et qui déjà ne peut concerner que ceux qui ont accès à internet)...

Score : 1 / Réponses : 0

----

Indépendance de la justice

Nicole Belloubet a promis lors du congrès annuel de l’Union syndicale des magistrats (USM), vendredi 13 octobre 2017 d’avancer sur l’indépendance du parquet. Fermement interpellée au sujet de sa discrétion à ce propos par Virginie Duval, la présidente de ce syndicat qui recueille près des trois quarts des voix aux élections professionnelles, Nicole Belloubet a rassuré : « Voici le temps venu de la révision constitutionnelle. » 
« Notre démocratie ne peut pas faire l’économie d’un tel combat pour l’indépendance de la justice », a plaidé Mme Duval. Combien de fois la CEDH ne l’a t’elle pas rappelé ! 
Il s’agirait de faire entrer dans la Loi fondamentale la nomination des procureurs généraux et des procureurs de la République sur avis conforme du Conseil supérieur de la magistrature (CSM). Cette pratique, respectée par les gouvernements successifs depuis 2010, lie le garde des sceaux à l’avis que le CSM émet sur les propositions de nomination. Le texte voté au printemps 2016 alignait également le régime disciplinaire des magistrats du parquet (ceux qui pilotent les enquêtes, décident de l’action publique et requièrent les sanctions pénales) sur celui des magistrats du siège (ceux qui jugent). La ministre affirme vouloir se « battre sur les conditions de nomination de magistrats du parquet, gage de leur indépendance ». 
Les citoyens ne peuvent qu’approuver… MAIS pas question de remettre en cause le lien hiérarchique avec la chancellerie dans la mise en œuvre d’une politique pénale définie par le gouvernement. Pourquoi donc? Les juges n'appliqueraient pas la loi ? Ils ne seraient pas assez obéissants ? Le gouvernement veut moderniser? Si vraiment il le veut il suffit de couper le cordon ombilical, mis en place par Napoléon 1er et en revenir à Montesquieu : séparation des pouvoirs ! On éviterait ainsi de voir se renouveler sans cesse les scandales de santé publique ou autres scandales, à la faveur d’une impunité qui sert les lobbies mais nuisent à la nation.
Demandons à Mr de Rugy de consulter les citoyens.

Score : 1 / Réponses : 0

----

Je pense difficile d'établir un modèle de consultation préalable applicable à n'importe quel projet de loi. Tout dépend du sujet. Bien sûr, une phase amont de consultation des experts et des corps intermédiaires (syndicats, associations, ONG) est indispensable. Les propositions d'ateliers citoyens dans les circonscriptions (Jean-François Kerléo) ou de consultations internet décentralisées sont intéressantes ; elles supposent, comme il a été indiqué fort justement, un gros travail de pédagogie.
Mais comment donner la parole à Monsieur ou Madame Toutlemonde, pas spécialement investi(e) dans le débat public, en particulier en amont des projets de loi concernant la vie personnelle (bio-éthique), et les "règles du jeu" démocratiques ? Je pense que la formule des conventions de citoyen(ne)s tirés au sort apporterait un grand renouvellement de notre démocratie. Elle ne peut être appliquée que pour des projets de loi importants car elle requiert des conditions particulières (isolement, formation, durée, possibilité d'auditionner des experts, etc.) qui ne sont pas gratuites. Pour se convaincre de la capacité d'un groupe de citoyens ordinaires, mis dans de bonnes conditions, de dire le bien commun, voir le livre de Jacques Testart, "l'humanitude au pouvoir", Seuil, 2015.  Cette formule a été utilisée pour la préparation de la loi de transition énergétique (journée citoyenne du 25 mai 2013 ; voir le rapport 2013 de l'Observatoire citoyen de la qualité démocratique du Pacte civique), 
Un processus à ajouter à notre boite à outils démocratique !

Score : 1 / Réponses : 0

----

A titre d'exemple, le projet de loi de financement de la sécurité sociale a été voté en première lecture et préalablement aucun débat public sur les cotisations RSI sur dividendes de gérant de SARL (mesure injuste du gouvernement hollande de 2012)  ! alors que le RSI va être profondément modifié voir supprimé........la conséquence depuis 2012, beaucoup de transformation de SARL en SAS pour éviter cela...
les débats se tiennent à l'assemblée et uniquement à l'assemblée, c'est dommage : peu de contacts entre les représentants participants aux débats à l'assemblée ou au Sénat  et les élus locaux dans un premier temps. Dommage.

Score : 1 / Réponses : 0

----

Une démocratie, c’est par le peuple… Alors il faut utiliser la proximité et l’outil informatique. Chaque citoyen devrait pouvoir poser LA question qui lui tient le plus à cœur sur un site de sa circonscription et visible par tous. Si cette question obtient un certain % ( par exemple 5% des inscrits), alors le député prends le relais en invitant un groupe constitué de 10 citoyens afin de lancer une consultation dans sa circonscription. Si cette dernière obtient un seuil à définir, alors elle est lancée dans la région puis si succès au niveau national. Nous avons les idées, les outils ALORS passons à l’action.

Score : 1 / Réponses : 0

----

Afin de mieux impliquer les citoyens lors des consultations préalables, il me semble d'abord que les députés représentent l'échelon local naturel pour faire le lien entre les citoyens et l'assemblée nationale. Il s'agirait donc de développer leur rôle de retranscription des avis, qu'ils peuvent  recueillir au sein de leurs comités locaux (c'est ce que fait notre députée, et rien qu'à une vingtaine de personnes, une grande diversité d'avis s'exprime), au cours de réunions de circonscription, et de réunions publiques.  
Pour une meilleure crédibilité, si l'assemblée propose une nouvelle loi, elle doit pouvoir s'appuyer sur le bilan des lois passées sur le même sujet, qui démontre l'utilité d'une nouvelle loi, et en même temps :-) définir un objectif pour ce nouveau projet de loi, assorti  d'indicateurs de réussite.  Ce sont en particulier ces indicateurs qui peuvent être soumis à l'avis des citoyens car ils doivent se rapprocher au maximum de leur quotidien  et rendre la loi accessible. 

Comme je l'ai lu dans un des commentaires, je pense qu'inscrire la consultation dans un calendrier global qui montre aux personnes participantes les différentes étapes de construction de la loi, et donc, quand  un retour concret sur la mise en application de la loi est prévu/possible, est aussi une bonne chose pour la transparence de notre démocratie. 

En cas de projet de réforme constitutionnelle, je pense qu'une consultation type référendum est la bienvenue, mais doit être accompagnée d'autres démarches pour multiplier les lieux d'information et de débat contradictoire, via les députés, les sénateurs, la mise en place de MOOC, une plateforme consultative relayée par les médias...et ainsi faire que le vote des citoyens soit un vote éclairé.

Score : 1 / Réponses : 0

----

Simplifier l'accès à la loi. Le législateur ou un avocat raisonne en terme d'articles et de droit (ex: L-230 A2-3), le français, lui raisonne en sujet (ex: que dit la loi sur un accident de voiture, ou que dit la loi sur le délai de rétractation).

Il faudrait donc améliorer Légisfrance ou les sites similaires et l'organiser de manière thématiques (tout en laissant le numéro de l'article dans l'article). Ex si je recherche un article m'expliquant mes droits en tant que consommateur sur un contrat d'assurance, je tape dans un champ de recherche "contrat d'assurance" et là je voix apparaître l'article "320, par exemple) ou alors faire des menus déroulants.

Il faut aussi créer des canaux indépendants en terme de communication. Nous sommes à l'AN pas aux Etats-Unis, communiquer par Facebook c'est bien, mais c'est pas "Français". Il faudrait donc peut-être penser à un genre de "facebook live", mais sur le site de l'AN, voir pourquoi pas créer un réseau social des lois pour tous les français où on aurait l'actualité législatif, gouvernemental en temps réel.
On peut imaginer également puisque nul n'est censé ignorer la loi que l'Etat nous envoie un mail avec les nouvelles lois (le JO) de manière gratuite car comment peut-on savoir la loi si ce service est payant?

Cordialement,

Score : 1 / Réponses : 0

----

D'accord

Score : 1 / Réponses : 0

----

Il serait intéressant de créer un groupe de citoyens représentatifs de la population qui pourraient donner leur avis pendant une période donnée. Ce serait une sorte de panel de citoyens. Pour qu'ils prennent le temps de bien étudier les dossiers, ils seraient déchargés d'une partie de leur emploi habituel. Il faudrait en revanche bien s'assurer qu'il ne puisse pas y avoir de lobbying à leur encontre (mais comment?)

Score : 1 / Réponses : 0

----

ainsi  je  découvre  qu'après  avoir  rédigé  mon  compte -rendu  d'atelier  je ne peux  pas  le  transmettre  est-ce  bien cela ?

Score : 1 / Réponses : 0

----

Une consultation citoyenne avec "notre" député serait une bonne chose. Nous élisons et n'avons plus de "contact" avec le député. 
Certes il ne peut pas "voir" tous les citoyens, mais par une plateforme numérique, ce serait intéressant et enrichissant pour tous d'avoir un débat participatif. 
Par contre, tout citoyen doit savoir qu'en dernier ressort le parlementaire vote selon sa conscience et ne satisfera pas tout le monde. A lui d'expliquer pourquoi il fait ce choix.

Score : 1 / Réponses : 0

----

Il faut systématiser les consultations citoyennes pour toutes les lois. Une plate-forme de consultation doit être ouverte sans interruption. Chaque projet ou proposition de loi  doit être soumis à l'avis des citoyens sur cette plate-forme.

Il faut grandement améliorer la communication sur toute initiative de consultation des citoyens. J'ai appris seulement deux jours avant la fermeture l’existence de cette consultation à laquelle je participe. Cela doit être communiqué dans tous les grands médias.

Score : 1 / Réponses : 0

----

Au sein de notre comité local, après débat, nous avons remarqué que les velléités de manifester dans les rues sont beaucoup moins présentes et moins fortes lorsqu’une consultation préalable a été faite sur les sujets concernés (ex : ordonnances sur le travail).

La consultation doit venir d’en haut : l’idée est déjà présente – les citoyens échangent sur ces idées (ex : grande consultation lancée sur le logement).

Le comité local doit être vecteur de rassemblement des remontées citoyennes. Attention toutefois à ne pas être élitistes : les comités doivent être ouverts à tout citoyen qui souhaiterait contribuer au débat, qu’il soit « En Marche » ou non.
Mais ce sujet pose la question annexe de la logistique : comment accueillir correctement les personnes intéressées (difficile de trouver une salle sans connaître à l’avance le nombre de participants) ? Comment gérer les débats (distribution de la parole / canaliser les gens mécontents) ?

Les députées doivent également jouer leur rôle de relais mais cela risque d’être plus compliqué si leur nombre diminue.

Score : 1 / Réponses : 0

----

quelle  déception!   je pensais que je  trouverais  ici un  cadre  où le  compte-  rendu   de l'atelier   qui  s'est  tenu à  Fréjus  Var-Est  dans  la  REM   sur  ce  sujet  avec  ses 4  volets   pourrait   trouver  sa  place,  je me  vois  contrainte  de   recopier  et  c'est   absolument  fastidieux. jJe  vais  donc  tenter  de  résumer  donc ôter et  de la  matière et  du  raisonnement:  - obligation  pour  les  élus  et les  gouvernants  de  tenir  compte du  rapport  de  force  créé par  le  numérique et  de la  volonté de  s'en servir  pour  participer directement et  individuellement. Pour  mieux  armer  les citoyens  volontaires  , mettre  à  disposition un  centre  d'appels ou  FAQ ou  source  documentaire car la  participation  à  ce  stade  de  l'élaboration   d'un projet  de  loi   par le  gouvernement  nécessite d'accéder  à un bon  niveau  d'information. Pour  la  réforme  constitutionnelle   en  préparation, les participants attendent encore plus  qu'ailleurs une  mobilisation de   tous  les  moyens d'information pour  permettre aux  citoyens  qui  le  souhaitent  de  s'exprimer   avant la  rédaction  définitive de la  réforme . Cet  exercice  de  copie par rapport  au rapport  établi  n'a  guère  de  sens , j'en ai  conscience ! et  de plus  je  m'ennuie  fort à  copier, je  vais  donc  arrêter  là

Score : 1 / Réponses : 0

----

Les consultations citoyennes n'ont de sens que lorsqu'elles ont un impact sur les décisions politiques, et n'ont de légitimité que lorsqu'elles donnent à toute la population la possibilité de s'exprimer. 
Pour garantir cette deuxième partie, il faut pouvoir réduire la fracture numérique et donner des moyens d'accès multiples aux consultations, en encourageant les regroupements citoyens et le partage de l'accès et de l'expertise en stimulant les interactions entre diverses générations. Le vote en ligne peut rester sécurisé et anonyme par de nouvelles procédures cryptographies, mais là n'est pas aujourd'hui la question. Nous disposons déjà des technologies nécessaires, il faut donc les rendre utilisables par tout un chacun. 
Pour cela, il faudrait s'appuyer sur plusieurs méthodes en parallèle:
- l'encouragement à la création de groupes de réflexion locaux
- la mise en place d'un système d'information/discussion/débat/réflexion à l'échelle nationale permettant non seulement d'avoir accès aux textes mais aussi de suivre les réflexions des citoyens sur ces textes, en s'inspirant de République Numérique mais en intégrant des outils plus avancés de discussion (comme pol.is), qui permettraient au système de rester compréhensible par tous les citoyens sans avoir à investir un temps démentiel. Ces systèmes devraient reconnaître et stimuler les groupes locaux.
- Un engagement fort de la part non seulement des parlementaires mais des ministres, pour garantir que le gouvernement ne peut pas simplement se permettre d'ignorer certains sujets (ce qui eut lieu avec République Numérique). 
Dans un premier temps, ces outils (comme celui-ci) ne seront pas utilisés massivement, car une culture de participation citoyenne se forme sur des décennies. Ce manque de participation viendra non seulement d'une incompréhension (et d'un manque de publicité) mais aussi d'une illégitimité perçue du système par son faible taux de participation. Cela changera avec le temps (comme a été observé avec les procédures de participation locale en Amérique du Sud), et nous ne devons pas abandonner simplement car le succès initial est mitigé.

Score : 1 / Réponses : 0

----

-	Ne pas systématiser les consultations
-	Reste la prérogative des élus : marge du rôle de chacun 
-	Votations : peut être dangereux dans un contexte « populiste »
-	Boîte à idées avec triage avant remontée aux députés
-	Ministre porteur d’une loi en visite sur le terrain pour jauger
-	Avoir connaissance des éléments constitutifs et amendements en temps réel

Contribution collective du comité LREM Rennes Centre Sud

Score : 1 / Réponses : 0

----

Contribution effectuée par un groupe de citoyens de la 13e circonscription des Hauts-de-Seine ( Antony, Bourg-la-Reine-Châtenay-Malabry, Sceaux)

(1) Une consultation citoyenne est plus efficace si les personnes sollicitées connaissent les dossiers. D’où l’intérêt de proposer une consultation en deux temps. Dans une première phase, seul un nombre restreint de personnes, à savoir les milieux intéressés professionnels, répondent à des questions. Cette première partie sera relativement facile à dépouiller et permettra de préparer un questionnaire plus accessible et plus documenté à l’ensemble des citoyens.

A noter que s’il existe un risque que des experts professionnels participent aux consultations citoyennes pour faire du lobbying en vue de défendre leurs intérêts particuliers, ils peuvent aussi apporter des idées sérieuses et contradictoires sur les dossiers et, ainsi, alimenter le débat.

(2) Il n’est pas possible de systématiser les consultations. Sauf sur les thématiques majeures, où elles sont importantes. Car pour que la loi soit appliquée, il faut qu’elle soit acceptée par les citoyens. 

(3) En raison de la pression existante pour transformer le pays rapidement, lorsqu’un projet de loi est déjà en lecture à l’Assemblée Nationale, il est déjà trop tard pour organiser une consultation, la dépouiller et utiliser les résultats. Les  consultations sont donc souhaitables très en amont des projets de loi, pour définir les grandes lignes de la politique sur des grands thèmes, comme cela a été fait sur la question du logement par exemple.  

(4) Les consultations doivent être lancées sur internet par le ou les ministres concerné(s). Il convient toutefois de noter que les questions posées et les solutions apportées peuvent être liées à un projet de loi, mais pas nécessairement : certaines propositions peuvent être mises en œuvre par d’autres voies que le processus législatif.

(5) Qui doit dépouiller les résultats de la consultation et en faire la synthèse ? 

Cela peut être le personnel des ministères, mais cela nécessite beaucoup de travail. Des organismes indépendants peuvent donc être missionnés pour cette tâche, ce qui pose la question du coût de l’opération, mis en regard de la garantie d’objectivité à assurer au public. 
 
(6) Comment s’assurer que les contributions soient bien prises en compte par les politiques ? Il est possible que la synthèse soit analysée et publiée par le ministère, comme  Julien Denormandie l’a fait avec le logement. 
 
Une autre très bonne idée est que les pouvoirs publics invitent les citoyens qui ont proposé des idées innovantes, dans le cadre de la restitution de la consultation sur l’élaboration de la loi, comme cela est fait dans le cadre de la présente consultation.

Score : 1 / Réponses : 0

----

Il est important d'impliquer plus régulièrement les citoyens. La fréquence souhaitable est probablement plus proche de la semaine que du quinquennat. C'est l'un des moyens qui permettra à chacun de se responsabiliser dans son intervention citoyenne. Cela signifie aussi que le temps est laissé pour réfléchir les sujets et que des allers-retours sont possibles pour permettre à l'intelligence collective de s'exprimer. 
Il faudra associer des temps de discussion et d'échange par groupe en utilisant des facilitateurs et des moments de décision. 
Les règles de prise de décision, majorité, consensus ou autre doivent être définies fonction des sujets.
Toutes les remarques, questions doivent faire l'objet d'une réponse argumentée permettant aux citoyens de prendre la mesure de la situation et de la décision. C'est en décidant que l'on cherche à avoir les informations. C'est en s'informant que l'on comprend. 
En résumé :
Des décisions fréquentes qui s'appuient sur des discussions facilitées, des processus de décision , une mise à disposition des informations explicitant la proposition et la prise en compte des arguments présentés par les groupes facilités. 
Oui ça va être long au début le temps de monter en compétences sur la citoyenneté. Pour un peuple responsable et impliqué réellement  vive la transparence !

Score : 1 / Réponses : 0

----

Ces consultations préalables doivent-elles être : 
Repensées ?  Qui doit les organiser ? 

Repensées dans leur formule/organisation
L’usage des consultations numériques est finalement encore assez récent. Il ne faudrait donc pas s’interdire de tester plusieurs formules sans partir a priori du principe qu’une méthode est meilleure qu’une autre. Si elles doivent être repensées, ce serait davantage pour combiner plusieurs modes de réponses pour une même consultation afin de toucher un public plus large. Le numérique exclut de facto une partie de la population : fracture numérique dans les usages, dans l’accès au haut débit, zones blanches...

On pourrait par exemple penser à la combinaison, à la fois :
- D’une plateforme internet comme celle sur laquelle nous répondons actuellement,
- Des formules plus courtes, en mode sondage, voir même avec une seule question,
- Des soirée, matinée, après-midi réponses… pas uniquement à PARIS, par exemple dans les mairies ou les associations locales, auprès d’un public manipulant mal internet. Elles sont alors organisées par des agents de la mairie ou des médiateurs volontaires.
- Des ateliers enfants,
- Des ateliers dans les collèges et les lycées, assurés par exemple par des médiateurs volontaires.
- Des stands avec goodies (écologiques) : un “cadeau” pour ceux qui ont répondu à la consultation.

Lorsque j’évoque des médiateurs volontaires, je pense par exemple à :
- Des retraités, 
- Des personnes sans emploi, 
- Des personnes en service civique,
- Des étudiants,
- D’autres volontaires issus d’associations, de partis politiques…

Le volontariat doit être gratifié d’une manière ou d’une autre (par exemple : indemnité symbolique, livre offert, invitation à l’Assemblée nationale…) et les frais doivent être prise en charge (transport, hébergement éventuel et restauration). Le volontariat doit aussi être précédé d’une petite formation (par exemple en ligne) et donner lieu à la remise d’un certificat.
Le volontariat me parait une solution plus créative que le recours à des salariés. Il me semble que sur la durée, des volontaires sollicités ponctuellement resteront plus motivés et créatifs. De même, je pense que le recours à des volontaires et/ou à des vacataires pour l’élaboration des “kits” de consultation serait un gage de créativité et de renouvellement des pratiques dans la durée.
Une plateforme de mise en relation pourrait fédérer les volontaires et les demandeurs d’atelier de consultation (mairies, groupes de citoyens, écoles, associations de quartier…), dans des zones géographiques définies.

Score : 1 / Réponses : 0

----

Ces consultations préalables doivent-elles être repensées dans leur communication

L’objectif principal est d’amener un maximum de personnes à participer, même si ça n’est pas à la consultation dans son intégralité.

Des partenariats peuvent être envisagés avec des marques, utilisées uniquement comme canal d’information, l’idée n’étant pas de les rendre propriétaires des données ni d’ouvrir la porte au lobbying,

Une idée originale serait une application mobile “Occupation citoyenne dans la file d’attente”. Nous sommes tous amenés à “faire la queue” et c’est généralement quelque chose qui nous agace. Pourquoi ne pas transformer ce temps d’attente en action citoyenne. Si je saisie mon smartphone, je peux avoir accès par exemple :
- A une rubrique ephéméride : “C’est un ## septembre #### que cette loi a été adoptée. Voici les # choses importantes qu’elle a changé dans les vie des Français”.
- Un sondage,
- Un résultat de consultation visuel optimisé pour mobile,
- La liste des consultations en cours,
- Des quizz ludiques sur les institutions ou certaines dispositions de la loi...

Score : 1 / Réponses : 0

----

Ces consultations préalables doivent-elles être systématisées ? 
Une question spécifique se pose s’agissant des projets de réforme constitutionnelle, qui touchent aux « règles du jeu » démocratique et qui suivent une procédure d’adoption particulière. Faudrait-il envisager, pour les plus importants d’entre eux, des modalités spécifiques de participation citoyenne ?

On ne peut que souhaiter que la consultation des citoyens dans l’élaboration de la loi soit quelque chose qui se généralise. La systématisation pourrait entraîner une lassitude à la fois chez les parlementaires, comme chez des citoyens sur-sollicités. 
- Des sondages peuvent être réalisés en amont pour déterminer les sujets qui intéressent le plus les citoyens et qui nécessitent une consultation.
- On peut partir du principe que tous les projets de réforme constitutionnelle doivent donner lieu à une participation citoyenne, même si elle est limitée.
- Tout texte de loi en élaboration devrait en revanche être ouvert aux commentaires (sans entrer dans une démarche plus complète de consultation). On peut penser à une plateforme ouverte en permanence pour laisser des contributions identifiées. Les commentaires anonymes ne me semblent pas opportuns puisqu’ils ne sont pas aussi engageants. Une identification avec France Connect me paraît intéressante à explorer. 
- On peut partir sur un objectif d’un certain pourcentage de textes de loi soumis à consultations chaque année.

Score : 1 / Réponses : 0

----

Ces consultations préalables doivent être réalisées selon quelles modalités ? 

Une consultation me paraît plus pertinente si elle touche des profils variés et est représentative de la diversité des citoyens. Pour amener davantage de participants, deux types d’options s’envisagent : le pull et le push.

Le push : on diffuse l’information de manière plus large, on s’”invite” dans la sphère des citoyens en diversifiant les médiums, les formules pour les publics (voir réponse précédente à la question “Repensées”).

Le pull : on impose la participation. On pourrait par exemple s’inspirer : 
- Du tirage du sort des jurys populaires ou 
- De la Journée d’Appel à la Préparation de la Défense (JAPD) obligatoire. 
Ainsi, pour certains sujets considérés comme “grande cause nationale”, la participation est obligatoire et les coûts sont pris en charge par l’Etat.

Chaque commune pourrait aussi être contrainte d’organiser les réponses à au moins une consultation citoyenne chaque année (prioritairement pour les projets de réforme constitutionnelle) en mettant en place une communication adéquate en réalisant un/des ateliers… Un kit de mise en oeuvre serait fourni, une ressource humaine mise à disposition pour cette/ces opérations et la publication des résultats en open data assurée pour la commune.
On peut penser à une obligation graduée en fonction du nombre d’habitants : une fois dans l’année pour les communes de moins de 50 000 habitants, davantage pour les autres.

Une approche plus spécifique peut aussi être conduite avec les bénéficiaires de prestations sociales, universitaires ou fiscales ou les usagers de certains services publics :
- Les bénéficiaires de l’aide au logement sont sollicités pour des consultations relatives au logement.
- Les usagers des services d’état civil : naissance, mariage, PACS, renouvellement de carte d’identité ou de passeport… ont à se prononcer sur l’amélioration du service comme sur des questions plus larges liées à la filiation, à la PMA, à la GPA…
- Chaque personne qui actualise son profil de demandeur d’emploi en ligne est sollicitée pour répondre aux consultations concernant l’emploi et/ou son domaine d’activité. 
- Un bénéficiaire de crédit d’impôt lié à la rénovation énergétique doit compléter une consultation liée au développement durable,
- Les étudiants rémunérés des grandes écoles publiques : ENA, Normal Sup, Polytechnique… doivent être médiateur “volontaire” au moins une fois par an...

L’idée est de montrer que la loi est un élément moteur de notre quotidien.

Score : 1 / Réponses : 0

----

Par ailleurs, comment s'assurer que les contributions recueillies sont bien étudiées en vue de la décision finale ?

On peut ici aussi avoir recours à des volontaires, par exemple tirés au sort parmi les médiateurs volontaires souhaitant s’engager davantage. À l’issue de chaque consultation, ils sont invités à venir participer à un séminaire de dépouillement et de synthèse (après un premier dépouillement à distance). Ce dernier pourrait idéalement se dérouler un samedi et/ou donner lieu à une compensation financière notamment liée à la journée de travail éventuellement perdue par le salarié. Les frais doivent être prise en charge (transport, hébergement éventuel et restauration). Le volontariat de dépouillement et de synthèse doit aussi être précédé d’une petite formation (par exemple en ligne) et donner lieu à la remise d’un certificat (d’un type spécifique).

Score : 1 / Réponses : 0

----

Nous proposons  d’initier la consultation par un questionnaire en ligne.
Celui-ci doit être bref, 5 à 10 minutes maximum, afin de ne pas « perdre l‘internaute ».

A la fin du questionnaire, il est proposé au citoyen de participer à un atelier d’échange, au sein de sa commune ou de sa circonscription, afin de débattre des sujets.

La synthèse de l’atelier est transmise au député pour être remontée à l’Assemblée Nationale.

Score : 1 / Réponses : 0

----

Le droit d'initiative citoyenne pourrait, outre le droit de pétition revu, intervenir sur la base d'une communication pédagogique d'un-e député-e autour des tenants et aboutissants d'un texte de loi. En tant que citoyen-ne-s, nous ne sommes impliqué-e-s dans un (ou plusieurs) aspect(s) d'une question sociale nécessitant une application législative et nous ne sommes pas toujours au fait des autres aspects. En utilisant un support pédagogique (infographie comme déjà soulevé, vidéo ou texte) dans lequel un-e député-e expliquerait tous les arguments et problèmes soulevés, permettrait aux citoyen-ne-s de commenter, argumenter ou contre-argumenter afin d'obtenir un premier jet d'amendement ou de projet de loi.

Score : 1 / Réponses : 0

----

CONSULTATION EN AMONT: Que chaque institution (mairie, département, région, assemblée) partage les thématiques et sujets à voter en amont, et qu'on puisse voter à titre consultatif pour s'exprimer.

Score : 1 / Réponses : 0

----

En amont de chaque séance, une vidéo courte présente les débats à l'ordre du jour, avec des liens pour aller plus loin par la suite, pour se positionner (type Le Drenche, etc). A partir de là les citoyens peuvent interpeller leur député et donner leur point de vue.

Score : 1 / Réponses : 0

----

Consultation en amont des textes (exemples du Royaume-Uni)
L’examen des textes avant le commencement de la procédure législative (pre-legislative scrutiny) est également possible, mais dépend de la volonté du gouvernement de partager les projets de loi suffisamment tôt pour avoir le temps de mettre en œuvre cette procédure. Pour l’Assemblée, il semble préférable de s’intéresser à la consultation en amont des propositions de loi. 
Ainsi toute proposition de loi pourrait être présentée au public avant l’étape d’examen de la loi. Au Royaume Uni, la consultation en amont est gérée par les commissions parlementaires. Si la commission juge qu’un texte de loi pourrait bénéficier d’une consultation avant le commencement de la procédure législative, elle publie le texte sur son site internet et diffuse un appel à contributions écrites dans la presse, sur le site internet de la commission et de la législature, et par autres voies numériques (comptes Twitter ; les commissions parlementaires ont toutes un compter Twitter). Les élu.e.s peuvent également contribuer à la publicité de la consultation par tous les moyens qu’ils jugent utiles ; leur connaissance des réseaux et intérêts dans leurs circonscriptions peut être très utiles pour solliciter des contributions. 
Les pages internet des commissions expliquent aussi comment répondre à une consultation parlementaire (notamment en termes de longueur, format, et style). Ceci permet de familiariser les citoyen.ne.s à une procédure avec laquelle ils ne sont pas familiers. Les pages expliquent aussi le rôle de la consultation et la façon dont les commissions utilisent les contributions écrites. A nouveau, expliquer comment participer de façon efficace et comment les contributions sont intégrées dans la procédure parlementaire est très important pour la légitimité de la procédure et aide les citoyen.ne.s à comprendre la valeur de leur engagement avec la législature.
A la fin de la période de consultation, les contributions écrites sont passée en revue et résumées, et la commission parlementaire peut auditionner les contributeur.rice.s jugé.e.s les plus pertinent.e.s et auxquel.le.s les député.e.s souhaitent poser plus de questions. Sur la base de ces auditions, la commission parlementaire écrit un rapport d’évaluation de la proposition de loi. Son auteur.rice peut ainsi prendre en compte la consultation et le rapport de la commission afin d’éventuellement amender la proposition de loi. 

Recommandations :
-	Réformer la procédure d’examen des propositions de loi afin d’inclure une période de consultation avant de l’examen formel en commission ;
-	Dès qu’une proposition de loi est transmise à une commission, celle-ci doit décider s’il est désirable d’ouvrir un processus d’examen pré-législatif. Les critères applicables peuvent être l’intérêt public pour le sujet de la proposition, ou le caractère controversé ou technique de la proposition. 
-	Lorsqu’un processus de consultation est ouvert, s’assurer que l’appel à contributions reçoive la publicité la plus large possible, notamment en le publiant dans certains journaux et/ou publications spécialisées appropriées, sur le site internet de l’Assemblée Nationale et la page de la commission, sur La Chaîne Parlementaire et Public Sénat, et via les réseaux sociaux ;
-	Création de comptes Twitter pour chaque commission afin de rendre plus visibles les travaux des commissions et les appels à contributions lors des consultations parlementaires ;
-	Création d’un portail sur le site de chaque commission permettant aux citoyen.ne.s et organismes de soumettre leurs contributions par internet ;
-	Réformer l’étape d’examen des lois en commission afin de permettre l’audition publique de certains contributeurs à la consultation sur la base de la qualité et la pertinence de leur contribution.

Score : 1 / Réponses : 0

----

Je pense que chaque texte pourrait en amont être présenté via une plateforme du site de l'Assemblée Nationale. Chaque groupe parlementaire pourrait exposer son point de vue, auquel pourraient répondre les citoyens.

Score : 1 / Réponses : 0

----

La meilleure façon d'associer le citoyen est de véritablement rendre lisible le projet ou la proposition de loi, or ce n'est pas du tout le cas.  Ma suggestion est donc toute simple : pour chaque article de loi modifié le ou les signataires doivent en rédiger la future rédaction complète. Pour être concret je prends exemple sur l'article 13 du projet de loi de finances pour 2018. Il a vocation à modifier l'article 213 du code général des impôts, et plus particulièrement son alinéa 1. Mon idée consiste à obliger le rédacteur de la loi à insérer dans son projet ou sa proposition l'article dans son entier, tel qu'il le souhaiterait voir voter.
Je comprends parfaitement le fait de devoir écrire que "Au premier alinéa de l’article 213 [du CGI] , les mots : « , la contribution additionnelle à l'impôt sur les sociétés sur les montants distribués mentionnée à l'article 235 ter ZCA » sont supprimés ; " mais dans le même temps cela devrait être suivi d'un "L'article 213 [du CGI] serait alors ainsi rédigé : " suivi de sa future rédaction in extenso.
Cela éviterait au citoyen de faire la longue et fastidieuse recherche de quoi change dans quel article de quel code. 
En cas de suppression d'un article, idem : obligation de ne pas se contenter d'un "l'article x du code y est abrogé". Ecrire à la place "l'article x du code y tel que repris ci-dessous est abrogé" suivi du texte de l'article en question.

Score : 0 / Réponses : 0

----

et ça serait bien si les commentaires intégraient les CR/LF du texte qu'on tape, ça serait plus lisible.

Score : 0 / Réponses : 2

----

Les propositions de lois présentées à l'Assemblée Nationale devraient afficher clairement les motivations de leurs auteurs et expliquer comment la nouvelle législation ou la modification de l'ancienne va bénéficier à l'intérêt général. Les opposants au nouveau texte ayant, eux aussi, l'obligation afficher leurs motivations et d'expliquer les inconvénients présentés par son adoption.

Score : 0 / Réponses : 0

----

Les députés doivent aller au devant des concitoyens de leur circonscription pour avoir l'avis du terrain sur les projets de lois mais le calendrier de l'assemblée nationale le permet-il. Il faut imposer cette consultation en amont par l'organisation de réunions publiques avec documents préalablement établis pour centraliser les apports citoyens. Les députés seront en mesure de transmettre les idées consignées dans un document type pour étayer les débats parlementaires.

Score : 0 / Réponses : 0

----

Synthèse de l'atelier citoyen organisé par la Députée de la 1ère circonscription de la Drôme:
- Avoir une plus grande visibilité de plusieurs mois à l’avance sur le calendrier législatif  offrirait aux citoyens la possibilité de faire remonter leurs idées. Pour les députés cela leur permettrait également de s’adresser en avance aux citoyens pour recueillir leurs avis. Ainsi dès le mois de mars ou avril on devrait pouvoir voir les  les projets ou propositions de loi qui seront portés la rentrée.
	Mettre en place des créneaux thématiques sur l'agenda
	Lancer des campagnes publicitaires à la TV pour inciter les gens à débattre sur certains sujets via des structures organisées.
- Des groupes de travail citoyens devraient être organisés par les députés autour des lois fixées au calendrier. Ces contributions seraient portées par le député devant un bureau ad hoc. Il est ensuite important que les membres de ces groupes puissent avoir un retour sur leur contribution.
- Sur des sujets d’actualité, les citoyens devraient avoir la possibilité de faire des propositions de loi (ex: consentement des mineurs), ou être consultés, ceci notamment afin de ne pas voter des lois en urgence qui auraient seulement pour effet de répondre aux effets médiatiques.
- Les sujets importants de société devraient faire l’objet de consultations citoyennes intermédiaires.
- Il serait plus incitatif de parler de contribution que de consultation, ce qui englobe la possibilité de faire des propositions.

Score : 0 / Réponses : 0

----

Proposition : Pour simplifier la procédure de consultation amont, fusionner l'ensemble des plateformes de consultation sur un seul et unique site internet. La variété des plateformes existantes nuisent à l'accès à l'information.

Score : 0 / Réponses : 0

----

Pour que ce type de participation puisse être mise en oeuvre cela nécessite un accès aux textes en amont de leur présentation pour étude et transmission des réflexions. Cela impose au Gouvernement et/ou au Parlement de pouvoir rendre ces textes accessibles sur des plateformes dédiées avec des délais raisonnables permettant au citoyen de s'exprimer et/ou de proposer.
Tout cela va nécessiter un travail supplémentaire pour le Gouvernement, pour le Parlement.
Il me semble important également que cette procédure, rajoutant un échelon dans le processus, ne vienne pas ralentir le travail parlementaire.

Score : 0 / Réponses : 0

----

SYNTHESE CONSULTATION CITOYENNE 5911
L’Assemblée doit informer en amont sur l’objectif de la loi.
Mise en place de collèges citoyens et/ou de sachants éventuellement par tirage au sort
Différencier les consultations selon le type de loi ou de texte, les lois sociétales (type PMA), les lois qui concernent l’ensemble des citoyens (logement, écologie...) et les lois plus techniques (ex : réforme du dialogue social).
Mise en place de consultations élargies et dématérialisées, consultation de groupe pour permettre réflexion et débat.

Score : 0 / Réponses : 0

----

Certes, « nul n’est censé ignorer la loi », mais si on souhaite consulter les citoyens, deux conditions sont nécessaires : rédiger en français courant et indiquer un calendrier de travail. 
« Rédiger en français courant » : cela veut dire fournir (i) un exposé des motifs clairs précisant pour chaque article l’objectif de la rédaction ou de la modification et son éventuel articulation avec d’autres articles ; (ii) des liens sur les études d’impacts et plus généralement les études et publications qui ont amené à la rédaction du projet de loi, (iii) une version complète de l’article tel qu’envisagé ; en effet, trop souvent les projets de lois ne décrivent que les changements (par exemple : A l’alinéa X de l’article Y, remplacer « xxxxx » par « yyyy »). A l’heure du traitement de texte et du lien hypertexte, la chose très faisable.
 « Un calendrier de travail » : cela veut dire : (i) fournir les projets de loi à l’avance afin de donner aux députés un délai pour étudier les textes et consulter leur base ; (ii) informer de l’avancement de chaque projet de loi en amont puis lors du processus de passage en conseil des ministres et enfin devant le parlement.
La consultation en amont pourrait être faite par l’Assemblée en concertation avec les ministères concernés. A partir du moment où le texte est soumis à l’Assemblée, la consultation devrait être organisée par chaque député.

Score : 0 / Réponses : 0

----

Je crois que la consultation en amont des textes est une bonne idée mais elle doit se faire dans un temps assez long et ne peut donc pas s'appliquer à tous les types de lois.
En d'autres termes, ce n'est pas aux citoyens de s'adapter à la méthode de l'assemblée nationale mais plutôt à l'assemblée de mettre en place une méthode de consultation qui crée l'adhésion des citoyens.

Pour bien faire, il faut à mon avis :

-une consultation préalable large et representative sur les objectifs de la loi soumise à consultation (sous forme de questions ouvertes ou fermées);
- la constitution d'un groupe de personnes (par exemple une 20aine tirée au sort faisant parti du premier groupe consulté et volontaire) accompagné par des experts et modérateurs
- ce groupe sera réuni pour vérifier la bonne mise en œuvre des principaux objectifs ressortant de la consultation initiale. Il pourrait être aussi impliqué dans le processus législatif.
- ce groupe, completé par d'autres volontaires tirés au sort, contribuera à l'évaluation de la loi.

Score : 0 / Réponses : 0

----

Une programmation annuelle de certains projets de lois
Il serait impossible de prévoir une programmation annuelle pour l’ensemble des projets de lois. Pour autant, le programme présidentiel fixe des objectifs de mise en œuvre de certaines politiques publiques sur la durée du quinquennat et peuvent faire l’objet d’une programmation annuelle et ainsi être rendue publique pour consultation

Cette programmation permettra de réserver des périodes incompréhensibles pour consultation des citoyens. Cette consultation fait l’objet de contribution intéressante notamment sur les moyens matériels de mise en œuvre

Score : 0 / Réponses : 0

----

Améliorer l'information aux citoyens sur les textes à l'étude : 

Ouvrir la procédure législative à la participation citoyenne est une très bonne chose. Mais au-delà de l'ouverture d'un espace de débat en ligne ou en présentiel, d'autres conditions sont indispensables pour permettre une participation plus large, et notamment une meilleure information du public ! 

Il faudrait ainsi que sur les sites parlementaires, on puisse plus facilement accéder : 
- au calendrier précis de l'examen des différents textes 
- aux textes de travail présentant les anciennes et nouvelles versions des dispositions législatives à l'examen (les textes de loi bruts étant difficiles à lire)

Score : 0 / Réponses : 0

----

1ère remarque : Les personnes qui accèdent à cette plateforme sont des citoyens qui s’intéressent de très près à la vie politique (au sens noble du terme) de leur pays et qui possèdent un esprit de curiosité. Ce n’est pas donné à tout le monde pour diverses raisons (compréhension, culture, temps, disponibilité, envie etc…). 
2ème remarque : Les autres citoyens, intéressés par la politique (heureusement il en reste !), s’en remettent majoritairement aux médias (journaux, télé, radio et réseaux sociaux) pour parfaire leur niveau d’information et cela leur parait suffisant.
Personnellement je ne suis pas convaincu que cette deuxième catégorie soit suffisamment motivée pour pénétrer les méandres du processus législatif de la « fabrication » de la loi.
Faire participer le maximum de citoyens volontaires à l’élaboration de la loi est un vœu plus que louable qu’on ne peut qu’approuver, toutefois, si certains textes de portée générale se prêteraient tout à fait à cet exercice, d’autres par contre, à caractère technique, ne peuvent être étudiés que par des spécialistes.
Les premières questions que je me pose lors de l’annonce par le gouvernement d’un futur projet de loi, c’est : Pourquoi ? Pour qui ? Comment ? Quand ?
Les réponses, en principe, figurent dans l’exposé des motifs du projet. C’est à partir de cet instant qu’il conviendrait d’être informé de son existence (de l’exposé - accompagné des avis des commissions ad hoc) afin de pouvoir poser les questions qui permettent de s’assurer de l’utilité, de la pertinence et des conséquences du texte législatif projeté, de sorte à ce que cela ne soit pas un texte de plus alors qu’une règlementation déjà existante pourrait se révéler suffisante. La seconde phase consisterait à mettre à disposition des parlementaires les avis et remarques personnalisées transmises par les citoyens qui ont pris connaissance du projet de loi, de sorte à enrichir les débats parlementaires et susciter d’éventuels amendements. Tant que nos institutions sont celles de la V ème république, la démocratie représentative doit rester la règle, ce qui n’exclut pas que la démocratie de proximité volontaire apporte sa contribution auprès du pouvoir législatif, me semble t-il.

Score : 0 / Réponses : 0

----

La démocratie française est malade de la monarchie présidentielle. Le pouvoir exécutif y marginalise le Parlement qui devrait être un lieu de publicité du débat et des décisions des représentants de la Nation. 
La démocratie française est malade de ses élus qui ne représentent que certaines catégories professionnelles ou sociales, et ne défendent que certains intérêts économiques imposés par la pensée formatée des médias. 
La Cinquième République dicte ainsi au citoyen des lois dont il ne veut pas. Pire !  Qu’il a refusé ! 
Que ce soit pour la Constitution européenne, rejetée par 54,68% des votants, mais finalement adoptée par l’assemblée et le Sénat, par l’usage antidémocratique du 49-3, les Français-e-s sont systématiquement tenus à l’écart des décisions. Ils ne croient plus à leurs représentants. Le taux de participation aux élections ne cesse de baisser. La France vit une crise démocratique profonde.

Score : 0 / Réponses : 0

----

Comment rendre le pouvoir au peuple, garantir la souveraineté de la France et protéger les biens communs pour les générations futures ? Par une révolution citoyenne et pacifique : l’écriture d’une nouvelle constitution et l’avènement de la 6ème République.

Score : 0 / Réponses : 0

----

Il manque clairement des médiateurs et des outils de médiation dans la mise à disposition de l'appareil législatif. La loi, et tout ce qui s'y rapporte, est normée dans une forme et un fond qui la rende inaccessible. Si ce jargon est indispensable aux législateurs, il faut une version vulgarisée pour les citoyens, plus dynamique avant même de penser à la rendre interactive, ce qui est facile avec le numérique.

Score : 0 / Réponses : 0

----

Les consultations citoyennes pourraient prendre la forme de questionnaires simples avec des choix par rapport à des idées dans un projet de loi et avoir également la possibilité de proposer d'autres idées. Si la proposition est portée par le gouvernement ce serait lui de faire cette consultation et si c'est l'assemblée nationale ce serait à elle de le faire. Pour permettre d'être sûr que les contributions sont bien étudiées ne serait il pas bon de permettre à des citoyens de siéger dans les commissions concernées par le sujet de la consultation.

Score : 0 / Réponses : 0

----

Bref en quelques mots essayer de faire des textes compréhensible pour la plupart une citrouille ayant un bagage intellectuel ou pas pouvoir avoir une meilleure idée ce qui se trame sous les textes et pourquoi pas plateforme téléphonique qui pourrait expliquer les choses plus en profondeur donc voilà ce que j'ai à dire beaucoup de gens beaucoup de gens aimeraient s'investir plus dans notre système mais vu Descriptif de certaines moi très difficile à déconner pour certains on du mal avoir participé aux débats une discussion et à proposer donc encore une fois plateforme téléphonique puisse bien expliquer les choses tout le monde plus que tout le monde puisse comprendre

Score : 0 / Réponses : 0

----

les propositions de loi devraient, à mon avis, être lue par une assemblée de diverses personnes concernés par la loi qui devraient pouvoir y apposer leur avis avant le vote final

Score : 0 / Réponses : 0

----

Consultation citoyenne (20 personnes) au Lion d'Angers
- Avoir un accès internet disponible pour chaque citoyen (notamment pour les zones rurales et les personnes âgées).
- Concilier les initiatives individuelles et groupes de réflexion.
- S'identifier et avoir la possibilité de contrôler l'identité réelle des contributeurs pour lutter contre les propos inadaptés au débat.
- Prévoir une déclaration d'intérêt pour contrôler l’influence des lobbies, groupes de pression.
- Définir un format codifié, simple et intuitif pour la consultation (exemple : formulaire avec des questions courtes et réponses à choix multiples complétées par une ou deux questions ouvertes...).

Score : 0 / Réponses : 0

----

Depuis de nombreuses années la démocratie a été bafouée par le législateur,  les citoyens ont leurs droits qui rétrécissent au fil des années mais de plus en plus de devoirs.... Autour de moi j'entends chaque jour la colère qui monte, monte !

Score : 0 / Réponses : 0

----

Associer les citoyens à la politique afin de faire remonter les remarques du "terrain" au niveau des Assemblées ne peut que re-instiller une plus grande motivation pour la politique. 
Beaucoup de personnes montrent une attitude désabusée face au débat politique de ces dernières années et à certains "dérapages" de certains politiciens qui se croient au dessus des lois. Le niveau des participants aux dernières élections est inquiétant pour la politique.
En vous remerciant pour cette belle initiative

Score : 0 / Réponses : 0

----

Je ne peux être que favorable à la consultation des textes par tous MAIS en vérité cela à vite ses limites par le fait que le jargon juridique amène vite une difficulté à la compréhension du texte pour tout à chacun qui n'est pas de la partie. D'autre part, pour tous ceux qui ont une activité professionnelle ou en recherche d'emploi, qui a le temps de lire cela (qui voudra prendre ce temps après ses journées d'activités).
Il y a les lanceurs d'alerte, les syndicats, l'opposition politique que nous pouvons remercier à juste titre nous alerter sur tel ou tel sujet avec leur compétence juridique et autres. Je leur demande une honnêteté intellectuelle pour nous expliquer objectivement les textes  de lois en projets.
Salut à tous.

Score : 0 / Réponses : 0

----

D'accord

Score : 0 / Réponses : 0

----

Une "vulgarisation" semble indispensable car elle toucherait plus de monde qu'actuellement.
Des vidéos sous-titrées, en français signé et des formats audio pourraient être ce support.
On pourrait imaginer un relais par les opérateurs téléphoniques pour un envoi d'un lien vers un site dédié où un questionnaire serait à remplir à la suite, comme les consultations actuelles des différents ministères.

Quant aux projets de réformes constitutionnelles, c'est plutôt difficile à dire. Parle-t-on d'un changement dû à une directive européenne ?

Score : 0 / Réponses : 0

----

Consulter les citoyens en amont... rien que de se poser la question de savoir si c'est utile permet de mettre en évidence la faiblesse de notre "démocratie"! Les dernières "consultations" étaient plus faites pour essayer de convaincre que pour prendre en compte des avis divergents. Quel valeurs juridiques donner à ces consultations? Quand on voit que la plupart des amendements ne sont même pas sérieusement étudiés à l'Assemblée Nationale à cause des consignes de vote (à quoi ça sert d'avoir 500 députés dans ce cas? Il suffit d'élire un roi pour 5 ans, ça nous couterait moins chère). Et qui participera à ces consultations? Le plus souvent ce sera les csp+, les groupes constitués (syndicats, ONG...) et les lobbyistes des grandes entreprises... Il manquera encore la parole d'une grande partie de la population qui est pourtant capable de donner son avis

Score : 0 / Réponses : 0

----

- Propositions de Renaissance Numérique et la Fondation Jean Jaurès

Systématiser le processus de co-construction de la loi 
- Systématiser le processus participatif de la loi numérique et l’amplifier : cela passerait par un ajout à la Constitution d’une procédure formalisée de co-élaboration, dans le processus de fabrication de la loi.
- Pour la préparation de chaque loi (projet ou proposition de loi), la mise en place d’une commission mixte élus/citoyens et d’une plateforme de concertation numérique devra être systématique. 
- La réforme de la Constitution devra particulièrement s’attacher à préciser ce nouveau processus d’élaboration des lois : les projets et propositions de lois faisant l’objet d’études d’impact ont vocation à intégrer ce nouveau dispositif de concertation citoyenne, dont la durée et l’ampleur peuvent être modulées. 
- Pour les projets et propositions de loi, la conférence des présidents de chaque assemblée pourra décider pareillement de la mise en place d’une commission mixte élus/citoyens (que ce soit sur l’ordre du jour prioritaire ou complémentaire), pilotée par la commission saisie au fond (qui adoptera in fine le texte présenté en séance, avec les amendements issus du processus collaboratif qu’elle acceptera et les amendements des parlementaires de la commission). Il importe que ce processus collaboratif n’entraîne pas un allongement du calendrier d’examen.

Budget participatif : le passage à l’échelle
- Comme pour la systématisation du processus de co-construction des lois, une telle disposition doit pouvoir être mise en place en ce qui concerne le budget de la nation, dans des conditions que précisera la réforme de la Constitution.

[Propositions issues du Livre blanc “Démocratie : Le réenchantement numérique” : http://bit.ly/2zAPucV]

Score : 0 / Réponses : 0

----

** 2/ Responsabiliser les institutions**
Les institutions à l’initiative des consultations en amont des textes doivent mettre en oeuvre le maximum pour donner confiance aux citoyens dans ces démarches de participation, notamment en garantissant un maximum de transparence à toutes les étapes. 

Pour cela, nous les invitons à prendre des engagements tant sur des aspects technologiques, que méthodologiques et éthiques, à savoir :
* Prendre des engagements forts quant au traitement des propositions citoyennes exprimées sur les outils numériques,
Notamment : permettre 	la traçabilité des propositions citoyennes dans la version finale de la proposition de loi (qu’a t-on concrètement retenu in fine)
* Recourir à des logiciels libres, dans un soucis de transparence et de sincérité envers les citoyens, 
* Publier en Open Data l’intégralité des contributions émises sur les plateformes de consultation, de manière complémentaire avec la publication d’une synthèse visuelle, dans un soucis de transparence et pour encourager la réutilisation de ces données par les citoyens, 
* Être transparent sur la publication, l’exploitation et la gestion des données issues des consultations.

Score : 0 / Réponses : 0

----

3/ Dans ce cadre, nous proposons l’adoption d’une **charte co-construite** qui couvre les aspects méthodologiques comme éthiques de ces consultations. Cette charte a vocation à donner confiance à chaque partie prenante dans ces démarches et à les pérenniser. 

Nous proposons également, pour simplifier au maximum l’expérience des usagers, la **création d’une plateforme numérique** (basée sur du logiciel libre) qui recense les consultations en cours autour de projets et propositions de loi. Les citoyens pourraient se connecter grâce à un identifiant numérique citoyen sécurisé, indiquer leurs thématiques de prédilection et être notifiés (mail, application, texto…) lorsqu’une consultation dédiée est ouverte. Ils pourraient ensuite déposer leurs contributions, et un système de vote permettrait de demander l’examen automatique d’une proposition ayant reçu un nombre X de votes. La gouvernance de la plateforme serait partagée entre les institutions concerné

Score : 0 / Réponses : 0

----

Deux obstacles limitent l'accès à la loi par les simples citoyens et donc leur capacité à y contribuer :
- beaucoup de textes modifient des mots par-ci par-là renvoyant à des modifications antérieures de même nature, on ne peut que se perdre dans une telle cascade ;
- l'architecture globale des textes n'est pas documentée (et manifestement est complexe).

Ne faudrait il pas les intégrer dans un outil de "traitement de texte" qui comme pour le code source des logiciels permettrait de mieux les structurer ?

Score : 0 / Réponses : 0

----

Pour la consultation en amont, il faudrait que les différents groupes parlementaires  puissent présenter et exposer le contexte qui les incitent à proposer une loi ainsi que de motiver les orientations qui entendent donner à cette loi.

De même, il faudrait que la présidence de l'assemblée nationale, donne une sorte de résumé des lois existantes sans en oublier le contexte qui les a naître.

Enfin, il revient aux députés d'organiser dans leur circonscription des ateliers pour expliquer la proposition de loi et éventuellement l'enrichir des contributions citoyennes. Il me paraît aussi important de travailler à la vulgarisation des textes de lois. Pourquoi pas en associant à ces ateliers, étudiants ou professeurs de droit et/ou tout autre secteur concerné par une nouvelle loi.

Score : 0 / Réponses : 0

----

Calendrier Budgétaire des collectivités : Les collectivités font leurs prévisions budgétaires dans le cadre d’un calendrier vertueux qui peux s’achever en Novembre ou Décembre pour certaines et de Janvier au maximum pour le 15 Avril de l’année (sauf année d’élection pour faire simple). Ne serrait il pas possible de les associer et / ou de les former (élus techniciens) au PLF de l année suivante car pour la plupart Il faut donc commencer le BP et DOB dès la fin de l’été.
Faisant également des projections sur des cycles de 3 à 5 ans. L’exercice est de plus en plus délicat en l’absence de données certaines principalement sur les recettes et cette année encore plus particulièrement compte tenu des annonces sur les mesures gouvernementales.
 
En revanche les collectivités doivent prendre des délibérations sur la fiscalité à appliquer en N+1 et ce avant le 01/10 de l’année en cours. C’était le sens de ma question sur les contradictions actuelles du calendrier qui met les collectivités en difficulté de prise de décision de projection sans avoir le contenu du PLF plus en amont.

Score : 0 / Réponses : 0

----

Ceci est une contribution collective du comité Eurométropole Strasbourg Nord .
En réunion le 1er novembre, nous avons abouti aux propositions suivantes :
1 - la consultation en amont est un moyen d'identifier les attentes des français dans le domaine d'application de la future loi. Cette consultation directe est sans intermédiaire et donc sans déformation ni filtre. 
2 - Cette consultation en amont peut se faire par voie numérique sur Internet avec des points relais en mairie avec un agent spécialement formé pour recevoir les contributions des citoyens privés d'Internet ou mal à l'aise avec les outils numériques (éviter la fracture numérique) et les transcrire en direct sur l'outil numérique de consultation.
3 - Cette consultation doit se faire sur la base 
- d'un résumé grand public synthétisant les enjeux de la future loi (les problèmes à résoudre), les grands objectifs, les principaux points saillants de la loi 
- d'un questionnaire à questions fermées pour simplifier l'agrégation des résultats.
4 - De fait, cette consultation doit se faire après une première réflexion en commission (pour dessiner les premiers contours de la future loi et rédiger le résumé grand public) et avant le premier examen en séance plénière à l'Assemblée Nationale avec un délai pour répondre d'au moins un mois.
5 - Les résultats de la consultation publique en amont doivent être rendus publics et diffusés sur Internet
6 - L'Assemblée Nationale doit systématiquement rédiger un rapport explicite pour comparer les résultats de la consultation en amont et les dispositions finalement retenues en expliquant / justifiant les écarts.
7 - les indicateurs de mise en oeuvre et d'impact des différents éléments de chaque loi doivent être élaborés et rédigés par les parlementaires au fur et à mesure de la rédaction de la loi et publiés en annexe au JO. L'évaluation régulière en sera facilitée.

Secrétaire du groupe de travail : Vincent MATHIEU

Score : 0 / Réponses : 0

----

Propositions collective de l'un des 4 groupes de travail mis en place par les comités locaux de Villeurbanne (Rhône). 

Dans une réflexion collective, nous avons voulu faire quelques propositions sur cette consultation en amont des textes législatifs. Cette consultation des citoyens est la bienvenue, car elle aura la vertu de minimiser les risques de mobilisations dans la rue pour contester un certain nombre de réformes, le citoyen ayant été préalablement consulté.  Il est donc important de commencer par la création d'une plateforme numérique interactive qui permettra un échange entre les parlementaires et les citoyens, ces derniers pourront donner leur avis sur des textes en préparation. Les projets de loi seraient d'abord débattus sur cette plateforme citoyenne, les modifications nécessaires apportées, avant la présentation au parlement pour discussion et adoption si possible. Bien évidemment il se pose la question de la valeur de ces consultations pour les élus, il serait dommage de mobiliser les citoyens pour une consultation en amont sachant que leurs avis ne seront pas pris en compte. Penser une consultation des citoyens en amont c'est aussi envisager une nouvelle culture politique de la part des parlementaires, faire participer les citoyens au débat parlementaire suppose qu'il y aura une prise en compte leurs propositions, de leurs questionnements, de leurs différents apports. Enfin, ces consultations doivent concerner les projets de loi non finalisés, ainsi la consultation permettra une meilleure adhésion des citoyens après adoption des textes par le parlement.

Score : 0 / Réponses : 0

----

Bonjour, 
Le think tank point d'aencrage a publié cette année son rapport démocratie, technologie & citoyenneté. 

http://pointdaencrage.org/2017/05/09/democratie-technologie-citoyennete-construire-nos-institutions-numeriques/

Parmi les 10 propositions du rapport on trouve en particulier:  

SYSTÉMATISER LES CONSULTATIONS EN ÉCHANGE D’UNE ACCÉLÉRATION DU TEMPS LÉGISLATIF
Il s’écoule généralement de longs mois entre la préparation d’un projet de loi par le Gouvernement ou le dépôt d’une proposition de loi par des parlementaires et l’exécution effective des décrets d’application - lorsque ces derniers ne sont tout simplement pas relégués jusqu’à l’oubli malgré les jours et les nuits de débats animés dans les deux chambres et dans la société. Ajouter encore un ou plusieurs temps de consultation des citoyens peut paraître inconséquent.
Le quinquennat qui s’achève a vu la France s’inscrire parmi les nations pionnières en matière de fabrique collaborative de la loi. Nous pensons naturellement à la démarche d’Axelle Lemaire sur le projet de loi « République numérique », mais aussi aux différentes consultations sur les sites de l’Assemblée nationale ou ceux des associations Parlement & Citoyens et Regards citoyens, ainsi qu’aux projets des développeurs qui se regroupent dans des collectifs comme Légilibre et #MAVOIX pour faire aboutir le projet d’une plateforme permettant aux citoyens de suivre et de participer en temps réel à l’écriture de la loi.
Des élus de tous bords ont déjà repris la proposition de généraliser les processus de consultation à l’écriture de toutes les lois et l’Etat s’est engagé à adopter une plateforme libre. La légitimité supplémentaire apportée par ces processus devrait permettre de limiter le reste de la procédure parlementaire à une lecture par chambre. Nous y sommes favorables tout en restant vigilants à la nécessaire extension du public et des modalités de participation
en amont, pendant et dans le contrôle a posteriori de la procédure législative.

Score : 0 / Réponses : 0

----

Le problème de ce genre d'initiative est qu'il n'y a eu aucun ou très peu de communication donc beaucoup de citoyens ne savent pas qu'elle a eu lieu et ne peuvent pas participer. Pour ma part, je viens de la découvrir.
quelques commentaires:
* simplifier les textes de loi pour que les citoyens les comprennent
* évaluer les textes de lois précédents connectés, voir ce qui est appliqué ou non et pourquoi,  pour faire des synthèses au lieu d'empliler les lois les unes sur les autres
* tenir compte des pétitions en ligne auxquelles beaucoup de citoyens participent sur des grands sujet de sociétés,  comme l'égalité homme/femme dans le travail,  ou lutter contre les violences faites aux femmes, ou encore comment lutter contre l'empoisonnement de notre environnement par les pesticides, et en particulier quand ces pétitions interpellent les députés, les sénateurs qui souvent n'y répondent pas.  Elles sont souvent bien rédigés et documentées.

Score : 0 / Réponses : 0

----

s’inspirer du travail mène par Marie-Aleth Grard au Cese pour écrire le rapport sur « grande pauvreté et réussite scolaire »: travailler en croisement des savoirs avec des citoyens travaillant en groupes de purs et portant ainsi une parole collective.
- puisque « nul n’est censé ignorer la loi », créer un groupe de citoyens (renouvelé régulièrement pour éviter un effet de « professionnalisation » par tirage au sort pour garantir la représentativité) évaluant la lisibilité des lois à adopter.

Score : 0 / Réponses : 0

----

Le problème est flagrant, notre société se modernise avec l’apparition de nouveaux outils de communication et de l’autre côté notre système démocratique français ne laisse pas la possibilité à chaque citoyen de participer à l'élaboration des lois. 

Le plus simple serait de créer une application disponible sur smartphone, gratuite, accessible et simple d’utilisation. Par le bais de cette application les députés, sénateurs et le gouvernement notifierait en amont leurs projets et propositions de loi. Les propositions seront ainsi directement communiquées sur l’application. Chaque citoyen disposant de l’application pourrait proposer un avis pour la compléter, une autre reformulation de la proposition, une question que pose la proposition, ou rejeter la proposition en expliquant pourquoi. Le but étant d’instaurer un dialogue, un débat, une nouvelle vision sur la proposition. Nous serons donc en tant que citoyens directement concernés et nos avis pris en compte. 

Chacun pourrait discuter des propositions faites par les autres citoyens. Le député ou sénateur qui a formé la question pourrait aussi en discuter directement avec les citoyens. Chaque question ouverte serait disponible sur l’application pour une durée déterminée. Passé ce délai, le député ou sénateur qui a soumis la question rédige sa proposition enrichit bien sûr de tous les commentaires et la soumet au Parlement avec la procédure habituelle. 

De plus, les projets de loi faits par le gouvernement devront aussi figurer en amont sur l'application et devront être traitées au même titre que les propositions de loi. 

Dès le moment où vous donnerez un avis sur une proposition vous recevrez grâce à des notification les autres avis des citoyens puis les débats au Parlement du texte et enfin sa publication. Vous avez ainsi la possibilité de suivre la proposition jusqu’à son adoption. 

Toutefois, il faut prendre en compte que tous les citoyens ne disposent pas de smartphone. C'est pour cela qu'en parallèle de cette application il faudrait aussi généraliser les ateliers citoyens faits en amont de l'élaboration des lois. 

L'application aurait de multiples avantages mais le plus important est de donner un moyen de communication moderne aux citoyens qui souhaitent s'investir dans l'élaboration des lois.

Score : 0 / Réponses : 0

----

proposer la lecture des textes à un pannel de citoyens intéressés et non spécialistes et lui demander s'il comprend le vocabulaire employé

proposer à la profession judiciaire et juridique un avis simple

demander aux députés de faire remonter les questions ou propositions des citoyens 'de base' qu'ils solliciteront

Score : 0 / Réponses : 0

----

Je vais globalement synthétiser ce qui avait été dis lors du groupe de travail différentes idées sont ressorties du brainstorming. 

 En termes de méthodologie, l'ambition globale était de proposer une diversification des canaux de la consultation de manière à n'exclure aucun citoyen de la démocratie participative y compris ceux qui ne sont pas "connectés". Dans cette même perspective, il est important à chaque étape d'avoir un échantillon représentatif pour éviter le "biais" statistique. 

Parmi les différentes idées proposées, une plateforme d'initiative citoyenne pourrait être mise en place qui pourrait comprendre différents élements. La possibilité de proposer des amendements par exemple avec un système de votation par les internautes. A partir d'un certain nombre de votes, les députés seraient dans l'obligation d'en tenir compte ou devront se justifier (cette obligation peut devenir constitutionnel). Il pourra y avoir des rapporteurs (citoyens ou députés) qui auront pour role d'expliquer le projet de loi et/ou amendement à voter. En termes technologiques, il s'agirait d'une plateforme intégrant éventuellement une blockchain pour la sécurisation du vote, la transparence et la traçabilité des projets de lois. 

Une seconde option serait de rendre également la consultation plus ludique et de rapprocher la loi du citoyen. Il serait par exemple possible de mettre en place des MOOCs ou encore de le faire sous la forme d'une gamification. Cette dernière permettrait de schématiser la situation et de "jouer" avec la loi, cette dimension pouvant prendre en compte les impacts de la loi également.

Dans une logique toujours de non-exclusion, il a été proposé la mise en place de comités de citoyens "en réel", l'organisation de rencontre également avec le/la député(e) de sa circonscription.

Score : 0 / Réponses : 0

----

Il me parait très utopiste d'imaginer que tous les citoyens ont quelque chose à dire sur tel projet de loi et même le souhaiteraient. Il me semble beaucoup plus constructif de procéder par étapes  en :
> publiant les projets de loi à venir et leur objectif
> faisant appel à participation des acteurs de terrain à même d'avoir une connaissance empirique ou à des volontaires intéressés à se pencher sur la question en débat
> d'en passer par des réunions de travail collaboratifs avec des temps de réflexion, de brainstorming et des temps de consultation, de positionnement point par point
Peut-on imaginer que les Français accepteraient de se prépositionner sur des centres d'intérêt et de faire partie d'un vivier consultatif, ce que nous pratiquons finalement à LaREM?
Sur les réformes constitutionnelles, elles devraient être explorées, et adoptées sur la base d'une consultation soumise à toute personne en âge et position de voter. 
On voit à la télévision des candidats inconnus défendre leurs talents. Pourquoi ne pas mettre la politique en vue sur un mode plus attractif, moins belliqueux, voire ludique dans un programme où les gens voteraient pour leurs favoris sur un pitch "la France de demain"?

Score : 0 / Réponses : 0

----

Ces consultations préalables doivent-elles être repensées ? 
-> Oui, dans le sens où il faut former les citoyens sur les sujets qui vont donner lieu à consultation, mettre en place non seulement des documents "officiels", mais suffisamment d'éléments de vulgarisation pour que les citoyens comprennent ce qu'ils commentent. 

systématisées ? 
-> Cela semble difficile à mettre en place mais est souhaitable, oui. 

Selon quelles modalités ? 
-> voir ci-dessus pour la formation avant la consultation. Pour ce qui est de la phase de consultation, il pourrait être envisagé d'utiliser une plateforme collaborative de type Wiki pour que les contributions individuelles puissent être agrégées en un contenu qui s'enrichit par l'intelligence collective. Ce qui n'empêche pas d'avoir des position différentes qui se constituent. Et avec une possibilité d'apporter son "vote" phrase par phrase. Ressortiraient ainsi les phrases les plus soulignées, dans un contexte plus large. 
  
Qui doit les organiser ? 
-> Une plateforme peut être mise en place par les pouvoirs publics (le parlement) et animée par des ONG qui proposeraient des premières bases de positions. 

Par ailleurs, comment s'assurer que les contributions recueillies sont bien étudiées en vue de la décision finale ?
-> Ainsi, l'étude des propositions pourrait être priorisée, avec une orientation selon le nombre de "votes", sachant que de fait le nombre des contributions devrait diminuer, et la qualité/profondeur des contributions s'en trouvera renforcé.
 
Une question spécifique se pose s’agissant des projets de réforme constitutionnelle, qui touchent aux « règles du jeu » démocratique et qui suivent une procédure d’adoption particulière. Faudrait-il envisager, pour les plus importants d’entre eux, des modalités spécifiques de participation citoyenne ?
-> la "formation préalable" devrait être fortement augmentée dans ce cas.

Score : 0 / Réponses : 0

----

OUI il faut pouvoir travailler collectivement sur les documents préparatoires
Quand un texte est le support d'un débat collectif, un dispositif doit rendre lisible les passages débattus, l'évolution du débat  et la nature des arguments. En effet le débat collectif doit montrer un déploiement sur l'écran pour permettre aux plus jeunes d'exercer son esprit critique et aux plus anciens d'apporter leur expérience sans rendre confus son exposition. Le système de commentaire utilisé n'a visiblement  pas ces qualités.  Faire un accès correct au débat pour tout participant est un point fondamental de l'éthique des débats.

Score : 0 / Réponses : 0

----

Le Conseil Législatif Citoyen (ou CLC)

Cette contribution est un résumé. La totalité de ce travail est le produit d'une réflexion juridique, approfondie et structurée. Il n'est pas possible d'en présenter ici tous les éléments et les spécificités. 

Il s'agit d'une part, de créer une réelle initiative législative populaire, et d'autre part, de rétablir le député dans ses fonctions de représentant de la Nation tout entière.

Ce nouvel organe a pour vocation d'être un "filtre" entre les citoyens et les députés.
L'action du CLC est guidée par deux principes : impartialité et indépendance

Sa mise en place et son fonctionnement n'engage que des fonds très limités au regard de sa composition, des compétences et de la procédure envisagée.
Le CLC est composé d'environ 140 personnes, en respectant la parité au sein de chaque catégorie.
Le CLC est composé d'intervenants locaux, de juristes et de représentants départementaux.
Pour préserver une totale indépendance, les membres du CLC :
-	Ne peuvent pas avoir le statut de fonctionnaire.
-	Sont bénévoles. Seuls les frais d'hébergement et de déplacement sont remboursés sur présentation de justificatifs.
-	Sont nommés en interne, par les membres en place. Aucune instance étatique intervient dans le processus de désignation, à l'exception de la mise en place du premier CLC.

Le CLC a trois compétences principales :
•	La vérification de l'opportunité et de la pertinence de la sollicitation reçue.
•	La présentation au(x) député(s) de nouveaux textes. 
•	Le contrôle du travail parlementaire sur le texte présenté.

Aperçu de la procédure devant le CLC :
Elle s'effectue dans un délai maximum de deux mois.
Un groupe de citoyen met en place une sollicitation, en ligne et / ou en présentiel, dans le but de recueillir 10 000 signatures. Ce texte est présenté et argumenté devant le CLC par un représentant départemental.
À l'issue d'un débat, de vérifications et de la rédaction, le texte définitif est soumis au député, qui, en fonction de l'objectif visé, le porte devant les différentes instances du Parlement, comme une proposition de loi ou un amendement. 
Le député doit rendre compte au CLC de l'avancée de la procédure jusqu'à l'adoption du texte.
Une nouvelle collaboration doit se mettre en place.

Cette nouvelle instance peut inciter les citoyens à reprendre possession de la vie démocratique, à intervenir pleinement dans l'élaboration des textes qui régissent leur vie quotidienne.

Score : 0 / Réponses : 0

----

Propos collectés lors d’un atelier citoyen à Saint-Jean-de-Luz organisé par Vincent Bru, député des Pyrénées Atlantiques et les comités locaux LaREM:
Constituer un panel représentatif de citoyens (tirage au sort?) en prenant garde  à ce que cela ne devienne pas l’instrument de  lobbies pour faire remonter des intérêts. Ce panel pourrait auditionner les représentants des collectivités territoriales, ou encore l'association des maires de France etc... pour solliciter leur avis. Il est important de prendre en compte la parole des élus locaux dans la fabrication de la loi.

Score : 0 / Réponses : 0

----

Il semble important de donner aujourd'hui une meilleure visibilité aux citoyens sur le calendrier des réformes proposées par le Gouvernement. 

Il faut par ailleurs systématiser les consultations numériques sur les grands projets de loi, afin de donner la voix à l'ensemble des acteurs d'un secteur donné, qu'ils soient sociaux, économiques, culturels, politiques, ou de simples citoyens désireux de participer à l'élaboration des grands objectifs d'une loi. 

Enfin, pour palier à la fracture numérique qui préexiste, il semble nécessaire d'institutionnaliser les ateliers citoyens en circonscription, par le biais des parlementaires, chargés, avec leurs équipes, de faire remonter les retours des citoyens rencontrés. 

> Contribution à l’atelier citoyen organisé par Olivier Véran, Député de la 1ère circonscription de l’Isère, le vendredi 3 novembre 2017

Score : 0 / Réponses : 0

----

La démocratie participative implique que les avis et les consultations des citoyens soient effectivement pris en compte par la représentation nationale. L'exemple de la pétition contre la loi travail qui avait réuni contre elle plus de 1 million de signatures puis qui fut superbement ignorée par les parlementaires et le gouvernement avec les résultats qu'on a vu  n'incite pas à l'optimisme.

Afin que cet exercice soit autre chose qu'une simple opération de communication pour espérer endiguer la défiance grandissante vis à vis des parlementaires, il faudrait que ces consultations aient une force contraignante. 

Il serait utile d'envisager une modification de la constitution avec à minima un système calqué sur les initiatives populaires de nos voisins suisses. Ce système (pétition conduisant à référendum) se traduit in fine en loi d'application immédiate. En France, un pas dans cette direction a été fait pour en réalité mieux en saper toute possibilité pratique.... Pourtant, nos voisins Suisses disposent de ce droit depuis au moins 1891. Le pays n'est pas à feu et à sang et les citoyens ont le sentiment de pouvoir se faire réellement entendre. Les Helvètes sont capables de le faire. Pourquoi pas les Français, Mesdames, Messieurs les députés?

En ce qui concerne les modifications de la constitution et l'adoption ou la dénonciation des traités internationaux, il me semble clair qu'une majorité de nos concitoyens souhaite que cela puisse se faire (comme c'est le cas dans certains pays) uniquement par la voie du référendum.

Score : -1 / Réponses : 0

----

Consulter ne veut pas dire prendre en compte dans les de2bats et dans le vote de la loi et des amendements tous les apports de la consultation citoyenne. Le député a été élu sur une programme de gouvernement et c'est dans cet esprit et en respect de ses engagements qu'il vote les lois. En revanche une consultation périodique trimestrielle des électeurs de sa circonscription sur la base des projets a venir sur le trimestre assorti d'un CR de mandat serait un bon moyen de mobiliser la population sur le travail législatif.

Score : -1 / Réponses : 0

----

Il est dit depuis des milliers d année quebpour qu un peuple de saisissent et comprennent les lois qui régissent la vie en société, il doit en avoir une part active dans son élaboration : réflexion, élaboration, construction et vote. Le citoyen, hormis sa représentation démocratique et électorale, dont on le sait aujourd’hui, se sent et s en sent bien éloigné, doit avoir la capacité d agir concrètement et réellement. Les lois sont faites par les citoyens et pour les citoyens. Par eux, pour eux. C est donc une réflexion en profondeur qui doit être réalisée, pour réaménager la vie politique et nos institutions. 
En d autre thermes : constitution de conseil citoyen dans toutes nos institutions ( commune, département, région,
État, Europe) renouveller tous les’ans Pour rester dans une dynamique de construction active et d une chance de participer plus importante pour tous. 
Au niveau de l état et de l Europe choix proportionnellement réalisé via les législatives de citoyens électeurs et inscrit dans des partis politiques.
Au niveau des collectivités territoriales un choix réalisé part tirage au sort en fonction des listes électorales.

Score : -1 / Réponses : 1

----

Pour que l’évolution soit démocratique elle doit tendre vers un accès au plus grand nombre et aussi loin que possible dans la participation à la rédaction.
Pour être capable de gérer un public étendu, il faut en premier lieu améliorer la lisibilité et la traçabilité. Par exemple sur ce site une suite de commentaires est rapidement difficile à lire et le lien avec le résultat final n’est pas évident.
Structurer le débat autour de plusieurs brouillons du texte final auxquels les usager pourront directement participer à la rédaction en proposant des amendements qui seront votés par la communauté pour être pris en compte. Chaque proposition ayant un préambule décrivant pourquoi elle existe, de quel original elle part et pourquoi elle en bifurque. Permettre aussi de créer une proposition à partir d’une feuille vierge.
Permettre de poser des questions sur les différents paragraphes du texte (liaison graphique) pour éliminer la technicité du texte par des bulles de vulgarisation dont le contenu sera coopté par les usagers.
Structurer également les commentaires sur ces propositions pour en faciliter la lecture, idées, arguments. Établir des règles automatisées évitant une modération humaine si possible.
Permettre des amendements du texte cooptés par les usagers dans le cadre de groupes de travail sur une proposition.
Faciliter les groupes de travail autour de ces propositions autant en ligne que physiquement en organisant des journées civiques où les personnes les plus avancées en technique viendront former les autres. Permettre aux personnes qui travaillent d’avoir le temps de venir à ces ateliers.
Relier humainement les usagers pour que ceux qui rédigent bien aident ceux qui ont du mal, par exemple permettre à une personne ayant une écriture difficile de soumettre anonymement son texte pour assistance, recevoir toujours anonymement des propositions de réécriture de son texte par les autres usagers parmi lesquelles il peut en choisir une après une éventuelle adaptation par lui-même.
Travailler par phases, d’abord faire venir le plus de participants possibles pour établir des propositions en nombre. Puis organiser des rencontres virtuelles et réelles pour fédérer les propositions compatibles autour de synthèses. Éventuellement faire un nouveau tour de discussion. Puis enfin lorsque le nombre de propositions incompatibles entre elles a atteint un nombre minimal procéder à un vote adapté à un choix parmi de nombreuses propositions comme le jugement majoritaire.

Score : -1 / Réponses : 0

----

Cette question pose la question de l'acces au droit et à la vulgarisation des textes légaux.

Score : -1 / Réponses : 0

----

ces consultations devraient être systématique, les modalités devraient être simple d'accès et d'utilisation (tout le monde n'a pas internet et une partie de la population se voit privée de donner son avis ou de contribuer à ce genre de consultation)
Elles devraient être organisées par l'organe qui fait la proposition et suffisamment claire pour sa compréhension
Pour s'assurer de leur prise en compte, qu'elles soient bien étudiées, un groupe indépendant de l'organe organisateur devrait être formé. Et pour finir, le résultat et la synthèse de cette consultation devrait être diffusée publiquement

Score : -1 / Réponses : 0

----

ça se gère comme un projet, il y a une phase de démarrage, où on pose un problème, et ça serait bien si on pouvait aussi intégrer la notion de son coût, 1 phase d'étude de faisabilité (et de rentabilité?), une phase d'établissement du cahier des charges et de planification, une phase de développement, un phase de test/pilote, une livraison (promulgation) et une évaluation post-mortem.  

Il y a une structure avec un chef de projet qui répond à une groupe directeur du projet avec les chefs des intervenants, et nous nous devrions en faire partie en ayant à tout moment accès aux détails du projet.  

On suggère pour ce faire un compte twitter par projet qu'on suivrait suivant le niveau d'intérêt qu'il suscite en nous, ou où on pourrait envoyer des remarques ponctuelles si les médias attirent notre attention sur tel ou tel point, et où on pourrait voter le passage d'une phase à l'autre. Le chef de projet  en ferait l'animation et y fournirait les liens vers le site où seront stocké les documents de travail, les textes en cours les amendements etc. pour consultation.  

Seuls les comptes certifiés électeur français devraient pouvoir accéder aux  comptes projet de loi.
Voir avec twitter c'est une fonctionnalité qu'ils peuvent coder facilement, et ils peuvent aussi vous fournir un site spécifique accessible de manière transparente mais relié à vos systèmes pour permettre ce contrôle et situé derrière un pare-feu.  Ils ne devraient pas vous faire pas payer ça très cher, avec un développement pareil, quel saut dans l'avenir et pour la cote de leur action..
Le grand avantage d'une telle solution est une mise en place éclair.  D'ailleurs vous pourriez déjà commencer avec deux trois prototypes sur des projets de lois d'importance moyenne.

Désolé si c'est un peu long.

Score : -1 / Réponses : 0

----

Créer un profil des élus en ligne sur lequel ils s'adressent aux citoyens de leur circonscription pour leur demander leur avis sur un texte de loi et leur proposer d'ajouter des amendements. Permet une interaction directe entre les élus et les citoyens et la possibilité de participer à la rédaction des lois.

Proposer qu'une pétition qui atteint un certain nombre de signatures doit être obligatoirement prise en compte.

Créer une newsletter obligatoire pour que les élus rendent compte de leurs travaux aux citoyens de leur circonscription.

Score : -1 / Réponses : 1

----

La multiplication des sites est un frein à la diffusion des informations. Je plaide pour l'existence d'un front office unique pour toute consultation officielle quelque soit l'organisme public à son origine. Ce front office pourrait être le lieu d'identification des contributeurs tout en laissant le libre accès en lecture à tous. Il n'est pas contradictoire avec la mise en ligne sur des sites différents des consultations, sites qui doivent cependant rester exclusivement public en format ouvert pour garantir la lisibilité et la traçabilité des travaux.

Score : -1 / Réponses : 0

----

PREALABLE à l'interaction du citoyen dans une loi :

Une procédure claire devrait être appliquée, au sens qu'avant de pouvoir faire une proposition, le citoyen a l'obligation de prendre connaissance des tenants et aboutissant de cette loi.
En aval un comité d'experts universitaires indépendants doit faire une synthèse des enjeux que peuvent avoir les modifications de la législation actuelle, en s'appuyant sur des données fiables et chiffrées.
Le citoyen doit lire cette synthèse et peut faire une proposition argumentée de son point de vue, qui peut aller dans un des sens de cette synthèse ou différée si elle est construite avec des exemples concrets, remettant en cause un ou plusieurs points de la synthèse.
Avec pour objectif final de concilier l’intérêt général avec l’intérêt individuel.

Score : -1 / Réponses : 0

----

C'est une excellente initiative. J'apprends par un mail reçu aujourd'hui que cela existe. J'avais, il y a +/- 15 jours envoyé un avis sur un aspects du projet de Loi finances 2018, à un membre de la commission ad hoc. J'avais un peu galérer pour juger à qui j'allais envoyer, ma demande d'information complémentaire et mes suggestions. J'ai évidemment exclus le président de cette commission..Ce qui m'intéressait, c'est la flat-tax, combinée à un abattement forfaitaire de 4000 EUR. Cette disposition permet à de petits épargnants, avertis cependant, de penser à l'épargne en actions. Je n'ai pas reçu le texte précis des articles en question, comme demandé.

Score : -1 / Réponses : 0

----

un referundum via ce site peut etre mis en place lors de la propositions d'une nouvelle loi et le resultat peut etre pris en compte par l'assamblé (comme un vote citoyen symbolique par exemple)

Score : -1 / Réponses : 0

----

La compréhension de la loi et des décisions politiques est un enjeu majeur. Hors, les "codes" culturels forment un délit d'initiés... Il faudrait qu'un service public dédié force les élus et fonctionnaires à expliciter les choix et décisions prises au nom du peuple. Les élus parlementaires ou territoriaux seraient tous concernés et confrontés non pas seulement à leurs pairs et à leur parti politique, mais à tous les citoyen-ne-s.
Les citoyen-ne-s seraient tous invités à participer à des assemblées locales, en proximité de leur lieux de vie, pour être informés des décisions publiques et questionner les fonctionnaires indépendamment des élus de l’exécutif.

Score : -1 / Réponses : 0

----

La compréhension des textes mais aussi leur longueur devraient être revu  à la baisse pour que tous personnes les  lisent en intégralité

Score : -1 / Réponses : 0

----

je pense qu'il faudrait , dans les ordres du jour des commissions,  avoir plus de détails sur le thème sur le quel on souhaite intervenir afin de faire remonter, suffisamment tôt dans les débats ,un avis ou une proposition. L'outil de consultation et de collecte des avis doit resté simple et consultable facilement.

Score : -1 / Réponses : 0

----

Le droit est une délimitation de la liberté individuelle et toute erreur de rédaction est vécue comme une atteinte à la personne par les citoyens concernés par un texte mal rédigé. Et ils sont très fréquents.
Or, les textes normatifs qui ont pour origine les projets gouvernementaux sont rédigés par des personnes certainement brillantes (pour la plupart énarques) mais qui ne sont pas des juristes, c’est à dire les professionnels qui ont appris à traduire en termes univoques les volontés politiques.
S’il convient donc que les citoyens soient associés en amont à l’élaboration du FOND des futures lois, il faut aussi établir un contrôle a priori sur la FORME afin que le texte - avant son vote solennel - soit rédigé clairement et dans un langage techniquement approprié. Il faut donc y associer des juristes (comme c’était le cas jusqu’à la fin des années 70) dont le rôle n’est pas de travestir les idées politiques mais au contraire d’en fournir une rédaction claire et concise pour en faciliter la compréhension par tous et donc l’effectivité.

Score : -1 / Réponses : 1

----

Proposition collective des comités strasbourgeois En Marche ! :
Organisation de consultations citoyennes par le Député sur une thématique précise dont l’objectif est de recueillir les avis de l’ensemble des citoyens, à proximité géographique de l’élu. Le député doit ensuite transformer ces avis en texte à défendre devant l’Assemblée nationale par une proposition de
loi.

Score : -1 / Réponses : 0

----

Accompagner chaque projet de loi par un SWOT afin de rendre visible les débats et le réalisme de la proposition, ses forces, ses faiblesses, ses risques et opportunités.

Score : -1 / Réponses : 0

----

Excellente initiative!

Score : -1 / Réponses : 0

----

Il conviendrait de systématiser les consultations préalables lorsqu'elles concernent certains domaines tels que l'emploi, l'économie, le social, etc. 
Ces consultations pourraient être organisées par la commission concernée par le(s) projet(s) de loi via une plate-forme comme celle sur laquelle nous interagissons à cet instant et où seraient exposés tous les éléments de nature à permettre aux citoyens de formuler des observations éclairées.
Un "avis d'appel à participation citoyenne" serait publié au Journal officiel et repris sur ladite plate-forme. Cet avis inviterait les citoyens à formuler leurs observations dans un délai déterminé (de 2 semaines à 1 mois imaginons) à compter de la publication de l'avis. Le délai pourrait être fixé par le président de l'Assemblée nationale.
Aucune condition de seuil (plancher) de participation ne devrait être fixée.
Lorsqu'il s'agit de textes sur la jeunesse ou l'éducation, il pourrait être également permis aux jeunes gens n'étant pas majeurs, mais ayant toutefois remplis leur obligation de recensement, de participer à ce processus.
Par la suite, les propositions exprimées durant la période de consultation et jugées les plus réalisables, pertinentes, réfléchies et plausibles, par la commission idoine, pourraient faire l'objet d'un résumé et être discutées en commission.
Si l'on ne saurait faire en sorte que les parlementaires soient liés par les propositions émises lors de la consultation citoyenne, cela aurait toutefois le mérite de susciter le débat et de porter à leur connaissance un certain nombre de problématiques relatives à l'impact du texte soumis à leur approbation, ou d'ouvrir d'autres pistes de réflexion.
Vive la France, vive la liberté d'expression!

Score : -2 / Réponses : 0

----

Consultations en amont des textes : gagner du temps avec un lien de redirection URL : http://www.medef.com/fr/grand-projets/france-2020

Score : -2 / Réponses : 1

----

Les citoyens ne sont pas écoutés ni consultés pour l'élaboration des projets de loi . Comme la mi NOTRE qui transfert aux EPCI que les communes ne veulent pas transférer comme l'eau potable et assainissement.

Score : -2 / Réponses : 1

----

pouvoir voir si les propositions des lois sont applicables et réaliste   une consultation en amont permetereas d avoir des lois en accord avec la réalité

Score : -2 / Réponses : 0

----

On pourrait envisager une consultation préalable avec un questionnaire en quelques points.

Score : -2 / Réponses : 0

----

Chaque président de groupe parlementaire devrait être dans l'obligation d'expliquer le vote de son groupe sur un projet de loi de façon claire et concise et à destination des citoyens. Pourquoi pas une vidéo de 2 à 5 min sur LCP?

Score : -2 / Réponses : 0

----

Pas une consultation en amont quand la loi est déjà faite mais une participation à cette loi mais j'imagine que, projets de loi concernés, c'est le domaine du Gouvernement et sinon rendre plus lisible les lois pour que les citoyens puissent mieux les comprendre et proposer des modifications le cas échéant.

Score : -2 / Réponses : 0

----

Les textes sont devenus trop bavards, les rendant pour beaucoup peu compréhensibles.
De plus, il est difficile de savoir à l'avance quels proposition ou projet de loi ira jusqu'à son terme alors que beaucoup de propositions sont déposées chaque semaine.
Il faudrait :
- pouvoir pousser par une consultation citoyenne une proposition de loi particulière.
- pouvoir avoir connaissance plus tôt des textes inscrits en commission. La loi s'élabore déjà au moment de son passage en commission, c'est à ce moment-là qu'il peut être intéressant de discuter d'un amendement citoyen.

Score : -2 / Réponses : 0

----

Le travail parlementaire est devenu trop lourd car trop important avec un nombre de normes incommensurables. Il faudrait déjà moins de normes donc moins de propositions ou de projets de loi et in fine, davantage d'application des lois déjà votées. 

La consultation en amont des textes législatifs et pourquoi pas réglementaires est une bonne idée si celle-ci ne perturbe pas le fonctionnement des chambres législatives. 

L'idéal serait que les commissions de chaque assemblée dispose d'un outil numérique permettant lors du travail en commission, celle-ci puisse disposer des avis et requêtes populaires.

Score : -2 / Réponses : 0

----

moi j'aimerais que la question politique soit plus claire , ex qui se soucie de la santé , insecticides où rien n'est clair depuis des années, des années vous vous rendez compte ?

Score : -2 / Réponses : 0

----

Afin de développer  la démocratie et que le peuple reprenne confiance, il serait très intéressant que tout texte soit validé d office de façon dématérialisée par la population et qu au titre de la transparence, les législateurs répondent aux questions des personnes souhaitant des précisions, Sans la majorité, le texte ne pourrait pas être adopté!! Et voilà une tres belle façon d innover et d entreprendre où chaque citoyen serai impliqué! : )

Score : -2 / Réponses : 0

----

Il faut simplifier l'ensemble es codes.
Une solution simple:
Pour tout nouveau texte ou article de lois, décrets, etc., il faut en supprimer le même nombre.
Ainsi au moins, cela ne compliquera pas nos lois.

Score : -3 / Réponses : 0
